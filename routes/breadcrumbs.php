<?php

// use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

// For Dashboard
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Dashboard', route('admin.dashboard'));
});

/**
 * For Admin Users
 */
Breadcrumbs::for('admin_users', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Admin Users', route('admin.admins.index'));
});
Breadcrumbs::for('admin_users.create', function ($trail) {
    $trail->parent('admin_users');
    $trail->push('Create', route('admin.admins.create'));
});
Breadcrumbs::for('admin_users.edit', function ($trail,$admin) {
    $trail->parent('admin_users');
    $trail->push('Edit', route('admin.admins.edit',$admin->id));
});
Breadcrumbs::for('admin_users.show', function ($trail,$admin) {
    $trail->parent('admin_users');
    $trail->push('Show', route('admin.admins.show',$admin->id));
});
Breadcrumbs::for('admin_users.access_logs', function ($trail,$admin) {
    $trail->parent('admin_users');
    $trail->push('Access Logs', route('admin.admins.access_logs',$admin->id));
});
Breadcrumbs::for('admin_users.activity_logs', function ($trail,$admin) {
    $trail->parent('admin_users');
    $trail->push('Activity Logs', route('admin.admins.activity_logs',$admin->id));
});
Breadcrumbs::for('admin_users.change_password', function ($trail,$admin) {
    $trail->parent('admin_users');
    $trail->push('Change Password', route('admin.admins.change_password',$admin->id));
});

/**
 * For CMS Pages 
 */
Breadcrumbs::for('cms_pages', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('CMS Pages', route('admin.pages.index'));
});
Breadcrumbs::for('cms_pages.create', function ($trail) {
    $trail->parent('cms_pages');
    $trail->push('Create', route('admin.pages.create'));
});
Breadcrumbs::for('cms_pages.edit', function ($trail,$page) {
    $trail->parent('cms_pages');
    $trail->push('Edit', route('admin.pages.edit',$page->id));
});
Breadcrumbs::for('cms_pages.show', function ($trail,$page) {
    $trail->parent('cms_pages');
    $trail->push('Show', route('admin.pages.show',$page->id));
});

/**
 * For Roles
 */
Breadcrumbs::for('roles', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Roles', route('admin.roles.index'));
});
Breadcrumbs::for('roles.create', function ($trail) {
    $trail->parent('roles');
    $trail->push('Create', route('admin.roles.create'));
});
Breadcrumbs::for('roles.edit', function ($trail,$role) {
    $trail->parent('roles');
    $trail->push('Edit', route('admin.roles.edit',$role->id));
});
Breadcrumbs::for('roles.show', function ($trail,$role) {
    $trail->parent('roles');
    $trail->push('Show', route('admin.roles.show',$role->id));
});

/**
 * For FAQ 
 */
Breadcrumbs::for('frequently_asked_questions', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Frequently Asked Questions', route('admin.frequently_asked_questions.index'));
});
Breadcrumbs::for('frequently_asked_questions.create', function ($trail) {
    $trail->parent('frequently_asked_questions');
    $trail->push('Create', route('admin.frequently_asked_questions.create'));
});
Breadcrumbs::for('frequently_asked_questions.edit', function ($trail,$frequentlyAskedQuestion) {
    $trail->parent('frequently_asked_questions');
    $trail->push('Edit', route('admin.frequently_asked_questions.edit',$frequentlyAskedQuestion->id));
});
Breadcrumbs::for('frequently_asked_questions.show', function ($trail,$frequentlyAskedQuestion) {
    $trail->parent('frequently_asked_questions');
    $trail->push('Show', route('admin.frequently_asked_questions.show',$frequentlyAskedQuestion->id));
});

/**
 * For SMS Templates
 */
Breadcrumbs::for('sms_templates', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Sms Templates', route('admin.sms_templates.index'));
});
Breadcrumbs::for('sms_templates.create', function ($trail) {
    $trail->parent('sms_templates');
    $trail->push('Create', route('admin.sms_templates.create'));
});
Breadcrumbs::for('sms_templates.edit', function ($trail,$smsTemplate) {
    $trail->parent('sms_templates');
    $trail->push('Edit', route('admin.sms_templates.edit', $smsTemplate->id));
});
Breadcrumbs::for('sms_templates.show', function ($trail,$smsTemplate) {
    $trail->parent('sms_templates');
    $trail->push('Show', route('admin.sms_templates.show',$smsTemplate->id));
});

/**
 * For Push Notification Templates
 */
Breadcrumbs::for('push_notification_templates', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Push Notification Templates', route('admin.push_notification_templates.index'));
});
Breadcrumbs::for('push_notification_templates.create', function ($trail) {
    $trail->parent('push_notification_templates');
    $trail->push('Create', route('admin.push_notification_templates.create'));
});
Breadcrumbs::for('push_notification_templates.edit', function ($trail,$pushNotificationTemplate) {
    $trail->parent('push_notification_templates');
    $trail->push('Edit', route('admin.push_notification_templates.edit', $pushNotificationTemplate->id));
});
Breadcrumbs::for('push_notification_templates.show', function ($trail,$pushNotificationTemplate) {
    $trail->parent('push_notification_templates');
    $trail->push('Show', route('admin.push_notification_templates.show',$pushNotificationTemplate->id));
});

/**
 * For In-App Notification Templates
 */
Breadcrumbs::for('in_app_notification_templates', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('In-App Notification Templates', route('admin.in_app_notification_templates.index'));
});
Breadcrumbs::for('in_app_notification_templates.create', function ($trail) {
    $trail->parent('in_app_notification_templates');
    $trail->push('Create', route('admin.in_app_notification_templates.create'));
});
Breadcrumbs::for('in_app_notification_templates.edit', function ($trail,$inAppNotificationTemplate) {
    $trail->parent('in_app_notification_templates');
    $trail->push('Edit', route('admin.in_app_notification_templates.edit', $inAppNotificationTemplate->id));
});
Breadcrumbs::for('in_app_notification_templates.show', function ($trail,$inAppNotificationTemplate) {
    $trail->parent('in_app_notification_templates');
    $trail->push('Show', route('admin.in_app_notification_templates.show',$inAppNotificationTemplate->id));
});


/**
 * Settings
 */
Breadcrumbs::for('settings', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('settings', route('admin.settings.index'));
});
Breadcrumbs::for('settings.create', function ($trail) {
    $trail->parent('settings');
    $trail->push('Create', route('admin.settings.create'));
});
Breadcrumbs::for('settings.edit', function ($trail,$setting) {
    $trail->parent('settings');
    $trail->push('Edit', route('admin.settings.edit',$setting->id));
});
Breadcrumbs::for('settings.show', function ($trail,$setting) {
    $trail->parent('settings');
    $trail->push('Show', route('admin.settings.show',$setting->id));
});

/**
 * Contact Us
 */
Breadcrumbs::for('contact_us', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('contact_us', route('admin.contact_us.index'));
});
Breadcrumbs::for('contact_us.create', function ($trail) {
    $trail->parent('contact_us');
    $trail->push('Create', route('admin.contact_us.create'));
});
Breadcrumbs::for('contact_us.edit', function ($trail,$contact_us) {
    $trail->parent('contact_us');
    $trail->push('Edit', route('admin.contact_us.edit',$contact_us->id));
});
Breadcrumbs::for('contact_us.show', function ($trail,$contact_us) {
    $trail->parent('contact_us');
    $trail->push('Show', route('admin.contact_us.show',$contact_us->id));
});

/**
 * Email Queues
 */
Breadcrumbs::for('email_queues', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Email Queues', route('admin.email_queues.index'));
});
Breadcrumbs::for('email_queues.show', function ($trail,$email_queue) {
    $trail->parent('email_queues');
    $trail->push('Show', route('admin.email_queues.show',$email_queue->id));
});

/**
 *  For Clients
 */
Breadcrumbs::for('users', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Clients', route('admin.users.index'));
});
Breadcrumbs::for('users.create', function ($trail) {
    $trail->parent('users');
    $trail->push('Create', route('admin.users.create'));
});
Breadcrumbs::for('users.edit', function ($trail,$user) {
    $trail->parent('users');
    $trail->push('Edit', route('admin.users.edit',$user->id));
});
Breadcrumbs::for('users.show', function ($trail,$user) {
    $trail->parent('users');
    $trail->push('Show', route('admin.users.show',$user->id));
});
Breadcrumbs::for('users.membership', function ($trail,$user) {
    $trail->parent('users');
    $trail->push('Membership', route('admin.users.membership',$user->id));
});
Breadcrumbs::for('users.transactions', function ($trail,$user) {
    $trail->parent('users');
    $trail->push('Transactions', route('admin.users.transaction',$user->id));
});
Breadcrumbs::for('users.projects', function ($trail,$user) {
    $trail->parent('users');
    $trail->push('Projects', route('admin.users.project',$user->id));
});
Breadcrumbs::for('users.reviewsRatings', function ($trail,$user) {
    $trail->parent('users');
    $trail->push('Reviews & Ratings', route('admin.users.reviewsRatings',$user->id));
});
Breadcrumbs::for('users.access_logs', function ($trail,$user) {
    $trail->parent('users');
    $trail->push('Access Logs', route('admin.users.access_logs',$user->id));
});
Breadcrumbs::for('users.activity_logs', function ($trail,$user) {
    $trail->parent('users');
    $trail->push('Activity Logs', route('admin.users.activity_logs',$user->id));
});

/**
 * SMS Queues
 */
Breadcrumbs::for('sms_queues', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('SMS Queues', route('admin.sms_queues.index'));
});
Breadcrumbs::for('sms_queues.show', function ($trail,$sms_queue) {
    $trail->parent('sms_queues');
    $trail->push('Show', route('admin.sms_queues.show',$sms_queue->id));
});

/**
 * Push Notification Queues
 */
Breadcrumbs::for('push_notification_queues', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Push Notification Queues', route('admin.push_notification_queues.index'));
});
Breadcrumbs::for('push_notification_queues.show', function ($trail,$pushNotificationQueue) {
    $trail->parent('push_notification_queues');
    $trail->push('Show', route('admin.push_notification_queues.show',$pushNotificationQueue->id));
});

/**
 * For Email Templates
 */
Breadcrumbs::for('email_templates', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Email Templates', route('admin.email_templates.index'));
});
Breadcrumbs::for('email_templates.create', function ($trail) {
    $trail->parent('email_templates');
    $trail->push('Create', route('admin.email_templates.create'));
});
Breadcrumbs::for('email_templates.edit', function ($trail,$emailTemplate) {
    $trail->parent('email_templates');
    $trail->push('Edit', route('admin.email_templates.edit', $emailTemplate->id));
});
Breadcrumbs::for('email_templates.show', function ($trail,$emailTemplate) {
    $trail->parent('email_templates');
    $trail->push('Show', route('admin.email_templates.show',$emailTemplate->id));
});

/**
 * Industries
 */

Breadcrumbs::for('industries', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Industries', route('admin.industries.index'));
});

/**
 * Subcategories
 */

Breadcrumbs::for('sub-categories', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Subcategories', route('admin.sub_categories.index'));
});

Breadcrumbs::for('sub-categories.create', function ($trail) {
    $trail->parent('sub-categories');
    $trail->push('Create subcategories', route('admin.sub_categories.create'));
});

Breadcrumbs::for('sub-categories.show', function ($trail,$subCategory) {
    $trail->parent('sub-categories');
    $trail->push('View subcategories', route('admin.sub_categories.show',$subCategory->id));
});

Breadcrumbs::for('sub-categories.edit', function ($trail,$subCategory) {
    $trail->parent('sub-categories');
    $trail->push('Edit subcategories', route('admin.sub_categories.edit',$subCategory->id));
});

/**
 * Skills
 */

Breadcrumbs::for('skills', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Skills', route('admin.skills.index'));
});

Breadcrumbs::for('skills.create', function ($trail) {
    $trail->parent('skills');
    $trail->push('Create skills', route('admin.skills.create'));
});

Breadcrumbs::for('skills.show', function ($trail,$skills) {
    $trail->parent('skills');
    $trail->push('View skills', route('admin.skills.show',$skills->id));
});

Breadcrumbs::for('skills.edit', function ($trail,$skills) {
    $trail->parent('skills');
    $trail->push('Edit skills', route('admin.skills.edit',$skills->id));
});

/**
 * Categories
 */

Breadcrumbs::for('categories', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Categories', route('admin.categories.index'));
});

Breadcrumbs::for('categories.create', function ($trail) {
    $trail->parent('categories');
    $trail->push('Create categories', route('admin.categories.create'));
});

Breadcrumbs::for('categories.show', function ($trail,$categories) {
    $trail->parent('categories');
    $trail->push('View Categories', route('admin.categories.show',$categories->id));
});

Breadcrumbs::for('categories.edit', function ($trail,$categories) {
    $trail->parent('categories');
    $trail->push('Edit Categories', route('admin.categories.edit',$categories->id));
});

/**
 *  For Freelancers
 */
Breadcrumbs::for('freelancers', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Freelancers', route('admin.freelancers.index'));
});
Breadcrumbs::for('freelancers.create', function ($trail) {
    $trail->parent('freelancers');
    $trail->push('Create', route('admin.freelancers.create'));
});
Breadcrumbs::for('freelancers.edit', function ($trail,$user) {
    $trail->parent('freelancers');
    $trail->push('Edit', route('admin.freelancers.edit',$user->id));
});
Breadcrumbs::for('freelancers.show', function ($trail,$user) {
    $trail->parent('freelancers');
    $trail->push('Show', route('admin.freelancers.show',$user->id));
});
Breadcrumbs::for('freelancers.membership', function ($trail,$user) {
    $trail->parent('freelancers');
    $trail->push('Membership', route('admin.freelancers.membership',$user->id));
});
Breadcrumbs::for('freelancers.projects', function ($trail,$user) {
    $trail->parent('freelancers');
    $trail->push('Projects', route('admin.freelancers.project',$user->id));
});
Breadcrumbs::for('freelancers.transaction', function ($trail,$user) {
    $trail->parent('freelancers');
    $trail->push('Transactions', route('admin.freelancers.transaction',$user->id));
});
Breadcrumbs::for('freelancers.reviewsRatings', function ($trail,$user) {
    $trail->parent('freelancers');
    $trail->push('Reviews & Ratings', route('admin.freelancers.reviewsRatings',$user->id));
});
Breadcrumbs::for('freelancers.access_logs', function ($trail,$user) {
    $trail->parent('freelancers');
    $trail->push('Access Logs', route('admin.freelancers.access_logs',$user->id));
});
Breadcrumbs::for('freelancers.activity_logs', function ($trail,$user) {
    $trail->parent('freelancers');
    $trail->push('Activity Logs', route('admin.freelancers.activity_logs',$user->id));
});
/** 
 * Membership plans
 */

Breadcrumbs::for('membershipPlans', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Membership Plans', route('admin.membership_plans.index'));
});

Breadcrumbs::for('membershipPlans.create', function ($trail) {
    $trail->parent('membershipPlans');
    $trail->push('Create Membership Plan', route('admin.membership_plans.create'));
});

Breadcrumbs::for('membershipPlans.show', function ($trail,$membershipPlan) {
    $trail->parent('membershipPlans');
    $trail->push('View Membership Plan', route('admin.membership_plans.show',$membershipPlan->id));
});

Breadcrumbs::for('membershipPlans.edit', function ($trail,$membershipPlan) {
    $trail->parent('membershipPlans');
    $trail->push('Edit Membership Plan', route('admin.membership_plans.edit',$membershipPlan->id));
});

/**
 * Security Questions
 */

Breadcrumbs::for('securityQuestions', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Security Questions', route('admin.security_questions.index'));
});

/**
 * Rating Questions
 */

Breadcrumbs::for('ratingQuestions', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Rating Questions', route('admin.rating_questions.index'));
});

/**
 * Terms and Conditions
 */

Breadcrumbs::for('termsAndConditions', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Terms And Conditions', route('admin.terms_and_conditions.index'));
});

Breadcrumbs::for('termsAndConditions.create', function ($trail) {
    $trail->parent('termsAndConditions');
    $trail->push('Create Terms And Conditions', route('admin.terms_and_conditions.create'));
});

Breadcrumbs::for('termsAndConditions.show', function ($trail,$termsAndCondition) {
    $trail->parent('termsAndConditions');
    $trail->push('View Terms And Conditions', route('admin.terms_and_conditions.show',$termsAndCondition->id));
});

Breadcrumbs::for('termsAndConditions.edit', function ($trail,$termsAndCondition) {
    $trail->parent('termsAndConditions');
    $trail->push('Edit Terms And Conditions', route('admin.terms_and_conditions.edit',$termsAndCondition->id));
});

/**
 * Dispute
 */

Breadcrumbs::for('disputes', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Disputes', route('admin.disputes.index'));
});
Breadcrumbs::for('disputes.edit', function ($trail,$dispute) {
    $trail->parent('disputes');
    $trail->push('Edit', route('admin.disputes.edit',$dispute->id));
});
Breadcrumbs::for('disputes.show', function ($trail,$dispute) {
    $trail->parent('disputes');
    $trail->push('Show', route('admin.disputes.show',$dispute->id));
});
/**
 * Subscription Payments
 */

Breadcrumbs::for('subscription_payments', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Subscription Payments', route('admin.user_subscriptions.index'));
});

Breadcrumbs::for('subscription_payments.show', function ($trail,$invoice) {
    $trail->parent('subscription_payments');
    $trail->push('View Subscription Payments', route('admin.user_subscriptions.show',$invoice->id));
});

/**
 *  For Projects
 */
Breadcrumbs::for('open_projects', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Open Projects', route('admin.projects.index'));
});
Breadcrumbs::for('inprogress_projects', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Inprogress Projects', route('admin.projects.list_inprogress'));
});
Breadcrumbs::for('completed_projects', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Completed Projects', route('admin.projects.list_completed'));
});
Breadcrumbs::for('create_projects', function ($trail) {
    $trail->parent('open_projects');
    $trail->push('Create', route('admin.projects.create'));
});
Breadcrumbs::for('show_projects', function ($trail,$project) {
    if(in_array($project->status,[0,1])){
        $trail->parent('open_projects');
    }elseif(in_array($project->status,[2,3])){
        $trail->parent('inprogress_projects');
    }else{
        $trail->parent('completed_projects');
    }
    $trail->push('Show', route('admin.projects.show',$project->uuid));
});
Breadcrumbs::for('edit_projects', function ($trail,$project) {
    if(in_array($project->status,[0,1])){
        $trail->parent('open_projects');
    }elseif(in_array($project->status,[2,3])){
        $trail->parent('inprogress_projects');
    }else{
        $trail->parent('completed_projects');
    }
    $trail->push('Edit', route('admin.projects.edit',$project->uuid));
});

/**
 * Invoice 
 */

Breadcrumbs::for('create_invoice', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Create Invoice', route('admin.user_subscriptions.invoice'));
});

/**
 * Transactions
 */

Breadcrumbs::for('transactions', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Transactions', route('admin.transactions.index'));
});

Breadcrumbs::for('transactions.show', function ($trail,$transaction) {
    $trail->parent('transactions');
    $trail->push('Show Trasaction', route('admin.transactions.show',$transaction->uuid));
});

/**
 * Invoices
 */

Breadcrumbs::for('invoices', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Invoices', route('admin.invoices.index'));
});

Breadcrumbs::for('invoices.create', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Create Invoice', route('admin.invoices.create'));
});

Breadcrumbs::for('invoices.show', function ($trail,$invoice) {
    $trail->parent('invoices');
    $trail->push('Show Invoice', route('admin.invoices.show',$invoice->uuid));
});

/**
 * Project Payments
 */

Breadcrumbs::for('project_payments', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Project Payment', route('admin.project_payments.index'));
});

Breadcrumbs::for('project_payments.show', function ($trail,$project_payment) {
    $trail->parent('project_payments');
    $trail->push('Show Project Payment', route('admin.project_payments.show',$project_payment->uuid));
});