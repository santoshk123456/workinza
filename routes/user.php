<?php

use GuzzleHttp\Client;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'revalidate'], function () {
  
    Route::get('/', 'HomeController@home')->name('home');
    
    Route::get('/home', 'HomeController@newhome')->name('newhome');
    Route::get('/services', 'HomeController@services')->name('homeservices');
    Route::get('/login', 'HomeController@showLogin')->name('showLogin');
    
    Route::get('/forgot-password', 'HomeController@forgotPassword')->name('forgotPassword');
    Route::post('/reset-password', 'HomeController@resetPassword')->name('resetPassword');
    Route::post('/reset-password/verification', 'HomeController@resetVerification')->name('resetVerification');
    Route::post('/send-email-notification', 'HomeController@send_email_notification')->name('send_email_notification');
    Route::post('/contact-send-email-notification', 'HomeController@send_email_notification_contact')->name('send_email_notification_contact');
   
    // Route::get('/test', 'HomeController@test')->name('test');

    
    Route::group(['namespace'=>'Auth'], function () {
       
        Route::post('/login', 'LoginController@login')->name('login');
        Route::get('/logout', 'LoginController@logout')->name('logout');
        Route::get('/change-password', 'LoginController@changePassword')->name('change_password');
        Route::post('/email-exist', 'LoginController@emailExist')->name('emailExist');
        Route::post('/email-exist-check', 'LoginController@emailExistcheck')->name('emailExistcheck');
        //Route::get('/dashboard/view', 'LoginController@viewdashboard')->name('viewdashboard');
    });
    Route::group(['as'=>'user.'], function () {
        Route::group(['middleware'=>['auth:web']],function(){
            Route::get('/dashboard', 'UserController@viewdashboard')->name('viewdashboard');
        });
    });

    Route::group(['prefix'=>'register','as'=>'user.'], function () {

        Route::get('/', 'UserController@create')->name('register');
        Route::post('/user-type', 'UserController@userType')->name('userType');
        Route::get('/account-details', 'UserController@accountDetails')->name('accountDetails');
      



        
        Route::post('/', 'UserController@store')->name('store');
        Route::get('/{user}/email-verification', 'UserController@emailVerification')->name('emailVerification');
        Route::put('/{user}/email-verification/update', 'UserController@otpVerification')->name('otpVerification');
        Route::post('/resend/email', 'UserController@resendEmail')->name('resendEmail');
       

     

           Route::group(['middleware'=>['auth:web']],function(){
            Route::get('/contact-information', 'UserController@contactInformation')->name('contactInformation');
            Route::post('/contact-information/store', 'UserController@storeContactInformation')->name('storeContactInformation');
            Route::get('/contact-information/view', 'UserController@viewContactInformation')->name('viewContactInformation');

          
            Route::get('/change-password', 'UserController@changePassword')->name('change_password');
            Route::post('/user/change-password', 'UserController@userchangePassword')->name('user.change_password');
            Route::get('/contact-information/edit', 'UserController@editContactInformation')->name('editContactInformation');
            Route::post('/contact-information/update', 'UserController@updateContactInformation')->name('updateContactInformation');
            Route::get('/redirect-back', 'UserController@redirectBack')->name('redirectBack');
            Route::post('/username-exist', 'UserController@usernameExist')->name('usernameExist');
            Route::post('/phonenumber-exist', 'UserController@phoneNumberExist')->name('phoneNumberExist');
            Route::get('/security-questions', 'UserController@securityQuestions')->name('securityQuestions');
            Route::get('/security-questions/edit', 'UserController@editSecurityQuestions')->name('editSecurityQuestions');
            Route::get('/security-questions/view', 'UserController@viewSecurityQuestions')->name('viewSecurityQuestions');
            Route::post('/security-questions/update', 'UserController@updateSecurityQuestions')->name('updateSecurityQuestions');
            Route::post('/security-questions/store', 'UserController@storeSecurityQuestions')->name('storeSecurityQuestions');
            Route::get('/background-check', 'UserController@profileAcknowledgement')->name('profileAcknowledgement');
            Route::post('/payment', 'UserController@oneTimePayment')->name('oneTimePayment');
            Route::post('/store/payment', 'UserController@storeOnePayment')->name('storeOnePayment');
            
            Route::get('/background-verification', 'UserController@backgroundVerification')->name('backgroundVerification');

        });

    });
    Route::group(['prefix'=>'stripe','as'=>'stripe.'], function () {
        Route::post('/save-payment', 'UserController@saveOneTimePayment')->name('saveOneTimePayment');
    });
    Route::group(['prefix'=>'account','as'=>'account.'], function () {
        Route::group(['middleware'=>['auth:web']],function(){
            Route::get('/create-public-profile', 'UserController@createPublicProfile')->name('createPublicProfile');
            Route::post('/create-public-profile/store', 'UserController@storePublicProfile')->name('storePublicProfile');
            Route::get('/view-public-profile', 'UserController@viewPublicProfile')->name('viewPublicProfile');
            Route::get('/{user}/{project}/{status}/details', 'UserController@viewUserDetails')->name('viewUserDetail');
            Route::get('/edit-public-profile', 'UserController@editPublicProfile')->name('editPublicProfile');
            Route::post('/create-public-profile/update', 'UserController@updatePublicProfile')->name('updatePublicProfile');
            Route::get('/{user}/profile', 'UserController@myProfile')->name('myProfile');
        });
    });


    Route::group(['prefix'=>'project','as'=>'project.'], function () {
        Route::group(['middleware'=>['auth:web']],function(){
            Route::get('/create', 'ProjectController@createProject')->name('createProject');
            Route::post('/create/store', 'ProjectController@storeProject')->name('storeProject');
            Route::get('{project}/edit', 'ProjectController@editProject')->name('editProject');
            Route::put('{project}/update', 'ProjectController@updateProject')->name('updateProject');
            Route::get('open-projects', 'ProjectController@listOpenProjects')->name('listOpenProjects');
            Route::get('inporgress-projects', 'ProjectController@listInProgressProjects')->name('listInProgressProjects');

            Route::get('completed-projects', 'ProjectController@listCompletedProjects')->name('listCompletedProjects');

            Route::get('freelancer-inporgress-projects', 'ProjectController@freelancerlistInProgressProjects')->name('freelancerlistInProgressProjects');
            Route::get('{project}/details', 'ProjectController@projectDetails')->name('projectDetails');
            Route::get('{project}/inprogress-project-details', 'ProjectController@inProgressProjectDetails')->name('inProgressProjectDetails');
            Route::get('{project}/completed-project-details', 'ProjectController@completedProjectDetails')->name('completedProjectDetails');
            Route::get('/{project}/details', 'ProjectController@projectDetails')->name('projectDetails');
            Route::get('/{project}/proposals', 'ProjectController@projectProposals')->name('projectProposals');
            Route::get('/{project}/invite-freelancers', 'ProjectController@inviteFreelancers')->name('inviteFreelancers');
            Route::any('/{project}/search-freelancer','ProjectController@searchFreelancers')->name('searchFreelancers'); 
            Route::post('/send-invitation','ProjectController@sendInvitationToFreelancer')->name('sendInvitationToFreelancer');
            Route::get('/invitations','ProjectController@listInvitations')->name('listInvitations');
            Route::get('{project}/invitation-details','ProjectController@viewInvitation')->name('viewInvitation');
            Route::get('{project}/create-proposal','ProjectController@createProposal')->name('createProposal'); 
            Route::post('{project}/store-proposal','ProjectController@storeProposal')->name('storeProposal'); 
            Route::get('/my-proposals','ProjectController@listProposals')->name('listProposals');
            Route::get('{project_proposal}/edit-proposal', 'ProjectController@editProposal')->name('editProposal');
            Route::put('{project_proposal}/update-proposal', 'ProjectController@updateProposal')->name('updateProposal');
            Route::get('{project_contract}/contract', 'ProjectController@prepareContract')->name('prepareContract');
            Route::post('/create-contract', 'ProjectController@createContract')->name('createContract');
            Route::post('/accept-contract', 'ProjectController@acceptProposal')->name('acceptProposal');
            Route::get('/contracts', 'ProjectController@listContracts')->name('listContracts');
            Route::post('/file-upload', 'ProjectController@fileUpload')->name('fileUpload');
            Route::get('/force-download-file', 'ProjectController@forceDownloadFile')->name('forceDownloadFile');
            Route::get('{project}/add-document', 'ProjectController@addDocument')->name('AddDocument');
            Route::post('/store-document', 'ProjectController@storeDocument')->name('StoreDocument');
            Route::get('{id}/destroy-document', 'ProjectController@destroyDocuments')->name('DestroyDocuments');
            Route::get('{project}/add-milestones', 'ProjectController@addMilestone')->name('AddMilestone');
            Route::post('/store-milestone', 'ProjectController@storeMilestone')->name('StoreMilestone');
            Route::get('/remove-file', 'ProjectController@removeFile')->name('RemoveFile');
            Route::get('{project}/destroy-project', 'ProjectController@destroyProject')->name('DestroyProject');

            Route::get('{project}/hide-project', 'ProjectController@hideProject')->name('HideProject');

            Route::get('{project}/unhide-project', 'ProjectController@unhideProject')->name('unHideProject');

            Route::get('{milestone}/milestone-details', 'ProjectController@milestoneDetails')->name('MilestoneDetails');
            Route::put('{milestone}/post-message', 'ProjectController@storeComment')->name('PostComment');
            Route::post('/activate-milestone', 'ProjectController@ActivateMilestone')->name('ActivateMilestone');
            Route::post('/milestone-status-change', 'ProjectController@milestoneStatusChange')->name('milestoneStatusChange');
            Route::get('/find-a-project', 'ProjectController@findProject')->name('findProject');
            Route::get('/reset-filter', 'ProjectController@resetFilter')->name('resetFilter');
            Route::get('/search-name', 'ProjectController@searchByName')->name('searchByName');
            Route::get('{proposal}/view-proposal','ProjectController@viewProposal')->name('viewProposal');
            
            
            //});
        });
    });
    Route::group(['prefix'=>'finance','as'=>'finance.'], function() {
        Route::group(['middleware'=>['auth:web']], function(){
            Route::get('add-money/{milestone}', 'PaymentController@addMoney')->name('addMoney');
            Route::get('{milestone}/milestone-payment-success', 'PaymentController@milestonePaymentSuccess')->name('milestonePaymentSuccess');
            Route::get('/my-wallet', 'PaymentController@myWallet')->name('myWallet');
            Route::get('/my-vault', 'PaymentController@myVault')->name('myVault');
            Route::get('/create-checkout-session', 'PaymentController@createCheckoutSession')->name('createCheckoutSession');
            Route::get('/wallet-payment-success', 'PaymentController@walletPaymentSuccess')->name('walletPaymentSuccess');
            Route::get('/transactions', 'PaymentController@listTransactions')->name('listTransactions');
            Route::get('/invoices', 'PaymentController@listInvoices')->name('listInvoices');
            Route::get('/invoice-details/{invoice}', 'PaymentController@invoiceDetails')->name('invoiceDetails');
            Route::get('/invited-clients', 'PaymentController@invitedClients')->name('invitedClients');
            Route::get('/current-clients', 'PaymentController@currentClients')->name('currentClients');
            Route::get('transactions/{transaction}', 'PaymentController@viewTransaction')->name('viewTransaction');
            Route::get('referrals', 'PaymentController@listReferrals')->name('listReferrals');
            Route::get('bonus', 'PaymentController@listBonus')->name('listBonus');
            Route::get('refer-person', 'PaymentController@referPerson')->name('referPerson');
            Route::post('send-referral-invitations', 'PaymentController@sendReferralInvitation')->name('sendReferralInvitation');
            Route::get('/disputes', 'ProjectController@ListDisputes')->name('ListDisputes');
            Route::get('/dispute/{dispute}/view', 'ProjectController@viewDispute')->name('viewDispute');
            Route::get('/dispute/{milestoneId}/create/{projectId}', 'ProjectController@raiseDispute')->name('raiseDispute');
            Route::post('/dispute/store', 'ProjectController@storeDispute')->name('storeDispute');
            Route::get('{project}/project-transactions', 'PaymentController@projectTransactions')->name('projectTransactions');
        });
    });
    Route::group(['prefix'=>'membership-plan','as'=>'membership_plan.'], function() {
        Route::group(['middleware'=>['auth:web']], function(){
            Route::get('/', 'MembershipPlanController@membershipPlans')->name('membershipPlans');
            Route::get('{membershipPlan}/plan-review','MembershipPlanController@planReview')->name('planReview');
            Route::get('/subscription','PaymentController@subscriptionSuccess')->name('subscriptionSuccess');
            Route::post('/stripe-payment', 'PaymentController@StripePayment')->name('StripePayment');
             Route::post('/accept-proposal', 'ProjectController@acceptProposal')->name('acceptProposal');


        });
    });


    Route::group(['prefix'=>'message','as'=>'message.'], function() {
        Route::group(['middleware'=>['auth:web']], function(){

  Route::get('messages/{project?}/{freelancer?}','MessageController@message')->name('messages'); 
  Route::get('dispute-messages/{dispute}/{project}','MessageController@disputeMessage')->name('disputeMessages'); 
//   Route::get('message/{project?}/{freelancer?}','MessageController@disputeMessage')->name('disputeMessages'); 
  Route::post('/post-messages', 'MessageController@store_message')->name('PostMessage');
  Route::post('/post-message', 'MessageController@store_dispute_message')->name('PostDisputeMessage');
  Route::get('/change-archive','MessageController@changeArchive')->name('ChangeArchive'); 
  Route::get('/public-message','MessageController@publicMessages')->name('publicMessages'); 
  Route::get('/change-public-archive','MessageController@changePublicArchive')->name('changePublicArchive'); 
  Route::get('message-detail/{project?}/{user?}','MessageController@PublicMessageDetail')->name('PublicMessageDetail'); 
    

    
});
});

    

   
    Route::get('/home/pages/{id}', 'HomeController@homecmspages')->name('homecmspages');
    Route::get('/contact-us', 'HomeController@homecontactus')->name('homecontact');
    Route::get('/{username}', 'HomeController@userPublicProfile')->name('userPublicProfile');
  
    //Route::get('/pages/{slug}', 'HomeController@cmspages')->name('cmspages');

    Route::group(['prefix'=>'rating','as'=>'rating.'], function() {
        Route::group(['middleware'=>['auth:web']], function(){
    
            Route::get('{project}/add-rating','RatingController@addRating')->name('addRating');
            Route::put('{project}/add-rating','RatingController@addUserRating')->name('addUserRating');
            Route::get('{project}/add-company-rating','RatingController@addCompanyRating')->name('addCompanyRating');
            Route::put('{project}/add-company-rating','RatingController@storeCompanyRating')->name('storeCompanyRating');
            Route::get('/my-ratings','RatingController@myRating')->name('myRating');
            Route::get('/my-recieved-ratings','RatingController@myRatingReceived')->name('myRatingReceived');
            Route::get('/{rating}{type}/rating-detail','RatingController@ratingDetail')->name('ratingDetail');
    });
    });
});