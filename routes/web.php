<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('home');

// Auth::routes();

/**
 * Authentication Management
 */
    Route::group(['prefix'=>'admin','namespace'=>'Auth','as'=>'admin.'], function () {
    Route::get('/','AdminLoginController@showLoginForm')->name('login.form');
    Route::post('login','AdminLoginController@login')->name('login');
    Route::get('logout','AdminLoginController@logout')->name('logout');
    Route::get('login/{social}','AdminLoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket');
    Route::get('login/{social}/callback','AdminLoginController@handleProviderCallback')->where('social','twitter|facebook|google');
    Route::post('password/email','AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset','AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/reset','AdminResetPasswordController@reset');
    Route::get('password/reset/{token}','AdminResetPasswordController@showResetForm')->name('password.reset');

});

   Route::get('auth/google', 'Auth\AdminLoginController@redirectToGoogle');
   Route::get('auth/google/callback', 'Auth\AdminLoginController@handleGoogleCallback');
   // socialite login ( facebook  and twitter and github)
   Route::get('auth/{provider}', 'Auth\AdminLoginController@redirect');
   Route::get('auth/{provider}/callback', 'Auth\AdminLoginController@callback');


   

/**
 * Admin users portal
 */
    Route::group(['prefix'=>'admin','namespace'=>'Admin','as'=>'admin.','middleware'=>['revalidate','auth:admin']], function () {
    Route::get('dashboard','PageController@dashboard')->name('dashboard');
    /**
     * Admin Roles Management 
     */
        Route::get('roles', 'RoleController@index')->name('roles.index');
        Route::get('role/{role}/show', 'RoleController@show')->name('roles.show');
        Route::get('role/{role}/edit', 'RoleController@edit')->name('roles.edit');
        Route::delete('role/{role}/destroy', 'RoleController@destroy')->middleware('permission:delete roles')->name('roles.destroy');
        Route::put('role/{role}/update','RoleController@update')->middleware('permission:edit roles')->name('roles.update');
        Route::get('role/create','RoleController@create')->middleware('permission:add roles')->name('roles.create');
        Route::post('role/store','RoleController@store')->middleware('permission:add roles')->name('roles.store');
        Route::delete('role/destroy/{role}','RoleController@destroy')->name('roles.destroy');
    /**
     *Admins Management  
     */    
        Route::get('admin-users/{role?}/{status?}','AdminController@index')->middleware('permission:list admin users')->name('admins.index');
        Route::get('admin-user/{admin}/edit/{role?}','AdminController@edit')->middleware('permission:edit admin users')->name('admins.edit');
        Route::get('admin/{admin}/edit/{role?}','AdminController@editprofile')->name('admins.profile-edit');
        Route::put('admin-user/{admin}/updates/{role?}','AdminController@update')->middleware('permission:edit admin users')->name('admins.update');
        Route::put('admin-user/{admin}/update/{role?}','AdminController@updateprofile')->name('admins.updateprofile');
        Route::delete('admin-user/{admin}/destroy','AdminController@destroy')->name('admins.destroy');
        Route::get('admin-user/create/{role?}','AdminController@create')->middleware('permission:add admin users')->name('admins.create');
        Route::post('admin-user/store/{role?}','AdminController@store')->middleware('permission:add admin users')->name('admins.store');
        Route::get('admin-user/{admin}/show/{role?}','AdminController@show')->middleware('permission:view admin users')->name('admins.show');
        Route::get('admin-user/{admin}/access-logs','AdminController@accessLogs')/* ->middleware('permission:list admin user access logs') */->name('admins.access_logs');
        Route::get('admin-user/{admin}/activity-logs/{role?}','AdminController@activityLogs')/* ->middleware('permission:list admin user activity logs') */->name('admins.activity_logs');
        
    /**
     * Industries
     */
        Route::get('industries', 'IndustryController@index')->middleware('permission:list industries')->name('industries.index');
        Route::post('industries/store','IndustryController@store')->middleware('permission:add industries')->name('industries.store');
        Route::put('industries/{industry}/update','IndustryController@update')->middleware('permission:edit industries')->name('industries.update');
        Route::delete('industries/{industry}/destroy','IndustryController@destroy')->middleware('permission:delete industries')->name('industries.destroy');
        Route::put('industries/toggle-active-status/{id}', 'IndustryController@toggleActiveStatus')->name('industries.changeStatus');

    /**
     * Categories
     */
       Route::get('categories', 'CategoryController@index')->middleware('permission:list categories')->name('categories.index');
       Route::get('categories/create','CategoryController@create')->middleware('permission:add categories')->name('categories.create');
       Route::post('categories/store','CategoryController@store')->middleware('permission:add categories')->name('categories.store');
       Route::get('categories/{category}/edit', 'CategoryController@edit')->middleware('permission:edit categories')->name('categories.edit');
       Route::get('categories/{category}/show', 'CategoryController@show')->middleware('permission:show categories')->name('categories.show');
       Route::put('categories/{category}/update','CategoryController@update')->middleware('permission:edit categories')->name('categories.update');
       Route::delete('categories/{category}/destroy','CategoryController@destroy')->middleware('permission:delete categories')->name('categories.destroy');
       Route::put('categories/toggle-active-status/{id}', 'CategoryController@toggleActiveStatus')->name('categories.changeStatus');

       /**
        *Sub Categories 
       */

       Route::get('sub-categories', 'SubCategoryController@index')->middleware('permission:list subcategories')->name('sub_categories.index');
       Route::get('sub-categories/create','SubCategoryController@create')->middleware('permission:add subcategories')->name('sub_categories.create');
       Route::post('sub-categories/store','SubCategoryController@store')->middleware('permission:add subcategories')->name('sub_categories.store');
       Route::get('sub-categories/{subCategory}/show', 'SubCategoryController@show')->middleware('permission:show subcategories')->name('sub_categories.show');
       Route::get('sub-categories/{subCategory}/edit', 'SubCategoryController@edit')->middleware('permission:edit subcategories')->name('sub_categories.edit');
       Route::put('sub-categories/{subCategory}/update','SubCategoryController@update')->middleware('permission:edit subcategories')->name('sub_categories.update');
       Route::delete('sub-categories/{subCategory}/destroy','SubCategoryController@destroy')->middleware('permission:delete subcategories')->name('sub_categories.destroy');
       Route::put('sub-categories/toggle-active-status/{id}', 'SubCategoryController@toggleActiveStatus')->name('sub_categories.changeStatus');

       /**
        * Skills
        */
       Route::get('skills', 'SkillController@index')->middleware('permission:list skills')->name('skills.index');
       Route::get('skills/create','SkillController@create')->middleware('permission:add skills')->name('skills.create');
       Route::post('skills/store','SkillController@store')->middleware('permission:add skills')->name('skills.store');
       Route::get('skills/{skill}/show', 'SkillController@show')->middleware('permission:show skills')->name('skills.show');
       Route::get('skills/{skill}/edit', 'SkillController@edit')->middleware('permission:edit skills')->name('skills.edit');
       Route::put('skills/{skill}/update','SkillController@update')->middleware('permission:edit skills')->name('skills.update');
       Route::delete('skills/{skill}/destroy','SkillController@destroy')->middleware('permission:delete skills')->name('skills.destroy');
       Route::put('skills/toggle-active-status/{id}', 'SkillController@toggleActiveStatus')->name('skills.changeStatus');
      
       /**
        * Membership plans
        */
        Route::get('membership-plans', 'MembershipPlanController@index')->middleware('permission:list membership plan')->name('membership_plans.index');
        Route::get('membership-plans/create','MembershipPlanController@create')->middleware('permission:add membership plan')->name('membership_plans.create');
        Route::post('membership-plans/store','MembershipPlanController@store')->middleware('permission:add membership plan')->name('membership_plans.store');
        Route::get('membership-plans/{membershipPlan}/edit', 'MembershipPlanController@edit')->middleware('permission:edit membership plan')->name('membership_plans.edit');
        Route::put('membership-plans/{membershipPlan}/update','MembershipPlanController@update')->middleware('permission:edit membership plan')->name('membership_plans.update');
        Route::get('membership-plans/{membershipPlan}/show', 'MembershipPlanController@show')->middleware('permission:view membership plan')->name('membership_plans.show');
        Route::delete('membership-plans/{membershipPlan}/destroy','MembershipPlanController@destroy')->middleware('permission:delete membership plan')->name('membership_plans.destroy');
        Route::put('membership-plans/toggle-active-status/{id}', 'MembershipPlanController@toggleActiveStatus')->name('membership_plans.changeStatus');


        /**
         * Dispute
         */

        Route::get('disputes', 'DisputeController@index')->name('disputes.index');
        Route::get('disputes/{dispute}/edit', 'DisputeController@edit')->name('disputes.edit');
        Route::put('dispute/{dispute}/update', 'DisputeController@update')->name('disputes.update');
        Route::get('disputes/{dispute}/show', 'DisputeController@show')->name('disputes.show');
        Route::get('disputes/{dispute}/{project}/message', 'DisputeController@message')->name('disputes.message');
        Route::post('disputes/message', 'DisputeController@store_message')->name('disputes.store_message');
        Route::get('dispute-export', 'DisputeController@ExportDispute')->name('disputes.export_dispute');
        Route::get('wallets', 'MembershipPlanController@wallets')->name('membership_plans.wallets');
        Route::post('wallets/pay-to-freelancer', 'MembershipPlanController@payToFreelancer')->name('membership_plans.pay_to_freelancer');
    /**
    *  Transactions
    */
        Route::get('transactions', 'TransactionController@index')->middleware('permission:list transactions')->name('transactions.index');
        Route::get('transactions/{transaction}/show', 'TransactionController@show')->middleware('permission:view transactions')->name('transactions.show');
        Route::put('transactions/{transaction}/update','TransactionController@update')->middleware('permission:edit transactions')->name('transactions.update');
        
    /**
        *  Project Payment - Invoice
        */
        Route::get('invoices', 'ProjectPaymentController@invoiceIndex')->name('invoices.index');
        Route::get('project-payments', 'ProjectPaymentController@index')->name('project_payments.index');
        Route::get('project-payments/{invoice}/show', 'ProjectPaymentController@show')->name('project_payments.show');
        Route::get('invoices/{invoice}/show', 'ProjectPaymentController@showInvoice')->name('invoices.show');
        Route::get('invoices/create','ProjectPaymentController@create')->name('invoices.create');
        Route::get('/invoices/select_milestones','ProjectPaymentController@select_milestones')->name('invoices.select_milestones');
        Route::get('/invoices/select_project','ProjectPaymentController@select_project')->name('invoices.select_project');
        Route::post('invoices/store','ProjectPaymentController@store')->name('invoices.store');
        Route::post('project-payments/exportlist','ProjectPaymentController@exportlist')->name('project_payment.exportlist');

    /**
     * For changing password of admin user
     */ 
        Route::get('{admin}/change-password','AdminController@showChangePassword')->name('admins.show_change_password');  
        Route::post('change-password', 'AdminController@changePassword')->name('admins.change_password');

    /**
     * User Subscriptions
     */
       Route::get('subscription-payments', 'UserSubscriptionController@index')->middleware('permission:list subscription payments')->name('user_subscriptions.index');
       Route::get('subscription-payments/{invoice}/show', 'UserSubscriptionController@show')->middleware('permission:view subscription payments')->name('user_subscriptions.show');
       Route::get('subscription-payments-export', 'UserSubscriptionController@ExportSubscription')->name('user_subscriptions.export_subscription');
       Route::get('invoice', 'UserSubscriptionController@invoice')->name('user_subscriptions.invoice');
       Route::post('invoice', 'UserSubscriptionController@StoreInvoice')->name('user_subscriptions.StoreInvoice');
        
    /**
     *  CMS page Management
     */    
        Route::get('cms-pages', 'PageController@index')->middleware('permission:list pages')->name('pages.index');
        Route::get('cms-page/{page}/show','PageController@show')->middleware('permission:view pages')->name('pages.show');
        Route::get('cms-page/{page}/edit','PageController@edit')->middleware('permission:edit pages')->name('pages.edit');
        Route::delete('cms-page/{page}/destroy','PageController@destroy')->middleware('permission:delete pages')->name('pages.destroy');
        Route::put('cms-page/{page}/update','PageController@update')->middleware('permission:edit pages')->name('pages.update');
        Route::get('cms-page/create','PageController@create')->middleware('permission:add pages')->name('pages.create');
        Route::post('cms-page/store','PageController@store')->middleware('permission:add pages')->name('pages.store');
        Route::delete('cms-page/{page}/destroy', 'PageController@destroy')->middleware('permission:delete pages')->name('pages.destroy');

    /**
    *  Terms and Conditions  
    */
       Route::get('terms-and-conditions', 'TermsAndConditionController@index')->middleware('permission:list terms and conditions')->name('terms_and_conditions.index');
       Route::get('terms-and-conditions/create','TermsAndConditionController@create')->middleware('permission:add terms and conditions')->name('terms_and_conditions.create');
       Route::post('terms-and-conditions/store','TermsAndConditionController@store')->middleware('permission:add terms and conditions')->name('terms_and_conditions.store');
       Route::get('terms-and-conditions/{termsAndCondition}/edit','TermsAndConditionController@edit')->middleware('permission:edit terms and conditions')->name('terms_and_conditions.edit');
       Route::put('terms-and-conditions/{termsAndCondition}/update','TermsAndConditionController@update')->middleware('permission:edit terms and conditions')->name('terms_and_conditions.update');
       Route::get('terms-and-conditions/{termsAndCondition}/show','TermsAndConditionController@show')->middleware('permission:view terms and conditions')->name('terms_and_conditions.show');
       Route::delete('terms-and-conditions/{page}/destroy', 'TermsAndConditionController@destroy')->middleware('permission:delete terms and conditions')->name('terms_and_conditions.destroy');
    /**
     * Frequently Asked Questions Management 
     */
        Route::get('frequently-asked-questions/{type?}/{status?}', 'FrequentlyAskedQuestionController@index')->name('frequently_asked_questions.index');
        Route::get('frequently-asked-question/{frequentlyAskedQuestion}/show', 'FrequentlyAskedQuestionController@show')->name('frequently_asked_questions.show');
        Route::get('frequently-asked-questions/{frequentlyAskedQuestion}/edit/{type?}', 'FrequentlyAskedQuestionController@edit')->name('frequently_asked_questions.edit');
        Route::delete('frequently-asked-questions/{frequentlyAskedQuestion}/destroy/{type?}', 'FrequentlyAskedQuestionController@destroy')->middleware('permission:delete frequently asked questions')->name('frequently_asked_questions.destroy');
        Route::put('frequently-asked-question/{frequentlyAskedQuestion}/update/{type?}','FrequentlyAskedQuestionController@update')->middleware('permission:edit frequently asked questions')->name('frequently_asked_questions.update');
        Route::get('frequently-asked-question/create/{type?}','FrequentlyAskedQuestionController@create')->middleware('permission:add frequently asked questions')->name('frequently_asked_questions.create');
        Route::post('frequently-asked-question/store/{type?}','FrequentlyAskedQuestionController@store')->middleware('permission:add frequently asked questions')->name('frequently_asked_questions.store');

    /**
     * Email Contents Management 
     */
        Route::get('email-templates', 'EmailTemplateController@index')->middleware('permission:list email templates')->name('email_templates.index');
        Route::get('email-template/{emailTemplate}/show', 'EmailTemplateController@show')->middleware('permission:view email templates')->name('email_templates.show');
        Route::get('email-template/{emailTemplate}/edit', 'EmailTemplateController@edit')->middleware('permission:edit email templates')->name('email_templates.edit');
        Route::delete('email-template/{emailTemplate}/destroy', 'EmailTemplateController@destroy')->middleware('permission:delete email templates')->name('email_templates.destroy');
        Route::put('email-template/{emailTemplate}/update','EmailTemplateController@update')->middleware('permission:edit email templates')->name('email_templates.update');
        Route::get('email-template/create','EmailTemplateController@create')->middleware('permission:add email templates')->name('email_templates.create');
        Route::post('email-template/store','EmailTemplateController@store')->middleware('permission:add email templates')->name('email_templates.store');

    /**
     * Settings Management 
     */
        Route::get('settings/{type?}', 'SettingController@index')->middleware('permission:list settings')->name('settings.index');
        Route::put('setting/update/{type?}','SettingController@update')->middleware('permission:edit settings')->name('settings.update');
     
    /**
     * Settings Management 
     */
        Route::get('contact-us/', 'ContactUsController@index')->name('contact_us.index');
        Route::put('contact-us/{contact_us}/update','ContactUsController@update')->name('contact_us.update');
    /**
     * Security Questions
     */

        Route::get('security-questions', 'SecurityQuestionController@index')->middleware('permission:list security questions')->name('security_questions.index');
        Route::post('security-questions/store','SecurityQuestionController@store')->middleware('permission:add security questions')->name('security_questions.store');
        Route::put('security-questions/{securityQuestion}/update','SecurityQuestionController@update')->middleware('permission:edit security questions')->name('security_questions.update');
        Route::delete('security-questions/{securityQuestion}/destroy', 'SecurityQuestionController@destroy')->middleware('permission:delete security questions')->name('security_questions.destroy');

    /**
     * Rating Questions
     */

        Route::get('rating-questions', 'RatingQuestionController@index')->middleware('permission:list rating questions')->name('rating_questions.index');
        Route::post('rating-questions/store','RatingQuestionController@store')->middleware('permission:add rating questions')->name('rating_questions.store');
        Route::put('rating-questions/{ratingQuestion}/update','RatingQuestionController@update')->middleware('permission:edit rating questions')->name('rating_questions.update');
        Route::delete('rating-questions/{ratingQuestion}/destroy', 'RatingQuestionController@destroy')->middleware('permission:delete rating questions')->name('rating_questions.destroy');

    /** 
     * SMS Templates Management
     */
        Route::get('sms-templates', 'SmsTemplateController@index')->middleware('permission:list sms templates')->name('sms_templates.index');
        Route::get('sms-template/{smsTemplate}/show', 'SmsTemplateController@show')->middleware('permission:view sms templates')->name('sms_templates.show');
        Route::get('sms-template/{smsTemplate}/edit', 'SmsTemplateController@edit')->middleware('permission:edit sms templates')->name('sms_templates.edit');
        Route::delete('sms-template/{smsTemplate}/destroy', 'SmsTemplateController@destroy')->middleware('permission:delete sms templates')->name('sms_templates.destroy');
        Route::put('sms-template/{smsTemplate}/update','SmsTemplateController@update')->middleware('permission:edit sms templates')->name('sms_templates.update');
        Route::get('sms-template/create','SmsTemplateController@create')->middleware('permission:add sms templates')->name('sms_templates.create');
        Route::post('sms-template/store','SmsTemplateController@store')->middleware('permission:add sms templates')->name('sms_templates.store');
    
    /**
     * pusher web app notifications
     * 
     * 
     */
        Route::post('notification', 'PusherNotificationController@sendNotification')->name('notificationshow');

    /**
     * Push Notification Template Management
     */
        Route::get('push-notification-templates', 'PushNotificationTemplateController@index')->middleware('permission:list push notification templates')->name('push_notification_templates.index');
        Route::get('push-notification-template/{pushNotificationTemplate}/show', 'PushNotificationTemplateController@show')->middleware('permission:view push notification templates')->name('push_notification_templates.show');
        Route::get('push-notification-template/{pushNotificationTemplate}/edit', 'PushNotificationTemplateController@edit')->middleware('permission:edit push notification templates')->name('push_notification_templates.edit');
        Route::delete('push-notification-template/{pushNotificationTemplate}/destroy', 'PushNotificationTemplateController@destroy')->middleware('permission:delete push notification templates')->name('push_notification_templates.destroy');
        Route::put('push-notification-template/{pushNotificationTemplate}/update','PushNotificationTemplateController@update')->middleware('permission:edit push notification templates')->name('push_notification_templates.update');
        Route::get('push-notification-template/create','PushNotificationTemplateController@create')->middleware('permission:add push notification templates')->name('push_notification_templates.create');
        Route::post('push-notification-template/store','PushNotificationTemplateController@store')->middleware('permission:add push notification templates')->name('push_notification_templates.store');

    /**
     * In-App Notification Template Management
     */
        Route::get('in-app-notification-templates', 'InAppNotificationTemplateController@index')/* ->middleware('permission:list in app notification templates') */->name('in_app_notification_templates.index');
        Route::get('in-app-notification-template/{inAppNotificationTemplate}/show', 'InAppNotificationTemplateController@show')/* ->middleware('permission:view in app notification templates') */->name('in_app_notification_templates.show');
        Route::get('in-app-notification-template/{inAppNotificationTemplate}/edit', 'InAppNotificationTemplateController@edit')/* ->middleware('permission:edit in app notification templates') */->name('in_app_notification_templates.edit');
        Route::delete('in-app-notification-template/{inAppNotificationTemplate}/destroy', 'InAppNotificationTemplateController@destroy')/* ->middleware('permission:delete in app notification templates') */->name('in_app_notification_templates.destroy');
        Route::put('in-app-notification-template/{inAppNotificationTemplate}/update','InAppNotificationTemplateController@update')/* ->middleware('permission:edit in app notification templates') */->name('in_app_notification_templates.update');
        Route::get('in-app-notification-template/create','InAppNotificationTemplateController@create')/* ->middleware('permission:add in app notification templates') */->name('in_app_notification_templates.create');
        Route::post('in-app-notification-template/store','InAppNotificationTemplateController@store')/* ->middleware('permission:add in app notification templates') */->name('in_app_notification_templates.store');

    /**
     * Email Queues Management
     */
        Route::get('email-queues', 'EmailQueueController@index')/* ->middleware('permission:list email queus') */->name('email_queues.index');
        Route::get('email-queue/{emailQueue}/show', 'EmailQueueController@show')/* ->middleware('permission:view in app notification templates') */->name('email_queues.show');
        Route::get('email-queue/{emailQueue}/resend', 'EmailQueueController@resend')->name('email_queues.resend');
     

    /**
     * Client Management
     */
        Route::get('clients/{user}/show', 'UserController@show')->middleware('permission:view clients')->name('users.show');
        Route::get('clients/{user}/membership', 'UserController@membership')->middleware('permission:view clients')->name('users.membership');
        Route::get('clients/{user}/transactions', 'UserController@transaction')->middleware('permission:view clients')->name('users.transaction');
        Route::get('clients/{user}/projects', 'UserController@project')->middleware('permission:view clients')->name('users.project');
        Route::get('clients/{user}/reviews-ratings', 'UserController@reviewsRatings')->middleware('permission:view clients')->name('users.reviewsRatings');
        Route::get('clients/{user}/{project}/reviews-ratings-details', 'UserController@ratingDetails')->middleware('permission:view clients')->name('users.ratingDetails');
        Route::get('clients/{user}/edit', 'UserController@edit')->middleware('permission:edit clients')->name('users.edit');
        Route::delete('clients/{user}/destroy', 'UserController@destroy')->middleware('permission:delete clients')->name('users.destroy');
        Route::put('clients/{user}/update','UserController@update')->middleware('permission:edit clients')->name('users.update');
        Route::get('clients/create','UserController@create')->middleware('permission:add clients')->name('users.create');
        Route::post('clients/store','UserController@store')->middleware('permission:add clients')->name('users.store');
        Route::get('clients/{user}/access-logs','UserController@accessLogs')/* ->middleware('permission:list admin user access logs') */->name('users.access_logs');
        Route::get('clients/{user}/activity-logs','UserController@activityLogs')/* ->middleware('permission:list admin user activity logs') */->name('users.activity_logs');
        Route::post('clients/list','UserController@userslists')->name('users.list');
        Route::post('clients/export-list','UserController@exportuserslists')->name('users.exportlist');
        Route::get('clients/not-approved', 'UserController@notApproved')->middleware('permission:list clients')->name('users.not_approved');
        Route::get('clients/{status?}', 'UserController@index')->middleware('permission:list clients')->name('users.index');
        Route::post('clients/{user}/transactions/list', 'UserController@transactionList')->name('users.transactionList');
        Route::post('clients/{user}/projects/list', 'UserController@projectList')->name('users.projectList');
        Route::post('clients/{user}/ratings/list', 'UserController@clientRatings')->name('users.clientRatings');

    /**
     * Freelancer Management
     */
        Route::get('freelancers/{user}/show', 'FreelancerController@show')->middleware('permission:view freelancers')->name('freelancers.show');
        Route::get('freelancers/{user}/membership', 'FreelancerController@membership')->middleware('permission:view freelancers')->name('freelancers.membership');
        Route::get('freelancers/{user}/transactions', 'FreelancerController@transaction')->middleware('permission:view freelancers')->name('freelancers.transaction');
        Route::get('freelancers/{user}/projects', 'FreelancerController@project')->middleware('permission:view freelancers')->name('freelancers.project');
        Route::get('freelancers/{user}/reviews-ratings', 'FreelancerController@reviewsRatings')->middleware('permission:view freelancers')->name('freelancers.reviewsRatings');
        Route::get('freelancers/{user}/{project}/reviews-ratings-details', 'FreelancerController@ratingDetails')->middleware('permission:view freelancers')->name('freelancers.ratingDetails');
        Route::get('freelancers/{user}/edit', 'FreelancerController@edit')->middleware('permission:edit freelancers')->name('freelancers.edit');
        Route::delete('freelancers/{user}/destroy', 'FreelancerController@destroy')->middleware('permission:delete freelancers')->name('freelancers.destroy');
        Route::put('freelancers/{user}/update','FreelancerController@update')->middleware('permission:edit freelancers')->name('freelancers.update');
        Route::get('freelancers/create','FreelancerController@create')->middleware('permission:add freelancers')->name('freelancers.create');
        Route::post('freelancers/store','FreelancerController@store')->middleware('permission:add freelancers')->name('freelancers.store');
        Route::get('freelancers/{user}/access-logs','FreelancerController@accessLogs')/* ->middleware('permission:list admin user access logs') */->name('freelancers.access_logs');
        Route::get('freelancers/{user}/activity-logs','FreelancerController@activityLogs')/* ->middleware('permission:list admin user activity logs') */->name('freelancers.activity_logs');
        Route::post('freelancers/list','FreelancerController@userslists')->name('freelancers.list');
        Route::post('freelancers/export-list','FreelancerController@exportuserslists')->name('freelancers.exportlist');
        Route::get('freelancers/not-approved', 'FreelancerController@notApproved')->middleware('permission:list freelancers')->name('freelancers.not_approved');
        Route::get('freelancers/{status?}', 'FreelancerController@index')->middleware('permission:list freelancers')->name('freelancers.index');
        Route::post('freelancers/{user}/transactions/list', 'FreelancerController@transactionList')->name('freelancers.transactionList');
        Route::post('freelancers/{user}/projects/list', 'FreelancerController@projectList')->name('freelancers.projectList');
        Route::post('freelancers/{user}/ratings/list', 'FreelancerController@FreelancerRatings')->name('users.FreelancerRatings');

    /**
     * Sms Queues Management
     */
        Route::get('sms-queues', 'SmsQueueController@index')/* ->middleware('permission:list email queus') */->name('sms_queues.index');
        Route::get('sms-queue/{smsQueue}/show', 'SmsQueueController@show')/* ->middleware('permission:view in app notification templates') */->name('sms_queues.show');
        Route::get('sms-queue/{smsQueue}/resend', 'SmsQueueController@resend')->name('sms_queues.resend');

    /**
     * Push Notification Queues Management
     */
        Route::get('push-notification-queues', 'PushNotificationQueueController@index')/* ->middleware('permission:list email queus') */->name('push_notification_queues.index');
        Route::get('push-notification-queue/{pushNotificationQueue}/show', 'PushNotificationQueueController@show')/* ->middleware('permission:view in app notification templates') */->name('push_notification_queues.show');
        Route::get('push-notification-queue/{pushNotificationQueue}/resend', 'PushNotificationQueueController@resend')->name('push_queues.resend');
    
    /** 
     * Email Templates Management
     */
        Route::get('email-templates', 'EmailTemplateController@index')->middleware('permission:list email templates')->name('email_templates.index');
        Route::get('email-template/{emailTemplates}/show', 'EmailTemplateController@show')->middleware('permission:view email templates')->name('email_templates.show');
        Route::get('email-template/{emailTemplates}/edit', 'EmailTemplateController@edit')->middleware('permission:edit email templates')->name('email_templates.edit');
        Route::delete('email-template/{emailTemplates}/destroy', 'EmailTemplateController@destroy')->middleware('permission:delete email templates')->name('email_templates.destroy');
        Route::put('email-template/{emailTemplates}/update','EmailTemplateController@update')->middleware('permission:edit email templates')->name('email_templates.update');
        Route::get('email-template/create','EmailTemplateController@create')->middleware('permission:add email templates')->name('email_templates.create');
        Route::post('email-template/store','EmailTemplateController@store')->middleware('permission:add email templates')->name('email_templates.store');
    /**
     * Projects
     */
        Route::group(['prefix' => 'projects','as'=>'projects.'], function () {
            Route::get('hide', 'ProjectController@hidePublic')->name('hidePublic');

            Route::get('/open-projects', 'ProjectController@index')->middleware('permission:list projects')->name('index');
            Route::get('/inprogress-projects', 'ProjectController@inprogressList')->middleware('permission:list projects')->name('list_inprogress');
            Route::get('/completed-projects', 'ProjectController@completedList')->middleware('permission:list projects')->name('list_completed');
            Route::get('/create', 'ProjectController@create')->middleware('permission:add projects')->name('create');
            Route::post('/store', 'ProjectController@store')->middleware('permission:add projects')->name('store');
            Route::get('{project}/edit', 'ProjectController@edit')->middleware('permission:edit projects')->name('edit');
            Route::get('{project}/show', 'ProjectController@show')->middleware('permission:show projects')->name('show');
            Route::get('{project}/proposals', 'ProjectController@proposals')->middleware('permission:show projects')->name('proposals');

            Route::get('{project}/transactions', 'ProjectController@transactions')->middleware('permission:show projects')->name('transactions');
            Route::get('{project}/chats', 'ProjectController@chats')->middleware('permission:show projects')->name('chats');

            Route::get('{project}/proposals/{proposal}/show', 'ProjectController@proposalShow')->middleware('permission:show projects')->name('proposal_show');
            Route::put('{project}/update', 'ProjectController@update')->middleware('permission:edit projects')->name('update');
            Route::put('{project}/update-status', 'ProjectController@updateStatus')->middleware('permission:edit projects')->name('updateStatus');
            Route::delete('{project}/destroy', 'ProjectController@destroy')->middleware('permission:delete projects')->name('destroy');
            Route::post('/file-upload', 'ProjectController@fileUpload')->middleware('permission:add projects')->name('fileUpload');
            Route::get('/remove-file', 'ProjectController@removeFile')->middleware('permission:edit projects')->name('removeFile');

        });
        

    Route::get('toggle-leftbar', 'PageController@toggleLeftbar');
    Route::get('toggle-active-status/{uuid}', 'AdminController@toggleActiveStatus');
    Route::get('user/toggle-active-status/{id}', 'UserController@toggleActiveStatus');
    Route::get('project/toggle-active-status/{id}', 'UserController@toggleActiveStatuscomplete');
    Route::get('project-close/toggle-active-status/{id}', 'UserController@toggleActiveStatusclose');
    Route::get('user/approve', 'UserController@approveUser');



    // socialite login
    Route::get('google', function () {
        return view('googleAuth');
    });
});
Route::get('/get-categories','Admin\IndustryController@getCategories');
Route::get('/get-sub-categories','Admin\IndustryController@getSubCategories');
Route::get('/get-skills','Admin\IndustryController@getSkills');
