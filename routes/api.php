<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['api'],
    // 'prefix' => 'auth',
    'namespace'=>'Api'

], function ($router) {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('sendPasswordResetLink', 'ForgotPasswordController@sendResetLinkEmail');
    Route::post('resetPassword', 'ResetPasswordController@reset');   
    Route::post('user/show', 'UserController@show'); 
    Route::post('change-password', 'UserController@changePassword'); 
    Route::post('user/changePassword', 'UserController@changePassword');  
    Route::post('pages/show', 'PageController@show');
    Route::put('user/update', 'UserController@update');
});
