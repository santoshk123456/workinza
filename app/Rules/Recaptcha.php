<?php

namespace App\Rules;
use App\Setting;
use GuzzleHttp\Client;
use Illuminate\Contracts\Validation\Rule;

class Recaptcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //get captcha secrte key
        $secret_key = Setting::select('value')->where('key', 'captcha_secret_key')->first()['value'];
        $client = new Client;
        $response = $client->post(
            'https://www.google.com/recaptcha/api/siteverify',
            [
                'form_params' =>
                    [
                        'secret' => $secret_key,
                        'response' => $value
                    ]
            ]
        );
        $body = json_decode((string)$response->getBody());
       
        return $body->success;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The captcha invalid.';
    }
}
