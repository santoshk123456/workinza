<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Setting extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','key','value'
    ];
    
    public static function getSettingValue($name){
        $value = Setting::select('value')->where('key',$name)->first()->value;
        return $value;
    }

}