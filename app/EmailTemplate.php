<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class EmailTemplate extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'static_email_heading','subject','description','template',
    ];

    

}