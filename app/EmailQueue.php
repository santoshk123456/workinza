<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class EmailQueue extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','body','sent_status','attachments','to_email','to_name','from_email','from_name','subject','time_to_send','action_type','action_id'
    ];

}