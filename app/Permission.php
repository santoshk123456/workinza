<?php

namespace App;

use App\Traits\Attributes\PermissionAttributes;
use Spatie\Activitylog\Models\Activity;

/**
 * Class Permission.
 */
class Permission extends \Spatie\Permission\Models\Permission
{
    use PermissionAttributes;
}
