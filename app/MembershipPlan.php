<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\General\Uuid;

class MembershipPlan extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid','plan_id', 'user_type', 'name', 'plan_type','monthly_amount', 'yearly_amount', 'setup_charge', 'number_of_projects', 'number_of_proposals', 'number_of_products', 'service_percentage', 'chat_feature', 'active','product_commission','product_id'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
