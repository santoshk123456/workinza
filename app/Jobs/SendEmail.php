<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\GenerateEmail;


class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $email;
    
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::to($this->email->to_email)->send(new GenerateEmail($this->email));
            $this->email->update([
                'sent_status' => 1,
                'time_to_send' => date('Y-m-d H:i:s')
            ]);
        } catch (Exception $e) {
            if(count(Mail::failures()) == 0){
                $this->email->update([
                    'sent_status' => 1,
                    'time_to_send' => date('Y-m-d H:i:s')
                ]);
            }
        }
    }
}
