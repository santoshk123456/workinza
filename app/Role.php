<?php

namespace App;

use App\Traits\Attributes\RoleAttributes;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * Class Role.
 */
class Role extends \Spatie\Permission\Models\Role
{
    use RoleAttributes,
        LogsActivity;

    /**
     *  Activity Log
     */
            
    protected static $logAttributes = ['name'];
    protected static $logName = 'admin-role';
    protected static $logOnlyDirty = true;

    /**
     * Set Activit Description
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return 'Role "' . $this->name . '" was created';
        }
        if ($eventName == 'updated') {
            return 'Role "' . $this->name . '" was updated';
        }
        if ($eventName == 'deleted') {
            return 'Role "' . $this->name . '" was deleted';
        }
        return $eventName;
    }
}
