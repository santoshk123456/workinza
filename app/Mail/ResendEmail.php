<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_template_data)
    {
        $this->email_template_data = $email_template_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('resend_email')
                    ->from($this->email_template_data['from_email'], $this->email_template_data['from_name'])
                    ->subject($this->email_template_data['subject'])
                    ->with('body',$this->email_template_data['body']);
    }
}
