<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminCreation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_template_data)
    {
        $this->email_template_data = $email_template_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('admin_creation')
                    ->from($this->email_template_data['from_email'], $this->email_template_data['from_name'])
                    ->subject('Admin Creation')
                    ->with('admin',$this->email_template_data['to_name'])
                    ->with('email',$this->email_template_data['to_email'])
                    ->with('password',$this->email_template_data['password'])
                    ->with('login_link',route('admin.login.form'));
    }
}
