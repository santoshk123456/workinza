<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRating extends Model
{
    //
    protected $fillable = [
        'rated_by_user_id', 'project_id', 'rating_question_id', 'rating_to_user_id','rating',
    ];

    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function rated_by(){
        return $this->belongsTo(User::class,'rated_by_user_id','id');
    }

    public function rated_to(){
        return $this->belongsTo(User::class,'rating_to_user_id','id');
    }

    public function ratingQuestion(){
        return $this->belongsTo(RatingQuestion::class,'rating_question_id','id');
    }
}
