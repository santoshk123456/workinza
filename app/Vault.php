<?php

namespace App;

use App\User;
use App\Transaction;
use App\ProjectMilestone;
use Illuminate\Database\Eloquent\Model;

class Vault extends Model
{
    protected $fillable = ['user_id', 'user_type','project_milestone_id','transaction_id','notes','transaction_type','amount','vault_status'];

    public function milestones(){
        return $this->belongsTo(ProjectMilestone::class,'project_milestone_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function transactions(){
        return $this->belongsTo(Transaction::class,'transaction_id');
    }
}


