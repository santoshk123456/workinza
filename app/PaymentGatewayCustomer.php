<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentGatewayCustomer extends Model
{

    protected $fillable = ['payment_gateway_id','user_id', 'customer_id','card_number','card_type','is_default'];
}
