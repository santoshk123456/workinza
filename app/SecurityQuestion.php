<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{

    protected $fillable = [
        'user_type', 'question'
    ];

    

}
