<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\General\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Industry extends Model
{
    use Uuid,SoftDeletes;

    protected $fillable = [
        'uuid', 'name', 'active'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * The categories that belong to the Industry.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Categories')->withTimestamps();
    }


    public function subcategories()
    {
        return $this->belongsToMany('App\SubCategory')->withTimestamps();
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill')->withTimestamps();
    }

    /**
     * The users that belong to the Industry.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }
}
