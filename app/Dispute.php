<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\General\Uuid;

class Dispute extends Model
{
    use Uuid;
    protected $fillable = ['client_user_id','freelancer_user_id','project_contract_id','project_milestone_id','subject','description','amount','status','raised_by'];


    public function client()
    {
        return $this->belongsTo(User::class,'client_user_id');
    }

    public function freelancer()
    {
        return $this->belongsTo(User::class,'freelancer_user_id');
    }
    public function milestone()
    {
        return $this->belongsTo(ProjectMilestone::class,'project_milestone_id');
    }
    public function raisedBy()
    {
        return $this->belongsTo(User::class,'raised_by');
    }
}
