<?php

namespace App;

use App\User;
use App\Setting;
use App\EmailTemplate;
use App\Services\ProcessEmailQueue;
use Illuminate\Database\Eloquent\Model;

class EmailOTP extends Model
{
    protected $table = 'email_otp';
    protected $fillable = ['email','otp','inactive_time'];
    
    // Function for send OTP to email 
    public static function sendOTP($email){
        $user = User::where('email',$email)->first();
        if(!empty($user)){

            $otp_time = Setting::select('value')->where('key','otp_expire_time')->first()['value'];
            $strArr = explode(":", $otp_time);
            if(count($strArr) == 2){
                $hours_add =  $strArr[0];
                $minutes_add = $strArr[1];
            }
            $expiry_time = date('Y-m-d H:i:s',strtotime('+'.$hours_add.' hour +'.$minutes_add.' minutes'));

            $otp = rand(100000,999999);
            $email_otp = EmailOTP::create([
                'email' => $email,
                'otp' => $otp,
                'inactive_time' => $expiry_time
            ]);
            // Send Email and save to email queue
            $user_creation_template = EmailTemplate::where('static_email_heading','EMAIL_OTP')->first();
            if ($user_creation_template) {
                $email_html = $user_creation_template->template;
                $email_html = str_replace("[USER_NAME]", $user->full_name, $email_html);
                $email_html = str_replace("[LOGIN_OTP]", $otp, $email_html);
                $email_html = str_replace("[OTP_HOUR]", $hours_add, $email_html);
                $email_html = str_replace("[OTP_MINUTE]", $minutes_add, $email_html);

                $template = view('admin.email_templates.layout', compact('email_html'));
                $data = [];
                $data['to_email'] = $user->email;
                $data['to_name'] = $user->full_name;
                $data['subject'] = $user_creation_template->subject;
                $data['body'] = $template;
                $data['action_id'] = $user->id;
                $data['action_type'] = 'email_otp';
                $processEmailQueue = new ProcessEmailQueue($data);
                $processEmailQueue->sendEmail();
                return true;
            }
        }
    }

    // Function for verify otp
    public static function otpValidate($otp,$email){
        $send_otp = EmailOTP::where('email',$email)->orderBy('id','desc')->first();
        if(!empty($send_otp)){
            if($send_otp->otp == $otp){
                $send_otp->delete();
                return true;
            }else{
                return false;
            }
        }
        
    }
}
