<?php

namespace App;

use App\User;
use App\Country;
use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $guarded = [
        'id'
    ];

    /**
     * Releationships
     */
    
    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function referedBy(){
        return $this->belongsTo(User::class,'referral_user_id','id')->withTrashed();
    }
}
