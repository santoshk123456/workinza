<?php

namespace App\Observers;

use App\Admin;
use App\Mail\AdminCreation;
use Illuminate\Support\Facades\Hash;
use App\Setting;
use App\EmailQueue;
use App\Jobs\SendEmail;
use Carbon\Carbon;
use Twilio;
use App\Services\ProcessEmailQueue;
use App\Services\ProcessSmsQueue;
use App\EmailTemplate;
use App\SmsTemplate;




class AdminObserver
{
    /**
     * Event After Admin Creation
     *
     * @param Admin $admin
     * @return void
     */
    public function created(Admin $admin)
    {
        $password = str_random(8);
        $admin->password = Hash::make($password);
        $admin->update();
        
        // Send Email and save to email queue
        $admin_creation_template = EmailTemplate::where('static_email_heading','ADMIN_CREATION')->first();
        if ($admin_creation_template) {
            $email_html = $admin_creation_template->template;
            $email_html = str_replace("[ADMIN_NAME]", $admin->name, $email_html);
            $email_html = str_replace("[ADMIN_LOGIN_LINK]", route('admin.login.form'), $email_html);
            $email_html = str_replace("[ADMIN_EMAIL]", $admin->email, $email_html);
            $email_html = str_replace("[ADMIN_PASSWORD]", $password, $email_html);
            $template = view('admin.email_templates.layout', compact('email_html'));
            $data = [];
            $data['to_email'] = $admin->email;
            $data['to_name'] = $admin->name;
            $data['subject'] = $admin_creation_template->subject;
            $data['body'] = $template;
            $data['action_id'] = $admin->id;
            $data['action_type'] = 'admin_creation';
            $processEmailQueue=new ProcessEmailQueue($data);
            $processEmailQueue->sendEmail();
        }

        // Send SMS and save to SMS queue
        $admin_creation_sms_template = SmsTemplate::where('process_name','ADMIN_CREATION')->first();
        if ($admin_creation_sms_template) {
            $sms_text = $admin_creation_sms_template->template;
            $sms_text = str_replace("[ADMIN_NAME]", $admin->name, $sms_text);
            $data = [];
            $data['process_name'] = 'ADMIN_CREATION';
            $data['message'] = $sms_text;
            $data['action_id'] = $admin->id;
            $data['action_type'] = 'admin_creation';
            $data['mobile_number'] = $admin->mobile_number;
            $processSmsQueue=new ProcessSmsQueue($data);
            //$processSmsQueue->sendSms();
        }
    }
    /**
     * Event After Admin Updation
     *
     * @param Admin $admin
     * @return void
     */
    public function updated(Admin $admin)
    {
        // if ($admin->isDirty('profile_image')) {
            
        // }
    }
}
