<?php

namespace App;

use App\Skill;
use App\Category;
use App\Industry;
use App\SubCategory;
use App\ProjectDocument;
use App\ProjectProposal;
use App\ProjectMilestone;
use App\Models\Traits\General\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use Uuid,SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function industries()
    {
        return $this->belongsToMany(Industry::class,'project_industry')->withPivot('project_id','industry_id')->withTimestamps();
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class,'project_category')->withPivot('project_id','category_id')->withTimestamps();
    }
    public function subCategories()
    {
        return $this->belongsToMany(SubCategory::class,'project_sub_category')->withPivot('project_id','sub_category_id')->withTimestamps();
    }
    public function skills()
    {
        return $this->belongsToMany(Skill::class,'project_skill')->withPivot('project_id','skill_id')->withTimestamps();
    }
    public function milestones(){
        return $this->hasMany(ProjectMilestone::class);
    }
    public function documents(){
        return $this->hasMany(ProjectDocument::class);
    }
    public function proposals(){
        return $this->hasMany(ProjectProposal::class);
    }
    public function contracts(){
        return $this->hasOne(ProjectContract::class);
    }
    public function projectInvitation(){
        return $this->hasMany(ProjectInvitation::class);
    }
    public function UserRating(){
        return $this->hasMany(UserRating::class);
    }
    public function UserReview(){
        return $this->hasMany(UserReview::class);
    }
}
