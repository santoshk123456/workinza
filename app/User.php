<?php

namespace App;

use View;
use App\Skill;
use App\Category;
use App\Industry;
use App\UserDetail;
use App\SubCategory;
use App\ProjectContract;
use App\UserSubscription;
use App\NotificationPreference;
use Laravel\Passport\HasApiTokens;
use App\Models\Traits\General\Uuid;
use App\Services\ProcessEmailQueue;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Yadahan\AuthenticationLog\AuthenticationLogable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, AuthenticationLogable,Uuid,HasApiTokens,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['organization_type','user_type','password','profile_image','full_name','email','username','country_code','password','user_type','phone','phone_verified_at','email_varified_at','organization_type','username','step'];
    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function sendPasswordResetNotification($token)
    {
        // Send Email and save to email queue
        $user_reset_password_template = EmailTemplate::where('static_email_heading','USER_RESET_PASSWORD')->first();
        if ($user_reset_password_template) {
            $email_html = $user_reset_password_template->template;
            $email_html = str_replace("[USER_NAME]", $this->first_name, $email_html);
            $email_html = str_replace("[RESET_TOKEN]", 'http://localhost:4200/response-password-reset?token='.$token, $email_html);
            $template = (string)View::make('admin.email_templates.layout', compact('email_html'));
            $data = [];
            $data['to_email'] = $this->email;
            $data['to_name'] = $this->first_name;
            $data['subject'] = $user_reset_password_template->subject;
            $data['body'] = $template;
            $data['action_id'] = $this->id;
            $data['action_type'] = 'user_reset_password';
            $processEmailQueue=new ProcessEmailQueue($data);
            $processEmailQueue->sendEmail();
        }
    }
    // for notification preference 
    public static function setNotificationPreference($userId = null)
    {
        if(!empty($userId)){
            $notificationPreference = new NotificationPreference();
            $notificationPreference['user_id'] = $userId;
            $notificationPreference['email_notification'] = 1;
            $notificationPreference['push_notification'] = 1;
            $notificationPreference['sms_notification'] = 1;
            $notificationPreference->save();
        }
    }

    // Attributes
    public function getFullNumberAttribute(){
        return '+'.$this->country_code.$this->phone;
    }

    // Realationships
    /**
     * The industries that belong to the users.
     */
    public function industries()
    {
        return $this->belongsToMany(Industry::class,'user_industry')->withPivot('user_id','industry_id')->withTimestamps();
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class,'user_category')->withPivot('user_id','category_id')->withTimestamps();
    }
    public function subCategories()
    {
        return $this->belongsToMany(SubCategory::class,'user_sub_category')->withPivot('user_id','sub_category_id')->withTimestamps();
    }
    public function skills()
    {
        return $this->belongsToMany(Skill::class,'user_skill')->withPivot('user_id','skill_id')->withTimestamps();
    }
    public function userDetail(){
        return $this->hasOne(UserDetail::class);
    }
    public function userSubscription(){
        return $this->hasMany(UserSubscription::class);
    }
    public function projectInvitation(){
        return $this->hasMany(ProjectInvitation::class,'freelancer_id');
    }
    
    public function portfolio(){
        return $this->hasMany(UserPortfolio::class,'user_id');
    }
  
   
}
