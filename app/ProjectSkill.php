<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectSkill extends Model
{
    protected $guarded = [];
    protected $table = "project_skill";
}
