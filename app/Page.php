<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Page extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'heading','meta_title','meta_description','meta_keywords','content','image','active','short_description','slug'
    ];

}
