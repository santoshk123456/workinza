<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReview extends Model
{
    protected $fillable = [
        'reviewed_by_user_id', 'project_id', 'reviewed_to_user_id', 'review_user','review_company',
    ];
    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function reviewed_by(){
        return $this->belongsTo(User::class,'reviewed_by_user_id','id');
    }

    public function reviewed_to(){
        return $this->belongsTo(User::class,'reviewed_to_user_id','id');
    }
}
