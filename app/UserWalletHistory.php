<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserWalletHistory extends Model
{
    protected $fillable = [
        'user_id', 'transaction_id', 'transaction_type', 'amount','new_balance','notes','type','payment_status','referral_bonus_charge','freelancer_service_charge','freelancer_service_percentage','referral_bonus_percentage','referral_or_bonus','is_dispute'
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function transactions(){
        return $this->belongsTo(Transaction::class,'transaction_id','id');
    }
}
