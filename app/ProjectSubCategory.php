<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectSubCategory extends Model
{
    protected $guarded = [];
    protected $table = "project_sub_category";
}
