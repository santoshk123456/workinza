<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectProposalMilestone extends Model
{
    //
    protected $fillable = ['project_proposal_id','milestone_name','milestone_description','milestone_amount','milestone_start_date','milestone_end_date','comments','status'];
}
