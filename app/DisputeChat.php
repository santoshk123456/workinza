<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\ProcessEmailQueue;

class DisputeChat extends Model
{
    protected $guarded = ['id'];

    protected $fillable = ['project_id','from_user_id', 'message', 'attachment', 'read_status', 'dispute_id', 'user_type'];

    
    public function sender()
    {
        return $this->belongsTo('App\User', 'from_user_id', 'id');
    }

    public function admin_sender()
    {
        return $this->belongsTo('App\Admin', 'from_user_id', 'id');
    }

    public function project()
    {
        return $this->belongsTo('App\Project', 'project_id', 'id');
    }

    public function dispute()
    {
        return $this->belongsTo('App\Dispute', 'dispute_id', 'id');
    }
    public static function sendEmailNotification($message)
    {
        $data = [];

        // $user_details = User::select('email','username')->where('id',$proposal_details->user_id)->first();
        $email_template = EmailTemplate::where('static_email_heading', 'MESSAGE_SEND')->first();
        $data['action_type'] = 'message_send';


        if ($email_template) {
            $email_html = $email_template->template;

            $email_html = str_replace("[USER_NAME]", ucwords($message->recipient->username), $email_html);
            $email_html = str_replace("[FROM_NAME]", ucwords($message->sender->username), $email_html);
            $email_html = str_replace("[MESSAGE]", $message->message, $email_html);
            $email_html = str_replace("[PROJECT_NAME]", $message->project->title, $email_html);



            $template = view('admin.email_templates.layout', compact('email_html'));

            $data['to_email'] = $message->recipient->email;
            $data['to_name'] = ucwords($message->recipient->username);
            $data['subject'] = $email_template->subject;
            $data['body'] = $template;
            $data['action_id'] = $message->id;

            $processEmailQueue = new ProcessEmailQueue($data);
            $processEmailQueue->sendEmail();
            return true;
        }
    }
    public static function ChatFeature()
    {
        $user_subscription = auth()->user()->UserSubscription->where('active', 1)->first();
        if (isset($user_subscription->membership)) {

            if ($user_subscription->membership->chat_feature == 1) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}
