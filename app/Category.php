<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\General\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    
    use Uuid,SoftDeletes;

    protected $fillable = [
        'uuid', 'name','icon', 'active','home_active'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }


     /**
     * The industries that belong to the Category.
     */
    public function industries()
    {
        return $this->belongsToMany('App\Industry')->withPivot('category_id','industry_id')->withTimestamps();
    }


    // public function test()
    // {
    //     return $this->hasMany('App\SubCategory');
    // }

    public function subcategories()
    {
        return $this->belongsToMany('App\SubCategory','sub_category_category')->withPivot('category_id','sub_category_id')->withTimestamps();
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill')->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }
}
