<?php

namespace App;

use App\Services\ProcessEmailQueue;
use Illuminate\Database\Eloquent\Model;

class ProjectInvitation extends Model
{
    protected $fillable = [
        'project_id','freelancer_id','status'
    ];
    public function project(){
        return $this->belongsTo(Project::class,'project_id');
    }

    public function freelancer(){
        return $this->belongsTo(User::class,'freelancer_id');
    }


    /**
     * inviteFreelancer
     * Params   : User and request
     * Response : return array
     * STEP 1   : Get the freelancer details
     * STEP 2   : Fetch the email template for sending invitiations
     * STEP 3   : Replace the values
     * STEP 4   : Insert an entry in the email queue
     * STEP 5   : Send mail
     * STEP 6   : Return proper response   
     */

    public static function inviteFreelancer($user,$request,$project){
        $freelancer_details = User::select('email','username')->where('id',$request->freelancer_id)->first();
        $invitation_template = EmailTemplate::where('static_email_heading','INVITE_FREELANCERS')->first();
        if ($invitation_template) {
            $email_html = $invitation_template->template;
            $email_html = str_replace("[USER_NAME]", $freelancer_details->username, $email_html);
            $email_html = str_replace("[FROM_NAME]", $user->username, $email_html);
            $email_html = str_replace("[PROJECT]", $project->title, $email_html);
            $invitation_link = route('project.listInvitations');
            $email_html = str_replace("[INVITATION_LINK]", $invitation_link,$email_html);

            $template = view('admin.email_templates.layout', compact('email_html'));
            $data = [];
            $data['to_email'] = $freelancer_details->email;
            $data['to_name'] = $freelancer_details->username;
            $data['subject'] = $invitation_template->subject;
            $data['body'] = $template;
            $data['action_id'] = $user->id;
            $data['action_type'] = 'invite_freelancer';
            $processEmailQueue = new ProcessEmailQueue($data);
            $processEmailQueue->sendEmail();
            return true;
        }
    }
}
