<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\General\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
    use Uuid,SoftDeletes;

    protected $fillable = [
        'uuid', 'name','category_id', 'active'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category','sub_category_category')->withPivot('sub_category_id','category_id')->withTimestamps();
    }

    public function industries()
    {
        return $this->belongsToMany('App\Industry','sub_category_industry')->withPivot('sub_category_id','industry_id')->withTimestamps();
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill')->withTimestamps();
    }
    
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }
}
