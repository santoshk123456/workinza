<?php

namespace App;
use App\ProjectContract;
use App\Models\Traits\General\Uuid;
use Illuminate\Database\Eloquent\Model;

class ProjectProposal extends Model
{
    use Uuid;
    protected $fillable = ['user_id','project_id','proposal_amount','proposal_document','comments','service_percentage','status'];
    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function project(){
        return $this->belongsTo(Project::class,'project_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function proposalMilestones(){
        return $this->hasMany(ProjectProposalMilestone::class);
    }
    public function projectContract(){
        return $this->hasMany(ProjectContract::class,'project_proposal_id');
    }
    
    
    public function getApplyStatusAttribute(){
        if($this->status == 0){
            return "Pending";
        }elseif($this->status == 1){
            return "Accepted";
        }else{
            return "Rejected";
        }
    }

}
