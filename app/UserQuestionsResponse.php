<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestionsResponse extends Model
{
    protected $fillable = [
    'user_id', 'security_question_id', 'answer'
    ];
    public function securityQuestions()
    {
       
        return $this->belongsTo(SecurityQuestion::class,'security_question_id','id');
    }
}