<?php

namespace App\DataTables;

use App\PushNotificationQueue;
use Yajra\DataTables\Services\DataTable;

class PushNotificationQueueDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->addIndexColumn()
            ->addColumn('sent_status', function ($pushNotificationQueue) {
                if($pushNotificationQueue->sent_status == 1) {
                    return '<div><span class="badge badge-success badge-pill">Sent</span></div>';
                } else {
                    return '<div><span class="badge badge-danger badge-pill">Failed</span></div>';
                }
            })
            ->editColumn('created_at', function ($pushNotificationQueue) {
                return $pushNotificationQueue->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($pushNotificationQueue->created_at), timezone()->setShortDateTimeFormat())) : '';
            })
            ->addColumn('action', function ($pushNotificationQueue) {
                $buttons = "";
                /* if (auth()->user()->hasPermissionTo('view email queue')) { */
                    $buttons .='<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.push_notification_queues.show', $pushNotificationQueue->id) .'\'"><i class="mdi mdi-eye"></i> </button> ';
                /* } */

                /* if (auth()->user()->hasPermissionTo('Send email queues')) { */
                    $buttons .='<button type="button" title="Resend" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#danger-alert-modal" onclick="resendForm(\'resend-form'.$pushNotificationQueue->id.'\')"><i class="mdi mdi-telegram"></i> </button><form id="resend-form'.$pushNotificationQueue->id.'" method="get" action="'.route("admin.push_queues.resend", $pushNotificationQueue->id).'"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';                
                /* } */
                return $buttons;
            })->rawColumns(['sent_status','action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PushNotificationQueue $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PushNotificationQueue $model)
    {
        $model = $model->orderBy('created_at','desc');
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->processing(true)
                    ->language ([
                        'searchPlaceholder' => 'Device/Device Id',
                        'loadingRecords' => '&nbsp;',
                        'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                            <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                                Loading...
                                        </button>',
                    ]) 
                    ->addAction()
                    ->parameters($this->getBuilderParameters());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'device_type','title' => 'Device'];
        $column_specifications[] = ['data' => 'device_id','title' => 'Device Id'];
        $column_specifications[] = ['data' => 'sent_status','title' => 'Status'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created On'];
        return $column_specifications;
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
