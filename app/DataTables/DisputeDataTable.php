<?php

namespace App\DataTables;

use App\Dispute;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class DisputeDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->editColumn('client_user_id', function ($dispute) {
                return ucwords($dispute->client->full_name);
            })
            ->editColumn('freelancer_user_id', function ($dispute) {
                return ucwords($dispute->freelancer->full_name);
            })
            ->editColumn('project_name', function ($dispute) {
                return ucwords($dispute->milestone->project->title);
            })
            ->editColumn('project_milestone_id', function ($dispute) {
                return ucwords($dispute->milestone->milestone_name);
            })
            ->editColumn('status', function ($dispute) {
                if(empty($dispute->status)){
                    return "Pending";
                }else{
                    return "Resolved";
                }
            })
            ->editColumn('created_at', function ($dispute) {
                return hcms_date(strtotime($dispute->created_at), 'date-time', false);
            })->addColumn('action', function ($dispute) {
              
                $buttons = "";
                $buttons .='<button type="button" title="Edit" class="btn ml-1 btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.disputes.edit', $dispute->id) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> ';
                $buttons .='<button type="button" title="View" class="btn ml-1 btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.disputes.show', $dispute->id) .'\'"><i class="mdi mdi-eye-outline"></i> </button> ';
    
                return $buttons;
    
            })
            ->filter(
                function ($instance) {
                    $condition = [];
                    
                    if(request('search') && request('search')['value']??false){
                        $search = request('search')['value'];
                        $instance->where('users.full_name','LIKE','%'.$search.'%');
                    }

                    if(request('daterange') != ''){
                        $range = explode('-',request('daterange'));
                        $from_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[0]))));
                        $to_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[1]))));
                        if ($from_date != '') {
                            $condition[] = ['disputes.created_at',">=",$from_date];
                        } 
                        if ($to_date != '') {
                            $condition[] = ['disputes.created_at',"<=",$to_date];
                        } 
                    }

                    
                    
                    $instance->where($condition);
                }
            );
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Dispute $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Dispute $model)
    {
        $model= $model->select('disputes.*','users.full_name') 
        ->join('users','users.id','=','disputes.client_user_id')
        ->orderBy('disputes.created_at','desc');
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Client',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->ajax([
            'url' => '',
            'type' => 'GET',
            'data' => 'function(d) { 
                d.daterange = $("#daterange_id").val();
            }',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'client_user_id','title' => 'Client','orderable'=> false];
        $column_specifications[] = ['data' => 'freelancer_user_id','title' => 'Freelancer','orderable'=> false];
        $column_specifications[] = ['data' => 'project_milestone_id','title' => 'Milestone','orderable'=> false];
        $column_specifications[] = ['data' => 'project_name','title' => 'Project','orderable'=> false];
        $column_specifications[] = ['data' => 'status','title' => 'Status','orderable'=> false];
        $column_specifications[] = ['data' => 'created_at','title' => 'Submitted On','orderable'=> false];
        return $column_specifications;
    }

    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }

    
}
