<?php

namespace App\DataTables;
use App\SmsTemplate;
use Yajra\DataTables\Services\DataTable;

class SmsTemplateDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->editColumn('created_at', function ($smsTemplate) {
            $smsTemplate->orderBy('created_at', 'desc');
        })
        ->addColumn('action', function ($smsTemplate) {
            $buttons = "";
            if (auth()->user()->hasPermissionTo('view sms templates')) {
                $buttons .='<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.sms_templates.show', $smsTemplate->id) .'\'"><i class="mdi mdi-eye"></i> </button> ';
            }
            if (auth()->user()->hasPermissionTo('edit sms templates')) {
                $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.sms_templates.edit', $smsTemplate->id) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> ';
            }
            return $buttons;
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\SmsTemplate $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SmsTemplate $model)
    {
        $model = $model->query();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->processing(true)
                    ->language ([
                        'searchPlaceholder' => 'Name/Description',
                        'loadingRecords' => '&nbsp;',
                        'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                            <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                                Loading...
                                        </button>',
                    ]) 
                    ->addAction(['title' => 'Actions'])
                    ->parameters($this->getBuilderParameters());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'process_name','title' => 'Process Name'];
        $column_specifications[] = ['data' => 'description','title' => 'Description'];
        return $column_specifications;
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
