<?php

namespace App\DataTables;

use App\MembershipPlan;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MembershipPlanDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->editColumn('name',function($membershipPlan){
            return ucwords($membershipPlan->name);
        })
        ->editColumn('user_type',function($membershipPlan){
            if(empty($membershipPlan->user_type)){
                return "Client";
            }else{
                return "Freelancer";
            }
        })
        ->editColumn('plan_type',function($membershipPlan){
            if($membershipPlan->plan_type == 1){
                return 'Monthly';
            }else if($membershipPlan->plan_type == 2){
                return 'Yearly';
            }
            
        })
        ->editColumn('monthly_amount',function($membershipPlan){
            if(!empty($membershipPlan->monthly_amount)){
                return formatAmount($membershipPlan->monthly_amount);
            }else{
                return 'NA';
            }
            
        })
        ->editColumn('yearly_amount',function($membershipPlan){
            if(!empty($membershipPlan->yearly_amount)){
                return formatAmount($membershipPlan->yearly_amount);
            }else{
                return 'NA';
            }
            
        })
        ->editColumn('created_at',function($membershipPlan){
            return hcms_date(strtotime($membershipPlan->created_at), 'date-time', false);
        })
        ->addColumn('action', function ($membershipPlan) {
              
            $buttons = "";
                if(auth()->user()->hasPermissionTo('view membership plan')){
                $buttons .= '<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.membership_plans.show', $membershipPlan->uuid) .'\'"><i class="mdi mdi-eye"></i> </button>';
                }
                if(auth()->user()->hasPermissionTo('edit membership plan')){
                $buttons .='<button type="button" title="Edit" class="btn ml-1 btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.membership_plans.edit', $membershipPlan->uuid) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> ';
                }
                if(auth()->user()->hasPermissionTo('delete membership plan')){
                $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$membershipPlan->uuid.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form style="display:none" id="delete-form'.$membershipPlan->uuid.'" method="post" action="'.route("admin.membership_plans.destroy", $membershipPlan->uuid).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }

                if($membershipPlan->active == 1){
                    $buttons .='<button type="button" title="Disable" class="btn btn-icon btn-warning btn-sm deactivate-btn ml-1" data-toggle="modal" data-target="#danger-alert-modal" onclick="deactivateForm(\'deactivate-form'.$membershipPlan->uuid.'\')"><i class="mdi mdi-check-circle-outline"></i> </button><form style="display:none" id="deactivate-form'.$membershipPlan->uuid.'" method="post" action="'.route("admin.membership_plans.changeStatus", $membershipPlan->uuid).'"><input type="hidden" name="_method" value="put"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }else{
                    $buttons .='<button type="button" title="Enable" class="btn btn-icon btn-primary btn-sm deactivate-btn ml-1" data-toggle="modal" data-target="#danger-alert-modal" onclick="activateForm(\'active-form'.$membershipPlan->uuid.'\')"><i class="mdi mdi-close-circle"></i> </button><form style="display:none" id="active-form'.$membershipPlan->uuid.'" method="post" action="'.route("admin.membership_plans.changeStatus", $membershipPlan->uuid).'"><input type="hidden" name="_method" value="put"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }
                
           return $buttons;

       }); 
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\MembershipPlan $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MembershipPlan $model)
    {
        $model = MembershipPlan::where('user_type',1)->orderby('created_at','desc')->get();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Plan',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> 'Sl#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'name','title' => 'Plan'];
        $column_specifications[] = ['data' => 'plan_type','title' => 'Plan Type'];
        $column_specifications[] = ['data' => 'monthly_amount','title' => 'Monthly Plan Amount'];
        $column_specifications[] = ['data' => 'yearly_amount','title' => 'Yearly Plan Amount'];
        $column_specifications[] = ['data' => 'user_type','title' => 'User Type'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created On'];
        return $column_specifications;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
