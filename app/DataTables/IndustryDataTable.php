<?php

namespace App\DataTables;

use App\Industry;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class IndustryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->editColumn('name',function($industry){
                return ucwords($industry->name);
            })
            ->editColumn('created_at', function ($industry) {
                return hcms_date(strtotime($industry->created_at), 'date-time', false);
            })
            ->addColumn('action', function ($industry) {
                    $buttons = "";
                    if (auth()->user()->hasPermissionTo('edit industries')) {
                    $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" data-link = "'.route('admin.industries.update', $industry->uuid).'" data-industry="'.$industry->name.'" onclick="edit(this)"><i class="mdi mdi-pencil-outline"></i> </button> ';
                    }
                    
                    if (auth()->user()->hasPermissionTo('delete industries')) {
                        $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$industry->uuid.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form style="display:none;" id="delete-form'.$industry->uuid.'" method="post" action="'.route("admin.industries.destroy", $industry->uuid).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                    }
                    if($industry->active == 1){
                        $buttons .='<button type="button" title="Disable" class="btn btn-icon btn-warning btn-sm deactivate-btn ml-1" data-toggle="modal" data-target="#danger-alert-modal" onclick="deactivateForm(\'deactivate-form'.$industry->uuid.'\')"><i class="mdi mdi-check-circle-outline"></i> </button><form style="display:none" id="deactivate-form'.$industry->uuid.'" method="post" action="'.route("admin.industries.changeStatus", $industry->uuid).'"><input type="hidden" name="_method" value="put"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                    }else{
                        $buttons .='<button type="button" title="Enable" class="btn btn-icon btn-primary btn-sm deactivate-btn ml-1" data-toggle="modal" data-target="#danger-alert-modal" onclick="activateForm(\'active-form'.$industry->uuid.'\')"><i class="mdi mdi-close-circle"></i> </button><form style="display:none" id="active-form'.$industry->uuid.'" method="post" action="'.route("admin.industries.changeStatus", $industry->uuid).'"><input type="hidden" name="_method" value="put"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                    }

                    return $buttons;
            })
            ->rawColumns(['action']); ; 
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Industry $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Industry $model)
    {
        $model = Industry::orderby('created_at','desc')->get();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Industry',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'name','title' => 'Indusrty'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created On'];
        return $column_specifications;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
