<?php

namespace App\DataTables;

use App\Dispute;
use App\UserWalletHistory;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class WalletDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->editColumn('created_at', function ($wallet) {
                return hcms_date(strtotime($wallet->created_at), 'date-time', false);
            })
            ->editColumn('amount', function ($wallet) {
                return formatAmount($wallet->amount);
        
          
            })->addColumn('action', function ($wallet) {
                if($wallet->is_dispute == 0){
                    $buttons = "";
                    $buttons .='<button type="button" class="btn btn-icon btn-info btn-sm" data-id="'.$wallet->id.'" data-name="'.$wallet->amount.'" title="Edit" id="pay_now">Pay now </button> ';
        
                    return $buttons;
                }else{
                    $buttons = "";
                    $buttons .='<button type="button" class="btn btn-icon btn-danger btn-sm"  title="Dispute" >Dsipute </button> ';
        
                    return $buttons;
                }
    
            })
            ->filter(
                function ($instance) {
                    $condition = [];
                    
                    // if(request('search') && request('search')['value']??false){
                    //     $search = request('search')['value'];
                    //     $instance->where('users.full_name','LIKE','%'.$search.'%');
                    // }

                    if(request('daterange') != ''){
                        $range = explode('-',request('daterange'));
                        $from_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[0]))));
                        $to_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[1]))));
                        if ($from_date != '') {
                            $condition[] = ['created_at',">=",$from_date];
                        } 
                        if ($to_date != '') {
                            $condition[] = ['created_at',"<=",$to_date];
                        } 
                    }

                    
                    
                    $instance->where($condition);
                }
            );
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Dispute $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserWalletHistory $model)
    {
        $cond = [];
        $cond['payment_status'] = 0;
        $cond['transaction_type'] = 0;
      
        $model = $model->select('user_wallet_histories.*')->where($cond)->orderby('id','DESC')->with('user'); 
     
      
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Client',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->ajax([
            'url' => '',
            'type' => 'GET',
            'data' => 'function(d) { 
                d.daterange = $("#daterange_id").val();
            }',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'created_at','title' => 'Date','orderable'=> false];
        $column_specifications[] = ['data' => 'amount','title' => 'Amount','orderable'=> false];
        $column_specifications[] = ['data' => 'notes','title' => 'Description','orderable'=> false];
       
        return $column_specifications;
    }

    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }

    
}
