<?php

namespace App\DataTables;

use App\EmailQueue;
use Yajra\DataTables\Services\DataTable;

class EmailQueueDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->addColumn('sent_status', function ($emailQueue) {
            if($emailQueue->sent_status == 1) {
                return '<div><span class="badge badge-success badge-pill">Sent</span></div>';
            } else {
                return '<div><span class="badge badge-danger badge-pill">Failed</span></div>';
            }
        })
        ->editColumn('created_at', function ($emailQueue) {
            return $emailQueue->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($emailQueue->created_at), timezone()->setShortDateTimeFormat())) : '';
        })
        ->addColumn('action', function ($emailQueue) {
            $buttons = "";
            if (auth()->user()->hasPermissionTo('view email queues')) { 
                $buttons .='<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.email_queues.show', $emailQueue->id) .'\'"><i class="mdi mdi-eye"></i> </button> ';
            } 
            /* if (auth()->user()->hasPermissionTo('Send email queues')) { */
                $buttons .='<button type="button" title="Resend" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#danger-alert-modal" onclick="resendForm(\'resend-form'.$emailQueue->id.'\')"><i class="mdi mdi-telegram"></i> </button><form id="resend-form'.$emailQueue->id.'" method="get" action="'.route("admin.email_queues.resend", $emailQueue->id).'"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';                
            /* } */
            return $buttons;
        })->rawColumns(['sent_status','action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\EmailQueue $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(EmailQueue $model)
    {
        $model = $model->query();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->processing(true)
                    ->language ([
                        'searchPlaceholder' => 'Email/Subject',
                        'loadingRecords' => '&nbsp;',
                        'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                            <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                                Loading...
                                        </button>',
                    ]) 
                    ->addAction(['title' => 'Actions'])
                    ->parameters($this->getBuilderParameters());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'to_email','title' => 'Email'];
        $column_specifications[] = ['data' => 'subject','title' => 'Subject'];
        $column_specifications[] = ['data' => 'sent_status','title' => 'Status'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created On'];
        return $column_specifications;
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
