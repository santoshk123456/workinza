<?php

namespace App\DataTables;

use App\TermsAndCondition;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TermsAndConditionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->editColumn('heading',function($termsAndCondition){
                return ucwords($termsAndCondition->heading);
            })
            ->editColumn('created_at', function ($termsAndCondition) {
                return hcms_date(strtotime($termsAndCondition->created_at), 'date-time', false);
            })
            ->editColumn('updated_at', function ($termsAndCondition) {
                return hcms_date(strtotime($termsAndCondition->updated_at), 'date-time', false);
            })
            ->addColumn('action', function ($termsAndCondition) {
                $buttons = "";
                if(auth()->user()->hasPermissionTo('view terms and conditions')){
                    $buttons .='<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.terms_and_conditions.show', $termsAndCondition->id) .'\'"><i class="mdi mdi-eye"></i> </button> '; 
                }
                if(auth()->user()->hasPermissionTo('edit terms and conditions')){
                    $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.terms_and_conditions.edit', $termsAndCondition->id) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> '; 
                }
                if(auth()->user()->hasPermissionTo('delete terms and conditions')){
                $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm mr-0" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$termsAndCondition->id.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form id="delete-form'.$termsAndCondition->id.'" method="post" action="'.route("admin.terms_and_conditions.destroy", $termsAndCondition->id).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }
                return $buttons;
            }); 
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\TermsAndCondition $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TermsAndCondition $model)
    {
        $model = TermsAndCondition::orderby('created_at','desc')->get();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Heading',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> 'Sl#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'heading','title' => 'Heading'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created On'];
        $column_specifications[] = ['data' => 'updated_at','title' => 'Last Modified On'];
        return $column_specifications;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
