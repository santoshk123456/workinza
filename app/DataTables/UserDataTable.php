<?php

namespace App\DataTables;
use App\User;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->addColumn('active', function ($user) {
            if (auth()->user()->hasPermissionTo('edit clients')) {
                if($user->active == 1) {
                    return '<div class="active-checkbox"><input type="checkbox" data-user="'.$user->id.'" id="switch'.$user->id.'" checked data-switch="success"><label for="switch'.$user->id.'" data-on-label="Active" data-off-label="Inactive" class="mb-0 mt-0 d-block"></label></div>';
                } else {
                    return '<div class="active-checkbox"><input type="checkbox" data-user="'.$user->id.'" id="switch'.$user->id.'"  data-switch="success"><label for="switch'.$user->id.'" data-on-label="Active" data-off-label="Inactive" class="mb-0 mt-0 d-block"></label></div>';
                }
            }
            else{
                if($user->active == 1) {
                    return "<span class='badge badge-success p-1'>Active</span>";
                } else {
                    return "<span class='badge badge-danger p-1'>Inactive</span>";
                }
            }
        })
        ->addColumn('action', function ($user) {
            $buttons = "";
            if (auth()->user()->hasPermissionTo('view clients')) {
                $buttons .='<button type="button" class="btn btn-icon btn-success btn-sm" title="View" onclick="window.location.href=\''. route('admin.users.show', $user->uuid) .'\'"><i class="mdi mdi-eye"></i> </button> ';
            }
            if (auth()->user()->hasPermissionTo('edit clients')) {
                $buttons .='<button type="button" class="btn btn-icon btn-info btn-sm" title="Edit" onclick="window.location.href=\''. route('admin.users.edit', $user->uuid) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> ';
            }
            if (auth()->user()->hasPermissionTo('delete clients')) {
                $buttons .='<button type="button" class="btn btn-icon btn-danger btn-sm" title="Delete" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$user->uuid.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form id="delete-form'.$user->uuid.'" method="post" action="'.route("admin.users.destroy", $user->uuid).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
            }
            return $buttons;
        })->rawColumns(['active','action']); 
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $model = $model->query();
        if($this->request->get('status_filter') == 'active'){
            $model = $model->where('active',1)->orderBy('updated_at','desc');
        } elseif($this->request->get('status_filter') == 'inactive'){
            $model = $model->where('active',0)->orderBy('updated_at','desc');
        }
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->processing(true)
                    ->language ([
                        'searchPlaceholder' => 'Name/Email',
                        'loadingRecords' => '&nbsp;',
                        'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                            <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                                Loading...
                                        </button>',
                    ]) 
                    // ->pagingType("input")
                    ->ajax([
                        'url' => '',
                        'type' => 'GET',
                        'data' => 'function(d) { d.status_filter = $("#status-filter").val(); }',
                    ])
                    ->addAction(['title' => 'Actions'])
                    ->parameters($this->getBuilderParameters());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'username','title' => 'User Name'];
        $column_specifications[] = ['data' => 'email','title' => 'Email'];
        $column_specifications[] = ['data' => 'active','title' => 'Status','orderable'=> false];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created Date','orderable'=> false];
        return $column_specifications;
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            // 'pagingType' => 'full_numbers',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
