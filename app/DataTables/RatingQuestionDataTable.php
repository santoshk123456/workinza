<?php

namespace App\DataTables;

use App\RatingQuestion;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class RatingQuestionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->editColumn('user_type',function($ratingQuestion){
            return ucwords($ratingQuestion->user_type);
        })
        ->editColumn('updated_at', function ($ratingQuestion) {
            return hcms_date(strtotime($ratingQuestion->updated_at), 'date-time', false);
        })
        ->addColumn('action', function ($ratingQuestion) {
            $buttons = "";
            if (auth()->user()->hasPermissionTo('edit rating questions')) {
            $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" data-link = "'.route('admin.rating_questions.update', $ratingQuestion->id).'" data-question="'.$ratingQuestion->question.'" data-user_type="'.$ratingQuestion->user_type.'" onclick="edit(this)"><i class="mdi mdi-pencil-outline"></i> </button> ';
            }
            if (auth()->user()->hasPermissionTo('edit rating questions')) {
            $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm mr-0" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$ratingQuestion->id.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form id="delete-form'.$ratingQuestion->id.'" method="post" action="'.route("admin.rating_questions.destroy", $ratingQuestion->id).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
            }

            return $buttons;
            
    
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\RatingQuestion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(RatingQuestion $model)
    {
        $model = RatingQuestion::orderby('created_at','desc')->get();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'User Type',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> 'Sl#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'user_type','title' => 'User Type'];
        $column_specifications[] = ['data' => 'question','title' => 'Question'];
        $column_specifications[] = ['data' => 'updated_at','title' => 'Updated On'];
        return $column_specifications;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
