<?php

namespace App\DataTables;

use App\SecurityQuestion;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SecurityQuestionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->editColumn('user_type',function($securityQuestion){
                if(empty($securityQuestion->user_type)){
                    return "Client";
                }elseif($securityQuestion->user_type == 1){
                    return "Freelancer";
                }else{
                    return "Both";
                }
            })
            ->editColumn('updated_at', function ($securityQuestion) {
                return hcms_date(strtotime($securityQuestion->updated_at), 'date-time', false);
            })
            ->addColumn('action', function ($securityQuestion) {
                $buttons = "";
                if (auth()->user()->hasPermissionTo('edit security questions')) {
                $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" data-link = "'.route('admin.security_questions.update', $securityQuestion->id).'" data-question="'.$securityQuestion->question.'" data-user_type="'.$securityQuestion->user_type.'" onclick="edit(this)"><i class="mdi mdi-pencil-outline"></i> </button> ';
                }
                if (auth()->user()->hasPermissionTo('delete security questions')) {
                $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm mr-0" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$securityQuestion->id.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form id="delete-form'.$securityQuestion->id.'" method="post" action="'.route("admin.security_questions.destroy", $securityQuestion->id).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }

                return $buttons;
                
        })

        ->filter(
            function ($instance) {

                if(request('search') && request('search')['value']??false){
                    $search = request('search')['value'];
                    if($search == 'Both' || $search == 'both')
                    $instance->where('user_type',2);
                    elseif($search == 'Client' || $search == 'client' )
                    $instance->where('user_type',0);
                    elseif($search == 'Freelancer' || $search == 'freelancer')
                    $instance->where('user_type',1);
                }
                
            }
        )
        ->rawColumns(['action']);  
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\SecurityQuestion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SecurityQuestion $model)
    {
        $model = SecurityQuestion::orderby('created_at','desc');
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'User Type',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> 'Sl#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'user_type','title' => 'User Type'];
        $column_specifications[] = ['data' => 'question','title' => 'Question'];
        $column_specifications[] = ['data' => 'updated_at','title' => 'Updated On'];
        return $column_specifications;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
