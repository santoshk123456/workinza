<?php

namespace App\DataTables;

use App\EmailTemplate;
use Yajra\DataTables\Services\DataTable;

class EmailTemplateDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->editColumn('created_at', function ($emailTemplate) {
            $emailTemplate->orderBy('created_at', 'desc');
        })
            ->addColumn('action', function ($emailTemplate) {
                $buttons = "";
                if (auth()->user()->hasPermissionTo('view email templates')) {
                    $buttons .='<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.email_templates.show', $emailTemplate->id) .'\'"><i class="mdi mdi-eye"></i> </button> ';
                }
                if (auth()->user()->hasPermissionTo('edit email templates')) {
                    $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.email_templates.edit', $emailTemplate->id) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> ';
                }
                return $buttons;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\EmailTemplate $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(EmailTemplate $model)
    {
        $model = $model->query();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Heading/Subject',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->columns($this->getColumns())
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'static_email_heading','title' => 'Email Heading'];
        $column_specifications[] = ['data' => 'subject','title' => 'Subject'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created On'];
        return $column_specifications;
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [4,'desc'],
           ];
    }
}
