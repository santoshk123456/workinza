<?php

namespace App\DataTables;

use App\Admin;
use Yajra\DataTables\Services\DataTable;

class AdminDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        if(!empty($this->role)){
            // Returning the json response data for ajax datatable
            return datatables($query)
            ->addIndexColumn()
            ->editColumn('name',function($admin){
                return ucwords($admin->name);
            })
            ->editColumn('created_at',function($admin){
                return date('Y-m-d H:i:s',strtotime($admin->created_at));
                // return date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime($admin->created_at)));
            })
            ->addColumn('active_status', function ($admin) {
                if ($admin->id != 1) {
                    if(auth()->user()->hasPermissionTo('edit admin users')){
                        if ($admin->active == 1) {
                            return '<div><input type="checkbox" id="switch'.$admin->id.'" checked="" data-switch="success"><label for="switch'.$admin->id.'" data-on-label="Yes" data-off-label="No" class="mb-0 d-block"></label></div>';
                        } else {
                            return '<div><input type="checkbox" id="switch'.$admin->id.'" checked="" data-switch="success"><label for="switch'.$admin->id.'" data-on-label="Yes" data-off-label="No" class="mb-0 d-block"></label></div>';
                        }
                    } else {
                        if ($admin->active == 1) {
                            return 'Active';
                        } else {
                            return 'Inactive';
                        }
                    }
                } else {
                    return 'Active';
                }
            })
            ->addColumn('action', function ($admin) {
                if($admin->id != 1){
                    $buttons = "";
                    if(auth()->user()->hasPermissionTo('view admin users')){
                        $buttons .='<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.admins.show', $admin->uuid) .'\'"><i class="mdi mdi-eye"></i> </button> '; 
                    }
                    if(auth()->user()->hasPermissionTo('edit admin users')){
                        $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.admins.edit', $admin->uuid) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> '; 
                    }
                    if(auth()->user()->hasPermissionTo('delete admin users')){
                        $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$admin->uuid.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form id="delete-form'.$admin->id.'" method="post" action="'.route("admin.admins.destroy", $admin->id).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>'; 
                    }
                    return $buttons;
                } else {
                    return "Not Applicable";
                }
            })->rawColumns(['active_status','action']);
        } else {
            // Returning the json response data for ajax datatable
            return datatables($query)
            ->addIndexColumn()
            ->addColumn('role', function ($admin) {
                $admin_role = $admin->roles()->first();
                if($admin_role) {
                    return ucwords($admin_role->name);
                }
            })
            ->addColumn('active_status', function ($admin) {
                if ($admin->id != 1) {
                    if(auth()->user()->hasPermissionTo('edit admin users')){
                        if ($admin->active == 1) {
                            return '<div class="active-checkbox"><input type="checkbox" data-admin="'.$admin->uuid.'" id="switch'.$admin->uuid.'" data-switch="success" checked><label for="switch'.$admin->uuid.'" data-on-label="Active" data-off-label="Inactive" class="mb-0 mt-0 d-block"></label></div>';
                        } else {
                            return '<div class="active-checkbox"><input type="checkbox" data-admin="'.$admin->uuid.'" id="switch'.$admin->uuid.'"  data-switch="success"><label for="switch'.$admin->uuid.'" data-on-label="Active" data-off-label="Inactive" class="mb-0 mt-0 d-block"></label></div>';
                        }
                    } else {
                        if ($admin->active == 1) {
                            return "<span class='badge badge-success p-1'>Active</span>";
                        } else {
                            return "<span class='badge badge-danger p-1'>Inactive</span>";
                        }
                    }
                } else {
                    return 'Active';
                }
            })
            ->editColumn('created_at', function ($admin) {
                //return date('Y-m-d H:i:s',strtotime($admin->created_at));
                return hcms_date(strtotime($admin->created_at), 'date', false);
               // return date('Y-m-d H:i:s',strtotime('+5 hour +30 minutes',strtotime($admin->created_at)));
            })
            ->addColumn('action', function ($admin) {
                if($admin->id != 1){
                    $buttons = "";
                    if(auth()->user()->hasPermissionTo('view admin users')){
                        $buttons .='<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.admins.show', $admin->uuid) .'\'"><i class="mdi mdi-eye"></i> </button> '; 
                    }
                    if(auth()->user()->hasPermissionTo('edit admin users')){
                        $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.admins.edit', $admin->uuid) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> '; 
                    }
                    if(auth()->user()->hasPermissionTo('delete admin users')){
                        $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$admin->uuid.'\')" ><i class="mdi mdi-trash-can-outline"></i> </button><form id="delete-form'.$admin->uuid.'" method="post" action="'.route("admin.admins.destroy", $admin->uuid).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>'; 
                    }
                    return $buttons;
                } else {
                    return "Not Applicable";
                }
            })->rawColumns(['active_status','action']);
        }
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Admin $model)
    {
        if($this->role != ""){
            $model = $model->role(str_replace("-"," ",$this->role))->with('roles');
        } else {
            $model = $model->with('roles');
        }
        if($this->request->get('status_filter') == 'active'){
            $model = $model->where('active',1);
        } elseif($this->request->get('status_filter') == 'inactive'){
            $model = $model->where('active',0);
        } 
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->processing(true)
                    ->language ([
                        'searchPlaceholder' => 'Name/Email',
                        'loadingRecords' => '&nbsp;',
                        'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                            <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                                Loading...
                                        </button>',
                    ]) 
                    ->ajax([
                        'url' => '',
                        'type' => 'GET',
                        'data' => 'function(d) { d.status_filter = $("#status-filter").val(); }',
                    ])
                    ->addAction(['title' => 'Actions'])
                    ->parameters($this->getBuilderParameters());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        // Defining the required columns and their specifications
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'footer'=> ''];
        if($this->role == ""){
            $column_specifications[] = ['data' => 'role','title' => 'Role','orderable'=>false];
        }
        $column_specifications[] = ['data' => 'name','title' => 'Name',];
        $column_specifications[] = ['data' => 'email','title' => 'Email'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created Date'];
        $column_specifications[] =  ['data' => 'active_status','title' => 'Status','orderable'=> false,'searchable'=> false,'exportable'=> false];
        return $column_specifications;
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
                'dom' => 'Bflrtip',
                'responsive'=> false,
                'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
                'buttons'=> [],
                'order' => [0,'desc'],
               ];
    }
}
