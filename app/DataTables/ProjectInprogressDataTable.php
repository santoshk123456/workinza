<?php

namespace App\DataTables;

use App\Project;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProjectInprogressDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->editColumn('id',function($project){
            return "<a href='".route('admin.projects.show', $project->uuid)."'>".generateProjectId($project->id)."</a>";
        })
        ->editColumn('user.email',function($project){
            return "<a href='".route('admin.users.show',$project->user->uuid)."'>".$project->user->email."</a>";
        })
        ->editColumn('title',function($project){
            return ucwords($project->title);
        })
        ->editColumn('amount',function($project){
            return config('data.currency_symbol').number_format($project->amount, 2, '.', ',');
        })
        ->editColumn('expected_start_date',function($project){
            return hcms_date(strtotime($project->expected_start_date), 'date', false);
        })
        ->editColumn('expected_end_date',function($project){
            return hcms_date(strtotime($project->expected_end_date), 'date', false);
        })
        ->editColumn('contracts.id',function($project){
            return $project->contracts->contract_id;
        })
        ->addColumn('active', function ($project) {
            $buttons = "";
            
                  
                  //  $buttons .='<button  onclick="is_active('.$project->id.',1)"  class="btn btn-danger mr-2" title="Click To Enable" ><i class="fa fa-close"></i></button>';
                   // return '<div class="active-checkbox"><input type="checkbox" data-user="'.$project->id.'" id="switch'.$project->id.'" checked data-switch="success"><label for="switch'.$project->id.'" data-on-label="Active" data-off-label="Inactive" class="mb-0 mt-0 d-block"></label></div>';
                // } 
                // else {
                 //   $buttons .='<button  onclick="is_active('.$project->id.',0)"  class="btn btn-success mr-2" title="Click To Disable" ><i class="fa fa-check"></i></button>';
                    return '<div class="active-checkbox"><input type="checkbox" data-user="'.$project->id.'" id="switch'.$project->id.'"  data-switch="success"><label for="switch'.$project->id.'" data-on-label="Completed" data-off-label=" In Progress" class="mb-0 mt-0 d-block"></label></div>';
            
            
          
        })
        ->editColumn('status',function($project){
           return projectStatus($project->status);
        })->addColumn('action', function ($project) {
              
            $buttons = "";

            // $buttons .= '<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.projects.show', $project->uuid) .'\'"><i class="mdi mdi-eye"></i> </button>';
            
            $buttons .='<button type="button" title="Edit" class="btn ml-1 btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.projects.edit', $project->uuid) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> ';

            $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm mr-0" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$project->uuid.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form id="delete-form'.$project->uuid.'" method="post" action="'.route("admin.projects.destroy", $project->uuid).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';

            return $buttons;

        })
        ->rawColumns(['user.email','action','id','active']); 
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Project $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Project $model)
    {
        return $model->whereIn('status',[2,3])->with('user','contracts')->orderby('created_at','DESC');;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Project',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> 'Sl#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'contracts.id','title' => 'Contract ID'];
        $column_specifications[] = ['data' => 'id','title' => 'Project ID'];
        $column_specifications[] = ['data' => 'user.email','title' => 'Client'];
        $column_specifications[] = ['data' => 'title','title' => 'Project Name'];
        // $column_specifications[] = ['data' => 'amount','title' => 'Project Amount'];
        $column_specifications[] = ['data' => 'expected_start_date','title' => 'Start Date'];
        $column_specifications[] = ['data' => 'expected_end_date','title' => 'End Date'];
        $column_specifications[] = ['data' => 'status','title' => 'Status'];
        // $column_specifications[] = ['data' => 'active','title' => 'Complete Status','orderable'=> false];
        return $column_specifications;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
