<?php

namespace App\DataTables;

use App\Page;
use Yajra\DataTables\Services\DataTable;

class PageDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->editColumn('heading',function($page){
                return ucwords($page->heading);
            })
            ->editColumn('created_at', function ($page) {
                return $page->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($page->created_at), timezone()->setShortDateTimeFormat())) : '';
            })
            ->editColumn('updated_at', function ($page) {
                return $page->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($page->updated_at), timezone()->setShortDateTimeFormat())) : '';
            })
            ->addColumn('action', function ($page) {
                $buttons = "";
                if(auth()->user()->hasPermissionTo('view pages')){
                    $buttons .='<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.pages.show', $page->id) .'\'"><i class="mdi mdi-eye"></i> </button> '; 
                }
                if(auth()->user()->hasPermissionTo('edit pages')){
                    $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.pages.edit', $page->id) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> '; 
                }
                return $buttons;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Page $model)
    {
        $model = $model->query();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->processing(true)
                    ->language ([
                        'searchPlaceholder' => 'Heading',
                        'loadingRecords' => '&nbsp;',
                        'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                            <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                                Loading...
                                        </button>',
                    ]) 
                    ->addAction(['title' => 'Actions'])
                    ->parameters($this->getBuilderParameters());
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'heading','title' => 'Heading'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Created On'];
        $column_specifications[] = ['data' => 'updated_at','title' => 'Last Modified On'];
        return $column_specifications;
    }
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
