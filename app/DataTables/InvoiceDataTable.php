<?php

namespace App\DataTables;

use App\Invoice;
use App\Transaction;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class InvoiceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('id', function ($invoice){ 
                return 'IN'.$invoice->id;
            })
            ->editColumn('user_id', function ($invoice){ 
                return '<a href="'.route('admin.users.show', $invoice->user->uuid).'">'.$invoice->user->full_name.'</a>';
            })
            ->editColumn('type', function ($invoice){ 
                if(empty($invoice->type)){
                    return "Arbitation";
                }elseif($invoice->type == 1){
                    return "Project Payment";
                }elseif($invoice->type == 2){
                    return "Cancellation";
                }elseif($invoice->type == 3){
                    return "Refund";
                }elseif($invoice->type == 4){
                    return "Bonus";
                }elseif($invoice->type == 5){
                    return "Subscription Payment";
                }
            })
            ->editColumn('status', function ($invoice){ 
                if(empty($invoice->status)){
                    return "Pending";
                }elseif($invoice->status == 1){
                    return "Paid";
                }elseif($invoice->status == 2){
                    return "Rejected";
                }
            })
            ->editColumn('amount', function ($invoice){ 
                return formatAmount($invoice->amount);
            })
            ->editColumn('created_at', function ($invoice) {
                return hcms_date(strtotime($invoice->created_at), 'date-time', false);
            })
            ->filter(
                function ($instance) {
                    $condition = [];
                    
                    if(request('search') && request('search')['value']??false){
                        $search = request('search')['value'];
                        $split = explode("IN",$search);
                        if(!empty($split[1]))
                        $instance->where('id',$split[1]);
                    }

                    if(request('status') != ''){
                        $instance->where('status', request('status'));
                    }
                    if(request('type') != ''){
                        $instance->where('type', request('type'));
                    }

                    if(request('daterange') != ''){
                        $range = explode('-',request('daterange'));
                        $from_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[0]))));
                        $to_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[1]))));
                        if ($from_date != '') {
                            $condition[] = ['created_at',">=",$from_date];
                        } 
                        if ($to_date != '') {
                            $condition[] = ['created_at',"<=",$to_date];
                        } 
                    }

                    
                    
                    $instance->where($condition);
                }
            )
            ->addColumn('action', function ($invoice) {
              
                $buttons = "";
                $buttons .= '<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.invoices.show', $invoice->uuid) .'\'"><i class="mdi mdi-eye"></i> </button>';
                    
               return $buttons;
    
           })->rawColumns(['user_id','action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Transaction $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Invoice $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Invoice ID',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->ajax([
            'url' => '',
            'type' => 'GET',
            'data' => 'function(d) { 
                d.daterange = $("#daterange_id").val();
                d.status = $("#status").val();
                d.type = $("#type").val();
            }',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['data' => 'id','title' => 'Invoice Id'];
        $column_specifications[] = ['data' => 'user_id','title' => 'Bill From','orderable'=> false];
        $column_specifications[] = ['data' => 'type','title' => 'Type'];
        $column_specifications[] = ['data' => 'status','title' => 'Status'];
        $column_specifications[] = ['data' => 'amount','title' => 'Amount'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Invoiced On','orderable'=> false];
        return $column_specifications;
    }


    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
