<?php

namespace App\DataTables;

use App\Transaction;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TransactionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('id', function ($transaction){ 
                return getTransaction($transaction->id);
            })
            ->editColumn('user_id', function ($transaction){ 
                return ucwords($transaction->user->full_name);
            })
            ->editColumn('transaction_type', function ($transaction){ 
                if(empty($transaction->transaction_type)){
                    return "Credit";
                }else{
                    return "Debit";
                }
            })
            ->editColumn('payment_status', function ($transaction){ 
                if(empty($transaction->payment_status)){
                    return "Pending";
                }elseif($transaction->payment_status == 1){
                    return "Paid";
                }elseif($transaction->payment_status == 2){
                    return "Failed";
                }
            })
            ->editColumn('amount', function ($transaction){ 
                return formatAmount($transaction->amount);
            })
            ->editColumn('created_at', function ($transaction) {
                return hcms_date(strtotime($transaction->created_at), 'date-time', false);
            })
            ->filter(
                function ($instance) {
                    $condition = [];
                    
                    if(request('search') && request('search')['value']??false){
                        $search = request('search')['value'];
                        $split = explode("TN",$search);
                        if(!empty($split[1]))
                        $instance->where('id',$split[1]);
                    }

                    if(request('payment_status_id') != ''){
                        $instance->where('payment_status', request('payment_status_id'));
                    }

                    if(request('daterange') != ''){
                        $range = explode('-',request('daterange'));
                        $from_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[0]))));
                        $to_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[1]))));
                        if ($from_date != '') {
                            $condition[] = ['created_at',">=",$from_date];
                        } 
                        if ($to_date != '') {
                            $condition[] = ['created_at',"<=",$to_date];
                        } 
                    }

                    
                    
                    $instance->where($condition);
                }
            )
            ->addColumn('action', function ($transaction) {
              
                $buttons = "";
                    if(auth()->user()->hasPermissionTo('view transactions')){
                        $buttons .= '<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.transactions.show', $transaction->uuid) .'\'"><i class="mdi mdi-eye"></i> </button>';
                    }
                    if(auth()->user()->hasPermissionTo('edit transactions')){
                        $buttons .='<button type="button" title="Edit" class="btn btn-icon btn-info btn-sm ml-1" data-link = "'.route('admin.transactions.update', $transaction->uuid).'" data-payment_status="'.$transaction->payment_status.'" onclick="edit(this)"><i class="mdi mdi-pencil-outline"></i> </button> ';
                    }
                    
               return $buttons;
    
           });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Transaction $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Transaction $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Transaction ID',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->ajax([
            'url' => '',
            'type' => 'GET',
            'data' => 'function(d) { 
                d.daterange = $("#daterange_id").val();
                d.payment_status_id = $("#payment_status_id").val();
            }',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['data' => 'id','title' => 'Transaction ID'];
        $column_specifications[] = ['data' => 'user_id','title' => 'Paid By','orderable'=> false];
        $column_specifications[] = ['data' => 'user_id','title' => 'Paid To','orderable'=> false];
        $column_specifications[] = ['data' => 'transaction_type','title' => 'Transaction Type'];
        $column_specifications[] = ['data' => 'payment_status','title' => 'Status'];
        $column_specifications[] = ['data' => 'amount','title' => 'Amount'];
        $column_specifications[] = ['data' => 'created_at','title' => 'Transferred On','orderable'=> false];
        return $column_specifications;
    }


    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
