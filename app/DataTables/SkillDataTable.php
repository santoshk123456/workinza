<?php

namespace App\DataTables;

use App\Skill;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class SkillDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->addIndexColumn()
        ->editColumn('name',function($skill){
            return ucwords($skill->name);
        })
        ->addColumn('category',function($skill){
            $all_cat = $skill->categories->pluck('name')->toArray();
            $List = ucwords(implode(', ', $all_cat)); 
            return $List;
        })
        ->addColumn('subcategory',function($skill){
            $all_cat = $skill->subcategories->pluck('name')->toArray();
            $List = ucwords(implode(', ', $all_cat)); 
            return $List;
        })
        ->addColumn('action', function ($skill) {
              
            $buttons = "";
            
                if(auth()->user()->hasPermissionTo('show skills')){
                $buttons .= '<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.skills.show', $skill->uuid) .'\'"><i class="mdi mdi-eye"></i> </button>';
                }
                if(auth()->user()->hasPermissionTo('edit skills')){
                $buttons .='<button type="button" title="Edit" class="btn ml-1 btn-icon btn-info btn-sm" onclick="window.location.href=\''. route('admin.skills.edit', $skill->uuid) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> ';
                }
                if(auth()->user()->hasPermissionTo('delete skills')){
                $buttons .='<button type="button" title="Delete" class="btn btn-icon btn-danger btn-sm" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$skill->uuid.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form style="display:none" id="delete-form'.$skill->uuid.'" method="post" action="'.route("admin.skills.destroy", $skill->uuid).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }
                if($skill->active == 1){
                    $buttons .='<button type="button" title="Disable" class="btn btn-icon btn-warning btn-sm deactivate-btn ml-1" data-toggle="modal" data-target="#danger-alert-modal" onclick="deactivateForm(\'deactivate-form'.$skill->uuid.'\')"><i class="mdi mdi-check-circle-outline"></i> </button><form style="display:none" id="deactivate-form'.$skill->uuid.'" method="post" action="'.route("admin.skills.changeStatus", $skill->uuid).'"><input type="hidden" name="_method" value="put"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }else{
                    $buttons .='<button type="button" title="Enable" class="btn btn-icon btn-primary btn-sm deactivate-btn ml-1" data-toggle="modal" data-target="#danger-alert-modal" onclick="activateForm(\'active-form'.$skill->uuid.'\')"><i class="mdi mdi-close-circle"></i> </button><form style="display:none" id="active-form'.$skill->uuid.'" method="post" action="'.route("admin.skills.changeStatus", $skill->uuid).'"><input type="hidden" name="_method" value="put"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }
           return $buttons;

       }); 
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Skill $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Skill $model)
    {
        $model = Skill::orderby('created_at','desc')->get();
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Skill',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> 'Sl#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'name','title' => 'Skill'];
        $column_specifications[] = ['data' => 'category','title' => 'Categories'];
        $column_specifications[] = ['data' => 'subcategory','title' => 'Subcategories'];
        return $column_specifications;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
