<?php

namespace App\DataTables;

use App\User;
use App\Invoice;
use App\UserSubscription;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UserSubscriptionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->editColumn('id', function ($invoice) {
                return getInvoice($invoice->id);
            })
            ->editColumn('user_id', function ($invoice) {
                if(empty($invoice->user->user_type)){
                    return '<a class="user-details" onclick="window.location.href=\''. route('admin.users.show', $invoice->user->uuid) .'\'">'.ucwords($invoice->user->full_name).'</a>';
                }else{
                    return '<a class="user-details" onclick="window.location.href=\''. route('admin.freelancers.show', $invoice->user->uuid) .'\'">'.ucwords($invoice->user->full_name).'</a>';
                }
            })
            ->editColumn('amount', function ($invoice) {
                return formatAmount($invoice->amount);
            })
            ->editColumn('created_at', function ($invoice) {
                return hcms_date(strtotime($invoice->created_at), 'date-time', false);
            })
            ->addColumn('user_type', function ($invoice) {
                if(empty($invoice->user->user_type))
                    return "Client";
                else
                    return "Freelancer";
            })
            ->editColumn('status', function ($invoice) {
                if(empty($invoice->status))
                return "Pending";
                elseif($invoice->status == 1)
                return "Paid";
                else
                return "Rejected";
            })
            ->addColumn('membership_plan_id', function ($invoice) {
                if(!empty($invoice->userSubscription->membership->name))
                return '<a class="user-details" onclick="window.location.href=\''. route('admin.membership_plans.show', $invoice->userSubscription->membership->uuid) .'\'">'.ucwords($invoice->userSubscription->membership->name).'</a>';
                else
                 return "NA";
            })

            ->addColumn('duration_type', function ($invoice) {
                if(!empty($invoice->userSubscription->duration_type))
                 return "Annual";
                else
                 return "Monthly";
            })
            
            ->filter(function ($instance)   {
                
                    $condition = [];


                    if(request('search') && request('search')['value']??false){
                        $search = request('search')['value'];
                        $split = explode("IN",$search);
                        if(!empty($split[1]))
                        $instance->where('id',$split[1]);
                    }

                    if(request('user_type_id') != ''){
                        $instance = $instance->whereHas('user', function($q){
                            $q->where('users.user_type', request('user_type_id'));
                       });
                    }

                    if(request('mem_plan') != ''){
                        $instance = $instance->whereHas('userSubscription', function($q){
                             $q->where('user_subscriptions.membership_plan_id', request('mem_plan'));
                        });
                    }
                    
                    if(request('daterange') != ''){
                        $range = explode('-',request('daterange'));
                        $from_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[0]))));
                        $to_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[1]))));
                        if ($from_date != '') {
                            $condition[] = ['invoices.created_at',">=",$from_date];
                        } 
                        if ($to_date != '') {
                            $condition[] = ['invoices.created_at',"<=",$to_date];
                        } 
                    }
                    
                     $instance->where($condition);
                }
            )
            
            ->addColumn('action', function ($invoice) {
              
                $buttons = "";
                    if(auth()->user()->hasPermissionTo('view subscription payments')){
                        $buttons .= '<button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href=\''. route('admin.user_subscriptions.show', $invoice->uuid) .'\'"><i class="mdi mdi-eye"></i> </button>';
                    }
                   
                    
               return $buttons;
    
           })

            
            ->rawColumns(['membership_plan_id','user_id','action']); 
    } 
  
    /**
     * Get query source of dataTable.
     *
     * @param \App\UserSubscription $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Invoice $model)
    {
         $model = Invoice::with(['userSubscription','user'=>function($q){
                $q->select('id','user_type','full_name','uuid');
         }])->where('invoices.type',5);
    
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->processing(true)
        ->language ([
            'searchPlaceholder' => 'Invoice Number',
            'loadingRecords' => '&nbsp;',
            'processing'=> '<button class="btn btn-primary" type="button" disabled="">
                                <span class="spinner-border spinner-border-sm mr-1" role="status" aria-hidden="true"></span>
                                    Loading...
                            </button>',
        ]) 
        ->ajax([
            'url' => '',
            'type' => 'GET',
            'data' => 'function(d) { 
                d.daterange = $("#daterange_id").val(); 
                d.mem_plan = $("#mem_plan_id").val(); 
                d.user_type_id = $("#user_type_id").val(); 
            }',
        ]) 
        ->addAction(['title' => 'Actions'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $column_specifications = [];
        $column_specifications[] =  ['data' => 'updated_at','title' => 'Updated At','orderable'=> true,'searchable'=> false,'visible'=> false];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '#','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'id','title' => 'Invoice Number'];
        $column_specifications[] = ['data' => 'user_id','title' => 'Paid By'];
        $column_specifications[] = ['data' => 'user_type','title' => 'User Type'];
        // $column_specifications[] = ['data' => 'plan_name','title' => 'Subscription Plan'];

        $column_specifications[] = ['data' => 'membership_plan_id','name'=>'user_subscriptions.membership_plan_id','title' => 'Subscription Plan'];
        $column_specifications[] = ['data' => 'duration_type','name'=>'user_subscriptions.duration_type','title' => 'Subscription Type'];
        $column_specifications[] = ['data' => 'status','title' => 'Status'];
        $column_specifications[] = ['data' => 'amount','title' => 'Amount'];
        
        $column_specifications[] = ['data' => 'created_at','title' => 'Invoiced On'];
        return $column_specifications;
    }

    protected function getBuilderParameters()
    {
        return [
            'dom' => 'Bflrtip',
            'responsive'=>false,
            'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',
            'buttons'=> [],
            'order' => [0,'desc'],
           ];
    }
}
