<?php

namespace App;

use App\Models\Traits\General\Uuid;
use App\Services\ProcessEmailQueue;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use App\Notifications\AdminResetPassword;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Yadahan\AuthenticationLog\AuthenticationLogable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Admin extends Authenticatable
{
   
    use Notifiable, AuthenticationLogable,HasRoles,Uuid,LogsActivity;

    protected $guard_name = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'provider', 'provider_id','password','username','mobile_number','profile_image','active','google_id', 'country_code'
    ];

    /**
     * for setting google login
     *Rest omitted for brevity
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        // Send Email and save to email queue
        $admin_reset_password_template = EmailTemplate::where('static_email_heading','ADMIN_RESET_PASSWORD')->first();
        if ($admin_reset_password_template) {
            $email_html = $admin_reset_password_template->template;
            $email_html = str_replace("[ADMIN_NAME]", $this->name, $email_html);
            $email_html = str_replace("[RESET_TOKEN]", route('admin.password.reset',$token), $email_html);
            $template = view('admin.email_templates.layout', compact('email_html'));
            $data = [];
            $data['to_email'] = $this->email;
            $data['to_name'] = $this->name;
            $data['subject'] = $admin_reset_password_template->subject;
            $data['body'] = $template;
            $data['action_id'] = $this->id;
            $data['action_type'] = 'admin_reset_password';
            $processEmailQueue=new ProcessEmailQueue($data);
            $processEmailQueue->sendEmail();
        }
    }

    public function getFullNumberAttribute(){
        return '+'.$this->country_code.$this->mobile_number;
    }
    

    /**
     *  Activity Log
     */
            
    protected static $logAttributes = ['name'];
    protected static $logName = 'admin-user';
    protected static $logOnlyDirty = true;

    /**
     * Set Activit Description
     */
    public function getDescriptionForEvent(string $eventName): string
    {
        if ($eventName == 'created') {
            return 'Admin "' . $this->name . '" was created';
        }
        if ($eventName == 'updated') {
            return 'Admin "' . $this->name . '" was updated';
        }
        if ($eventName == 'deleted') {
            return 'Admin "' . $this->name . '" was deleted';
        }
        return $eventName;
    }
}
