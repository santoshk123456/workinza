<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTerm extends Model
{
    protected $fillable = [
        'user_id', 'terms_and_condition_id', 'version', 'accepted_at'
    ];

    public function termsAndConditions(){
        return $this->belongsTo(TermsAndCondition::class,'terms_and_condition_id');
    }
}
