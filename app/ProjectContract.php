<?php

namespace App;
use App\Models\Traits\General\Uuid;
use App\Services\ProcessEmailQueue;
use Illuminate\Database\Eloquent\Model;

class ProjectContract extends Model
{
    use Uuid;
    protected $fillable = ['project_id','project_proposal_id','freelancer_service_charge','client_id','freelancer_id','amount','status','freelancer_acceptance','client_acceptance'];

    public function getRouteKeyName()
    {
        return 'uuid';
    }
    public function user(){
       
        return $this->belongsTo(User::class,'freelancer_id')->withTrashed();
    }
    public function client(){
       
        return $this->belongsTo(User::class,'client_id')->withTrashed();
    }
    public function project(){
       
        return $this->belongsTo(Project::class,'project_id')->withTrashed();
    }
   
    public function getContractIdAttribute(){
        return 'C'.$this->id;
    }
    /**
     * sendEmailToFreelancer
     * Params   : User,request,proposal_details
     * Response : return array
     * STEP 1   : Get the freelancer details
     * STEP 2   : Fetch the email template for sending notification to freelancer
     * STEP 3   : Replace the values
     * STEP 4   : Insert an entry in the email queue
     * STEP 5   : Send mail
     * STEP 6   : Return proper response   
     */

    public static function sendEmailNotification($user,$request,$proposal_details,$user_type,$contract){
        $data = [];
        if($user_type == 0){
            $user_details = User::select('email','username')->where('id',$proposal_details->user_id)->first();
            $email_template = EmailTemplate::where('static_email_heading','CONTRACT_INVITATIONS')->first();
            $data['action_type'] = 'contract_invitations';
        }else{
            $user_details = User::select('email','username')->where('id',$proposal_details->project->user_id)->first();
            $email_template = EmailTemplate::where('static_email_heading','ACCEPT_CONTRACT')->first();
            $data['action_type'] = 'accept_contract';
        }
        $contract_link = route('project.prepareContract',$contract);
      
        if ($email_template) {
            $email_html = $email_template->template;
            if($user_type == 0){
                $email_html = str_replace("[USER_NAME]", ucwords($user_details->username), $email_html);
                $email_html = str_replace("[FROM_NAME]", ucwords($user->username), $email_html);
            }else{
                $email_html = str_replace("[USER_NAME]", ucwords($user_details->username), $email_html);
                $email_html = str_replace("[FROM_NAME]", ucwords($user->username), $email_html);
            }
            $email_html = str_replace("[PROJECT_NAME]", $proposal_details->project->title, $email_html);
            $email_html = str_replace("[CONTRACT_LINK]", $contract_link, $email_html);
            

            $template = view('admin.email_templates.layout', compact('email_html'));
           
            $data['to_email'] = $user_details->email;
            $data['to_name'] = ucwords($user_details->username);
            $data['subject'] = $email_template->subject;
            $data['body'] = $template;
            $data['action_id'] = $user->id;
           
            $processEmailQueue = new ProcessEmailQueue($data);
            $processEmailQueue->sendEmail();
            return true;
        }
    }
}
