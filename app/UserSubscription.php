<?php

namespace App;

use App\Invoice;
use App\MembershipPlan;
use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $guarded = ['id'];

    protected $fillable = ['user_id','membership_plan_id','start_date','remaining_project_count','remaining_proposal_count','end_date','duration_type','amount','setup_charge','service_percentage','auto_renewal','product_commission','active'];

    public function membership(){
        return $this->belongsTo(MembershipPlan::class,'membership_plan_id','id');
    }

    public function invoice(){
        return $this->hasMany(Invoice::class,'user_subscription_id');
    }
}
