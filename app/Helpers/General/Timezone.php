<?php

namespace App\Helpers\General;

use Carbon\Carbon;

/**
 * Class Timezone.
 */
class Timezone
{
    /**
     * @param Carbon $date
     * @param string $format
     *
     * @return Carbon
     */
    public function convertToLocal(Carbon $date, $format = 'D M j G:i:s T Y', $timezone = 'UTC') : string
    {
        return $date->setTimezone($timezone ?? config('app.timezone'))->format($format);
    }

    /**
     * @param $date
     *
     * @return Carbon
     */
    public function convertFromLocal($date) : Carbon
    {
        return Carbon::parse($date, auth()->user()->timezone)->setTimezone('UTC');
    }

    public function setDateTimeFormat()
    {
        return "d-m-Y h:i A";
    }
    public function setShortDateTimeFormat()
    {
        return "d-M-y h:i A";
    }
    public function setDateFormat()
    {
        return "d-m-Y";
    }
    public function setShortDateFormat()
    {
        return "d-M-y";
    }
}
