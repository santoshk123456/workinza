<?php

namespace App\Libraries\PaymentGateways;

use App\Organization;
use App\Organizations;
use App\PaymentGateway;
use App\OrganizationPaymentGateway;
use App\Libraries\PaymentGateways\Stripe;


class PaymentGatewaysController{

    public $authkey = "";
    public $authtoken = "";
    public $payment_gateway_id = "";
   
    /**
     * Authenticate the payment gateway
     * STEP 1: Identify which the payment gateway is enabled on DB
     * STEP 2: Get the credentials
     */
    //$this->authkey = "";
    public function initiatePaymentGateway(){
        $payment_gateway_details = PaymentGateway::where('status',1)->first();
        
        if(isset($payment_gateway_details)){
            
            $this->authkey = $payment_gateway_details->authentication_key;
            $this->authtoken = $payment_gateway_details->authentication_token;
            $this->payment_gateway_id  = $payment_gateway_details->id;
            
            // Stripe
            if($this->payment_gateway_id == 1){
                $this->stripeObj = new Stripe($this->authtoken);
            }
            return true;

        }else{
            return false;
        }
    }
    
    public function createPlan($plan){
        if($this->payment_gateway_id == 1){ 
            $plan_details = $this->stripeObj->createPlan($plan);
            return $plan_details;
        }
    }

    public function createCustomer($customer){
        if($this->payment_gateway_id == 1){
            $customer_details = $this->stripeObj->createCustomer($customer);
            return $customer_details;
        }
    }
    public function chargeFromCustomerId($charge){
        if($this->payment_gateway_id == 1){
            $charge_details = $this->stripeObj->chargeFromCustomerId($charge);
            return $charge_details;
        }else if($this->payment_gateway_id == 2){
            $charge_details = $this->paypalObj->chargeFromCustomerId($charge);
            return $charge_details;
        }
    }
    public function attachCard($card){
        if($this->payment_gateway_id == 1){
            $charge_details = $this->stripeObj->attachCard($card);


             if($charge_details['card']['brand'] == "MasterCard"){
                $charge_details['card']['brand'] = 0;
             }elseif($charge_details['card']['brand'] == "Visa"){
                $charge_details['card']['brand'] = 1;
             }else{
                $charge_details['card']['brand'] = 1;
             }

            

            return $charge_details;
        }else if($this->payment_gateway_id == 2){
            $charge_details = $this->paypalObj->attachCard($card);
            return $charge_details;
        }
    }
    public function createSubscription($subscription){
        if($this->payment_gateway_id == 1){
            $subscription_details = $this->stripeObj->createSubscription($subscription);
            return $subscription_details;
        }
    }
    public function createScheduleSubscription($subscription){
        if($this->payment_gateway_id == 1){
            $subscription_details = $this->stripeObj->createScheduleSubscription($subscription);
            return $subscription_details;
        }
    }
    public function cancelSubscription($subscription){
        if($this->payment_gateway_id == 1){
            $cancel_subscription_details = $this->stripeObj->cancelSubscription($subscription);
            return $cancel_subscription_details;
        }
    }

    /**
    *
    * make transaction
    *
    */
    public function makeTransaction($request){

       // Stripe integration

      if($this->payment_gateway_id == 1){
         
         $transaction = $this->stripeObj->makeTransaction($request);
        return $transaction;

      }
    }

    public function createCheckoutSession($request){
        if($this->payment_gateway_id == 1){
            $sessionToken = $this->stripeObj->createCheckoutSession($request);
            return $sessionToken;
        }
    }
    
    

}



?>