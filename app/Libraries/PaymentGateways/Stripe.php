<?php

namespace App\Libraries\PaymentGateways;

use Stripe\StripeClient;


class Stripe{
	public $stripe = "";
	/**
	 * Constructor 
	 * Param : authentication keys
	 * Description : For authenticate the stripe using authentication keys
	 */
    public function __construct($authtoken){
      $this->stripe = new \Stripe\StripeClient(
        $authtoken
      );
    }
    /**
	 * createPlan()
	 * Params : plan array
	 * response : $return array with success and plan field
	 */
    public function createPlan($data){
	
		$return_arr = [];
		$paln = [];
		try{

           if($data['interval'] == '0'){
			
           	 $plan = $this->stripe->plans->create([
				'amount' => $data['amount']*100,
				'currency' =>  'usd',
				'interval' =>  'month',
				'product' => ['name' =>  $data['name'] ],
			]); 
           }

           if($data['interval'] == '1'){

           	 $plan = $this->stripe->plans->create([
				'amount' => $data['amount']*100,
				'currency' =>  'usd',
				'interval' =>  'month',
				'interval_count'=>'3',
				'product' => ['name' =>  $data['name'] ],
			]); 
           }

           if($data['interval'] == '2'){

           	 $plan = $this->stripe->plans->create([
				'amount' => $data['amount']*100,
				'currency' =>  'usd',
				'interval' =>  'year',
				'product' => ['name' =>  $data['name'] ],
			]); 
           }

			
		}catch(\Stripe\Exception\CardException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\RateLimitException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\InvalidRequestException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\AuthenticationException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiConnectionException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (Exception $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		}
		

		if(isset($plan['id']) && !empty($plan['id'])){
			$return_arr['success'] = 1;
			$return_arr['plan'] = $plan;
		}else{
			$return_arr['success'] = 0;
			$return_arr['plan'] = "";
		}
		return $return_arr;
	}
	 /**
	 * createCustomer()
	 * Params : customer array
	 * response : $return array with success and customer field
	 */
	public function createCustomer($customer){
		

		$return_arr = [];
		try{ 
			$customer = $this->stripe->customers->create([
				'email' => $customer['email'],
				'name' => $customer['name'],
				'description' => $customer['description'],
				'address' => $customer['address']
			  ]);
		}catch(\Stripe\Exception\CardException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\RateLimitException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\InvalidRequestException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\AuthenticationException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiConnectionException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (Exception $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		}
	
		if(isset($customer['id']) && !empty($customer['id'])){
			$return_arr['success'] = 1;
			$return_arr['customer'] = $customer;
		}else{
			$return_arr['success'] = 0;
			$return_arr['customer'] = "";
		}
		return $return_arr;
    }
    /**
	 * chargeFromCustomerId()
	 * Params : charge array
	 * response : $return array with success and charge field
	 */
	public function chargeFromCustomerId($charge){
		$return_arr = [];
		try{ 
			$charge_details = $this->stripe->charges->create([
				'amount' => $charge['amount'],
				'currency' => $charge['currency'],
				'customer' => $charge['customer'],
			]);
		}catch(\Stripe\Exception\CardException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		  } catch (\Stripe\Exception\RateLimitException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		  } catch (\Stripe\Exception\InvalidRequestException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		  } catch (\Stripe\Exception\AuthenticationException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		  } catch (\Stripe\Exception\ApiConnectionException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		  } catch (\Stripe\Exception\ApiErrorException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		  } catch (Exception $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		  }
	
		if(isset($charge_details['id']) && !empty($charge_details['id'])){
			$return_arr['success'] = 1;
			$return_arr['charge'] = $charge_details;
		}else{
			$return_arr['success'] = 0;
			$return_arr['charge'] = "";
		
		}
		return $return_arr;
	}
	/**
	 * attachCard()
	 * Params : charge array
	 * response : $return array with success and card field
	 */
	public function attachCard($card){
		$return_arr = [];

		try{ 
			$attach_card_details = $this->stripe->customers->createSource(
				$card['customer'],
				// ['source'=>'tok_mastercard'],
				['source'=>$card['card_token']],

			  );
		}catch(\Stripe\Exception\CardException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\RateLimitException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\InvalidRequestException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\AuthenticationException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiConnectionException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (Exception $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		}
		if(isset($attach_card_details['id']) && !empty($attach_card_details['id'])){
			$return_arr['success'] = 1;
			$return_arr['card'] = $attach_card_details;
		}else{
			$return_arr['success'] = 0;
			$return_arr['card'] = "";
		
		}
		// dd($attach_card_details);
		return $return_arr;
	}
	/**
	 * createSubscription()
	 * Params : subscription array
	 * response : $return array with success and subscription field
	 */
	public function createSubscription($subscription){
		$return_arr = [];
		try{ 
			$subscription_details = $this->stripe->subscriptions->create([
				'customer' => $subscription['customer'],
				'items' => [['plan' => $subscription['plan_id']]],
			  ]);
		}catch(\Stripe\Exception\CardException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\RateLimitException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\InvalidRequestException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\AuthenticationException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiConnectionException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (Exception $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		}
		if(isset($subscription_details['id']) && !empty($subscription_details['id'])){
			$return_arr['success'] = 1;
			$return_arr['subscription'] = $subscription_details;
		}else{
			$return_arr['success'] = 0;
			$return_arr['subscription'] = "";
		
		}
		return $return_arr;
	}
	/**
	 * createScheduleSubscription()
	 * Params : subscription array
	 * response : $return array with success and subscription field
	 */
	public function createScheduleSubscription($schedule){
		$return_arr = [];
		try{ 
			$param_arr = [];
			$param_arr['plans']['plan'] = $schedule['plan_id'];

			if(isset($schedule['iterations']) && !empty($schedule['iterations'])){
				$param_arr['iterations'] = $schedule['iterations'];
			}
			if(isset($schedule['end_date']) && !empty($schedule['end_date'])){
				$param_arr['end_date'] = $schedule['end_date'];
			}
			$schedule_subscription_details = $this->stripe->subscriptionSchedules->create([
				'customer' => $schedule['customer'],
				'start_date' => $schedule['start_date'],
				'end_behavior' =>$schedule['start_date'],
				'phases' => [
					$param_arr
				],
			  ]);
		}catch(\Stripe\Exception\CardException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\RateLimitException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\InvalidRequestException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\AuthenticationException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiConnectionException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (Exception $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		}
		if(isset($schedule_subscription_details['id']) && !empty($schedule_subscription_details['id'])){
			$return_arr['success'] = 1;
			$return_arr['schedule_subscription'] = $schedule_subscription_details;
		}else{
			$return_arr['success'] = 0;
			$return_arr['schedule_subscription'] = "";
		
		}
		return $return_arr;
	}
	/**
	 * cancelSubscription()
	 * Params : subscription array
	 * response : $return array with success and subscription field
	 */
	public function cancelSubscription($subscription){
		$return_arr = [];
		try{ 
			if($subscription['schedule'] == 1){
				$cancel_subscription_details = $this->stripe->subscriptionSchedules->cancel(
					$subscription['subscription_id'],
					[]
				  );
			}else{
				$cancel_subscription_details = $this->stripe->subscriptions->cancel(
					$subscription['subscription_id'],
					[]
				  );
			}
			
		}catch(\Stripe\Exception\CardException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\RateLimitException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\InvalidRequestException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\AuthenticationException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiConnectionException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (Exception $e) {
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		}
		
		if(isset($cancel_subscription_details['id']) && !empty($cancel_subscription_details['id'])){
			$return_arr['success'] = 1;
			$return_arr['cancel_subscription'] = $cancel_subscription_details;
		}else{
			$return_arr['success'] = 0;
			$return_arr['cancel_subscription'] = "";
		
		}
		return $return_arr;
	}

	public function createCheckoutSession($request){
		
		$return_arr = [];
		if($request['subscription'] == 1){
			$line_items = [[
				'price' =>$request['plan_id'],
				'quantity' => 1
			]];
			
			if (isset($request['extra_charge']) && $request['extra_charge'] > 0) {
				$line_items[] = ['quantity' => 1, 'price_data' => ['unit_amount' => $request['extra_charge']*100, 'currency' => 'usd', 'product_data'=>['name'=>"Keyoxa Setup Charge",'description'=>"Keyoxa Setup charge"]]];
			}
			
			$session =  $this->stripe->checkout->sessions->create([
				'success_url' => $request['success_url'],
				'cancel_url' => $request['cancel_url'],
				'customer_email' => $request['email'],
				'payment_method_types' => ['card'],
				'line_items' => $line_items,
				'mode' => 'subscription',
			  ]);

		}else{
			$session =  $this->stripe->checkout->sessions->create([
				'success_url' => $request['success_url'],
				'cancel_url' => $request['cancel_url'],
				'customer_email' => $request['email'],
				'payment_method_types' => ['card'],
				'line_items' => [
				  [
					'name' => $request['name'],
					'description' => $request['description'],
					'amount' => $request['charge'] * 100,
					'currency' => 'USD',
					'quantity' => 1,
					
					
				  ],
				],
				'mode' => 'payment',
				'payment_intent_data' => [
					
					'metadata' => [
					  'milestone' => $request['milestone'],
					],
				  ],
				
			  ]);
		}
		
		return $session;
	}

    /**
    *
    * Make transaction
    *
    */
    public function makeTransaction($data){
      

      $amount = (int)$data['amount'];
      
      if($data['currency'] =="usd"){

          $amount = $amount*100;
      } 

      try{
         
        $response = $this->stripe->charges->create([
		  'amount' =>$amount,
		  'currency' => $data['currency'],
		  'customer' => $data['customer_id'],
	    ]);

        $return_arr['success'] = "1";  

        $return_arr['response'] =$response; 

      }catch(\Stripe\Exception\CardException $e) {
      	    $return_arr['success'] = "0";
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\RateLimitException $e) {
			$return_arr['success'] = "0";
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\InvalidRequestException $e) {
			$return_arr['success'] = "0";
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\AuthenticationException $e) {
			$return_arr['success'] = "0";
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiConnectionException $e) {
			$return_arr['success'] = "0";
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (\Stripe\Exception\ApiErrorException $e) {
			$return_arr['success'] = "0";
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} catch (Exception $e) {
			$return_arr['success'] = "0";
			$return_arr['error_code'] = $e->getError()->code;
			$return_arr['error_msg']  = $e->getError()->message;
		} 
      
      if($return_arr['success']=="1"){
           
         $return_arr['transaction_id'] = $return_arr['response']['id'];
         
         return $return_arr;

      }else{

      	  return $return_arr;
      }
         
    }


}



?>