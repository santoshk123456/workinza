<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMilestone extends Model
{
    protected $guarded = ['id'];
    public function project(){
       
        return $this->belongsTo(Project::class,'project_id')->withTrashed();
    }
}
