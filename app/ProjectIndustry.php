<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectIndustry extends Model
{
    protected $guarded = [];
    protected $table = "project_industry";
}
