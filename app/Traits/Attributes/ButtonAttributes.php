<?php

namespace App\Traits\Attributes;

use Illuminate\Support\Facades\Hash;
use App\Models\Hotel;
use App\Repositories\Admin\Hotel\HotelRepository;
use App\Models\Auth\User;

/**
 * Trait ButtonAttributes.
 */
trait ButtonAttributes
{

/**
 *  These Button Attributes Are Used In The Superadmin Side
 */

    // Admin Users
   
    /**
    * @return string
    */
    public function RoleUseredit($route)
    {
        $role_name= $this->roles->pluck('name')->first();
       
        return '<button data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.edit').'" onclick="window.location.href=\''.route($route, [$this,$role_name]).'\'" class="btn btn-icons btn-rounded btn-warning " data-original-title="'.__('buttons.title.edit').'"><i class="fas fa-pencil-alt"></i></button>';
    }

    /**
    * @return string
    */
    public function RoleUserview($route)
    {
        $role_name= $this->roles->pluck('name')->first();

        return '<button data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.view').'" onclick="window.location.href=\''.route($route, [$this,$role_name]).'\'" class="btn btn-icons btn-rounded btn-primary mr-1" data-original-title="View"><i class="fa fa-eye"></i></button>';
    }


    /**
    * @return string
    */
    public function edit($route)
    {
        return '<button data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.edit').'" onclick="window.location.href=\''.route($route, $this).'\'" class="btn btn-icons btn-rounded btn-warning " data-original-title="'.__('buttons.title.edit').'"><i class="fas fa-pencil-alt"></i></button>';
    }

    /**
    * @return string
    */
    public function view($route)
    {
        return '<button data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.view').'" onclick="window.location.href=\''.route($route, $this).'\'" class="btn btn-icons btn-rounded btn-primary mr-1" data-original-title="View"><i class="fa fa-eye"></i></button>';
    }


    /**
    * @return string
    */
    public function login($route)
    {
        return '<button data-toggle="tooltip" data-placement="top" title="'.__('Login as Hotel Admin').'" onclick="window.location.href=\''.route($route).'\'" class="btn btn-icons btn-rounded btn-info mr-1   " data-original-title="Login as Hotel Admin"><i class="fa fa-sign-in"></i></button>';
    }
    
    /**
    * @return string
    */
    public function deleteButton($route, $name)
    {
        return '<a href="'.route($route, $this).'"
        data-method="delete" data-id="delete" name="delete_item">Delete</a>';
    }

    /**
    * @return string
    */
    public function deactivate($route, $name)
    {
        return '<a href="'.route($route, $this).'"
        data-method="patch"  data-id="deactivate" name="deactivate_item">Deactivate</a>';
    }

    /**
    * @return string
    */
    public function resendMail($route)
    {
        return '<a href="'.route($route, $this).'"
        data-method="post"  data-id="resend" name="resend_mail">Resend Mail</a>';
    }


    /**
    * @return string
    */
    public function permenantDeleteButton($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete" class="ml-2">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token"  value="'.csrf_token().'">
                    <button type="submit" data-toggle="tooltip" data-placement="top" 
                        data-method="delete"
                        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
                        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
                        data-trans-title="'.__('alerts.confirm.permanent_delete').'?"
                        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
                </form>';
    }


   
   
    /**
    * @return string
    */
    public function activate($route, $name)
    {
        return '<form action="'.route($route, $this).'" method="post" name="activate"  >
        <input type="hidden" name="_method" value="patch">
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="patch"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="'.__('alerts.confirm.activate').'?"
            title="'.__('buttons.title.activate').'" class="btn btn-icons btn-rounded btn-success" data-original-title="'.__('buttons.title.activate').'"><i class="fa fa-check"></i></button>

    </form>';
    }

    /**
    *
    *@return string
    */
   
    public function restoreButton($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="restoreUser"  >
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="post"
            data-trans-title="'.__('alerts.confirm.restore').'?"
            title="'.__('buttons.title.restore').'" class="btn btn-icons btn-rounded btn-success" data-original-title="'.__('buttons.title.restore').'"><i class="fas fa-redo"></i></button>
      </form>';
    }
    /**
    * @return string
    *
    */
    public function notavailableButton()
    {
        return '<button data-toggle="tooltip" data-placement="top" title="Not available"  class="btn btn-icons btn-rounded btn-danger " data-original-title="Not available"><i class="fa fa-ban"></i></button>';
    }
     
    // Admin Roles
    
    /**
      * @return string
      */
    public function editRole($route)
    {
        return '<button data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.edit').'" onclick="window.location.href=\''.route($route, $this).'\'" class="btn btn-icons btn-rounded btn-warning " data-original-title="'.__('buttons.title.edit').'"><i class="fas fa-pencil-alt"></i></button>';
    }

    /**
    * @return string
    *
    */
    public function deleteRole($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete" style="display:inline-flex" class="mt-2">
    <input type="hidden" name="_method" value="delete">
    <input type="hidden" name="_token"  value="'.csrf_token().'">
    <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="delete"
        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
        data-trans-title="'.__('alerts.confirm.title').'?"
        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
    </form>';
    }
    
    //Hotel Users
    
    /**
    * @return string
    */
    public function editHotelUser($route)
    {
        $hotel=Hotel::find($this->pivot->hotel_id);

        return '<button data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.edit').'" onclick="window.location.href=\''.route($route, [$hotel->uuid , $this->uuid]).'\'" class="btn btn-icons btn-rounded btn-warning " data-original-title="'.__('buttons.title.edit').'"><i class="fas fa-pencil-alt"></i></button>';
    }

    /**
    * @return string
    */
    public function viewHotelUser($route)
    {
        $hotel=Hotel::find($this->pivot->hotel_id);

        return '<button data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.view').'" onclick="window.location.href=\''.route($route, [$hotel->uuid , $this->uuid]).'\'" class="btn btn-icons btn-rounded btn-primary mr-1" data-original-title="View"><i class="fa fa-eye"></i></button>';
    }

    /**
    * @return string
    */
    public function deactivateHotelUser($route)
    {
        $hotel=Hotel::find($this->pivot->hotel_id);
        return '<a href="'.route($route, [$hotel->uuid , $this->uuid]).'"
      data-method="patch"  data-id="deactivate" name="deactivate_item">Deactivate</a>';
    }
    /**
    * @return string
    */
    public function activateHotelUser($route, $name)
    {
        $hotel=Hotel::find($this->pivot->hotel_id);
        
        return '<form action="'.route($route, [$hotel->uuid , $this->uuid]).'" method="post" name="activate"  >
        <input type="hidden" name="_method" value="patch">
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="patch"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="'.__('alerts.confirm.activate').'?"
            title="'.__('buttons.title.activate').'" class="btn btn-icons btn-rounded btn-success" data-original-title="'.__('buttons.title.activate').'"><i class="fa fa-check"></i></button>

    </form>';
    }
    /**
    * @return string
    */
    public function hotelUserDelete($route, $name)
    {
        $hotel=Hotel::find($this->pivot->hotel_id);

        return '<a href="'.route($route, [$hotel->uuid , $this->uuid]).'"
                data-method="delete" data-id="delete" name="delete_item">Delete</a>';
    }
    /**
    * @return string
    */
    public function deactivatedHotelUserDelete($route, $name)
    {
        $hotel=Hotel::find($this->pivot->hotel_id);

        return '<form action="'.route($route, [$hotel->uuid , $this->uuid]).'" method="post" name="delete" class="ml-2">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token"  value="'.csrf_token().'">
                    <button type="submit" data-toggle="tooltip" data-placement="top" 
                        data-method="delete"
                        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
                        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
                        data-trans-title="'.__('alerts.confirm.title').'?"
                        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
                </form>';
    }


    /**
    *
    *@return string
    */
   
    public function hotelUserRestore($route, $name)
    {
        $hotel=Hotel::find($this->pivot->hotel_id);
        
        return '<form action="'.route($route, [$hotel , $this->uuid]).'" method="post" name="restoreUser"  >
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="post"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="'.__('alerts.confirm.restore').'?"
            title="'.__('buttons.title.restore').'" class="btn btn-icons btn-rounded btn-success" data-original-title="'.__('buttons.title.restore').'"><i class="fas fa-redo"></i></button>

    </form>';
    }
    /**
    * @return string
    */
    public function hoteUserPermenantDelete($route, $name)
    {
        $hotel=Hotel::find($this->pivot->hotel_id);
 
        return '<form action="'.route($route, [$hotel->uuid , $this->uuid]).'" method="post" name="delete" class="ml-2">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token"  value="'.csrf_token().'">
                    <button type="submit" data-toggle="tooltip" data-placement="top" 
                        data-method="delete"
                        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
                        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
                        data-trans-title="'.__('alerts.confirm.permanent_delete').'?"
                        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
                </form>';
    }

   
    /**
    * @return string
    */
    public function deleteDeactivatedHotelButton($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete" class="ml-2">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token"  value="'.csrf_token().'">
                    <button type="submit" data-toggle="tooltip" data-placement="top" 
                        data-method="delete"
                        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
                        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
                        data-trans-title="'.__('alerts.confirm.title').'?"
                        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
                </form>';
    }
    /**
    * @return string
    */
    public function groupLoginPermenantDeleteButton($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete" class=" mt-2">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token"  value="'.csrf_token().'">
                    <button type="submit" data-toggle="tooltip" data-placement="top" 
                        data-method="delete"
                        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
                        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
                        data-trans-title="'.__('alerts.confirm.permanent_delete').'?"
                        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
                </form>';
    }


    /**
     * These Button Attributes Are Used In The Hoteladmin Side
     *
     */

    /**
     * @return string
     */
    public function hotelUserEdit($route)
    {
        return '<button data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.edit').'" onclick="window.location.href=\''.route($route, $this).'\'" class="btn btn-icons btn-rounded btn-warning " data-original-title="'.__('buttons.title.edit').'"><i class="fas fa-pencil-alt"></i></button>';
    }

    /**
    * @return string
    */
    public function hotelUserDeactivate($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="deactivate" class="mt-2 mr-2"  >
        <input type="hidden" name="_method" value="patch">
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="patch"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="'.__('alerts.confirm.deactivate').'?"
            title="'.__('buttons.title.deactivate').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.deactivate').'"><i class="fas fa-times"></i></button>

    </form>';
    }


    /**
    * @return string
    */
    public function hotelUserActivate($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="activate" class="mt-2 mr-2"  >
        <input type="hidden" name="_method" value="patch">
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="patch"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="'.__('alerts.confirm.activate').'?"
            title="'.__('buttons.title.activate').'" class="btn btn-icons btn-rounded btn-success" data-original-title="'.__('buttons.title.activate').'"><i class="fa fa-check"></i></button>

    </form>';
    }
   
    /**
    * @return string
    */
    public function hotelUserSoftDelete($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete" class="mt-2">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token"  value="'.csrf_token().'">
                    <button type="submit" data-toggle="tooltip" data-placement="top" 
                        data-method="delete"
                        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
                        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
                        data-trans-title="'.__('alerts.confirm.title').'?"
                        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
                </form>';
    }
    /**
    * @return string
    */
    public function hotelUserForceDelete($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token"  value="'.csrf_token().'">
                    <button type="submit" data-toggle="tooltip" data-placement="top" 
                        data-method="delete"
                        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
                        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
                        data-trans-title="'.__('alerts.confirm.permanent_delete').'?"
                        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
                </form>';
    }
   
    /**
    *
    *@return string
    */
   
    public function hotelUserRestoreButton($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="restoreUser" class="mr-2"  >
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="post"
            data-trans-title="'.__('alerts.confirm.restore').'?"
            title="'.__('buttons.title.restore').'" class="btn btn-icons btn-rounded btn-success" data-original-title="'.__('buttons.title.restore').'"><i class="fas fa-redo"></i></button>
      </form>';
    }
  
    /**
    *
    *@return string
    */
   
    public function loginAsHotelAdmin($route)
    {
        return '<a href="'.route($route, $this).'"
      data-method="post"  data-id="login_hoteluser" name="deactivate_item">Login As Hotel User</a>';
    }

    //Subscriptions
 
    /**
    * @return string
    */
    public function HotelSubscriptionupgrade($route)
    {
        $hotel=Hotel::find($this->hotel_id);
        return '<button data-toggle="tooltip" data-placement="top" title="Modify" onclick="window.location.href=\''.route($route, [$hotel->uuid,$this]).'\'" class="btn btn-icons btn-rounded btn-warning " data-original-title="'.__('buttons.title.edit').'"><i class="fas fa-angle-double-up"></i></button>';
    }
    /**
     * @return string
     */
    public function HotelSubscriptionrenewal($route)
    {
        $hotel=Hotel::find($this->hotel_id);

                 
        return '<form action="'.route($route, [$hotel->uuid , $this]).'" method="post" name="renewal" style="display: -webkit-inline-box;" class="mt-2">
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="post"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="Do you want subscription renewal ?"
            title="Renewal" class="btn btn-icons btn-rounded btn-success" data-original-title="Renewal"><i class="fas fa-redo-alt"></i></button>

    </form>';
    }

    /**
    * @return string
    */
    public function HotelSubscriptioncancel($route)
    {
        $hotel=Hotel::find($this->hotel_id);

                 
        return '<form action="'.route($route, [$hotel->uuid , $this]).'" method="post" name="delete" style="display: -webkit-inline-box;" class="mt-2 ml-2">
        <input type="hidden" name="_method" value="delete">
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
            data-method="delete"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="Do you want to cancel subscription ?"
            title="Cancel" class="btn btn-icons btn-rounded btn-danger" data-original-title="Cancel"><i class="fas fa-ban"></i></button>
    </form>';
    }

     
    /**
    * @return string
    */
    public function upgrade($route)
    {
        return '<button data-toggle="tooltip" data-placement="top" title="Modify" onclick="window.location.href=\''.route($route, $this).'\'" class="btn btn-icons btn-rounded btn-warning " data-original-title="'.__('buttons.title.edit').'"><i class="fas fa-angle-double-up"></i></button>';
    }
    /**
     * @return string
     */
    public function renewal($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="renewal" style="display: -webkit-inline-box;" class="mt-2">
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="post"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="Do you want subscription renewal ?"
            title="Renewal" class="btn btn-icons btn-rounded btn-success" data-original-title="Renewal"><i class="fas fa-redo-alt"></i></button>

    </form>';
    }

  
    /**
    * @return string
    */
    public function cancel($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete" style="display: -webkit-inline-box;">
        <input type="hidden" name="_method" value="delete">
        <input type="hidden" name="_token"  value="'.csrf_token().'">
        <button type="submit" style="margin-top: -11px;margin-left: -3px;"  data-toggle="tooltip" data-placement="top" 
            data-method="delete"
            data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
            data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
            data-trans-title="Do you want to cancel subscription ?"
            title="Cancel" class="btn btn-icons btn-rounded btn-danger" data-original-title="Cancel"><i class="fas fa-ban"></i></button>
    </form>';
    }

    /**
    * @return string
    */
    public function clearAllAnnouncements($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete">
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token"  value="'.csrf_token().'">
                    <button type="submit" data-toggle="tooltip" data-placement="top" 
                        data-method="delete"
                        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
                        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
                        data-trans-title="'.__('alerts.confirm.permanent_delete').'?"
                        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
                </form>';
    }

    /**
    * @return string
    *
    */
    public function deleteInvoice($route)
    {
        if ($this->auto==1) {
            $type="auto";
        } else {
            $type="manual";
        }
        
        return '<form action="'.route($route, [$this,$type]).'" method="post" name="delete" style="display:inline-flex" class="mt-2">
    <input type="hidden" name="_method" value="delete">
    <input type="hidden" name="_token"  value="'.csrf_token().'">
    <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="delete"
        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
        data-trans-button-confirm="'.__('alerts.confirm.confirm_button').'"
        data-trans-title="'.__('alerts.confirm.title').'?"
        title="'.__('buttons.title.delete').'" class="btn btn-icons btn-rounded btn-danger" data-original-title="'.__('buttons.title.delete').'"><i class="fa fa-trash"></i></button>
    </form>';
    }
    /**
    * @return string
    *
    */
    public function archiveAnnouncement5($route)
    {
        return '<form action="'.route($route, $this).'" method="post" name="delete" style="display:inline-flex" class="mt-2">
    <input type="hidden" name="_method" value="delete">
    <input type="hidden" name="_token"  value="'.csrf_token().'">
    <button type="submit" data-toggle="tooltip" data-placement="top" 
        data-method="delete"
        data-trans-button-cancel="'.__('alerts.confirm.cancel_button').'"
        data-trans-button-confirm="Do you want to archive?"
        data-trans-title="Do you want to archive?"
        data-trans-image="../../images/general/archive.png"
        title="Archive" class="btn btn-icons btn-rounded btn-danger" data-original-title="Archive"><i class="fas fa-archive"></i></button>
    </form>';
    }
}
