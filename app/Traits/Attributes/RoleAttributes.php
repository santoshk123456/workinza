<?php

namespace App\Traits\Attributes;

use App\Traits\Attributes\ButtonAttributes;

/**
 * Trait RoleAttributes.
 */
trait RoleAttributes
{
    use ButtonAttributes;

    /**
    * @return string
    */
    public function getActionButtonsAttribute()
    {
        if ($this->name == 'superadmin') {
            return '<label class="badge badge-info" data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.not_available').'" data-original-title="'.__('buttons.title.not_available').'">'.__('buttons.title.not_available').'</label>';
        }
        return '<div class="template-demo" >'.$this->editRole('admin.role.edit').$this->deleteRole('admin.role.destroy', 'name').'</div>';
    }

    /**
    * @return string
    */
    public function getActionRoleButtonsAttribute()
    {
        if ($this->name == 'superadmin') {
            return '<label class="badge badge-info" data-toggle="tooltip" data-placement="top" title="'.__('buttons.title.not_available').'" data-original-title="'.__('buttons.title.not_available').'">'.__('buttons.title.not_available').'</label>';
        }
        return '<div class="template-demo">'.$this->editRole('admin.hotel.role.edit').$this->deleteRole('admin.hotel.role.destroy', 'name').'</div>';
    }

    /**
    * @return string
    */
    public function getDisplayNameAttribute()
    {
        return ucfirst($this->name);
    }
}
