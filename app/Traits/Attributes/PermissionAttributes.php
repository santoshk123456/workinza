<?php

namespace App\Traits\Attributes;

/**
 * Trait PermissionAttribute.
 */
trait PermissionAttributes
{
    /**
    * @return string
    */
    public function getDisplayNameAttribute()
    {
        return ucfirst($this->name);
    }
}
