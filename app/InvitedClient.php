<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvitedClient extends Model
{
    protected $fillable = [
        'freelancer_id', 'user_name', 'country_code','email','mobile_number','joined_date','user_type','status','is_availed','client_id'
    ];
}
