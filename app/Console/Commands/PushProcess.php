<?php

namespace App\Console\Commands;

use App\PushNotificationQueue;
use Illuminate\Console\Command;

class PushProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'PushProcess:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notifications to android/ios devices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get all push notifications which are not proccessed yet
        $date = date('Y-m-d H:i:s');
        $pushNotificationQueues = PushNotificationQueue::where('sent_status',0)->orderBy('created_at','ASC')->limit(50)->get();
        if(count($pushNotificationQueues)){
            foreach ($pushNotificationQueues as $push) {
                $send_status = '';
                $count = PushNotificationQueue::where(['user_id'=>$push['user_id'],'sent_status'=>0])->count();
                if ($push['device_type'] == 'ios') {
                    $send_status = PushNotificationQueue::pushNotificationIphone($push['id'],$push['user_id'],$push['device_id'],$push['subject'],$push['message'],$push['action_type'],$push['action_id'],$push['sub_action_id'],$count);
                }elseif (empty($push['device_type'])) {
                    $send_status = PushNotificationQueue::pushNotificationAndroid($push['id'],$push['user_id'],$push['device_id'],$push['subject'],$push['message'],$push['action_type'],$push['action_id'],$push['sub_action_id'],$count);
                }
                echo('Push send status for ' . $push ['device_id']);
                if($push['device_type'] == 'ios'){
                    if ($send_status<=400) {
                        $push['sent_status'] = 1;
                    }
                }elseif(empty($push['device_type'])){
                    $sendstatus =json_decode($send_status);
                    if(isset($sendstatus->success) && $sendstatus->success==1){
                        $push['sent_status'] = 1;
                    }
                }
                if(empty($send_status)){
                    $send_status = 'failure';
                }
                echo( 'Push status for ' . $push['device_id'] );
                print_r($push);
                if ($push->save()) {
                    echo ('Updated');
                    print_r($push);
                } else {
                    echo('Not updated');
                }
            }
        }else{
            echo('No records found');
        }
    }
}
