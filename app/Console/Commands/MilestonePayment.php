<?php

namespace App\Console\Commands;

use App\User;
use App\Vault;
use App\Invoice;
use App\Setting;
use Carbon\Carbon;
use App\ProjectMilestone;
use App\UserWalletHistory;
use Illuminate\Console\Command;

class MilestonePayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'milestone:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Milestone payment after approve from client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //get all the freelancer vault having approved milestone status
        $vault_condition = [];
        $vault_condition['vault_status'] = 0;
        $vault_condition['transaction_type'] = 0;
       
        //get day value from  settings
        $day_count = Setting::select('value')->where('key', 'milestone_payment_day_count')->first()['value'];
        $date = \Carbon\Carbon::today()->subDays($day_count);
      
        $vaults = Vault::where($vault_condition)->with('user','milestones.project','transactions.contracts')->whereHas('milestones', function($q){
            $q->where('status', '4');
        })->get();
        
        foreach($vaults as $vault){
           
            //update vault status
            Vault::where('id',$vault->id)->update(['vault_status'=>1]);
            //Add entry in the vault table for both client and freelancer
            if($vault->user_type == 0){
                $vault_sts = Vault::create([
                    'user_id' => $vault->user_id,
                    'user_type' => $vault->user_type,
                    'project_milestone_id' => isset($vault->project_milestone_id)? $vault->project_milestone_id : "",
                    'transaction_type' => 1,
                    'transaction_id'=>$vault->transaction_id,
                    'amount' => $vault->amount,
                    'notes' => "Debit milestone amount from your vault for the milestone ". ucwords($vault->milestones->milestone_name)." for the project ".ucwords($vault->milestones->project->title),
                    'vault_status' =>1,
                    
                ]);
               
            }else{
                $vault_sts = Vault::create([
                    'user_id' => $vault->user_id,
                    'user_type' => $vault->user_type,
                    'project_milestone_id' => isset($vault->project_milestone_id)? $vault->project_milestone_id : "",
                    'transaction_type' => 1,
                    'transaction_id'=>$vault->transaction_id,
                    'amount' => $vault->amount,
                    'notes' => "Debit milestone amount from your vault and credited to your wallet for the milestone ". ucwords($vault->milestones->milestone_name)." of the project ".ucwords($vault->milestones->project->title),
                    'vault_status' =>1,
                    
                ]);
                //get user details
                $user_details = User::where('id',$vault->user_id)->first();
                $service_charge = "";
                $paid_amount = "";
                // if(isset($vault->transactions->contracts) && $vault->transactions->contracts != ""){
                //     $service_charge = $vault->transactions->contracts->freelancer_service_charge;
                //     if(isset($vault->transactions->contracts->freelancer_service_charge) && $vault->transactions->contracts->freelancer_service_charge){
                //         $service_charge = ($vault->amount * $vault->transactions->contracts->freelancer_service_charge)/100;
                //         $paid_amount    = $vault->amount - $service_charge;
                //     }
                // }
               
                //Create an entry in wallet
                $wallet_sts = UserWalletHistory::create([
                    'user_id' => $vault->user_id,
                    'transaction_type' => 0,
                    'transaction_id'=>$vault->transaction_id,
                    'amount' => $vault->amount,
                    'new_balance' => $user_details->wallet_balance + $vault->amount,
                    'notes' => "Credit milestone amount from your vault for the milestone '". ucwords($vault->milestones->milestone_name)."' of the project '".ucwords($vault->milestones->project->title)."'",
                    'type' =>2,
                    'payment_status'=>0
                    
                ]);
                

                //update wallet balance
                $user_update = User::where('id',$vault->user_id)->update(['wallet_balance'=>$user_details->wallet_balance + $vault->amount]);
            }
            if(isset($vault->project_milestone_id) && $vault->project_milestone_id != ""){
                //update the milestone
                //ProjectMilestone::where('id',$vault->project_milestone_id)->update(['status'=>5]);
                //Update invoice
                $invitation_condition = [];
                $invitation_condition['project_milestone_id'] = $vault->project_milestone_id;
                $invitation_condition['user_id'] = $vault->milestones->project->user_id;
                $invitation_condition['type'] = 1;
                $invitation_condition['status'] = 0;
                $update_invoice = Invoice::where($invitation_condition)->update(['status'=>1]);

            }
            
            


			
        }
        
    }
}
