<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->headers->set('Access-Control-Allow-Origin' , '*');
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-type, X-Auth-Token, Authorization, Origin,enctype');

        // return $next($request)->header('Access-Control-Allow-Origin', '*')
        // ->header('Access-Control-Allow-Headers', 'Content-type, X-Auth-Token, Authorization, Origin,enctype')
        // ->header('Access-Control-Allow-Methods', 'POST,GET,OPTIONS,PUT,DELETE');
        
        return $response;

    }
}
