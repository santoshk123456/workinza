<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {



        // code for redirecting to dashboard after resetting password submit   
        if(auth()->guard('web')->check()){
            return redirect('/');
        }elseif(auth()->guard('admin')->check()){
            return redirect()->route('admin.dashboard');
        }
        return $next($request);
    }
}
