<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ContactInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        if($this->method() == 'POST'){
            $request['username'] = 'required|unique:users,username|min:3|max:50|regex:/^[a-zA-Z0-9_.-]*$/';
            $request['address_line_1'] = 'required|min:3|max:300';
            $request['address_line_2'] = 'nullable|min:3|max:300';
            $request['country_id'] = 'required';
            $request['state'] = 'required|min:2|max:100';
            $request['city'] = 'required|min:2|max:100';
            $request['zipcode'] = 'required|min:3|max:20';
            $request['website_url'] = 'min:3|max:191|nullable';
            $request['country_code'] = 'required';
            $request['phone'] = 'required|unique:users,phone|regex:/^([0-9\s\-\+\(\)]*)$/|min:8|max:15';
            $request['otp'] = 'required|min:6|max:12';
        }
        return $request;
    }
}
