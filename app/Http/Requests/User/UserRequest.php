<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        if($this->method() == 'POST'){
            $request['full_name'] = 'required|min:3|max:50|regex:/^[a-zA-Z ]+$/';
            $request['company_name'] = 'nullable|min:3|max:50';
            $request['email'] = 'unique:users,email|email|required|max:50|regex:/^[_a-zA-Z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
            $request['password'] = 'required|min:3';
            $request['terms'] = 'nullable';
            $request['user_type'] = 'required|boolean';
            $request['organization_type'] = 'required|boolean';
        }
        return $request;
    }
}
