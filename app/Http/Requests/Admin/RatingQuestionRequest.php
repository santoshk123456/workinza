<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RatingQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST'){ 
            $request['user_type'] = 'required|in:client,freelancer,company';
            $request['question'] = 'required|max:1000';
        } else {
            $request['user_type_edit'] = 'required|in:client,freelancer,company';
            $request['question'] = 'required|max:1000';
        }
        return $request;
    }
}
