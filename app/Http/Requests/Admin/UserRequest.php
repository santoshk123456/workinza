<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

/**
 * Class StoreRoleRequest.
 */
class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        if($this->method() == 'POST'){
            $request['username'] = 'required|unique:users,username|min:3|max:50|regex:/^[a-zA-Z0-9_.-]*$/';
            $request['full_name'] = 'required|min:3|max:100|regex:/^[a-zA-Z ]+$/';
            $request['email'] = 'email|unique:users,email|required|max:50|regex:/^.*(?=.{8,})[\w.]+@[\w.]+[.][a-zA-Z0-9]+$/';
            // $request['paypal_email'] = 'email|unique:user_details,paypal_email|required|max:50|regex:/^[.a-zA-Z0-9]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/';
            $request['organization_type'] = 'required';
            $request['address_line_1'] = 'required|min:3|max:300';
            $request['address_line_2'] = 'nullable|min:3|max:300';
            $request['country_id'] = 'required';
            $request['state'] = 'required|min:2|max:100';
            $request['city'] = 'required|min:2|max:100';
            $request['zipcode'] = 'required|min:3|max:20';
            $request['website_url'] = 'min:3|max:191|nullable|url';
            $request['country_code'] = 'required';
            $request['phone'] = 'required|unique:users,phone|regex:/^([0-9\s\-\+\(\)]*)$/|min:8|max:15';
            $request['profile_headline'] = 'required|min:3|max:250';
            $request['industry_ids'] = 'required';
            $request['categories_ids'] = 'required';
            $request['sub_categories_ids'] = 'required';
            // $request['skills'] = 'required';
            $request['company_tagline'] = 'nullable|min:3|max:300';
            $request['company_history'] = 'required';
            $request['profile_image'] = 'mimes:jpeg,jpg,png,gif|max:10000';
            $request['years_of_experience'] = 'integer|nullable';
        } else {
            if(!$this->user){
                $this->user = User::where('id',Auth::guard('api')->user()->id)->first();
            }
            $request['username'] = 'unique:users,username,'.$this->user->id.'required|min:3|max:50|regex:/^[a-zA-Z0-9_.-]*$/';
            $request['full_name'] = 'required|min:3|max:100|regex:/^[a-zA-Z ]+$/';
            $request['email'] = 'unique:users,email,'.$this->user->id.'email|required|max:50|regex:/^[.a-zA-Z0-9]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/';
            // $request['paypal_email'] = 'email|unique:user_details,paypal_email,'.$this->user->id.',user_id|required|max:50|regex:/^[.a-zA-Z0-9]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/';
            $request['organization_type'] = 'required';
            $request['address_line_1'] = 'required|min:3|max:300';
            $request['address_line_2'] = 'nullable|min:3|max:300';
            $request['country_id'] = 'required';
            $request['state'] = 'required|min:2|max:100';
            $request['city'] = 'required|min:2|max:100';
            $request['zipcode'] = 'required|min:3|max:20';
            $request['website_url'] = 'min:3|max:191|nullable|url';
            $request['country_code'] = 'required';
            $request['phone'] = 'unique:users,phone,'.$this->user->id.'numeric|required|regex:/^([\+\d]?(?:[\d\-\s()]*))$/|min:7|max:15';
            $request['profile_headline'] = 'required|min:3|max:250';
            $request['industry_ids'] = 'required';
            $request['categories_ids'] = 'required';
            $request['sub_categories_ids'] = 'required';
            // $request['skills'] = 'required';
            $request['company_tagline'] = 'nullable|min:3|max:300';
            $request['company_history'] = 'required';
            $request['profile_image'] = 'mimes:jpeg,jpg,png,gif|max:10000';
            $request['years_of_experience'] = 'integer|nullable';
        }
        return $request;
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'First Name is required.',
            'first_name.regex' => 'Please enter only alphabets.',
            'last_name.required' => 'Last Name is required.',
            'last_name.regex' => 'Please enter only alphabets.',
            'username.required' => 'Username is required.',
            'username.regex' => 'Please enter only alphabets.',
            'email.required'  => 'Message is required.',
            'mobile_number.required'  => 'Mobile number is required.',
            'username.unique' => 'This username already exists.',
            'email.unique'  => 'This email already exists.',
            'mobile_number.unique'  => 'This mobile number already exists.',
            'password_confirmation.confirmed' => __('The password confirmation does not match.'),
            'password.min' => __('The password should have atleast 6 characters.'),
        ];
    }
}
