<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SecurityQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST'){ 
            $request['user_type'] = 'required';
            $request['question'] = 'required|max:500';
        } else {
            $request['user_type_edit'] = 'required';
            $request['question'] = 'required|max:500';
        }
        return $request;
    }
}
