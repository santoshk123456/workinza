<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST'){ 
            $request['sub_category_name'] = 'required|min:2|max:50';
            $request['categories_ids'] = 'required';
            
        } else {
            $request['sub_category_name'] = 'required|min:2|max:50';
            $request['category_id'] = 'required';
          
            
        }
        return $request;
    }
}
