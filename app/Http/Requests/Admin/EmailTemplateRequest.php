<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRoleRequest.
 */
class EmailTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        $request['static_email_heading'] = 'required|min:3|max:100';
        $request['subject'] = 'required|min:3|max:150';
        $request['description'] = 'required';
        $request['template'] = 'required';
        return $request;
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'static_email_heading.required' => 'Email Heading cannot be empty.',
            'subject.required' => 'Subject cannot be empty.',
            'description.required' => 'Descrition cannot be empty.',
            'template.required' => 'Template cannot be empty.',
        ];
    }
}
