<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRoleRequest.
 */
class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        $request['current_password'] = 'required';
        $request['password'] = 'required|confirmed|min:6';
        $request['password_confirmation'] = 'required';
        return $request;
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'current_password.required' => __('Current Password is required.'),
            'password.required' => __('New Password is required.'),
            'password_confirmation.confirmed' => __('The password confirmation does not match.'),
            'password.min' => __('The password should have atleast 6 characters.'),
        ];
    }
}
