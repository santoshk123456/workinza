<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRoleRequest.
 */
class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        if($this->method() == 'POST'){
            $request['name'] = 'required|max:50|min:3';
            $request['username'] = 'unique:admins|required|min:3|max:50';
            $request['email'] = 'unique:admins|required|max:50|email|regex:/^[a-zA-Z]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/';
            //$request['mobile_number'] = 'unique:admins|required|regex:/^([\+\d]?(?:[\d\-\s()]*))$/|min:10|max:15';
            $request['country_code'] = 'required';
            $request['phone'] = 'required|unique:admins,mobile_number|regex:/^([0-9\s\-\+\(\)]*)$/|min:8|max:15';
            $request['profile_image'] = 'mimes:jpeg,jpg,png,gif|max:10000';
        } else {
            
            $request['name'] = 'required|max:50|min:3';
            $request['username'] = 'unique:admins,username,'.$this->admin->id.'|required|min:3|max:50';
            $request['email'] = 'unique:admins,email,'.$this->admin->id.'|required|max:50|email|regex:/^[a-zA-Z]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/';
            //$request['mobile_number'] = 'unique:admins,mobile_number,'.$this->admin->id.'numeric|required|regex:/^([\+\d]?(?:[\d\-\s()]*))$/|min:10|max:15';
            $request['country_code'] = 'required';
            $request['phone'] = 'unique:admins,mobile_number,'.$this->admin->id.'numeric|required|regex:/^([\+\d]?(?:[\d\-\s()]*))$/|min:7|max:15';
            $request['profile_image'] = 'mimes:jpeg,jpg,png,gif|max:10000';
        }
        return $request;
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'username.required' => 'Username is required.',
            'email.required'  => 'Message is required.',
            'mobile_number.required'  => 'Mobile number is required.',
            'username.unique' => 'This username already exists.',
            'email.unique'  => 'This email already exists.',
            'mobile_number.unique'  => 'This mobile number already exists.',
        ];
    }
}
