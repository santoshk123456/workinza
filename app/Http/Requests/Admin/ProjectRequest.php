<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        if($this->method() == 'POST'){
            $request['user_id'] = 'required';
            $request['title'] = 'required|min:3|max:150';
            $request['location'] = 'required|min:3|max:150';
            $request['type'] = 'required';
            $request['industry_ids'] = 'required';
            $request['categories_ids'] = 'required';
            $request['sub_categories_ids'] = 'required';
            $request['skills'] = 'required';
            $request['description'] = 'required';
            $request['budget_range'] = 'required';
            // $request['budget_to'] = 'integer|required';
            $request['timeline_count'] = 'integer|required';
            $request['timeline_type'] = 'required';
            $request['expected_start_date'] = 'required';
            $request['expected_end_date'] = 'required';
            $request['bid_start_date'] = 'required';
            $request['bid_end_date'] = 'required';
        } else {
            $request['user_id'] = 'required';
            $request['title'] = 'required|min:3|max:150';
            $request['location'] = 'required|min:3|max:150';
            $request['type'] = 'required';
            $request['industry_ids'] = 'required';
            $request['categories_ids'] = 'required';
            $request['sub_categories_ids'] = 'required';
            $request['skills'] = 'required';
            $request['description'] = 'required';
            $request['budget_range'] = 'required';
            // $request['budget_to'] = 'integer|required';
            $request['timeline_count'] = 'integer|required';
            $request['timeline_type'] = 'required';
            $request['expected_start_date'] = 'required';
            $request['expected_end_date'] = 'required';
            $request['bid_start_date'] = 'required';
            $request['bid_end_date'] = 'required';
        }
        return $request;
    }
}
