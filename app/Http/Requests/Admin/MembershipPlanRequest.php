<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MembershipPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        if($this->method() == 'POST'){
            $request['plan_name'] = 'required|max:90|min:2';
            $request['user_type'] = 'required';
            $request['monthly_amount'] = 'between:0,999999.99';
            $request['yearly_amount'] = 'between:0,999999.99';
            $request['max_project_fr'] = 'nullable';
            $request['max_project_cl'] = 'nullable';
            $request['service_fee'] = 'required|between:0,9999.99';
            $request['max_proposal'] = 'nullable';
            $request['max_products'] = 'nullable';
            $request['product_commission'] = 'nullable|between:0,999999.99';
            $request['chat_feature'] = 'required';
            $request['setup_charge'] = 'required|between:0,999999.99';
        } else {
            
            $request['plan_name'] = 'required|max:90|min:2';
            $request['user_type'] = 'required';
            $request['monthly_amount'] = 'between:0,999999.99';
            $request['yearly_amount'] = 'between:0,999999.99';
            $request['max_project_fr'] = 'nullable';
            $request['max_project_cl'] = 'nullable';
            $request['service_fee'] = 'required|between:0,9999.99';
            $request['max_proposal'] = 'nullable';
            $request['max_products'] = 'nullable';
            $request['product_commission'] = 'nullable|between:0,999999.99';
            $request['chat_feature'] = 'required';
            $request['setup_charge'] = 'required|between:0,999999.99';
            
        }
        return $request;
    }
}
