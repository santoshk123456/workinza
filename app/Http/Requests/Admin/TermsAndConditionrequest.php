<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TermsAndConditionrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST'){ 
            $request['heading'] = 'required|min:2|max:50';
            $request['slug'] = 'required';
            $request['static_heading'] = 'required';
            $request['version'] = 'required';
            $request['short_description'] = 'nullable';
            $request['description'] = 'required';
        } else {
            $request['heading'] = 'required|min:2|max:50';
            $request['slug'] = 'required';
            $request['version'] = 'required';
            $request['short_description'] = 'nullable';
            $request['description'] = 'required';
            
        }
        return $request;
    }

}
