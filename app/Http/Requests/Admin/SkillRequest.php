<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'POST'){ 
            $request['skill_name'] = 'required|min:2|max:50';
            $request['categories_ids'] = 'required';
            $request['sub_categories_ids'] = 'required';
            
        } else {
            $request['skill_name'] = 'required|min:2|max:50';
            $request['category_id'] = 'required';
            $request['subcategory_id'] = 'required';
            
        }
        return $request;
    }
}
