<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreRoleRequest.
 */
class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = [];
        $request['image'] = 'mimes:jpeg,jpg,png | max:2048';
        if($this->method() == 'POST'){ 
            $request['heading'] = 'unique:pages|required|max:191';
            $request['slug'] = 'unique:pages|required';
        } else {
            $request['heading'] = 'unique:pages,heading,'.$this->page->id.'|required|max:191';
            $request['slug'] = 'unique:pages,slug,'.$this->page->id.'|required|max:191';
        }
        return $request;
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'heading.unique' => 'The page with this heading already exists.',
            'slug.unique' => 'The page with this slug already exists.',
        ];
    }
}
