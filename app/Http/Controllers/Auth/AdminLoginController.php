<?php



namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Admin;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class AdminLoginController extends Controller

{

    /*

    |--------------------------------------------------------------------------

    | Login Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles authenticating users for the application and

    | redirecting them to your home screen. The controller uses a trait

    | to conveniently provide its functionality to your applications.

    |

    */



    use AuthenticatesUsers;



    protected $guard = 'admin';



    /**

     * Where to redirect users after login.

     *

     * @var string

     */

    protected $redirectTo = '/home';



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('guest')->except('logout');
    }



    public function showLoginForm()

    {
        return view('auth.admin-login');
    }
    // socialite login

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {


        try {
            $user = Socialite::driver('google')->user();
            // $finduser = User::where('google_id', $user->id)->first();
            $finduser = Admin::where('google_id', $user->id)->first();

            if ($finduser) {

                Auth::guard('admin')->login($finduser);
                return redirect()->route('admin.dashboard')->withToastr(['type' => 'success', 'text' => 'You have been successfully logged in.']);
            } else {
                $updateuser = Admin::where('email', $user->email)->first();
                if ($updateuser) {
                    $updateuser->google_id = $user->id;
                    $updateuser->save();
                    Auth::guard('admin')->login($finduser);
                    return redirect()->route('admin.dashboard')->withToastr(['type' => 'success', 'text' => 'You have been successfully logged in.']);
                } else {
                    // this is new user who is not supposed to be admin
                    return redirect('/admin')->withToastr(['type' => 'error', 'title' => 'Error', 'text' => 'You are not authorized to login']);
                    //  dd($user);
                    //   $newUser = Admin::create([
                    //             'name' => $user->name,
                    //             'email' => $user->email,
                    //             'google_id'=> $user->id
                    //         ]);

                    //         Auth::login($newUser);
                }
            }
        } catch (Exception $e) {
            return redirect('auth/google');
        }
    }
    /**
     * for facebook twitter and github
     */

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        if($provider=='github'){
            $getInfo = Socialite::driver('github')->stateless()->user();
        }
        if($provider=='facebook'){
            $getInfo = Socialite::driver('facebook')->stateless()->user();  
        } 
        if($provider=="twitter"){
            $getInfo = Socialite::driver($provider)->user();
        }

       
        $updateuser = Admin::where('email', $getInfo->email)->first();
        
        
        if($updateuser != null){
             
            if($provider=='github'){ 
                $updateuser->github_id = $getInfo->id;
                $updateuser->save(); 
           }
           if($provider=='facebook'){
             $updateuser->provider_id = $getInfo->id;
               $updateuser->save();
           } 
           if($provider=="twitter"){
              $updateuser->twitter_id = $getInfo->id;
               $updateuser->save();
           }

            Auth::guard('admin')->login($updateuser);
            return redirect()->route('admin.dashboard')->withToastr(['type' => 'success', 'text' => 'You have been successfully logged in.']);     
            
        }
        else{
            // in case of different mail
            if($provider=='github'){
            $user = Admin::where('github_id', $getInfo->id)->first(); 
           }
           if($provider=='facebook'){
            $user = Admin::where('provider_id', $getInfo->id)->first(); 
           } 
           if($provider=="twitter"){
            $user = Admin::where('twitter_id', $getInfo->id)->first(); 
           }
            
        }
        if($user == null){
          
         //not authenticated user
         return redirect('/admin')->withToastr(['type' => 'error', 'title' => 'Error', 'text' => 'You are not authorized to login']);   
        }else{     
              
                Auth::guard('admin')->login($user);
                return redirect()->route('admin.dashboard')->withToastr(['type' => 'success', 'text' => 'You have been successfully logged in.']);     
            }
        }
    

    public function login(Request $request)

    {
        if (auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $new_user = auth()->guard('admin')->user();
            if ($new_user->active == 1) {
                activity()->causedBy(auth()->guard('admin')->user())
                    ->performedOn(auth()->guard('admin')->user())
                    ->withProperties(['name' => auth()->guard('admin')->user()->name])
                    ->useLog('admin-user')
                    ->log('Logged in');
                //For set timezone
                if (!empty($request->timezone)) {
                    Session(['timezone' => Config('app.site_timezone')]);
                } else {
                    Session(['timezone' => Config('app.site_timezone')]);
                }
                return redirect()->route('admin.dashboard')->withToastr(['type' => 'success', 'text' => 'You have been successfully logged in.']);
            } else {
                Auth::guard('admin')->logout();
                $request->session()->invalidate();
                return redirect('/admin')->withErrors(['password' => 'Your account is inactive. Please contact your administrator for more details.']);
            }
        }
        $admins = Admin::where('email', $request->email)->first();
        if ($admins == null) {
            return redirect()->back()->withInput($request->only('', 'remember'))->withErrors([
                'email' => 'Email is incorrect'
            ]);
        } else {
            return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
                'password' => 'Password is incorrect'
            ]);
        }
    }

    /**
     * Handle Social login request
     *
     * @return response
     */
    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }
    /**
     * Obtain the user information from Social Logged in.
     * @param $social
     * @return Response
     */
    public function handleProviderCallback($social)
    {
        $userSocial = Socialite::driver($social)->user();
        $user = User::where(['email' => $userSocial->getEmail()])->first();
        if ($user) {
            Auth::login($user);
            return redirect()->action('HomeController@index');
        } else {
            return view('auth.register', ['name' => $userSocial->getName(), 'email' => $userSocial->getEmail()]);
        }
    }
    public function logout(Request $request)
    {
        if (auth()->guard('admin')->user()) {

            activity()->causedBy(auth()->guard('admin')->user())
                ->performedOn(auth()->guard('admin')->user())
                ->withProperties(['name' => auth()->guard('admin')->user()->name])
                ->useLog('admin-user')
                ->log('Logged out');
            Auth::guard('admin')->logout();
            $request->session()->invalidate();
            return $this->loggedOut($request) ?: redirect('/admin')->withToastr(['type' => 'success', 'text' => 'Logged out']);;
        } else {
            return redirect('/admin')->withToastr(['type' => 'success', 'text' => 'Logged out']);
        }
    }
}
