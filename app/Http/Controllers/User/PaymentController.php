<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Vault;
use App\Invoice;
use App\Project;
use App\Setting;
use Stripe\Charge;
use Stripe\Stripe;
use App\Transaction;
use App\EmailTemplate;
use App\InvitedClient;
use App\PaymentGateway;
use App\ProjectContract;
use App\ProjectMilestone;
use App\UserSubscription;
use App\UserWalletHistory;
use Illuminate\Http\Request;
use App\PaymentGatewayCustomer;
use Illuminate\Support\Facades\DB;
use App\Services\ProcessEmailQueue;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Libraries\PaymentGateways\PaymentGatewaysController;

class PaymentController extends Controller
{
    
  

    /**
    * subscriptionSuccess : for showing the success subscription payment 
    */
    public function subscriptionSuccess(Request $request)
    {
      $user = auth('web')->user();

      if($user){
        if($user->step == 6){
		  if($user->user_type == 0){
			return redirect()->route('account.createPublicProfile');
		  }else{
		  	return redirect()->route('membership_plan.membershipPlans')->with('toastr', ['type'=>'success','text'=>__('Membership Plan has been subscribed successfully.')]);
		  }
        }else{
		  $user_step = User::where('id',$user->id)->update(['step'=>5]);
		  if($user->user_type == 0){
			return redirect()->route('account.createPublicProfile');
		  }else{
			return redirect()->route('account.createPublicProfile')->with('toastr', ['type'=>'success','text'=>__('Membership Plan has been subscribed successfully.')]);
		  }
          
        }
      }
	}
	
    /**
	 * addMoney : addMoney to vault for milestone activation
	 * Params 		: ProjectMilestone object
	 * Response 	: If successfull redirect to add mone page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Create stripe session checkout
	 * STEP 3  		: Redirect to add Money page with session token
	 */

	public function addMoney(ProjectMilestone $milestone = null){
		$user = auth('web')->user();

		if($user){
			//Check the freelacner accept the contract
			$conditions = [];
			$conditions['client_id'] = $user->id;
			$conditions['project_id'] = $milestone->project_id;
			$conditions['status'] = 1;
			$conditions['freelancer_acceptance'] = 1;
			$contract_Details = ProjectContract::where($conditions)->first();
			if(empty($contract_Details)){
				return redirect()->back()->with('toastr', ['type'=>'error','text'=>__('You can activate this milestone after the freelancer acceptance.')]);;
			}else{
				$transaction_fee = Setting::select('value')->where('key', 'transaction_fee')->first()['value'];
				if(isset($milestone) && $milestone != null){
					$payment_keys = PaymentGateway::where('status',1)->first();
					$total_amount = $milestone->milestone_amount + ($milestone->milestone_amount * $transaction_fee)/100;
					$paymentGateway = new PaymentGatewaysController;
					if($paymentGateway->initiatePaymentGateway()){
						$request = [];
						$request['charge'] = $total_amount;
						$request['subscription'] = 0;
						$request['name'] = "Milestone Payment";
						$request['email'] = $user->email;
						$request['success_url'] = route('finance.milestonePaymentSuccess',$milestone);
						$request['milestone'] = $milestone->id;
						$request['cancel_url'] = route('finance.addMoney',$milestone);
						$request['description'] = "A one time charge USD" . $milestone->milestone_amount . " will be debited by Keyoxa for activating the milestone.";
						$checkout_session =  $paymentGateway->createCheckoutSession($request);
						
						return view('user.finance.add_money',compact('milestone','checkout_session','user','payment_keys','total_amount','transaction_fee'));
					}
				}else{
					return view('user.finance.add_money',compact('user'));
				}
			}
			
			
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);;
		}
	}
	/**
	 * milestonePaymentSuccess : for displaying milestone success payment
	 */
	public function milestonePaymentSuccess(ProjectMilestone $milestone){
		$user = auth('web')->user();

		if($user){
			
			return redirect()->route('project.inProgressProjectDetails',$milestone->project)->with('toastr', ['type'=>'success','text'=>__('You have successfully paid the milestone amount and activated the milestone.')]);
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);;
		}
	} 

	/**
	 * MyWallet : List wallet
	 */
	public function myWallet(){
		$user = auth('web')->user();

		if($user){
			 //vault
			 $wallets = UserWalletHistory::where('user_id',$user->id);
			 if(isset($request->title) && $request->title != ""){
					   $vaults = $vaults->whereHas('milestones.project',  function($query) use ($request)  {
				 $query->where('milestone_name', 'LIKE', '%' . $request->title . '%')->orWhere('title', 'LIKE', '%' . $request->title . '%');
			   });
			 }
			
			 $wallets = $wallets->orderBy('id','DESC')->paginate(10);
			return view('user.finance.my_wallet',compact('user','wallets'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * Keyoxa Vault : List Vault with search filter
	 */
	public function myVault(Request $request){
		$user = auth('web')->user();

		if($user){
      //vault
      $vaults = Vault::where('user_id',$user->id);
      if(isset($request->title) && $request->title != ""){
				$vaults = $vaults->whereHas('milestones.project',  function($query) use ($request)  {
          $query->where('milestone_name', 'LIKE', '%' . $request->title . '%')->orWhere('title', 'LIKE', '%' . $request->title . '%');
        });
      }
     
      $vaults = $vaults->orderBy('id','DESC')->paginate(10);
    
			return view('user.finance.my_vault',compact('user','vaults'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * createCheckoutSession : Create session for stripe payment
	 * 
	 */
	public function createCheckoutSession(Request $request){
		$user = auth('web')->user();
		if($user){
		
			$payment_keys = PaymentGateway::where('status', 1)->first();
			$paymentGateway = new PaymentGatewaysController;
			if ($paymentGateway->initiatePaymentGateway()) {
				$request_data = [];
				$request_data['charge'] = $request->amount;
				$request_data['subscription'] = 0;
				$request_data['name'] = "Wallet Payment";
				$request_data['email'] = $user->email;
				$request_data['success_url'] = route('finance.walletPaymentSuccess');
				$request_data['cancel_url'] = route('finance.addMoney');
				$request_data['description'] = "A one time charge USD" . $request->charge . " will be debited by Keyoxa for activating the milestone.";
				$checkout_session =  $paymentGateway->createCheckoutSession($request_data);
				$resultArr['success'] = 1;
				$resultArr['msg'] = "Success";
				$resultArr['checkout_session'] = $checkout_session;
				$resultArr['paymnet_gateways'] = $payment_keys;
			}
		}else{
			$resultArr['success'] = 1;
			$resultArr['msg'] = "Authorization failed";
		}
		return response()->json($resultArr);
	}

	/**
	 * Wallet Payment Success
	 */
	public function walletPaymentSuccess(){
		$user = auth('web')->user();

		if($user){
			//Entry into wallet table

			return view('user.finance.my_wallet',compact('user'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * List Transactions
	 */
	public function listTransactions(Request $request){
		$user = auth('web')->user();

		if($user){
			$transactions = Transaction::where('user_id',$user->id);
			if(isset($request->contract_id) && $request->contract_id != ""){
				$transactions = $transactions->where('project_contract_id', 'LIKE', '%' . substr($request->contract_id , 2). '%')->orWhere('id', 'LIKE', '%' . substr($request->contract_id , 2). '%');
			}
			
			$transactions = $transactions->orderBy('id','DESC')->paginate(10);

			return view('user.finance.transactions',compact('user','transactions'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * listInvoices
	 */
	public function listInvoices(Request $request){
		$user = auth('web')->user();

		if($user){
			$invoices = Invoice::where('user_id',$user->id);
			if(isset($request->title) && $request->title != ""){
				$invoices = $invoices->where('project_contract_id', 'LIKE', '%' . substr($request->title , 2). '%')->orWhere('id', 'LIKE', '%' . substr($request->title , 2). '%');
			}
			$invoices = $invoices->orderBy('id','DESC')->paginate(10);

			return view('user.finance.invoices',compact('user','invoices'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}
	/**
	 * invoiceDetails
	 */
	public function invoiceDetails(Invoice $invoice){
		$user = auth('web')->user();

		if($user){
			$invoice = Invoice::where('id',$invoice->id)->with('milestones.project.user','contracts.user')->first();
		
			
			$admin_name = Setting::select('value')->where('key', 'application_admin_name')->first()['value'];
			return view('user.finance.invoice_view',compact('user','invoice','admin_name'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * invitedClients : List invited clients
	 */
	public function invitedClients(){
		$user = auth('web')->user();
		
		if($user){
			$invited_clients = InvitedClient::where('freelancer_id',$user->id)->orderBy('id','DESC')->paginate(10);
			return view('user.finance.invited_clients',compact('user','invited_clients'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * currentClients : List current clients
	 */
	public function currentClients(){
		$user = auth('web')->user();
		if($user){
			$resultArr = [];
			//get all client related to in contract
			$contracts = ProjectContract::where('freelancer_id',$user->id)->groupBy('client_id')->with('client')->get();
			foreach($contracts as $contract){
			
				$contract_details = ProjectContract::where('freelancer_id',$user->id)->where('client_id',$contract->client_id)->with('client','project')->whereHas('project', function($query)  {
					$query->where([['status', '=',4]]);
				})->orderBy('id','DESC')->get();
			
				if(isset($contract_details) && $contract_details->isNotEmpty()){
					$resultArr[$contract->client->username] = $contract_details;
				}
				
				 
			}
		
			
			
			return view('user.finance.current_clients',compact('user','contracts','resultArr'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * viewTransaction
	 */
	public function viewTransaction(Transaction $transaction){
		$user = auth('web')->user();
		if($user){
			return view('user.finance.transaction_view',compact('transaction','user'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}
	/**
	 * listIncentives
	 */
	public function listReferrals(Request $request){
		$user = auth('web')->user();
		if($user){
			//get all the wallet history 
			
			if(isset($request->username) && $request->username != ""){
				$wallets = UserWalletHistory::where('user_id',$user->id)->where('referral_bonus_charge','!=','0')->where('referral_or_bonus',0)->with('transactions.contracts.client')->whereHas('transactions.contracts.client', function($query) use($request)  {
					$query->where([['username', '=',$request->username]]);
				});
			
		     }else{
				$wallets = UserWalletHistory::where('user_id',$user->id)->where('referral_bonus_charge','!=','0')->where('referral_or_bonus',0)->with('transactions.contracts.client');
			 }
			
			$wallets = $wallets->orderBy('id','DESC')->paginate(10);
			return view('user.finance.referrals',compact('user','wallets'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}
	/**
	 * listBonus
	 */
	public function listBonus(){
		$user = auth('web')->user();
		if($user){
			$resultArr = [];
			//bonus 
			$clients = DB::table('user_wallet_histories')
			->select('user_wallet_histories.*','transactions.*','project_contracts.*','users.*')
			->where('referral_bonus_charge','!=','0')->where('referral_or_bonus',1)
			->join('transactions', function ($join) {
				$join->on('user_wallet_histories.transaction_id', '=', 'transactions.id');
					
			})->join('project_contracts', function ($join) {
				$join->on('transactions.project_contract_id', '=', 'project_contracts.id');
			})
			->join('users', function ($join) {
				$join->on('project_contracts.client_id', '=', 'users.id');
			})
			->groupBy('transactions.project_contract_id')
			->get();
		
		   

		
			//dd($wallets);
			foreach($clients as $key=>$client){
				
				$wallets = UserWalletHistory::where('user_id',$user->id)->where('referral_bonus_charge','!=','0')->where('referral_or_bonus','=','1')->with('transactions.contracts.client')->whereHas('transactions.contracts', function($q) use($client){
					$q->where('id', '=', $client->project_contract_id);
				})->get();
				if($wallets->isNotEmpty()){
					$resultArr[$client->username] = $wallets;
				}
				
			}
			
			return view('user.finance.bonus',compact('user','resultArr'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * referPerson
	 */
	public function referPerson(){
		$user = auth('web')->user();
		if($user){
			return view('user.finance.refer_person',compact('user'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}

	/**
	 * sendReferralInvitation
	 * Params 		: Request object
	 * Response 	: If successfull return proper response
	 * STEP 1 		: Check the user authentication
	 * STEP 2 		: Send refer email 
	 */
	public function sendReferralInvitation(Request $request){
		$user = auth('web')->user();
		
		if($user){
			
			//insert an entry in invited client table
			$invited_clients_sts = InvitedClient::create([
				'freelancer_id' =>$user->id,
				'user_name' => $request->username,
				'user_type'=> $request->user_type,
				'email'=> $request->email,
				'country_code'=> $request->country_code,
				'mobile_number'=> $request->mobile_number,
				'status'=>0,
				

			]);
			//send email notification to that email id
			$invite_start_template = EmailTemplate::where('static_email_heading','REFER_PERSON')->first();
			
            if ($invite_start_template) {
                $email_html = $invite_start_template->template;
                $email_html = str_replace("[USER_NAME]",ucwords($request->username), $email_html);
				$message = "";
				$message = ucwords($user->username)." has invited you to join in Keyoxa".
				$email_html = str_replace("[MESSAGE]", $message, $email_html);
		
				$enc_ref_code = bin2hex($user->userDetail->referral_code);
			
                $email_html = str_replace("[INVITATION_LINK]", route('user.accountDetails', ['organization_type' => $request->user_type, 'user_type' => 0,'referral'=>$enc_ref_code,'invitation'=>bin2hex($invited_clients_sts->id)]), $email_html);
				
                $template = view('admin.email_templates.layout', compact('email_html'));
                $data = [];
                $data['to_email'] = $request->email;
                $data['to_name'] = ucwords($request->username);
                $data['subject'] = $invite_start_template->subject;
                $data['body'] = $template;
                $data['action_id'] = $user->id;
                $data['action_type'] = 'refer_person';
              
                $processEmailQueue=new ProcessEmailQueue($data);
                $processEmailQueue->sendEmail();
			}
			return redirect()->route('finance.invitedClients',compact('user'))->with('toastr', ['type'=>'success','text'=>__('Referral invitation has been sent succssfully.')]);;
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}
	/**
	 * projectTransaction : List transaction under the project
	 */
	public function projectTransactions(Project $project){
		$user = auth('web')->user();
		if($user){
			$cond = [];
			$cond['user_id'] = $user->id;
			$cond['type'] = 2;
			$cond['project_id'] = $project->id;
			$transactions = Transaction::where($cond)->orderBy('id','DESC')->paginate(20);
			
			return view('user.finance.project_transactions',compact('user','transactions','project'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>__('Authentication failed.')]);
		}
	}
}
