<?php

namespace App\Http\Controllers\User\Auth;

use Hash;
use Session;
use App\User;
use App\Project;
use App\UserSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    protected $guard = 'web';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout','viewdashboard');
    }

    public function login(Request $request)
    {
        $returnArr = [];
        $remember_me = $request->has('remember') ? true : false;
       
        if (auth()->guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $remember_me)) {
           
            $new_user = auth()->guard('web')->user();
          
            if ($new_user->active == 1 || $new_user->active == 2) {
                activity()->causedBy(auth()->guard('web')->user())
                    ->performedOn(auth()->guard('web')->user())
                    ->withProperties(['name' => auth()->guard('web')->user()->name])
                    ->useLog('user')
                    ->log('Logged in');
                //For set timezone
                if (!empty($request->timezone)) {
                    Session(['timezone' => Config('app.site_timezone')]);
                } else {
                    Session(['timezone' => Config('app.site_timezone')]);
                }
                $returnArr['success'] = 1;
                $returnArr['msg'] = "You have been successfully logged in";
                Session:: flash('flash_success', 'Successfully Logged In.');
              
                if($new_user->step == 1){
                    if(empty($new_user->email_verified_at)){
                        return redirect()->route('user.emailVerification',$new_user);
                    }else{
                        return redirect()->route('user.contactInformation');
                    }
                   
                }else if($new_user->step == 2){
                    return redirect()->route('user.securityQuestions');
                }else if($new_user->step == 3){
                    return redirect()->route('user.profileAcknowledgement');
                }else if($new_user->step == 4){
                    return redirect()->route('user.backgroundVerification');
                }else if($new_user->step == 5) {
                    return redirect()->route('account.createPublicProfile');
                }else if($new_user->step == 6){
                    //get the subscription status
                    $sub_conditions = [];
                    $sub_conditions['user_id'] = $new_user->id;
                    $subscription_status = UserSubscription::where($sub_conditions)->orderBy('id', 'DESC')->first();
                   
                    if($new_user->user_type == 0){
                        if(isset($subscription_status) && !empty($subscription_status) && $subscription_status->active == 1){
                            return redirect()->route('user.viewdashboard');
                           // return redirect('project/open-projects');
                        }else{
                            return redirect()->route('membership_plan.membershipPlans');
                        }
                        
                    }else{
                       
                        return redirect()->route('user.viewdashboard');
                        //return redirect()->route('project.createPublicProfile');
                      //  return redirect('/project/invitations');
                    }
                }
                // require('quora/wp-load.php');
                // wpLoginFunction(auth('web')->user()->email);
                //return response()->json($returnArr);
                
            } else {
                auth()->guard('web')->logout();
                $request->session()->invalidate();
                return redirect('/login')->withToastr(['type' => 'error', 'text' => 'Your account has been blocked. Please contact admin for more details.']);
            }
        }else{
            return redirect('/login')->withToastr(['type' => 'error', 'text' => 'Your email or password is incorrect']);
            // $user = User::where([['email',$request->user_name],['is_deleted',0]])->onlyTrashed()->first();
            // if(empty($user)){
            //     $returnArr['success'] = 0;
            //     $returnArr['msg'] = "Your username or password is incorrect";
            // }else{
            //     $returnArr['success'] = 0;
            //     $returnArr['msg'] = "Your account has been deleted. Please contact admin to recover your account";
            // }
            // return response()->json($returnArr);
        }
    }

    public function emailExist(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'email' => 'required|max:50'
        ]);
        $msg = "";
        $resultArr = [];
        if (!$validator->fails()) {
            $user = User::where('email', $request->email)->withTrashed()->first();
            if ($user) {
                $email_exist = 1;
                $msg =  "Email already exist.";
            } else {
                $email_exist = 0;
            }
        } else {
            $email_exist = 2;
            $msg = "The email format is invalid.";
        }
        $resultArr['email_exist'] = $email_exist;
        $resultArr['msg'] = $msg;
        return response()->json($resultArr);
    }

    public function emailExistcheck(Request $request)
    {
      
       
        $msg = "";
        $resultArr = [];
       
            $user = User::where('email',$request->username)->withTrashed()->first();
            if(empty($user)) {
                $email_exist = 1;
                $msg =  "Email is not existing.";
            }
   
        $resultArr['email_exist'] = $email_exist;
        $resultArr['msg'] = $msg;

        return response()->json($resultArr);
    }
    
    public function logout(Request $request)
    {
        if (auth()->guard('web')->user()) {
            activity()->causedBy(auth()->guard('web')->user())
                ->performedOn(auth()->guard('web')->user())
                ->withProperties(['name' => auth()->guard('web')->user()->name])
                ->useLog('user')
                ->log('Logged out');
            auth()->guard('web')->logout();
            $request->session()->invalidate();
            return $this->loggedOut($request) ?: redirect('/login')->withToastr(['type' => 'success', 'text' => 'Logged out']);
        } else {
            return redirect('/login')->withToastr(['type' => 'success', 'text' => 'Logged out']);
        }
    }

      /**
      * dashboard
      */
    //   public function viewdashboard()
    //   {
         
    //      return view('user.registration.dashboard');
    //   }
 

      public function viewdashboard()
      {
         $user = auth('web')->user();
         $open_projects = Project::where('user_id',$user->id)->whereIn('status', [0, 1])->count();
         $inprogress_projects = Project::where('user_id',$user->id)->whereIn('status', [2, 3])->count();
         $completed_projects = Project::where('user_id',$user->id)->whereIn('status', [4,5])->count();
         
         $accept_proposal = []; 
             // dd($user)
         if($user->user_type == 1)
         {
          
             $accept_proposal['status'] 	= 1;
             $accept_proposal['freelancer_acceptance'] = 1;
             $cond_sub = [];
             $cond_sub['user_id'] = $user->id;
             $cond_sub['active'] = 1;
             $user_sub = UserSubscription::where($cond_sub)->with('membership')->orderBy('id','DESC')->first();
          
             if(isset($user_sub) && $user_sub != ""){
                 $freelancer_service_charge = $user_sub->service_percentage;
               //  dd($freelancer_service_charge);
             }
         }else{
             $freelancer_service_charge = null;
         }
 
 
 
         return view('user.registration.dashboard',compact('open_projects','user','freelancer_service_charge','inprogress_projects','completed_projects'));
      }

  
}
