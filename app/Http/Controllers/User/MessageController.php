<?php

namespace App\Http\Controllers\User;

use DB;
use Auth;
use App\User;
use App\Admin;
use App\Dispute;
use App\Project;
use Carbon\Carbon;
use App\DisputeChat;
use App\ProjectChat;
use App\EmailTemplate;
use Illuminate\Http\Request;
use App\Services\ProcessEmailQueue;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\Types\Null_;

class MessageController extends Controller
{
     /**
     * message:List all messages for a particular project
     * Params : project and user object information as parameter
     * Response : Redirected to message list for a particular project
     */
    public function message(Project $project, User $freelancer)
    {

        $user = auth('web')->user();
        $user_id = $user->id;
        $freelancer_id = $freelancer->id;

        if (auth('web')->check()) {



            if (!isset($freelancer->uuid)) {
                $getuser = ProjectChat::where('project_id', $project->id)->where('archive_status', 0)
                    ->where(function ($q) use ($user_id) {
                        $q->Where('from_user_id', $user_id)->orWhere('to_user_id', $user_id);
                    })->orderby('created_at', 'desc')->first();
                if (!isset($getuser)) {
                    $getuser = ProjectChat::where('project_id', $project->id)->where('archive_status', 1)
                        ->where(function ($q) use ($user_id) {
                            $q->Where('from_user_id', $user_id)->orWhere('to_user_id', $user_id);
                        })->orderby('created_at', 'desc')->first();
                }
                if (!empty($getuser->from_user_id) && $getuser->from_user_id != $user_id) {
                    $to_user_id = $getuser->from_user_id;
                    $freelancer = $getuser->sender;
                } else {
                    if (!empty($getuser->to_user_id)) {
                        $to_user_id = $getuser->to_user_id;
                        $freelancer = $getuser->recipient;
                    }
                }
            } else {
                $to_user_id = $freelancer->id;
            }




            $archive_highlights = ProjectChat::from('project_chats AS m')
                ->select('m.*')->with('sender', 'recipient')
                ->join(DB::raw('(select least(from_user_id, to_user_id) as user_1,greatest(from_user_id, to_user_id) as user_2,max(ID) as last_id,max(created_at) as last_timestamp from project_chats where project_id=' . $project->id . ' and archive_status=1
           
                group by
            least(from_user_id, to_user_id),
            greatest(from_user_id, to_user_id) )  as s'), function ($q) {
                    $q->on('m.id', '=', 's.last_id');
                })->orderby('created_at', 'desc')
                ->where(function ($q) use ($user_id) {
                    $q->Where('m.to_user_id', $user_id)->orWhere('m.from_user_id', $user_id);
                })->get();
            if (auth()->user()->user_type == 0) {
                $where = 'where project_id=' . $project->id . ' and archive_status=0';
            } else {
                $where = 'where project_id=' . $project->id;
            }

            $non_archive_highlights = ProjectChat::from('project_chats AS m')
                ->select('m.*')->with('sender', 'recipient')
                ->join(DB::raw('(select least(from_user_id, to_user_id) as user_1,greatest(from_user_id, to_user_id) as user_2,max(ID) as last_id,max(created_at) as last_timestamp from project_chats ' . $where . '
           
                group by
            least(from_user_id, to_user_id),
            greatest(from_user_id, to_user_id) )  as s'), function ($q) {
                    $q->on('m.id', '=', 's.last_id');
                })->orderby('created_at', 'desc')
                ->where(function ($q) use ($user_id) {
                    $q->Where('m.to_user_id', $user_id)->orWhere('m.from_user_id', $user_id);
                })->get();

            // dd($non_archive_highlights); 


            // Dispute chats
            $dispute_chats = DisputeChat::where('project_id', $project->id)
                ->where(function ($q) use ($user_id,  $freelancer_id) {
                    $q->where(function ($q) use ($user_id) {
                        $q->where('from_user_id', '=', $user_id);
                    })->orWhere(function ($q) use ($freelancer_id) {
                        $q->where('from_user_id', '=', $freelancer_id);
                    });
                })
                ->orderBy('created_at', 'DESC')->groupBy('dispute_id')->get();


            //DB::enableQueryLog();
            if (isset($to_user_id) && !empty($to_user_id)) {
                $chats = ProjectChat::with('sender', 'recipient')
                    ->where('project_id', $project->id)
                    ->where(function ($q) use ($user_id, $to_user_id) {
                        $q->where(function ($q) use ($user_id, $to_user_id) {
                            $q->where('from_user_id', '=', $user_id)
                                ->where('to_user_id', '=', $to_user_id);
                        })->orWhere(function ($q) use ($user_id, $to_user_id) {
                            $q->where('from_user_id', '=', $to_user_id)
                                ->where('to_user_id', '=', $user_id);
                        });
                    })->get();
                $to_count = $chats->count();
            } else {
                $chats = [];
                $to_count = 0;
            }
            $admin = Admin::where('id', 1)->first();
            return view('user.messages.message_list', compact('to_count', 'freelancer', 'admin', 'non_archive_highlights', 'chats', 'project', 'archive_highlights', 'dispute_chats'));
        } else {
            return redirect()->route('home');
        }
    }

      /**
     * message:List dispute for a particular project
     * Params : dispute and project object information as parameter
     * Response : Redirected to dispute list page for a particular project
     */

    public function disputeMessage($dispute, $project)
    {
        $user = auth('web')->user();
        $dispute = Dispute::where('id', $dispute)->first();
        $project = Project::where('id', $project)->first();
        $user_id = $user->id;
        if($user->user_type == 0){
        $freelancer_id = $dispute->freelancer->id;
        }
        else{
            $freelancer_id = $dispute->client->id;
        }
        $admin = Admin::where('id', 1)->first();

        if (auth('web')->check()) {

            if (!isset($freelancer_id)) {
                $getuser = DisputeChat::where('project_id', $project->id)
                    ->where(function ($q) use ($user_id, $admin, $freelancer_id) {
                        $q->Where('from_user_id', $user_id)->orWhere('from_user_id', $admin->id)->orWhere('from_user_id', $freelancer_id);
                    })->orderby('created_at', 'desc')->first();

                if (!empty($getuser->from_user_id) && $getuser->from_user_id != $admin->id) {
                    $to_user_id = $getuser->from_user_id;
                    $freelancer = $getuser->sender;
                } else {
                    if (!empty($getuser->to_user_id)) {
                        $to_user_id = $getuser->to_user_id;
                        $freelancer = $getuser->recipient;
                    }
                }
            } else {
                $to_user_id = $freelancer_id;
            }
            $non_archive_highlights = [];
            if (auth()->user()->user_type == 0) {
                $where = 'where project_id=' . $project->id . ' and archive_status=0';
            } else {
                $where = 'where project_id=' . $project->id;
            }
            $non_archive_highlights = ProjectChat::from('project_chats AS m')
                ->select('m.*')->with('sender', 'recipient')
                ->join(DB::raw('(select least(from_user_id, to_user_id) as user_1,greatest(from_user_id, to_user_id) as user_2,max(ID) as last_id,max(created_at) as last_timestamp from project_chats ' . $where . '
           
                group by
            least(from_user_id, to_user_id),
            greatest(from_user_id, to_user_id) )  as s'), function ($q) {
                    $q->on('m.id', '=', 's.last_id');
                })->orderby('created_at', 'desc')
                ->where(function ($q) use ($user_id) {
                    $q->Where('m.to_user_id', $user_id)->orWhere('m.from_user_id', $user_id);
                })->get();

            //DB::enableQueryLog();
            if (isset($freelancer_id) && !empty($freelancer_id)) {
                $chats = DisputeChat::where('project_id', $project->id)
                    ->where('dispute_id', $dispute->id)
                    ->where(function ($q) use ($user_id, $admin, $freelancer_id) {
                        $q->where(function ($q) use ($user_id) {
                            $q->where('from_user_id', '=', $user_id);
                        })->orWhere(function ($q) use ($admin) {
                            $q->where('from_user_id', '=', $admin->id);
                        })->orWhere(function ($q) use ($freelancer_id) {
                            $q->where('from_user_id', '=', $freelancer_id);
                        });
                    })->get();
                $to_count = $chats->count();
                if ($to_count > 0) {
                    $latest_msg = DisputeChat::where('project_id', $project->id)
                        ->where('dispute_id', $dispute->id)->latest()->first();
                    $last_msg = $latest_msg->message;
                } else {
                    $last_msg = 'No Messages Found';
                }
            } else {
                $chats = [];
                $to_count = 0;
                $last_msg = 'No Messages Found';
            }
            return view('user.messages.dispute_message_list', compact('last_msg', 'to_count', 'non_archive_highlights', 'chats', 'project', 'admin', 'dispute', 'user'));
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * store_message:For saving messages from project-message/public-message detail form
     * Params : Form input data ($request)
     * Response : If successfull redirected to message listing/public message listing pages
     */

    public function store_message(Request $request)
    {
        $user = auth('web')->user();
        if ($user) {

            //save proposal
            $project_id = $request->project_id;
            $user_id = $user->id;
            $to_user = $request->to_user_id;
            $message = [];
            $message['project_id'] = $project_id;
            $message['from_user_id'] = $user_id;
            $message['to_user_id'] = $to_user;
            $message['message'] = $request->message;
            $message['admin_user_id'] = !empty($request->admin_user_id) ? $request->admin_user_id : Null;
            $message['read_status'] = 0;


            $archive_count = ProjectChat::where('project_id', $project_id)->where('archive_status', 1)
                ->where(function ($q) use ($user_id, $to_user) {
                    $q->where(function ($q) use ($user_id, $to_user) {
                        $q->where('from_user_id', '=', $user_id)
                            ->where('to_user_id', '=', $to_user);
                    })->orWhere(function ($q) use ($user_id, $to_user) {
                        $q->where('from_user_id', '=', $to_user)
                            ->where('to_user_id', '=', $user_id);
                    });
                })->count();


            // dd($message);
            if (isset($request->attachement)) {

                $path = public_path('storage/projects/comment_attachements');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $file = $request->file('attachement');

                $name = uniqid() . '_' . trim($file->getClientOriginalName());

                $file->move($path, $name);

                $message['attachment'] = $name;
            }

            $message_create = ProjectChat::create($message);
            if ($archive_count > 0) {
                ProjectChat::where('project_id', $project_id)
                    ->where(function ($q) use ($user_id, $to_user) {
                        $q->where(function ($q) use ($user_id, $to_user) {
                            $q->where('from_user_id', '=', $user_id)
                                ->where('to_user_id', '=', $to_user);
                        })->orWhere(function ($q) use ($user_id, $to_user) {
                            $q->where('from_user_id', '=', $to_user)
                                ->where('to_user_id', '=', $user_id);
                        });
                    })
                    ->update(['archive_status' => 0]);
            }

            ProjectChat::sendEmailNotification($message_create);

            if ($message_create) {
                if (isset($request->admin_user_id) && $request->admin_user_id != "") {
                    return redirect()->route('message.disputeMessages', array($request->project_uuid, $request->user_uuid));
                } else {
                    if (isset($request->page_status))
                        return redirect()->route('message.PublicMessageDetail', array($request->project_uuid, $request->user_uuid));
                    else
                        return redirect()->route('message.messages', array($request->project_uuid, $request->user_uuid));
                }
            }
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * store_dispute_message:For saving dispute messages
     * Params : Form input data ($request)
     * Response : If successfull redirected to dispute list
     */

    public function store_dispute_message(Request $request)
    {
        $user = auth('web')->user();
        if ($user) {

            //save proposal
            $project_id = $request->project_id;
            $user_id = $user->id;
            $message = [];
            $message['project_id'] = $project_id;
            $message['from_user_id'] = $user_id;
            $message['message'] = $request->message;
            $message['read_status'] = 0;
            $message['dispute_id'] = $request->dispute_id;
            $message['user_type'] = 'user';
            $admin = Admin::where('id', 1)->first();

            if (isset($request->attachement)) {

                $path = public_path('storage/projects/comment_attachements');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $file = $request->file('attachement');

                $name = uniqid() . '_' . trim($file->getClientOriginalName());

                $file->move($path, $name);

                $message['attachment'] = $name;
            }

            $message_create = DisputeChat::create($message);
            $dispute = Dispute::where('id', $request->dispute_id)->first();
            $data = [];
            $email_template = EmailTemplate::where('static_email_heading', 'MESSAGE_SEND')->first();
            $data['action_type'] = 'message_send';
            if ($user->user_type == 0) {
                $emails = [$dispute->freelancer, $admin];
            } else {
                $emails = [$dispute->client, $admin];
            }
            if ($email_template) {
                foreach ($emails as $email) {
                    $email_html = $email_template->template;

                    $email_html = str_replace("[USER_NAME]", ucwords($email->username), $email_html);
                    $email_html = str_replace("[FROM_NAME]", ucwords($user->username), $email_html);
                    $email_html = str_replace("[MESSAGE]", $message_create->message, $email_html);
                    $email_html = str_replace("[PROJECT_NAME]", $message_create->project->title, $email_html);



                    $template = view('admin.email_templates.layout', compact('email_html'));

                    $data['to_email'] = $email->email;
                    $data['to_name'] = ucwords($email->username);
                    $data['subject'] = $email_template->subject;
                    $data['body'] = $template;
                    $data['action_id'] = $message_create->id;

                    $processEmailQueue = new ProcessEmailQueue($data);
                    $processEmailQueue->sendEmail();
                }
            }

            if ($message_create) {
                return redirect()->route('message.disputeMessages', array($request->dispute_id, $request->project_id));
            }
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * changeArchive:Change the message thread status in to archive/non archive
     * Params : project id,user list received from ajax
     * Response : If successfull redirected to message list
     */
    public function changeArchive(Request $request)
    {
        $user = auth('web')->user();
        $user_id = $user->id;
        $project_id = $request->project_id;
        foreach ($request->to_users as $key => $to_user) {
            $chat_user['from_user_id'] = $to_user;
            $chat_user['to_user_id'] = $user_id;

            $chat_user1['from_user_id'] = $user_id;
            $chat_user1['to_user_id'] = $to_user;

            ProjectChat::where('project_id', $project_id)
                ->where(function ($q) use ($user_id, $to_user) {
                    $q->where(function ($q) use ($user_id, $to_user) {
                        $q->where('from_user_id', '=', $user_id)
                            ->where('to_user_id', '=', $to_user);
                    })->orWhere(function ($q) use ($user_id, $to_user) {
                        $q->where('from_user_id', '=', $to_user)
                            ->where('to_user_id', '=', $user_id);
                    });
                })
                ->update(['archive_status' => $request->type]);
        }
    }

      /**
     * publicMessages:Listing public messages
     * Params : Get parameter from request
     * Response : Redirect to public message listing page
     */

    public function publicMessages(Request $request)
    {

        $user = auth('web')->user();
        if ($user) {
            $user_id = $user->id;

            if (isset($request->keyword) && $request->keyword != "") {
                $keyword = $request->keyword;
                $user_ids = User::where('username', 'LIKE', '%' . $keyword . '%')->pluck('id')->toArray();

                //dd($user_ids);
                $project_ids = Project::where('title', 'LIKE', '%' . $keyword . '%')->pluck('id')->toArray();
                $project_ids = implode(",", $project_ids);
                if (!empty($project_ids))
                    $where_search = "(project_id IN($project_ids)";
                else
                    $where_search = "(project_id IN('')";

                $user_ids = implode(",", $user_ids);
                if (!empty($user_ids))
                    $where_search .= "or (to_user_id IN($user_ids) or from_user_id IN($user_ids)))";
                else
                    $where_search .= "or to_user_id IN('') or from_user_id IN(''))";
            } else {
                $where_search = "";
            }

            $archive_highlights = ProjectChat::from('project_chats AS m')
                ->select('m.*')->with('sender', 'recipient')
                ->join(DB::raw('(select least(from_user_id, to_user_id) as user_1,greatest(from_user_id, to_user_id) as user_2,max(ID) as last_id,max(created_at) as last_timestamp from project_chats where archive_status=1
   
        group by
    least(from_user_id, to_user_id),
    greatest(from_user_id, to_user_id),project_id )  as s'), function ($q) {
                    $q->on('m.id', '=', 's.last_id');
                })->orderby('created_at', 'desc')
                ->where(function ($q) use ($user_id) {
                    $q->Where('m.to_user_id', $user_id)->orWhere('m.from_user_id', $user_id);
                })->get();




            if (auth()->user()->user_type == 0) {

                $where = "where archive_status=0";

                if (!empty($where_search)) {
                    $where .= " and $where_search";
                }
            } else {
                if (!empty($where_search)) {
                    $where = "where $where_search";
                } else {
                    $where = '';
                }
            }
            //dd($where);

            $non_archive_highlights = ProjectChat::from('project_chats AS m')
                ->select('m.*')->with('sender', 'recipient')
                ->join(DB::raw('(select least(from_user_id, to_user_id) as user_1,greatest(from_user_id, to_user_id) as user_2,max(ID) as last_id,max(created_at) as last_timestamp from project_chats ' . $where . '
   
        group by
    least(from_user_id, to_user_id),
    greatest(from_user_id, to_user_id),project_id )  as s'), function ($q) {
                    $q->on('m.id', '=', 's.last_id');
                })->orderby('created_at', 'desc')
                ->where(function ($q) use ($user_id) {
                    $q->Where('m.to_user_id', $user_id)->orWhere('m.from_user_id', $user_id);
                })->get();




            return view('user.messages.public_message', compact('archive_highlights', 'non_archive_highlights'));
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * changePublicArchive:Change the public message thread status in to archive/non archive
     * Params : ge project list id's from ajax
     * Response : If successfull redirected to public message list
     */
    public function changePublicArchive(Request $request)
    {
        $user = auth('web')->user();
        $user_id = $user->id;
        foreach ($request->to_users_projects as $key => $to_users_project) {

            $list = explode('_', $to_users_project);
            $project_id = $list[1];
            $to_user = $list[0];
            $chat_user['from_user_id'] = $to_user;
            $chat_user['to_user_id'] = $user_id;

            $chat_user1['from_user_id'] = $user_id;
            $chat_user1['to_user_id'] = $to_user;

            ProjectChat::where('project_id', $project_id)
                ->where(function ($q) use ($user_id, $to_user) {
                    $q->where(function ($q) use ($user_id, $to_user) {
                        $q->where('from_user_id', '=', $user_id)
                            ->where('to_user_id', '=', $to_user);
                    })->orWhere(function ($q) use ($user_id, $to_user) {
                        $q->where('from_user_id', '=', $to_user)
                            ->where('to_user_id', '=', $user_id);
                    });
                })
                ->update(['archive_status' => $request->type]);
        }
    }

        /**
     * PublicMessageDetail:Showing public message details
     * Params : get project and user object information from parameters
     * Response : Redirect to public message details
     */
    public function PublicMessageDetail(Project $project, User $user)
    {

        $to_user_id = $user->id;

        if (auth('web')->check()) {
            $current_user = auth('web')->user();
            $user_id = $current_user->id;
            $chats = ProjectChat::with('sender', 'recipient')
                ->where('project_id', $project->id)
                ->where(function ($q) use ($user_id, $to_user_id) {
                    $q->where(function ($q) use ($user_id, $to_user_id) {
                        $q->where('from_user_id', '=', $user_id)
                            ->where('to_user_id', '=', $to_user_id);
                    })->orWhere(function ($q) use ($user_id, $to_user_id) {
                        $q->where('from_user_id', '=', $to_user_id)
                            ->where('to_user_id', '=', $user_id);
                    });
                })->get();
            $to_count = $chats->count();

            return view('user.messages.public_messages_view', compact('to_count', 'project', 'user', 'chats'));
        } else {
            return redirect()->route('home');
        }
    }
}
