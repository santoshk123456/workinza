<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Skill;
use App\Dispute;
use App\Invoice;
use App\Project;
use App\Category;
use App\Industry;
use Carbon\Carbon;
use App\UserDetail;
use App\ProjectChat;
use App\SubCategory;
use App\ProjectSkill;
use App\EmailTemplate;
use App\ProjectCategory;
use App\ProjectContract;
use App\ProjectDocument;
use App\ProjectIndustry;
use App\ProjectProposal;
use App\ProjectMilestone;
use App\UserSubscription;
use App\ProjectInvitation;
use App\ProjectSubCategory;
use Illuminate\Http\Request;
use App\ProjectPropsalMilestone;
use function PHPSTORM_META\type;
use Intervention\Image\Response;
use App\ProjectProposalMilestone;
use App\Services\ProcessEmailQueue;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

use Illuminate\Support\Facades\Session;
use League\CommonMark\Block\Element\Document;

class ProjectController extends Controller
{
	/**
	 * createProject : Implementing project create form
	 * Params   : Nil
	 * Response : If successfull redireted to project create form
	 * STEP 1   : Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 	: Check the user have active subscrition for creating project if no the redirect to upgrade plan page else step 3
	 * STEP 3 	: Redirect to project create form
	 */
    public function createProject(){
		
        $user = auth('web')->user();
       
		if($user){
			$conditions = [];
			$conditions['active'] = 1;
			
			$conditions['user_id'] = $user->id;
			$user_subscription = UserSubscription::where($conditions)->orderby('id','DESC')->first();
			
			//if($user_subscription != "" && $user_subscription->remaining_project_count > 0){
				$industries = Industry::where('active',1)->get();
				$categories = Category::where('active',1)->get();
				return view('user.project.create_project',compact('industries','categories','user_subscription'));
			// }else{
			// 	return redirect()->route('membership_plan.membershipPlans')->with('toastr', ['type'=>'error','text'=>'You have reached the maximum number of project. You need to upgarade the membership plan for posting a project',]);
			// }
			
		}else{
			return redirect()->route('home');
		}
        
	}
	
	/**
	 * storeProject : For saving Project
	 * Params 	: Form input data ($request)
	 * Response : If successfull redireted to open project listing
	 * STEP 1 	: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 	: Save project, project document and project milestone
	 * STEP 3   : Update the remining project count for a user
	 * STEP 4 	: After successfull project creation, redirect to open project listing
	 * 
	 */
	public function storeProject(Request $request){
		//dd($request->budget_range);

		$range = explode('-',request('budget_range'));
		$budget_from = trim($range[0]);
		$budget_to =trim($range[1]);

		$user = auth('web')->user();
		if($user){
			// For Create new project
			$project = Project::create([
				'title' => $request->title,
				'user_id' => $user->id,
				'location' => $request->location,
				'type' => $request->type,
				'description' => $request->description,
				'budget_from' => $budget_from,
				'budget_to' => $budget_to,
				'timeline_count' => $request->timeline_count,
				'timeline_type' => $request->timeline_type,
				'expected_start_date' => date('Y-m-d',strtotime($request->expected_start_date)),
				'expected_end_date' => date('Y-m-d',strtotime($request->expected_end_date)),
				'bid_start_date' => date('Y-m-d',strtotime($request->bid_start_date)),
				'bid_end_date' => date('Y-m-d',strtotime($request->bid_end_date)),
			]);

			// Syncing all industries, categories, sub categories and skills with pivots table
			$project->industries()->attach($request->industry_ids);
			$project->categories()->attach($request->categories_ids);
			$project->subCategories()->attach($request->sub_categories_ids);
			$project->skills()->attach($request->skills);
			// Check for project document exisist. if exisist save corresponding detail to project_documents table
			if(!empty($request->project_documents)){
				$docArray = [];
				foreach($request->project_documents as $key=>$doc){
					$docArray = [];
					$docArray['project_id'] = $project->id;
					$docArray['file_name'] = $doc;
					
				
					   
					
					ProjectDocument::create($docArray);
				}
			}
			//Save project milestone details
			if(!empty($request->milestone_names)){
				$milestoneArray = [];
				foreach($request->milestone_names as $key=>$milestone){
					if(!empty($milestone))
					{
					$milestoneArray['project_id'] = $project->id;
					$milestoneArray['milestone_name'] = $milestone;
					$milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
					$milestoneArray['start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
					$milestoneArray['end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
					$milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
					$milestoneArray['status'] = 0;
					ProjectMilestone::create($milestoneArray);
				}
				}
			}
			if ($project) {
				//update user subscription
				$conditions = [];
				$conditions['active'] = 1;
				$conditions['user_id'] = $user->id;
				$user_subscription = UserSubscription::where($conditions)->decrement('remaining_project_count');
				//dd("success");
				return redirect()->route('project.listOpenProjects')->with('toastr', ['type'=>'success','text'=>'Project created successfully',]);
			} else {
				//throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
			}
		}else{
			return redirect()->route('home');
		}
	}

	/**
	 * editProject 	: Implementing Edit project feature
	 * Params 		: Project object
	 * Response 	: If successfull redireted to edit project page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Get all the selected categories,industries,subcategories
	 * STEP 3 		: Redirect to edit page
	 */
	
    public function editProject(Project $project){
		$user = auth('web')->user();
		if($user){
			$slctd_industries = $project->industries->pluck('id')->toArray();
			$slctd_categories = $project->categories->pluck('id')->toArray();
			$slctd_sub_categories = $project->subCategories->pluck('id')->toArray();
			$slctd_skills = $project->skills->pluck('id')->toArray();
		
			$industries = Industry::where('active',1)->get();
		
			$categories_list =  Category::select('id','name')->where('active',1)->get();
			$categories = Category::select('id','name')->where('active',1)->whereIn('id',$slctd_categories)->get();
			
			$sub_categories = SubCategory::where('active',1)->whereHas('categories',function($q) use($slctd_categories){
				$q->whereIn('id',$slctd_categories);
			})->get();
			$skills = Skill::where('active',1)->whereHas('subCategories',function($q) use($slctd_sub_categories){
				$q->whereIn('id',$slctd_sub_categories);
			})->get();
			$conditions = [];
			$conditions['active'] = 1;
			
			$conditions['user_id'] = $user->id;
			$user_subscription = UserSubscription::where($conditions)->orderby('id','DESC')->first();
			return view('user.project.edit_project',compact('slctd_industries','categories_list','slctd_categories','slctd_sub_categories','slctd_skills'
											,'industries','user_subscription','categories','sub_categories','skills','project'));
		}else{
			return redirect()->route('home');
		}
        
	}
	
	/**
	 * updateProject: update the project
	 * Params 		: Request object(Form data)
	 * Response 	: If successfull redireted to project detail page
	 * STEP 1		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Update the project,industries,categories,subcategories, milestones, documents as per the request object
	 * STEP 3		: If successfull, redirect to project detail page
	 */
	public function updateProject(Project $project, Request $request){
		$user = auth('web')->user();
		if($user){
			$range = explode('-',request('budget_range'));
			$budget_from = trim($range[0]);
			$budget_to =trim($range[1]);
			$project->update([
				'title' => $request->title,
				'location' => $request->location,
				'type' => $request->type,
				'description' => $request->description,
				'budget_from' => $budget_from,
				'budget_to' => $budget_to,
				'timeline_count' => $request->timeline_count,
				'timeline_type' => $request->timeline_type,
				'expected_start_date' => date('Y-m-d',strtotime($request->expected_start_date)),
				'expected_end_date' => date('Y-m-d',strtotime($request->expected_end_date)),
				'bid_start_date' => date('Y-m-d',strtotime($request->bid_start_date)),
				'bid_end_date' => date('Y-m-d',strtotime($request->bid_end_date)),
			]);

			// Syncing all industries, categories, sub categories and skills with pivots table
			$project->industries()->detach();
			$project->categories()->detach();
			$project->subCategories()->detach();
			$project->skills()->detach();

			// Syncing all industries, categories, sub categories and skills with pivots table
			$project->industries()->attach($request->industry_ids);
			$project->categories()->attach($request->categories_ids);
			$project->subCategories()->attach($request->sub_categories_ids);
			$project->skills()->attach($request->skills);

			// Check for project document exisist. if exisist save corresponding detail to project_documents table
			if(!empty($request->project_documents)){
				$docArray = [];
				foreach($request->project_documents as $key=>$doc){
					$docArray = [];
					$docArray['project_id'] = $project->id;
					$docArray['file_name'] = $doc;
					ProjectDocument::create($docArray);
				}
			}
			ProjectMilestone::whereNotIn('id',$request->milestone_id)->where('project_id',$project->id)->delete();
		
			//Save project milestone details
			if(!empty($request->milestone_names)){
				$milestoneArray = [];
				foreach($request->milestone_names as $key=>$milestone){
					if(isset($request->milestone_id[$key]) && !empty($request->milestone_id[$key])){
						$milestoneArray['milestone_name'] = $milestone;
						$milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
						$milestoneArray['start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
						$milestoneArray['end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
						$milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
						ProjectMilestone::where('id',$request->milestone_id[$key])->update($milestoneArray);
					}else{
						$milestoneArray['project_id'] = $project->id;
						$milestoneArray['milestone_name'] = $milestone;
						$milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
						$milestoneArray['start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
						$milestoneArray['end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
						$milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
						$milestoneArray['status'] = 0;
						ProjectMilestone::create($milestoneArray);
					}
				}
			}
			if ($project) {
				if($project->status == 0 || $project->status == 1 ){
					return redirect()->route('project.projectDetails',$project)->with('toastr', ['type'=>'success','text'=>'Project updated successfully',]);
				}else if($project->status == 2 || $project->status == 3){
					return redirect()->route('project.inProgressProjectDetails',$project)->with('toastr', ['type'=>'success','text'=>'Project updated successfully',]);
				}
				
			} else {
				throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
			}
		}else{
			return redirect()->route('home');
		}
	}

	/**
	 * listOpenProjects 
	 * Params 		: Request object(search filter)
	 * Response 	: If successfull redireted to open project listing
	 * STEP 1		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Check any filter is applied, then use that filter condition for listing projects
	 * STEP 3		: If successfull, redirect to listing page
	 */
	public function listOpenProjects(Request $request){
		$user = auth('web')->user();
		if($user){
			
			$open_projects = Project::where('user_id',$user->id)->whereIn('status', [0, 1]);//->paginate(1);
			if(isset($request->title) && $request->title != ""){
				$open_projects = $open_projects->where('title', 'LIKE', '%' . $request->title . '%');
			}
			if(isset($request->status)){
				if($request->status == 0){
					$open_projects = $open_projects->where('status','=',0);
				}else if($request->status == 1){
					$open_projects = $open_projects->where('status','=',1);
				}
			}

			if(isset($request->proposal_end_date)){
				$dates = explode(' - ', request('proposal_end_date'));
				$parsed_date1 = strtotime(str_replace('/', '-', $dates[0]));
				$parsed_date2 = strtotime(str_replace('/', '-', $dates[1]));
				$from_date =date("Y-m-d",$parsed_date1);
				$to_date =date("Y-m-d",$parsed_date2);
				$open_projects = $open_projects->where('bid_end_date', '>', $from_date)->where('bid_end_date', '<=', $to_date);
			
			}
			$open_projects = $open_projects->with('proposals')->orderBy('id', 'DESC')->paginate(10);
			return view('user.project.open_projects',compact('open_projects'));
			
		}else{
			return redirect()->route('home');
		}
	}

	
	/**
	 * freelancerlistInProgressProjects
	 * Params 		: Request object(search filter)
	 * Response 	: If successfull redireted to inprogress project listing
	 * STEP 1		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Check any filter is applied, then use that filter condition for listing inprogress projects
	 * STEP 3		: If successfull, redirect to listing page
	 */
	public function freelancerlistInProgressProjects(Request $request){

		Session::forget("left_page");
		Session::push("left_page",2);
       	$user = auth('web')->user();
		if($user){
			
			$inprogress_projects = Project::whereIn('status', [2, 3]);//->paginate(1);
			$inprogress_projects = $inprogress_projects->with('contracts')->whereHas('contracts', function($query)  {
				$query->where([['freelancer_id', '=',auth('web')->user()->id]]);
			});
			if(isset($request->title) && $request->title != ""){
				$inprogress_projects = $inprogress_projects->where('title', 'LIKE', '%' . $request->title . '%');
			}
			if(isset($request->status)){
				if($request->status == 2){
					$inprogress_projects = $inprogress_projects->where('status','=',2);
				}else if($request->status == 3){
					$inprogress_projects = $inprogress_projects->where('status','=',3);
				}
			}
			
			$inprogress_projects =  $inprogress_projects->orderBy('id', 'DESC')->paginate(10);
			return view('user.project.freelancer_inprogress_projects',compact('inprogress_projects'));
			
		}else{
			return redirect()->route('home');
		}
	}
	/**
	 * listInProgressProjects
	 * Params 		: Request object(search filter)
	 * Response 	: If successfull redireted to inprogress project listing
	 * STEP 1		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Check any filter is applied, then use that filter condition for listing inprogress projects
	 * STEP 3		: If successfull, redirect to listing page
	 */
	public function listInProgressProjects(Request $request){

		
		$user = auth('web')->user();
		if($user){
			
			$inprogress_projects = Project::where('user_id',$user->id)->whereIn('status', [2, 3]);//->paginate(1);

			if(isset($request->title) && $request->title != ""){
				$inprogress_projects = $inprogress_projects->where('title', 'LIKE', '%' . $request->title . '%');
			}

			if(isset($request->proposal_end_date)){
				$dates = explode(' - ', request('proposal_end_date'));
				$parsed_date1 = strtotime(str_replace('/', '-', $dates[0]));
				$parsed_date2 = strtotime(str_replace('/', '-', $dates[1]));
				$from_date =date("Y-m-d",$parsed_date1);
				$to_date =date("Y-m-d",$parsed_date2);
							
				$inprogress_projects = $inprogress_projects->where('bid_start_date', '>', $from_date)->where('bid_end_date', '>', $to_date);
			
			}
			
			$inprogress_projects = $inprogress_projects ->with('milestones','contracts')
			->whereHas('contracts', function($query)   {
				$query->where('deleted_at', '=',NULL);
			});
			$inprogress_projects =  $inprogress_projects->orderBy('id', 'DESC')->paginate(10);
			
			
			
			return view('user.project.inprogress_projects',compact('inprogress_projects'));
			
		}else{
			return redirect()->route('home');
		}
	}
	/**
	 * listCompletedProjects
	 * Params 		: Request object(search filter)
	 * Response 	: If successfull redireted to completed project listing
	 * STEP 1		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Check any filter is applied, then use that filter condition for listing completed projects
	 * STEP 3		: If successfull, redirect to listing page
	 */
	public function listCompletedProjects(Request $request){
		
		Session::forget("left_page");
		Session::push("left_page",3);
		$user = auth('web')->user();
		if($user){
			
			$completed_projects = "";
			if($user->user_type == 1){
				$completed_projects = Project::whereIn('status', [4,5])->with('contracts')->whereHas('contracts', function($query) use($user)  {
					$query->where([['status', '=',1],['freelancer_id', '=',$user->id]]);
				});
			
		    }else{
				$completed_projects = Project::where('user_id',$user->id)->whereIn('status', [4,5]);//->paginate(1);
			}

			if(isset($request->title) && $request->title != ""){
				$completed_projects = $completed_projects->where('title', 'LIKE', '%' . $request->title . '%');
			}
			if(isset($request->status)){
				if($request->status == 0){
					$completed_projects = $completed_projects->where('status','=',4);
				}else if($request->status == 1){
					$completed_projects = $completed_projects->where('status','=',5);
				}
			}
			$completed_projects = $completed_projects->orderBy('updated_at','DESC')->paginate(10);


			if($completed_projects->count()== 0){
				$completed_project="null";
				return view('user.project.completed_projects',compact('completed_projects','completed_project','user'));
			}
			return view('user.project.completed_projects',compact('completed_projects','user'));
		
		}else{
			return redirect()->route('home');
		}
	}

	/**
	 * projectDetails 	: For showing detail of the project
	 * Params 			: Project object
	 * Response 		: If successfull redireted to project detail page
	 * STEP 1			: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2			: Calculate the average proposal amount
	 * STEP 3			: If successfull, redirect to detail page with project object
	 *  
	 */
	public function projectDetails(Project $project){
		$user = auth('web')->user();
		if($user){
			$proposals = ProjectProposal::where('project_id',$project->id)->get();
			$total_propsals = ProjectProposal::where('project_id',$project->id)->sum('proposal_amount');
			$average_proposal_amount = 0;
			$total_milestone_amount = ProjectMilestone::where('project_id',$project->id)->sum('milestone_amount');
			if($total_propsals > 0 ){
				$average_proposal_amount = $total_propsals/count($proposals);
			}
			
			$documents = ProjectDocument::where('project_id',$project->id)->get();

			return view('user.project.project_details',compact('project','total_milestone_amount','proposals','average_proposal_amount','documents'));
			
		}else{
			return redirect()->route('home');
		}
	}

	/**
	 * viewInvitation 	: For displaying the invitation details freelancer side
	 * Params 			: Project object
	 * Response 		: If successfull redireted to invitation detail page
	 * STEP 1			: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2			: Calculate the average proposal amount
	 * STEP 3			: If successfull, redirect to detail page with project object
	 */
	public function viewInvitation(Project $project){
		$user = auth('web')->user();
		if($user){
			$projectInvitations = ProjectInvitation::where('freelancer_id',$user->id)->where('status',0)->get();
			$proposals = ProjectProposal::where('project_id',$project->id)->get();
			$total_propsals = ProjectProposal::where('project_id',$project->id)->sum('proposal_amount');
			$total_milestone_amount = ProjectMilestone::where('project_id',$project->id)->sum('milestone_amount');
			$average_proposal_amount = 0;
			if($total_propsals > 0 ){
				$average_proposal_amount = $total_propsals/count($proposals);
			}
			
			$documents = ProjectDocument::where('project_id',$project->id)->get();
			

			return view('user.project.invitation_details',compact('project','proposals','average_proposal_amount','documents','total_milestone_amount','projectInvitations'));
			
		}else{
			return redirect()->route('home');
		}
	}
	/**
	 * inProgressProjectDetails  : For displaying inprogress project details
	 * Params 			: Project object
	 * Response 		: If successfull redireted to project detail page
	 * STEP 1			: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2			: Calculate the average proposal amount
	 * STEP 3			: If successfull, redirect to detail page with project object
	 *  
	 */
	
	public function inProgressProjectDetails(Project $project){
		$user = auth('web')->user();
		if($user){
			
			$project = Project::where('id',$project->id)->with('contracts.user.userDetail')->first();
			$proposals = ProjectProposal::where('project_id',$project->id)->get();
			$total_propsals = ProjectProposal::where('project_id',$project->id)->sum('proposal_amount');
			$total_milestone_amount = ProjectMilestone::where('project_id',$project->id)->sum('milestone_amount');
			$average_proposal_amount = 0;
			if($total_propsals > 0 ){
				$average_proposal_amount = $total_propsals/count($proposals);
			}
			
			
			return view('user.project.inprogress-project-details',compact('project','total_milestone_amount','user','proposals','total_propsals','average_proposal_amount'));
		}else{
			return redirect()->route('home');
		}
	}
	/**
	 * completedProjectDetails  : For displaying completed project details
	 * Params 			: Project object
	 * Response 		: If successfull redireted to project detail page
	 * STEP 1			: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2			: Calculate the average proposal amount
	 * STEP 3			: If successfull, redirect to detail page with project object
	 *  
	 */
	public function completedProjectDetails(Project $project){
		$user = auth('web')->user();
		if($user){
			
			$project = Project::where('id',$project->id)->with('contracts.user.userDetail')->first();
			$proposals = ProjectProposal::where('project_id',$project->id)->get();
			$total_propsals = ProjectProposal::where('project_id',$project->id)->sum('proposal_amount');
			$total_milestone_amount = ProjectMilestone::where('project_id',$project->id)->sum('milestone_amount');
			$average_proposal_amount = 0;
			if($total_propsals > 0 ){
				$average_proposal_amount = $total_propsals/count($proposals);
			}
			
			
			return view('user.project.complete-project-details',compact('project','total_milestone_amount','user','proposals','total_propsals','average_proposal_amount'));
		}else{
			return redirect()->route('home');
		}
	}

	/**
	 * Project Proposals: List project proposal
	 * Params 			: Project object
	 * Response 		: If successfull redireted to project proposal page 
	 * STEP 1			: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2			: Get the proposal corresponding to that project
	 * STEP 3			: If successfull, redirect to proposal listing
	 */

	 public function projectProposals(Project $project){
		$user = auth('web')->user();
	
		$proposals = ProjectProposal::with('project','user','projectContract')->where('project_id',$project->id)->paginate(10);
		
		if($user){
			return view('user.project.project_proposals',compact('project','proposals'));
		}else{
			return redirect()->route('home');
		}
	 }

	 /**
	  * inviteFreelancers : for displaying freelancer list for project invitation
	  * Params 			: Project object
	  * Response 		: If successfull redireted to freelancer listing page 
	  * STEP 1			: Check the user authentication if yes goto STEP 2  else redirect to home
	  * STEP 2			: Get the freelancer listing page
	  * STEP 3			: If successfull, redirect to freelancers listing
	  */
	
	  public function inviteFreelancers(Project $project){
		$user = auth('web')->user();
		if($user){
		$freelancers = User::where('user_type',1)->with(
			['projectInvitation' => function($p)use($project){
					$p->where('project_id',$project->id);
				},
			])->orderby('id','DESC')->paginate(10);
		
		
			return view('user.project.invite_freelancers',compact('project','freelancers'));
		}else{
			return redirect()->route('home');
		}
	  }

	  /**
	   * search freelancers
	   * Params 		: Project object , request object
	   * Response 		: If successfull redireted to freelancer listing page 
	   * STEP 1			: Check the user authentication if yes goto STEP 2  else redirect to home
	   * STEP 2			: Get the freelancer listing page with search filter
	   * STEP 3			: If successfull, redirect to freelancers listing
	   */


	  public function searchFreelancers(Request $request,Project $project){
		$search = '';
		if($request->has('search')){
			$search = $request->search;
		}
		
		
		$freelancers = User::with([
			'skills',
			'projectInvitation' => function($p)use($project){
				$p->where('project_id','=',$project->id);
			},
			'userDetail' => function($q){
				$q->select('profile_headline','user_id','company_tagline','city','state');
			}
		])->where([['user_type',1],
		
		[
			function($q) use ($search){
				if(!empty($search)){
					$q->Where('users.full_name','LIKE','%' . $search. '%');
				}else   {
					$q;
				}
			}
		]
		
		])->paginate(5)
		->appends(request()->all());
		
		return view('user.project.invite_freelancers',compact('freelancers','project'));

	  }

	/**
	 * sendInvitationToFreelancer
	 * Params 		: Project object , request object
	 * Response 	: If successfull return response
	 * STEP 1		: Check any invitation  already there for freelancer if yes GOTO step4 else STEP 2
	 * STEP 2		: Create an entry in ProjectInviations table
	 * STEP 3		: Send mail to freelancer
	 * STEP 4		: return proper response
	 */
	public function sendInvitationToFreelancer(Request $request,Project $project){
		$resultArr = [];
		$user = auth('web')->user();
		if($user){
			$conditions = [];
			$conditions['freelancer_id'] = $request->freelancer_id;
			$conditions['project_id'] = $request->project_id;
			$project_invitations = ProjectInvitation::where($conditions)->first();
			if($project_invitations){
				$resultArr ['success'] = 2;
				$resultArr ['msg'] = "Already invited";
			}else{
				$project = Project::where('id',$request->project_id)->with('projectInvitation')->first();
				$freelancers = User::where('user_type',1)->paginate(5);
				$project_invitation = [];
				$project_invitation['project_id'] = $request->project_id;
				$project_invitation['freelancer_id'] = $request->freelancer_id;
				$project_invitation['status'] = 0;
			
				if(ProjectInvitation::create($project_invitation)){
					//send mail to freelancer
					ProjectInvitation::inviteFreelancer($user,$request,$project);
					$resultArr ['success'] = 1;
					$resultArr ['msg'] = "Invitation sent successfully";
					
					
				}else{
					$resultArr ['success'] = 0;
					$resultArr ['msg'] = "Something went wrong in sending invitations";
				}
			}
			
		}else{
			$resultArr ['success'] = 0;
			$resultArr ['msg'] = "Authentication failed";
		}
		return response()->json($resultArr);
	}

	/**
	 * listInvitations
	 * Params 		: Request object
	 * Response 	: If successfull redirect to invitation listing page
	 * STEP 1 		: Check the user is authenticated or not
	 * STEP 2 		: Get the user inviatations
	 */

	public function listInvitations(Request $request){
		Session::forget("left_page");
		Session::push("left_page",4);
		$user = auth('web')->user();
		if($user){
			$conditions['freelancer_id'] = $user->id;
			$conditions['status'] = 0;
			$invitations = ProjectInvitation::where($conditions);
			if(isset($request->title) && $request->title != ""){
				$invitations = $invitations->whereHas('project',function($q) use($request){
					$q->where('title', 'LIKE', '%' . $request->title . '%');
				});
			}
			$invitations = $invitations->orderBy('id', 'DESC')->paginate(20);
			return view('user.project.invitations',compact('invitations'));
		}else{
			return redirect()->route('login');
		}

	}

	/**
	 * createProposal
	 * Params 		: Project object
	 * Response 	: If successfull redirect to create proposal form
	 * STEP 1 		: Check the user has a valid mebership subscription
	 * STEP 2 		: Check the user has eligible for sending propsal
	 * STEP 3 		: Redirect to create proposal form
	 */
	public function createProposal(Project $project){
	
		$user = auth('web')->user();
		
       	if($user){
			//get the freelancer plan details
			$condition = [];
			$condition['user_id'] = $user->id;
			$condition['active'] = 1;
			$subscription_details =  UserSubscription::where($condition)->first();
			
			
					
			return view('user.project.create_proposal',compact('project','subscription_details'));
			

			
		}else{
			return redirect()->route('home');
		}
	}

	/**
	 * storePropsal
	 * Params 		: Project object, Request object (form data)
	 * Response 	: If successfull redirect to proposal listing
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Create entry in proposal, create proposal milestones,update invitation status
	 * STEP 3 		: Send email notification to client
	 * STEP 4 		: If successfull, redirect to proposal listing
	 */
	public function storeProposal(Project $project,Request $request){
	
		$user = auth('web')->user();
		if($user){
			
			//save proposal
			$proposal = [];
			$proposal['user_id'] = $user->id;
			$proposal['project_id'] = $project->id;
			$proposal['proposal_amount'] = $request->total_amount;
			$proposal['service_percentage'] = $request->service_percentage;
			// if ($request->hasFile('proposal_document'))
			// {
			// 	$proposal_document = $request->file('proposal_document')->store('public/admin/profile-images');
			// 	$proposal['proposal_document'] = str_replace('public/admin/profile-images/','',$proposal_document);
			// }
			
			$proposal['proposal_document'] = $request->comments;
			$proposal['status'] = 0;
			$proposal = ProjectProposal::create($proposal);
			//Save project proposal milestone details
			if(!empty($request->milestone_names)){
				$milestoneArray = [];
				foreach($request->milestone_names as $key=>$milestone){
					$milestoneArray['project_proposal_id'] = $proposal->id;
					$milestoneArray['milestone_name'] = $milestone;
					$milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
					$milestoneArray['milestone_start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
					$milestoneArray['milestone_end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
					$milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
				
					ProjectProposalMilestone::create($milestoneArray);
				}
			}

			//update the project status if it is new to active
			if($project->status == 0){
				$project->update(['status'=>1]);
			}


			//update propsal count in usersubscription
			$condition = [];
			$condition['user_id'] = $project->user_id;
			$condition['active'] = 1;
			$subscription_details =  UserSubscription::where($condition)->decrement('remaining_proposal_count', 1);

			//update project invitation status
			$invitation_cond = [];
			$invitation_cond['project_id'] = $project->id;
			$invitation_cond['freelancer_id'] = $user->id;
			ProjectInvitation::where($invitation_cond)->update(['status'=>1]);
			//Send notiification to Client
		
            $proposal_create_template = EmailTemplate::where('static_email_heading','ACCEPT_INVITATION')->first();

            if ($proposal_create_template) {
                $email_html = $proposal_create_template->template;
				$email_html = str_replace("[USER_NAME]",ucwords($project->user->username), $email_html);
				$email_html = str_replace("[FROM_NAME]", ucwords($user->username), $email_html);
				$email_html = str_replace("[PROJECT]", $project->title, $email_html);
                $email_html = str_replace("[INVITATION_LINK]", route('project.listProposals',$project), $email_html);
				
                $template = view('admin.email_templates.layout', compact('email_html'));
                $data = [];
                $data['to_email'] =$project->user->email;
                $data['to_name'] = ucwords($project->user->username);
                $data['subject'] = $proposal_create_template->subject;
                $data['body'] = $template;
                $data['action_id'] = $project->user->id;
                $data['action_type'] = 'accept_invitation';
               
                $processEmailQueue=new ProcessEmailQueue($data);
                $processEmailQueue->sendEmail();
            }

			return redirect()->route('project.listProposals')->with('toastr', ['type'=>'success','text'=>'Proposal accepted successfully']);

		}else{
			return redirect()->route('home');
		}

	}

	/**
	 * listProposals
	 * Params 		:  Request object (form data)
	 * Response 	: If successfull redirect to proposal listing
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Get the proposal with filter params
	 */
	public function listProposals(Request $request){
		// dd($request->all());
		Session::forget("left_page");
		Session::push("left_page",1);

		$user = auth('web')->user();
		if($user){
		
			$proposals = ProjectProposal::where('user_id',$user->id);
			if(isset($request->title) && $request->title != ""){
				$proposals = $proposals->whereHas('project',function($q) use($request){
					$q->where('title', 'LIKE', '%' . $request->title . '%');
				});
				// $proposals = $proposals->whereHas('user',function($p) use($request){
				// 	$p->where('username', 'LIKE', '%' . $request->title . '%');
				// });
			}
			if(isset($request->status)){
				if($request->status == 0){
					$proposals = $proposals->where('status','=',0);
				}else if($request->status == 1){
					$proposals = $proposals->where('status','=',1);
				}else if($request->status == 2){
					$proposals = $proposals->where('status','=',2);
				}
			}

			if(isset($request->big_end_date)){

				$date = \Carbon\Carbon::parse($request->big_end_date)->toDateString();
				
				
				
				$proposals = $proposals->whereHas('project',function($q) use($date){
					$q->whereDate('bid_end_date', '=',$date);
				});
			
				// whereDate('bid_end_date', '=',$date);
			}
		
			
       


			$proposals = $proposals->whereHas('project',function($q) {
				$q->where('deleted_at',NULL);
			})->with('project')->orderBy('id', 'DESC')->paginate(20);
		
			return view('user.project.list_proposals',compact('proposals','user'));
		}else{
			return redirect()->route('home');
		}
	}

	/**
	 * editProposal
	 * Params 		:  ProjectProposal object 
	 * Response 	: If successfull redirect to proposal edit form
	 */
	public function editProposal(ProjectProposal $projectProposal){
		$user = auth('web')->user();
		if($user){
			
			//get the freelancer plan details
			$condition = [];
			$condition['user_id'] = $user->id;
			$condition['active'] = 1;
			$subscription_details =  UserSubscription::where($condition)->first();

			if(!empty($subscription_details)){
				if($subscription_details->remaining_proposal_count > 0){
					
					return view('user.project.edit_proposal',compact('projectProposal','subscription_details'));
				}else{
					return redirect()->back();
				}
				
			}else{
				return redirect()->back();
			}
			
		}else{
			return redirect()->route('home');
		}		
	}

	/**
	 * updateProposal
	 * Params 		: ProjectProposal object, Request object (form data)
	 * Response 	: If successfull redirect to proposal listing
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2		: Update proposal, proposal milestone and if successfull redirect to listing
	 */
	public function updateProposal(ProjectProposal $projectProposal,Request $request){
		$user = auth('web')->user();
		if($user){
			//save proposal
			$proposal = [];
			$proposal['proposal_amount'] = $request->total_amount;
			$proposal['service_percentage'] = $request->service_percentage;
			$proposal['proposal_document'] = $request->proposal_document;
			$projectProposal->update($proposal);
			//Save/edit project proposal milestone details
			if(!empty($request->milestone_names)){
				$milestoneArray = [];
				foreach($request->milestone_names as $key=>$milestone){
					if(isset($request->milestone_id[$key]) && !empty($request->milestone_id[$key])){
						$milestoneArray['milestone_name'] = $milestone;
						$milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
						$milestoneArray['milestone_start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
						$milestoneArray['milestone_end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
						$milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
						
						ProjectProposalMilestone::where('id',$request->milestone_id[$key])->update($milestoneArray);
					}else{
						$milestoneArray['project_proposal_id'] = $projectProposal->id;
						$milestoneArray['milestone_name'] = $milestone;
						$milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
						$milestoneArray['milestone_start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
						$milestoneArray['milestone_end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
						$milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
					
						ProjectProposalMilestone::create($milestoneArray);
					}
				}
			}
			if ($proposal) {
				return redirect()->route('project.listProposals')->with('toastr', ['type'=>'success','text'=>'Project updated successfully']);
				
			} else {
				
			}
		}else{
			return redirect()->route('home');
		}
	}
	/**
	 * prepareContract
	 * Params 		: ProjectContract object
	 * Response 	: If successfull redirect to contract detail page
	 */
	public function prepareContract(ProjectContract $projectContract){
		$user = auth('web')->user();
		if($user){
			
		//	$user_details = UserDetail::where('user_id',$user->id)->first();
			
			//get contract details
			$contract_details = ProjectContract::where('uuid',$projectContract->uuid)->with('user','project')->first();
			$proposal_milestones = ProjectProposalMilestone::where('project_proposal_id',$contract_details->project_proposal_id)->get();
			return view('user.project.prepare_contract',compact('contract_details','user','proposal_milestones'));
		}else{
			return redirect()->route('home');
		}
	}
	/**
	 * acceptProposal
	 * Params 		: ProjectContract object
	 * Response 	: If successfull redirect to contract detail page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2 		: Handle contract acceptance as per user type
	 * STEP 3 		: Send mail notification
	 */
	public function acceptProposal(Request $request){
		$user = auth('web')->user();
		if($user){
			//get Propsal details
		
			$proposal_details = ProjectProposal::where('id',$request->proposal_id)->with('proposalMilestones','project')->first();
			$projectContract  = ProjectContract::where('id',$request->contract_id)->first();
			$accept_proposal = []; 
			
			if($user->user_type == 0){
				$accept_proposal['client_acceptance'] = 1;
				$accept_proposal['status'] = 1;
			}else{
				$accept_proposal['status'] 	= 1;
				$accept_proposal['freelancer_acceptance'] = 1;
				//get the service charge of freelancer
				$cond_sub = [];
				$cond_sub['user_id'] = $user->id;
				$cond_sub['active'] = 1;
				$user_sub = UserSubscription::where($cond_sub)->with('membership')->orderBy('id','DESC')->first();
				if(isset($user_sub) && $user_sub != ""){
					$accept_proposal['freelancer_service_charge'] = $user_sub->service_percentage;
				}
			}
			if($contract = ProjectContract::where('id',$request->contract_id)->update($accept_proposal)){
				if($user->user_type == 0){
					//Sent mail to freelancer
					//Replace project milestone.amount with propsal milestone and proposal amount
					$update_project = [];
					$update_project['status'] = 2;
					$update_project['amount'] = $proposal_details->proposal_amount;
					Project::where('id',$proposal_details->project_id)->update($update_project);
					ProjectContract::sendEmailNotification($user,$request,$proposal_details,$user->user_type,$projectContract);
					return redirect()->route('project.listInProgressProjects')->with('toastr', ['type'=>'success','text'=>'Project contract created successfully']);
				}else{
					$update_project = [];
					$update_project['status'] = 3;
					
					Project::where('id',$proposal_details->project_id)->update($update_project);
					ProjectMilestone::where('project_id',$proposal_details->project_id)->delete();
					//Save project proposal milestone details
					if(!empty($proposal_details->proposalMilestones)){
						$milestoneArray = [];
						foreach($proposal_details->proposalMilestones as $key=>$milestone){
							$milestoneArray['project_id'] = $proposal_details->project_id;
							$milestoneArray['milestone_name'] = $milestone->milestone_name;
							$milestoneArray['milestone_amount'] = $milestone->milestone_amount;
							$milestoneArray['start_date'] = date('Y-m-d',strtotime($milestone->milestone_start_date));
							$milestoneArray['end_date'] = date('Y-m-d',strtotime($milestone->milestone_end_date));
							$milestoneArray['milestone_description'] = $milestone->milestone_description;
							$milestoneArray['status'] = 0;
						
							ProjectMilestone::create($milestoneArray);
						}
					}
					///////archive all messages associated with this project////////
					$freelancer_id=$user->id;
					$client_id=$proposal_details->project->user_id;
					
					ProjectChat::where('project_id', $proposal_details->project_id)
					->where(function ($q) use ($freelancer_id, $client_id) {
						$q->where(function ($q) use ($freelancer_id, $client_id) {
							$q->where('from_user_id', '<>', $freelancer_id)
								->orWhere('to_user_id', '<>', $client_id);
						})->Where(function ($q) use ($freelancer_id, $client_id) {
							$q->where('from_user_id', '<>', $client_id)
								->orWhere('to_user_id', '<>', $freelancer_id);
						});
					})
					->update(['archive_status' => 1]);

					////////////end////////////
					//Sent mail to client
					ProjectContract::sendEmailNotification($user,$request,$proposal_details,$user->user_type,$projectContract);
					return redirect()->route('project.freelancerlistInProgressProjects')->with('toastr', ['type'=>'success','text'=>'Contract accepted successfully']);
				}
			}
		}else{
			return redirect()->route('home');
		}
	}
	/**
	 * createContract : for creating a contract page 
	 * Params 		: Request object
	 * Response 	: If successfull return proper response
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : create contract if successfull, return proper response
	 */

	public function createContract(Request $request){
		$user = auth('web')->user();
		if($user){
			
			if(isset($request->contract_id) && $request->contract_id !=""){
				
				$contract = ProjectContract::where('id',$request->contract_id)->first();
				$resultArr ['success'] = 1;
				$resultArr ['uuid'] = $contract->uuid;
				$resultArr ['msg'] = "Accepted successfully";
			}else{
				$accept_proposal = []; 
				$accept_proposal['project_id'] 				= $request->project_id;
				$accept_proposal['project_proposal_id'] 	= $request->project_proposal_id;
				$accept_proposal['client_id'] 				= $user->id;
				$accept_proposal['freelancer_id'] 			= $request->freelancer_id;
				$accept_proposal['amount'] 					= $request->proposal_amount;
				$accept_proposal['status'] 					= 0;
				$accept_proposal['client_acceptance'] 		= 0;
				$accept_proposal['freelancer_acceptance'] 	= 0;
				
				if($contract = ProjectContract::create($accept_proposal)){
					$resultArr ['success'] = 1;
					$resultArr ['uuid'] = $contract->uuid;
					$resultArr ['msg'] = "Accepted successfully";
				}else{
					$resultArr ['success'] = 0;
					$resultArr ['msg'] = "Something went wrong on accepting proposal";
				}
			}
			
		}else{
			$resultArr ['success'] = 0;
			$resultArr ['msg'] = "Authentication failed";
		}
		return response()->json($resultArr);
	}

	/**
	 * listContracts
	 * Params 		: Request object
	 * Response 	: If successfull return proper response
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : List the contracts
	 */
	public function listContracts(Request $request){
		$user = auth('web')->user();
		if($user){
			$contracts = ProjectContract::where('freelancer_id',$user->id)->paginate(20);
			return view('user.project.list_contracts',compact('contracts','user'));
		}else{
			return redirect()->route('home');
		}
	}
	/**
	 * fileUpload
	 * Params 		: Request object
	 * Response 	: If successfull return proper response
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Upload the file
	 */
	public function fileUpload(Request $request){
        $path = public_path ('storage/projects/project_documents');
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
	}

	/**
	 * addDocument
	 * Params 		: Project object
	 * Response 	: If successfull redirect to document page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Redirect to document page
	 */


	public function addDocument(Project $project){
		
        $user = auth('web')->user();
       
		if($user){
			return view('user.project.add_document',compact('project'));
		}else{
			return redirect()->route('home');
		}
        
	}
	/**
	 * storeDocument
	 * Params 		: Project object
	 * Response 	: If successfull project detail page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Redirect to project detail page
	 */

	public function storeDocument(Request $request){
		
        $user = auth('web')->user();
       
		if($user){
			if(!empty($request->project_documents)){
				$docArray = [];
				foreach($request->project_documents as $key=>$doc){
					$docArray = [];
					$docArray['project_id'] = $request->project_id;
					$docArray['file_name'] = $doc;
					ProjectDocument::create($docArray);
				}
			}
			if($request->status == 0 || $request->status == 1){
				return redirect()->route('project.projectDetails',$request->uuid)->with('toastr', ['type'=>'success','text'=>'Document added successfully',]);
			}else if($request->status == 2 || $request->status == 3){
				return redirect()->route('project.inProgressProjectDetails',$request->uuid)->with('toastr', ['type'=>'success','text'=>'Document added successfully',]);
			}

			
		}else{
			return redirect()->route('home');
		}
        
	}
	/**
	 * destroyDocuments
	 * Params 		: ProjectDocument object
	 * Response 	: If successfull project detail page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Destroy document and Redirect to project detail page
	 */

	public function destroyDocuments(ProjectDocument $id)
	{
		$user = auth('web')->user();
       	if($user){
			$document = ProjectDocument::find($id)->first();
			$file_path = public_path("storage/projects/project_documents/".$document->file_name); 
		
			if(File::exists($file_path)) 
				File::delete($file_path);
			$document->delete();
			return redirect()->back()->with('toastr', ['type'=>'success','text'=>'Document deleted successfully',]);
		}
		else{
			return redirect()->route('home');
		}
	}

	/**
	 * addMilestone
	 * Params 		: Project object
	 * Response 	: If successfull project milestone page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Redirect to milestone add page
	 */
	public function addMilestone(Project $project){
		
        $user = auth('web')->user();
       
		if($user){
			$projectMilestone = ProjectMilestone::where('project_id',$project->id)->orderBy('id', 'desc')->first();
			$startDate =date("m/d/Y",strtotime($projectMilestone->end_date . ' + 1 days'));
			$total_milestone_amount = ProjectMilestone::where('project_id',$project->id)->sum('milestone_amount');
			return view('user.project.add_milestones',compact('project','startDate','total_milestone_amount','project'));
		}else{
			return redirect()->route('home');
		}
        
	}
	/**
	 * storeMilestone
	 * Params 		: Request object
	 * Response 	: If successfull project detail page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Add milestone to that project and redirect to project detail page
	 */
	public function storeMilestone(Request $request)
	{
		$user = auth('web')->user();
       
		if($user){
			if(!empty($request->milestone_names)){
			
				$milestoneArray = [];
				foreach($request->milestone_names as $key=>$milestone){
					if(!empty($milestone))
					{
					$milestoneArray['project_id'] = $request->project_id;
					$milestoneArray['milestone_name'] = $milestone;
					$milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
					$milestoneArray['start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
					$milestoneArray['end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
					$milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
					$milestoneArray['status'] = 0;
					ProjectMilestone::create($milestoneArray);
				}
				}
			}
		return redirect()->route('project.projectDetails',$request->uuid)->with('toastr', ['type'=>'success','text'=>'Milestone added successfully',]);
		}
		else
		{
			return redirect()->route('home');	
		}
	}
	/**
	 * removeFile
	 * Params 		: Request object
	 * Response 	: If successfull return proper response
	 */
	public function removeFile(Request $request)
    {
        $doc= ProjectDocument::where('id',$request->id)->first();
        $path =public_path('storage/projects/project_documents/'. $doc->file_name);
        if(file_exists($path)){
            unlink(  $path );
        }
        $doc->delete();
	}
	/**
	 * destroyProject
	 * Params 		: Project object
	 * Response 	: If successfull return proper response
	 */
	public function destroyProject(Project $project)
	{
		
	// 	if(!empty($project->documents))
	// 	{
	// 		foreach($project->documents as $key=>$doc){
	// 		$path =public_path('storage/projects/project_documents/'. $doc->file_name);
	// 		if(file_exists($path)){
	// 			unlink($path );
	// 		}
	// 	}
	// }
		$project->delete();
			return redirect()->route('project.listOpenProjects')->with('toastr', ['type'=>'success','text'=>'Project deleted successfully',]);
		
	
	}
	/**
	 * hideProject
	 * Params 		: Project object
	 * Response 	: If successfull return proper response
	 */
	public function hideProject(Project $project)
	{
	

		$project->hide_from_public = 1;
		$project->update();
		
			return redirect()->route('project.listOpenProjects')->with('toastr', ['type'=>'success','text'=>'Project hided successfully',]);
		
	
	}
	/**
	 * unhideProject
	 * Params 		: Project object
	 * Response 	: If successfull return proper response
	 */
	public function unhideProject(Project $project)
	{
		
		$project->hide_from_public = 0;
		$project->update();
			return redirect()->route('project.listOpenProjects')->with('toastr', ['type'=>'success','text'=>'Project unhided successfully',]);
		
	
	}
	/**
	 * milestoneDetails
	 * Params 		: ProjectMilestone object
	 * Response 	: If successfull redirect to milestone detail page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Redirect to milestone detail page
	 * 
	 */
	public function milestoneDetails(ProjectMilestone $milestone)
	{
		$user = auth('web')->user();
		if ($user) {
			
			$milestone_comments = ProjectChat::where('project_milestone_id',$milestone->id)->with('sender')->orderBy('id', 'DESC')->get();
		
			return view('user.project.milestone_details',compact('milestone','milestone_comments','user'));
		}
		else
		{
			return redirect()->route('home');
		}

	}
	/**
	 * storeComment : Save chat comments
	 * Params 		: ProjectMilestone object, Request object
	 * Response 	: If successfull redirect to milestone detail page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Redirect to milestone detail page if success
	 * 
	 */
	public function storeComment(ProjectMilestone $milestone,Request $request)
	{
		
		$user = auth('web')->user();
		if ($user) {
			if($user->id!= $milestone->project->contracts->client_id)
			{
				$to_user_id=$milestone->project->contracts->client_id;
			}
			else
			{
				$to_user_id=$milestone->project->contracts->freelancer_id;
			}
			
			$comment = [];
			$comment['project_id'] = $milestone->project_id;
			$comment['project_milestone_id'] = $milestone->id;
			$comment['from_user_id'] = $user->id;
			$comment['to_user_id'] = $to_user_id;
			$comment['message'] = $request->comment;
			$comment['read_status'] = 0;
			if(isset($request->attachement))
			{
			
				$path = public_path ('storage/projects/comment_attachements');
				if (!file_exists($path)) {
					mkdir($path, 0777, true);
				}

				$file = $request->file('attachement');

				$name = uniqid() . '_' . trim($file->getClientOriginalName());

				$file->move($path, $name);
				
				$comment['attachment'] =$name;
			}
		

			$comment_create = ProjectChat::create($comment);
			//send email notiifcation

			return redirect()->route('project.MilestoneDetails',$milestone->id)->with('toastr', ['type'=>'success','text'=>'Comment added successfully',]);

		}
		else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>'Authentication failed']);
		}
	}

	/**
	 * ActivateMilestone
	 * Params 		: Request object
	 * Response 	: If successfull redirect to project detail page
	 * STEP 1 		: Update the status to activated
	 * STEP 2 		: Send mail Notifications
	 */
	public function ActivateMilestone(Request $request){
		$user = auth('web')->user();
		if ($user) {
			
			$update = ProjectMilestone::where('id',$request->id)->update(['status'=>1]);
			
			$project = Project::where('id',$request->project_id)->with('contracts.user.userDetail')->first();
			//send email notification freelancer
			$milestone_start_template = EmailTemplate::where('static_email_heading','ACTIVATE_MILESTONE')->first();
			
            if ($milestone_start_template) {
                $email_html = $milestone_start_template->template;
				$email_html = str_replace("[USER_NAME]",ucwords($project->contracts->user->username), $email_html);
				$email_html = str_replace("[FROM_USER]", ucwords($user->username), $email_html);
				$email_html = str_replace("[PROJECT]", $project->title, $email_html);
				$email_html = str_replace("[MILESTONE_NAME]", $request->milestone_name, $email_html);
                $email_html = str_replace("[LOGIN_LINK]", route('project.MilestoneDetails',$request->id), $email_html);
				
                $template = view('admin.email_templates.layout', compact('email_html'));
                $data = [];
                $data['to_email'] =$project->contracts->user->email;
                $data['to_name'] = ucwords($project->contracts->user->username);
                $data['subject'] = $milestone_start_template->subject;
                $data['body'] = $template;
                $data['action_id'] = $project->contracts->user->id;
                $data['action_type'] = 'activate_milestone';
               
                $processEmailQueue=new ProcessEmailQueue($data);
                $processEmailQueue->sendEmail();
            }
			return redirect()->back()->with('toastr', ['type'=>'success','text'=>'Milestone Activated successfully']);

		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>'Authentication failed']);
		}
	}
	/**
	 * milestoneStatusChange
	 * Params 		: Request object
	 * Response 	: If successfull redirect to project detail page
	 * STEP 1 : Update the status to completed
	 * STEP 2 : Send mail Notifications
	 */
	public function milestoneStatusChange(Request $request){
		
		$user = auth('web')->user();
		if ($user) {
			$message = "";
			$update = ProjectMilestone::where('id',$request->id)->update(['status'=>$request->status]);
		
			$project = Project::where('id',$request->project_id)->with('user','contracts.user.userDetail')->first();
			if($request->status == 3){
				//send email notification client
				$milestone_complete_template = EmailTemplate::where('static_email_heading','COMPLETE_MILESTONE')->first();
		
				if ($milestone_complete_template) {
					$email_html = $milestone_complete_template->template;
					$email_html = str_replace("[USER_NAME]",ucwords($project->user->username), $email_html);
					$email_html = str_replace("[FROM_USER]", ucwords($user->username), $email_html);
					$email_html = str_replace("[PROJECT]", $project->title, $email_html);
					$email_html = str_replace("[MILESTONE_NAME]", $request->milestone_name, $email_html);
					$email_html = str_replace("[LOGIN_LINK]", route('project.MilestoneDetails',$request->id), $email_html);
					
					$template = view('admin.email_templates.layout', compact('email_html'));
					$data = [];
					$data['to_email'] =$project->user->email;
					$data['to_name'] = ucwords($project->user->username);
					$data['subject'] = $milestone_complete_template->subject;
					$data['body'] = $template;
					$data['action_id'] = $project->user->id;
					$data['action_type'] = 'complete_milestone';
				
					$processEmailQueue=new ProcessEmailQueue($data);
					$processEmailQueue->sendEmail();
				}
				
				//invoice generation from freelancer to client
				$input_data = [];
                $input_data['user_id'] = $project->user->id;
                $input_data['type'] = 1;
                $input_data['amount'] = $request->milestone_amount;
                $input_data['notes'] = "Invoice generated for the milestone ".ucwords($request->milestone_name)." for the project ".ucwords($project->title);
				$input_data['status'] = 0;
				$input_data['project_milestone_id'] = $request->id;
				$input_data['project_contract_id'] = $project->contracts->id;
				$input_data['status'] = 0;
                $now_date = \Carbon\Carbon::now();
				$input_data['paid_on'] = $now_date->toDateTimeString();;
				$input_data['due_date'] =  $date = \Carbon\Carbon::today()->subDays(7);
			
                Invoice::invoiceGeneration($input_data,$project->user);

				$message = 'Milestone completed successfully';
			}else if($request->status == 4){
				//send email notification client
				$milestone_complete_template = EmailTemplate::where('static_email_heading','APPROVE_MILESTONE')->first();
				if ($milestone_complete_template) {
					$email_html = $milestone_complete_template->template;
					$email_html = str_replace("[USER_NAME]",ucwords($project->contracts->user->username), $email_html);
					$email_html = str_replace("[FROM_USER]", ucwords($user->username), $email_html);
					$email_html = str_replace("[PROJECT]", $project->title, $email_html);
					$email_html = str_replace("[MILESTONE_NAME]", $request->milestone_name, $email_html);
					$email_html = str_replace("[LOGIN_LINK]", route('project.MilestoneDetails',$request->id), $email_html);
					
					$template = view('admin.email_templates.layout', compact('email_html'));
					$data = [];
					$data['to_email'] =$project->contracts->user->email;
					$data['to_name'] = ucwords($project->contracts->user->username);
					$data['subject'] = $milestone_complete_template->subject;
					$data['body'] = $template;
					$data['action_id'] = $project->contracts->user->id;
					$data['action_type'] = 'approve_milestone';
				
					$processEmailQueue=new ProcessEmailQueue($data);
					$processEmailQueue->sendEmail();
				}
				$message = 'Milestone approved successfully';
			}else if($request->status == 6){
				//send email notification client
				$milestone_complete_template = EmailTemplate::where('static_email_heading','REJECT_MILESTONE')->first();
				if ($milestone_complete_template) {
					$email_html = $milestone_complete_template->template;
					$email_html = str_replace("[USER_NAME]",ucwords($project->contracts->user->username), $email_html);
					$email_html = str_replace("[FROM_USER]", ucwords($user->username), $email_html);
					$email_html = str_replace("[PROJECT]", $project->title, $email_html);
					$email_html = str_replace("[MILESTONE_NAME]", $request->milestone_name, $email_html);
					$email_html = str_replace("[LOGIN_LINK]", route('project.MilestoneDetails',$request->id), $email_html);
					
					$template = view('admin.email_templates.layout', compact('email_html'));
					$data = [];
					$data['to_email'] =$project->contracts->user->email;
					$data['to_name'] = ucwords($project->contracts->user->username);
					$data['subject'] = $milestone_complete_template->subject;
					$data['body'] = $template;
					$data['action_id'] = $project->contracts->user->id;
					$data['action_type'] = 'approve_milestone';
				
					$processEmailQueue=new ProcessEmailQueue($data);
					$processEmailQueue->sendEmail();
				}
			$message = 'Milestone rejected successfully';
			}
			
			return redirect()->back()->with('toastr', ['type'=>'success','text'=>$message]);

		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>'Authentication failed']);
		}
	}

	/**
	 * proposalView
	 * Params 		: ProjectProposal object
	 * Response 	: If successfull redirect to proposal detail page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Redirect to proposal detail page if success
	 */
	public function viewProposal(ProjectProposal $proposal){
		$user = auth('web')->user();
		if ($user) {
			
		
			$total_milestone_amount = ProjectProposalMilestone::where('project_proposal_id',$proposal->id)->sum('milestone_amount');
			return view('user.project.proposal_detail',compact('proposal','user','total_milestone_amount'));
		}else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>'Authentication failed']);
		}
	}

	/**
	 * findProject
	 * Params 		: Request object(filter)
	 * Response 	: If successfull redireted to back to project listing
	 * STEP 1		: Find the projects based the filter requests
	 * STEP 2 		: Save the filter options in session 
	 * STEP 3 		: Filter the query, with session data, if empty of session data filter with request data  
	 * STEP 4		: If successfull, redirect to listing page
	 */
	public function findProject(Request $request){
		$user = auth('web')->user();
		$projectInvitations = ProjectInvitation::where('freelancer_id',$user->id)->where('status',0)->get();
		$min_price = $min_value = 0;
		$max_price = $max_value = 100000000;
		$projects = Project::whereIn('status', [0, 1]);
		$industries = Industry::whereActive(1)->get();
		$categories = Category::whereActive(1)->get();
		$sub_categories = SubCategory::whereActive(1)->get();
		$skills = Skill::whereActive(1)->get();
		// $session_types = !empty(Session::get('types'))?Session::get('types'):[0,1];
		// $session_industries = !empty(Session::get('industries'))?Session::get('industries'):[];
		// $session_categories = !empty(Session::get('categories'))?Session::get('categories'):[];
		// $session_sub_categories = !empty(Session::get('sub_categories'))?Session::get('sub_categories'):[];
		// $session_skills = !empty(Session::get('skills'))?Session::get('skills'):[];
		// $session_sort_value = !empty(Session::get('sort_value'))?Session::get('sort_value'):'latest';
		// searching
		if(isset($request->title) && $request->title != ""){
			$projects = $projects->where('title', 'LIKE', '%' . $request->title . '%');
		}
		// Filtering
		
		if(isset($request->types) && $request->types != ""){
			Session::forget('types');
			Session::push("types",$request->types);
			$session_types = $request->types;
		}
		elseif(isset($request->submitValue) && $request->submitValue != ""){
			Session::forget('types');
			$session_types = [0,1];
		}
		else{
			if(!empty(Session::get('types'))){$session_types = Session::get('types'); }
			else{ $session_types = [0,1]; }
		}
		$projects = $projects->WhereIn('type', $session_types);

		if(isset($request->industries) && $request->industries != ""){
			Session::forget('industries');
			Session::push("industries",$request->industries);
			$session_industries = $request->industries;
		}
		elseif(isset($request->submitValue) && $request->submitValue != ""){
			Session::forget('industries');
			$session_industries = [];
		}
		else{
			if(!empty(Session::get('industries'))){$session_industries = Session::get('industries'); }
			else{ $session_industries = []; }
		}
		$projectIndustries = array_unique(ProjectIndustry::whereIn('industry_id',$session_industries)->pluck('project_id')->toArray());

		if(isset($request->categories) && $request->categories != ""){
			Session::forget('categories');
			Session::push("categories",$request->categories);
			$session_categories = $request->categories;
		}
		elseif(isset($request->submitValue) && $request->submitValue != ""){
			Session::forget('categories');
			$session_categories = [];
		}
		else{
			if(!empty(Session::get('categories'))){$session_categories = Session::get('categories'); }
			else{ $session_categories = []; }
		}
		$projectCategories = array_unique(ProjectCategory::whereIn('category_id',$session_categories)->pluck('project_id')->toArray());

		if(isset($request->sub_categories) && $request->sub_categories != ""){
			Session::forget('sub_categories');
			Session::push("sub_categories",$request->sub_categories);
			$session_sub_categories = $request->sub_categories;
		}
		elseif(isset($request->submitValue) && $request->submitValue != ""){
			Session::forget('sub_categories');
			$session_sub_categories = [];
		}
		else{
			if(!empty(Session::get('sub_categories'))){$session_sub_categories = Session::get('sub_categories'); }
			else{ $session_sub_categories = []; }
		}
		$project_sub_categories = array_unique(ProjectSubCategory::whereIn('sub_category_id',$session_sub_categories)->pluck('project_id')->toArray());

		if(isset($request->skills) && $request->skills != ""){
			Session::forget('skills');
			Session::push("skills",$request->skills);
			$session_skills = $request->skills;
		}
		elseif(isset($request->submitValue) && $request->submitValue != ""){
			Session::forget('skills');
			$session_skills = [];
		}
		else{
			if(!empty(Session::get('skills'))){$session_skills = Session::get('skills'); }
			else{ $session_skills = []; }
		}
		$project_skills = array_unique(ProjectSkill::whereIn('skill_id',$session_skills)->pluck('project_id')->toArray());

		$projectIdArray = array_merge($projectIndustries, $projectCategories, $project_sub_categories, $project_skills);

		// sorting
		if(isset($request->sort_value) && $request->sort_value != ""){
			Session::forget('sort_value');
			Session::put("sort_value",$request->sort_value);
			if($request->sort_value == 'lowest_price'){
				$projects = $projects->orderBy('budget_to', 'ASC');
			}
			elseif($request->sort_value == 'highest_price'){
				$projects = $projects->orderBy('budget_to', 'DESC');
			}
			elseif($request->sort_value == 'latest'){
				$projects = $projects->orderBy('id', 'DESC');
			}
		}
		elseif(!empty(Session::get('sort_value'))){
			if(Session::get("sort_value") == 'lowest_price'){
				$projects = $projects->orderBy('budget_to', 'ASC');
			}
			elseif(Session::get("sort_value") == 'highest_price'){
				$projects = $projects->orderBy('budget_to', 'DESC');
			}
			elseif(Session::get("sort_value") == 'latest'){
				$projects = $projects->orderBy('id', 'DESC');
			}
		}
		else{
			$projects = $projects->orderBy('id', 'DESC');
		}
		if(!empty($projectIdArray) || !empty(Session::get('categories')) || !empty(Session::get('sub_categories')) || !empty(Session::get('skills'))){
			$projects = $projects->WhereIn('id', $projectIdArray);
		}
		if(isset($request->min_value) && isset($request->max_value)){
			Session::forget('min_value');
			Session::forget('max_value');
			Session::put("min_value",$request->min_value);
			Session::put("max_value",$request->max_value);
			$min_value = $request->min_value;
			$max_value = $request->max_value;
		 }
		 $projects=$projects->where('budget_from','>=',$min_value)->where('budget_to','<=',$max_value);
		// dd($projects->toSql());
		$projects = $projects->paginate(5);
		return view('user.project.find_project',compact('projects', 'industries', 'categories', 'sub_categories', 'skills', 'min_price', 'max_price','projectInvitations'));
	}

	/**
	 * resetFilter: To flush the session value stored for filter
	 */
	public function resetFilter(){
		Session::forget('types');
		Session::forget('industries',);
		Session::forget('categories',);
		Session::forget('sub_categories');
		Session::forget('skills');
		Session::forget('min_value');
		Session::forget('max_value');
		Session::forget('sort_value');
		return response()->json(["reset"=>true]);
	}

	/**
	 * searchByName : Find a project by its name
	 * params : request data
	 * response: if successfull redirected to project listing
	 */
	public function searchByName(Request $request){
		$industries = Industry::whereActive(1)->pluck('id','name')->toArray();
		$categories = Category::whereActive(1)->pluck('id','name')->toArray();
		$sub_categories = SubCategory::whereActive(1)->pluck('id','name')->toArray();
		$skills = Skill::whereActive(1)->pluck('id','name')->toArray();
		if(isset($request->industry) && $request->industry != ""){
			$industries = Industry::whereActive(1)->where('name', 'LIKE', '%' . $request->industry . '%')->pluck('id','name')->toArray();
		}
		if(isset($request->category) && $request->category != ""){
			$categories = Category::whereActive(1)->where('name', 'LIKE', '%' . $request->category . '%')->pluck('id','name')->toArray();
		}
		if(isset($request->sub_category) && $request->sub_category != ""){
			$sub_categories = SubCategory::whereActive(1)->where('name', 'LIKE', '%' . $request->sub_category . '%')->pluck('id','name')->toArray();
		}
		if(isset($request->skill) && $request->skill != ""){
			$skills = Skill::whereActive(1)->where('name', 'LIKE', '%' . $request->skill . '%')->pluck('id','name')->toArray();
		}
		return response()->json(["industries"=>$industries,"categories"=>$categories,"sub_categories"=>$sub_categories,"skills"=>$skills]);
	}
	/**
	 * ListDisputes 
	 * Params 		: Request object
	 * Response 	: If successfull redirect to dsipute detail page
	 * STEP 1 		: Check the user authentication if yes goto STEP 2  else redirect to home
	 * STEP 2       : Redirect to dispute detail page
	 *
	 */
	public function ListDisputes(Request $request){
		$user = auth('web')->user();
		if($user){
			if($user->user_type == 0){
				$disputes = Dispute::where('client_user_id',$user->id);
				if(isset($request->title) && $request->title != ""){
					$searchValue = str_replace("CR", "",$request->title);
					$disputes = $disputes->where('project_contract_id', 'LIKE', '%' . $searchValue . '%');
				}
				$disputes = $disputes->orderby('id','DESC')->paginate(10);
			}
			else{
				$disputes = Dispute::where('freelancer_user_id',$user->id);
				if(isset($request->title) && $request->title != ""){
					$searchValue = str_replace("CR", "",$request->title);
					$disputes = $disputes->where('project_contract_id', 'LIKE', '%' . $searchValue . '%');
				}
				$disputes = $disputes->orderby('id','DESC')->paginate(10);
			}
		}
		return view('user.project.dispute',compact('disputes','user'));
	}

	/**
	 * viewDispute: To show the dispute detail
	 */
	public function viewDispute(Request $request, Dispute $dispute){
		$user = auth('web')->user();
		return view('user.project.view_dispute',compact('dispute','user'));
	}

	/**
	 * raiseDispute : To get the raise dispute form
	 */
	public function raiseDispute(Request $request, $milestoneId, $projectId){
		$user = auth('web')->user();
		if($user){
			if($user->user_type == 1){
				$available_projects = Project::whereIn('status', [2, 3]);//->paginate(1);
				$available_projects = $available_projects->with('contracts')->whereHas('contracts', function($query)  {
					$query->where([['freelancer_id', '=',auth('web')->user()->id]]);
				})->pluck('title','id')->toArray();
			}
			else{
				$available_projects = Project::whereIn('status', [2, 3, 4]);
				$available_projects = $available_projects->where([['user_id', '=',auth('web')->user()->id]])->pluck('title','id')->toArray();
			}
			$milestone = ProjectMilestone::where('id',$milestoneId)->first();

			$freelacer = Project::where('id', $projectId);//->paginate(1);
			$freelacer = $freelacer->with('contracts')->first();
		}
		return view('user.project.raise_a_dispute', compact('milestone','available_projects','user','freelacer'));
	}

	/**
	 * storeDispute : To store the dispute details 
	 * Params 		: Request object
	 * Response 	: If successfull redirect to dsipute listing page
	 *
	 */
	public function storeDispute(Request $request){
		$project = Project::where('id',$request->project_id)->first();
		$request_data = [];
		$request_data['client_user_id'] = $project->user_id;
		$request_data['freelancer_user_id'] = $request->freelancer_id;
		$request_data['raised_by'] = auth('web')->user()->id;
		$request_data['project_contract_id'] = $request->project_contract_id;
		$request_data['project_milestone_id'] = $request->project_milestone_id;
		$request_data['subject'] = $request->subject;
		$request_data['description'] = $request->description;
		$request_data['amount'] = $request->amount;
		$request_data['status'] = 0;
		Dispute::create($request_data);
		$project->update(['status'=>6]);

		//mark dispute in wallet if project milestone is 
		return redirect()->route('finance.ListDisputes')->with('toastr', ['type'=>'success','text'=>'Milestone disputed successfully.']);		
	}
}