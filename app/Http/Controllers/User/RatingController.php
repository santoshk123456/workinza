<?php

namespace App\Http\Controllers\User;

use App\Project;
use App\RatingQuestion;
use App\UserRating;
use App\UserReview;
use App\User;
use App\Http\Controllers\Controller;
use App\UserDetail;
use Illuminate\Http\Request;

class RatingController extends Controller
{

      /**
     * addRating:For showing rating add page for client/freelancer
     * Params : get project object information from parameters
     * Response : Redirected to add rating page
     */
    public function addRating(Project $project){
		
        $user = auth('web')->user();
       
		if($user){
         if($user->user_type==0)
         {
             $question_type="freelancer";
         }
         else
         {
            $question_type="client"; 
         }
         $questions=RatingQuestion::where('user_type',$question_type)->get();
         
         return view('user.rating_review.rating_user',compact('questions','project'));
        }

    }
      /**
     * addUserRating:For storing client/freelancer rating information
     * Params : Form input data ($request)
     * Response : Redirected to company rating page
     */
    public function addUserRating(Request $request,Project $project)
    {
   
        $user = auth('web')->user();
        if($user){

          
          
           foreach($request->questions as $question)
           {
               
            $rating=[];
            $rating['rated_by_user_id']=$user->id;
            $rating['project_id']=$project->id;
           
            $rating['rating_to_user_id']=$request->rating_to_user_id;
            $rating['rating_question_id']=$question;
          $rate_value=$request['rating'.$question];
         
           if(isset($rate_value) && !empty($rate_value))
           {
            $rating['rating']=$rate_value;
           }
           else
           {
            $rating['rating']=0;  
           }
           UserRating::create($rating);
    
           }
    
           $avgStar = UserRating::where('rating_to_user_id',$request->rating_to_user_id)->avg('rating');
           $avgStar=round($avgStar,1);
           $userAvgRating=UserDetail::where('user_id',$request->rating_to_user_id)->first();
  
           $userAvgRating->avg_rating=$avgStar;
           $userAvgRating->save();

           $review=[];
           $review['project_id']=$project->id;
           $review['reviewed_by_user_id']=$user->id;
           $review['reviewed_to_user_id']=$request->rating_to_user_id;
           $review['review_user']=$request->user_review;
           UserReview::create($review);

           return redirect()->route('rating.addCompanyRating',$project);
        }
     

    }
     /**
     * addCompanyRating:For showing rating add page for company
     * Params : get project object information as parameter
     * Response : Redirected to add company rating page
     */
    public function addCompanyRating(Project $project){
		
        $user = auth('web')->user();
       
		if($user){
        
         $questions=RatingQuestion::where('user_type','company')->get();
         
         return view('user.rating_review.rating_company',compact('questions','project'));
        }

    }
    /**
     * storeCompanyRating:For storing company rating information
     * Params : Form input data ($request)
     * Response : Redirected to my rating page
     */
    public function storeCompanyRating(Request $request,Project $project)
    {
     
        $user = auth('web')->user();
        if($user){

          //dd($request);
          
           foreach($request->questions as $question)
           {
               
            $rating=[];
            $rating['rated_by_user_id']=$user->id;
            $rating['project_id']=$project->id;
            $rating['rating_question_id']=$question;
          $rate_value=$request['rating'.$question];
         
           if(isset($rate_value) && !empty($rate_value))
           {
            $rating['rating']=$rate_value;
           }
           else
           {
            $rating['rating']=0;  
           }
           UserRating::create($rating);
    
           }

           $userReview=UserReview::where('project_id',$project->id)->where('reviewed_by_user_id',$user->id)->first();
           $userReview->review_company=$request->company_review;
           $userReview->save();


           return redirect()->route('rating.myRating');

         
        }
     

    }
     /**
     * myRating:For showing my given rating list
     * Params :  Get parameter from request
     * Response : Redirected to my given rating list page
     */
    public function myRating(Request $request)
    {
        $user = auth('web')->user();
        if($user){

            $rating_givens=UserRating::where('rated_by_user_id',$user->id)->groupBy('project_id')->paginate(20);
       
           
       
            return view('user.rating_review.rating_given',compact('rating_givens','user'));
        }
    }
   /**
     * myRatingReceived:For showing my received rating list
     * Params :  Get parameter from request
     * Response : Redirected to my received rating list
     */
    public function myRatingReceived(Request $request)
    {
        $user = auth('web')->user();
        if($user){

            $rating_received=UserRating::where('rating_to_user_id',$user->id)->groupBy('project_id')->paginate(20);
       
           
      
            return view('user.rating_review.rating_received',compact('rating_received','user'));
        }
    }
      /**
     * ratingDetail:For showing rating detail page
     * Params :  Get parameter from request and rating object value
     * Response : Redirected to rating detail page
     */
    public function ratingDetail(UserRating $rating,Request $request)
    {
   $type=$request->type;
  
        $current_user = auth('web')->user();
        if($current_user){

            $rating_details=UserRating::with('ratingQuestion')->where('project_id',$rating->project_id)->where('rated_by_user_id',$rating->rated_by_user_id)->where('rating_to_user_id',$rating->rating_to_user_id)->get();
     
            $company_ratings=UserRating::with('ratingQuestion')->where('project_id',$rating->project_id)->where('rated_by_user_id',$rating->rated_by_user_id)->whereNull('rating_to_user_id')->get();
     
            $user_review=UserReview::where('project_id',$rating->project_id)->where('reviewed_by_user_id',$rating->rated_by_user_id)->where('reviewed_to_user_id',$rating->rating_to_user_id)->first();
          
           // $company_review=UserReview::where('project_id',$rating->project_id)->where('reviewed_by_user_id',$rating->rated_by_user_id)->whereNull('reviewed_to_user_id')->first();
      
            return view('user.rating_review.rating_details',compact('rating_details','company_ratings','user_review','type'));
        }
    }
    
}