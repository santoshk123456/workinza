<?php

namespace App\Http\Controllers\User;

use App\Page;
use App\User;
use App\Setting;
use App\Category;
use App\EmailOTP;
use Mailgun\Mailgun;
use App\EmailTemplate;
use Illuminate\Http\Request;
use App\Services\ProcessEmailQueue;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * show home : For displaying home page
     * Response  : redireted to home page
     */
    public function home(){
      $post_a_job= Page::where('id','6')->first();
      $hire_freelancers= Page::where('id','7')->first();
      $get_work_done= Page::where('id','8')->first();
      $make_secure_payments= Page::where('id','9')->first();
      $the_talent_marketplace= Page::where('id','11')->first();
      $pre_packaged_projects= Page::where('id','12')->first();
      $home_video = Setting::where('key','home_page_video')->first();

      return view('user.homepage',compact('post_a_job','the_talent_marketplace','pre_packaged_projects','hire_freelancers','get_work_done','make_secure_payments','home_video'));
    }
    /**
     * show services : For displaying categories
     * Response      : redireted to service page
     */
    public function services(){
        return view('user.services');
    }
    /**
     * show login : For displaying login form
     * Response   : redireted to login page
     */
    public function showLogin(){
        if(!auth('web')->check()){
            return view('user.login');
        }else{
            return redirectBack(auth('web')->user());
            
        }
    }
    
    /**
     * show forgot : For displaying forgot password form
     * Response    : redireted to forgot page
     */

     public function forgotPassword(){
        if(auth('web')->check()){
            return redirect()->route('home');
        }
        return view('user.forgot');
     }
    
    /**
     * show reset : For displaying reset password form
     * Params     : Form input data ($request)
     * Response   : redireted to reset page
    */

      public function resetPassword(Request $request){
          
          $validator = Validator::make($request->all(),[
            'email' => 'required|email|max:50',
          ]);
          if(!$validator->fails()){

            $user = User::where('email',$request->email)->first();
            if(!empty($user)){
                $otp = EmailOTP::sendOTP($request->email);
                if($otp){
                    return view('user.resetPassword')->with('email',$request->email);
                }
            }else{
                return redirect()->route('forgotPassword')->withFlashError('invalid email');
            }
          }else{
                return redirect()->back()->withErrors($validator);
          }
      }
 
    /**
     * reset password : For reseting password form
     * Params         : Form input data ($request)
     * Response       : save new password and if it successful redireted to view contact page 
    */
       public function resetVerification(Request $request){

           $validator = Validator::make($request->all(),[
            'password' => 'required|confirmed|min:3',
            'otp' => 'required',
           ]);
           $user = User::where('email',$request->email)->first();
           if(!$validator->fails()){
            $otp = $request->otp;
            $validate = EmailOTP::otpValidate($otp,$request->email);
            if($validate){
               
                $password = Hash::make($request->password);
                $user->password = $password;
                
                if($user->update()){
                    auth()->guard('web')->attempt(['email' => $user->email, 'password' => $request->password]);
                    activity()->causedBy(auth()->guard('web')->user())
                    ->performedOn(auth()->guard('web')->user())
                    ->withProperties(['name' => auth()->guard('web')->user()->full_name])
                    ->useLog('user')
                    ->log('Logged in');
                    Session(['timezone' => Config('app.site_timezone')]);
                    return redirect()->route('user.viewdashboard')->withFlashSuccess("Password changed successfully.");
                }else{
                    return redirect()->route('forgotPassword');
                }
            }
            else{
                return view('user.resetPassword')->with('email',$request->email)->with('otp', 'Please enter a valid OTP.');
            }
           }else{
                return redirect()->route('forgotPassword');
           }
       }
       /**
       * contact-us form : For showing contact form
       * Response        : redireted to contact form 
       */
        public function homecontactus(){
            return view('user.cms_pages.home_contact_us');
        }
        /**
        * cms pages : For showing cms page
        * Params    : slug as parameter
        * Response  : using slug redirect to curresponding cms page
        */
        public function cmspages($slug){
          
            $cms_content = Page::where('slug',$slug)->first();

            return view('user.cms_pages.common_cms',compact('cms_content'));
    
        }

        /**
        * cms pages : For showing cms page
        * Params    : id as parameter
        * Response  : using id redirect to curresponding cms page
         */
        public function homecmspages($id){
            $cms_content = Page::where('id',$id)->first();
            return view('user.cms_pages.home_cms',compact('cms_content'));
    
        }
        /**
         * sent contact mail : For sending mail about contacted person
         * Params    : Form input data ($request)
         * step 1    : sent details through mail 
         * Response  :  redirect back with success msg
         */
        public function send_email_notification_contact(Request $request){
        
            $rules = [
                'full_name' => 'required',
                'phone' => 'required',
               
                'message' => 'required',
                'email' => 'required|max:50|regex:/^[.a-zA-Z0-9]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',
            ];
            $validator = Validator::make($request->all(),$rules);
            if($validator->fails()){
                return redirect()->back()->with('toastr', ['type'=>'error','text'=>'Please enter valid details!',]);
                //return 'Please enter valid details!';
            }
            else{
                $admin_enquiry_notify_email = EmailTemplate::where('static_email_heading','NOTIFY_ADMIN_CONTACT')->first();
                if ($admin_enquiry_notify_email) {
                    $admin_name = Setting::getSettingValue('application_admin_name');
                    $email_html = $admin_enquiry_notify_email->template;
                    $email_html = str_replace("[ADMIN_NAME]", $admin_name, $email_html);
                    $email_html = str_replace("[FROM_NAME]", $request->full_name, $email_html);
                    $email_html = str_replace("[USER_NAME]", $request->full_name, $email_html);
                    $email_html = str_replace("[USER_EMAIL]", $request->email, $email_html);
                    $email_html = str_replace("[USER_PHONE]", $request->phone, $email_html);
                    $email_html = str_replace("[USER_COMPANY]", $request->company, $email_html);
                    $email_html = str_replace("[USER_MESSAGE]", $request->message, $email_html);
                    $template = view('admin.email_templates.layout', compact('email_html'));
                    $data = [];
                    $data['to_email'] = Setting::getSettingValue('application_admin_email');
                    $data['to_name'] = $admin_name;
                    $data['from_email'] = $request->email;
                    $data['from_name'] = $request->name;
                    $data['subject'] = $admin_enquiry_notify_email->subject;
                    $data['body'] = $template;
                    $data['action_id'] = 0;
                    $data['action_type'] = 'admin_contact_enquiry_notify';
                    $processEmailQueue=new ProcessEmailQueue($data);
                    $processEmailQueue->sendEmail();
                }
                
                return view('user.cms_pages.home_contact_us')->withToastr(['type' => 'success', 'text' => 'Mail send successfully.']);
                //return 'OK';
            }
        }
        /**
         * sent mail : For sending mail about contacted person to admin
         * Params    : Form input data ($request)
         * Response  : sent details through mail to admin
         */
        public function send_email_notification(Request $request){
            $rules = [
                'name' => 'required',
                'email' => 'required|max:50|regex:/^[.a-zA-Z0-9]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',
            ];
            $validator = Validator::make($request->all(),$rules);
            if($validator->fails()){
                return 'Please enter valid details!';
            }
            else{
                $admin_enquiry_notify_email = EmailTemplate::where('static_email_heading','NOTIFY_ADMIN')->first();
                if ($admin_enquiry_notify_email) {
                    $admin_name = Setting::getSettingValue('application_admin_name');
                    $email_html = $admin_enquiry_notify_email->template;
                    $email_html = str_replace("[ADMIN_NAME]", $admin_name, $email_html);
                    $email_html = str_replace("[FROM_NAME]", $request->name, $email_html);
                    $email_html = str_replace("[USER_NAME]", $request->name, $email_html);
                    $email_html = str_replace("[USER_EMAIL]", $request->email, $email_html);
                    $template = view('admin.email_templates.layout', compact('email_html'));
                    $data = [];
                    $data['to_email'] = Setting::getSettingValue('application_admin_email');
                    $data['to_name'] = $admin_name;
                    $data['from_email'] = $request->email;
                    $data['from_name'] = $request->name;
                    $data['subject'] = $admin_enquiry_notify_email->subject;
                    $data['body'] = $template;
                    $data['action_id'] = 0;
                    $data['action_type'] = 'admin_enquiry_notify';
                    $processEmailQueue=new ProcessEmailQueue($data);
                    $processEmailQueue->sendEmail();
                }
                return 'OK';
            }
        }

        /**
         * user public profile : For showing user public profile
         * Params  : Form input data ($request)
         * step 1  : if there is user , then redirect to public profile 
         * step 2  : if there is no user , then redirect to cms pages 
         * step 3  : if there is no cms content then it will redirect to home page
         */
        public function userPublicProfile($username){
           // dd("hhh");
            $user = User::where('username',$username)->first();
            if($user != ""){
                return view('user_public_profile',compact('user')); 
            }else{
                $cms_content = Page::where('slug',$username)->first();
                if($cms_content != ""){
                    return view('user.cms_pages.common_cms',compact('cms_content'));
                }else{
                    return redirect()->route('home');
                }
                
                
            }
            
        }

}
