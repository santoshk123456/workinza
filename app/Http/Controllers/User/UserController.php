<?php

namespace App\Http\Controllers\User;

use Hash;
use App\User;
use App\Admin;
use App\Skill;
use App\Vault;
use DataTables;
use App\Country;
use App\Invoice;
use App\Project;
use App\Setting;
use App\Category;
use App\EmailOTP;
use App\Industry;
use App\UserTerm;
use Carbon\Carbon;
use Stripe\Charge;
use Stripe\Stripe;
use App\UserDetail;
use App\UserRating;
use App\SubCategory;
use App\Transaction;
use App\EmailTemplate;
use App\InvitedClient;
use App\UserPortfolio;
use App\MembershipPlan;
use App\PaymentGateway;
use App\ProjectProposal;
use App\Rules\Recaptcha;
use App\ProjectMilestone;
use App\SecurityQuestion;
use App\UserSubscription;
use App\ProjectInvitation;
use App\TermsAndCondition;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\UserQuestionsResponse;
use \Torann\GeoIP\Facades\GeoIP;
use App\DataTables\AdminDataTable;


use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Html\Builder;

use App\Services\ProcessEmailQueue;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\User\UserRequest;
use Illuminate\Support\Facades\Session;
use Spatie\Activitylog\Models\Activity;
use App\Http\Requests\Admin\AdminRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Admin\ChangePasswordRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\User\ContactInformationRequest;
use App\Libraries\PaymentGateways\PaymentGatewaysController;

class UserController extends Controller
{
    /***
     * To select user type 
     */
    public function create()
    {
        if (!auth('web')->check()) {
            return view('user.registration.user_type_details');
        }else{
            return redirectBack(auth('web')->user());
            
        }
    }
    /**
      * changePassword
    */
    public function changePassword(){
      return view('user.registration.changepassword');
    }

    /**
	 * userchangePassword : For updating user password
	 * Params : Form input data ($request)
	 * Response : If successfull redireted to contact information view page
	 */
     public function userchangePassword(ChangePasswordRequest $request)
     {
        $user = User::where('id',auth()->user()->id)->first();
        if (!Hash::check($request->current_password, $user->password)) {
            throw new GeneralException("Current password was wrong.");
        }
        $user->password = Hash::make($request->password);
        $user->save();
        return redirectBack(auth('web')->user(),1);
        //return redirect()->route('user.viewContactInformation')->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
     }
     /**
      * dashboard : Show basic statistics in the user portal
      */
     public function viewdashboard()
     {
        $user = auth('web')->user();
        $accept_proposal = []; 
        $conditions = []; 
        if($user->user_type == 1)
        {
            // User subscription details
            $accept_proposal['status'] 	= 1;
            $accept_proposal['freelancer_acceptance'] = 1;
            $cond_sub = [];
            $cond_sub['user_id'] = $user->id;
            $cond_sub['active'] = 1;
            $user_sub = UserSubscription::where($cond_sub)->with('membership')->orderBy('id','DESC')->first();
         
            if(isset($user_sub) && $user_sub != ""){
                $freelancer_service_charge = $user_sub->service_percentage;
            }

            // Project proposal count
            $open_projects = null;
            $proposals_count = ProjectProposal::where('user_id',$user->id)->count();

            // In progress project count
            $inprogress_projects =Project::whereIn('status', [2, 3])->with('contracts')->whereHas('contracts', function($query)  {
				$query->where([['freelancer_id', '=',auth('web')->user()->id]]);
            })->count();

            // Completed project count
            $completed_projects =  Project::whereIn('status', [4,5])->with('contracts')->whereHas('contracts', function($query) use($user)  {
                $query->where([['status', '=',1],['freelancer_id', '=',$user->id]]);
            })->count();

            // Invitation count
            $conditions['freelancer_id'] = $user->id;
			$conditions['status'] = 0;
            $invitations = ProjectInvitation::where($conditions)->count();
            
        }else{
            $freelancer_service_charge = null;
            $proposals_count = null;
            $invitations = null;

            $open_projects = Project::where('user_id',$user->id)->whereIn('status', [0, 1])->count();
            $inprogress_projects = Project::where('user_id',$user->id)->whereIn('status', [2, 3])->count();
            $completed_projects = Project::where('user_id',$user->id)->whereIn('status', [4,5])->count();
        }

        return view('user.registration.dashboard',compact('invitations','proposals_count','open_projects','user','freelancer_service_charge','inprogress_projects','completed_projects'));
     }

     /**
	 * userType : To get user type and organization type details 
	 * Params : Form input data ($request)
	 * Response : If successfull redireted to user account view
	 */
    public function userType(Request $request)
    {
        if ($request->has('user_type') && $request->has('org_type')) {
            return redirect()->route('user.accountDetails', ['organization_type' => $request->org_type, 'user_type' => $request->user_type]);
        } else {
            return redirect()->back();
        }
    }

    /**
	 * accountDetails : To get user account details page 
	 * Response : If successfull redireted to user account view
	 */
    public function accountDetails(Request $request)
    {
        
        $terms = TermsAndCondition::select('id', 'content')->where('slug', 'terms-and-conditions')->first();
        $site_key = Setting::select('value')->where('key', 'captcha_site_key')->first()['value'];
        $freelancer_details = "";
        $invited_client = "";
        if($request->has('referral')){
            $enc_data = $request->referral;
            $decrypt_data = hex2bin($enc_data);
            //get freelancer_id 
            $freelancer_details = UserDetail::where('referral_code',$decrypt_data)->first();
            $invited_client = $request->invitation;
        }
        if ($request->has('user_type') && $request->has('organization_type')) {
            $organization_type = $request->organization_type;
            $user_type = $request->user_type;
            $user = "";
            if ($request->has('user')) {
                $user = User::where('uuid', $request->user)->with('userDetail')->first();
            }

            return view('user.registration.account_details', compact('organization_type', 'user_type', 'terms', 'user','freelancer_details','invited_client','site_key'));
        } else {
            return redirect()->back();
        }
    }

    /**
	 * store : To store basic details of the user 
	 * Params : Form input data ($request)
	 * Response : If successfull redireted to email verification page
    */
    public function store(UserRequest $request)
    {
        
        $validatedData = $request->validate(
            ['g-recaptcha-response' => new Recaptcha]
            
        );
       
       
        $request_data = [];
        $terms_data = [];
        $user_detail = "";
        $password = $request->password;
        $hashed_random_password = Hash::make($password);
        $request_data['full_name'] = $request->full_name;
        $request_data['email'] = $request->email;
        $request_data['password'] = $hashed_random_password;
        $request_data['user_type'] = $request->user_type;
        $request_data['organization_type'] = $request->organization_type;
        $request_data['username'] = str_random(15);
        $request_data['step'] = 1;
       
        if ($request->has('user') && $request->user != "") {

            $create_user = User::where('uuid', $request->user)->update($request_data);
        } else {
            $create_user = User::create($request_data);
        }

        if ($create_user) {
            // creating user detail 
            $details = [];
            $details['user_id'] = $create_user->id;
            if ($request->has('company_name')) {
                $details['company_name'] = $request->company_name;
            }
            $details['referral_code'] = str_random(8);
            if(isset($request->referral_freelancer) && $request->referral_freelancer != ""){
                $details['referral_user_id'] = $request->referral_freelancer;
            }
           
            if ($request->has('user') && $request->user != "") {
                $user_detail = UserDetail::where('user_id', $create_user->id)->update($details);
            } else {
                $user_detail = UserDetail::create($details);
            }
            
            if(isset($request->referral_freelancer) && $request->referral_freelancer != ""){
                //update the invited client table
                $update_status = [];
                $update_status['joined_date'] = Carbon::now();
                $update_status['status'] = 1;
                $update_status['is_availed'] = 0;
                $update_status['client_id'] = $create_user->id;
                $invitation_client_update = InvitedClient::where('id',hex2bin($request->invited_client))->update($update_status);
            }
            if ($request->has('terms')) {
                $terms_details = TermsAndCondition::where('id', $request->terms)->first();
                $terms_data['terms_and_condition_id'] = $terms_details->id;
                $terms_data['user_id'] = $create_user->id;
                $terms_data['version'] = $terms_details->version;
                $terms_data['accepted_at'] = date('Y-m-d H:i:s');
                $create_user_terms = UserTerm::create($terms_data);
                if ($create_user_terms) {
                    $otp = EmailOTP::sendOTP($request->email);
                    if ($otp) {
                        if ($request->has('user') && $request->user != "") {
                            return redirect()->route('user.emailVerification', ['user' => $request->user])->with('toastr', ['type' => 'success', 'text' => 'OTP has been sent to your registered email']);
                        } else {
                            return redirect()->route('user.emailVerification', ['user' => $create_user->uuid])->with('toastr', ['type' => 'success', 'text' => 'OTP has been sent to your registered email']);
                        }
                    }
                }
            } else {
                // Send verification otp to the user email
                $otp = EmailOTP::sendOTP($request->email);
                if ($otp) {
                    if ($request->has('user') && $request->user != "") {
                        return redirect()->route('user.emailVerification', ['user' => $request->user])->with('toastr', ['type' => 'success', 'text' => 'OTP has been sent to your registered email']);
                    } else {
                        return redirect()->route('user.emailVerification', ['user' => $create_user->uuid])->with('toastr', ['type' => 'success', 'text' => 'OTP has been sent to your registered email']);
                    }
                }
            }
        }
    }

    /**
     * emailVerification: Redirect Email verification page
     */
    public function emailVerification(User $user)
    {
        return view('user.registration.email_verification', compact('user'));
    }

    /**
	 * otpVerification : Validate the email OTP 
	 * Params : Form input data ($request)
	 * Response : If successfull redireted to create contact information screen
    */
    public function otpVerification(User $user, Request $request)
    {
        $otp = '';
        $otp = $request->otp_one . $request->otp_two . $request->otp_three . $request->otp_four . $request->otp_five . $request->otp_six;
        $email_validate = EmailOTP::otpValidate($otp, $user->email);
        if ($email_validate) {
            $request_data['email_verified_at'] = Carbon::now();
            User::where('email', $user->email)->update($request_data);
            $user = User::where('email', $user->email)->first();
            auth('web')->login($user);
            activity()->causedBy(auth()->guard('web')->user())
                ->performedOn(auth()->guard('web')->user())
                ->withProperties(['name' => auth()->guard('web')->user()->full_name])
                ->useLog('user')
                ->log('Logged in');
            Session(['timezone' => Config('app.site_timezone')]);
            
            // Send Email and save to email queue
            $user_creation_template = EmailTemplate::where('static_email_heading','WELCOME_EMAIL')->first();

            if ($user_creation_template) {
                $email_html = $user_creation_template->template;
                $email_html = str_replace("[USER_NAME]", ucwords($user->full_name), $email_html);
                $email_html = str_replace("[USER_LOGIN_LINK]", route('login'), $email_html);
              
                $template = view('admin.email_templates.layout', compact('email_html'));
                $data = [];
                $data['to_email'] = $user->email;
                $data['to_name'] = ucwords($user->full_name);
                $data['subject'] = $user_creation_template->subject;
                $data['body'] = $template;
                $data['action_id'] = $user->id;
                $data['action_type'] = 'user_creation';
               
                $processEmailQueue=new ProcessEmailQueue($data);
                $processEmailQueue->sendEmail();
            }
            return redirect()->route('user.contactInformation')->with('toastr', ['type' => 'success', 'text' => 'Email verified successfully']);
        } else {
            return redirect()->back()->with('toastr', ['type' => 'error', 'text' => 'Invalid OTP']);
        }
    }

    /**
     * contactInformation: To get contact information form
     */

    public function contactInformation()
    {
        $user = auth('web')->user();
        $countries = Country::select('id', 'name')->get();
        return view('user.registration.contact_information', compact('user', 'countries'));
    }

    /***
     * usernameExist: To check user name exist or not  
     * Response : If successfull, return JSON response array
     */
    public function usernameExist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:50|regex:/(^([a-zA-Z]+)(\d+)?$)/u'
        ]);
        $msg = "";
        $resultArr = [];
        if (!$validator->fails()) {
            $user = User::where('username', $request->username)->withTrashed()->first();
            if ($user) {
                $username_exist = 1;
                $msg =  "User name already exist.";
            } else {
                $username_exist = 0;
            }
        } else {
            $username_exist = 2;
            $msg = "The User name format is invalid.";
        }
        $resultArr['username_exist'] = $username_exist;
        $resultArr['msg'] = $msg;
        return response()->json($resultArr);
    }

   /***
     * usernameExist: To check if phone number exist or not  
     * Response : If successfull, return JSON response array
     */
    public function phoneNumberExist(Request $request)
    {
        $msg = "";
        $resultArr = [];
        $user = User::where('phone', $request->phone)->withTrashed()->first();
        if ($user) {
            $phone_exist = 1;
            $msg =  "Phone number already exist.";
        } else {
            $phone_exist = 0;
        }
        $resultArr['phone_exist'] = $phone_exist;
        $resultArr['msg'] = $msg;
        return response()->json($resultArr);
    }

    /**
	 * storeContactInformation : To store or update contact information of the user
	 * Params : Form input data ($request)
	 * Response : If successfull redirected to next registration step, security questions listing
    */
    public function storeContactInformation(ContactInformationRequest $request)
    {
        $user = auth('web')->user();
        // Request data
        $request_data = [];
        $request_data['username'] = $request->username;
        $request_data['country_code'] = $request->country_code;
        $request_data['phone'] = $request->phone;
        $request_data['phone_verified_at'] = Carbon::now();
        $request_data['step'] = 2;
        // Update user
        $user->update($request_data);

        $user_detail_exist = UserDetail::where('user_id', $user->id)->first();
        if (!empty($user_detail_exist)) {
            // updating user_details
            $details = [];
            $details['address_line_1'] = $request->address_line_1;
            $details['address_line_2'] = $request->address_line_2;
            // $details['paypal_email'] = $request->paypal_email;
            $details['country_id'] = $request->country_id;
            $details['state'] = $request->state;
            $details['city'] = $request->city;
            $details['zipcode'] = $request->zipcode;
            $details['website_url'] = $request->website_url;
            $user_detail = UserDetail::where('user_id', $user->id)->update($details);
        } else {
            // creating user detail
            $details = [];
            $details['address_line_1'] = $request->address_line_1;
            $details['address_line_2'] = $request->address_line_2;
            // $details['paypal_email'] = $request->paypal_email;
            $details['country_id'] = $request->country_id;
            $details['state'] = $request->state;
            $details['city'] = $request->city;
            $details['zipcode'] = $request->zipcode;
            $details['website_url'] = $request->website_url;
            $details['user_id'] = $user->id;
            $details['referral_code'] = str_random(8);
            $user_detail = UserDetail::create($details);
        }

        if ($user_detail) {
            return redirect()->route('user.securityQuestions')->with('toastr', ['type' => 'success', 'text' => 'Contact information saved successfully',]);
        }
    }

    /**
	 * viewContactInformation : To show user contact informations
    */
    public function viewContactInformation()
    {
        $user = auth('web')->user();
        return view('user.registration.contact_information_view',compact('user'));
     }
    
    /**
	 * redirectBack : Check the user step and redirect to specific step view
    */
    public function redirectBack()
    {
        $new_user = auth('web')->user();
        if($new_user->step == 2){
            // redirect to security questions view
            return redirect()->route('user.securityQuestions');
        }else if($new_user->step == 3){
            // redirect to profile acknolwedgment page
            return redirect()->route('user.profileAcknowledgement');
        } else if ($new_user->step == 4) {
            // redirect to background verification page
            return redirect()->route('user.backgroundVerification');
        }else if($new_user->step == 5) {
           // redirect to public profile view
            return redirect()->route('account.createPublicProfile');
        } else if ($new_user->step == 6) {
            //get the subscription status
            $sub_conditions = [];
            $sub_conditions['user_id'] = $new_user->id;
            $subscription_status = UserSubscription::where($sub_conditions)->orderBy('id', 'DESC')->first();
           
            if($new_user->user_type == 0){
                if(isset($subscription_status) && !empty($subscription_status) && $subscription_status->active == 1){
                    return redirect()->route('account.viewPublicProfile');
                }else{
                    return redirect()->route('membership_plan.membershipPlans');
                }
            }else{
                return redirect()->route('account.viewPublicProfile');
            }
        }
    }

    /**
	 * editContactInformation : To get update contact information form
    */
    public function editContactInformation()
    {
        $user = auth('web')->user();
        return view('user.registration.edit_contact_information', compact('user'));
    }

    /**
	 * updateContactInformation : To update contact information of the user
     * STEP 1: Send request mail for updating the basic contact information 
	 * Params : Form input data ($request)
	 * Response : If successfull redirected to contact information view
    */
    public function updateContactInformation(Request $request)
    {
        $rules = [
            // 'name' => 'required',
            // 'email' => 'required|max:50|regex:/^[.a-zA-Z0-9]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',
            'description' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return redirect()->back()->with('toastr', ['type'=>'error','text'=>'Please enter the description for your request.',]);
        }
        else{
            $update_contact_information = EmailTemplate::where('static_email_heading','UPDATE_PROFILE_NOTIFICATION')->first();
            if ($update_contact_information) {
                $admin_name = Setting::getSettingValue('application_admin_name');
                $email_html = $update_contact_information->template;
                $email_html = str_replace("[ADMIN_NAME]", $admin_name, $email_html);
                $email_html = str_replace("[FROM_NAME]", $request->name, $email_html);
                $email_html = str_replace("[USER_NAME]", $request->name, $email_html);
                $email_html = str_replace("[USER_EMAIL]", $request->email, $email_html);
                $email_html = str_replace("[PHONE]", $request->phone, $email_html);
                $email_html = str_replace("[SUBJECT]", $request->description, $email_html);
                $template = view('admin.email_templates.layout', compact('email_html'));
                $data = [];
                $data['to_email'] = Setting::getSettingValue('application_admin_email');
                $data['to_name'] = ucwords($admin_name);
                $data['from_email'] = $request->email;
                $data['from_name'] = ucwords($request->name);
                $data['subject'] = $update_contact_information->subject;
                $data['body'] = $template;
                $data['action_id'] = 0;
                $data['action_type'] = 'admin_enquiry_notify';
                $processEmailQueue = new ProcessEmailQueue($data);
                $processEmailQueue->sendEmail();
            }
            return redirect()->route('user.viewContactInformation')->with('toastr', ['type'=>'success','text'=>'Update request have been send successfully',]);
        }
    }

    /***
     * securityQuestions : To get Security Questions
     */

    public function securityQuestions()
    {
        $user = auth('web')->user();
        $security_questions = SecurityQuestion::select('id', 'question')->whereIn('user_type', [$user->user_type, 2])->get();
        return view('user.registration.security_questions', compact('security_questions'));
    }
    /***
     * editSecurityQuestions : To get update Security Questions form
     */
    public function editSecurityQuestions()
    {
        $user = auth('web')->user();
        $all_security_questions = SecurityQuestion::select('id', 'question')->whereIn('user_type', [$user->user_type, 2])->get();
        $security_questions = UserQuestionsResponse::where('user_id',$user->id)->with('securityQuestions')->get();
        return view('user.registration.edit_security_questions',compact('security_questions','all_security_questions'));
    }
    /***
     * viewSecurityQuestions : To view security questions
     */
    public function viewSecurityQuestions(){
        $user = auth('web')->user();
        $security_questions = UserQuestionsResponse::where('user_id',$user->id)->with('securityQuestions')->get();
        return view('user.registration.view_security_questions',compact('user','security_questions'));
     }

    /**
	 * updateSecurityQuestions : To update security questions of the user
	 * Params : Form input data ($request)
	 * Response : If successfull redirected to view security questions
    */
     public function updateSecurityQuestions(Request $request){
        $user = auth('web')->user();
        $data = [];
        if($request->has('answers')){
            
            foreach($request->answers as $qusetion_id => $ans){
                $data = [
                   'answer' => $ans,
                   'user_id' => $user->id
                ];
                UserQuestionsResponse::where('id',$qusetion_id)->update($data);
            }
            return redirect()->route('user.viewSecurityQuestions')->with('toastr', ['type'=>'success','text'=>'Security question has been updated successfully',]);;
           
        }
    }
    
    /**
	 * storeSecurityQuestions : To store security questions of the user
	 * Params : Form input data ($request)
	 * Response : If successfull redirected to next registration step, profile acknowledgemnt page
    */
     public function storeSecurityQuestions(Request $request){
         $user = auth('web')->user();
         $data = [];
         if($request->has('answers')){
            foreach($request->answers as $qusetion_id => $ans){
                $data[] = [
                    'security_question_id' => $qusetion_id,
                    'answer' => $ans,
                    'user_id' => $user->id
                ];
            }
            //store user responses
            $create_user_responses = UserQuestionsResponse::insert($data);
            //update step 
            if($user->step < 3){
                $user_step = User::where('id', $user->id)->update(['step' => 3]);
                if ($create_user_responses) {
                    return redirect()->route('user.profileAcknowledgement');
                }
            }else{
                return redirectBack($user);
            }
            
        }
    }

    /**
     * Profile acknowledgement
     * STEP 1 : Get the background charge amount from settings
     * STEP 2 : Get the payment gateway details
     * STEP 3 : Create checkout seestion for the payment 
     */
    public function profileAcknowledgement()
    {
        $user = auth('web')->user();

        $back_ground_charge = Setting::select('value')->where('key', 'back_ground_charge')->first()['value'];
    
        if($user->step <= 3){
            $user_step = User::where('id', $user->id)->update(['step' => 3]);
            $payment_keys = PaymentGateway::where('status', 1)->first();
            $paymentGateway = new PaymentGatewaysController;
            if ($paymentGateway->initiatePaymentGateway()) {
                $request = [];
                $request['charge'] = $back_ground_charge;
                $request['subscription'] = 0;
                $request['name'] = "Background Verification";
                $request['email'] = $user->email;
                $request['milestone'] = $user->id;
                $request['success_url'] = route('user.backgroundVerification', ['success' => '1']);
                $request['cancel_url'] = route('user.profileAcknowledgement');
                $request['description'] = "A one time charge USD" . $back_ground_charge . " will be debited by Keyoxa for verifying the address and the background details provided.";
                $checkout_session =  $paymentGateway->createCheckoutSession($request);
    
                return view('user.registration.profile_acknowledgement', compact('back_ground_charge', 'payment_keys', 'checkout_session'));
            }
        }else{
            return redirectBack($user);
        }
       
    }

    /**
     * oneTimePayment: Store acknowledgement
     */
    public function oneTimePayment()
    {
        //store agree values first
        $payment_keys = PaymentGateway::where('status', 1)->first();
        return view('user.registration.background_payment', compact('payment_keys'));
    }

    /**
     * saveOneTimePayment : Webhook for from stripe to handle different type of stripe events
     */
    public function saveOneTimePayment(Request $request)
    {
        $stripe_secret = PaymentGateway::select('authentication_token')->first()['authentication_token'];
        Stripe::setApiKey($stripe_secret);
        $input = @file_get_contents("php://input");
        $event_json = json_decode($input);
        $event = \Stripe\Event::retrieve($event_json->id);

        if (isset($event) && $event_json->type == 'checkout.session.completed') {
            
        } else if (isset($event) && $event_json->type == 'customer.subscription.created') {
            $customer = \Stripe\Customer::retrieve($event->data->object->customer);
            $customerEmail = $customer->email;
            $user = User::where('email', $customerEmail)->first();

            //update active status
            $cond = [];
            $cond['active'] = 0;
            UserSubscription::where('user_id', $user->id)->update($cond);
            //get plan id
            $plan_details = MembershipPlan::where('plan_id', $event->data->object->plan->id)->first();
            $sub = [];
            $sub['user_id'] = $user->id;
            $sub['membership_plan_id'] = $plan_details->id;
            $sub['start_date'] = date('Y-m-d H:i:s', $event->data->object->current_period_start);
            $sub['end_date'] = date('Y-m-d H:i:s', $event->data->object->current_period_end);
            $sub['duration_type'] = ($plan_details->plan_type == 0) ? 0 : 1;
            $sub['amount'] = $event->data->object->plan->amount / 100;
            $sub['setup_charge'] =  $plan_details->setup_charge;
            $sub['service_percentage'] = $plan_details->service_percentage;
            $sub['auto_renewal'] = 0;
            $sub['active'] = 1;
            $sub['auto_renewal'] = 1;
            $sub['remaining_proposal_count'] = $plan_details->number_of_proposals;
            $sub['remaining_project_count'] = $plan_details->number_of_projects;
            
            $subscription = UserSubscription::create($sub);


            //insert an entry in transaction table
            $transaction_sts = Transaction::create([
                'user_id' => $user->id,
                'type' => 0,
                'amount' => $event->data->object->plan->amount / 100,
                'total_amount' => $event->data->object->plan->amount / 100,
                'transaction_type' => 0,
                'notes' => "Subscription payment completed successfully",
                'payment_status' => 1,
                'payment_response' => "Paid",
                'user_subscription_id' => $subscription->id
            ]);
            if ($user->step != 6) {
                $user_step = User::where('id', $user->id)->update(['step' => 5]);
            }
            // Generate invoice for subscription payment
            $input_data = [];
            $input_data['user_id'] = $user->id;
            $input_data['type'] = 5;
            $input_data['amount'] = $event->data->object->plan->amount / 100;
            $input_data['notes'] = "Invoice generated for the subscription payment";
            $input_data['status'] = 1;
            $input_data['user_subscription_id'] = $subscription->id;
            $now_date = Carbon::now();;
            $input_data['paid_on'] = $now_date->toDateTimeString();;
            Invoice::invoiceGeneration($input_data,$user,);

        
            
        }else if(isset($event) && $event_json->type == 'charge.succeeded'){
            //get user detail
            $user = User::where('email', $event->data->object->billing_details->email)->first();
            if ($user->step == 3) {
                // create an transaction entry
                $transaction_sts = Transaction::create([
                    'user_id' => $user->id,
                    'type' => 6,
                    'amount' => $event->data->object->amount / 100,
                    'total_amount' => $event->data->object->amount / 100,
                    'transaction_type' => 0,
                    'notes' =>"Background verification payment succeeded",
                    'payment_status' => 1,
                    'payment_response' => $event->data->object->status,
                    'gateway_transactionID'=>$event->data->object->id
                ]);
                if ($transaction_sts) {
                    //update step
                     $user_step = User::where('id', $user->id)->update(['step' => 4]);
                    //send mail to admin
                    $admin_background_template = EmailTemplate::where('static_email_heading','NOTIFY_BACKGROUND_VERIFICATION')->first();
                    $admin_name = Setting::getSettingValue('application_admin_name');
                    $admin_email = Setting::getSettingValue('application_admin_email');
                    if ($admin_background_template) {
                        $email_html = $admin_background_template->template;
                        $email_html = str_replace("[USER_NAME]", ucwords($user->full_name), $email_html);
                        $email_html = str_replace("[USER_EMAIL]", $user->email, $email_html);
                        $email_html = str_replace("[USER_TYPE]", userType($user->user_type), $email_html);
                        $email_html = str_replace("[ADMIN_LOGIN_LINK]", route('admin.login.form'), $email_html);
                    
                        $template = view('admin.email_templates.layout', compact('email_html'));
                        $data = [];
                        $data['to_email'] = $admin_email;
                        $data['to_name'] = ucwords($admin_name);
                        $data['from_email'] = $user->email;
                        $data['from_name'] = ucwords($user->full_name);
                        $data['subject'] = $admin_background_template->subject;
                        $data['body'] = $template;
                        $data['action_id'] = $user->id;
                        $data['action_type'] = 'notify_verification_request';
                    
                        $processEmailQueue=new ProcessEmailQueue($data);
                        $processEmailQueue->sendEmail();
                    }
                    // Generate invoice for background verification
                    $input_data = [];
                    $input_data['user_id'] = $user->id;
                    $input_data['type'] = 6;
                    $input_data['amount'] = $event->data->object->amount / 100;
                    $input_data['notes'] = "Invoice generated for the background verification";
                    $input_data['status'] = 1;
                    $now_date = Carbon::now();
                    $input_data['paid_on'] = $now_date->toDateTimeString();;
                    Invoice::invoiceGeneration($input_data,$user);
                }
            }else if($user->step == 6 && isset($event->data->object->metadata->milestone) && $event->data->object->metadata->milestone !="" ){
                $transaction_fee = Setting::select('value')->where('key', 'transaction_fee')->first()['value'];
                $milestone = ProjectMilestone::where('id', $event->data->object->metadata->milestone)->with('project.contracts')->first();
                $transaction_sts = Transaction::create([
                    'user_id' => $user->id,
                    'type' => 2,
                    'amount' => $milestone->milestone_amount,
                    'project_milestone_id'=> $event->data->object->metadata->milestone,
                    'service_percentage'=>$transaction_fee,
                    'project_contract_id'=>$milestone->project->contracts->id,
                    'project_id'=>$milestone->project_id,
                    'total_amount' => $event->data->object->amount / 100,
                    'service_charge'=>($milestone->milestone_amount * $transaction_fee)/100,
                    'transaction_type' => 0,
                    'notes' =>"Milestone Payment succeeded",
                    'payment_status' => 1,
                    'payment_response' => $event->data->object->status,
                    'gateway_transactionID'=>$event->data->object->id
                ]);

               
                $update = ProjectMilestone::where('id', $milestone->id)->update(['status'=>1]);
                $project = Project::where('id',$milestone->project_id)->with('contracts.user.userDetail')->first();
                $getTransactions = Transaction::where('id',$transaction_sts->id)->with('contracts','milestones')->first();
                //Add entry in the vault table for both client and freelancer
                $vault_sts = Vault::create([
                    'user_id' => $user->id,
                    'user_type' => $user->user_type,
                    'project_milestone_id' => $milestone->id,
                    'transaction_id' => $transaction_sts->id,
                    'transaction_type' => 0,
                    'amount' => $milestone->milestone_amount,
                    'notes' => "Milestone amount credited to your vault for the milestone ". ucwords($milestone->milestone_name)." for the project ".ucwords($project->title),
                    'vault_status' =>0,
                    
                ]);
                $vault_sts = Vault::create([
                    'user_id' => $getTransactions->contracts->freelancer_id,
                    'user_type' => $getTransactions->contracts->user->user_type,
                    'project_milestone_id' => $getTransactions->project_milestone_id,
                    'transaction_id' => $transaction_sts->id,
                    'transaction_type' => 0,
                    'notes' => "Client credited milestone amount to your vault for the milestone ". ucwords($getTransactions->milestones->milestone_name)." for the project ".ucwords($project->title),
                    'amount' => $getTransactions->amount,
                    'vault_status' =>0,
                    
                ]);
                //send email notification freelancer
                $milestone_start_template = EmailTemplate::where('static_email_heading','ACTIVATE_MILESTONE')->first();
                
                if ($milestone_start_template) {
                    $email_html = $milestone_start_template->template;
                    $email_html = str_replace("[USER_NAME]",ucwords($project->contracts->user->username), $email_html);
                    $email_html = str_replace("[FROM_USER]", ucwords($user->username), $email_html);
                    $email_html = str_replace("[PROJECT]", $project->title, $email_html);
                    $email_html = str_replace("[MILESTONE_NAME]", $milestone->milestone_name, $email_html);
                    $email_html = str_replace("[LOGIN_LINK]", route('project.MilestoneDetails',$milestone->id), $email_html);
                    
                    $template = view('admin.email_templates.layout', compact('email_html'));
                    $data = [];
                    $data['to_email'] =$project->contracts->user->email;
                    $data['to_name'] = ucwords($project->contracts->user->username);
                    $data['subject'] = $milestone_start_template->subject;
                    $data['body'] = $template;
                    $data['action_id'] = $project->contracts->user->id;
                    $data['action_type'] = 'activate_milestone';
                   
                    $processEmailQueue=new ProcessEmailQueue($data);
                    $processEmailQueue->sendEmail();
                }
			
            }
        }
    }

    /**
     * backgroundVerification: Show Background verification page
     */

    public function backgroundVerification()
    {
        return view('user.registration.background_verification');
    }

    /**
     * createPublicProfile: To get create public profile form
     */
    public function createPublicProfile()
    {
        $industries = Industry::where('active', 1)->get();
        $category_list = Category::where('active', 1)->get();
        $user = auth('web')->user();

        if ($user) {
            return view('user.registration.create_public_profile', compact('industries', 'user', 'category_list'));
        } else {
            return redirect()->route('home');
        }
    }
    /**
     * storePublicProfile : To save the user public profile details
     * inputs : request array
     * Response : If success redirect to public profile view page
     */
    public function storePublicProfile(Request $request)
    {
        $user = auth('web')->user();
        //$user = User::where('id','=',1)->first();
        if ($user) {
            $request_data = [];
            $user_data = [];
            if ($request->hasFile('profile_image')) {
                $rules = [
                    'profile_image' => 'required|
					mimes:jpeg,jpg,png|max:10000'
                ];
                $request->validate($rules);

                $icon = $request->file('profile_image')->store('public/user/profile-images');
                $user_data['profile_image'] = str_replace('public/user/profile-images', '', $icon);
            } else {
                $user_data['profile_image'] = "";
            }
            User::where('id', $user->id)->update($user_data);
            $request_data['profile_headline'] = $request->profile_headline;
            //$request_data['website_url'] = $request->website_url;
            $request_data['company_tagline'] = $request->company_tagline;
            $request_data['about_me'] = $request->about_me;

            if (!empty($request->established_date)) {

                $request_data['established_since'] = gmtDateTime(strtotime($request->established_date), 'date');
            } else {
                $request_data['established_since'] = NULL;
            }

            if (!empty($request->years_of_experience)) {
                $request_data['years_of_experience'] = $request->years_of_experience;
            }
            if (!empty($request->corporate_presentation)) {
                $request_data['corporate_presentation'] = $request->corporate_presentation;
            }
            if(Auth::user()->organization_type==0)
            {
            $request_data['user_history'] = $request->company_history;
            }
            else
            {
            $request_data['user_history'] = $request->individual_history;
            }
            $request_data['video_urls'] = json_encode(array_filter($request->video_links));

            // Syncing to pivot tables
            $user->industries()->detach();
            $user->industries()->attach($request->industry_ids);
            $user->categories()->detach();
            $user->categories()->attach($request->categories_ids);
            $user->subCategories()->detach();
            $user->subCategories()->attach($request->sub_categories_ids);
             $user->skills()->detach();
             $user->skills()->attach($request->skills);

            $user_detail = UserDetail::where('user_id', $user->id)->update($request_data);
            if (!empty($request->portfolio_titles)) {

                $portfolioArray = [];
                foreach ($request->portfolio_titles as $key => $portfolio) {
                    if (!empty($portfolio)) {

                        $path = public_path('storage/user/portfolio-images');
                        if (!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
                        $portfolioArray['user_id'] =  $user->id;
                        //dd($request->portfolio_images[$key]->getClientOriginalName());
                        if(isset($request->portfolio_images[$key])){
                            $name = uniqid() . '_' . trim($request->portfolio_images[$key]->getClientOriginalName());
                            $request->portfolio_images[$key]->move($path, $name);
                            $portfolioArray['file_name'] = $name;
                        }
                        $portfolioArray['portfolio_title'] = $portfolio;
                        $portfolioArray['portfolio_description'] = $request->portfolio_descriptions[$key];
                        UserPortfolio::create($portfolioArray);
                    }
                }
            }
            if ($user_detail) {
                $user_step = User::where('id', $user->id)->update(['step' => 6]);
                $userDetails = UserDetail::where('user_id', $user->id)->first();
               
                //here any referal is there, then update on invited _client tabletable
                $invited_client = [];
                $invited_client['client_id'] = $user->id;
                $invited_client['freelancer_id'] = $userDetails->referral_user_id;
                $invited_client['status'] = 1;
               
                $invited_client = InvitedClient::where($invited_client)->update(['is_availed' => 1]);

                //return view('user.registration.view_public_profile',compact('user'));
                return redirect()->route('account.viewPublicProfile');
            } else {
                return view('user.registration.create_public_profile');
            }
        } else {
            return redirect()->route('home');
        }
    }
    /**
     * viewPublicProfile :  View the public profile details
     */
    public function viewPublicProfile(Request $request)
    {
        $user = auth('web')->user();
        //$user = User::where('id','=',1)->first();
        if ($user) {
            $user_details = UserDetail::where('user_id', '=', $user->id)->first();
            $slctd_industries = $user->industries->pluck('id')->toArray();

            $slctd_categories = $user->categories->pluck('id')->toArray();
            $slctd_sub_categories = $user->subCategories->pluck('id')->toArray();
            return view('user.registration.view_public_profile', compact('user', 'user_details', 'slctd_industries', 'slctd_categories', 'slctd_sub_categories'));
        } else {
            return redirect()->route('home');
        }
    }
    /**
     * editPublicProfile : To get update public profile form
     */
    public function editPublicProfile(Request $request)
    {
        $user = auth('web')->user();
        //$user = User::where('id','=',1)->first();
        if ($user) {
            $slctd_industries = $user->industries->pluck('id')->toArray();
            $slctd_categories = $user->categories->pluck('id')->toArray();
            $slctd_sub_categories = $user->subCategories->pluck('id')->toArray();
            $slctd_skills = $user->skills->pluck('id')->toArray();

            $industries = Industry::where('active', 1)->get();
            $categories = Category::where('active', 1)->whereHas('industries', function ($q) use ($slctd_industries) {
                $q->whereIn('id', $slctd_industries);
            })->get();
            $sub_categories = SubCategory::where('active', 1)->whereHas('categories', function ($q) use ($slctd_categories) {
                $q->whereIn('id', $slctd_categories);
            })->get();
            $skills = Skill::where('active', 1)->whereHas('subCategories', function ($q) use ($slctd_sub_categories) {
                $q->whereIn('id', $slctd_sub_categories);
            })->get();
            return view('user.registration.edit_public_profile', compact('user', 'industries', 'categories', 'sub_categories', 'slctd_industries', 'slctd_categories', 'slctd_sub_categories','slctd_skills','skills'));
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * updatePublicProfile : To update the user public profile details
     * inputs : request array
     * Response : If success redirect to public profile view page
     */
    public function updatePublicProfile(Request $request)
    {
        $user = auth('web')->user();
        //$user = User::where('id','=',1)->first();
        if ($user) {
            $request_data = [];
            $user_data = [];
            if ($request->hasFile('profile_image')) {
                $rules = [
                    'profile_image' => 'required|
					mimes:jpeg,jpg,png|max:10000'
                ];
                $request->validate($rules);
                $icon = $request->file('profile_image')->store('public/user/profile-images');
                $user_data['profile_image'] = str_replace('public/user/profile-images', '', $icon);
            } 

            User::where('id', $user->id)->update($user_data);
            $request_data['profile_headline'] = $request->profile_headline;
            $request_data['website_url'] = $request->website_url;
            $request_data['company_tagline'] = $request->company_tagline;
            $request_data['about_me'] = $request->about_me;
            if (!empty($request->established_date)) {
                $request_data['established_since'] = gmtDateTime(strtotime($request->established_date), 'date');
            } else {
                $request_data['established_since'] = NULL;
            }
            if (!empty($request->years_of_experience)) {
                $request_data['years_of_experience'] = $request->years_of_experience;
            }
            if (!empty($request->corporate_presentation)) {
                $request_data['corporate_presentation'] = $request->corporate_presentation;
            }
            if(Auth::user()->organization_type==0)
            {
            $request_data['user_history'] = $request->company_history;
            }
            else
            {
            $request_data['user_history'] = $request->individual_history;
            }
            $request_data['video_urls'] = json_encode(array_filter($request->video_links));

            // Syncing to pivot tables
            $user->industries()->detach();
            $user->industries()->attach($request->industry_ids);
            $user->categories()->detach();
            $user->categories()->attach($request->categories_ids);
            $user->subCategories()->detach();
            $user->subCategories()->attach($request->sub_categories_ids);
            $user->skills()->detach();
            $user->skills()->attach($request->skills);

            $user_detail = UserDetail::where('user_id', $user->id)->update($request_data);
           
            if(isset($request->portfolio_id))
            {
                UserPortfolio::whereNotIn('id', $request->portfolio_id)->where('user_id', $user->id)->delete();
            }
          
            if (!empty($request->portfolio_titles)) {
              
                $path = public_path('storage/user/portfolio-images');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
          
                foreach ($request->portfolio_titles as $key => $portfolio) {
                    $portfolioArray = [];
                    if (isset($request->portfolio_id[$key]) && !empty($request->portfolio_id[$key])) {
                        if(isset($request->portfolio_images[$key]) && !empty($request->portfolio_images[$key]))
                        {
                        
                            $name = uniqid() . '_' . trim($request->portfolio_images[$key]->getClientOriginalName());

                            $request->portfolio_images[$key]->move($path, $name);
                            $portfolioArray['file_name'] = $name;
                        }

                        $portfolioArray['portfolio_title'] = $portfolio;
                        $portfolioArray['portfolio_description'] = $request->portfolio_descriptions[$key];
                        UserPortfolio::where('id', $request->portfolio_id[$key])->update($portfolioArray);
                    } else {
                        if(!empty($portfolio))
                        {
                            $portfolioArray['user_id'] = $user->id;
                            if (isset($request->portfolio_images[$key]) && !empty($request->portfolio_images[$key])) {
                                $name = uniqid() . '_' . trim($request->portfolio_images[$key]->getClientOriginalName());
                                
                                $request->portfolio_images[$key]->move($path, $name);
                                $portfolioArray['file_name'] = $name;
                            }
                            $portfolioArray['portfolio_title'] = $portfolio;
                            $portfolioArray['portfolio_description'] = $request->portfolio_descriptions[$key];
                            UserPortfolio::create($portfolioArray);
                        }
                    }
                
                }
            }
        
            if ($user_detail) {
                $user_step = User::where('id', $user->id)->update(['step' => 6]);
                //return view('user.registration.view_public_profile',compact('user'));
                return redirect()->route('account.viewPublicProfile');
            } else {
                return view('user.registration.edit_public_profile');
            }
        } else {
            return redirect()->route('home');
        }
    }

    /**
     * Resend Email
     */

    public function resendEmail(Request $request)
    {

        $resultArr = [];
        $otp = EmailOTP::sendOTP($request->email);
        if ($otp) {
            $resultArr['email_resend'] = 1;
        } else {
            $resultArr['email_resend'] = 0;
        }
        return response()->json($resultArr);
    }

    /**
     * viewUserDetails : Show user detail page
     */
    public function viewUserDetails(User $user,Project $project,$status)
    {
        // $user = auth('web')->user();
        // $user = User::where('id','=',1)->first();
        if ($user) {
            $user_details = UserDetail::where('user_id', '=', $user->id)->first();
            $cond = [];
            $cond['freelancer_id'] = $user->id;
            $cond['project_id'] = $project->id;
            $invitations = ProjectInvitation::where($cond)->first();
            $slctd_industries = $user->industries->pluck('id')->toArray();
            $slctd_categories = $user->categories->pluck('id')->toArray();
            $slctd_sub_categories = $user->subCategories->pluck('id')->toArray();
            
           $ratings = UserRating::select('id','rated_by_user_id','rating_to_user_id','project_id',DB::raw('round(AVG(rating),1) as rating'))->where('rating_to_user_id',$user->id)->groupby('project_id')->get();
          
            return view('user.project.user_detail', compact('user','project', 'user_details', 'invitations','slctd_industries', 'slctd_categories', 'slctd_sub_categories','status','ratings'));
        }
    }
    
    /**
     * myProfile: To show the profile details of the user
     */
    public function myProfile(Request $request, $user){
        $user = User::where('id', '=', $user)->first();
        if($user->user_type == 0){
            // if client, get the project details visible to public
            $projects = Project::where('user_id',$user->id)->pluck('title')->toArray();
            $portfolio_list = Project::where('user_id',$user->id)
                              ->whereIn('status', [4,5])
                              ->where('hide_from_public',1)->get();
        }
        else{
            // if freelancer, get the portfolio details
            $projects = Project::whereIn('status', [2, 3]);
			$projects = $projects->with('contracts')->whereHas('contracts', function($query)  {
				$query->where([['freelancer_id', '=',auth('web')->user()->id]]);
            });
            $portfolio_list = $user->portfolio;
        }
        $user = auth('web')->user();
        $conditions = [];
        $conditions['active'] = 1;
        $conditions['user_id'] = $user->id;
        $user_subscription = UserSubscription::where($conditions)->orderby('id','DESC')->first();
        $security_questions = UserQuestionsResponse::where('user_id',$user->id)->with('securityQuestions')->get();
        $ratings = UserRating::select('id','rated_by_user_id','rating_to_user_id','project_id',DB::raw('round(AVG(rating),1) as rating'))->where('rating_to_user_id',$user->id)->groupby('project_id')->get();
       
        return view('user.project.my_profile',compact('user','portfolio_list','projects','security_questions','user_subscription','ratings')); 
    }

}
