<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Invoice;
use Carbon\Carbon;
use App\Transaction;
use App\MembershipPlan;
use App\PaymentGateway;
use App\UserSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\PaymentGateways\PaymentGatewaysController;

class MembershipPlanController extends Controller
{
    
    /**
     * For listing membership plan details
     */

     public function membershipPlans(){
		  $user = auth('web')->user();
		  $subscription = UserSubscription::get();
		  if($user){
			if($user->user_type == 0){
				
				// $monthly_plans = MembershipPlan::where(['plan_type'=>1,'user_type'=>0])->orderBy('monthly_amount','asc')->get();
				// $yearly_plans = MembershipPlan::where(['plan_type'=>2,'user_type'=>0])->orderBy('yearly_amount','asc')->get();
				//currently subscription is disabled, Now assign one plan automatically for client
				 //update active status
				 $cond = [];
				 $cond['active'] = 0;
				 UserSubscription::where('user_id', $user->id)->update($cond);
				 //get plan id
				 $plan_details = MembershipPlan::where('name', 'free')->first();
				
				 $sub = [];
				 $sub['user_id'] = $user->id;
				 $sub['membership_plan_id'] = $plan_details->id;
				 $sub['start_date'] = Carbon::now();
				 $sub['end_date'] =  date('Y-m-d', strtotime('+1 years'));;
				 $sub['duration_type'] = ($plan_details->plan_type == 0) ? 0 : 1;
				 $sub['amount'] =  $plan_details->monthly_amount ;
				 $sub['setup_charge'] =  0;
				 $sub['service_percentage'] = 0;
				 $sub['active'] = 1;
				 $sub['auto_renewal'] = 1;
				 $sub['remaining_proposal_count'] = $plan_details->number_of_proposals;
				 $sub['remaining_project_count'] = $plan_details->number_of_projects;
				
				 $subscription = UserSubscription::create($sub);
				
				
				 //insert an entry in transaction table
				//  $transaction_sts = Transaction::create([
				// 	 'user_id' => $user->id,
				// 	 'type' => 0,
				// 	 'amount' => $plan_details->monthly_amount,
				// 	 'total_amount' => $plan_details->monthly_amount,
				// 	 'transaction_type' => 0,
				// 	 'notes' => "Subscription payment completed successfully",
				// 	 'payment_status' => 1,
				// 	 'payment_response' => "Paid",
				// 	 'user_subscription_id' => $subscription->id
				//  ]);
				//  if ($user->step != 6) {
				// 	 $user_step = User::where('id', $user->id)->update(['step' => 5]);
				//  }
				//  // Generate invoice for subscription payment
				//  $input_data = [];
				//  $input_data['user_id'] = $user->id;
				//  $input_data['type'] = 5;
				//  $input_data['amount'] = $plan_details->monthly_amount;
				//  $input_data['notes'] = "Invoice generated for the subscription payment";
				//  $input_data['status'] = 1;
				//  $input_data['user_subscription_id'] = $subscription->id;
				//  $now_date = Carbon::now();;
				//  $input_data['paid_on'] = $now_date->toDateTimeString();;
				//  Invoice::invoiceGeneration($input_data,$user,);
				 return redirect()->route('membership_plan.subscriptionSuccess');

			}else{
				$monthly_plans = MembershipPlan::where(['plan_type'=>1,'user_type'=>1,'active'=>1])->orderBy('monthly_amount','asc')->get();
				$yearly_plans = MembershipPlan::where(['plan_type'=>2,'user_type'=>1,'active'=>1])->orderBy('yearly_amount','asc')->get();
				return view('user.membership_plan.membership_plans',compact('monthly_plans','yearly_plans','user','subscription'));
			}
		  }else{
			return redirect()->route('home')->with('toastr', ['type'=>'error','text'=>'Authentication error']);;
		  }
       
        
        
     }
	 /**
	  * planReview : for showing selected plan details
	  */
     public function planReview(MembershipPlan $membershipPlan){
		$user = auth('web')->user();
		$payment_keys = PaymentGateway::where('status',1)->first();
		$paymentGateway = new PaymentGatewaysController;
		if($paymentGateway->initiatePaymentGateway()){
			$request = [];
			if($membershipPlan->plan_type == 1){
				$request['charge'] = $membershipPlan->monthly_amount;
			}else{
				$request['charge'] = $membershipPlan->yearly_amount;
			}
			//if check the subscription
			$user_subscription = UserSubscription::where('user_id',$user->id)->get();
			
			if(isset($user_subscription) && !$user_subscription->isNotEmpty()){
				$request['extra_charge'] =   $membershipPlan->setup_charge;
				$request['product_id'] =  $membershipPlan->product_id;
			}
			$request['subscription'] = 1;
			$request['plan_id'] = $membershipPlan->plan_id;
            $request['name'] = "Membership Plan Subscription";
			$request['email'] = $user->email;
			
            $request['success_url'] = route('membership_plan.subscriptionSuccess');
            $request['cancel_url'] = route('membership_plan.membershipPlans');
            $request['description'] = "Subacription amount".$request['charge']. " for the membership plan ".$membershipPlan->name."will be debited by Keyoxa.";
			$checkout_session =  $paymentGateway->createCheckoutSession($request);
			
			return view('user.membership_plan.plan_review',compact('membershipPlan','payment_keys','checkout_session'));
		}
     }

}
