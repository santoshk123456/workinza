<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Response;


class ResetPasswordController extends Controller
{

   use ResetsPasswords;
   /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
      protected function sendResetResponse(Request $request, $response)
      {
         return response()->json(['data'=>'Password successfully updated.'],201);
      }

   /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
      protected function sendResetFailedResponse(Request $request, $response)
      {
         return response()->json(['data'=>'Sorry some error occured. Please try again after some time.'],404);
      }
   
   
}