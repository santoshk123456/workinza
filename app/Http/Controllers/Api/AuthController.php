<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use Hash;
use App\User;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Email or password does\'nt exist'],401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Register Function
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(UserRequest $request)
    {
        // Request data
        $request_data = [];
        $request_data['username'] = $request->username;
        $request_data['first_name'] = $request->first_name;
        $request_data['last_name'] = $request->last_name;
        $request_data['email'] = $request->email;
        $request_data['password'] = Hash::make($request->password);
        $request_data['mobile_number'] = $request->mobile_number;
        if($request->active && !empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        $request_data['remember_token'] = $request->remember_token;
        if($request->hasFile('profile_image')){
            $profile_image = $request->file('profile_image')->store('public/user/profile-images');
            $request_data['profile_image'] = str_replace('public/user/profile-images','',$profile_image);
        }
        // Create new user
        $user = User::create($request_data);
        return $this->login($request);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}