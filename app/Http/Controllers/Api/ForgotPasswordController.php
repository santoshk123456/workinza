<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Response;


class ForgotPasswordController extends Controller
{

   use SendsPasswordResetEmails;

   /**
     * Get the response for a successful password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
      protected function sendResetLinkResponse(Request $request, $response)
      {
         return  response()->json([
            'data'=>'Email sent successfully.'
         ],200);
      }

   /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
      protected function sendResetLinkFailedResponse(Request $request, $response)
      {
         return  response()->json([
            'error'=>'Please enter a valid email.'
         ],404);
      }
}