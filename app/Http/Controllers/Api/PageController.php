<?php

namespace App\Http\Controllers\Api;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class PageController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function show(Request $request){
        $page = Page::where('heading',$request->heading)->first();
        return response()->json(compact('page'));
    }
    /**
    * @param role
    * Function to handle the change password form request
    * @return mixed
    * @throws \Exception
    */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = User::where('id',Auth::guard('api')->user()->id)->first();
        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json(['errors'=>['current_password'=>'The current password you entered is incorrect.']],422);
        }
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json(['message'=>'Successfully changed the password.']);
    }
}