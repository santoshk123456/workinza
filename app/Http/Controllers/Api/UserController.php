<?php

namespace App\Http\Controllers\Api;

use Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\ChangePasswordRequest;


class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function show(Request $request){
        $user = Auth::guard('api')->user();
        if(!empty($user->profile_image) && (file_exists( public_path().'/storage/admin/profile-images/'.auth()->user()->profile_image))){
            $user->profile_image = asset('/storage/admin/profile-images/'.auth()->user()->profile_image);
        } else {
            $user->profile_image = asset('/images/admin/starter/default.png');
        }
        return response()->json(compact('user'));
    }
    /**
    * @param role
    * Function to handle the change password form request
    * @return mixed
    * @throws \Exception
    */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = User::where('id',Auth::guard('api')->user()->id)->first();
        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json(['errors'=>['current_password'=>'The current password you entered is incorrect.']],422);
        }
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json(['message'=>'Successfully changed the password.']);
    }
    /**
     * For storing the details submitted in edit form
     *
     * @param UserRequest $request
     * @param User $user
     * @return void
     */
    public function update(UserRequest $request)
    {
        $user = User::where('id',Auth::guard('api')->user()->id)->first();
        // Request data
        $request_data = [];
        $request_data['username'] = $request->username;
        $request_data['first_name'] = $request->first_name;
        $request_data['last_name'] = $request->last_name;
        $request_data['email'] = $request->email;
        $request_data['mobile_number'] = $request->mobile_number;
        if($request->active && !empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        if ($request->hasFile('profile_image'))
        {
            $profile_image = $request->file('profile_image')->store('public/user/profile-images');
            $request_data['profile_image'] = str_replace('public/user/profile-images/','',$profile_image);
        }
        // Update user
        $user->update($request_data);
        
        return response()->json(['message'=>'Successfully updated the profile.']);
    }
}