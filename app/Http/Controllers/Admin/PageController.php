<?php

namespace App\Http\Controllers\Admin;

use Cookie;
use App\Page;
use App\User;
use App\Admin;
use DataTables;
use App\Project;
use App\SmsQueue;
use Carbon\Carbon;
use App\EmailQueue;
use App\Transaction;
use Illuminate\Http\Request;
use App\PushNotificationQueue;
use App\FrequentlyAskedQuestion;
use App\DataTables\PageDataTable;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PageRequest;


class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $month_array = [];
        $amount_array = [];

        $transactionsgraph =  Transaction::orderBy('created_at', 'desc')
        ->get()
        ->groupBy(function($val) {
        return Carbon::parse($val->created_at)->format('M');
        })
        ->map(function ($month) {
            return $month->sum('amount');
        })
        ->take(3);
        
      //looping

        foreach($transactionsgraph as $key=>$value)
        {
       $month_array[]=$key;
       $amount_array[] =$value;
        }
       


//for key month name
//collect suim amount 
 
//key amount push 

       
        

        $client_companies = User::where([['user_type',0],['organization_type',0]])->get();
        $client_company_count = $client_companies->count(); 
        // dd($client_company_count);
        $current_month_client_company_count = count($client_companies->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()]));

        $client_individuals = User::where([['user_type',0],['organization_type',1]])->get();
        $client_individual_count = $client_individuals->count(); 
        $current_month_client_individual_count = count($client_individuals->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()]));

        $freelancers_companies = User::where([['user_type',1],['organization_type',0]])->get();
        $freelancer_company_count = $freelancers_companies->count(); 
        $current_month_freelancer_company_count = count($freelancers_companies->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()]));

        $freelancers_individuals = User::where([['user_type',1],['organization_type',1]])->get();
        $freelancer_individual_count = $freelancers_individuals->count(); 
        $current_month_freelancer_individual_count = count($freelancers_individuals->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()]));

        

        $transactions_amount = Transaction::sum('transactions.amount');
    

        $project_open = Project::whereIn('status', [0, 1])->get();
        $project_open_count = $project_open->count();

        $project_inprogress = Project::where('status',3)->get();
        $project_inprogress_count = $project_inprogress->count();

        $project_completed = Project::where('status',4)->get();
        $project_completed_count = $project_completed->count();

        $admins = Admin::get(); 
        $admin_count = $admins->count();
        $current_month_admin_count = count($admins->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()]));

        $pages = Page::get();
        $page_count = $pages->count();
        $current_month_page_count = count($pages->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()]));

        $faqs = FrequentlyAskedQuestion::get();
        $faq_count = $faqs->count();
        $current_month_faq_count =  count($faqs->whereBetween('created_at', [Carbon::now()->subDays(30), Carbon::now()])); 
        
        $email_queues = EmailQueue::get();
        $emails_queues_count = $email_queues->count();
        $email_queues_not_send = count($email_queues->where('sent_status', 1));
        $email_queues_sent = count($email_queues->where('sent_status', 0));

        $push_queues = PushNotificationQueue::get();
        $push_queues_count = $push_queues->count();
        $push_queues_not_send = count($push_queues->where('sent_status', 1));
        $push_queues_sent = count($push_queues->where('sent_status', 0));

        $sms_queues = SmsQueue::get();
        $sms_queues_count = $sms_queues->count();       
        $sms_queues_not_send = count($sms_queues->where('sent_status', 1));        
        $sms_queues_sent = count($sms_queues->where('sent_status', 0));

        return view('admin/pages/dashboard', compact('amount_array','month_array','client_company_count','transactions_amount','client_individual_count', 'admin_count', 'page_count' ,'faq_count',
         'current_month_client_company_count','current_month_client_individual_count', 'current_month_admin_count', 'current_month_page_count', 
         'current_month_faq_count', 'emails_queues_count', 'email_queues_not_send', 'email_queues_sent',
         'push_queues_count', 'push_queues_not_send', 'push_queues_sent',
         'sms_queues_count', 'sms_queues_not_send', 'sms_queues_sent','freelancer_company_count','current_month_freelancer_company_count','freelancer_individual_count',
         'current_month_freelancer_individual_count','project_inprogress_count','project_completed_count','project_open_count'));
    }
    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index(Builder $builder,PageDataTable $dataTable)
    {
        return $dataTable->render('admin.pages.index');
    }
    /**
     * Page Edit Function
     *
     * @param Page $page
     * @return void
     */
    public function edit(Page $page)
    {
        // Listing all the roles that are needed to be displayed in select box
        return view('admin/pages/edit', compact('page'));
    }
    /**
     * Page Update Function
     *
     * @param PageRequest $request
     * @param Page $page
     * @return void
     */
    public function update(PageRequest $request,Page $page)
    {
        // Request data
        $request_data = [];
        $request_data['heading'] = $request->heading;
        $request_data['meta_title'] = $request->meta_title;
        $request_data['meta_keywords'] = $request->meta_keywords;
        $request_data['meta_description'] = $request->meta_description;
        $request_data['short_description'] = $request->short_description;
        if(!empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        $request_data['content'] = $request->content;
        if ($request->hasFile('image'))
        {
            $image = $request->file('image')->store('public/cms-page-images');
            $request_data['image'] = str_replace('public/cms-page-images/','',$image);
        }

        if ($request->hasFile('profile_image')) {
            
            $icon = $request->file('profile_image')->store('public/cms-page-images');
            $request_data['image'] = str_replace('public/cms-page-images', '', $icon);
        

        } elseif ($request->remove_title == 'removed') {
            $request_data['image'] = "";
           // unlink(public_path() . '/storage/user/profile-images' . $user->profile_image);
        }
        // dd($request_data);
        // Update admin
        $page->update($request_data);
        return redirect()->route('admin.pages.index')->with('toastr', ['type'=>'success','text'=>__('Page details have been updated successfully.')]);
    }
    /**
     * Function for displaying edit page form
     */
    public function create()
    {
        return view('admin/pages/create');
    }
    /**
     * For updating the request data of CMS page form
     */
    public function store(PageRequest $request)
    {
       //dd($request->all());
        // Request data
        $request_data = [];
        $request_data['heading'] = $request->heading;
        $request_data['slug'] = $request->slug;
        $request_data['meta_title'] = $request->meta_title;
        $request_data['meta_keywords'] = $request->meta_keywords;
        $request_data['meta_description'] = $request->meta_description;
        $request_data['short_description'] = $request->short_description;
        if(!empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        $request_data['content'] = $request->content;
        if ($request->hasFile('image'))
        {
            $image = $request->file('image')->store('public/cms-page-images');
            $request_data['image'] = str_replace('public/cms-page-images/','',$image);
        }
        if ($request->hasFile('profile_image')) {   
          
           
            $icon = $request->file('profile_image')->store('public/cms-page-images');
            $request_data['image'] = str_replace('public/cms-page-images/', '', $icon);
        } else {
            $request_data['image'] = "";
        }


        // Create new page
        Page::create($request_data);
        return redirect()->route('admin.pages.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new page.')]);
    }
    /**
     * For storing the current status of the leftbar[Enabled/Disabled] in the session
     */
    public function toggleLeftbar()
    {
        $value = "disabled";
        $current_cookie_value = Cookie::get('enable_full_leftbar');
        if($current_cookie_value){
            if($current_cookie_value == "enabled"){
                $value = "disabled";
            } else {
                $value = "enabled";
            }
        }
        Cookie::queue('enable_full_leftbar', $value, 45000);
    }
    /**
     * For displaying the CMS page data
     */
    public function show(Page $page)
    {
        return view('admin.pages.show', compact('page'));
    }
    
}
