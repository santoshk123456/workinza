<?php

namespace App\Http\Controllers\Admin;

use App\TermsAndCondition;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\DataTables\TermsAndConditionDataTable;
use App\Http\Requests\Admin\TermsAndConditionrequest;

class TermsAndConditionController extends Controller
{
    /**
     * To list terms and conditions
     */

    public function index(Builder $builder,TermsAndConditionDataTable $dataTable)
    {
        return $dataTable->render('admin.terms_and_conditions.index');
    }

    /**
     * To create terms and condition
     */

     public function create(){

         return view('admin.terms_and_conditions.create');
     }

     /**
      * To store terms and conditions
      */

      public function store(TermsAndConditionrequest $request){
          $request_data = [];
          $request_data['heading'] = $request->heading;
          $request_data['slug'] = $request->slug;
          $request_data['static_heading'] = $request->static_heading;
          $request_data['version'] = $request->version;
          $request_data['content'] = $request->description;
          if($request->has('short_description')){
            $request_data['short_description'] = $request->short_description;
          }
          if(TermsAndCondition::create($request_data))
          return redirect()->route('admin.terms_and_conditions.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new Terms and Condition.')]);
          else
          return redirect()->route('admin.terms_and_conditions.index')->with('toastr', ['type'=>'error','text'=>__('Terms and Condition can not be created.')]);
    }

     /**
     * To edit terms and condition
     */

    public function edit(TermsAndCondition $termsAndCondition){

        return view('admin.terms_and_conditions.edit',compact('termsAndCondition'));
    }

    /**
     * To update terms and conditions
     */

    public function update(TermsAndConditionrequest $request,TermsAndCondition $termsAndCondition){

        $request_data = [];
        $request_data['heading'] = $request->heading;
        $request_data['slug'] = $request->slug;
        $request_data['version'] = $request->version;
        $request_data['content'] = $request->description;
        if($request->has('short_description')){
          $request_data['short_description'] = $request->short_description;
        }
        if($termsAndCondition->update($request_data))
        return redirect()->route('admin.terms_and_conditions.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the Terms and Condition.')]);
        else
        return redirect()->route('admin.terms_and_conditions.index')->with('toastr', ['type'=>'error','text'=>__('Terms and Condition can not be updated.')]);
  }

  /**
   * To show terms and conditions
   */

  public function show(TermsAndCondition $termsAndCondition){

    return view('admin.terms_and_conditions.show',compact('termsAndCondition'));
}


 /**
     * To delete terms and conditions
     */

    public function destroy(TermsAndCondition $termsAndCondition)
    {
        $termsAndCondition->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Terms and condition has been deleted successfully.'),]);
    }


}
