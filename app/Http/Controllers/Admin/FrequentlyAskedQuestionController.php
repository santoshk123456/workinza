<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FrequentlyAskedQuestion;
use Carbon\Carbon;
use App\Http\Requests\Admin\FrequentlyAskedQuestionRequest;
use Spatie\Activitylog\Models\Activity;
use App\Exceptions\GeneralException;



class FrequentlyAskedQuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * For Displaying the FAQ Listing page
     * @param type
     * @param status
     */
    public function index(Request $request,$type = null,$status = null)
    {
        if(empty($type)){
            $type = "user";
        } 
        if($status == 'active'){
            $active = 1;
        } else {
            $active = 0;
        }
        $frequently_asked_questions = FrequentlyAskedQuestion::where('type',$type)->orderBy('updated_at','desc');;
        // If search parameter is present
        if(isset($request->keyword) && ($request->keyword != "")){
            $frequently_asked_questions = $frequently_asked_questions->where('question','like','%'.$request->keyword.'%')->orWhere('answer','like','%'.$request->keyword.'%');
        }
        // Get all the faq based on the type
        if(!empty($status)){
            $frequently_asked_questions = $frequently_asked_questions->where('active',$active);
        }
        // For implementing the pagination
        $frequently_asked_questions = $frequently_asked_questions->orderByDesc('updated_at')->paginate(10);
        // For Listing all the Roles
        return view('admin.frequently_asked_questions.index', compact('frequently_asked_questions','type','status'));
    }
    /**
     * FAQ Create Function
     *
     * @param [type] $type
     * @return void
     */
    public function create($type = null)
    {
        if(empty($type)){
            $type = 'user';
        }
        return view('admin.frequently_asked_questions.create', compact('type'));
    }
    /**
     * FAQ Store Function
     *
     * @param FrequentlyAskedQuestionRequest $request
     * @param [type] $type
     * @return void
     */
    public function store(FrequentlyAskedQuestionRequest $request,$type=null)
    {
        if(empty($type)){
            $type = 'user';
        }
        // Request data
        $request_data = [];
        $request_data['question'] = $request->question;
        $request_data['answer'] = $request->answer;
        if(!empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        $request_data['type'] = $request->type;
        // Create new frequentlyasked question
        $faq = FrequentlyAskedQuestion::create($request_data);
        if ($faq) {
            // Assign admin role
            return redirect()->route('admin.frequently_asked_questions.index', $type)->with('toastr', ['type'=>'success','text'=>'Frequently asked question created successfully.',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }
    /**
     * FAQ Create Function
     *
     * @param [type] $type
     * @return void
     */
    public function edit(FrequentlyAskedQuestion $frequentlyAskedQuestion,$type = null)
    {
        if(empty($type)){
            $type = 'user';
        }
        return view('admin.frequently_asked_questions.edit', compact('frequentlyAskedQuestion','type'));
    }
    /**
     * FAQ Store Function
     *
     * @param FrequentlyAskedQuestionRequest $request
     * @param [type] $type
     * @return void
     */
    public function update(FrequentlyAskedQuestionRequest $request,FrequentlyAskedQuestion $frequentlyAskedQuestion,$type=null)
    {
        if(empty($type)){
            $type = 'user';
        }
        // Request data
        $request_data = [];
        $request_data['question'] = $request->question;
        $request_data['answer'] = $request->answer;
        if(!empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        // Create new frequentlyasked question
        $frequentlyAskedQuestion = $frequentlyAskedQuestion->update($request_data);
        if ($frequentlyAskedQuestion) {
            return redirect()->route('admin.frequently_asked_questions.index', $type)->with('toastr', ['type'=>'success','text'=>'Frequently asked question has been updated successfully.',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }
    /**
     * Destroy function
     *
     * @param FrequentlyAskedQuestion $frequentlyAskedQuestion
     * @param [type] $type
     * @return void
     */
    public function destroy(FrequentlyAskedQuestion $frequentlyAskedQuestion,$type=null)
    {
        $frequentlyAskedQuestion->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Frequently asked question has been deleted successfully.'),]);
    }
    
}
