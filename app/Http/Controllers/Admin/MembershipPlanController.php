<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Project;
use App\Setting;
use App\Transaction;
use App\InvitedClient;
use App\MembershipPlan;
use App\ProjectContract;
use App\ProjectMilestone;
use App\UserWalletHistory;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\DataTables\WalletDataTable;
use App\Http\Controllers\Controller;
use App\DataTables\MembershipPlanDataTable;
use App\Http\Requests\Admin\MembershipPlanRequest;
use App\Libraries\PaymentGateways\PaymentGatewaysController;

class MembershipPlanController extends Controller
{
       /**
     * To list membership plan table
     * */
    public function index(Builder $builder, MembershipPlanDataTable $dataTable)
    {
        return $dataTable->render('admin.membership_plans.index');
    }

     /**
     * To create membership plan
     */

    public function create()
    {
        return view('admin.membership_plans.create');
    }

     /***
     * To store Sub Categories 
     */

    public function store(MembershipPlanRequest $request)
    {
        
       $request_data = [];
       $request_data['name'] = $request->plan_name;
       $request_data['user_type'] = $request->user_type;
       $request_data['plan_type'] = $request->plan_type;
       $request_data['monthly_amount'] = $request->monthly_amount;
       $request_data['yearly_amount'] = $request->yearly_amount;
       if(!empty($request->user_type) && $request->has('max_project_fr') && !empty($request->max_project_fr)){
            $request_data['number_of_projects'] = $request->max_project_fr;
       }
       if(empty($request->user_type) && $request->has('max_project_cl') && !empty($request->max_project_cl)){
        $request_data['number_of_projects'] = $request->max_project_cl;
       }
       if($request->has('service_fee') && !empty($request->service_fee)){
        $request_data['service_percentage'] = $request->service_fee;
       }
       if($request->has('max_proposal') && !empty($request->max_proposal) && empty($request->user_type)){
        $request_data['number_of_proposals'] = $request->max_proposal;
       }
       if($request->has('max_products') && !empty($request->user_type)){
        $request_data['number_of_products'] = $request->max_products;
       }
       if($request->has('product_commission') && !empty($request->user_type)){
        $request_data['product_commission'] = $request->product_commission;
       }
   
       $request_data['chat_feature'] = $request->chat_feature;
       $request_data['setup_charge'] = $request->setup_charge;

       $paymentGateway = new PaymentGatewaysController;
        if($paymentGateway->initiatePaymentGateway()){
            
            // create plan in payment gateway

            $data['name'] = $request->plan_name;
            if(!empty($request->monthly_amount))
            $data['amount'] = $request->monthly_amount;
            else
            $data['amount'] = $request->yearly_amount;

            if(!empty($request->monthly_amount))
            $data['interval'] = 0;
            else
            $data['interval'] = 2;
            $payment_gateway_response= $paymentGateway->createPlan($data);
           
            if($payment_gateway_response['success']==1){
    
               $request_data['plan_id'] = $payment_gateway_response['plan']['id'];  
               $request_data['product_id'] = $payment_gateway_response['plan']['product'];               

            }else{
                
               return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'error','text'=>'Payment gateway integration error.']); 
            }

        }else{

            return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'error','text'=>'Payment gateway integration error.']);
        }

       $create_memplan = MembershipPlan::create($request_data);

       if($create_memplan)
       return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new membership plan.')]);
       else
       return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'error','text'=>__('Membership plan can not be created.')]);

    }

      /**
     * To edit Membership Plan
     */

    public function edit(MembershipPlan $membershipPlan)
    {
        return view('admin.membership_plans.edit', compact('membershipPlan'));
    }

    /**
     * To update Membership plan
     */

     public function update(MembershipPlanRequest $request,MembershipPlan $membershipPlan){
       
       $request_data = [];
       $request_data['name'] = $request->plan_name;
       $request_data['user_type'] = $request->user_type;
       $request_data['monthly_amount'] = $request->monthly_amount;
       $request_data['plan_type'] = $request->plan_type;
       $request_data['yearly_amount'] = $request->yearly_amount;
       if(!empty($request->user_type) && $request->has('max_project_fr') && !empty($request->max_project_fr)){
        $request_data['number_of_projects'] = $request->max_project_fr;
       }
       if(empty($request->user_type) && $request->has('max_project_cl') && !empty($request->max_project_cl)){
        $request_data['number_of_projects'] = $request->max_project_cl;
       }
       if($request->has('service_fee') && !empty($request->service_fee)){
        $request_data['service_percentage'] = $request->service_fee;
       }
       if($request->has('max_proposal') && !empty($request->max_proposal) && empty($request->user_type)){
        $request_data['number_of_proposals'] = $request->max_proposal;
       }
       if($request->has('max_products') && !empty($request->user_type)){
        $request_data['number_of_products'] = $request->max_products;
       }
       if($request->has('product_commission') && !empty($request->user_type)){
        $request_data['product_commission'] = $request->product_commission;
       }

       $request_data['chat_feature'] = $request->chat_feature;
       $request_data['setup_charge'] = $request->setup_charge;
     //updateplan amount
         
     $paymentGateway = new PaymentGatewaysController;
     if($paymentGateway->initiatePaymentGateway()){
          // create plan in payment gateway

     $data['name'] = $request->plan_name;
     if(!empty($request->monthly_amount))
     $data['amount'] = $request->monthly_amount;
     else
     $data['amount'] = $request->yearly_amount;

     if(!empty($request->monthly_amount))
     $data['interval'] = 0;
     else
     $data['interval'] = 2;
     $payment_gateway_response= $paymentGateway->createPlan($data);
    
     if($payment_gateway_response['success']==1){

        $request_data['plan_id'] = $payment_gateway_response['plan']['id'];  
        $request_data['product_id'] = $payment_gateway_response['plan']['product'];               

     }else{
         
        return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'error','text'=>'Payment gateway integration error.']); 
     }
     }
       if($membershipPlan->update($request_data)){
           
        return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the membership plan.')]);
       }
      
       else
       return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'error','text'=>__('Membership plan can not be updated.')]);
     }

      /**
     * To show Membership plan 
     */

    public function show(MembershipPlan $membershipPlan)
    {
        return view('admin.membership_plans.show', compact('membershipPlan'));
    }

     /**
     * To delete Membership plan
     */

    public function destroy(MembershipPlan $membershipPlan)
    {
        $membershipPlan->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Membership plan has been deleted successfully.'),]);
    }

     /**
     * To change status of Membership plan
     */

    public function toggleActiveStatus($id)
    {
        $mem_plan = MembershipPlan::where('uuid',$id)->first();
        if(!empty($mem_plan->active)){
            $mem_plan->active = 0;
        } else {
            $mem_plan->active = 1;
        }
        
        if($mem_plan->update()){
            return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'success','text'=>'Membership plan status has been updated successfully']);
        }else{
            return redirect()->route('admin.membership_plans.index')->with('toastr', ['type'=>'error','text'=>'Membership plan status can not be updated ']);
        }
    }

    public function wallets(Builder $builder, WalletDataTable $dataTable)
    {
        return $dataTable->render('admin.membership_plans.wallets');
    }
    public function payToFreelancer(Request $request){
        $notes = "";
        //get user details
        
        $wallet = UserWalletHistory::where('id',$request->wallet_id)->with('transactions.milestones.project','transactions.contracts')->first();
       
        $paid_amount = 0;
        $service_charge = 0;
        $referral_or_bonus = 0;
        $service_charge_after_referralorbouns = 0;
        if(isset($wallet->transactions->contracts) && $wallet->transactions->contracts != ""){
            $service_charge = $wallet->transactions->contracts->freelancer_service_charge;
            if(isset($wallet->transactions->contracts->freelancer_service_charge) && $wallet->transactions->contracts->freelancer_service_charge){
                $service_charge = ($wallet->amount * $wallet->transactions->contracts->freelancer_service_charge)/100;
              
                //check any referral or bonus is there for this freelancer
                $service_charge_after_referralorbouns  = $this->calculateReferralBonus($wallet->user_id,$wallet->transactions->user_id,$service_charge,$wallet->amount);
              
                $paid_amount    = $wallet->amount - $service_charge_after_referralorbouns['charge'];
                $referral_or_bonus = $service_charge_after_referralorbouns['referral_or_bonus'];
            }
        }
        if($wallet->type == 2){
            $transaction_sts = Transaction::create([
                'user_id' => $wallet->user_id,
                'type' => $wallet->type,
                'amount' => $wallet->amount,
                'project_milestone_id'=> isset($wallet->transactions->project_milestone_id)?$wallet->transactions->project_milestone_id:"",
                'project_contract_id'=>isset($wallet->transactions->project_contract_id)?$wallet->transactions->project_contract_id:"",
                'project_id'=>isset($wallet->transactions->project_id)?$wallet->transactions->project_id:"",
                'total_amount' => $paid_amount,
                'transaction_type' => 0,
                'notes' => $request->notes,
                'payment_status' => 1,
                'payment_response' => 'Paid',
               
            ]);
             //change the milestone status
            $milestone_update = ProjectMilestone::where('id',$wallet->transactions->project_milestone_id)->update(['status'=>5]);

            //check all last milestone if yes then update the project to completed
            $milestones = ProjectMilestone::where('project_id',$wallet->transactions->project_id)->where('status','<>',5)->get();
            if($milestones->isEmpty()){
                $project_update = Project::where('id',$wallet->transactions->project_id)->update(['status'=>4]);
            }
            $notes = "Milestone amount debited from your wallet and credited to your payapal account for the milestone ". ucwords($wallet->transactions->milestones->milestone_name)." for the project ".ucwords($wallet->transactions->milestones->project->title);

        }else{
            $transaction_sts = Transaction::create([
                'user_id' => $wallet->user_id,
                'type' => $wallet->type,
                'amount' => $wallet->amount,
                'total_amount' => $paid_amount,
                'transaction_type' => 0,
                'notes' => $request->notes,
                'payment_status' => 1,
                'payment_response' => 'Paid',
               
            ]);
            $notes = $request->notes;
        }
        
       
       

        //get user
        $user_details = User::where('id',$wallet->user_id)->first();
        //create debit entry from wallet
         $wallet_sts = UserWalletHistory::create([
            'transaction_id' => $transaction_sts->id,
            'user_id' => $wallet->user_id,
            'transaction_type' => 1,
            'amount' => $paid_amount,
            'freelancer_service_charge' =>$service_charge,
            'freelancer_service_percentage'=>isset($wallet->transactions->contracts->freelancer_service_charge)?$wallet->transactions->contracts->freelancer_service_charge:"",
            'referral_bonus_percentage' =>isset($service_charge_after_referralorbouns['percentage'])?$service_charge_after_referralorbouns['percentage']:'0',
            'referral_bonus_charge'=> isset($service_charge_after_referralorbouns['charge_deduction'])? $service_charge_after_referralorbouns['charge_deduction']:'0',
            'new_balance' => $user_details->wallet_balance - $paid_amount,
            'notes' => $notes,
            'type' => $wallet->type,
            'payment_status'=>1,
            'referral_or_bonus' => $referral_or_bonus, 
            
        ]);
        $wallet_updation = UserWalletHistory::where('id',$request->wallet_id)->update(['payment_status'=>1]);
        //update user wallet balance
        $update_balance = [];
        $update_balance['wallet_balance'] = $user_details->wallet_balance - $paid_amount;
        User::where('id',$wallet->user_id)->update($update_balance);
        return redirect()->route('admin.membership_plans.wallets')->with('toastr', ['type'=>'success','text'=>'Payment has been completed successfully']);


    }

    public function calculateReferralBonus($freelancer_id,$client_id,$service_charge,$payed_amount){
        $return_arr = [];
        //check any referral avail
        $cond = [];
        $cond['freelancer_id']  =  $freelancer_id;
        //$cond['client_id']      =  $client_id;  
        $cond['is_availed']     =  1;        
        $invited_clients = InvitedClient::where($cond)->first();
       
        if($invited_clients != ""){
            //deduct amount 
            $referral_percentage = Setting::select('value')->where('key', 'referral_percentage')->first()['value'];
            $service_charge_after_deduction = $service_charge - ($service_charge * $referral_percentage ) /100;
            $return_arr['percentage'] = $referral_percentage;
            $return_arr['charge'] = $service_charge_after_deduction;
            $return_arr['charge_deduction'] = ($service_charge * $referral_percentage ) /100;
            $return_arr['referral_or_bonus'] = 0;
            //update avail flag 2 in invited client
            $update_invited_clients = InvitedClient::where($cond)->update(['is_availed'=>2]);
        }else{
            //check the bonus amount
            $bonus_limit = Setting::select('value')->where('key', 'bonus_limit')->first()['value'];
            $bonus_percentage = Setting::select('value')->where('key', 'bonus_percentage')->first()['value'];
            //get sum of all the transaction with client freelancer
            $contract_condition = [];
            $contract_condition['freelancer_id']  =  $freelancer_id;
            $contract_condition['client_id']      =  $client_id;  
            $contract_condition['status']         =  1;  
            $contracts = ProjectContract::where($contract_condition)->pluck('id')->toArray();
           
            //get the sum of total amount of all transaction
            $transact_cond = [];
            $transact_cond['transaction_type']  = 0;
            $transact_cond['type']  = 2;
            $transaction_total_amount = Transaction::where($transact_cond)->whereIn('project_contract_id',$contracts)->sum('total_amount');
           
            if( $bonus_limit <  ($transaction_total_amount + $payed_amount)){
                $service_charge_after_deduction = $service_charge - ($service_charge * $bonus_percentage ) /100;
                $return_arr['percentage'] = $bonus_percentage;
                $return_arr['charge'] = $service_charge_after_deduction;
                $return_arr['charge_deduction'] = ($service_charge * $bonus_percentage ) /100;
                $return_arr['referral_or_bonus'] = 1;
               
            }else{
                $service_charge_after_deduction = 0;
                $return_arr['percentage'] = 0;
                $return_arr['charge'] = 0;
                $return_arr['charge_deduction'] = 0;
                $return_arr['referral_or_bonus'] = 1;
                
            }
          
        }

       
       
       
        return $return_arr;
    }

}
