<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pusher\Pusher;

class PusherNotificationController extends Controller
{
 

    public function sendNotification(){
     
        //Remember to change this with your cluster name.
        // $options = array(
        //     'cluster' => 'ap2',
        //     'encrypted' => true
        // );
        $options = array(
            'cluster' => 'ap2',
            'useTLS' => true
          );   
        $message= "Hello You have clicked  notification test button";
          $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
          );
        //Send a message to notify channel with an event name of notify-event
        $pusher->trigger('notification', 'notification-event', $message);
    }
}
