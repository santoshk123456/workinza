<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\User;
use App\Skill;
use App\Project;
use App\Category;
use App\Industry;
use App\ProjectChat;
use App\SubCategory;
use App\Transaction;
use App\ProjectContract;
use App\ProjectDocument;
use App\ProjectProposal;
use App\ProjectMilestone;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\DataTables\ProjectDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProjectRequest;
use App\DataTables\ProjectCompletedDataTable;
use App\DataTables\ProjectInprogressDataTable;

class ProjectController extends Controller
{
     /**
     * To list project table
     * */
    public function index(Builder $builder, ProjectDataTable $dataTable)
    {
        return $dataTable->render('admin.projects.index');
    }

     /**
     * To list project table
     * */
    public function inprogressList(Builder $builder, ProjectInprogressDataTable $dataTable)
    {
        
        return $dataTable->render('admin.projects.inprogress_project');
    }

    /**
     * To list project table
     * */
    public function completedList(Builder $builder, ProjectCompletedDataTable $dataTable)
    {
        return $dataTable->render('admin.projects.completed_projects');
    }
    
    /**
     * For render create form of project
     */
    public function create()
    {
        $users = User::where(['users.approved'=>1,'users.active'=>1,'users.user_type'=>0])->whereHas('userSubscription',function($q){
            $q->where('user_subscriptions.active',1);
        })->get();
        $industries = Industry::where('active',1)->get();
        $categories_list = Category::select('id','name')->where('active',1)->get();
        return view('admin.projects.create',compact('users','industries','categories_list'));
    }

    /**
     * For store project details
     */
    public function store(ProjectRequest $request){
        // return $request;
        // For Create new project
        $range = explode('-',request('budget_range'));
		$budget_from = trim($range[0]);
		$budget_to =trim($range[1]);
        $project = Project::create([
            'title' => $request->title,
            'user_id' => $request->user_id,
            'location' => $request->location,
            'type' => $request->type,
            'description' => $request->description,
            'budget_from' => $budget_from,
            'budget_to' => $budget_to,
            'timeline_count' => $request->timeline_count,
            'timeline_type' => $request->timeline_type,
            'expected_start_date' => date('Y-m-d',strtotime($request->expected_start_date)),
            'expected_end_date' => date('Y-m-d',strtotime($request->expected_end_date)),
            'bid_start_date' => date('Y-m-d',strtotime($request->bid_start_date)),
            'bid_end_date' => date('Y-m-d',strtotime($request->bid_end_date)),
        ]);

        // Syncing all industries, categories, sub categories and skills with pivots table
        $project->industries()->attach($request->industry_ids);
        $project->categories()->attach($request->categories_ids);
        $project->subCategories()->attach($request->sub_categories_ids);
        $project->skills()->attach($request->skills);

        // Check for project document exisist. if exisist save corresponding detail to project_documents table
        if(!empty($request->project_documents)){
            $docArray = [];
            foreach($request->project_documents as $key=>$doc){
                $docArray = [];
                $docArray['project_id'] = $project->id;
                $docArray['file_name'] = $doc;
                ProjectDocument::create($docArray);
            }
        }
        //Save project milestone details
        if(!empty($request->milestone_names)){
            $milestoneArray = [];
            foreach($request->milestone_names as $key=>$milestone){
                $milestoneArray['project_id'] = $project->id;
                $milestoneArray['milestone_name'] = $milestone;
                $milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
                $milestoneArray['start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
                $milestoneArray['end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
                $milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
                $milestoneArray['status'] = 0;
                ProjectMilestone::create($milestoneArray);
            }
        }
        if ($project) {
            return redirect()->route('admin.projects.index')->with('toastr', ['type'=>'success','text'=>'Project created successfully',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }

    /**
     * Function for edit
     */
    public function edit(Project $project){
        /* $users = User::where(['users.approved'=>1,'users.active'=>1,'users.user_type'=>0])->whereHas('userSubscription',function($q){
            $q->where('user_subscriptions.active',1);
        })->get(); */
        $slctd_industries = $project->industries->pluck('id')->toArray();
        $slctd_categories = $project->categories->pluck('id')->toArray();
        $slctd_sub_categories = $project->subCategories->pluck('id')->toArray();
        $slctd_skills = $project->skills->pluck('id')->toArray();
        $industries = Industry::where('active',1)->get();
        $categories_list =  Category::select('id','name')->where('active',1)->get();
        $categories = Category::select('id','name')->where('active',1)->whereIn('id',$slctd_categories)->get();
        $sub_categories = SubCategory::where('active',1)->whereHas('categories',function($q) use($slctd_categories){
            $q->whereIn('id',$slctd_categories);
        })->get();
        $skills = Skill::where('active',1)->whereHas('subCategories',function($q) use($slctd_sub_categories){
            $q->whereIn('id',$slctd_sub_categories);
        })->get();

        return view('admin.projects.edit',compact('slctd_industries','categories','categories_list','slctd_categories','slctd_sub_categories','slctd_skills'
                                        ,'industries','categories','sub_categories','skills','project'));
    }

    public function update(Project $project, Request $request){
        $range = explode('-',request('budget_range'));
		$budget_from = trim($range[0]);
		$budget_to =trim($range[1]);
        $project->update([
            'title' => $request->title,
            'location' => $request->location,
            'type' => $request->type,
            'description' => $request->description,
            'budget_from' => $budget_from,
            'budget_to' => $budget_to,
            'timeline_count' => $request->timeline_count,
            'timeline_type' => $request->timeline_type,
            'expected_start_date' => date('Y-m-d',strtotime($request->expected_start_date)),
            'expected_end_date' => date('Y-m-d',strtotime($request->expected_end_date)),
            'bid_start_date' => date('Y-m-d',strtotime($request->bid_start_date)),
            'bid_end_date' => date('Y-m-d',strtotime($request->bid_end_date)),
        ]);

        // Syncing all industries, categories, sub categories and skills with pivots table
        $project->industries()->detach();
        $project->categories()->detach();
        $project->subCategories()->detach();
        $project->skills()->detach();

        // Syncing all industries, categories, sub categories and skills with pivots table
        $project->industries()->attach($request->industry_ids);
        $project->categories()->attach($request->categories_ids);
        $project->subCategories()->attach($request->sub_categories_ids);
        $project->skills()->attach($request->skills);

        // Check for project document exisist. if exisist save corresponding detail to project_documents table
        if(!empty($request->project_documents)){
            $docArray = [];
            foreach($request->project_documents as $key=>$doc){
                $docArray = [];
                $docArray['project_id'] = $project->id;
                $docArray['file_name'] = $doc;
                ProjectDocument::create($docArray);
            }
        }
        ProjectMilestone::whereNotIn('id',$request->milestone_id)->where('project_id',$project->id)->delete();
        //Save project milestone details
        if(!empty($request->milestone_names)){
            $milestoneArray = [];
            foreach($request->milestone_names as $key=>$milestone){
                if(isset($request->milestone_id[$key]) && !empty($request->milestone_id[$key])){
                    $milestoneArray['milestone_name'] = $milestone;
                    $milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
                    $milestoneArray['start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
                    $milestoneArray['end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
                    $milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
                    ProjectMilestone::where('id',$request->milestone_id[$key])->update($milestoneArray);
                }else{
                    $milestoneArray['project_id'] = $project->id;
                    $milestoneArray['milestone_name'] = $milestone;
                    $milestoneArray['milestone_amount'] = $request->milestone_amounts[$key];
                    $milestoneArray['start_date'] = date('Y-m-d',strtotime($request->milestone_start_dates[$key]));
                    $milestoneArray['end_date'] = date('Y-m-d',strtotime($request->milestone_end_dates[$key]));
                    $milestoneArray['milestone_description'] = $request->milestone_descriptions[$key];
                    $milestoneArray['status'] = 0;
                    ProjectMilestone::create($milestoneArray);
                }
            }
        }
        if ($project) {
            return redirect()->route('admin.projects.index')->with('toastr', ['type'=>'success','text'=>'Project updated successfully',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }
    /**
     * For upload file
     */
    public function fileUpload(Request $request){
        $path = public_path ('storage/projects/project_documents');
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

    /**
     * Project document remove
     */
    public function removeFile(Request $request)
    {
        $doc= ProjectDocument::where('id',$request->id)->first();
        $path =public_path('storage/projects/project_documents/'. $doc->file_name);
        if(file_exists($path)){
            unlink(  $path );
        }
        $doc->delete();
    }

    /**
     * Function for show
     */
    public function show(Project $project){
        return view('admin.projects.show',compact('project'));
    }

    /**
     * Function update project status
     */
    public function updateStatus(Request $request, Project $project){
        if($project->active == 1){
            $project->active = 0;
            $project->inactive_reason = $request->inactive_reason;
        }else{
            $project->active = 1;
            $project->inactive_reason = "";
        }
        $project->update();
        return redirect()->route('admin.projects.show',$project->uuid)->with('toastr', ['type'=>'success','text'=>'Project status changed successfully',]);
    }

    /**
     * For destroy project
     */
    public function destroy(Project $project){
        $project->delete();
        return redirect()->route('admin.projects.index')->with('toastr', ['type'=>'success','text'=>'Project deleted successfully',]);
    }

    /**
     * Function for list all project proposals
     */
    public function proposals(Project $project){
        return view('admin.projects.proposals',compact('project'));
    }

    public function transactions(Project $project){
        $user = auth('web')->user();
	
			$cond = [];
			// $cond['user_id'] = $user->id;
			$cond['type'] = 2;
			$cond['project_id'] = $project->id;
            $transactions = Transaction::where($cond)->orderBy('id','DESC')->get();
           

        return view('admin.projects.transactions',compact('project','user','transactions'));
    }

    public function chats(Project $project){

    
        $projectContract  = ProjectContract::where('project_id',$project->id)->first();

        $user_id = $projectContract->client_id;
      //  $user_id = $user->id;
        $freelancer_id = $projectContract->freelancer_id;
        $freelancer=User::where('id',$projectContract->freelancer_id)->first();
        $user=User::where('id',$projectContract->client_id)->first();
    



            if (!isset($freelancer->uuid)) {
                $getuser = ProjectChat::where('project_id', $project->id)->where('archive_status', 0)
                    ->where(function ($q) use ($user_id) {
                        $q->Where('from_user_id', $user_id)->orWhere('to_user_id', $user_id);
                    })->orderby('created_at', 'desc')->first();
                if (!isset($getuser)) {
                    $getuser = ProjectChat::where('project_id', $project->id)->where('archive_status', 1)
                        ->where(function ($q) use ($user_id) {
                            $q->Where('from_user_id', $user_id)->orWhere('to_user_id', $user_id);
                        })->orderby('created_at', 'desc')->first();
                }
                if (!empty($getuser->from_user_id) && $getuser->from_user_id != $user_id) {
                    $to_user_id = $getuser->from_user_id;
                    $freelancer = $getuser->sender;
                } else {
                    if (!empty($getuser->to_user_id)) {
                        $to_user_id = $getuser->to_user_id;
                        $freelancer = $getuser->recipient;
                    }
                }
            } else {
                $to_user_id = $freelancer->id;
            }




            $archive_highlights = ProjectChat::from('project_chats AS m')
                ->select('m.*')->with('sender', 'recipient')
                ->join(DB::raw('(select least(from_user_id, to_user_id) as user_1,greatest(from_user_id, to_user_id) as user_2,max(ID) as last_id,max(created_at) as last_timestamp from project_chats where project_id=' . $project->id . ' and archive_status=1
           
                group by
            least(from_user_id, to_user_id),
            greatest(from_user_id, to_user_id) )  as s'), function ($q) {
                    $q->on('m.id', '=', 's.last_id');
                })->orderby('created_at', 'desc')
                ->where(function ($q) use ($user_id) {
                    $q->Where('m.to_user_id', $user_id)->orWhere('m.from_user_id', $user_id);
                })->get();
            if (auth()->user()->user_type == 0) {
                $where = 'where project_id=' . $project->id . ' and archive_status=0';
            } else {
                $where = 'where project_id=' . $project->id;
            }

            $non_archive_highlights = ProjectChat::from('project_chats AS m')
                ->select('m.*')->with('sender', 'recipient')
                ->join(DB::raw('(select least(from_user_id, to_user_id) as user_1,greatest(from_user_id, to_user_id) as user_2,max(ID) as last_id,max(created_at) as last_timestamp from project_chats ' . $where . '
           
                group by
            least(from_user_id, to_user_id),
            greatest(from_user_id, to_user_id) )  as s'), function ($q) {
                    $q->on('m.id', '=', 's.last_id');
                })->orderby('created_at', 'desc')
                ->where(function ($q) use ($user_id) {
                    $q->Where('m.to_user_id', $user_id)->orWhere('m.from_user_id', $user_id);
                })->get();

            // dd($non_archive_highlights); 




            //DB::enableQueryLog();
            if (isset($to_user_id) && !empty($to_user_id)) {
                $chats = ProjectChat::with('sender', 'recipient')
                    ->where('project_id', $project->id)
                    ->where(function ($q) use ($user_id, $to_user_id) {
                        $q->where(function ($q) use ($user_id, $to_user_id) {
                            $q->where('from_user_id', '=', $user_id)
                                ->where('to_user_id', '=', $to_user_id);
                        })->orWhere(function ($q) use ($user_id, $to_user_id) {
                            $q->where('from_user_id', '=', $to_user_id)
                                ->where('to_user_id', '=', $user_id);
                        });
                    })->get();
                $to_count = $chats->count();
            } else {
                $chats = [];
                $to_count = 0;
            }

            return view('admin.projects.chats', compact('to_count', 'freelancer','user', 'non_archive_highlights', 'chats', 'project', 'archive_highlights','user_id'));
        
    }

    /**
     * Function for show proposal
     */
    public function proposalShow(Project $project, ProjectProposal $proposal){
        return view('admin.projects.proposal_show',compact('project','proposal'));
    }

     /**
     * For project hide from public status
     */
    // public function hidePublic(Request $request){
    //     Project::where('id',$request->user_ids)->update([
    //         'approved' => $request->status,
    //         'approved_at' => date('Y-m-d H:i:s')
    //     ]);
    // }
}
