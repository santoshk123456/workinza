<?php

namespace App\Http\Controllers\Admin;

use App\Transaction;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\DataTables\TransactionDataTable;

class TransactionController extends Controller
{
     /**
     * To list transaction table
     * */
    public function index(Builder $builder, TransactionDataTable $dataTable)
    {
        return $dataTable->render('admin.transactions.index');
    }

    /**
     * show transaction
     */

    public function show(Transaction $transaction){
        return view('admin.transactions.show',compact('transaction'));
    }

    /**
     * update transaction
     */

      /**
     * To update industry
     */

    public function update(Request $request,Transaction $transaction)
    {
        $request_data = [];
        $request_data['payment_status'] = $request->payment_status;
        if($transaction->update($request_data))
        return redirect()->route('admin.transactions.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the status.')]);
        else
        return redirect()->route('admin.transactions.index')->with('toastr', ['type'=>'error','text'=>__('Status can not be updated.')]);
    }
}
