<?php

namespace App\Http\Controllers\Admin;

use App\Skill;
use App\Category;
use App\Industry;
use App\SubCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\DataTables\IndustryDataTable;
use App\Http\Requests\Admin\IndustryRequest;

class IndustryController extends Controller
{
    
    /**
     * To list industry table
     * */
    public function index(Builder $builder, IndustryDataTable $dataTable)
    {
        return $dataTable->render('admin.industries.index');
    }

    /***
     * To store industry 
     */

    public function store(IndustryRequest $request)
    {
        $request_data = [];
        $request_data['name'] = $request->industry_name;
        if(Industry::create($request_data))
        return redirect()->route('admin.industries.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new industry.')]);
        else
        return redirect()->route('admin.industries.index')->with('toastr', ['type'=>'error','text'=>__('industry can not be created.')]);
    }

    /**
     * To update industry
     */

    public function update(IndustryRequest $request,Industry $industry)
    {
        $request_data = [];
        $request_data['name'] = $request->industry_name;
        if($industry->update($request_data))
        return redirect()->route('admin.industries.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the new industry.')]);
        else
        return redirect()->route('admin.industries.index')->with('toastr', ['type'=>'error','text'=>__('industry can not be updated.')]);
    }

    /**
     * To delete industry
     */

    public function destroy(Industry $industry)
    {
        $industry->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Industry has been deleted successfully.'),]);
    }

    /**
     * to change industry status
     */

    public function toggleActiveStatus($id)
    {
        $industry = Industry::where('uuid',$id)->first();
        if(!empty($industry->active)){
            $industry->active = 0;
        } else {
            $industry->active = 1;
        }
        
        if($industry->update()){
            return redirect()->route('admin.industries.index')->with('toastr', ['type'=>'success','text'=>'Industry status has been updated successfully']);
        }else{
            return redirect()->route('admin.industries.index')->with('toastr', ['type'=>'error','text'=>'Industry status can not be updated ']);
        }
    }

    public function getCategories(Request $request){
        if(!empty($request->industry_ids)){
            $categories = Category::whereHas('industries',function($q) use($request){
                $q->whereIn('id',$request->industry_ids);
            })->where(['active'=>1])->pluck('name','id');
        }else{
            $categories = [];
        }
        return response()->json($categories);
    }
    public function getSubCategories(Request $request){
        if(!empty($request->category_ids)){
            $sub_categories = SubCategory::whereHas('categories',function($q) use($request){
                $q->whereIn('id',$request->category_ids);
            })->where(['active'=>1])->pluck('name','id');
        }else{
            $sub_categories = [];
        }
        return response()->json($sub_categories);
    }
    public function getSkills(Request $request){
        if(!empty($request->sub_category_ids)){
            $skills = Skill::whereHas('subCategories',function($q) use($request){
                $q->whereIn('id',$request->sub_category_ids);
            })->where(['active'=>1])->pluck('name','id');
        }else{
            $skills = Skill::where(['active'=>1])->pluck('name','id');
           
        }
        return response()->json($skills);
    }
}
