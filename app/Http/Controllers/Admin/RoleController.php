<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use DataTables;
use App\Permission;
use App\Models\Auth\Admin;
use Illuminate\Http\Request;
use App\DataTables\RoleDataTable;
use Yajra\DataTables\Html\Builder;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RoleRequest;



class RoleController extends Controller
{
    /**
     * @param ManageRoleRequest $request
     *
     * @return mixed
     */

    /**
     * For Displaying the Roles Listing page
     */
    public function index($role = null,Builder $builder, RoleDataTable $dataTable)
    {
        return $dataTable->render('admin.roles.index');
    }
    /**
     * For displaying the edit form for role
     */
    public function edit(Role $role)
    {
        // Splitting the name field inorder to get the module name
        $permissions = Permission::selectRaw("SUBSTRING(name, LOCATE(' ', name)+1) as module_name,name")->where("guard_name","admin")->get()->groupBy(['module_name']);
        return view('admin/roles/edit', compact('role','permissions'));
    }
    /**
     * For storing the details submitted in edit form
     */
    public function update(RoleRequest $request,Role $role)
    {
        // Request data
        $request_data = [];
        $request_data['name'] = $request->name;
        // Update admin
        $role->update($request_data);
        $permissions = [];
        if(!empty($request->roles)){
            $permissions = array_keys($request->roles);
        } 
        // Assign Admin role
        $role->syncPermissions($permissions);
        return redirect()->route('admin.roles.index')->with('toastr', ['type'=>'success','text'=>__('Permissions of the role have been updated successfully.')]);
    }
    /**
     * For displaying the add form for role
     */
    public function create()
    {
        // Splitting the name field inorder to get the module name
        $permissions = Permission::selectRaw("SUBSTRING(name, LOCATE(' ', name)+1) as module_name,name")->where("guard_name","admin")->get()->groupBy(['module_name']);
        return view('admin/roles/create',compact('permissions'));
    }
    /**
     * For storing the details submitted in add form
     */
    public function store(RoleRequest $request)
    {
        // Request data
        $request_data = [];
        $request_data['name'] = $request->name;
        $request_data['guard_name'] = "admin";
        // Update admin
        $role = Role::create($request_data);
        $permissions = [];
        if(!empty($request->roles)){
            $permissions = array_keys($request->roles);
        } 
        // Assign Admin role
        $role->syncPermissions($permissions);
        return redirect()->route('admin.roles.index')->with('toastr', ['type'=>'success','text'=>__('New role has been created successfully.')]);
    }
    /**
     * Undocumented function
     *
     * @param Role $role
     * @return void
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Role has been deleted successfully.'),]);
    }
    /**
     * For displaying the detail page including the permissions of Role
     */
    public function show(Role $role)
    {
        $permissions = Permission::selectRaw("SUBSTRING(name, LOCATE(' ', name)+1) as module_name,name")->where("guard_name","admin")->get()->groupBy(['module_name']);
        return view('admin/roles/show', compact('role','permissions'));
    }
}
