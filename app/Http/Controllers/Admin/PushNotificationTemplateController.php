<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use App\Models\Auth\Admin;
use Illuminate\Http\Request;
use App\PushNotificationTemplate;
use Yajra\DataTables\Html\Builder;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\DataTables\PushNotificationTemplateDataTable;
use App\Http\Requests\Admin\PushNotificationTemplateRequest;



class PushNotificationTemplateController extends Controller
{

    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index(Builder $builder, PushNotificationTemplateDataTable $dataTable)
    {
        return $dataTable->render('admin.push_notification_templates.index');
    }

    /**
     * For displaying the edit form for Push Notification Template
     *
     * @param PushNotificationTemplate $pushNotificationTemplate
     * @return void
     */
    public function edit(PushNotificationTemplate $pushNotificationTemplate)
    {
        return view('admin.push_notification_templates.edit', compact('pushNotificationTemplate'));
    }
 
    /**
     * For storing the details submitted in edit form
     *
     * @param PushNotificationTemplateRequest $request
     * @param PushNotificationTemplate $pushNotificationTemplate
     * @return void
     */
    public function update(PushNotificationTemplateRequest $request,PushNotificationTemplate $pushNotificationTemplate)
    {
        // Request data
        $request_data = [];
        $request_data['process_name'] = $request->process_name;
        $request_data['description'] = $request->description;
        $request_data['template'] = $request->template;
        //dd($request_data);
        $pushNotificationTemplate->update($request_data); 
        // Update Push Notification Template
        return redirect()->route('admin.push_notification_templates.index')->with('toastr', ['type'=>'success','text'=>__('Push Notification Template have been updated successfully.')]);
    }

    /**
     * For displaying the Create form for Push Notification Template
     *
     * @return void
     */
    public function create()
    {
        return view('admin.push_notification_templates.create');
    }

    /**
     * For storing the details submitted in create form
     *
     * @param PushNotificationTemplateRequest $request
     * @return void
     */
    public function store(PushNotificationTemplateRequest $request)
    {
        // Request data
        $request_data = [];
        $request_data['process_name'] = $request->process_name;
        $request_data['description'] = $request->description;
        $request_data['template'] = $request->template;
        // Create new Push Notification question
        $pushNotificationTemplate = PushNotificationTemplate::create($request_data);
        if ($pushNotificationTemplate) {
            return redirect()->route('admin.push_notification_templates.index')->with('toastr', ['type'=>'success','text'=>'Push Notification template created successfully.',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }

    /**
     * For deleting Push Notification template
     *
     * @param PushNotificationTemplate $pushNotificationTemplate
     * @return void
     */
    public function destroy(PushNotificationTemplate $pushNotificationTemplate)
    {
        $pushNotificationTemplate->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Push Notification Template has been deleted successfully.'),]);
    }


    /**
     * For displaying the detail page of Push Notification Template
     *
     * @param PushNotificationTemplate $pushNotificationTemplate
     * @return void
     */
    public function show(PushNotificationTemplate $pushNotificationTemplate)
    {
        return view('admin.push_notification_templates.show', compact('pushNotificationTemplate'));
    }
}
