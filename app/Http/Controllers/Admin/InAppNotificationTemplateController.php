<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use App\Models\Auth\Admin;
use Illuminate\Http\Request;
use App\InAppNotificationTemplate;
use Yajra\DataTables\Html\Builder;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\DataTables\InAppNotificationTemplateDataTable;
use App\Http\Requests\Admin\InAppNotificationTemplateRequest ;



class InAppNotificationTemplateController extends Controller
{

    /**
     * Index function
     *
     * @param Builder $builder
     * @return void   
     */
    public function index(Builder $builder, InAppNotificationTemplateDataTable $dataTable)
    {
        return $dataTable->render('admin.in_app_notification_templates.index');
    }

    /**
     * For displaying the edit form for In App Notification Template
     *
     * @param InAppNotificationTemplate $inAppnotificationTemplate
     * @return void
     */
    public function edit(InAppNotificationTemplate $inAppNotificationTemplate)
    {
        return view('admin.in_app_notification_templates.edit', compact('inAppNotificationTemplate'));
    }
 
    /**
     * For storing the details submitted in edit form
     *
     * @param InAppNotificationTemplate $request
     * @param InAppNotificationTemplate $inAppNotificationTemplate
     * @return void
     */
    public function update(InAppNotificationTemplateRequest $request,InAppNotificationTemplate $inAppNotificationTemplate)
    {
        // Request data
        $request_data = [];
        $request_data['process_name'] = $request->process_name;
        $request_data['description'] = $request->description;
        $request_data['template'] = $request->template;
        //dd($request_data);
        $inAppNotificationTemplate->update($request_data); 
        // Update In App Notification
        return redirect()->route('admin.in_app_notification_templates.index')->with('toastr', ['type'=>'success','text'=>__('In-App Notification Template have been updated successfully.')]);
    }

    /**
     * For displaying the Create form for In App Notification Template
     *
     * @return void
     */
    public function create()
    {
        return view('admin.in_app_notification_templates.create');
    }

    /**
     * For storing the details submitted in create form
     *
     * @param InAppNotificationTemplate $request
     * @return void
     */
    public function store(InAppNotificationTemplateRequest $request)
    {
        // Request data
        $request_data = [];
        $request_data['process_name'] = $request->process_name;
        $request_data['description'] = $request->description;
        $request_data['template'] = $request->template;
        // Create new In App Notification Template
        $inAppNotificationTemplate = InAppNotificationTemplate::create($request_data);
        if ($inAppNotificationTemplate) {
            return redirect()->route('admin.in_app_notification_templates.index')->with('toastr', ['type'=>'success','text'=>'In-App Notification template created successfully.',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }

    /**
     * For deleting In-App Notification template
     *
     * @param InAppNotificationTemplate $inAppNotificationTemplate
     * @return void
     */
    public function destroy(InAppNotificationTemplate $inAppNotificationTemplate)
    {
        $inAppNotificationTemplate->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('In-App Notification Template has been deleted successfully.'),]);
    }


    /**
     * For displaying the detail page of In-App Notification Template
     *
     * @param InAppNotificationTemplate $inAppNotificationTemplate
     * @return void
     */
    public function show(InAppNotificationTemplate $inAppNotificationTemplate)
    {
        return view('admin.in_app_notification_templates.show', compact('inAppNotificationTemplate'));
    }
}
