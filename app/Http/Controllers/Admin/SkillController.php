<?php

namespace App\Http\Controllers\Admin;

use App\Skill;
use App\Category;
use App\Industry;
use App\SubCategory;
use Illuminate\Http\Request;
use App\DataTables\SkillDataTable;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SkillRequest;

class SkillController extends Controller
{
    /**
     * To list skills table
     * */
    public function index(Builder $builder, SkillDataTable $dataTable)
    {
        return $dataTable->render('admin.skills.index');
    }

     /**
     * To create skills
     */

    public function create()
    {
        $categories_list = Category::select('id','name')->where('active',1)->get();
        $subcategories_list = SubCategory::select('id','name')->where('active',1)->get();
       
        return view('admin.skills.create',compact('categories_list','subcategories_list'));
    }

     /***
     * To store Skills 
     */

    public function store(SkillRequest $request)
    {
        $request_data = [];
        $request_data['name'] = $request->skill_name;
        $create_skill = Skill::create($request_data);
        $create_skill->categories()->attach($request->categories_ids);
        $create_skill->industries()->attach($request->industry_id);
        $create_skill->subcategories()->attach($request->sub_categories_ids);
        if($create_skill)
        return redirect()->route('admin.skills.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new skill.')]);
        else
        return redirect()->route('admin.skills.index')->with('toastr', ['type'=>'error','text'=>__('skill can not be created.')]);
    }

     /**
     * To show Skills
     */

    public function show(Skill $skill)
    {
        return view('admin.skills.show', compact('skill'));
    }

    /**
     * To edit Skills
     */

    public function edit(Skill $skill)
    {
       
        $categories_list = Category::select('id','name')->where('active',1)->get();
        $current_categories_ids = $categories_list->pluck('id')->toArray();
        $subcategories_list = SubCategory::select('id','name')->whereHas('categories', function ($k) use($current_categories_ids){
            $k->whereIn('id',$current_categories_ids);
        })->get();
        return view('admin.skills.edit',compact('categories_list','subcategories_list','skill'));
    }

    /**
     * To update Skill
     */

    public function update(SkillRequest $request,Skill $skill)
    {
        $request_data = [];
        $request_data['name'] = $request->skill_name;
        $skill->categories()->detach();
        $skill->industries()->detach();
        $skill->subcategories()->detach();
        $skill->categories()->attach($request->category_id);
        $skill->industries()->attach($request->industry_id);
        $skill->subcategories()->attach($request->subcategory_id);
        if($request->active =="on"){
            $request_data['active'] = 1;
        }else{
            $request_data['active'] = 0;
        }
        if($skill->update($request_data))
        return redirect()->route('admin.skills.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the skill.')]);
        else
        return redirect()->route('admin.skills.index')->with('toastr', ['type'=>'error','text'=>__('Skill can not be updated.')]);
    }

     /**
     * To delete sub Categories
     */

    public function destroy(Skill $skill)
    {
        $skill->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('skill has been deleted successfully.'),]);
    }

     /**
     * To change skill status
     */

    public function toggleActiveStatus($id)
    {
        $skill_data = Skill::where('uuid',$id)->first();
        if(!empty($skill_data->active)){
            $skill_data->active = 0;
        } else {
            $skill_data->active = 1;
        }
        
        if($skill_data->update()){
            return redirect()->route('admin.skills.index')->with('toastr', ['type'=>'success','text'=>'Skill status has been updated successfully']);
        }else{
            return redirect()->route('admin.skills.index')->with('toastr', ['type'=>'error','text'=>'Skill status can not be updated ']);
        }
    }

}
