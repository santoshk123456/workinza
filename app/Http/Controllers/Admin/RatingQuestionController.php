<?php

namespace App\Http\Controllers\Admin;

use App\RatingQuestion;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\DataTables\RatingQuestionDataTable;
use App\Http\Requests\Admin\RatingQuestionRequest;

class RatingQuestionController extends Controller
{
      
  /**
     * To list security questions
     * */
    public function index(Builder $builder, RatingQuestionDataTable $dataTable)
    {
        return $dataTable->render('admin.rating_questions.index');
    }

     /***
     * To store rating questions 
     */

    public function store(RatingQuestionRequest $request)
    {
        $request_data = [];
        $request_data['user_type'] = $request->user_type;
        $request_data['question'] = $request->question;
        if(RatingQuestion::create($request_data))
        return redirect()->route('admin.rating_questions.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new Question.')]);
        else
        return redirect()->route('admin.rating_questions.index')->with('toastr', ['type'=>'error','text'=>__('Question can not be created.')]);
    }

    /**
     * To update rating questions
     */

    public function update(RatingQuestionRequest $request,RatingQuestion $ratingQuestion)
    {
        $request_data = [];
        $request_data['user_type'] = $request->user_type_edit;
        $request_data['question'] = $request->question;
        if($ratingQuestion->update($request_data))
        return redirect()->route('admin.rating_questions.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the Question.')]);
        else
        return redirect()->route('admin.rating_questions.index')->with('toastr', ['type'=>'error','text'=>__('Question can not be updated.')]);
    }

     /**
     * To delete rating questions
     */

    public function destroy(RatingQuestion $ratingQuestion)
    {
        $ratingQuestion->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Question has been deleted successfully.'),]);
    }

}
