<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Setting;
use App\ContactUs;
use App\Models\Auth\Admin;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;

use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ContactUsRequest;

class ContactUsController extends Controller
{
    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index()
    {
        $contact_us = ContactUs::first();
        $data['api_key'] = Setting::where('key', 'google_api_key')->first()->value;
        return view('admin/contact_us/index',\compact('contact_us'),['data'=>$data]);
    }
    public function update(Request $request,ContactUs $contact_us)
    {
    
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' =>'required',
            'contact_email' =>'required|max:50|email',
            'contact_number' =>'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        else{
            $request_data = [];
            $request_data['name'] = $request->name;
            $request_data['address'] = $request->address;
            $request_data['latitude'] = $request->latitude;
            $request_data['longitude'] = $request->longitude;
            $request_data['contact_email'] = $request->contact_email;
            $request_data['contact_number'] = $request->contact_number;
            
            // Update contact us
            $contact_us->update($request_data);
            return redirect()->route('admin.contact_us.index')->with('toastr', ['type'=>'success','text'=>__('Contact us form have been updated successfully.')]);
        }
    }
}
