<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\GeneralException;
use App\Services\Mail\Mailtrap;
use App\EmailQueue;
use App\DataTables\EmailQueueDataTable;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Facades\Mail;
use App\Mail\GenerateEmail;


class EmailQueueController extends Controller
{
    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index(Builder $builder, EmailQueueDataTable $dataTable)
    {
        return $dataTable->render('admin.email_queues.index');
    }

    /**
     * View Function of email Queues
     *
     * @param [type] $id
     * @return void
     */
    public function show(EmailQueue $emailQueue)
    {
        return view('admin/email_queues/show',compact('emailQueue'));
    }
    public function resend(EmailQueue $emailQueue){
        try {
            Mail::to($emailQueue->to_email)->send(new GenerateEmail($emailQueue));
            $emailQueue->update([
                'sent_status'  => 1,
                'time_to_send' => date('Y-m-d H:i:s')
            ]);
            return redirect()->route('admin.email_queues.index')->with('toastr', ['type'=>'success','text'=>'Email was successfully sent.',]);
        } catch(Exception $e){
            if(count(Mail::failures()) == 0){
                $emailQueue->update([
                    'sent_status'  => 1,
                    'time_to_send' => date('Y-m-d H:i:s')
                ]);
                return redirect()->route('admin.email_queues.index')->withFlashSuccess("Email send successfully!");
            } else {
                throw new GeneralException(__('toastr.access_denied', ['attribute'=>"Some error occured.please try again !"]));
                return redirect()->route('admin.email_queues.index');
            }
        }
    }
}
