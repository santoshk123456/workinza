<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Vault;
use App\Invoice;
use App\Project;
use App\Transaction;
use App\ProjectContract;
use App\ProjectMilestone;
use App\UserSubscription;
use App\UserWalletHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Builder;
use App\DataTables\InvoiceDataTable;
use App\Http\Controllers\Controller;
use App\DataTables\ProjectPaymentDataTable;

class ProjectPaymentController extends Controller
{
    /**
     * To list project payment
     * */
    public function index(Builder $builder, ProjectPaymentDataTable $dataTable)
    {
        $request_data['project_contract_id'] = 0;
        $request_data["user_subscription_id"] = 0;
        $request_data["type"] = 0;
        $request_data["	amount"] = 0;
        $request_data["notes"] = 0;
        $request_data["status"] = 0;
        $request_data['paid_on'] = 0;
        $request_data['due_date'] = 0;
        foreach ($request_data as $key => $value) {
            $more_col[$key]=$key;
         }
        $milestones = ProjectMilestone::pluck('milestone_name','id')->toArray();
        return $dataTable->render('admin.project_payments.index',compact('milestones','more_col'));
    }
    /**
     * To export invoice list
     * */
    function exportlist(Invoice $model,Request $request){
        
        $sel[]=DB::raw('project_milestones.milestone_name as milestone_name');
        $sel[]=DB::raw('users.full_name as user_name');
        $sel[]=DB::raw('invoices.*');
        $sel[]=DB::raw('CONCAT("CR",invoices.project_contract_id) as project_contract_id');
        $sel[]=DB::raw('CASE WHEN invoices.status=0 THEN "Pending" WHEN invoices.status=1 THEN "Paid" ELSE "Rejected" END as status');  
        $sel[]=DB::raw('CASE WHEN invoices.type=0 THEN "Arbitation" WHEN invoices.type=1 THEN "Project payment" WHEN invoices.type=2 THEN "Cancellation" WHEN invoices.type=3 THEN "Refund" WHEN invoices.type=4 THEN "Bonus" ELSE "Subscription payment" END as type');
        
        $model=$model->query()->with(["user"])->with(["milestones"])->select($sel);
        $model=$model->leftjoin("users","users.id","=","invoices.user_id");
        $model=$model->leftjoin("project_milestones","project_milestones.id","=","invoices.project_milestone_id");
        $model->where("invoices.type",  1);
        //filters
        $condition = [];
        if(request('milestone') != ''){
            $model->where('invoices.project_milestone_id', request('milestone'));
        }
        if(request('daterange') != ''){
            $range = explode('-',request('daterange'));
            $from_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[0]))));
            $to_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[1]))));
            if ($from_date != '') {
                $condition[] = ['invoices.created_at',">=",$from_date];
            } 
            if ($to_date != '') {
                $condition[] = ['invoices.created_at',"<=",$to_date];
            } 
        }
        $model->where($condition);
        $data=  $model->get();
        return response()->json($data);
    }

    /**
     * show project payment
     */

    public function show(Invoice $invoice){
        return view('admin.project_payments.show',compact('invoice'));
    }

    /**
     * To list invoices
     * */
    public function invoiceIndex(Builder $builder, InvoiceDataTable $dataTable)
    {
        return $dataTable->render('admin.invoices.index');
    }
    /**
     * To create invoices
     * */
    public function create(Request $request){
        $users = User::get();
        return view('admin.invoices.create',compact('users'));
    }

    /**
     * To select milestones for the selected user
     * */
    public function select_milestones(Request $request){
        $user = User::where('id',$request->user)->first();
        $milestones = ProjectMilestone::where('project_id',$request->project)->pluck("milestone_name","id")->toArray();
        return response()->json($milestones);
    }
    /**
     * To select milestones for the selected user
     * */
    public function select_project(Request $request){
        $user = User::where('id',$request->user)->first();
        $projects = Project::where('user_id',$user->id)->pluck('title','id')->toArray();
        //$milestones = ProjectMilestone::whereIn('project_id',$projects)->pluck("milestone_name","id")->toArray();
        return response()->json($projects);
    }
    /**
     * To store invoice
     * */
    public function store(Request $request){
        $input_data = [];
        $messages = [
            'user_id.required' => 'Please choose a user.',
            'type.required' => 'Type cannot be empty.',
            'amount.required' => 'Amount cannot be empty.',
        ];
        $rules = [
            'user_id'=>'required',
            'type'=>'required',
            'amount'=>'required',
            ]; 
        $request->validate($rules,$messages);
        $input_data['user_id'] = $request->user_id;
        $input_data['type'] = $request->type;
        $input_data['amount'] = $request->amount;
        // $input_data['notes'] = $request->note;
        $input_data['status'] = 0;
        $now_date = \Carbon\Carbon::now();
        $input_data['paid_on'] = $now_date->toDateTimeString();
        // if invoice type is project payment 
        if($request->type == 1){
            $input_data['project_milestone_id'] = $request->project_milestone_id;
            $milestones = ProjectMilestone::where('id',$request->project_milestone_id)->first();
            $input_data['project_contract_id'] = $milestones->project->contracts->id;
            $input_data['due_date'] =  $date = \Carbon\Carbon::today()->subDays(7);
        }
        // if invoice type is subscription payment 
        elseif($request->type == 5){
            $subscription = UserSubscription::where('user_id', $request->user_id)->first();
            $input_data['user_subscription_id'] = $subscription->id;
            $input_data['due_date'] =  $date = \Carbon\Carbon::today()->subDays(7);
        }
        //if invoice type is refund (dispute case)
        else if($request->type == 3){
          
            //check the status of milestone
            $milestone_details = ProjectMilestone::where('id',$request->project_milestone_id)->first();
            $contract_details = ProjectContract::where('project_id',$milestone_details->project_id)->with('user')->first(); 
            $pay_amount =   $milestone_details->milestone_amount - $request->amount;
            $input_data['project_milestone_id'] = $milestone_details->id;
            $input_data['project_contract_id'] = $contract_details->id;
            //if milestone status is approve, then we need to deduct refund amount from freelancer wallet and create a transaction to the client with credit remining balance
            if(isset($milestone_details->status) && ($milestone_details->status == 4 || $milestone_details->status == 5)){
                //debit entry for freelancer in wallet
                //update dispute flag
                $dispute_cond = [];
                $dispute_cond['user_id'] = $contract_details->freelancer_id;
                $dispute_cond['transaction_type'] = 0;
                $update_user_wallet = UserWalletHistory::where($dispute_cond)->whereHas('transactions', function($q) use($milestone_details){
                    $q->where('project_milestone_id', $milestone_details->id);
                })->update(['is_dispute'=>1]);
                $user_wallet_history = UserWalletHistory::where($dispute_cond)->whereHas('transactions', function($q) use($milestone_details){
                    $q->where('project_milestone_id', $milestone_details->id);
                })->first();
                
                $wallet_sts = UserWalletHistory::create([
                    'user_id' => $contract_details->freelancer_id,
                    'transaction_type' => 1,
                    'amount' => $milestone_details->milestone_amount,
                    'new_balance' => $contract_details->user->wallet_balance - $milestone_details->milestone_amount,
                    'notes' => "Debit amount (Dispute case) from your wallet for the milestone '". ucwords($milestone_details->milestone_name)."' of the project '".ucwords($milestone_details->project->title)."'",
                    'type' =>6, //refund
                    'payment_status'=>1,
                    'is_dispute' => 1
                    
                ]);
                //update wallet balance
                $user_update = User::where('id',$contract_details->freelancer_id)->decrement('wallet_balance', $milestone_details->milestone_amount);//update(['wallet_balance'=> $contract_details->user->wallet_balance - $milestone_details->milestone_amount]);
                //get wallet balance
                $wallet_balance = UserWalletHistory::where('id',$wallet_sts->id)->first();
                $wallet_sts = UserWalletHistory::create([
                    'user_id' => $contract_details->freelancer_id,
                    'transaction_id'=>$user_wallet_history->transaction_id,
                    'transaction_type' => 0,
                    'amount' => $request->amount,
                    'new_balance' => $wallet_balance->new_balance + $request->amount,
                    'notes' => "Credit amount (Dispute case) to your wallet for the milestone '". ucwords($milestone_details->milestone_name)."' of the project '".ucwords($milestone_details->project->title)."'",
                    'type' =>6, //refund
                    'payment_status'=>0,
                    'is_dispute' => 0
                    
                ]);

                //entry to client transaction table
                $transaction_sts = Transaction::create([
                    'user_id' => $request->user_id,
                    'type' => 7,
                    'amount' => $milestone_details->milestone_amount - $request->amount,
                    'project_milestone_id'=> $milestone_details->id,
                    'project_contract_id'=>$contract_details->id,
                    'project_id'=>$milestone_details->project_id,
                    'total_amount' => $milestone_details->milestone_amount - $request->amount,
                    'transaction_type' => 0,
                    'notes' => "Refund (Dipsute case) for the milestone '". ucwords($milestone_details->milestone_name)."' of the project '".ucwords($milestone_details->project->title)."'",
                    'payment_status' => 1,
                    'payment_response' => 'Paid',
                   
                ]);
                //update wallet balance
                $user_update = User::where('id',$contract_details->freelancer_id)->increment('wallet_balance', $request->amount);//update(['wallet_balance'=> $contract_details->user->wallet_balance + $request->amount]);
                //invoice generation
                $input_data['amount'] = $milestone_details->milestone_amount - $request->amount;
                $input_data['status'] = 1;
                $input_data['notes'] = "Credit your milestone amount (Dispute case) for the milestone '". ucwords($milestone_details->milestone_name)."' of the project '".ucwords($milestone_details->project->title)."'";
            }else if(isset($milestone_details->status) && ($milestone_details->status == 1 || $milestone_details->status == 2 || $milestone_details->status == 3)){
                $update_vault = [];
                $update_vault['project_milestone_id'] = $milestone_details->id;
                Vault::where($update_vault)->update(['vault_status'=>1]);
                //debit current vault amount for the milestone both freelancer and client
                $vault_sts = Vault::create([
                    'user_id' =>  $request->user_id,
                    'user_type' => 0,
                    'project_milestone_id' => $milestone_details->id,
                    'transaction_type' => 1,
                    'amount' => $milestone_details->milestone_amount,
                    'notes' => "Debit amount (Dispute case) from your vault for the milestone '". ucwords($milestone_details->milestone_name)."' of the project '".ucwords($milestone_details->project->title)."'",
                    'vault_status' =>1,
                    
                ]);
                $vault_sts = Vault::create([
                    'user_id' => $contract_details->freelancer_id,
                    'user_type' => 1,
                    'project_milestone_id' => $milestone_details->id,
                    'transaction_type' => 1,
                    'notes' => "Debit amount (Dispute case) from your vault for the milestone '". ucwords($milestone_details->milestone_name)."' of the project '".ucwords($milestone_details->project->title)."'",
                    'amount' => $milestone_details->milestone_amount,
                    'vault_status' =>1,
                    
                ]);

                //entry to client transaction table
                $transaction_sts = Transaction::create([
                    'user_id' => $request->user_id,
                    'type' => 7,
                    'amount' => $milestone_details->milestone_amount - $request->amount,
                    'project_milestone_id'=> $milestone_details->id,
                    'project_contract_id'=>$contract_details->id,
                    'project_id'=>$milestone_details->project_id,
                    'total_amount' => $milestone_details->milestone_amount - $request->amount,
                    'transaction_type' => 0,
                    'notes' => "Refund (Dipsute case) for the milestone '". ucwords($milestone_details->milestone_name)."' of the project '".ucwords($milestone_details->project->title)."'",
                    'payment_status' => 1,
                    'payment_response' => 'Paid',
                   
                ]);
                //create entry both freelancer and client with dispute case
              
                $vault_sts = Vault::create([
                    'user_id' =>  $request->user_id,
                    'user_type' => 0,
                    'project_milestone_id' => $milestone_details->id,
                    'transaction_id' => $transaction_sts->id,
                    'transaction_type' => 0,
                    'amount' => $request->amount,
                    'notes' => "Milestone amount credited to your vault (After dispute) for the milestone ". ucwords($milestone_details->milestone_name)." of the project ".ucwords($milestone_details->project->title),
                    'vault_status' =>0,
                    
                ]);
                $vault_sts = Vault::create([
                    'user_id' => $contract_details->freelancer_id,
                    'user_type' => 1,
                    'project_milestone_id' => $milestone_details->id,
                    'transaction_id' => $transaction_sts->id,
                    'transaction_type' => 0,
                    'notes' => "Client credited milestone amount to your vault (After dispute) for the milestone ". ucwords($milestone_details->milestone_name)." for the project ".ucwords($milestone_details->project->title),
                    'amount' => $request->amount,
                    'vault_status' =>0,
                    
                ]);
                $input_data['amount'] = $milestone_details->milestone_amount - $request->amount;
                $input_data['status'] = 1;
                $input_data['notes'] = "Credit your milestone amount (Dispute case) for the milestone '". ucwords($milestone_details->milestone_name)."' of the project '".ucwords($milestone_details->project->title)."'";
                //current vault status need to update

                
            }
           
        }
     
        Invoice::create($input_data);
        return redirect()->route('admin.invoices.index')->with('toastr', ['type'=>'success','text'=>'Invoice created successfully',]);
    }

    /**
     * Show invoice
     */
    public function showInvoice(Invoice $invoice){
        return view('admin.invoices.show',compact('invoice'));
    }
}
