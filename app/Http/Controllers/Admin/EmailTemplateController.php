<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use App\EmailTemplate;
use App\Models\Auth\Admin;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\DataTables\EmailTemplateDataTable;
use App\Http\Requests\Admin\EmailTemplateRequest;



class EmailTemplateController extends Controller
{

    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index(Builder $builder, EmailTemplateDataTable $dataTable)
    {
        return $dataTable->render('admin.email_templates.index');
    }

    /**
     * For displaying the edit form for Email Template
     *
     * @param EmailTemplate $emailTemplate
     * @return void
     */
    public function edit(EmailTemplate $emailTemplate)
    {
        return view('admin.email_templates.edit', compact('emailTemplate'));
    }
 
    /**
     * For storing the details submitted in edit form
     *
     * @param EmailTemplateRequest $request
     * @param EmailTemplate $emailTemplate
     * @return void
     */
    public function update(EmailTemplateRequest $request,EmailTemplate $emailTemplate)
    {
        // Request data
        $request_data = [];
        $request_data['static_email_heading'] = $request->static_email_heading;
        $request_data['subject'] = $request->subject;
        $request_data['description'] = $request->description;
        $request_data['template'] = $request->template; 
        //dd($request_data);
        $emailTemplate->update($request_data); 
        // Update Email
        return redirect()->route('admin.email_templates.index')->with('toastr', ['type'=>'success','text'=>__('Email Template have been updated successfully.')]);
    }

    /**
     * For displaying the Create form for Email Template
     *
     * @return void
     */
    public function create()
    {
        return view('admin/email_templates/create');
    }

    /**
     * For storing the details submitted in create form
     *
     * @param EmailTemplateRequest $request
     * @return void
     */
    public function store(EmailTemplateRequest $request)
    {

        // Request data
        $request_data = [];
        $request_data['static_email_heading'] = $request->static_email_heading;
        $request_data['subject'] = $request->subject;
        $request_data['description'] = $request->description;
        $request_data['template'] = $request->template;        
        // Create new Email Template
        $email = EmailTemplate::create($request_data);
        if ($email) {
            return redirect()->route('admin.email_templates.index')->with('toastr', ['type'=>'success','text'=>'Email template created successfully.',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }

    /**
     * For displaying the detail page of Email Template
     *
     * @param EmailTemplate $emailTemplate
     * @return void
     */
    public function show(EmailTemplate $emailTemplate)
    {
        return view('admin.email_templates.show', compact('emailTemplate'));
    }
}
