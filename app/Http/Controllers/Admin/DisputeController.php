<?php

namespace App\Http\Controllers\Admin;

use CSV;
use Excel;
use App\Admin;
use App\Dispute;
use App\Project;
use Carbon\Carbon;
use App\DisputeChat;
use App\ProjectChat;
use App\EmailTemplate;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\Exports\DisputeExport;
use App\Exports\UserLogExport;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Builder;
use App\Services\ProcessEmailQueue;
use App\DataTables\DisputeDataTable;
use App\Http\Controllers\Controller;

class DisputeController extends Controller
{
    /**
     * index: To list dispute table
     * */
    public function index(Builder $builder, DisputeDataTable $dataTable)
    {
        return $dataTable->render('admin.disputes.index');
    }

    /**
     * index: To get update dispute form
     * */
    public function edit(Request $request, Dispute $dispute){
        return view('admin.disputes.edit',compact('dispute'));
    }
    /**
     * update : To update the dispute status
     * inputs : request array
     * Response : If success redirect to dispute listing
     */
    public function update(Request $request, Dispute $dispute){
        $dispute->update([
            'status' => 1
        ]);
        $project = Project::where('id',$dispute->milestone->project->id)->first();
        $project->update([
            'status' => $request->status
        ]);
        return redirect()->route('admin.disputes.index')->with('toastr', ['type'=>'success','text'=>'Project updated successfully',]);
    }

     /**
     * index: To view dispute detail
     * */
    public function show(Request $request, Dispute $dispute){
        return view('admin.disputes.show',compact('dispute'));
    }

     /**
     * index: To view messages for the particular dispute
     * */
    public function message($dispute, $project){
        $user = Admin::where('id',1)->first();
        $dispute = Dispute::where('id',$dispute)->first();
        $project = Project::where('id',$project)->first();
        $client_id = $dispute->client->id;
        $freelancer_id = $dispute->freelancer->id;

        if (auth()->check()) {

            if (!isset($freelancer_id) && !isset($client_id)) {
                $getuser = DisputeChat::where('project_id', $project->id)
                    ->where(function ($q) use ($client_id, $user, $freelancer_id) {
                        $q->Where('from_user_id', $client_id)->orWhere('from_user_id', $user->id)->orWhere('from_user_id', $freelancer_id);
                    })->orderby('created_at', 'desc')->first();
                
                if (!empty($getuser->from_user_id) && $getuser->from_user_id != $user->id) {
                    $to_user_id = $getuser->from_user_id;
                    $freelancer = $getuser->sender;
                } else {
                    if (!empty($getuser->to_user_id)) {
                        $to_user_id = $getuser->to_user_id;
                        $freelancer = $getuser->recipient;
                    }
                }
            } else {
                $to_user_id = $freelancer_id;
            }
            $non_archive_highlights = [];

            if (isset($freelancer_id) && !empty($freelancer_id)) {
                $chats = DisputeChat::where('project_id', $project->id)
                    ->where('dispute_id', $dispute->id)
                    ->where(function ($q) use ($client_id, $user, $freelancer_id) {
                        $q->where(function ($q) use ($client_id) {
                            $q->where('from_user_id', '=', $client_id);
                        })->orWhere(function ($q) use ($user) {
                            $q->where('from_user_id', '=', $user->id);
                        })->orWhere(function ($q) use ($freelancer_id) {
                            $q->where('from_user_id', '=', $freelancer_id);
                        });
                    })->get();
                $to_count = $chats->count();
                $latest_msg = DisputeChat::where('project_id', $project->id)
                ->where('dispute_id', $dispute->id)->latest()->first();

                $last_msg = !empty($latest_msg)?$latest_msg->message:'No Messages Found';
            } else {
                $chats = [];
                $to_count = 0;
                $last_msg = 'No Messages Found';
            }
            return view('admin.disputes.message', compact('last_msg', 'to_count', 'non_archive_highlights', 'chats', 'project', 'user', 'dispute'));
        } 
    }

    /**
     * store_message : To store messages from admin for a particular dispute chat group
     * inputs : request array
     * Response : If success redirect to message detail page
     */
    public function store_message(Request $request)
    {
        $user = Admin::where('id',1)->first();
        if ($user) {
            //save proposal
            $project_id = $request->project_id;
            $user_id = $user->id;
            $message = [];
            $message['project_id'] = $project_id;
            $message['from_user_id'] = $user_id;
            $message['message'] = $request->message;
            $message['read_status'] = 0;
            $message['dispute_id'] = $request->dispute_id;
            $message['user_type'] = 'admin';
            $dispute = Dispute::where('id',$request->dispute_id)->first();

            if (isset($request->attachement)) {

                $path = public_path('storage/projects/comment_attachements');
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                $file = $request->file('attachement');

                $name = uniqid() . '_' . trim($file->getClientOriginalName());

                $file->move($path, $name);

                $message['attachment'] = $name;
            }

            $message_create = DisputeChat::create($message);

            $data = [];
            $email_template = EmailTemplate::where('static_email_heading', 'MESSAGE_SEND')->first();
            $data['action_type'] = 'message_send';
            $emails = [$dispute->client,$dispute->freelancer];
            if ($email_template) {
                foreach($emails as $email){
                    $email_html = $email_template->template;
                    $email_html = str_replace("[USER_NAME]", ucwords($email->username), $email_html);
                    $email_html = str_replace("[FROM_NAME]", ucwords($user->username), $email_html);
                    $email_html = str_replace("[MESSAGE]", $message_create->message, $email_html);
                    $email_html = str_replace("[PROJECT_NAME]", $message_create->project->title, $email_html);
                    $template = view('admin.email_templates.layout', compact('email_html'));
                    $data['to_email'] = $email->email;
                    $data['to_name'] = ucwords($email->username);
                    $data['subject'] = $email_template->subject;
                    $data['body'] = $template;
                    $data['action_id'] = $message_create->id;
                    $processEmailQueue = new ProcessEmailQueue($data);
                    $processEmailQueue->sendEmail();
                }
            }

            if ($message_create) {
                    return redirect()->route('admin.disputes.message', array($request->dispute_id, $request->project_id));
            }
        } else {
            return redirect()->route('home');
        }
    }

    /***
     * ExportDispute:Export table
     */

    public function ExportDispute(Request $request){
        
        if($request->has('daterange') && $request->daterange != ''){
            $range = explode('-',$request->daterange);
            $from_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[0]))));
            $to_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[1]))));
        }
        $log_array = [];
        $logs = Dispute::with(['client'=>function($q){
            $q->select('id','full_name');
        },'freelancer'=>function($k){
            $k->select('id','full_name');
        }])->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)->get();

        foreach($logs as $key=>$log)
        {
            $dispute_sts = '';
            if(empty($log->status))
            $dispute_sts = "Pending";
            else
            $dispute_sts = "Resolved";

            $log_array[] = array(
                '#' => $key +1,
                'Client'  => $log->client->full_name,
                'Freelancer'   => $log->freelancer->full_name,
                'Status'   => $dispute_sts,
                'Submitted On'   => $log->created_at
            );
        }

        if($request->exp_excel){
            switch($request->exp_excel) {
                case 'excel': 
                    return Excel::download(new DisputeExport($log_array), 'dispute.xlsx');
                break;
    
                case 'csv': 
                    return Excel::download(new DisputeExport($log_array), 'dispute.csv');
                break;
            }
        }

    }

}
