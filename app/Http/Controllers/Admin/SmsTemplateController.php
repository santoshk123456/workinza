<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use App\SmsTemplate;
use App\Models\Auth\Admin;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\DataTables\SmsTemplateDataTable;
use App\Http\Requests\Admin\SmsTemplateRequest;



class SmsTemplateController extends Controller
{

    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index(Builder $builder, SmsTemplateDataTable $dataTable)
    {
        return $dataTable->render('admin/sms_templates/index');
    }

    /**
     * For displaying the edit form for SMS Template
     *
     * @param SmsTemplate $smsTemplate
     * @return void
     */
    public function edit(SmsTemplate $smsTemplate)
    {
        return view('admin.sms_templates.edit', compact('smsTemplate'));
    }
 
    /**
     * For storing the details submitted in edit form
     *
     * @param SmsTemplateRequest $request
     * @param SmsTemplate $smsTemplate
     * @return void
     */
    public function update(SmsTemplateRequest $request,SmsTemplate $smsTemplate)
    {
        // Request data
        $request_data = [];
        $request_data['process_name'] = $request->process_name;
        $request_data['description'] = $request->description;
        $request_data['template'] = $request->template;
        //dd($request_data);
        $smsTemplate->update($request_data); 
        // Update SMS
        return redirect()->route('admin.sms_templates.index')->with('toastr', ['type'=>'success','text'=>__('SMS Template have been updated successfully.')]);
    }

    /**
     * For displaying the Create form for Sms Template
     *
     * @return void
     */
    public function create()
    {
        return view('admin/sms_templates/create');
    }

    /**
     * For storing the details submitted in create form
     *
     * @param SmsTemplateRequest $request
     * @return void
     */
    public function store(SmsTemplateRequest $request)
    {
        // Request data
        $request_data = [];
        $request_data['process_name'] = $request->process_name;
        $request_data['description'] = $request->description;
        $request_data['template'] = $request->template;
        // Create new SMS Template
        $sms = SmsTemplate::create($request_data);
        if ($sms) {
            return redirect()->route('admin.sms_templates.index')->with('toastr', ['type'=>'success','text'=>'Sms template created successfully.',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }
    /**
     * For deleting SMS template
     *
     * @param SmsTemplate $smsTemplate
     * @return void
     */
    public function destroy(SmsTemplate $smsTemplate)
    {
        $smsTemplate->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('SMS Template has been deleted successfully.'),]);
    }

    /**
     * For displaying the detail page of SMS Template
     *
     * @param SmsTemplate $smsTemplate
     * @return void
     */
    public function show(SmsTemplate $smsTemplate)
    {
        return view('admin.sms_templates.show', compact('smsTemplate'));
    }
}
