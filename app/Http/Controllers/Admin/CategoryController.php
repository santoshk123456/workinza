<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Industry;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\DataTables\CategoryDataTable;
use App\Http\Requests\Admin\CategoryRequest;

class CategoryController extends Controller
{
    /**
     * To list category table
     * */
    public function index(Builder $builder, CategoryDataTable $dataTable)
    {
        return $dataTable->render('admin.categories.index');
    }

    /**
     * To create categories
     */

    public function create()
    {
        $industries = Industry::select('id','name')->orderby('created_at','desc')->get();
        return view('admin.categories.create',compact('industries'));
    }

     /***
     * To store Categories 
     */

    public function store(CategoryRequest $request)
    {
      
        $request_data = [];
        $request_data['name'] = $request->category_name;
        
        if($request->hasFile('icon')){

            $icon = $request->file('icon')->store('public/admin/categories');
           
            $request_data['icon'] = str_replace('public/admin/categories','',$icon);
        }

        if($request->home_active == "on"){
        
            $request_data['home_active'] = 1;
        }else{
            $request_data['home_active'] = 0;
        }
     

        $create_category = Category::create($request_data);
        $create_category->industries()->attach($request->industry_ids);
        if($create_category)
        return redirect()->route('admin.categories.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new industry.')]);
        else
        return redirect()->route('admin.categories.index')->with('toastr', ['type'=>'error','text'=>__('industry can not be created.')]);
    }

     /**
     * To show Categories
     */

    public function show(Category $category)
    {
        return view('admin.categories.show', compact('category'));
    }


    /**
     * To edit Categories
     */

    public function edit(Category $category)
    {
        $industries_list = Industry::select('id','name')->get();
        return view('admin.categories.edit', compact('category','industries_list'));
    }

     /**
     * To update Categories
     */

    public function update(CategoryRequest $request,Category $category)
    {
        $request_data = [];
        $request_data['name'] = $request->category_name;
        if ($request->hasFile('icon')) {
            
            $icon = $request->file('icon')->store('public/admin/categories');
            $request_data['icon'] = str_replace('public/admin/categories', '', $icon);
        

        } elseif ($request->remove_title == 'removed') {
            $request_data['icon'] = "";
           // unlink(public_path() . '/storage/user/profile-images' . $user->icon);
        }

        if($request->active == "on"){
            $request_data['active'] = 1;
        }else{
            $request_data['active'] = 0;
        }

        if($request->home_active == "on"){
            $request_data['home_active'] = 1;
           
        }else{
            $request_data['home_active'] = 0;
        }

        $category->industries()->detach();
        $category->industries()->attach($request->industry_ids);
        if($category->update($request_data))
        return redirect()->route('admin.categories.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the new category.')]);
        else
        return redirect()->route('admin.categories.index')->with('toastr', ['type'=>'error','text'=>__('category can not be updated.')]);
    }

     /**
     * To delete  Categories
     */

    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Category has been deleted successfully.'),]);
    }

     /**
     * To change  Category status
     */

    public function toggleActiveStatus($id)
    {
        $cat = Category::where('uuid',$id)->first();
        if(!empty($cat->active)){
            $cat->active = 0;
        } else {
            $cat->active = 1;
        }
        
        if($cat->update()){
            return redirect()->route('admin.categories.index')->with('toastr', ['type'=>'success','text'=>'Category status has been updated successfully']);
        }else{
            return redirect()->route('admin.categories.index')->with('toastr', ['type'=>'error','text'=>'Category status can not be updated ']);
        }
    }


}
