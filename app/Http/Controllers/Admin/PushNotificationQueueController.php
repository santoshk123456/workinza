<?php

namespace App\Http\Controllers\Admin;

use DataTables;
use Illuminate\Http\Request;
use App\PushNotificationQueue;
use Yajra\DataTables\Html\Builder;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\DataTables\PushNotificationQueueDataTable;


class PushNotificationQueueController extends Controller
{
    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index(Builder $builder, PushNotificationQueueDataTable $dataTable)
    {
        return $dataTable->render('admin.push_notification_queues.index');
    }

    /**
     * View Function of email Queues
     *
     * @param [type] $id
     * @return void
     */
    public function show(PushNotificationQueue $pushNotificationQueue)
    {
        return view('admin/push_notification_queues/show',compact('pushNotificationQueue'));
    }
    /**
     * resend push notifications
     *
     * @param PushNotificationQueue $pushNotificationQueue
     * @return void
     */
    public function resend(PushNotificationQueue $pushNotificationQueue)
    {
       $send_status = '';
       if ($pushNotificationQueue->device_type == 'ios') {
           $send_status = PushNotificationQueue::pushNotificationIphone($pushNotificationQueue->id,$pushNotificationQueue->user_id,$pushNotificationQueue->device_id,$pushNotificationQueue->subject,$pushNotificationQueue->message,$pushNotificationQueue->action_type,$pushNotificationQueue->action_id,$pushNotificationQueue->sub_action_id,$count);
       }elseif (empty($pushNotificationQueue->device_type)) {
           $send_status = PushNotificationQueue::pushNotificationAndroid($pushNotificationQueue->id,$pushNotificationQueue->user_id,$pushNotificationQueue->device_id,$pushNotificationQueue->subject,$pushNotificationQueue->message,$pushNotificationQueue->action_type,$pushNotificationQueue->action_id,$pushNotificationQueue->sub_action_id,$count);
       }
       if($pushNotificationQueue->device_type == 'ios'){
           if ($send_status<=400) {
               $pushNotificationQueue->sent_status = 1;
           }
       }elseif(empty($pushNotificationQueue->device_type)){
           $sendstatus =json_decode($send_status);
           if(isset($sendstatus->success) && $sendstatus->success==1){
               $pushNotificationQueue->sent_status = 1;
           }
       }
       $pushNotificationQueue->save();
       return redirect()->back();

    }

}
