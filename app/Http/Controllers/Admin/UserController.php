<?php

namespace App\Http\Controllers\Admin;

use Hash;
use App\User;
use App\Skill;
use DataTables;
use App\Country;
use App\Invoice;
use App\Project;
use App\Category;
use App\Industry;
use Carbon\Carbon;
use App\UserDetail;
use App\UserRating;
use App\UserReview;
use App\SubCategory;
use App\Transaction;
use App\EmailTemplate;
use App\MembershipPlan;
use App\UserSubscription;
use App\Models\Auth\Admin;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\PushNotificationQueue;
use \Torann\GeoIP\Facades\GeoIP;
use App\DataTables\UserDataTable;
use App\PushNotificationTemplate;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Builder;
use App\Services\ProcessEmailQueue;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;
use App\Http\Requests\Admin\UserRequest;


/**
 * User for clients
 */
class UserController extends Controller
{

    public function index()
    {
    //    return $dataTable->render('admin.users.index');
       $users=User::where(['user_type'=>0,'approved'=>1])->get();
       return view('admin.users.index', compact('users'));
    }

    public function notApproved(){
        return view('admin.users.not_approved');
    }

    // export users lists
    public function exportuserslists(Request $request)
    {
        if(isset($request->approved)){
            $approved = 0;
        }else{
            $approved = 1;
        }
        if($request->status == "all"){

            $users=User::where(['user_type'=>0,'approved'=>$approved])->get();

        }else if($request->status == "active"){
            $users=User::where(['user_type'=>0,'approved'=>$approved])->orderBy('created_at','desc')->get();
        }else{
            
            $users=User::where(['user_type'=>0,'approved'=>$approved])->orderBy('created_at','desc')->get();
        
        }
        return $users;
    }



    //users lists
    public function userslists(Request $request)
    {
        if(isset($request->approved)){
            $approved = [0,2];
        }else{
            $approved = [1];
        }
        $users=User::where(['user_type'=>0])->whereIn('approved',$approved);
        /* if($request->status == "all"){
            $users=$users;
        }else if($request->status == "active"){
            $users=$users->where('active',1);
        }else{
            dd($request->status);
            $users=$users->where('active',0);
        } */
        
        if($request->store_id!=""){
            $users = $users->where([['email','like', '%'.$request->store_id.'%']]); 
        }

        if($request->name_search!=""){
            $users = $users->where([['full_name', 'like', '%'.$request->name_search.'%']]);
        }
        $users = $users->orderBy('created_at','desc')->get();
        return Datatables::of($users)
            ->addIndexColumn()
            ->editColumn('created_at',function($user){
                return hcms_date(strtotime($user->created_at),'date-time');
            })
            ->editColumn('organization_type',function($user){
                return (!empty($user->organization_type))? 'Individual' : 'Company';
            })
            ->editColumn('wallet_balance',function($user){
                return formatAmount($user->wallet_balance);
            })
            ->editColumn('phone',function($user){
                if(!empty($user->phone))
                return $user->FullNumber;
                else
                return "NA";
            })
            ->addColumn('country',function($user){
                
                if(!empty($user->userDetail->country->name))
                return ucwords($user->userDetail->country->name);
                else
                return "NA";
            })
            ->addColumn('plan',function($user){
                $cur_plan = UserSubscription::where(['user_id'=>$user->id,'active'=>1])->first();
                if(!empty($cur_plan->membership->name))
                return ucwords($cur_plan->membership->name);
                else
                return "NA";
            })

            ->addColumn('project_completed',function($user){
                $proj_completed = Project::where(['user_id'=>$user->id,'status'=>4])->get()->count();
                if(isset($proj_completed))
                return $proj_completed;
                else
                return "NA";
            })

            ->addColumn('project_inprogress',function($user){
                $proj_inpro = Project::where(['user_id'=>$user->id,'status'=>3])->get()->count();
                if(isset($proj_inpro))
                return $proj_inpro;
                else
                return "NA";
            })


            ->addColumn('step',function($user){
                $step = $user->step;
                if($user->approved == 0 || $user->approved == 2){
                    if(empty($user->email_verified_at)){ return "New user"; }
                    elseif(!empty($user->email_verified_at) && $step == 1){ return "Email Verified"; }
                    elseif($step == 2){
                        return "Basic Details Completed";
                    }elseif($step == 3){
                        return "Security Questions Completed";
                    }
                    elseif($step == 4){ return "Payment completed"; }
                    elseif($step == 5){
                        return "Membership Plan Subscribed";
                    }
                    elseif($step == 6){
                        return "Public Profile Completed";
                    }
                    // else{ return "Payment failed"; }
                }
                else{
                    return "Background Check Completed";
                }
            })

            ->addColumn('action', function ($user) {
                $buttons = "";
                if (auth()->user()->hasPermissionTo('view clients')) {
                    $buttons .='<button type="button" class="btn btn-icon btn-success btn-sm" title="View" onclick="window.location.href=\''. route('admin.users.show', $user->uuid) .'\'"><i class="mdi mdi-eye"></i> </button> ';
                }
                if (auth()->user()->hasPermissionTo('edit clients')) {
                    $buttons .='<button type="button" class="btn btn-icon btn-info btn-sm" title="Edit" onclick="window.location.href=\''. route('admin.users.edit', $user->uuid) .'\'"><i class="mdi mdi-pencil-outline"></i> </button> ';
                }
                if (auth()->user()->hasPermissionTo('delete clients')) {
                    $buttons .='<button type="button" class="btn btn-icon btn-danger btn-sm" title="Delete" data-toggle="modal" data-target="#danger-alert-modal" onclick="deleteForm(\'delete-form'.$user->uuid.'\')"><i class="mdi mdi-trash-can-outline"></i> </button><form id="delete-form'.$user->uuid.'" method="post" action="'.route("admin.users.destroy", $user->uuid).'"><input type="hidden" name="_method" value="delete"><input type="hidden" name="_token"  value="'.csrf_token().'"></form>';
                }
                return $buttons;
            })->addColumn('active', function ($user) {
                if($user->active == 1) {
                    return '<div class="active-checkbox"><input type="checkbox" data-user="'.$user->id.'" id="switch'.$user->id.'" checked data-switch="success"><label for="switch'.$user->id.'" data-on-label="Active" data-off-label="Block" class="mb-0 mt-0 d-block"></label></div>';
                } else {
                    return '<div class="active-checkbox"><input type="checkbox" data-user="'.$user->id.'" id="switch'.$user->id.'"  data-switch="success"><label for="switch'.$user->id.'" data-on-label="Active" data-off-label="Block" class="mb-0 mt-0 d-block"></label></div>';
                }
            })->addColumn('checkbox', function ($user) {
                return '<input type="checkbox" id="'.$user->id.'" value="'.$user->id.'" name="user_checkbox[]" class="bulk-slct user-slct" />';
              })
            ->make(true);
    }

    /**
     * For displaying the edit form for User
     *
     * @param User $user
     * @return void
     */
    public function edit(User $user)
    {
        $slctd_industries = $user->industries->pluck('id')->toArray();
        $slctd_categories = $user->categories->pluck('id')->toArray();
        $slctd_sub_categories = $user->subCategories->pluck('id')->toArray();
        $slctd_skills = $user->skills->pluck('id')->toArray();

        $countries = Country::get();
        $industries = Industry::where('active',1)->get();
        $categories_list =  Category::select('id','name')->where('active',1)->get();
        $categories = Category::select('id','name')->where('active',1)->whereIn('id',$slctd_categories)->get();
        $sub_categories = SubCategory::where('active',1)->whereHas('categories',function($q) use($slctd_categories){
            $q->whereIn('id',$slctd_categories);
        })->get();
        $skills = Skill::where('active',1)->whereHas('subCategories',function($q) use($slctd_sub_categories){
            $q->whereIn('id',$slctd_sub_categories);
        })->get();
        $plans = MembershipPlan::where(['active'=>1,'user_type'=>0])->get();
        
        $current_subscription = UserSubscription::where(['user_id'=>$user->id,'active'=>1])->first();
        return view('admin.users.edit', compact('user','countries','categories_list','categories','industries','sub_categories','skills','plans','slctd_industries','slctd_categories','slctd_sub_categories','slctd_skills','current_subscription'));
    }
    /**
     * For storing the details submitted in edit form
     *
     * @param UserRequest $request
     * @param User $user
     * @return void
     */
    public function update(UserRequest $request,User $user)
    {
        // Request data
        $request_data = [];
        $request_data['username'] = $request->username;
        $request_data['full_name'] = $request->full_name;
        $request_data['email'] = $request->email;
        $request_data['organization_type'] = $request->organization_type;
        $request_data['country_code'] = $request->country_code;
        $request_data['phone'] = $request->phone;

        // if ($request->hasFile('profile_image'))
        // {
        //     $profile_image = $request->file('profile_image')->store('public/user/profile-images');
        //     $request_data['profile_image'] = str_replace('public/user/profile-images/','',$profile_image);
        // }
        if ($request->hasFile('profile_image')) {

            $rules = [
                'profile_image' => 'required|
                mimes:jpeg,jpg,png|max:10000'
                ]; 
            $request->validate($rules);
            
            $icon = $request->file('profile_image')->store('public/user/profile-images');
            $request_data['profile_image'] = str_replace('public/user/profile-images', '', $icon);
        

        } elseif ($request->remove_title == 'removed') {
            $request_data['profile_image'] = "";
            unlink(public_path() . '/storage/user/profile-images' . $user->profile_image);
        }
        // Update user
        $user->update($request_data);

        $deatils = [];
        // $deatils['paypal_email'] = $request->paypal_email;
        $deatils['address_line_1'] = $request->address_line_1;
        $deatils['address_line_2'] = $request->address_line_2;
        $deatils['country_id'] = $request->country_id;
        $deatils['state'] = $request->state;
        $deatils['city'] = $request->city;
        $deatils['zipcode'] = $request->zipcode;
        $deatils['website_url'] = $request->website_url;
        if(!empty($request->established_since)){
            $deatils['established_since'] = gmtDateTime(strtotime($request->established_since),'date');
        }else{
            $deatils['established_since'] = NULL;
        }
        $deatils['years_of_experience'] = $request->years_of_experience;
        $deatils['company_tagline'] = $request->company_tagline;
        // $deatils['user_id'] = $user->id;
        // $deatils['referral_code'] = str_random(8);
        $deatils['user_history'] = $request->company_history;
        $deatils['corporate_presentation'] = $request->corporate_presentation;
        $deatils['profile_headline'] = $request->profile_headline;
        if(isset($request->video_links)){
        $deatils['video_urls'] = json_encode(array_filter($request->video_links));
        }

        $user_detail = UserDetail::where('user_id',$user->id)->update($deatils);
        
        // Syncing to pivot tables
        $user->industries()->detach();
        $user->industries()->attach($request->industry_ids);
        $user->categories()->detach();
        $user->categories()->attach($request->categories_ids);
        $user->subCategories()->detach();
        $user->subCategories()->attach($request->sub_categories_ids);
        $user->skills()->detach();
        $user->skills()->attach($request->skills);

        // Subscribing to a plan
        if(!empty($request->plan_id)){
            $plan = MembershipPlan::where('id',$request->plan_id)->first();
            $current_subscription = UserSubscription::where(['user_id'=>$user->id,'active'=>1])->first();
            if(empty($current_subscription)){
                UserSubscription::create([
                    'user_id' => $user->id,
                    'membership_plan_id' => $plan->id,
                    'start_date' => date('Y-m-d H:i:s'),
                    'end_date' => date('Y-m-d H:i:s',strtotime('+1 months')),
                    'duration_type' => 0,
                    'amount' => $plan->monthly_amount,
                    'setup_charge' => $plan->setup_charge,
                    'service_percentage' => $plan->service_percentage,
                    'product_commission' => $plan->product_commission,
                    'auto_renewal' => 0,
                ]);

                Transaction::create([
                    'user_id' => $user->id,
                    'project_id' => $plan->id,
                    'user_subscription_id' => $plan->id,
                    'type' => 0,
                    'amount' => $plan->monthly_amount,
                    'transaction_type' => 0,
                    'notes' => 'membership payment',
                    'payment_status' => 1,
                    'payment_response' => 'payment response',
                ]);
    
                Invoice::create([
                    'user_id' => $user->id,
                    'project_milestone_id' => $plan->id,
                    'user_subscription_id' => $plan->id,
                    'type' => 5,
                    'amount' => $plan->monthly_amount,
                    'status' => 1,
                    'due_date' => date('Y-m-d H:i:s',strtotime('+1 months')),
                ]);


            }else{
                if($current_subscription->membership_plan_id != $plan->id){
                    $current_subscription->active = 0;
                    $current_subscription->save();
                    UserSubscription::create([
                        'user_id' => $user->id,
                        'membership_plan_id' => $plan->id,
                        'start_date' => date('Y-m-d H:i:s'),
                        'end_date' => date('Y-m-d H:i:s',strtotime('+1 months')),
                        'duration_type' => 0,
                        'amount' => $plan->monthly_amount,
                        'setup_charge' => $plan->setup_charge,
                        'service_percentage' => $plan->service_percentage,
                        'product_commission' => $plan->product_commission,
                        'auto_renewal' => 0,
                    ]);
                }
            }
        }
        return redirect()->route('admin.users.index')->with('toastr', ['type'=>'success','text'=>__('Client details have been updated successfully.')]);
    }

    /**
     * For displaying the Create form for user
     *
     * @return void
     */
    public function create()
    {
        $countries = Country::get();
        $industries = Industry::where('active',1)->get();
        $plans = MembershipPlan::where(['active'=>1,'user_type'=>0])->get();
        $categories_list =  Category::select('id','name')->where('active',1)->get();
        return view('admin.users.create',compact('countries','industries','plans','categories_list'));
    }

    /**
     * For storing the details submitted in create form
     *
     * 
     * @return void
     */
    public function store(UserRequest $request)
    {   
        // return $request;
        $password = str_random(8);
        $hashed_random_password = Hash::make($password);
        // Request data
        $request_data = [];
        $request_data['username'] = $request->username;
        $request_data['full_name'] = $request->full_name;
        $request_data['email'] = $request->email;
        $request_data['password'] = $hashed_random_password;
        $request_data['organization_type'] = $request->organization_type;
        $request_data['user_type'] = 0;
        $request_data['country_code'] = $request->country_code;
        $request_data['phone'] = $request->phone;
        $request_data['email_verified_at'] = Carbon::now();
        $request_data['phone_verified_at'] = Carbon::now();
        $request_data['approved'] = 1;
        $request_data['approved_at'] = Carbon::now();
        if(!empty($request->plan_id)){
            $request_data['step'] = 5;
        }else{
            $request_data['step'] = 4;
        }
        
        if ($request->hasFile('profile_image')) {   
            $rules = [
                'profile_image' => 'required|
                mimes:jpeg,jpg,png|max:10000'
                ]; 
            $request->validate($rules);
            $icon = $request->file('profile_image')->store('public/user/profile-images');
            $request_data['profile_image'] = str_replace('public/user/profile-images', '', $icon);
        } else {
            $request_data['profile_image'] = "";
        }
        // Create new user
        $user = User::create($request_data);
        // For create user details
        $deatils = [];
        // $deatils['paypal_email'] = $request->paypal_email;
        $deatils['address_line_1'] = $request->address_line_1;
        $deatils['address_line_2'] = $request->address_line_2;
        $deatils['country_id'] = $request->country_id;
        $deatils['state'] = $request->state;
        $deatils['city'] = $request->city;
        $deatils['zipcode'] = $request->zipcode;
        $deatils['website_url'] = $request->website_url;
        if(!empty($request->established_since)){
            $deatils['established_since'] = gmtDateTime(strtotime($request->established_since),'date');
        }else{
            $deatils['established_since'] = NULL;
        }
        $deatils['years_of_experience'] = $request->years_of_experience;
        $deatils['company_tagline'] = $request->company_tagline;
        $deatils['user_id'] = $user->id;
        $deatils['referral_code'] = str_random(8);
        $deatils['user_history'] = $request->company_history;
        $deatils['corporate_presentation'] = $request->corporate_presentation;
        $deatils['profile_headline'] = $request->profile_headline;
        $deatils['video_urls'] = json_encode(array_filter($request->video_links));

        $user_detail = UserDetail::create($deatils);

        // Syncing to pivot tables
        $user->industries()->attach($request->industry_ids);
        $user->categories()->attach($request->categories_ids);
        $user->subCategories()->attach($request->sub_categories_ids);
        //$user->skills()->attach($request->skills);

        // Subscribing to a plan
        if(!empty($request->plan_id)){
            $plan = MembershipPlan::where('id',$request->plan_id)->first();
            $user_subs = UserSubscription::create([
                'user_id' => $user->id,
                'membership_plan_id' => $plan->id,
                'start_date' => date('Y-m-d H:i:s'),
                'end_date' => date('Y-m-d H:i:s',strtotime('+1 months')),
                'duration_type' => 0,
                'amount' => $plan->monthly_amount,
                'setup_charge' => $plan->setup_charge,
                'service_percentage' => $plan->service_percentage,
                'product_commission' => $plan->product_commission,
                'auto_renewal' => 0,
            ]);

            Transaction::create([
                'user_id' => $user->id,
                'project_id' => $user_subs->id,
                'user_subscription_id' => $user_subs->id,
                'type' => 0,
                'amount' => $user_subs->amount,
                'transaction_type' => 0,
                'notes' => 'membership payment',
                'payment_status' => 1,
                'payment_response' => 'payment response',
            ]);

            Invoice::create([
                'user_id' => $user->id,
                'project_milestone_id' => $user_subs->id,
                'user_subscription_id' => $user_subs->id,
                'type' => 5,
                'amount' => $user_subs->amount,
                'status' => 1,
                'due_date' => date('Y-m-d H:i:s',strtotime('+1 months')),
            ]);

        }
        // Send Email and save to email queue
        $user_creation_template = EmailTemplate::where('static_email_heading','USER_CREATION')->first();
        if ($user_creation_template) {
            $email_html = $user_creation_template->template;
            $email_html = str_replace("[USER_NAME]", $user->full_name, $email_html);
            $email_html = str_replace("[USER_LOGIN_LINK]", route('admin.login.form'), $email_html);
            $email_html = str_replace("[USER_EMAIL]", $user->email, $email_html);
            $email_html = str_replace("[USER_PASSWORD]", $password, $email_html);
            $template = view('admin.email_templates.layout', compact('email_html'));
            $data = [];
            $data['to_email'] = $user->email;
            $data['to_name'] = $user->full_name;
            $data['subject'] = $user_creation_template->subject;
            $data['body'] = $template;
            $data['action_id'] = $user->id;
            $data['action_type'] = 'user_creation';
            $processEmailQueue=new ProcessEmailQueue($data);
            $processEmailQueue->sendEmail();
        }
        if ($user) {
            return redirect()->route('admin.users.index')->with('toastr', ['type'=>'success','text'=>'Client created successfully',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }

    /**
    * @param id
    * Function to toggle active and inactive status of an user
    * @return mixed
    * @throws \Exception
    */
    public function toggleActiveStatus($id)
    {
        $user = User::where('id',$id)->first();
        if(!empty($user->active)){
            $user->active = 0;
        } else {
            $user->active = 1;
        }
        $user->update();
    }
    public function toggleActiveStatuscomplete($id)
    {
      
        $project = Project::where('id',$id)->first();
        
            $project->status = 4;
      
        $project->update();
    }
    public function toggleActiveStatusclose($id)
    {
      
        $project = Project::where('id',$id)->first();
        
            $project->status = 5;
      
        $project->update();
    }
    

    /**
     * For deleting User
     *
     * @param User $user
     * @return void
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Client has been deleted successfully.'),]);
    }


    /**
     * For displaying the detail page of User
     *
     * @param User $user
     * @return void
     */
    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }
    /**
    * @param id
    * Function to list all the access logs
    * @return mixed
    * @throws \Exception
    */
    public function accessLogs(User $user,Builder $builder)
    {
        // For loading the ajax datatable data
        if (request()->ajax()) {
            // Get all the access logs of the admin user
            $logs = $user->authentications;
            $agent = new Agent();
            $logs->each(function ($log, $key) use ($agent) {
                $agent->setUserAgent($log->user_agent);
                $log->platform = $agent->platform();
                $log->platform_version = $agent->version($log->platform);
                $log->device = $agent->device();
                if ($agent->isMobile()) {
                    $log->device_type = 'mobile';
                } elseif ($agent->isTablet()) {
                    $log->device_type = 'tablet';
                } elseif ($agent->isDesktop()) {
                    $log->device_type = 'desktop';
                } else {
                    $log->device_type = 'laptop';
                }
                $log->browser = $agent->browser();
                $log->browser_version = $agent->version($log->browser);
                //Location
                $location = geoip()->getLocation($log->ip_address);
                $log->timezone = $location->timezone;
                $log->city = $location->city;
                $log->state = $location->state;
                $log->country = $location->country;
            });
            return Datatables::of($logs)
            ->addIndexColumn()
            ->addColumn('device', function ($log) {
                if ($log->device_type === "mobile") {
                    $device_icon = '<i class="fas fa-mobile"></i>';
                    $device_name = __("strings.mobile");
                } elseif ($log->device_type === "tablet") {
                    $device_icon = '<i class="fas fa-tablet"></i>';
                    $device_name = __("strings.tablet");
                } elseif ($log->device_type === "desktop") {
                    $device_icon = '<i class="fa fa-trash-o" aria-hidden="true"></i>';
                    $device_name = __("strings.desktop");
                } else {
                    $device_icon = '<i class="fas fa-laptop"></i>';
                    $device_name = __("strings.laptop");
                }
                $device_info = $device_name ?? $device_name. $log->platform ?? $log->platform.
                    $log->platform_version ?? $log->platform_version. $log->browser ?? $log->browser. $log->browser_version ?? $log->browser_version;
                $device_info = '';
                $device_info .= $log->device ? $log->device.', ' : '';
                $device_info .= $log->platform ? $log->platform.' ' : '';
                $device_info .= $log->platform_version ? $log->platform_version.', ' : '';
                $device_info .= $log->browser ? $log->browser.' ' : '';
                $device_info .= $log->browser_version ? $log->browser_version.' ' : '';
                // return $device_info;
                return  '<p title="'.$device_info.'">'.$log->device.'</p>';
            })
            ->addColumn('location', function ($log) {
                return  '<p title="'.$log->city.', '.$log->state.', '.$log->country.'">'.$log->city.'</p>';
            })
            ->editColumn('login_at', function ($log) {
                return $log->login_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($log->login_at), timezone()->setShortDateTimeFormat())) : '';
            })
            ->editColumn('logout_at', function ($log) {
                return $log->logout_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($log->logout_at), timezone()->setShortDateTimeFormat())) : '';
            })
            ->rawColumns(['location','device'])
            ->make(true);
        }
        // Defining the required columns and their specifications
        $column_specifications = [];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'login_at','title' => 'Logged In'];
        $column_specifications[] = ['data' => 'ip_address','title' => 'IP Address'];
        $column_specifications[] = ['data' => 'location','title' => 'Location'];
        $column_specifications[] = ['data' => 'device','title' => 'Device'];
        $column_specifications[] = ['data' => 'logout_at','title' => 'Logged Out'];
        // Pass the column specifications and required parameters along with the html builder
        $html = $builder->columns($column_specifications)->parameters(['responsive'=>true,'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',]);
        // For Listing all the Roles
        return view('admin.users.access_logs', compact('html','user'));
    }
    /**
     * User Activity Logs
     *
     * @param User $user
     * @param Request $request
     * @param [type] $role
     * @return void
     */
    public function activityLogs(User $user, Request $request,$role = null)
    {
        $activitiesList = Activity::where('causer_type', User::class)
            ->where('causer_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('admin.users.activity_logs', compact('activitiesList','role','user'));
    }

    /**
     * For approve or reject multiple user
     */
    public function approveUser(Request $request){
        User::whereIn('id',$request->user_ids)->update([
            'approved' => $request->status,
            'approved_at' => date('Y-m-d H:i:s')
        ]);
        
        if($request->status == 1){
            //approve
            $approve_template = EmailTemplate::where('static_email_heading','APPROVE_USER')->first();
            //Send notiification to Client
            foreach($request->user_ids as $user_id){
                //get user 
                $user = User::where('id',$user_id)->first();
                

                if ($approve_template) {
                    $email_html = $approve_template->template;
                    $email_html = str_replace("[USER_NAME]", $user->full_name, $email_html);
                    $email_html = str_replace("[USER_LOGIN_LINK]", route('login'), $email_html);
                    
                    $template = view('admin.email_templates.layout', compact('email_html'));
                    $data = [];
                    $data['to_email'] =$user->email;
                    $data['to_name'] = $user->full_name;
                    $data['subject'] = $approve_template->subject;
                    $data['body'] = $template;
                    $data['action_id'] = $user->id;
                    $data['action_type'] = 'approve_user';
                   
                    $processEmailQueue=new ProcessEmailQueue($data);
                    $processEmailQueue->sendEmail();
                }
            }
            

        }else if($request->status == 2) {
            
            $reject_template = EmailTemplate::where('static_email_heading','REJECT_USER')->first();      
            
            //Send notiification to Client
            foreach($request->user_ids as $user_id){
                //get user 
                $user = User::where('id',$user_id)->first();
              

                if ($approve_template) {
                    $email_html = $approve_template->template;
                    $email_html = str_replace("[USER_NAME]", $user->full_name, $email_html);
                    $email_html = str_replace("[USER_LOGIN_LINK]", route('login'), $email_html);
                    
                    $template = view('admin.email_templates.layout', compact('email_html'));
                    $data = [];
                    $data['to_email'] =$user->email;
                    $data['to_name'] = $user->full_name;
                    $data['subject'] = $approve_template->subject;
                    $data['body'] = $template;
                    $data['action_id'] = $user->id;
                    $data['action_type'] = 'approve_user';
                   
                    $processEmailQueue=new ProcessEmailQueue($data);
                    $processEmailQueue->sendEmail();
                }
            }
        }
    }

    /**
     * For get membership of user
     */
    public function membership(User $user){
        $current_subscription = UserSubscription::where(['user_id'=>$user->id,'active'=>1])->first();
        return view('admin.users.membership',compact('current_subscription','user'));
    }

    /**
     * For Transaction 
     */
    public function transaction(User $user){
        return view('admin.users.transactions',compact('user'));
    }

    /**
     * For Transaction List
     */

    public function transactionList(User $user){
        
        $transactions = Transaction::where('user_id',$user->id)->orderby("created_at","desc")->get();

        return Datatables::of($transactions)
        ->addIndexColumn()
        ->editColumn('transaction_id',function($trans){
            return getTransaction($trans->id);
        })
        ->editColumn('contract_id',function($trans){
            if(!empty($trans->project_contract_id))
            return ($trans->project_contract_id);
            else
            return "NA";
        })
        ->addColumn('invoice_number',function($trans){
            return "NA";
        })
        ->addColumn('paid_to',function($trans){
            return "NA";
        })
        ->editColumn('payment_status',function($trans){
            if(empty($trans->payment_status)){
                return "Pending";
            }elseif($trans->payment_status == 1){
                return "Paid";
            }elseif($trans->payment_status == 2){
                return "Failed";
            }
        })
        ->editColumn('amount',function($trans){
            return formatAmount($trans->amount);
        })
        ->editColumn('created_at',function($trans){
            return hcms_date(strtotime($trans->created_at), 'date-time');
        })
        ->make(true);
    }
    /**
     * For projects
     */

    public function project(User $user){
        return view('admin.users.projects',compact('user'));
    }

    /**
     * For listing all client projects
     */

    public function projectList(User $user){
        
        $projects_list = Project::where('user_id',$user->id)->orderby("created_at","desc")->get();

        return Datatables::of($projects_list)
        ->addIndexColumn()
        ->editColumn('title',function($proj){
            return ucwords($proj->title);
        })
        ->addColumn('industry',function($proj){
            $all_ind = $proj->industries->pluck('name')->toArray();
            $List = ucwords(implode(', ', $all_ind)); 
            return $List;
        })

        ->addColumn('category',function($proj){
            $all_cat = $proj->categories->pluck('name')->toArray();
            $catList = ucwords(implode(', ', $all_cat)); 
            return $catList;
        })

        ->editColumn('status',function($proj){
            return projectStatus($proj->status);
        })

        ->editColumn('created_at',function($proj){
            return hcms_date(strtotime($proj->created_at), 'date-time');
        })
        ->make(true);
    }

    /**
     * For Reviews and Ratings 
     */
    public function reviewsRatings(User $user){
        return view('admin.users.ratings_and_reviews',compact('user'));
    }


    /**
     * Client Ratings 
     */

    public function clientRatings(User $user){

        $rating_details = UserRating::whereHas('ratingQuestion',function($q){
            $q->where('user_type','client');
        })->select('id','project_id','rating_question_id','rated_by_user_id','rating_to_user_id','created_at',DB::raw('round(AVG(rating),1) as rating'))->where('rating_to_user_id',$user->id)->groupby('project_id','rated_by_user_id')->get();
        
        return Datatables::of($rating_details)
        ->addIndexColumn()

        ->editColumn('project_id',function($rating){
            return ucwords($rating->project->title);
        })

        ->editColumn('rated_by_user_id',function($rating){
            return ucwords($rating->rated_by->full_name);
        })

        ->editColumn('created_at',function($rating){
            return hcms_date(strtotime($rating->created_at), 'date-time');
        })

        ->editColumn('net_rating',function($rating){
            return ($rating->rating);
        })

        ->addColumn('action', function ($rating) {
                $buttons = "";
                $buttons .='<button type="button" class="btn btn-icon btn-success btn-sm" title="View" onclick="window.location.href=\''. route('admin.users.ratingDetails', ['user'=>$rating->rated_to,'project'=>$rating->project->uuid]) .'\'"><i class="mdi mdi-eye"></i> </button> ';
                return $buttons;
        })
       
        ->make(true);
    }

    /**
     * Reviews and ratings details
     */

     public function ratingDetails(User $user,Project $project){
       
        $all_ratings = UserRating::whereHas('ratingQuestion',function($q){
            $q->where('user_type','client');
        })->where([['rating_to_user_id',$user->id],['project_id',$project->id]])->get();

        $company_ratings = UserRating::whereHas('ratingQuestion',function($q){
            $q->where('user_type','company');
        })->where([['rated_by_user_id',$project->contracts->freelancer_id],['project_id',$project->id]])->get();

        $reviews = UserReview::where([['reviewed_to_user_id',$user->id],['project_id',$project->id]])->get();

        return view('admin.users.rating_details',compact('user','project','all_ratings','company_ratings','reviews'));
     }


}
