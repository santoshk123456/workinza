<?php

namespace App\Http\Controllers\Admin;

use Twilio;
use DataTables;
use App\SmsQueue;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\DataTables\SmsQueueDataTable;



class SmsQueueController extends Controller
{
    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index(Builder $builder, SmsQueueDataTable $dataTable)
    {
        return $dataTable->render('admin.sms_queues.index');
    }

    /**
     * View Function of email Queues
     *
     * @param [type] $id
     * @return void
     */
    public function show(SmsQueue $smsQueue)
    {
        return view('admin/sms_queues/show',compact('smsQueue'));
    }
    public function resend(SmsQueue $smsQueue){
        try{
            Twilio::message($smsQueue->mobile_number, $smsQueue->message);
            $smsQueue->update([
                'sent_status' => 1,
                'time_to_send' => date('Y-m-d H:i:s')
            ]);
            return redirect()->route('admin.sms_queues.index')->with('toastr', ['type'=>'success','text'=>'SMS was successfully sent.',]);
        } catch(Twilio\Exceptions\RestException  $e){
            throw new GeneralException($e->getMessage());
            return redirect()->route('admin.sms_queues.index');
        }
    }

}
