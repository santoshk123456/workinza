<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Hash;
use Illuminate\Support\Str;
use App\Http\Requests\Admin\AdminRequest;
use App\Http\Requests\Admin\ChangePasswordRequest;
use App\DataTables\AdminDataTable;
use Jenssegers\Agent\Agent;
use \Torann\GeoIP\Facades\GeoIP;
use Spatie\Activitylog\Models\Activity;
use App\Exceptions\GeneralException;



class AdminController extends Controller
{
    protected $actions = ['print', 'excel', 'myCustomAction'];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * For Displaying the Admins Listing page
     */
    public function index($role = null,$status = null,Builder $builder,AdminDataTable $dataTable)
    {
        // For Listing all the Roles
        $roles = Role::where("guard_name","admin")->get();
        return $dataTable->with('role', $role)->render('admin.admins.index', compact('roles','role'));
    }
    /**
     * For displaying the add form for new admin user
     */
    public function create($role = null)
    {
        $roles = Role::where("guard_name","admin")->get();
        return view('admin/admins/create', compact('roles','role'));
    }
    /**
     * For displaying the add form for new admin user
     */
    public function store(AdminRequest $request,$role=null)
    {
        // $hashed_random_password = Hash::make(str_random(8));
        // Request data
        $request_data = [];
        $request_data['name'] = $request->name;
        $request_data['username'] = $request->username;
        $request_data['email'] = $request->email;
        // $request_data['password'] = $hashed_random_password;
        $request_data['country_code'] = $request->country_code;
        $request_data['mobile_number'] = $request->phone;
        if($request->active && !empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        $request_data['remember_token'] = $request->remember_token;
       
        // if($request->hasFile('profile_image')){
        //     $profile_image = $request->file('profile_image')->store('public/admin/profile-images');
        //     $request_data['profile_image'] = str_replace('public/admin/profile-images','',$profile_image);
        // }
        if ($request->hasFile('profile_image')) {   
            $rules = [
                'profile_image' => 'required|
                mimes:jpeg,jpg,png|max:10000'
                ]; 
            $request->validate($rules);
            $icon = $request->file('profile_image')->store('public/admin/profile-images');
            $request_data['profile_image'] = str_replace('public/admin/profile-images', '', $icon);
        } else {
            $request_data['profile_image'] = "";
        }



        // Create new admin
        $admin = Admin::create($request_data);
        if ($admin) {
            // Assign admin role
            $admin->syncRoles($request->role);
            return redirect()->route('admin.admins.index', $role)->with('toastr', ['type'=>'success','text'=>'User created successfully',]);
        } else {
            throw new GeneralException(__('toastr.access_denied', ['attribute'=>"please there is some error.please try again !"]));
        }
    }
    /**
     * For displaying the edit form for admin user
     */
    public function edit(Admin $admin,$role = null)
    {
        // Listing all the roles that are needed to be displayed in select box
        $roles = Role::where("guard_name","admin")->get();
        return view('admin/admins/edit', compact('admin','roles','role'));
    }
     /**
     * For displaying the edit form for admin 
     */
    public function editprofile(Admin $admin,$role = null)
    {
        // Listing all the roles that are needed to be displayed in select box
        $roles = Role::where("guard_name","admin")->get();
        return view('admin/admins/editprofile', compact('admin','roles','role'));
    }
    /**
     * For displaying the add form for new admin user
     */
    public function update(AdminRequest $request,Admin $admin,$role = null)
    {
        // Request data
        $request_data = [];
        $request_data['name'] = $request->name;
        $request_data['email'] = $request->email;
        $request_data['username'] = $request->username;
        // $request_data['mobile_number'] = $request->mobile_number;
        $request_data['country_code'] = $request->country_code;
        $request_data['mobile_number'] = $request->phone;
        if($request->active && !empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        // if ($request->hasFile('profile_image'))
        // {
        //     $profile_image = $request->file('profile_image')->store('public/admin/profile-images');
        //     $request_data['profile_image'] = str_replace('public/admin/profile-images/','',$profile_image);
        // }

        if ($request->hasFile('profile_image')) {

            $rules = [
                'profile_image' => 'required|
                mimes:jpeg,jpg,png|max:10000'
                ]; 
            $request->validate($rules);
            
            $icon = $request->file('profile_image')->store('public/admin/profile-images');
            $request_data['profile_image'] = str_replace('public/admin/profile-images', '', $icon);
        

        } elseif ($request->remove_title == 'removed') {
            $request_data['profile_image'] = "";
            unlink(public_path() . '/storage/admin/profile-images' . $admin->profile_image);
        }
        // Update admin
        $admin->update($request_data);
        // Assign Admin role
        $admin->syncRoles($request->role);
        return redirect()->route('admin.admins.index', $role)->with('toastr', ['type'=>'success','text'=>__('Admin user details have been updated successfully.')]);
    } 
     /**
     * For displaying the add form for new admin user
     */
    public function updateprofile(AdminRequest $request,Admin $admin,$role = null)
    {
        // Request data
        $request_data = [];
        $request_data['name'] = $request->name;
        $request_data['email'] = $request->email;
        $request_data['username'] = $request->username;
        $request_data['country_code'] = $request->country_code;
        $request_data['mobile_number'] = $request->phone;
        if($request->active && !empty($request->active)){
            $request_data['active'] = 1;
        } else {
            $request_data['active'] = 0;
        }
        if ($request->hasFile('profile_image'))
        {
            $profile_image = $request->file('profile_image')->store('public/admin/profile-images');
            $request_data['profile_image'] = str_replace('public/admin/profile-images/','',$profile_image);
        }
        // Update admin
        $admin->update($request_data);
        // Assign Admin role
        $admin->syncRoles($request->role);
        return redirect()->route('admin.admins.show',compact('admin','role'))->with('toastr', ['type'=>'success','text'=>__('Admin user details have been updated successfully.')]);
    }
    /**
     * For displaying the detail page of Admin user
     */
    public function show(Admin $admin,$role = null)
    {
        return view('admin/admins/show', compact('admin','role'));
    }
    /**
    * @param Role
    *
    * @return mixed
    * @throws \Exception
    */
    public function destroy(Admin $admin,$role = null)
    {
       //Check that  not super admin
       if ($admin->id == 1) {
           throw new GeneralException('Sorry you cannot delete this user.');
       } else {
           $admin->delete();
       }
       return redirect()->route('admin.admins.index',$role)->with('toastr', ['type'=>'success','text'=>__('Admin user has been deleted successfully'),]);
    }
    /**
    * @param id
    * Function to toggle active and inactive status of an admin user
    * @return mixed
    * @throws \Exception
    */
    public function toggleActiveStatus($uuid)
    {
        $admin = Admin::where('uuid',$uuid)->first();
        if(!empty($admin->active)){
            $admin->active = 0;
        } else {
            $admin->active = 1;
        }
        $admin->update();
    }
    /**
    * @param id
    * Function to list all the access logs
    * @return mixed
    * @throws \Exception
    */
    public function accessLogs(Admin $admin,Builder $builder,$role = null)
    {
        // For loading the ajax datatable data
        if (request()->ajax()) {
            // Get all the access logs of the admin user
            $logs = $admin->authentications;
            $agent = new Agent();
            $logs->each(function ($log, $key) use ($agent) {
                $agent->setUserAgent($log->user_agent);
                $log->platform = $agent->platform();
                $log->platform_version = $agent->version($log->platform);
                $log->device = $agent->device();
                if ($agent->isMobile()) {
                    $log->device_type = 'mobile';
                } elseif ($agent->isTablet()) {
                    $log->device_type = 'tablet';
                } elseif ($agent->isDesktop()) {
                    $log->device_type = 'desktop';
                } else {
                    $log->device_type = 'laptop';
                }
                $log->browser = $agent->browser();
                $log->browser_version = $agent->version($log->browser);
                //Location
                $location = geoip()->getLocation($log->ip_address);
                $log->timezone = $location->timezone;
                $log->city = $location->city;
                $log->state = $location->state;
                $log->country = $location->country;
            });
            return Datatables::of($logs)
            ->addIndexColumn()
            ->addColumn('device', function ($log) {
                if ($log->device_type === "mobile") {
                    $device_icon = '<i class="fas fa-mobile"></i>';
                    $device_name = __("strings.mobile");
                } elseif ($log->device_type === "tablet") {
                    $device_icon = '<i class="fas fa-tablet"></i>';
                    $device_name = __("strings.tablet");
                } elseif ($log->device_type === "desktop") {
                    $device_icon = '<i class="fa fa-trash-o" aria-hidden="true"></i>';
                    $device_name = __("strings.desktop");
                } else {
                    $device_icon = '<i class="fas fa-laptop"></i>';
                    $device_name = __("strings.laptop");
                }
                $device_info = $device_name ?? $device_name. $log->platform ?? $log->platform.
                    $log->platform_version ?? $log->platform_version. $log->browser ?? $log->browser. $log->browser_version ?? $log->browser_version;
                $device_info = '';
                $device_info .= $log->device ? $log->device.', ' : '';
                $device_info .= $log->platform ? $log->platform.' ' : '';
                $device_info .= $log->platform_version ? $log->platform_version.', ' : '';
                $device_info .= $log->browser ? $log->browser.' ' : '';
                $device_info .= $log->browser_version ? $log->browser_version.' ' : '';
                // return $device_info;
                return  '<p title="'.$device_info.'">'.$log->device.'</p>';
            })
            ->addColumn('location', function ($log) {
                return  '<p title="'.$log->city.', '.$log->state.', '.$log->country.'">'.$log->city.'</p>';
            })
            ->editColumn('login_at', function ($log) {
                return $log->login_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($log->login_at), timezone()->setShortDateTimeFormat())) : '';
            })
            ->editColumn('logout_at', function ($log) {
                return $log->logout_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($log->logout_at), timezone()->setShortDateTimeFormat())) : '';
            })
            ->rawColumns(['location','device'])
            ->make(true);
        }
        // Defining the required columns and their specifications
        $column_specifications = [];
        $column_specifications[] = ['defaultContent'=> '','data'=> 'DT_RowIndex','name'=> 'DT_RowIndex','title'=> '','render'=> null,'orderable'=> false,'searchable'=> false,'exportable'=> false,'printable'=> true,'footer'=> ''];
        $column_specifications[] = ['data' => 'login_at','title' => 'Logged In'];
        $column_specifications[] = ['data' => 'ip_address','title' => 'IP Address'];
        $column_specifications[] = ['data' => 'location','title' => 'Location'];
        $column_specifications[] = ['data' => 'device','title' => 'Device'];
        $column_specifications[] = ['data' => 'logout_at','title' => 'Logged Out'];
        // Pass the column specifications and required parameters along with the html builder
        $html = $builder->columns($column_specifications)->parameters(['responsive'=>false,'drawCallback' => 'function() {$(".dataTables_paginate > .pagination").addClass("pagination-rounded")}',]);
        // For Listing all the Roles
        return view('admin.admins.access_logs', compact('html','role','admin'));
    }
    /**
    * @param id
    * Function to list all the activity logs
    * @return mixed
    * @throws \Exception
    */
    public function activityLogs(Admin $admin, Request $request,$role = null)
    {
        $activitiesList = Activity::where('causer_type', Admin::class)
            ->where('causer_id', $admin->id)
            ->orderBy('created_at', 'desc')
            ->get();
        // foreach ($activitiesList as $activity) {
        //     $activity->created_at = timezone()->convertToLocal(Carbon::parse($activity->created_at), timezone()->setShortDateTimeFormat());
        // }
        return view('admin.admins.activity_logs', compact('activitiesList','role','admin'));
    }
    /**
    * @param role
    * Function to display the change password form
    * @return mixed
    * @throws \Exception
    */
    public function showChangePassword(Admin $admin, Request $request,$role = null)
    {
        return view('admin.admins.change_password',compact('role','admin'));
    }
    /**
    * @param role
    * Function to handle the change password form request
    * @return mixed
    * @throws \Exception
    */
    public function changePassword(ChangePasswordRequest $request)
    {
        $role = null;
        $admin = Admin::where('id',auth()->user()->id)->first();
        if (!Hash::check($request->current_password, $admin->password)) {
            throw new GeneralException("Please enter the correct current password.");
        }
        $admin->password = Hash::make($request->password);
        $admin->save();
        return redirect()->route('admin.admins.show',$admin->uuid)->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
    }
}
