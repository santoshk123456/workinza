<?php

namespace App\Http\Controllers\Admin;

use config;
use DataTables;
use App\Setting;
use App\Models\Auth\Admin;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use App\Http\Requests\Admin\SettingRequest;



class SettingController extends Controller
{
    /**
     * Index function
     *
     * @param Builder $builder
     * @return void
     */
    public function index($type = null)
    {
        $settings = Setting::where('type',$type)->orderBy('updated_at','desc')->get();
        return view('admin/settings/index',compact(['settings','type']));
    }
    public function update(Request $request,$type = null)
    {
        $settings = Setting::where('type',$type)->get();
        foreach ($settings as $setting) {
            if($setting->value_type == 'checkbox'){
                if(empty($request->input($setting->key))){
                    $setting->value = 0;
                } else {
                    $setting->value = 1;
                }
            } else {
                $setting->value = $request->input($setting->key);
            }
            $setting->update();
        }
            //  config(['data.currency_symbol' => $request->currency_symbol]); 
            //  config(['data.default_date_format' => $request->display_date_format]);
            //  $text = '<?php return ' . var_export(config('data'), true) . ';';
            //  file_put_contents(config_path('data.php'), $text);
     

            config(['data.default_date_format' =>  $request->display_date_format]);
            $fp = fopen(base_path() .'/config/data.php' , 'w');
            fwrite($fp, '<?php return ' . var_export(config('data'), true) . ';');
            fclose($fp);
            Artisan::call('config:clear');
        
        return redirect()->route('admin.settings.index',$type)->with('toastr', ['type'=>'success','text'=>__('Settings have been updated successfully.')]);
    }
    
}
