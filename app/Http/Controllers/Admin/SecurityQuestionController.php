<?php

namespace App\Http\Controllers\Admin;

use App\SecurityQuestion;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\DataTables\SecurityQuestionDataTable;
use App\Http\Requests\Admin\SecurityQuestionRequest;

class SecurityQuestionController extends Controller
{
    
  /**
     * To list security questions
     * */
    public function index(Builder $builder, SecurityQuestionDataTable $dataTable)
    {
        return $dataTable->render('admin.security_questions.index');
    }

    /***
     * To store security questions 
     */

    public function store(SecurityQuestionRequest $request)
    {
        $request_data = [];
        $request_data['user_type'] = $request->user_type;
        $request_data['question'] = $request->question;
        if(SecurityQuestion::create($request_data))
        return redirect()->route('admin.security_questions.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new Question.')]);
        else
        return redirect()->route('admin.security_questions.index')->with('toastr', ['type'=>'error','text'=>__('Question can not be created.')]);
    }

    /**
     * To update security questions
     */

    public function update(SecurityQuestionRequest $request,SecurityQuestion $securityQuestion)
    {
        $request_data = [];
        $request_data['user_type'] = $request->user_type_edit;
        $request_data['question'] = $request->question;
        if($securityQuestion->update($request_data))
        return redirect()->route('admin.security_questions.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the Question.')]);
        else
        return redirect()->route('admin.security_questions.index')->with('toastr', ['type'=>'error','text'=>__('Question can not be updated.')]);
    }

    /**
     * To delete security questions
     */

    public function destroy(SecurityQuestion $securityQuestion)
    {
        $securityQuestion->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Question has been deleted successfully.'),]);
    }


}
