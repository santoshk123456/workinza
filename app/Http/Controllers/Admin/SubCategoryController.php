<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Industry;
use App\SubCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\DataTables\SubCategoryDataTable;
use App\Http\Requests\Admin\SubCategoryRequest;

class SubCategoryController extends Controller
{
    
    /**
     * To list sub category table
     * */
    public function index(Builder $builder, SubCategoryDataTable $dataTable)
    {
        return $dataTable->render('admin.sub_categories.index');
    }

    /**
     * To create sub categories
     */

    public function create()
    {
        $categories_list = Category::select('id','name')->where('active',1)->get();
    
       
        return view('admin.sub_categories.create',compact('categories_list'));
    }

    /***
     * To store Sub Categories 
     */

    public function store(SubCategoryRequest $request)
    {
        $request_data = [];
        $request_data['name'] = $request->sub_category_name;
        $create_sub_category = SubCategory::create($request_data);
        $create_sub_category->categories()->attach($request->categories_ids);
        $create_sub_category->industries()->attach($request->industry_ids);
        if($create_sub_category)
        return redirect()->route('admin.sub_categories.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully created the new sub category.')]);
        else
        return redirect()->route('admin.sub_categories.index')->with('toastr', ['type'=>'error','text'=>__('sub category can not be created.')]);
    }

    /**
     * To show sub Categories
     */

    public function show(SubCategory $subCategory)
    {
        return view('admin.sub_categories.show', compact('subCategory'));
    }

    /**
     * To edit sub Categories
     */

    public function edit(SubCategory $subCategory)
    {
       
       
        $categories_list = Category::select('id','name')->where('active',1)->get();
        return view('admin.sub_categories.edit', compact('subCategory','categories_list'));
    }

     /**
     * To update sub Categories
     */

    public function update(SubCategoryRequest $request,SubCategory $subCategory)
    {
        $request_data = [];
        $request_data['name'] = $request->sub_category_name;
        if($request->active =="on"){
            $request_data['active'] = 1;
        }else{
            $request_data['active'] = 0;
        }
        $subCategory->categories()->detach();
        $subCategory->industries()->detach();
        $subCategory->categories()->attach($request->category_id);
        $subCategory->industries()->attach($request->industry_id);
        if($subCategory->update($request_data))
        return redirect()->route('admin.sub_categories.index')->with('toastr', ['type'=>'success','text'=>__('You have successfully updated the new Sub category.')]);
        else
        return redirect()->route('admin.sub_categories.index')->with('toastr', ['type'=>'error','text'=>__('Sub category can not be updated.')]);
    }


    /**
     * To delete sub Categories
     */

    public function destroy(SubCategory $subCategory)
    {
        $subCategory->delete();
        return redirect()->back()->with('toastr', ['type'=>'success','text'=>__('Sub Category has been deleted successfully.'),]);
    }

    /**
     * To change sub Category status
     */

    public function toggleActiveStatus($id)
    {
        $sub_cat = SubCategory::where('uuid',$id)->first();
        if(!empty($sub_cat->active)){
            $sub_cat->active = 0;
        } else {
            $sub_cat->active = 1;
        }
        
        if($sub_cat->update()){
            return redirect()->route('admin.sub_categories.index')->with('toastr', ['type'=>'success','text'=>'Sub category status has been updated successfully']);
        }else{
            return redirect()->route('admin.sub_categories.index')->with('toastr', ['type'=>'error','text'=>'Sub category status can not be updated ']);
        }
    }

}
