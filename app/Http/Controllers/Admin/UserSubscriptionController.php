<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Invoice;
use App\MembershipPlan;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserSubscriptionExport;
use App\DataTables\UserSubscriptionDataTable;

class UserSubscriptionController extends Controller
{
    /***
     * To list User Subscription Table
     */

    public function index(Builder $builder, UserSubscriptionDataTable $dataTable)
    {
        $membership_plans = MembershipPlan::select('id','name')->get();
        return $dataTable->render('admin.user_subscriptions.index',compact('membership_plans'));
    }

     /***
     * Export Subscription payments table
     */

    public function ExportSubscription(Request $request){
        
        $condition = [];
        $condition[] = ['invoices.type',5];
        if($request->has('daterange') && $request->daterange != ''){
            $range = explode('-',$request->daterange);
            $from_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[0]))));
            $to_date = date('Y-m-d',strtotime(str_replace('/','-', trim($range[1]))));
        }else{
            $from_date ='';
            $to_date ='';
        }

        if($request->has('mem_plan')){
            $condition[] = ['user_subscriptions.membership_plan_id',$request->mem_plan];
        }

        if($request->has('user_type')){
            $condition[] = ['users.user_type',$request->user_type];
        }

        $log_array = [];
        
       

        $logs = Invoice::with(['userSubscription','user'=>function($q){
        $q->select('id','user_type','full_name');
        }])
        ->whereHas('userSubscription', function($q){
            $q->where('user_subscriptions.membership_plan_id', request('mem_plan'));
        })
        ->whereHas('user', function($q){
            $q->where('users.user_type', request('user_type'));
        }
        )
        ->whereDate('created_at','>=',$from_date)->whereDate('created_at','<=',$to_date)->where('invoices.type',5)->get();


        foreach($logs as $key=>$log)
        {
                $invoice_sts = '';
                if(empty($log->status))
                $invoice_sts = "Pending";
                elseif($log->status == 1)
                $invoice_sts = "Paid";
                else
                $invoice_sts = "Rejected";

            $log_array[] = array(
                '#' => $key +1,
                'Invoice Number'  => "IN".$log->id,
                'Paid By'   => ucwords($log->user->full_name),
                'User Type'   => (empty($log->user->user_type)? "Client" : "Freelancer"),
                'Subscription Plan'   => ucwords($log->userSubscription->membership->name),
                'Subscription Type'   => (empty($log->userSubscription->duration_type)? "Monthly" : "Annual"),
                'Status'   => $invoice_sts,
                'Amount'   => formatAmount($log->amount),
                'Invoiced On'   => hcms_date(strtotime($log->created_at), 'date-time', false)
            );
        }

        if($request->exp_excel){
            switch($request->exp_excel) {
    
                case 'excel': 
                    return Excel::download(new UserSubscriptionExport($log_array), 'SubscriptionPayments.xlsx');
                break;
    
                case 'csv': 
                    return Excel::download(new UserSubscriptionExport($log_array), 'SubscriptionPayments.csv');
                break;
            }
        }

    }

    /**
     * To show Subscription Payments
     */

     public function show(Invoice $invoice){
         return view('admin.user_subscriptions.show',compact('invoice'));
     }

     /**
      * To create invoice 
      */

    public function invoice(){

        $all_users = User::select('id','full_name','email')->get();
        return view('admin.user_subscriptions.invoice',compact('all_users'));
    }

    /**
     * To store invoice 
     */

    public function StoreInvoice(Request $request){

        $request_data = [];
        $request_data['user_id'] = $request->invoice_user;
        $request_data['type'] = $request->invoice_type;
        $request_data['amount'] = $request->bill_amount;
        $request_data['due_date'] = date('Y-m-d H:i:s');
        //create invoice
        $create_invoice = Invoice::create($request_data);
        if($create_invoice){
            return redirect()->route('admin.user_subscriptions.index')->with('toastr', ['type'=>'success','text'=>'Invoice created successfully',]);
        }
    }





}
