<?php

// use auth;
// use DateTime;
// use DateTimeZone;
use App\Setting;
use App\UserSubscription;
use App\Helpers\General\Timezone;
use App\Helpers\General\HtmlHelper;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;
use App\UserRating;

/*
 * Global helpers file with misc functions.
 */
if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('timezone')) {
    /**
     * Access the timezone helper.
     */
    function timezone()
    {
        return resolve(Timezone::class);
    }
}

if (! function_exists('include_route_files')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function include_route_files($folder)
    {
        try {
            $rdi = new recursiveDirectoryIterator($folder);
            $it = new recursiveIteratorIterator($rdi);

            while ($it->valid()) {
                if (! $it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

if (! function_exists('home_route')) {

    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function home_route()
    {
        if (auth()->check()) {
            if (auth()->user()->can('view backend')) {
                return 'admin.admins.dashboard';
            } else {
                return 'frontend.user.dashboard';
            }
        }

        return 'frontend.index';
    }
}

if (! function_exists('style')) {

    /**
     * @param       $url
     * @param array $attributes
     * @param null  $secure
     *
     * @return mixed
     */
    function style($url, $attributes = [], $secure = null)
    {
        return resolve(HtmlHelper::class)->style($url, $attributes, $secure);
    }
}

if (! function_exists('script')) {

    /**
     * @param       $url
     * @param array $attributes
     * @param null  $secure
     *
     * @return mixed
     */
    function script($url, $attributes = [], $secure = null)
    {
        return resolve(HtmlHelper::class)->script($url, $attributes, $secure);
    }
}

if (! function_exists('form_cancel')) {

    /**
     * @param        $cancel_to
     * @param        $title
     * @param string $classes
     *
     * @return mixed
     */
    function form_cancel($cancel_to, $title, $classes = 'btn btn-danger btn-sm')
    {
        return resolve(HtmlHelper::class)->formCancel($cancel_to, $title, $classes);
    }
}

if (! function_exists('form_submit')) {

    /**
     * @param        $title
     * @param string $classes
     *
     * @return mixed
     */
    function form_submit($title, $classes = 'btn btn-success btn-sm pull-right')
    {
        return resolve(HtmlHelper::class)->formSubmit($title, $classes);
    }
}

if (! function_exists('camelcase_to_word')) {

    /**
     * @param $str
     *
     * @return string
     */
    function camelcase_to_word($str)
    {
        return implode(' ', preg_split('/
          (?<=[a-z])
          (?=[A-Z])
        | (?<=[A-Z])
          (?=[A-Z][a-z])
        /x', $str));
    }
}

if (! function_exists('get_date_formats')) {

    /**
     * @param $str
     *
     * @return string
     */
    function get_date_format($type)
    {
        switch ($type) {
            case 'log':
                return 'd-m-Y h:i:s A';
            break;
            case 'activity-date':
                return 'F j, Y';
            break;
            case '12-hour-time-only':
                return 'g:i A';
            break;
            default:
                return 'd-m-Y';
            break;
        }
    }
}
if (! function_exists('set_page_titile')) {

    /**
     * @param $str
     *
     * @return string
     */
    function set_page_titile($title)
    {
        return $title.' | '.app_name();
    }
}

if (! function_exists('snake_to_ucwords')) {

    /**
     * @param $str
     *
     * @return string
     */
    function snake_to_ucwords($str)
    {
        return ucwords(str_replace("_", " ", $str));
    }
}

if (! function_exists('format_xml')) {

    /**
     * @param $str
     *
     * @return string
     */
    function format_xml($xml)
    {
        $t_xml = new \DOMDocument();
        $t_xml->loadXML($xml);
        return $t_xml->saveXML($t_xml->documentElement);
    }
}

    function gmtDateTime($dateStringInput,$type)
	{
		switch ($type){
            case 'date-time': return date('Y-m-d H:i:s', $dateStringInput); break;
            case 'time': return date('h:i s', $dateStringInput); break;
            case 'date': return date('Y-m-d', $dateStringInput); break;
            default: return date('Y-m-d', $dateStringInput); break;
        }
    }
    function projectStatus($status){
        if($status == 0){
            return "New";
        }else if($status == 1){
            return "Active";
        }else if($status == 2){
            return "Assigned";
        }else if($status == 3){
            return "In Progress";
        }else if($status == 4){
            return "Completed";
        }else if($status == 5){
            return "Closed";
        }else if($status == 6){
            return "Dispute";
        }
    }
    function projectType($type){
        if($type == 0){
            return "Fixed";
        }else if($type == 1){
            return "Hourly";
        }
    }
    function userType($type){
        if($type == 0){
            return "Client";
        }else if($type == 1){
            return "Freelancer";
        }
    }
    function getProposalStatus($type){
        if($type == 0){
            return "Submitted";
        }else if($type == 1){
            return "Active";
        }else if($type == 2){
            return "Rejectecd";
        }
    }
    function getTransactionStatus($type){
        if($type == 0){
            return "Pending";
        }else if($type == 1){
            return "Success";
        }else if($type == 2){
            return "Failed";
        }
    }
    
    
    function getTimeline($timeline_count,$type){
        if($timeline_count == 1){
            $type = rtrim($type, "s");
        }
        $concatinated_string = $timeline_count ." ". $type;
        return $concatinated_string;
    }
    function generateProjectId($id){
        return "PR".$id;
    }
    function generateTransactionId($id){
        return "TR".$id;
    }
    
      //common datetime format function 
      function hcms_date($dateStringInput, $type, $timezoneEnabled = false){
        
        $format = config('data.default_date_format'); //m-d-Y
        $timezone = config('data.default_timezone'); //5.5
        $siteTimezone = config('app.timezone_value'); //5.5

        $timezoneList = collect(config('data.timezones'))->map(function($data, $index){
            $data['timezone_value'] = (string)$data['value'];
            return $data;
        })->keyBy('timezone_value');
        
        if($timezoneEnabled){
            $sitTimezoneName = $timezoneList[(string)$siteTimezone]['timezone'];
            $timezoneName = $timezoneList[(string)$timezone]['timezone'];
            $date = new DateTime(date("Y-m-d H:i:s", $dateStringInput), new DateTimeZone($sitTimezoneName));
            $date->setTimezone(new DateTimeZone($timezoneName));
            $dateString = strtotime($date->format('Y-m-d H:i:s'));
        }else{
            $dateString = $dateStringInput;
        }
        
        switch ($type){
            case 'date-time': return date($format.' h:i A', $dateString); break;
            case 'time': return date('h:i A', $dateString); break;
            case 'date': return date($format, $dateString); break;
            default: return date($format, $dateString); break;
        }
    }


    /***Image */

    function thump_resize_image($image, $width = 150, $height = null, $force = false)
    {
        //return asset($image);
        /* FIle does not exist */
        if(!file_exists(public_path($image))){
            $image = "images/no-image.png";
        }
        $width = $width == 0? null: $width;
        $height = $height == 0? null: $height;
        if($force || true || !file_exists(public_path("storage/thumb/".dirname($image)."/".$width."x".$height."-".basename($image)))){

            if(!is_dir(base_path('storage/thumb/'.dirname($image)))){
                Storage::disk('public')->makeDirectory('thumb/'.dirname($image));
            }
            Image::configure(array('driver' => 'gd'));
            $img = Image::make(public_path($image))->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
            
            $img->save(public_path("storage/thumb/".dirname($image)."/".$width."x".$height."-".basename($image)));
        }
        return asset("storage/thumb/".dirname($image)."/".$width."x".$height."-".basename($image));
    }

    function formatAmount($amount){
        return config('data.currency_symbol').number_format((float)$amount, 2, '.', '');;
    }
    
    

    function getInvoice($invoice){
        return "IN".$invoice;
    }

    function getTransaction($transaction){
        return "TN".$transaction;
    }
    function getContractId($id){
        return "CR".$id;
    }
    function getMilestoneStatus($status){
        if($status == 0){
            $msg = "Not Started";
        }else if($status == 1){
            $msg = "Activated";
        }else if($status == 2){
            $msg = "In Progress";
        }else if($status == 3){
            $msg = "Completed (In Review)";
        }else if($status == 4){
            $msg = "Approved";
        }else if($status == 5){
            $msg = "Paid";
        }else if($status == 6){
            $msg = "Rejected";
        }
        return $msg;
        
    }
    function generateTransactionType($type){
        $msg = "NA";
        if($type == 0){
            $msg = "Membership Payment";
        }else if($type == 1){
            $msg = "Vault Recharge";
        }else if($type == 2){
            $msg = "Milestone Payment";
        }else if($type == 3){
            $msg = "Referal Payment";
        }else if($type == 4){
            $msg = "Bonus Payment";
        }else if($type == 5){
            $msg = "Product Payment";
        }else if($type == 6){
            $msg = "Background Verification";
        }
        else if($type == 7){
            $msg = "Refund (Dispute)";
        }
        return $msg;
    }
    function getInvoiceType($status){
        if($status == 6){
            $msg = "Background verifcation";
        }else if($status == 1){
            $msg = "Project milestone payment";
        }else if($status == 2){
            $msg = "Cancellation";
        }else if($status == 3){
            $msg = "Refund";
        }else if($status == 4){
            $msg = "Bonus";
        }else if($status == 5){
            $msg = "Subscription payment";
        }
        return $msg;
        
    }
    function getContractStatus($user_type,$status,$freelance_status){
        if($status == 0){
            return "Open";
        }else if($status == 0 && $contract_status == 0){
            return "Open";
        }else if($status == 0 && $contract_status == 1){
            return "Signed";
        }
        else if($status == 1){
            return "Completed";
        }
    }
    function timeAgoConversion($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

    function getSettingValue($code){
        $value = Setting::getSettingValue($code);
        return $value;
    }
    function redirectBack($new_user,$change_pwd=null){
        if($new_user->step == 1){
            if($change_pwd == 1){
                return redirect()->route('user.contactInformation')->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
            }else{
                return redirect()->route('user.contactInformation');
            }
            
        }
        if($new_user->step == 2){
            if($change_pwd == 1){
                return redirect()->route('user.securityQuestions')->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
            }else{
                return redirect()->route('user.securityQuestions');
            }
            
        }else if($new_user->step == 3){
            if($change_pwd == 1){
                return redirect()->route('user.profileAcknowledgement')->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
            }else{
                return redirect()->route('user.profileAcknowledgement');
            }
            
        }else if($new_user->step == 4){
            if($change_pwd == 1){
                return redirect()->route('user.backgroundVerification')->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
            }else{
                return redirect()->route('user.backgroundVerification');
            }
            
        }else if($new_user->step == 5) {
            if($change_pwd == 1){
                return redirect()->route('account.createPublicProfile')->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
            }else{
                return redirect()->route('account.createPublicProfile');
            }
            
        }else if($new_user->step == 6){
            //get the subscription status
            $sub_conditions = [];
            $sub_conditions['user_id'] = $new_user->id;
            $subscription_status = UserSubscription::where($sub_conditions)->orderBy('id', 'DESC')->first();
           
            if($new_user->user_type == 0){
                if($change_pwd == 1){
                    return redirect('project/open-projects')->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
                }else{
                    return redirect('project/open-projects');
                }
                //if($subscription_status->active == 1){
                    return redirect('project/open-projects');
                // }else{
                //     return redirect()->route('membership_plan.membershipPlans');
                // }
            }else{
                if($change_pwd == 1){
                    return redirect('/project/invitations')->with('toastr', ['type'=>'success','text'=>'Password updated successfully',]);
                }else{
                    return redirect('/project/invitations');
                }
                
            }
        }

    }


//Set cookie function
if (! function_exists('cookie_set')) {

    /**
    * @param $str
    *
    * @return string
    */
    function cookie_set($key,$value)
    {
    Cookie::queue($key,$value);
    }
    }
    
    //Get cookie function
    if (! function_exists('cookie_get')) {
    
    /**
    * @param $str
    *
    * @return string
    */
    function cookie_get($key)
    {
    return Cookie::get($key);
    }
    }

    //destroy cookie function
if (! function_exists('cookie_delete')) {

    /**
    * @param $str
    *
    * @return string
    */
    function cookie_delete($key)
    {
    Cookie::queue(Cookie::forget($key));
    }
    }

    function getGivenRating($project_id,$type,$to_user_id){
       
        $user = auth('web')->user();
       
        $rating=UserRating::where('project_id',$project_id)->where('rated_by_user_id',$user->id);
        if($type==1)
        {
            $rating->where('rating_to_user_id',$to_user_id);
        }
        else{
            $rating->whereNull('rating_to_user_id');
        }
     $total_rating=  $rating->sum('rating');
    
     return $total_rating == 0 ? 0 : ($total_rating/$rating->count());
    
    }
    
    function getReceiveRating($project_id,$type,$by_user_id){
       
       
        $user = auth('web')->user();
       
        $rating=UserRating::where('project_id',$project_id)->where('rated_by_user_id',$by_user_id);
        if($type==1)
        {
            $rating->where('rating_to_user_id',$user->id);
        }
        else{
            $rating->whereNull('rating_to_user_id');
        }
     $total_rating=  $rating->sum('rating');
    
     return $total_rating == 0 ? 0 : ($total_rating/$rating->count());
    
    }