<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationPreference extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }

    public static function getNotificationStatus($user_id=0,$field=null){
        if(!empty($user_id) && !empty($field)){
            $result = NotificationPreference::select('id')->where([['user_id',$user_id],[$field,1]])->first();
            if(!is_null($result)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
