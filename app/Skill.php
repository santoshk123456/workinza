<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\General\Uuid;

class Skill extends Model
{
    use Uuid;

    protected $fillable = [
        'uuid', 'name', 'active'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }


    public function categories()
    {
        return $this->belongsToMany('App\Category','skill_category')->withPivot('skill_id','category_id')->withTimestamps();
    }

    public function industries()
    {
        return $this->belongsToMany('App\Industry','skill_industry')->withPivot('skill_id','industry_id')->withTimestamps();
    }

    public function subCategories()
    {
        return $this->belongsToMany('App\SubCategory','skill_sub_category')->withPivot('skill_id','sub_category_id')->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }
}
