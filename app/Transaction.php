<?php

namespace App;

use App\ProjectContract;
use App\ProjectMilestone;
use App\Models\Traits\General\Uuid;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use Uuid;

    protected $fillable = ['user_id','project_contract_id','project_milestone_id','project_id','user_subscription_id','type','amount','total_amount','service_percentage','service_charge','transaction_type','notes','payment_response','payment_status'];

/**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function contracts(){
        return $this->belongsTo(ProjectContract::class,'project_contract_id');
    }
    public function milestones(){
        return $this->belongsTo(ProjectMilestone::class,'project_milestone_id');
    }
}
