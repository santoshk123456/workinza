<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class SmsQueue extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'action_type','action_id','sent_status','message','mobile_number'
    ];

}