<?php

namespace App;

use App\Setting;
use App\EmailTemplate;
use App\ProjectMilestone;
use App\UserSubscription;
use App\Models\Traits\General\Uuid;
use App\Services\ProcessEmailQueue;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use Uuid;

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    protected $fillable = [
        'user_id', 'amount', 'type', 'due_date','notes','project_milestone_id','project_contract_id','status','user_subscription_id','paid_on'
    ];

    public function userSubscription(){
        return $this->belongsTo(UserSubscription::class,'user_subscription_id','id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function milestones(){
        return $this->belongsTo(ProjectMilestone::class,'project_milestone_id','id');
    }
    public function contracts(){
        return $this->belongsTo(ProjectContract::class,'project_contract_id','id');
    }

    /**
     * invoiceGeneration
     * STEP 1 : Insert an entry in step 1
     */
    public static function invoiceGeneration($input_data,$user=null){
        $invoice_sts = Invoice::create($input_data);
        if($invoice_sts){
            //send invoice email 
            $invoice_template = EmailTemplate::where('static_email_heading','INVOICE_GENERATION')->first();
            $admin_name = Setting::getSettingValue('application_admin_name');
            $admin_email = Setting::getSettingValue('application_admin_email');
            if ($invoice_template) {
                $email_html = $invoice_template->template;
                $email_html = str_replace("[USER_NAME]", ucwords($user->full_name), $email_html);
                $email_html = str_replace("[INVOICE_DATE]", $input_data['paid_on'], $email_html);
                $email_html = str_replace("[INVOICE_LINK]", route('finance.listInvoices'), $email_html);
                $email_html = str_replace("[AMOUNT]", formatAmount($input_data['amount']), $email_html);
                $message = "";
                if($input_data['type'] == 5){
                    $message = "An invoice is generated for the subscription plan"; 
                }else if($input_data['type'] == 6){
                    $message = "An invoice is generated for the background verification"; 
                }else if($input_data['type'] == 1){
                    $message = $input_data['notes'];
                }
                $email_html = str_replace("[MESSAGE]", $message, $email_html);
                $template = view('admin.email_templates.layout', compact('email_html'));
                $data = [];
                $data['to_email'] = $user->email;
                $data['to_name'] = ucwords($user->full_name);
                $data['from_email'] = $admin_email;
                $data['from_name'] = ucwords($admin_name);
                $data['subject'] = $invoice_template->subject;
                $data['body'] = $template;
                $data['action_id'] = $user->id;
                $data['action_type'] = 'invoice_generation';
            
                $processEmailQueue=new ProcessEmailQueue($data);
                $processEmailQueue->sendEmail();
            }
        }
    }
}
