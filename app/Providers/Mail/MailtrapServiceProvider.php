<?php

namespace App\Providers\Mail;

use Illuminate\Support\ServiceProvider;
use App\Services\Mail\Mailtrap;
use GuzzleHttp\Client;

class MailtrapServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    // protected $defer = true;
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Mailtrap::class, function ($app) {
            return new Mailtrap(new Client(['base_uri' => 'https://mailtrap.io']));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Mailtrap::class];
    }
}