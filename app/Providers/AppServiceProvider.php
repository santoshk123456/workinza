<?php

namespace App\Providers;
// use Laravel\\;
use App\Admin;
use App\Observers\AdminObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // ::auth(function ($request) {
        //     if(auth()->guard('admin')->check()){
        //         return true;
        //     } else {
        //          return false;
        //     }
            
        // });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Admin::observe(AdminObserver::class);
    }
}
