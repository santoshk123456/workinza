<?php

namespace App\Services\Mail;
use GuzzleHttp\Client;
use Spatie\ArrayToXml\ArrayToXml;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;



class ProcessEmailQueue
{

    public function __construct(Client $client)
    {
        $this->client= $client;
        $this->headers = [
            'Content-Type'  => 'text/xml',
            'Cache-Control' => 'no-cache',
            'Api-Token'     => 'fbf8902de2aa2474d4ef93d487b71b84',
        ];
        $this->response = false;
    }
    public function listAll(){
        $url = 'api/v1/inboxes/565118/messages';
        try {
            $res = $this->client->request('GET',$url, [
                'headers' => $this->headers]);
            $this->response = $res->getBody();
        } catch (RequestException | ConnectException | ClientException | ServerException $e) {
            if ($e->hasResponse()) {
                $this->response = $e->getResponse();
            }
        }
        return $this->response;
    }
    public function show($id=null){
        $url = 'api/v1/inboxes/565118/messages/'.$id;
        try {
            $res = $this->client->request('GET',$url, [
                'headers' => $this->headers]);
            $this->response = $res->getBody();
        } catch (RequestException | ConnectException | ClientException | ServerException $e) {
            if ($e->hasResponse()) {
                $this->response = $e->getResponse();
            }
        }
        return $this->response;
    }

}