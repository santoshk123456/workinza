<?php

namespace App\Services;
use App\Setting;
use App\EmailQueue;
use App\Jobs\SendEmail;


class ProcessEmailQueue
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    public function sendEmail(){
        $settings = Setting::whereIn('key',['application_admin_name','application_admin_email'])->get();
        if (!$settings->isEmpty()) {
            if ($settings->where('key', 'application_admin_email')->first() && $settings->where('key', 'application_admin_name')->first()) {
                if (isset($this->data['from_email']) && isset($this->data['from_name'])) {
                    $from_email = $this->data['from_email'];
                    $from_name = $this->data['from_name'];
                }
                else{
                    $from_email = $settings->where('key', 'application_admin_email')->first()->value;
                    $from_name = $settings->where('key', 'application_admin_name')->first()->value;
                }
                $data = [];
                $email_template_data = [];
                $this->data['from_email'] = $from_email;
                $this->data['from_name'] = $from_name;
                $this->data['sent_status'] = 0;
                $email = EmailQueue::create($this->data);
                SendEmail::dispatch($email);
            }
        }
    }
}