<?php

namespace App\Services;
use App\Setting;
use App\SmsQueue;
use App\Jobs\SendSms;


class ProcessSmsQueue
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }
    public function sendSms(){
        $data = [];
        $sms_template_data = [];
        $this->data['sent_status'] = 0;
        $sms = SmsQueue::create($this->data);
        SendSms::dispatch($sms)->onQueue('sms');
    }
}