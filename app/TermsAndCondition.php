<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermsAndCondition extends Model
{
    protected $fillable = [
        'heading', 'static_heading', 'slug', 'short_description', 'content', 'version'
    ];
}
