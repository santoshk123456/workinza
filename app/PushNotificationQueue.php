<?php

namespace App;
use App\User;
use App\PushNotificationQueue;
use App\NotificationPreference;
use App\PushNotificationTemplate;
use Illuminate\Database\Eloquent\Model;

//Test 
//define ( 'API_ACCESS_KEY', 'AAAAJ8m0aqs:APA91bHbbSeNPwM345Y0Y_kXd0KNEmkDvrIkFs8OclOsXBEPfNmfS7GcyjuINlU8e7pVvzrqi2crhThfhdg5uKuI1ob_af2oXcNV-UeNshixVunuRR4-QvV152k1n9ZET4ieYOSZ9haS' );
class PushNotificationQueue extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'action_type','action_id','device_type','device_id','sent_status','message','extra_datas','time_to_send'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * Function For Sending Notification
    **/

    public static function sendNotification($user=null,$action_type=null,$action_id=0,$fromUser=null,$datas=null,$sub_action_id=0){
      
        if(!empty($user) && !empty($action_type)){
           
            // for notification content

            $notification = PushNotificationQueue::getNotificationMessage($action_type);
           
            if(!empty($notification)){

            $notification_value = $notification->template;
            $subject = $notification->process_name;

             //checking notification preference 
             $preferenceStatus = NotificationPreference::getNotificationStatus($user['id'],'push_notification');

             if(!empty($preferenceStatus)){
              
               
                if(isset($action_type)&& $action_type=='USER_REGISTRATION'){
                
                    $replace_arr = array ('name' => ucfirst($user['full_name']));
                    $notification_body = PushNotificationQueue::notificationFormatting($notification_value,$replace_arr);
                  
                    $result = PushNotificationQueue::pushNotificationQueue($user,$action_type,$action_id,$subject,$notification_body,$sub_action_id);
                   
                   
                }
             }

            }
             
        }
    }

    //For getting notification template
	public static function getNotificationMessage($name)
	{
		$notification = PushNotificationTemplate::select(['process_name','template'])->where('process_name',$name)->first();
		return !is_null($notification) ? $notification : '';
    }
    
    //For formatting message
	public static function notificationFormatting($body=null,$replace_arr=[],$start_identifier='[',$end_identifier=']')
	{
    	//check the notification is empty
    	if (!empty($body)){
    		//check wether the repalce_arr is empty or not
    		if (!empty($replace_arr)){
    			foreach ($replace_arr as $key => $replace) {
    				//replace the dynamic value
    				$search = $start_identifier . $key . $end_identifier;
    				$body = str_ireplace($search,$replace,$body);
    			}
    		}
    	}
    	//return the notification body
    	return $body;
    }

    
    public static function pushNotificationQueue($user=null,$action_type,$action_id=null,$subject=null,$message=null,$sub_action_id=null,$deviceId=null){
        if (isset($user['id']) && !empty($user['id'])) {
                $devices = [];
                
                $devices = PushNotificationQueue::getUserDevices($user);
                
                // if(!empty($devices)){
                   
                    // foreach ($devices as $key => $device) {
                        $pushNotification = new PushNotificationQueue();
                        $pushNotification['user_id'] = $user['id'];
                        // for test
                        $pushNotification['device_id'] = 123;
                        // $pushNotification['device_id'] = $device['device_id'];
                        // $pushNotification['device_type'] = $device['device_type'];
                        $pushNotification['device_type'] = 2;
                        $pushNotification['action_type'] = $action_type;
                        $pushNotification['action_id'] = $action_id;
                        if(!empty($sub_action_id)){
                            $pushNotification['sub_action_id'] = $sub_action_id; 
                        }else{
                            $pushNotification['sub_action_id'] =0;
                        }
                        $pushNotification['extra_datas'] = "";
                        $pushNotification['sent_status'] = 0;
                        $pushNotification['subject'] = $subject;
                        $pushNotification['message'] = $message;
                        $pushNotification->save();
                    // }
                // }
            
        }
    }


    //For getting notification template
    public static function getUserDevices($user)
    {
        $devices = [];
        if(!empty($user)){
            if(!empty($user['android_deviceid'])){
                $devices[0]['device_id'] = $user['android_deviceid'];
                $devices[0]['device_type'] = 'android';
            }
            if(!empty($user['ios_deviceid'])){
                $devices[1]['device_id'] = $user['ios_deviceid'];   
                $devices[1]['device_type'] = 'ios';
            }
        }
        return $devices;
    }


    //For sending push notifications to android devices
    public static function pushNotificationAndroid($id=null,$userid=null,$deviceid=null,$subject=null,$message=null,$action_type=null,$action_id=0,$sub_action_id=0,$count=0) {
        try {
            $userType = [];
            $userType = ['USER_REGISTRATION'];

            $channel = '';
            $result = [];
            if(!empty($action_type)){
                $data = [];
                if(in_array($action_type,$userType)){
                    $channel = 'Users';
                    $key = array_search($action_type, $userType);
                    $channelId = 1;
                }

                $push_data = array(
                    "id" => 1,
                    "title" => "TEST",
                    "message" => $message,
                    "channel_id" => $channelId,
                    "channel" => $channel,
                    "channel_description" => $subject,
                    'data_id' => $key,
                    'notification_data' => $data,
                    'badgecount' => $count,
                    'vibrate' => 0,
                    'sound' => 0,
                );

                $fields = array(
                            "to" => $deviceid,
                            "priority" => "high",
                            "data" => $push_data,
                );
                
                $headers = array (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );
                
                $ch = curl_init ();
                curl_setopt ( $ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt ( $ch, CURLOPT_POST, true );
                curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
                curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
                curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
                 
                $result = curl_exec ( $ch );
                curl_close ( $ch );
                return $result;
            }else{
                return false;
            }
        } catch ( Exception $e ) {
            $error = print_r ( $e, true );
            return $error;
        }
    }


    //For sending push notifications to ios devices
    public static function pushNotificationIphone($id=null,$userid=null,$device_id=null,$subject=null,$message=null,$action_type=null,$action_id=0,$sub_action_id=0,$count=0) {
    	try {
    		$deviceToken = $device_id;
            // Put your private key's passphrase here:
            $passphrase = '123456';
    			
    		$ctx = stream_context_create ();

    		stream_context_set_option ( $ctx, 'ssl', 'passphrase', $passphrase );
    		// Open a connection to the APNS server
            $fp = stream_socket_client ( 'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx );
            
    		if (! $fp){
    			return ( "Failed to connect: $err $errstr" . PHP_EOL );
    		}else{
                $user_version = User::where('id',$userid)->first();
                
                    $body ['aps'] = array (
                        'alert' => $message,
    					'sound' => 'default',
    					'type' => $action_type,
    					'thread_id' => $action_id,
    					'badge' => $count,
                        'sub_thread_id' => $sub_action_id,
                        'content-available' => 0
                    );
                
    			// Encode the payload as JSON
    			$payload = json_encode ( $body );
    			// Build the binary notification
    			$msg = chr ( 0 ) . pack ( 'n', 32 ) . pack ( 'H*', $deviceToken ) . pack ( 'n', strlen ( $payload ) ) . $payload;
    			// Send it to the server
    			$result = fwrite ( $fp, $msg, strlen ( $msg ) );
    			if (!$result){
    				return $result;
    			}else{
    				// Close the connection to the server
    				fclose ( $fp );
    				return $result;
    			}
    		}
    	} catch ( Exception $e ) {
    		$error = print_r ( $e, true );
    		return $error;
    	}
    }

}