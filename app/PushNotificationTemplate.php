<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class PushNotificationTemplate extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'process_name','description','template',
    ];

}