

function generateFirebaseInstant(id){
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(id, {
        'size': 'invisible',
        'callback': function(response) {
            // reCAPTCHA solved, allow signInWithPhoneNumber.
            onSignInSubmit();
        }
    });
}

function sendFirebaseOTP(phone){
    var appVerifier = window.recaptchaVerifier;
    firebase.auth().signInWithPhoneNumber(phone, appVerifier)
        .then(function (confirmationResult) {
            // SMS sent. Prompt user to type the code from the message, then sign the
            // user in with confirmationResult.confirm(code).
            window.confirmationResult = confirmationResult;
        }).catch(function (error) {
            // Error; SMS not sent
            // ...
        });
}

// $(document).ready(function () {
//     // Success popup message
//     if(flash_success != ''){
//         showToast('Success', flash_success, 'success');
//     }

//     // Error popup message
//     if(flash_error != ''){
//         showToast('Error', flash_error, 'error');
//     }
// });


/* Show Success Message*/
showToast = function(heading, text, icon, background) {
    resetToastPosition();
    $.toast({
        heading: (heading == '')?'Success':heading,
        text: text,
        showHideTransition: 'slide',
        icon: (icon == '')? 'success':icon,
        loaderBg: (background == '')?'#f96868':background,
        position: 'top-right',
        hideAfter: 7000
    });
};


/* Toaster Reset Position*/
resetToastPosition = function() {
  $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
  $(".jq-toast-wrap").css({
    "top": "",
    "left": "",
    "bottom": "",
    "right": ""
  }); //to remove previous position style
}

$(".numericOnly").keypress(function (e) {
    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
});
$(document).ready(function () {
$(".user-rating").each(function (e) {
    var rateValue = $(this).attr('data-value');
    $(this).rateYo({
        rating: rateValue,
        numStars: 5,
        starWidth: "15px",
        spacing: "0px",
        readOnly: true,
    });
});
});



  

