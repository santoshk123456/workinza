<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],
    // 'facebook' => [
    //     'client_id' => env('FACEBOOK_APP_ID'),
    //     'client_secret' => env('FACEBOOK_APP_SECRET'),
    //     'redirect' => 'https://localhost:8000/oauth/complete/facebook/',
    // ],
    'google' => [
        'client_id'     => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect'      => env('GOOGLE_REDIRECT')
    ],
    'facebook' => [
        'client_id' => '658720744670438',
        'client_secret' => 'ee62780e97533d00278afcbdcdf0e61d',
        'redirect' => 'http://localhost:4007/auth/facebook/callback',      
      ],
      'twitter' => [
        'client_id' => 'rJikKVDqrwq9nGtv5rTCYFytI',
        'client_secret' => 'JuU4p9ch38wSQc4ogUKIVo8ONlDzy9BDN0sNIya3z7hvs29r3J',
        'redirect' => 'http://192.168.2.109:4007/auth/twitter/callback',
    ],
    'github' => [
        'client_id' => env('GITHUB_ID'),
        'client_secret' => env('GITHUB_SECRET'),
        'redirect' => env('GITHUB_URL'),
    ],

];
