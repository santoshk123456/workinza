<?php return array (
  'default_date_format' => 'd-m-Y',
  'timezones' => 
  array (
    0 => 
    array (
      'value' => -12,
      'text' => '(GMT -12:00) Eniwetok, Kwajalein',
      'timezone' => 'Pacific/Fiji',
    ),
    1 => 
    array (
      'value' => -11,
      'text' => '(GMT -11:00) Midway Island, Samoa',
      'timezone' => 'US/Samoa',
    ),
    2 => 
    array (
      'value' => -10,
      'text' => '(GMT -10:00) Hawaii',
      'timezone' => 'US/Hawaii',
    ),
    3 => 
    array (
      'value' => -9,
      'text' => '(GMT -9:00) Alaska',
      'timezone' => 'US/Alaska',
    ),
    4 => 
    array (
      'value' => -8,
      'text' => '(GMT -8:00) Pacific Time (US & Canada)',
      'timezone' => 'US/Pacific',
    ),
    5 => 
    array (
      'value' => -7,
      'text' => '(GMT -7:00) Mountain Time (US & Canada)',
      'timezone' => 'US/Mountain',
    ),
    6 => 
    array (
      'value' => -6,
      'text' => '(GMT -6:00) Central Time (US & Canada], Mexico City',
      'timezone' => 'US/Central',
    ),
    7 => 
    array (
      'value' => -5,
      'text' => '(GMT -5:00) Eastern Time (US & Canada], Bogota, Lima',
      'timezone' => 'US/Eastern',
    ),
    8 => 
    array (
      'value' => -4,
      'text' => '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz',
      'timezone' => 'Canada/Atlantic',
    ),
    9 => 
    array (
      'value' => -3.5,
      'text' => '(GMT -3:30) Newfoundland',
      'timezone' => 'Canada/Newfoundland',
    ),
    10 => 
    array (
      'value' => -3,
      'text' => '(GMT -3:00) Brazil, Buenos Aires, Georgetown',
      'timezone' => 'America/Buenos_Aires',
    ),
    11 => 
    array (
      'value' => -2,
      'text' => '(GMT -2:00) Mid-Atlantic',
      'timezone' => 'Atlantic/Stanley',
    ),
    12 => 
    array (
      'value' => -1,
      'text' => '(GMT -1:00) Azores, Cape Verde Islands',
      'timezone' => 'Atlantic/Azores',
    ),
    13 => 
    array (
      'value' => 0,
      'text' => '(GMT) Western Europe Time, London, Lisbon, Casablanca',
      'timezone' => 'Europe/London',
    ),
    14 => 
    array (
      'value' => 1,
      'text' => '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris',
      'timezone' => 'Europe/Brussels',
    ),
    15 => 
    array (
      'value' => 2,
      'text' => '(GMT +2:00) Kaliningrad, South Africa',
      'timezone' => 'Asia/Jerusalem',
    ),
    16 => 
    array (
      'value' => 3,
      'text' => '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg',
      'timezone' => 'Asia/Baghdad',
    ),
    17 => 
    array (
      'value' => 3.5,
      'text' => '(GMT +3:30) Tehran',
      'timezone' => 'Asia/Tehran',
    ),
    18 => 
    array (
      'value' => 4,
      'text' => '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi',
      'timezone' => 'Asia/Muscat',
    ),
    19 => 
    array (
      'value' => 4.5,
      'text' => '(GMT +4:30) Kabul',
      'timezone' => 'Asia/Kabul',
    ),
    20 => 
    array (
      'value' => 5,
      'text' => '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent',
      'timezone' => 'Asia/Karachi',
    ),
    21 => 
    array (
      'value' => 5.5,
      'text' => '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi',
      'timezone' => 'Asia/Kolkata',
    ),
    22 => 
    array (
      'value' => 5.75,
      'text' => '(GMT +5:45) Kathmandu',
      'timezone' => 'Asia/Kathmandu',
    ),
    23 => 
    array (
      'value' => 6,
      'text' => '(GMT +6:00) Almaty, Dhaka, Colombo',
      'timezone' => 'Asia/Almaty',
    ),
    24 => 
    array (
      'value' => 7,
      'text' => '(GMT +7:00) Bangkok, Hanoi, Jakarta',
      'timezone' => 'Asia/Bangkok',
    ),
    25 => 
    array (
      'value' => 8,
      'text' => '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong',
      'timezone' => 'Asia/Hong_Kong',
    ),
    26 => 
    array (
      'value' => 9,
      'text' => '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk',
      'timezone' => 'Asia/Tokyo',
    ),
    27 => 
    array (
      'value' => 9.5,
      'text' => '(GMT +9:30) Adelaide, Darwin',
      'timezone' => 'Australia/Adelaide',
    ),
    28 => 
    array (
      'value' => 10,
      'text' => '(GMT +10:00) Eastern Australia, Guam, Vladivostok',
      'timezone' => 'Pacific/Guam',
    ),
    29 => 
    array (
      'value' => 11,
      'text' => '(GMT +11:00) Magadan, Solomon Islands, New Caledonia',
      'timezone' => 'Pacific/Guadalcanal',
    ),
    30 => 
    array (
      'value' => 12,
      'text' => '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka',
      'timezone' => 'Pacific/Fiji',
    ),
  ),
  'currency_symbol' => 'USD ',
);