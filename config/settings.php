<?php

return [

    /*
    |--------------------------------------------------------------------------
    | FAQ Settings
    |--------------------------------------------------------------------------
    |
    |The list of types in FAQ settings
    |
    */

    'faq' => [
        'types' => [
            'user', 'admin','guest'
        ],
    ],
    'settings' => [
        'types' => [
            'general','mobile'
        ],
    ],

];
