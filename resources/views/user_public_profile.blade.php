<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage">

<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <title>User Profile | Keyoxa</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#233D63">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#233D63">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#233D63">
    <!-- favicons -->
    @include('user.layouts.includes.head_one')
    <!-- fonts -->
</head>
<body itemscope="" itemtype="http://schema.org/WebPage">
    <!--Start: Browser update alert -->
    <!--[if lt IE 10]>
           <div class="browser-update">
           <div class="update-info-wrap">
           <img src="./images/logo.svg" alt="">
           <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
           </div>
            </div>
    <![endif]-->
    <!-- End: Browser update alert -->
          <!-- Header:start -->
          <header class="main-header">          
            <div class="hdr-wrapper">                                      
                <nav class="navbar navbar-expand-lg main-navbar navbar-light">
                  <div class="container">  
                    <a class="navbar-brand" href="#"><img src={{asset('/images/user/150_fordark_bg.png')}} alt="Keyoxa-logo"></a>              
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target=".mobcollapse" aria-label="Toggle navigation">
                      <span> </span>
                      <span> </span>
                      <span> </span>
                    </button>                  
                    <div class="collapse navbar-collapse mobcollapse">
                      {{-- <form class="form-inline menu-search">
                        <input class="form-control" type="search" placeholder="Search by title" aria-label="Search by title">
                        <button class="btn btn-search" type="submit"><span class="sprites-col search-icon"></span></button>
                      </form> --}}
                      <ul class="navbar-nav px-0 ml-auto logged-in">                   
                        <li class="nav-item">
                          <a class="nav-link" href="{{route('userPublicProfile','how-it-works')}}">How it works?</a>
                        </li>    
                        @auth('web')                
                        <li class="nav-item dropdown"> 
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  @if(!empty(auth('web')->user()->profile_image) && (file_exists( public_path().'/storage/admin/profile-images/'.auth('web')->user()->profile_image)))
                        @php
                            $image_with_path = 'storage/admin/profile-images/'.auth('web')->user()->profile_image;
                            $url = thump_resize_image($image_with_path,32,32);
                        @endphp
                        <img src="{{ $url }}" alt="user-image" class="rounded-circle mr-2">
                    @else 
                        @php
                            $image_with_path = 'images/admin/starter/default.png';
                            $url = thump_resize_image($image_with_path,32,32);
                        @endphp
                        <img src="{{ $url }}" alt="user-image" class="rounded-circle mr-2">
                    @endif </span>My Account
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @if(auth('web')->user()->step >= 6)
                     <a class="dropdown-item">  {{ ucfirst(auth('web')->user()->full_name) }}</a>
                    @endif
                     <a class="dropdown-item" href="{{route('logout')}}">  Logout</a>

                    <!-- <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div> 
                    <a class="dropdown-item" href="#">Something else here</a> -->
                  </div>
                </li>   
                <!-- <li class="nav-item"> <button class="btn btn-post my-2 my-sm-0" type="submit"><span class="sprites-col post-icon"></span>Post a Project</button></li>                   -->
                @if(auth('web')->user()->user_type == 0 && auth('web')->user()->step == 6)
                <li class="nav-item mt-3"> <a class="btn btn-post my-2 my-sm-0" type="button" href="{{ route('project.createProject') }}"><i class="fi-flaticon flaticon-project mr-2"></i>Post a Project</a></li>
               
                @endif
                @if(auth('web')->user()->user_type == 1 && auth('web')->user()->step == 6)
                <li class="nav-item mt-3"> <a class="btn btn-post my-2 my-sm-0" type="button" href="{{ route('project.findProject') }}"><i class="fi-flaticon flaticon-project mr-2"></i>Find a Project</a></li>
               
                @endif
                @endauth  
                    </ul>                  
                    </div>
                  </div>
                </nav>
              
                <nav class="navbar navbar-expand-lg sub-menus">                
                  <div class="container">  
                  <div class="collapse navbar-collapse mobcollapse">
                    <ul class="navbar-nav inner-pg-menu">
                        @auth('web')
                        @if(auth('web')->user()->step >= 3)
                        <li class="nav-item @if(Request::is('dashboard') ) active @endif">
                          <a class="nav-link" href="{{route('user.viewdashboard')}}">Dashboard</a>
                          </li>
                        @endif
                        @if(auth('web')->user()->step >= 3)
                        <li class="nav-item active">
                            <a class="nav-link" @if(auth('web')->user()->step >= 3) href="{{route('user.viewContactInformation')}}" @else href="" @endif">Profile</a>
                        </li>
                        @endif
                        <li class="nav-item @if(Request::is('project*') ) active @endif">
                  @if(auth('web')->user()->user_type == 0)
                    @if(auth('web')->user()->step == 6)
                      <a class="nav-link" href="{{route('project.listOpenProjects')}}">Projects</a>
                    @else
                      <a class="nav-link" href="">Projects</a>
                    @endif
                  @else 
                    @if(auth('web')->user()->step == 6)
                      <a class="nav-link" href="{{route('project.listInvitations')}}">Projects</a>
                    @else
                      <a class="nav-link" href="">Projects</a>
                    @endif
                  @endif
              </li> 
              <li class="nav-item @if(Request::is('finance*') ) active @endif">
                @if(auth('web')->user()->user_type == 1)
                  <a class="nav-link" href="{{route('finance.listTransactions')}}">Finances</a>
                @else
                  <a class="nav-link" href="{{route('finance.listTransactions')}}">Finances</a>
                @endif
              </li> 
              <li class="nav-item">
                <a class="nav-link" href="#">Messages</a>
              </li>   
              @else
           
              <li class="nav-item @if(Request::is('register*') || Request::is('membership-plan*') || Request::is('account*') ) active @endif">
                <a class="nav-link" @if($user->step >= 3) href="{{route('userPublicProfile',$user->username)}}" @else href="" @endif"></a>
            </li>
           
                      @endauth                 
                    </ul>               
                  </div>
                </div>
              </nav>           
          </div>

        </header>
        <!-- Header:end -->
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout">
    <h3 class="title pl-0 mb-4">@if(!empty($user->userDetail->profile_headline)){{$user->userDetail->profile_headline}}@else NA @endif </h3>
      <div class="freelancer-view-details">    
          <div class="row mb-3">
            <div class="col-lg-9 pr-lg-0">
              <div class="col-list-item bg-shadow">                    
                <div class="media text-center text-md-left d-block d-md-flex">
                @if(isset($user->profile_image) && (file_exists( public_path().'/storage/user/profile-images/'.$user->profile_image)))
                    <img class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail"  src="{{ asset('storage/user/profile-images/'.$user->profile_image) }}" alt="{{ $user->username }}">
                                @else 
                    <img src="{{ asset('images/admin/starter/default.png') }}"  alt="profile"
                                            class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                @endif
                  <div class="media-body">
                    <div class="row">
                      <div class="col-md-7 mb-2 mb-md-0">
                        <h5 class="mt-0 name">@if(!empty($user->username)){{$user->username}}@else NA @endif</h5>
                        <h6 class="designation">@if(!empty($user->userDetail->profile_headline)){{$user->userDetail->profile_headline}}@else NA @endif</h6>
                        <div class="ratings"><div class="rate-count">@if(!empty($user->userDetail->avg_rating)){{$user->userDetail->avg_rating}} @else 0 @endif</div> <div class="user-rating" data-value="@if(!empty($user->userDetail->avg_rating)){{$user->userDetail->avg_rating}}@else 0 @endif"></div></div>
                        <span class="industry">{{ !empty($user->industries) ? implode(', ', $user->industries->pluck('name')->toArray()) : __('NA') }}</span>
                        <p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>@if(!empty($user->userDetail->city)){{ $user->userDetail->city }}, {{ $user->userDetail->state }}, {{ $user->userDetail->country->name }} @endif</p>
                      </div>
                      <div class="col-md-5 mb-2 mb-md-0">
                        <div class="list-right-aside">
                          <!-- <button class="btn bg-nm-btn mr-3" type="submit"><i class="fi-flaticon flaticon-send mr-2"></i>Invite</button>
                          <button class="btn white-nm-btn" type="submit"><i class="fi-flaticon flaticon-comments mr-2"></i>Message</button> -->
                      </div>
                      </div>
                    </div>           
                  </div>  
                </div>
              </div>    
            </div>
            <div class="col-lg-3">
              <div class="bg-shadow project-proposals-col">
                <ul class="d-block">
                  <li><p>Years of Experienece</p><span>@if(!empty($user->userDetail->years_of_experience)){{$user->userDetail->years_of_experience}}@else NA @endif</span></li>
                  <li><p>Projects in Progress</p><span>4</span></li>
                  <li><p>Projects Delivered</p><span>24</span></li>
                  <li><p>Total Earnings</p><span>$7000</span></li>
                  <li><p>Active Since</p><span>{{ !empty($user->created_at)?hcms_date(strtotime($user->created_at),'date'):'NA' }}</span></li>
                </ul>
              </div>
            </div>
          </div>        
          <div class="seperated-cols bg-shadow mb-3 feature-skills">
            <h6 class="">About</h6>   
            <p class="mb-2">@if(!empty($user->userDetail->about_me)){{$user->userDetail->about_me}}@else NA @endif</p>                     
             <!-- <p class="mb-2">I have more than 7 years of experience in designing meaningful, user-centered and functional UI and in improving UX across the web and mobile All that using the whole pile of a designing tool such as Figma, Sketch, Balsamiq, Adobe Photoshop CC, Adobe Illustrator CC.</p> -->
          </div>
          @if(!empty($user->skills) && count($user->skills))
            <div class="seperated-cols bg-shadow mb-3 feature-skills">
                <h6 class="">Skills and Expertise</h6>   
                <div class="skils-cols">
                  
                  
                  @foreach($user->skills->pluck('name') as $skill)
                    <span class="notify-col">{{$skill}}</span>
                  @endforeach
                 
                
            
                </div>                     
            </div>
            @endif
            @if(!empty($user->categories) && count($user->categories))
              <div class="row">
                <div class="col-md-6 pr-md-0">
                  <div class="seperated-cols bg-shadow mb-3 feature-skills">
                    <h6 class="">Category</h6>   
                    <div class="skils-cols">
                      @foreach($user->categories->pluck('name') as $category)
                      <span class="notify-col">{{$category}}</span>
                    @endforeach
                  
                    </div>                     
                </div>
                </div>
                <div class="col-md-6">
                  <div class="seperated-cols bg-shadow mb-3 feature-skills">
                    <h6 class="">Sub Category</h6>   
                    <div class="skils-cols">
                      @foreach($user->subCategories->pluck('name') as $subCategories)
                      <span class="notify-col">{{$subCategories}}</span>
                    @endforeach
                   
                    <!-- <span class="notify-col">WordPress</span>          -->
                    </div>                     
                </div>
                </div>
              </div>
            @endif
        <!-- <div class="seperated-cols bg-shadow mb-3">
          <h6 class="">Recent Top Projects</h6>   
          <ul>
            <li>Banking Penetarion Test</li>
            <li>Donec facilisis ligula et ornare luctus. In pretium sit amet est ac dapibus. </li>
            <li>Banking Penetarion Test</li>
            <li>Donec facilisis ligula et ornare luctus. In pretium sit amet est ac dapibus. </li>
          </ul>                    
        </div> -->
        @if(isset($user->userDetail->video_urls) && !empty($user->userDetail->video_urls))  
        <div class="seperated-cols bg-shadow mb-3">
          <h6 class="">Videos</h6> 
       
          <ul class="videos-listing">                  
          @php
         $videos = json_decode($user->userDetail->video_urls);
         @endphp

 @if(!empty($videos))
     @foreach ($videos as $video)
            {{-- <li>
              <video width="70" height="70" controls>
                <source src="{{$video}}" type="video/mp4">             
              </video>
            </li> --}}
            @php
            $video_extract = explode("v=",$video);
            $embed_code = "";
            if(isset($video_extract[1])){
              $embed_code = explode("&",$video_extract[1]) ;
            }
          @endphp
          @if(isset($embed_code[0]))
          <li>
           
          
          <iframe width="100" height="100" src="https://www.youtube.com/embed/{{$embed_code[0]}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </li>
          @endif
       @endforeach
                @else
                 <li>{{__('NA')}}  </li>
                @endif
           
          </ul>  
        </div>
        @endif  
        <!-- <div class="seperated-cols bg-shadow mb-3">
          <h6 class="">Documents</h6>
          <div class="document-col my-3">
            <span class="file-name"><i class="sprites-col acrobit-icon mr-2"></i>corporate presentation.pdf</span>
            <div class="btns-col">           
              <button class="btn white-nm-btn" type="submit"><i class="fi-flaticon flaticon-download"></i></button>       
            </div>
          </div>     
        </div> -->
        @if(isset($ratings) && count($ratings) > 0)
        <div class="seperated-cols bg-shadow mb-3 ratings-cols profile-rating-col">
          <h6 class="rating-title">All Ratings </h6>
          @foreach($ratings as $rating)
          <div class="media text-center text-md-left d-block d-md-flex mt-3">
            @if($rating->rated_by->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$rating->rated_by->profile_image)))
            <img  class="mr-md-3 thumb mb-3 mb-md-0"  src="{{ asset('storage/user/profile-images/'.$rating->rated_by->profile_image) }}"  alt="{{ $rating->rated_by->username }}">
            @else 
            <img class="mr-md-3 thumb mb-3 mb-md-0"  src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $rating->rated_by->full_name }}" class="mr-md-3 thumb mb-3 mb-md-0">
            @endif
           
            
            <div class="media-body">    
              <h5 class="mt-0 name">{{ucwords($rating->rated_by->username)}}</h5>
              <h5 class="mt-0 name">{{__('Project')}} : {{ucwords($rating->project->title)}}</h5>
              <div class="ratings mb-2"><div class="rate-count">@if(!empty($rating->rating)){{$rating->rating}}@else 0 @endif</div> <div class="user-rating" data-value="@if(!empty($rating->rating)){{$rating->rating}} @else 0 @endif"></div></div>
              @php $review=App\UserReview::where('reviewed_to_user_id',$user->id)->where('project_id',$rating->project->id)->first(); @endphp
             
              <h6 class="designation">@if(!empty($review->review_user)){{$review->review_user}} @else NA @endif</h6>
          </div>   
          </div>  
          @endforeach  
        </div> 
        @endif
      </div> 
  </div> 
  </div>
</main>

@include('user.layouts.includes.footer_one')
<script src="{{asset('js/user/bundle.min.js')}}"></script> 
</body>
</html>