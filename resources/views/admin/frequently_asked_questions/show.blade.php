@extends('admin.layouts.app')
@section('title', set_page_titile(__('Admin Users')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('admin_users.show',$admin) }}
            </div>
            <h4 class="page-title">{{ ucwords($admin->roles()->first()->name)."'s Details"}}</h4>
        </div>
    </div>
</div>
@include('admin.includes.profile.profile_head')
<div class="row">
    @include('admin.includes.profile.tab_head')
    <div class="col-md-8">
        <!-- Personal-Information -->
        <div class="card">
            <div class="card-body">
                <div class="tab-content" id="v-pills-tabContent">
                    @include('admin.includes.profile.tabs.profile')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
