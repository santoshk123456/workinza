@extends('admin.layouts.app')
@section('title', set_page_titile(__('Edit New Frequently Asked Question')))
@push('custom-styles')
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('frequently_asked_questions.edit',$frequentlyAskedQuestion) }}
            </div>
            <h4 class="page-title">{{ __('Edit') }} @if(!empty($type)){{ ucwords($type) }} @else {{ __('User') }} @endif {{ __(' Frequently Asked Question') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('admin.frequently_asked_questions.update',[$frequentlyAskedQuestion->id,$type])}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-lg-6 @if ($errors->has('question')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom01">{{ __('Question') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ $frequentlyAskedQuestion->question }}" name="question" class="form-control" id="validationCustom01" placeholder="Question" required>
                                <input type="hidden" value="{{ $frequentlyAskedQuestion->type }}" name="type">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('question'))
                                        {{ $errors->first('question') }}
                                    @else 
                                        {{ __('Question cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group active-checkbox">
                                <label for="validationCustom01" class="d-block mt-0">{{ __('Status') }}</label>
                                <input name="active" type="checkbox" id="switch3" @if(!empty($frequentlyAskedQuestion->active)) checked @endif data-switch="success">
                                <label for="switch3" data-on-label="Active" data-off-label="Inactive" class="mt-0"></label>
                            </div>
                        </div>
                        <div class="col-lg-12 @if ($errors->has('answer')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom01">{{ __('Answer') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <textarea data-toggle="maxlength" class="form-control" maxlength="225" rows="3" placeholder="Answer..." name="answer" required>{{ $frequentlyAskedQuestion->answer }}</textarea>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('answer'))
                                        {{ $errors->first('answer') }}
                                    @else 
                                        {{ __('Answer cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div> 
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

@endpush
