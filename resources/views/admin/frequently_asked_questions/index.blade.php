@extends('admin.layouts.app')
@section('title', set_page_titile(__('Admin Users Listing')))
@push('custom-styles')
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('frequently_asked_questions') }}
            <h4 class="page-title text-capitalize">{{ $type }} {{ __('FAQ') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card p-0 m-0">
            <div class="card-body p-0 m-0">
                @include('admin.includes.faq.tab_head')
                <div class="tab-content p-3">
                    @include('admin.includes.faq.tab')
                </div>
            </div>
            <!-- end card-body -->
        </div>
        <!-- end card-->
    </div>
    <!-- end col -->
</div>
<!-- end row -->
@endsection
@push('custom-scripts')
<script>


</script>
@endpush
