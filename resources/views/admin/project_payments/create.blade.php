@extends('admin.layouts.app')
@section('title', set_page_titile(__(Project Payment Create')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{ Breadcrumbs::render('email_templates.create') }}
            </div>
            <h4 class="page-title">{{ __('Create Email Template') }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.invoices.store')}}"
            class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
            @csrf
            <div class="row">
                <div class="col-6 mb-3 @if ($errors->has('static_email_heading')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom01">{{ __('User') }}</label>
                        <select name="type" class="form-control" id="type" required>
                            <option value="" selected disabled>Select user</option>
                            @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->full_name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('static_email_heading'))
                        <div class="invalid-feedback">
                            {{ $errors->first('static_email_heading') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-6 mb-3 @if ($errors->has('type')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom02">{{ __('Type') }}</label>
                        <select name="type" class="form-control" id="type" required>
                            <option value="" selected disabled>Select Type</option>
                            <option value="0">Abrevation</option>
                            <option value="1">Project Payment</option>
                            <option value="2">Cancellation</option>
                            <option value="3">Refund</option>
                            <option value="4">Bonus</option>
                            <option value="5">Subscription Payment</option>
                        </select>
                        @if ($errors->has('type'))
                        <div class="invalid-feedback" >
                            {{ $errors->first('type') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-6 mb-3 @if ($errors->has('amount')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom03">{{ __('Amount') }}</label>
                        <input type="text" name="amount" class="form-control" id="amount" required>
                        @if ($errors->has('amount'))
                        <div class="invalid-feedback" >
                            {{ $errors->first('amount') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-6 mb-3 @if ($errors->has('static_email_heading')) validation-failed @endif d-none">
                    <div class="form-group">
                        <label for="validationCustom01">{{ __('User') }}</label>
                        <select name="type" class="form-control" id="type" required>
                            <option value="" selected disabled>Select user</option>
                            @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->full_name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('static_email_heading'))
                        <div class="invalid-feedback">
                            {{ $errors->first('static_email_heading') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-12 text-right px-0">
                    <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                    <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                </div> 
            </div>
        </form>
    </div>
</div>
@endsection

