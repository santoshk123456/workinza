@extends('admin.layouts.app')
@section('title', set_page_titile(__('Project Payments')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('project_payments') }}
            <h4 class="page-title">{{ __('Project Payments') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

            <div class="row mb-0">
                            <div class="col-sm-4">
                            </div>         
                            <div class="col-sm-8 mb-2 m-0 filter-control">
                                <button type="button" data-target="#filter" data-toggle="collapse" class="btn btn-sm btn-primary float-right filter"><i class="mdi mdi-filter"></i> Filter</button>
                                <button type="button" class="btn btn-sm btn-success btn mb-3 mr-2 float-right" data-toggle="modal" data-target="#myModal"><span class="mdi mdi-file-export"></span>
                            Export</button>
                            </div>     
                        
                                <div id="filter" class="collapse mb-1">
                                    <div class="row">

                                        <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('Date') }}
                                            </label>
                                            <input type="text" name="daterange" id="daterange_id" class="input-text with-border form-control readonly" style="caret-color: transparent;" placeholder="Select date range" required/>
                                        </div>
                                        <div class="col-lg-2">
                                            <label for="validationCustom01">{{ __('Milestone') }}
                                            </label>
                                            <select name="milestone" id="milestone" class="form-control">
                                                <option value="" selected disabled>Select Milestone</option>
                                                @foreach($milestones as $key=>$value)
                                                <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                
                                        
                                        <div class="col-sm-6 mt-3">
                                            <button type="button" class="btn btn-danger mb-3 mr-1 filter_button" name="exp_excel" value="search"> {{ __('Search') }}</button>
                                            <!-- <button type="button" class="btn btn-primary mt-3 filter_button filter-button"  title="Apply"><i class="mdi mdi-check"></i> Apply</button> -->
                                        </div>
                                    </div>
                                </div>
                               
                </div>
                {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                <div class="col-12 text-right px-0">
                    <div class="form-group row">
                        <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                        <div class="col-sm-1 mt-1">
                            <input type="text" id="pageNum" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title text-center" id="exampleModalLongTitle">Choose the fields to export</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
     <div class="modal-body">

              
                <button type="button" class="close position-absolute" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="form-check slct-all ">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input " id="blk_dt_chkall" onchange="chk_all_box();" checked> Select All <i class="input-helper"></i></label>
                </div>
            </div>
            <div class="modal-body py-0 mt-2">
                <div class="row cust_draw_table">
                
               
                    <div class="col-lg-6">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input data-column-name="User" type="checkbox" class="form-check-input" name="user_name" value="user_name" checked>User <i
                                    class="input-helper"></i></label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input data-column-name="Milestone" type="checkbox" class="form-check-input" name="milestone_name" value="milestone_name" checked>Milestone <i
                                    class="input-helper"></i></label>
                        </div>
                        @foreach (array_chunk($more_col,6) as $dat)
                        @foreach ( $dat as $key => $value) 
                            @php
                            $alias_table= ($value);
                            $new_value = ucwords(str_replace('_', ' ', $value));
                            @endphp
                            
                           
                        <div class="form-check">
                            <label class="form-check-label">
                            <input data-column-name="{{$new_value}}" type="checkbox" class="form-check-input" name="{{ $alias_table}}" value="{{ $alias_table}}"
                                    checked>{{$new_value}} <i class="input-helper"></i></label>
                        </div>
                        @endforeach
                        @endforeach
                    </div>
                   

                    
                </div>
            </div>
            <div class="modal-footer mx-auto mt-5">
                <!-- <button type="button" class="btn btn-danger blkv-pdf"><span class="mdi mdi-file-pdf"></span>
 PDF</button> -->
                <button type="button" class="btn btn-success blkv-excel"><span class="mdi mdi-file-excel"></span>
Excel</button>
                <!-- <button type="button" class="btn btn-primary blkv-csv"><span class="mdi mdi-file"></span>
CSV</button> -->
            </div>

           </div>    
        </div>
    </div>

@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

<script src="https://cdn.datatables.net/fixedcolumns/3.3.2/js/dataTables.fixedColumns.min.js"></script>

<script>

    $('input[name="daterange"]').daterangepicker({
            autoUpdateInput: false,
            opens: 'right',
            locale: {
                format: 'DD/MM/YYYY'
            }
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                .format('YYYY-MM-DD'));
        });
        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
</script>


<script>

   $(document).on('click','.filter_button',function(){
       $('#basic-datatable').DataTable().draw();
   })
</script>

<script>
    $(".readonly").keydown(function(e){
        e.preventDefault();
    });
</script>

<script>
    function edit(transaction){
        $("#edit_pay_status_id").val($(transaction).data('payment_status')).change();
        $('.edit-form').attr('action',$(transaction).data('link'));
        $('#edit-transaction').modal('show');
    }
</script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.fn.DataTable.Api.register( 'buttons.exportData()', function( options ) {  
        if ( this.context.length ) {
            var jsonResult = $.ajax({
                url: '{!! route('admin.project_payment.exportlist') !!}',
                type: 'post',
                data: { daterange : $("#daterange_id").val(), milestone :$("#milestone").val()},
                success: function (result) {
                    console.log(result);
                },
                async: false
            });
            var resultArray = [];
            retunArray = jsonResult.responseJSON.map( function(el){
                resultArray =[];
                    Object.keys(el).map(function(key){
                        resultArray[key] =  el[key];
                })
                return resultArray;
            });
            
            header = [];
            body = [];
            selectedHederArray.forEach(function (item, index) {
                console.log(selectedHederArray);
                console.log(item);
                header.push(snake_to_humanize(item));
            });
            retunArray.map(function (el) {
                var temp = [];
                selectedArray.forEach(function (item, index) {
                    
                    temp[item] = el[item];
                });
                body.push(temp);
            });
            return {
                body: body.map(function (el) {
                    return Object.keys(el).map(function (key) {
                        return el[key];
                    });
                }),
                header: header
            };
        }
    });

     // download as excel
    $('.blkv-excel').click(function(){ 
        var first_date = $('#daterange_id').val();
        var second_date = $('#daterange_id').val();
        if(first_date=='' && second_date=='') {
            $.toast({
                heading: 'Error',
                text: 'Please select a date range for exporting',
                icon: 'error',
                position: 'top-right',
            })
        }
        else{
            selectedArray = [];
            selectedHederArray=[]; 
            $(".cust_draw_table .form-check-input:checked").each(function(){
                selectedArray.push($(this).attr("name"));
                selectedHederArray.push($(this).attr("data-column-name"));
            });
            $('.buttons-excel').trigger('click');
        }
    });
    // download as excel
    $('.blkv-pdf').click(function(){ 
        selectedArray = []; 
        selectedHederArray=[];
        $(".cust_draw_table .form-check-input:checked").each(function(){
            selectedArray.push($(this).attr("name"));
            selectedHederArray.push($(this).attr("data-column-name"));
        });
        $('.buttons-pdf').trigger('click');
    });
    // download as csv
    $('.blkv-csv').click(function(){ 
        selectedArray = []; 
        selectedHederArray=[];
        $(".cust_draw_table .form-check-input:checked").each(function(){
            selectedArray.push($(this).attr("name"));
            selectedHederArray.push($(this).attr("data-column-name"));
        });
        $('.buttons-csv').trigger('click');
    });
    // print
    $('.blkv-print').click(function(){ 
        selectedArray = []; 
        selectedHederArray=[];
        $(".cust_draw_table .form-check-input:checked").each(function(){
            selectedArray.push($(this).attr("name"));
            selectedHederArray.push($(this).attr("data-column-name"));
        });
        $('.buttons-print').trigger('click');
    });
    // copy
    $('.blkv-copy').click(function(){ 
        selectedArray = []; 
        selectedHederArray=[];
        $(".cust_draw_table .form-check-input:checked").each(function(){
            selectedArray.push($(this).attr("name"));
            selectedHederArray.push($(this).attr("data-column-name"));
        });
        $('.buttons-copy').trigger('click');
    });
    function snake_to_humanize(text){
        return text.replace(/^lead_/, "").replace(/_/g, " ").charAt(0).toUpperCase() +text.replace(/^lead_/, "").replace(/_/g, " ").slice(1)
    }
    function chk_all_box(){
        if($("#blk_dt_chkall").is(':checked')){
            $('.cust_draw_table input[type=checkbox]').each(function () {
                $(this).prop( "checked", true );
            });
        }else{
            $('.cust_draw_table input[type=checkbox]').each(function () {
                $(this).prop( "checked", false );
            });
        }
    }
    $('.cust_draw_table .form-check-input').click(function(e){
        var checkboxCount = $('.cust_draw_table .form-check-input').length;
        if($('.cust_draw_table .form-check-input:checked').length != checkboxCount){
            $('#blk_dt_chkall').prop('checked', false);
        }else{
            $('#blk_dt_chkall').prop('checked', true);
        }
    });
</script>
{!! $dataTable->scripts() !!}
<script>
    $('.dt-buttons').hide();
</script>

@endpush
