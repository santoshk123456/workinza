@extends('admin.layouts.app')
@section('title', set_page_titile('Project Payment View'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('project_payments.show',$invoice) }}
            </div>
            <h4 class="page-title">{{'IN'.($invoice->id)}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               <div class="row mb-3">
                <div class="col-12 text-right">
                        <div class="master-actions-inner">
                       
                         <button  onclick="location.href='{{ route('admin.invoices.index') }}'" type="button" class="btn btn-primary btn-sm-multi-common btn-white mb-3"><i class="mdi mdi-format-list-bulleted"></i>List All</button>
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                          <label>{{ __('Client') }}</label>
                            <p>{{ucwords($invoice->user->full_name)}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                          <label>{{ __('Project') }}</label>
                            <p>{{ucwords($invoice->milestones->project->title)}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                          <label>{{ __('Milestone') }}</label>
                            <p>{{ucwords($invoice->milestones->milestone_name)}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Freelancer') }}</label>
                            <p>{{__('Sai')}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Amount') }}  ({{config('data.currency_symbol')}})</label>
                            <p>{{ formatAmount($invoice->amount) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Status') }}</label>
                            @if(empty($invoice->status))
                            <p>{{__('Pending')}}</p>
                            @elseif($invoice->status == 1)
                            <p>{{__('Paid')}}</p>
                            @else
                            <p>{{__('Failed')}}</p>
                            @endif
                        </div>
                    </div>
                   
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Invoiced On') }}</label>
                            <p>{{ hcms_date(strtotime($invoice->created_at), 'date-time', false) }}</p>
                        </div>
                    </div>
                   
                </div>

            </div>


        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
@endsection
@push('custom-scripts')




@endpush
