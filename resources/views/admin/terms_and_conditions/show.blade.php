@extends('admin.layouts.app')
@section('title', set_page_titile(__($termsAndCondition->heading.' - Terms And Conditions')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('termsAndConditions.show',$termsAndCondition) }}
            </div>
            <h4 class="page-title">{{ $termsAndCondition->heading }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Heading') }}</label>
                            <p>{{ $termsAndCondition->heading }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                            <div class="data-view">
                                <label>{{ __('Slug') }}</label>
                                <p>{{ $termsAndCondition->slug }}</p>
                            </div>
                        </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Static Heading') }}</label>
                            <p>{{ $termsAndCondition->static_heading }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Version') }}</label>
                            <p>{{ $termsAndCondition->version }}</p>
                        </div>
                    </div>
                  
                  
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Created At') }}</label>
                            <p>{{ hcms_date(strtotime($termsAndCondition->created_at),'date-time') }}</p>
                            
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Modified At') }}</label>
                            <p>{{ hcms_date(strtotime($termsAndCondition->updated_at),'date-time') }}</p>
                        </div>
                    </div>
                    <div class="col-md-8 mb-3">
                        <div class="data-view">
                            <label>{{ __('Short Description') }}</label>
                            <p>{{ $termsAndCondition->short_description }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="data-view border-0">
                            <label>{{ __('Content') }}</label>
                            <div class="border-bottom d-table w-100">
                                <p>{!! $termsAndCondition->content !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
@endpush
