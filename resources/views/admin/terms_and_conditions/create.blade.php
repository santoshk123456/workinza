@extends('admin.layouts.app')
@section('title', set_page_titile(__('Pages')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
<style>
.mce-notification
 {display: none !important;}
  </style>
@endpush

@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
            {{ Breadcrumbs::render('termsAndConditions.create') }}
            </div>
            <h4 class="page-title">{{ __('Create Terms And Conditions') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.terms_and_conditions.store')}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-6 mb-4 @if ($errors->has('heading')) validation-failed @endif">
                                            <label for="validationCustom01">{{ __('Heading') }}
                                                <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" value="{{old('heading')}}" name="heading" class="form-control" id="validationCustom01" placeholder="Heading" required>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('heading'))
                                                    {{ $errors->first('heading') }}
                                                @else 
                                                    {{ __('Heading cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('slug')) validation-failed @endif">
                                            <label for="validationCustom02">{{ __('Slug') }}
                                                <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" value="{{old('slug')}}" name="slug" class="form-control" id="validationCustom02" placeholder="Slug" readonly required>
                                            @if ($errors->has('slug'))
                                                <div class="invalid-feedback" >
                                                    {{ $errors->first('slug') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 mb-4 @if ($errors->has('static_heading')) validation-failed @endif">
                                            <label for="validationCustom01">{{ __('Static Heading') }}
                                                <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" value="{{old('static_heading')}}" name="static_heading" class="form-control" id="validationCustom01" placeholder="Static Heading" required>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('static_heading'))
                                                    {{ $errors->first('static_heading') }}
                                                @else 
                                                    {{ __('Static heading cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-lg-6 mb-4 @if ($errors->has('version')) validation-failed @endif">
                                            <label for="validationCustom01">{{ __('Version') }}
                                                <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" pattern="\d*" maxlength="2" value="{{ old('version') }}" name="version" class="form-control numericOnly" id="validationCustom01" placeholder="Version" required>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('version'))
                                                    {{ $errors->first('version') }}
                                                @else 
                                                    {{ __('Version cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>
                                       
                     
                                        <div class="col-lg-6 mb-4 @if ($errors->has('short_description')) validation-failed @endif">
                                            <label for="validationCustom06">{{ __('Short Description') }}
                                            </label>
                                            <textarea name="short_description" class="form-control" id="validationCustom06" >{{old('short_description')}}</textarea>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('short_description'))
                                                    {{ $errors->first('short_description') }}
                                                @else 
                                                    {{ __('Short Description cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-lg-12 @if ($errors->has('description')) validation-failed @endif">
                                            <div class="form-group">
                                                <label for="validationCustom03">{{ __('Description') }}
                                                    <span class="mandatory">*</span>
                                                </label>
                                                <textarea value="{{old('description')}}" id="mytextarea"  type="text" name="description" class="form-control" id="validationCustom03" placeholder="Enter Description" required>{{old('description')}}</textarea>
                                                <div class="invalid-feedback">
                                                    @if ($errors->has('description'))
                                                    {{ $errors->first('description') }}
                                                    @else
                                                    {{ __('Description cannot be empty.') }}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 text-right px-0">
                                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
   
</div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}
<script>
    $("input[name='heading']").on('input', function() {
        $("input[name='slug']").val(convertToSlug($(this).val()));
    })
</script>
<script>
$(".numericOnly").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
});
</script>
<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=7sdct7i66vo9fc0um5p6j9alp89sh4p014iz8so863zyi2rf'></script>

<script>
tinymce.init({
selector: '#mytextarea',
entity_encoding : "raw",
plugins: 'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
toolbar: 'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table',
toolbar_mode: 'floating',
tinycomments_mode: 'embedded',
tinycomments_author: 'Author name',
});
</script>
@endpush
