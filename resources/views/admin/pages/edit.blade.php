@extends('admin.layouts.app')
@section('title', set_page_titile(__($page->heading.' - CMS Page')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('cms_pages.edit',$page) }}
            </div>
            <h4 class="page-title">{{ __($page->heading) }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.pages.update',$page)}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-6 mb-4">
                                            <label for="validationCustom01">{{ __('Heading') }}
                                                <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" value="{{ $page->heading }}" name="heading" class="form-control" id="validationCustom01" placeholder="Heading" required>
                                        </div>
                                        <div class="col-lg-6 mb-4">
                                            <label for="validationCustom02">{{ __('Slug') }}
                                               
                                            </label>
                                            <input type="text" value="{{ $page->slug }}" name="slug" class="form-control" id="validationCustom02" placeholder="Slug" readonly >
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('meta_title')) validation-failed @endif">
                                            <label for="validationCustom03">{{ __('Meta Title') }}
                                                
                                            </label>
                                            <input type="text" value="{{ $page->meta_title }}" name="meta_title" class="form-control" id="validationCustom03" placeholder="Meta Title" >
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('meta_title'))
                                                    {{ $errors->first('meta_title') }}
                                                @else 
                                                    {{ __('Meta Title cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('meta_keywords')) validation-failed @endif">
                                            <label for="validationCustom04">{{ __('Meta Keywords') }}
                                                
                                            </label>
                                            <textarea name="meta_keywords" class="form-control" id="validationCustom04" >{{ $page->meta_keywords }}</textarea>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('meta_keywords'))
                                                    {{ $errors->first('meta_keywords') }}
                                                @else 
                                                    {{ __('Meta Keywords cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('meta_description')) validation-failed @endif">
                                            <div class="form-group">
                                                <label for="validationCustom05">{{ __('Meta Description') }}
                                                    
                                                </label>
                                                <textarea name="meta_description" class="form-control" id="validationCustom05" >{{ $page->meta_description }}</textarea>
                                                <div class="invalid-feedback" >
                                                    @if ($errors->has('meta_description'))
                                                        {{ $errors->first('meta_description') }}
                                                    @else 
                                                        {{ __('Meta Description cannot be empty.')}}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('short_description')) validation-failed @endif">
                                            <div class="form-group">
                                                <label for="validationCustom06">{{ __('Short Description') }}
                                                   
                                                </label>
                                                <textarea name="short_description" class="form-control" id="validationCustom06" >{{ $page->short_description }}</textarea>
                                                <div class="invalid-feedback" >
                                                    @if ($errors->has('short_description'))
                                                        {{ $errors->first('short_description') }}
                                                    @else 
                                                        {{ __('Short Description cannot be empty.')}}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                       
                                        
                    <div class="col-lg-12 @if ($errors->has('profile_image')) validation-failed @endif edit-popup">
                        <div class="form-group">
                                <label>{{ __('Profile Image ') }} </label>
                                <div class="input-group file">
                                @php 
                                $url = asset('storage/cms-page-images/'.$page->image);
                                @endphp
                              
                                    @if(!empty($page->image))
                                    <input type="file" id="drop1" class="dropify"  data-default-file="{{ $url }}" name="profile_image" />
                                    <input type="hidden" name="remove_title">
                                    @else
                                    <input type="file" id="drop1" class="dropify" accept = 'image/jpeg , image/jpg, image/gif, image/png'  name="profile_image" />
                                    @endif
                                    @if ($errors->has('profile_image'))
                                       <div class="invalid-feedback" @if ($errors->has('profile_image')) style="display:block" @endif>
                                           {{ $errors->first('profile_image') }} 
                                       </div>
                                     @endif
                                </div>
                            </div>
                        </div> 




                                    </div>
                                    <div class="col-12 px-0 @if ($errors->has('content')) validation-failed @endif">
                                        <label for="summernote-basic">{{ __('Content') }}
                                            <span class="mandatory">*</span>
                                        </label>
                                        <textarea id="summernote-basic" class="form-control" name="content" required>{{ $page->content }}</textarea>
                                        <div class="invalid-feedback" >
                                            @if ($errors->has('content'))
                                                {{ $errors->first('content') }}
                                            @else 
                                                {{ __('Content cannot be empty.')}}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12 text-right px-0">
                                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                                    </div> 
                                </div>
                                <!-- end card-body -->
                            </div>
                            <!-- end card-->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
            </div>
            <!-- container -->
        </div>
        </form>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}
<script>
    $("input[name='heading']").on('input', function() {
        $("input[name='slug']").val(convertToSlug($(this).val()));
    })

    var drEvent1 = $('.dropify').dropify();
    drEvent1.on('dropify.afterClear', function(event, element){
        $('input[name=remove_title]').val('removed');
    });  


    var icon = {!! json_encode(url('storage/cms-page-images/')) !!} +'/'+ $(this).attr('data-image');
  var drEvent = $('#profile_image').dropify();
  drEvent = drEvent.data('dropify');
  drEvent.resetPreview();
  drEvent.clearElement();
  drEvent.destroy();
  drEvent.init();

  var drEvent1 = $('.dropify').dropify();

  drEvent1.on('dropify.afterClear', function(event, element){
      if($('.edit-popup').hasClass('dropify-selected') ){
          $('input[name=remove_title]').val('removed');
          alert($('input[name=remove_title]').val());
      }
  });
</script>
@endpush

