@extends('admin.layouts.app')
@section('title', set_page_titile(__('Pages')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{ Breadcrumbs::render('cms_pages.create') }}
            </div>
            <h4 class="page-title">{{ __('CMS Pages') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.pages.store')}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-6 mb-4 @if ($errors->has('heading')) validation-failed @endif">
                                            <label for="validationCustom01">{{ __('Heading') }}
                                                <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" value="{{old('heading')}}" name="heading" class="form-control" id="validationCustom01" placeholder="Heading" required>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('heading'))
                                                    {{ $errors->first('heading') }}
                                                @else 
                                                    {{ __('Heading cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('slug')) validation-failed @endif">
                                            <label for="validationCustom02">{{ __('Slug') }}
                                                <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" value="{{old('slug')}}" name="slug" class="form-control" id="validationCustom02" placeholder="Slug" readonly required>
                                            @if ($errors->has('slug'))
                                                <div class="invalid-feedback" >
                                                    {{ $errors->first('slug') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('meta_title')) validation-failed @endif">
                                            <label for="validationCustom03">{{ __('Meta Title') }}
                                                
                                            </label>
                                            <input type="text" value="{{old('meta_title')}}" name="meta_title" class="form-control" id="validationCustom03" placeholder="Meta Title" >
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('meta_title'))
                                                    {{ $errors->first('meta_title') }}
                                                @else 
                                                    {{ __('Meta Title cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('meta_keywords')) validation-failed @endif">
                                            <label for="validationCustom04">{{ __('Meta Keywords') }}
                                               
                                            </label>
                                            <textarea name="meta_keywords" class="form-control" id="validationCustom04" >{{old('meta_keywords')}}</textarea>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('meta_keywords'))
                                                    {{ $errors->first('meta_keywords') }}
                                                @else 
                                                    {{ __('Meta Keywords cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('meta_description')) validation-failed @endif">
                                            <label for="validationCustom05">{{ __('Meta Description') }}
                                                
                                            </label>
                                            <textarea name="meta_description" class="form-control" id="validationCustom05" >{{old('meta_description')}}</textarea>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('meta_description'))
                                                    {{ $errors->first('meta_description') }}
                                                @else 
                                                    {{ __('Meta Description cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-4 @if ($errors->has('short_description')) validation-failed @endif">
                                            <label for="validationCustom06">{{ __('Short Description') }}
                                                
                                            </label>
                                            <textarea name="short_description" class="form-control" id="validationCustom06" >{{old('short_description')}}</textarea>
                                            <div class="invalid-feedback" >
                                                @if ($errors->has('short_description'))
                                                    {{ $errors->first('short_description') }}
                                                @else 
                                                    {{ __('Short Description cannot be empty.')}}
                                                @endif
                                            </div>
                                        </div>

                         <div class="col-lg-12 @if ($errors->has('profile_image')) validation-failed @endif">
                                <div class="form-group">
                                    <label class="light-label">{{ __('Image') }} </label>
                                    <div class="input-group">
                                        <input type="file" class="dropify" id="userImage"  accept = 'image/jpeg , image/jpg, image/gif, image/png'  data-default-file="" name="profile_image"/>
                                        
                                    </div>
                                    @if ($errors->has('profile_image'))
                                        <div class="invalid-feedback" @if ($errors->has('profile_image')) style="display:block" @endif>
                                            {{ $errors->first('profile_image') }} 
                                        </div>
                                    @endif
                                </div>
                            </div>
                                       
{{--                                         <div class="col-lg-6 mb-4 active-checkbox">
                                            <input value="{{old('active')}}" name="active" type="checkbox" id="switch3" checked data-switch="success">
                                            <label for="switch3" data-on-label="Active" data-off-label="Inactive"></label>
                                        </div> --}}
                                    </div>
                                    <label for="summernote-basic">{{ __('Content') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <textarea id="summernote-basic" class="form-control"  name="content" required>{{old('content')}}</textarea>
                                    <div class="invalid-feedback" >
                                                @if ($errors->has('content'))
                                                    {{ $errors->first('content') }}
                                                @else 
                                                    {{ __('Content cannot be empty.')}}
                                                @endif
                                    </div>
                                    <div class="col-12 text-right px-0">
                                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}
<script>
    $("input[name='heading']").on('input', function() {
        $("input[name='slug']").val(convertToSlug($(this).val()));
    })
</script>

<script>
  var icon = {!! json_encode(url('/storage/cms-page-images/')) !!} +'/'+ $(this).attr('data-image');
  var drEvent = $('#userImage').dropify();
  drEvent = drEvent.data('dropify');
  drEvent.resetPreview();
  drEvent.clearElement();
  drEvent.destroy();
  drEvent.init();

  var drEvent1 = $('.dropify').dropify();

  drEvent1.on('dropify.afterClear', function(event, element){
      if($('.edit-popup').hasClass('dropify-selected') ){
          $('input[name=remove_title]').val('removed');
          alert($('input[name=remove_title]').val());
      }
  });


 

</script>
@endpush
