@extends('admin.layouts.app')
@section('title', set_page_titile(__($page->heading.' - CMS Page')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('cms_pages.show',$page) }}
            </div>
            <h4 class="page-title">{{ __($page->heading) }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Heading') }}</label>
                            <p>{{ $page->heading }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                            <div class="data-view">
                                <label>{{ __('Slug') }}</label>
                                <p>{{ $page->slug }}</p>
                            </div>
                        </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Meta Title') }}</label>
                            <p>{{ $page->meta_title }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Meta Keywords') }}</label>
                            <p>{{ $page->meta_keywords }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Meta Description') }}</label>
                            <p>{{ $page->meta_description }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Short Description') }}</label>
                            <p>{{ $page->short_description }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Created At') }}</label>
                            <p>{{ $page->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($page->created_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                            
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Modified At') }}</label>
                            <p>{{ $page->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($page->updated_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <div class="data-view border-0">
                            <label>{{ __('Content') }}</label>
                            <div class="border-bottom d-table w-100">
                                <p>{!! $page->content !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
@endpush
