@extends('admin.layouts.app')
@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{-- <form class="form-inline">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control form-control-light" id="dash-daterange" placeholder="Search">
                            <div class="input-group-append">
                                <span data-toggle="tooltip" data-placement="bottom" title="calendar"  class="input-group-text bg-primary border-primary text-white">
                                    <i class="mdi mdi-calendar-range font-13"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <a href="javascript: void(0);" data-toggle="tooltip" data-placement="bottom" title="Reload" class="btn btn-primary ml-2">
                        <i class="mdi mdi-autorenew"></i>
                    </a>
                    <a href="javascript: void(0);" data-toggle="tooltip" data-placement="bottom" title="Filter"  class="btn btn-primary ml-1">
                        <i class="mdi mdi-filter-variant"></i>
                    </a>
                </form> --}}
            </div>
            <h4 class="page-title">{{ __('Dashboard') }}</h4>
        </div>
    </div>
</div>
  {{-- <button type="button" id="notificationshow" class="btn btn-info mb-3 float-right"> {{ __('Test Notification') }}</button> --}}
 <!-- <h5 class="text-center col-md-6 col-sm-12 pl-0 d-block mb-2 text-primary"> Graphical Representation</h5>  	 -->

  <div class="row ml-0 mt-3 chartjs-chart shadow-sm bg-white p-4 mb-5" style="height: 250px; width:;">											
	<canvas id="bar-chart-example" class="chartjs-render-monitor" ></canvas>
</div> 

<!-- end page title -->
<div class="row">
    <div class="col-xl-12">
        <div class="row">
            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                            <i class="mdi mdi-home-modern widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Client Companies">{{ __('Client Companies') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $client_company_count }}</h3>
                        <p class="mb-0 text-muted">
                            <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> {{ $current_month_client_company_count }} </span>
                            <span class="text-nowrap">{{ __('Since last month') }}</span>
                        </p>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->

            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                            <i class="mdi mdi-account widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Client Individuals">{{ __('Client Individuals') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $client_individual_count }}</h3>
                        <p class="mb-0 text-muted">
                            <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> {{ $current_month_client_individual_count }} </span>
                            <span class="text-nowrap">{{ __('Since last month') }}</span>
                        </p>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->


            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                            <i class="mdi mdi-home-variant widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Freelancing Companies">{{ __('Freelancing Companies') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $freelancer_company_count }}</h3>
                        <p class="mb-0 text-muted">
                            <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> {{ $current_month_freelancer_company_count }} </span>
                            <span class="text-nowrap">{{ __('Since last month') }}</span>
                        </p>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->


            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                            <i class="mdi mdi-account-circle widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Freelancing Individuals">{{ __('Freelancing Individuals') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $freelancer_individual_count }}</h3>
                        <p class="mb-0 text-muted">
                            <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> {{ $current_month_freelancer_individual_count }} </span>
                            <span class="text-nowrap">{{ __('Since last month') }}</span>
                        </p>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->



 

            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                        <i class="dripicons-copy widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Projects In Progress">{{ __('Projects - Open') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $project_open_count }}</h3>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->
            

            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                        <i class="dripicons-copy widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Projects In Progress">{{ __('Projects - In Progress') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $project_inprogress_count }}</h3>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->

            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                        <i class="mdi mdi-file-lock widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Projects Completed">{{ __('Projects - Completed') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $project_completed_count }}</h3>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->

            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                        <i class="dripicons-copy widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Projects In Progress">{{ __('Total Earnings') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $transactions_amount }}</h3>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->

             
           {{-- <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                            <i class="mdi mdi-cart-plus widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Admins">{{ __('Admins') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $admin_count }}</h3>
                        <p class="mb-0 text-muted">
                            <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> {{ $current_month_admin_count }}</span>
                            <span class="text-nowrap">{{ __('Since last month') }}</span>
                        </p>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->
            <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                            <i class="mdi mdi-currency-usd widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of Pages">{{ __('Pages') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $page_count }}</h3>
                        <p class="mb-0 text-muted">
                            <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i>{{ $current_month_page_count }}</span>
                            <span class="text-nowrap">{{ __('Since last month') }}</span>
                        </p>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col--> --}}

            {{-- <div class="col-lg-3">
                <div class="card widget-flat">
                    <div class="card-body">
                        <div class="float-right">
                            <i class="mdi mdi-pulse widget-icon"></i>
                        </div>
                        <h5 class="text-muted font-weight-normal mt-0" title="Number of FAQ's">{{ __('FAQ\'s') }}</h5>
                        <h3 class="mt-3 mb-3">{{ $faq_count }}</h3>
                        <p class="mb-0 text-muted">
                            <span class="text-success mr-2"><i class="mdi mdi-arrow-up-bold"></i> {{ $current_month_faq_count }}</span>
                            <span class="text-nowrap">{{ __('Since last month') }}</span>
                        </p>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> --}} <!-- end col-->
        </div> <!-- end row -->
    </div> <!-- end col -->
</div><!-- end row -->
<div class="row">
    <div class="col-xl-4">
      {{--  <div class="card">
            <div class="card-body">                
                <h4 class="header-title pb-3">{{ __('Email Queues') }}</h4>               
                <h5 class="mb-1 mt-0 font-weight-normal">{{ __('Processed') }}</h5>
                <div class="progress-w-percent">
                    <span class="progress-value font-weight-bold">{{$email_queues_sent}} </span>
                    <div class="progress progress-sm">
                    <div class="progress-bar" role="progressbar" style="width: {{!empty($emails_queues_count) ? (($email_queues_sent * 100) / $emails_queues_count) : '0'}}%;" aria-valuenow="{{$email_queues_sent}}"  aria-valuemin="0" aria-valuemax="{{$emails_queues_count}}"></div>
                    </div>
                </div>
                <h5 class="mb-1 mt-0 font-weight-normal">{{ __('Not Processed') }}</h5>                          
                <div class="progress-w-percent mb-0">
                    <span class="progress-value font-weight-bold">{{$email_queues_not_send}} </span>
                    <div class="progress progress-sm">
                        <div class="progress-bar" role="progressbar" style="width: {{!empty($emails_queues_count) ? ($email_queues_not_send * 100 / $emails_queues_count) : '0'}}%;" aria-valuenow="{{$email_queues_not_send}}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> --}}
    {{-- <div class="col-xl-4">
        <div class="card">
            <div class="card-body">                
                <h4 class="header-title pb-3">{{ __('Push Notification Queues') }}</h4>               
                <h5 class="mb-1 mt-0 font-weight-normal">{{ __('Processed') }}</h5>
                <div class="progress-w-percent">
                    <span class="progress-value font-weight-bold">{{$push_queues_sent}} </span>
                    <div class="progress progress-sm">
                        <div class="progress-bar" role="progressbar" style="width: {{!empty($push_queues_count) ? (($push_queues_sent * 100) / $push_queues_count) : '0'}}%;" aria-valuenow="{{$push_queues_sent}}"  aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <h5 class="mb-1 mt-0 font-weight-normal">{{ __('Not Processed') }}</h5>                          
                <div class="progress-w-percent mb-0">
                    <span class="progress-value font-weight-bold">{{$push_queues_not_send}} </span>
                    <div class="progress progress-sm">
                        <div class="progress-bar" role="progressbar" style="width: {{!empty($push_queues_count) ? (($push_queues_not_send * 100) / $push_queues_count) : '0'}}%;" aria-valuenow="{{$push_queues_not_send}}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div>
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body">                
                <h4 class="header-title pb-3">{{ __('SMS Queues') }}</h4>               
                <h5 class="mb-1 mt-0 font-weight-normal">{{ __('Processed') }}</h5>
                <div class="progress-w-percent">
                    <span class="progress-value font-weight-bold">{{$sms_queues_sent}} </span>
                    <div class="progress progress-sm">
                        <div class="progress-bar" role="progressbar" style="width: {{ !empty($sms_queues_count) ? (($sms_queues_sent * 100) / $sms_queues_count) : '0'}}%;" aria-valuenow="{{$sms_queues_sent}}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <h5 class="mb-1 mt-0 font-weight-normal">{{ __('Not Processed') }}</h5>                          
                <div class="progress-w-percent mb-0">
                    <span class="progress-value font-weight-bold">{{$sms_queues_not_send}} </span>
                    <div class="progress progress-sm">
                        <div class="progress-bar" role="progressbar" style="width: {{ !empty($sms_queues_count) ? (($sms_queues_not_send * 100) / $sms_queues_count) : '0'}}%;" aria-valuenow="{{$sms_queues_not_send}}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div>
</div> --}}

@endsection
@push('custom-scripts')
 <script src="https://js.pusher.com/4.2/pusher.min.js"></script>

 <script>
        //Remember to replace key and cluster with your credentials.
        var pusher = new Pusher('b015af90027f6ebb613f', {
            cluster: 'ap2',
            encrypted: true
        });

        //Also remember to change channel and event name if your's are different.
        var channel = pusher.subscribe('notification');
        channel.bind('notification-event', function(message) {
            alert(message);
        });

    </script>

    <script>
         jQuery(document).ready(function(){
            jQuery('#notificationshow').click(function(e){
               e.preventDefault();
            $.ajaxSetup({
            headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
              });
             
               jQuery.ajax({
                  url: "{{ route('admin.notificationshow') }}",
                  method: 'post',
            
                  success: function(result){
                     jQuery('.alert').show();
                     jQuery('.alert').html(result.success);
                  }});
               });
            });
    </script>

{{-- chart implementation --}}
<script>

! function(r) {

var array = '<?php echo json_encode($month_array) ?>';
console.log(array);
var current_evaluation_level_value = '<?php echo json_encode($amount_array) ?>';
console.log(current_evaluation_level_value);
//change stored json data to array 
array = JSON.parse(array);
console.log(array);
current_evaluation_level_value=JSON.parse(current_evaluation_level_value); 
console.log(current_evaluation_level_value);




"use strict";
var a = function() {
    this.$body = r("body"), this.charts = []
};
a.prototype.respChart = function(a, t, e, o) {
    var n = Chart.controllers.line.prototype.draw;
    Chart.controllers.line.prototype.draw = function() {
        n.apply(this, arguments);
        var r = this.chart.chart.ctx,
            a = r.stroke;
        r.stroke = function() {
            r.save(), r.shadowColor = "rgba(0,0,0,0.01)", r.shadowBlur = 20, r.shadowOffsetX = 0, r.shadowOffsetY = 5, a.apply(this, arguments), r.restore()
        }
    };
    var s = Chart.controllers.doughnut.prototype.draw;
    Chart.controllers.doughnut = Chart.controllers.doughnut.extend({
        draw: function() {
            s.apply(this, arguments);
            var r = this.chart.chart.ctx,
                a = r.fill;
            r.fill = function() {
                r.save(), r.shadowColor = "rgba(0,0,0,0.03)", r.shadowBlur = 4, r.shadowOffsetX = 0, r.shadowOffsetY = 3, a.apply(this, arguments), r.restore()
            }
        }
    });
    var l = Chart.controllers.bar.prototype.draw;
    Chart.controllers.bar = Chart.controllers.bar.extend({
        draw: function() {
            l.apply(this, arguments);
            var r = this.chart.chart.ctx,
                a = r.fill;
            r.fill = function() {
                r.save(), r.shadowColor = "rgba(0,0,0,0.01)", r.shadowBlur = 20, r.shadowOffsetX = 4, r.shadowOffsetY = 5, a.apply(this, arguments), r.restore()
            }
        }
    }), Chart.defaults.global.defaultFontColor = "#8391a2", Chart.defaults.scale.gridLines.color = "#8391a2";
    var i = a.get(0).getContext("2d"),
        d = r(a).parent();
    return function() {
        var n;
        switch (a.attr("width", r(d).width()), t) {
            case "Line":
                n = new Chart(i, {
                    type: "line",
                    data: e,
                    options: o
                });
                break;
            case "Doughnut":
                n = new Chart(i, {
                    type: "doughnut",
                    data: e,
                    options: o
                });
                break;
            case "Pie":
                n = new Chart(i, {
                    type: "pie",
                    data: e,
                    options: o
                });
                break;
            case "Bar":
                n = new Chart(i, {
                    type: "bar",
                    data: e,
                    options: o
                });
                break;
            case "Radar":
                n = new Chart(i, {
                    type: "radar",
                    data: e,
                    options: o
                });
                break;
            case "PolarArea":
                n = new Chart(i, {
                    data: e,
                    type: "polarArea",
                    options: o
                })
        }
        return n
    }()
}, a.prototype.initCharts = function() {
    var a = [];

    if (r("#bar-chart-example").length > 0) {

        var t = document.getElementById("bar-chart-example").getContext("2d").createLinearGradient(0, 500, 0, 150);
        t.addColorStop(0, "#fa5c7c"), t.addColorStop(1, "#727cf5");
        var e = {
            labels: array,
          //labels: ["Competence 3", "Competence 5", "Competence 37"],
            datasets: [{
                backgroundColor: t,
                borderColor: t,
                hoverBackgroundColor: t,
                hoverBorderColor: t,
              //  data: [1, 4, 5]
                data: current_evaluation_level_value
            }]
        };
        a.push(this.respChart(r("#bar-chart-example"), "Bar", e, {
            maintainAspectRatio: !1,
            legend: {
                display: !1
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        display: !1,
                        color: "rgba(0,0,0,0.05)"
                    },
                    stacked: !1,
                    ticks: {
                        // max: 5,
                        // min: 0,
                        // stepSize: 1


                    //     callback: function(value, index, values) {
                    //     return '$' + value.toFixed(decimals);
                    // }

                        min: 0,
                        max: 10000,
                        stepSize: 2000,
                    }
                }],
                xAxes: [{
                    barPercentage: .7,
                    categoryPercentage: .5,
                    stacked: !1,
                    gridLines: {
                        color: "rgba(0,0,0,0.01)"
                    }
                }]
            }
        }))
    }


    return a
}, a.prototype.init = function() {
    var a = this;
    Chart.defaults.global.defaultFontFamily = '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif', a.charts = this.initCharts(), r(window).on("resize", function(t) {
        r.each(a.charts, function(r, a) {
            try {
                a.destroy()
            } catch (r) {}
        }), a.charts = a.initCharts()
    })
}, r.ChartJs = new a, r.ChartJs.Constructor = a
}(window.jQuery),
function(r) {
"use strict";
r.ChartJs.init()
}(window.jQuery);


</script>

@endpush