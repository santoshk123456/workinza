@extends('admin.layouts.app')
@section('title', set_page_titile(__('SMS Template Detail Page')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('sms_templates.show',$smsTemplate) }}
            </div>
            <h4 class="page-title">{{ __('View '.$smsTemplate->process_name) }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Process Name') }}</label>
                    <p>{{ $smsTemplate->process_name }}</p>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Description') }}</label>
                    <p>{{ $smsTemplate->description }}</p>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Created At') }}</label>
                    <p>{{ $smsTemplate->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($smsTemplate->created_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                    
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Modified At') }}</label>
                    <p>{{ $smsTemplate->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($smsTemplate->updated_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Template') }}</label>                    
                    <p>{{$smsTemplate->template}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
