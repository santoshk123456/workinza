@extends('admin.layouts.app')
@section('title', set_page_titile(__('SMS Templates Create')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{ Breadcrumbs::render('sms_templates.create') }}
            </div>
            <h4 class="page-title">{{ __('Create SMS Template') }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.sms_templates.store')}}"
            class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
            @csrf
            <div class="row">
                <div class="col-12 mb-4">
                    <label for="validationCustom01">{{ __('Process Name') }}
                        <span class="mandatory">*</span>
                    </label>
                    <input type="text" value="{{old('process_name')}}" name="process_name" class="form-control" id="validationCustom01" placeholder="Process Name" required>
                </div>
                <div class="col-12 mb-4">
                    <label for="validationCustom02">{{ __('Description') }}
                        <span class="mandatory">*</span>
                    </label>
                    <textarea name="description" class="form-control" id="validationCustom02" rows="3" required>{{old('description')}}</textarea>
                </div>
            </div>
            <label for="summernote-basic">{{ __('Template') }}
                <span class="mandatory">*</span>
            </label>
            <textarea id="summernote-basic" class="form-control"  name="template" required>{{old('template')}}</textarea>
            <div class="col-12 text-right px-0">
                <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
            </div>   
        </form>
    </div>
</div>
@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}
<script>
    $("input[name='heading']").on('input', function() {
        $("input[name='slug']").val(convertToSlug($(this).val()));
    })
</script>
@endpush
