@extends('admin.layouts.app')
@section('title', set_page_titile($subCategory->name.' - Subcategories'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('sub-categories.show',$subCategory) }}
            </div>
            <h4 class="page-title">{{ucwords($subCategory->name)}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               <div class="row mb-3">
                <div class="col-12 text-right">
                        <div class="master-actions-inner">
                        <button onclick="location.href='{{ route('admin.sub_categories.edit',$subCategory->uuid) }}'"  type="button" class="btn btn-primary btn-sm-multi-common mb-3 mr-2"><i class="mdi mdi-note-outline mr-1"></i>Edit</button>
                         <button  onclick="location.href='{{ route('admin.sub_categories.index') }}'" type="button" class="btn btn-primary btn-sm-multi-common btn-white mb-3"><i class="mdi mdi-format-list-bulleted"></i>List All</button>
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Subcategory') }}</label>
                            <p>{{ucwords($subCategory->name)}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Category Name') }}</label>
                            @php
                            $all_cat = $subCategory->categories->pluck('name')->toArray();
                            $List = ucwords(implode(', ', $all_cat)); 
                            @endphp
                            @if(!empty($List))
                            <p>{{ $List }}</p>
                            @else
                            <p>{{ __('NA') }}</p>
                            @endif
                        </div>
                    </div>

                   
                   
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Created At') }}</label>
                            <p>{{ hcms_date(strtotime($subCategory->created_at), 'date-time', false) }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Modified At') }}</label>
                            <p>{{ hcms_date(strtotime($subCategory->updated_at), 'date-time', false) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                     <label>Status</label>
                        <div id="approval_status">
                        @if($subCategory->active==1)
                        <h4><span class="badge badge-success">Active</i></span>  </h4> 
                        @else
                        <h4><span class="badge badge-danger">Inactive</span> </h4>
                        @endif
                        </div>
                    </div>
                   
                </div>

            </div>


        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
@endsection
@push('custom-scripts')




@endpush
