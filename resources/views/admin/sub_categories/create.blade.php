@extends('admin.layouts.app')
@section('title', set_page_titile(__('Create Subcategories')))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('sub-categories.create') }}
            </div>
            <h4 class="page-title">{{ __('Create Subcategories') }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{route('admin.sub_categories.store')}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <div class="row">

                        <div class="col-lg-6 @if ($errors->has('sub_category_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Subcategory') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{old('sub_category_name')}}" name="sub_category_name" class="form-control" minlength="2"
                                    maxlength="50" required placeholder="Sub Category" autocomplete="off">
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('sub_category_name'))
                                    {{ $errors->first('sub_category_name') }}
                                    @else
                                    {{ __('Sub category cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>


                      

                        <div class="col-lg-6 @if ($errors->has('categories_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="categories_ids">{{ __('Categories') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="categories_ids[]" id="category_ids" class="form-control" multiple="multiple" required>
                                        <option value=""></option>
                                        @foreach ($categories_list as $categories)
                                            <option value="{{$categories->id}}">{{$categories->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback error-txt" >
                                        @if ($errors->has('categories_ids'))
                                            {{ $errors->first('categories_ids') }}
                                        @else 
                                            {{ __('Categories cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i
                                class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i
                                class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

<script>
 $(document).ready(function() {
        $('#industry_ids').select2({
            placeholder: "{{__('Select Industries')}}",
        });
        $('#category_ids').select2({
            placeholder: "{{__('Select Categories')}}",
        });

    });
</script>

@endpush
