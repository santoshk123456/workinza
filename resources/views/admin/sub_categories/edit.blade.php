@extends('admin.layouts.app')
@section('title', set_page_titile($subCategory->name.' - Subcategories'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('sub-categories.edit',$subCategory) }}
            </div>
            <h4 class="page-title">{{__($subCategory->name)}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{route('admin.sub_categories.update',$subCategory->uuid)}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="row">

                        <div class="col-lg-6 @if ($errors->has('sub_category_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Subcategory') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('sub_category_name',$subCategory->name)}}" name="sub_category_name" class="form-control" minlength="2"
                                    maxlength="50" required placeholder="Sub Category" autocomplete="off">
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('sub_category_name'))
                                    {{ $errors->first('sub_category_name') }}
                                    @else
                                    {{ __('Sub category cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>

                       
                        
                        @php
                         $selected_cat = $subCategory->categories->pluck('id')->toArray();
                         
                        @endphp
                        <div class="col-lg-6 @if ($errors->has('category_id')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Categories') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <select name="category_id[]" id="category_id" class="js-example-basic-multiple form-control"
                                    multiple="multiple" required>
                                    <option value=""  disabled>Select Category</option>
                                    @foreach ($categories_list as $key=>$category)
                                    <option value="{{$category->id}}" {{(in_array($category->id,$selected_cat)? "selected" : "")}}>{{ucwords($category->name)}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('category_id'))
                                    {{ $errors->first('category_id') }}
                                    @else
                                    {{ __('Category cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>
                        

                        <div class="col-lg-6">
                            <div class="form-group active-checkbox">
                                <input  name="active" type="checkbox" id="switch3" @if($subCategory->active == 1) checked @endif data-switch="success">
                                <label for="switch3" data-on-label="Active" data-off-label="Inactive"></label>
                            </div>
                        </div>
                        

                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i
                                class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i
                                class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

<script>
$('.js-example-basic-multiple').select2({
    placeholder: "Select Category",
    allowClear: true
});

$('.industries-multiple').select2({
    placeholder: "Select Industry",
    allowClear: true
});
</script>


@endpush
