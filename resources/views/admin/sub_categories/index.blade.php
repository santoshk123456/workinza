@extends('admin.layouts.app')
@section('title', set_page_titile(__('Subcategories')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('sub-categories') }}
            <h4 class="page-title">{{ __('Subcategories') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                       @can('add subcategories')
                        <div class="col-sm-4">
                            <button type="button" onclick="location.href='{{ route('admin.sub_categories.create') }}'" class="btn btn-danger mb-3"><i class="mdi mdi-plus"></i> {{ __('
                            Add Subcategory') }}</button>
                        </div>
                       @endcan
                </div>
                {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                <div class="col-12 text-right px-0">
                    <div class="form-group row">
                        <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                        <div class="col-sm-1 mt-1">
                            <input type="text" id="pageNum" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}

<script>
    function edit(SubCategory){
        $('#sub_category_name').val($(SubCategory).data('subcategory'));
        $("#cat_id").val($(SubCategory).data('category')).trigger('change');;
        $('.edit-form').attr('action',$(SubCategory).data('link'));
        $('#edit-subcategory').modal('show');
    }
</script>

<script>
$('.js-example-basic-single').select2({
    placeholder: "Select Category",
    allowClear: true
});

$('.edit-cat').select2({
    
});
</script>

@endpush
