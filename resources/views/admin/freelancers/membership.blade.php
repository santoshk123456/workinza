@extends('admin.layouts.app')
@section('title', set_page_titile(__($user->full_name.' | Membership')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">                
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('freelancers.membership',$user) }}
            </div>
            <h4 class="page-title text-capitalize">{{ "Membership Plan "}}{{$user->full_name}}</h4>
        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{ route('admin.freelancers.show',$user) }}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('General Details') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.membership',$user)}}" aria-expanded="false" class="nav-link active">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Membership') }}</span>
        </a>
    </li>
    <li class="nav-item">
         <a href="{{route('admin.freelancers.transaction',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Transactions') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.project',$user)}}" aria-expanded="false" class="nav-link"> 
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Projects') }}</span>
        </a>
    </li>
    <li class="nav-item">
    <a href="{{route('admin.freelancers.reviewsRatings',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Reviews & Rating') }}</span>
        </a>
    </li>
</ul>
<div class="tab-content p-3">
    <div class="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            @if(!empty($current_subscription))
                                <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Plan') }}</label>
                                        <p>{{ !empty($current_subscription->membership) ? $current_subscription->membership->name : __('NA') }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Amount') }}</label>
                                        <p>{{ !empty($current_subscription->amount) ? formatAmount($current_subscription->amount) : __('NA') }}</p>
                                    </div>
                                </div>
                                {{-- <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Number of Projects Can Post') }}</label>
                                        <p>{{ !empty($current_subscription->membership) ? $current_subscription->membership->number_of_projects : __('NA') }}</p>
                                    </div>
                                </div> --}}
                                <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Number of Proposal') }}</label>
                                        <p>{{ !empty($current_subscription->membership) ? $current_subscription->membership->number_of_projects : __('NA') }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Product Commission') }}</label>
                                        <p>{{ !empty($current_subscription->membership) ? $current_subscription->membership->product_commission : __('NA') }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Number of Products can Post') }}</label>
                                        <p>{{ !empty($current_subscription->membership) ? $current_subscription->membership->number_of_products : __('NA') }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Service Percentage') }}</label>
                                        <p>{{ !empty($current_subscription->service_percentage) ? $current_subscription->service_percentage : __('NA') }}</p>
                                    </div>
                                </div>
                                {{-- <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Auto Renewal') }}</label>
                                        <p>{{ ($current_subscription->auto_renewal == 0) ? __('Off') : __('On') }}</p>
                                    </div>
                                </div> --}}
                                <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Purchased On') }}</label>
                                        <p>{{ !empty($current_subscription->start_date) ? hcms_date(strtotime($current_subscription->start_date),'date-time') : __('NA') }}</p>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Expired On') }}</label>
                                        <p>{{ !empty($current_subscription->end_date) ? hcms_date(strtotime($current_subscription->end_date),'date-time') : __('NA') }}</p>
                                    </div>
                                </div>
                            @else
                                <div class="col-12 text-center">Client don't have any active subscription</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
