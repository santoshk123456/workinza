@extends('admin.layouts.app')
@section('title', set_page_titile(__($user->full_name.' | Reviews & Ratings')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">                
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('freelancers.reviewsRatings',$user) }}
            </div>
            <h4 class="page-title text-capitalize">{{ "Reviews & Ratings "}}{{$user->full_name}}</h4>
        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{ route('admin.freelancers.show',$user) }}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('General Details') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.membership',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Membership') }}</span>
        </a>
    </li>
    <li class="nav-item">
         <a href="{{route('admin.freelancers.transaction',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Transactions') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.project',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Projects') }}</span>
        </a>
    </li>
    <li class="nav-item">
    <a href="{{route('admin.freelancers.reviewsRatings',$user)}}" aria-expanded="false" class="nav-link active">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Reviews & Rating') }}</span>
        </a>
    </li>
</ul>


<div class="tab-pane" id="transactions">
      <div class="row">
         <div class="col-12 py-2">
            <div class="card m-0 card-multi-common h-100">
               <div class="card-body">
                  <div class="row">
                        <div class="col-sm-4">
                        </div>
                        
                    </div>
                  <!-- datatable:start -->
                    <div class="table-responsive export_table customized-export-table">
                        <table id="leads-table" class="table datatable-common">
                            <thead>
                            <tr>
                                <th>Sl #</th>
                                <th>Project Name</th>
                                <th>Clients</th>
                                <th>Date</th>
                                <th>Net Rating</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                  <!-- datatable:end -->
               </div>
            </div>
         </div>
      </div>
   </div>





@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}

{{-- table -ajax --}}
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        var table = $('#leads-table').DataTable({
            processing: true,
            serverSide: false,
            order: [],
            bFilter: true,

            ajax: {
                type: "POST",
                data: {
                    user: '{{$user->id}}',
                },
                url: "{{route('admin.users.FreelancerRatings',$user)}}",
               
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    searchable: false,
                    sortable: false,
                },
                {
                    data: 'project_id',
                    name: 'project_id'
                },
                {
                    data: 'rated_by_user_id',
                    name: 'rated_by_user_id'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'net_rating',
                    name: 'net_rating',
                    render: function (data, type, row) {
                        return '<div class="jobseeker-rating" data-value='+data+'>'+data+'</div>';
                    }
                },
                {
                    data: 'action',
                    name: 'action',
                    searchable: false
                }
            ],
            dom: 'Blfrtip',
           
            lengthMenu: [
                [20, 50, 100, -1],
                ['20', '50', '100', 'Show all']
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip({
                    trigger: 'hover'
                })
            }
        });

    });

</script>

<script>
    $(document).ready(function () {
        $('#leads-table_wrapper .dt-buttons').hide();

        $(".jobseeker-rating").each(function (e) {
			var rateValue = $(this).attr('data-value');
			$(this).rateYo({
				rating: rateValue,
				numStars: 5,
				starWidth: "15px",
				spacing: "5px",
				readOnly: true,
			});
		});

       
        
    });

    function getRate(data) {
	   return data;
    }



    // function getImg(data) {
    //     var obj = JSON.parse(data); 
    //     if(obj.rated_by['profile_image'] != null && obj.driver['id'] != null)
    //     return '<a href="/admin/driver/'+obj.driver['id']+'/show"><img src="/storage/uploads/drivers/'+obj.rated_by['id']+'/profile-images/'+obj.rated_by['profile_image']+'"  class="img-rounded round-image" align="center" alt="image"  title="'+obj.rated_by['first_name'].substr(0,1).toUpperCase()+obj.rated_by['first_name'].substr(1) +' '+ obj.rated_by['last_name'].substr(0,1).toUpperCase()+obj.rated_by['last_name'].substr(1)+'"    />'+'  '+ obj.rated_by['first_name'].substr(0,1).toUpperCase()+obj.rated_by['first_name'].substr(1) +' '+ obj.rated_by['last_name'].substr(0,1).toUpperCase()+obj.rated_by['last_name'].substr(1)+'</a>';
    //     else
    //     return '<a href="/admin/driver/'+obj.driver['id']+'/show"><img src="/storage/uploads/default/default.png" class="img-rounded round-image" align="center" alt="image"  title="'+obj.rated_by['first_name'].substr(0,1).toUpperCase()+obj.rated_by['first_name'].substr(1) +' '+ obj.rated_by['last_name'].substr(0,1).toUpperCase()+obj.rated_by['last_name'].substr(1)+'"  />'+'  '+ obj.rated_by['first_name'].substr(0,1).toUpperCase()+obj.rated_by['first_name'].substr(1) +' '+ obj.rated_by['last_name'].substr(0,1).toUpperCase()+obj.rated_by['last_name'].substr(1)+'</a>';
        
    // }




</script>


@endpush
