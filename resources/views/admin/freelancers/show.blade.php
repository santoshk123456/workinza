@extends('admin.layouts.app')
@section('title', set_page_titile(__($user->full_name.' | View Freelancer')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">                
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('freelancers.show',$user) }}
            </div>
            <h4 class="page-title text-capitalize">{{ "View "}}{{$user->full_name}}{{ __("'s Profile")}}</h4>
        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{ route('admin.freelancers.show',$user) }}" aria-expanded="false" class="nav-link active">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('General Details') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.membership',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Membership') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.transaction',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Transactions') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.project',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Projects') }}</span>
        </a>
    </li>
    <li class="nav-item">
    <a href="{{route('admin.freelancers.reviewsRatings',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Reviews & Rating') }}</span>
        </a>
    </li>
</ul>
<div class="tab-content p-3">
    @include('admin.includes.freelancers.profile.profile_head')
    @include('admin.includes.freelancers.profile.tab_head')
    <div class="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="tab-content" id="v-pills-tabContent">
                            @include('admin.includes.freelancers.profile.tabs.profile')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
<script>
    function blockUser(msg){
        Swal.fire({
            title: 'Are you sure?',
            text: msg,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Update Status!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "/admin/user/toggle-active-status/{{$user->id}}",
                    success:function(){
                        Swal.fire(
                            'Status Updated!',
                            'The status of the freelancer has been updated.',
                            'success'
                        )
                        location.reload();
                    }
                });
            }
        })
    }
    function approveUser(msg,status){
        Swal.fire({
            title: 'Are you sure?',
            text: msg,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Update Status!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                user_ids = new Array();
                user_ids.push("{{$user->id}}");
                $.ajax({
                    type: "GET",
                    url: "/admin/user/approve",
                    data:{status,user_ids},
                    success:function(){
                        Swal.fire(
                            'Status Updated!',
                            'The status of the freelancer has been updated.',
                            'success'
                        )
                        location.reload();
                    }
                });
            }
        })
    }
</script>
@endpush
