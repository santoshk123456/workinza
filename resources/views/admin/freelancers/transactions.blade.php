@extends('admin.layouts.app')
@section('title', set_page_titile(__($user->full_name.' | Transactions')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">                
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('freelancers.transaction',$user) }}
            </div>
            <h4 class="page-title text-capitalize">{{ "Transactions "}}{{$user->full_name}}</h4>
        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{ route('admin.freelancers.show',$user) }}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('General Details') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.membership',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Membership') }}</span>
        </a>
    </li>
    <li class="nav-item">
         <a href="{{route('admin.freelancers.transaction',$user)}}" aria-expanded="false" class="nav-link active">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Transactions') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('admin.freelancers.project',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Projects') }}</span>
        </a>
    </li>
    <li class="nav-item">
    <a href="{{route('admin.freelancers.reviewsRatings',$user)}}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Reviews & Rating') }}</span>
        </a>
    </li>
</ul>


<div class="tab-pane" id="transactions">
      <div class="row">
         <div class="col-12 py-2">
            <div class="card m-0 card-multi-common h-100">
               <div class="card-body">
                  <div class="row">
                        <div class="col-sm-4">
                        </div>
                        
                    </div>
                  <!-- datatable:start -->
                    <div class="table-responsive export_table customized-export-table">
                        <table id="leads-table" class="table datatable-common">
                            <thead>
                            <tr>
                                <th>Sl #</th>
                                <th>Transaction ID</th>
                                <th>Contract ID</th>
                                <th>Invoice Number</th>
                                <th>Paid To</th>
                                <th>Status</th>
                                <th>Amount</th>
                                <th>Transferred On</th>
                             
                            </tr>
                            </thead>
                        </table>
                    </div>
                  <!-- datatable:end -->
               </div>
            </div>
         </div>
      </div>
   </div>





@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}

{{-- table -ajax --}}
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        var table = $('#leads-table').DataTable({
            processing: true,
            serverSide: false,
            order: [],
            bFilter: true,

            ajax: {
                type: "POST",
                data: {
                    user: '{{$user->id}}',
                },
                url: "{{route('admin.users.transactionList',$user)}}",
               
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    searchable: false,
                    sortable: false,
                },
                {
                    data: 'transaction_id',
                    name: 'transaction_id'
                },
                {
                    data: 'contract_id',
                    name: 'contract_id',
                },
                {
                    data: 'invoice_number',
                    name: 'invoice_number',
                },
                {
                    data: 'paid_to',
                    name: 'paid_to',
                },
                {
                    data: 'payment_status',
                    name: 'payment_status',
                },
                {
                    data: 'amount',
                    name: 'amount',
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                }


            ],
            dom: 'Blfrtip',
           
            lengthMenu: [
                [20, 50, 100, -1],
                ['20', '50', '100', 'Show all']
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip({
                    trigger: 'hover'
                })
            }
        });

    });

</script>

<script>
    $(document).ready(function () {
        $('#leads-table_wrapper .dt-buttons').hide();
    });


</script>


@endpush
