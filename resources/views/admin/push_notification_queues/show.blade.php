@extends('admin.layouts.app')
@section('title', set_page_titile(__('Push Notification Queue Detail Page')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('push_notification_queues.show', $pushNotificationQueue) }}
            </div>
            <h4 class="page-title">{{ __('Push Notification Queue Detail Page') }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Action Type') }}</label>
                    <p>{{ $pushNotificationQueue->action_type }}</p>
                </div>
            </div>
            <div class="col-lg-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Device Type') }}</label>
                    <p>{{ $pushNotificationQueue->device_type }}</p>
                </div>
            </div>
            <div class="col-lg-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Device ID') }}</label>
                    <p>{{ $pushNotificationQueue->device_id }}</p>
                </div>
            </div>
            <div class="col-lg-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Message') }}</label>
                    <p>{{ $pushNotificationQueue->message }}</p>
                </div>
            </div>
            <div class="col-lg-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Extra Datas') }}</label>
                    <p>{{ $pushNotificationQueue->extra_datas }}</p>
                </div>
            </div>
            <div class="col-lg-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Status') }}</label>
                    @if($pushNotificationQueue->sent_status == 1)
                        <p><span class="badge badge-success px-2">{{ __('Sent') }}</span></p>
                    @else
                        <p><span class="badge badge-danger px-2">{{ __('Failed') }}</span></p>
                    @endif
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Created At') }}</label>
                    <p>{{ $pushNotificationQueue->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($pushNotificationQueue->created_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                    
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Modified At') }}</label>
                    <p>{{ $pushNotificationQueue->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($pushNotificationQueue->updated_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
