@extends('admin.layouts.app')
@section('title', set_page_titile($membershipPlan->name.' - Membership Plans'))
@push('custom-styles')

@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('membershipPlans.edit',$membershipPlan)}}
            </div>
            <h4 class="page-title">{{ __($membershipPlan->name) }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" id="vehicle_create" enctype="multipart/form-data" action="{{route('admin.membership_plans.update',$membershipPlan->uuid)}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="row">

                        <div class="col-6 @if ($errors->has('plan_name')) validation-failed @endif">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">{{ __('Plan Name') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('plan_name',$membershipPlan->name) }}" name="plan_name" class="form-control" id="validationCustom01" placeholder="Plan Name" minlength="2" maxlength="90"  required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('plan_name'))
                                        {{ $errors->first('plan_name') }}
                                    @else 
                                        {{ __('Plan name cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-6 @if ($errors->has('user_type')) validation-failed @endif">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('User Type') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <div class="">
                                        {{-- <input type="radio" name="user_type" value="0" id="user_client" @if( old('user_client',$membershipPlan->user_type) == 0) checked="checked" @endif required>
                                        <label class="wei-400 ml-1">Client</label> --}}

                                        <input type="radio" name="user_type" value="1" id="user_freelance" @if( old('user_client',$membershipPlan->user_type) == 1) checked="checked" @endif required>
                                        <label class="wei-400 ml-1 mr-2">Freelancer</label>
                                </div>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('user_type'))
                                        {{ $errors->first('user_type') }}
                                    @else 
                                        {{ __('User type cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-6 @if ($errors->has('plan_type')) validation-failed @endif">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('Plan Type') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <div class="">
                                        <input type="radio" name="plan_type" value="1" class="" id="plan_monthly"  required>
                                        <label class="wei-400 ml-1">Monthly</label>

                                        <input type="radio" name="plan_type" value="2" class="" id="plan_yearly" required>
                                        <label class="wei-400 ml-1 mr-2">Yearly</label>
                                </div>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('plan_type'))
                                        {{ $errors->first('plan_type') }}
                                    @else 
                                        {{ __('Plan type cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-6 @if ($errors->has('plan_amount')) validation-failed @endif client_fields">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('Plan Amount') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="number" value="{{ old('plan_amount') }}" name="plan_amount" class="form-control" id="validationCustom01" placeholder="Plan Amount">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('plan_amount'))
                                        {{ $errors->first('plan_amount') }}
                                    @else 
                                        {{ __('Plan Amount cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div> -->
                       
                        <div class="col-md-6 @if ($errors->has('monthly_amount')) validation-failed @endif month">
                            <div class="form-group mb-3">
                                <label class="form-label-multi-common" for="monthly_amount">{{ __('Monthly Plan Amount') }}
                                ({{config('data.currency_symbol')}})
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('monthly_amount',$membershipPlan->monthly_amount) }}"
                                    name="monthly_amount" class="form-control "
                                    id="monthly_amount" placeholder="Monthly Plan Amount" step="0.01" 
                                    onclick="ValidateDecimalInputs(this)" >
                                <div class="invalid-feedback">
                                    @if ($errors->has('monthly_amount'))
                                    {{ $errors->first('monthly_amount') }}
                                    @else
                                    {{ __('Enter a value between 1 and 999999.99')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-6 @if ($errors->has('yearly_amount') ) validation-failed @endif year">
                            <div class="form-group mb-3">
                                <label class="form-label-multi-common" for="yearly_amount">{{ __('Yearly Plan Amount') }}
                                ({{config('data.currency_symbol')}})
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('yearly_amount',$membershipPlan->yearly_amount) }}"
                                    name="yearly_amount" class="form-control "
                                    id="yearly_amount" placeholder="Yearly Plan Amount" step="0.01" 
                                    onclick="ValidateDecimalInputs(this)" >
                                <div class="invalid-feedback">
                                    @if ($errors->has('yearly_amount'))
                                    {{ $errors->first('yearly_amount') }}
                                    @else
                                    {{ __('Enter a value between 1 and 999999.99')}}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-6 @if ($errors->has('duration')) validation-failed @endif client_fields">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('Duration') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="number" value="{{ old('duration') }}" name="duration" class="form-control" id="validationCustom01" placeholder="Duration">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('duration'))
                                        {{ $errors->first('duration') }}
                                    @else 
                                        {{ __('Duration cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div> -->

                       
                        <div class="col-6 @if ($errors->has('max_project_fr')) validation-failed @endif freelancer_fields">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('Maximum Number of Projects Can Apply') }}
                                </label>
                                <input type="text" pattern="\d*" maxlength="4" value="{{ old('max_project_fr',$membershipPlan->number_of_projects) }}" name="max_project_fr" class="form-control numericOnly" id="validationCustom01" placeholder="Number of Projects">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('max_project_fr'))
                                        {{ $errors->first('max_project_fr') }}
                                    @else 
                                        {{ __('Projects cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-6 @if ($errors->has('max_project_cl')) validation-failed @endif client_fields">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('Maximum Number of Projects Post') }}
                                </label>
                                <input type="text" pattern="\d*" maxlength="4" value="{{ old('max_project_cl',$membershipPlan->number_of_projects) }}" name="max_project_cl" class="form-control numericOnly" id="validationCustom01" placeholder="Number of Projects">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('max_project_cl'))
                                        {{ $errors->first('max_project_cl') }}
                                    @else 
                                        {{ __('Projects cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-6 @if ($errors->has('service_fee')) validation-failed @endif">
                            <div class="form-group mb-3">
                                <label class="form-label-multi-common" for="service_fee">{{ __('Service Fee (%)') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('service_fee',$membershipPlan->service_percentage) }}"
                                    name="service_fee" class="form-control "
                                    id="service_fee" placeholder="Service Fee" step="0.01" min="1" max="9999.99"
                                    onclick="ValidateDecimalSecondInputs(this)">
                                <div class="invalid-feedback">
                                    @if ($errors->has('service_fee'))
                                    {{ $errors->first('service_fee') }}
                                    @else
                                    {{ __('Enter a value between 1 and 9999.99')}}
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="col-6 @if ($errors->has('max_proposal')) validation-failed @endif client_fields">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('Maximum Number of Proposal a Project Can Receive') }}
                                </label>
                                <input type="text" pattern="\d*" maxlength="3" value="{{ old('max_proposal',$membershipPlan->number_of_proposals) }}" name="max_proposal" class="form-control numericOnly" id="validationCustom01" placeholder="Number of Proposals">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('max_proposal'))
                                        {{ $errors->first('max_proposal') }}
                                    @else 
                                        {{ __('Proposal cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-6 @if ($errors->has('max_products')) validation-failed @endif freelancer_fields">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('Maximum Number of Products') }}
                                </label>
                                <input type="text" pattern="\d*" maxlength="2" value="{{ old('max_products',$membershipPlan->number_of_products) }}" name="max_products" class="form-control numericOnly" id="validationCustom01" placeholder="Number of Products">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('max_products'))
                                        {{ $errors->first('max_products') }}
                                    @else 
                                        {{ __('Products cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 @if ($errors->has('product_commission')) validation-failed @endif freelancer_fields">
                            <div class="form-group mb-3">
                                <label class="form-label-multi-common" for="product_commission">{{ __('Product Commission (%)') }}
                                </label>
                                <input type="text" value="{{ old('product_commission',$membershipPlan->product_commission) }}"
                                    name="product_commission" class="form-control "
                                    id="product_commission" placeholder="Product Commission" step="0.01" min="1" max="999999.99"
                                    onclick="ValidateDecimalInputs(this)">
                                <div class="invalid-feedback">
                                    @if ($errors->has('product_commission'))
                                    {{ $errors->first('product_commission') }}
                                    @else
                                    {{ __('Enter a value between 1 and 999999.99')}}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-lg-6 client_fields">
                           <label class="mb-0" for="switch3 mb-3">{{ __('Subscription Auto renewal') }}
                            </label>
                            <div class="form-group active-checkbox mt-0">
                                <input  name="active" type="checkbox" id="switch3" checked data-switch="success">
                                <label class="mt-2" for="switch3" data-on-label="On" data-off-label="Off"></label>
                            </div>
                        </div> -->
                       
                        <div class="col-6 @if ($errors->has('chat_feature')) validation-failed @endif">
                            <div class="form-group mb-3">
                              <label for="validationCustom01">{{ __('Chat Feature') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <div class="">
                                        <input type="radio" name="chat_feature" value="1" class="" id="" @if( old('chat_feature',$membershipPlan->chat_feature) == 1) checked="checked" @endif required>
                                        <label class="wei-400 ml-1 mr-2">Yes</label>

                                        <input type="radio" name="chat_feature" value="0" class="" id="" @if( old('chat_feature',$membershipPlan->chat_feature) == 0) checked="checked" @endif required>
                                        <label class="wei-400 ml-1">No</label>
                                </div>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('chat_feature'))
                                        {{ $errors->first('chat_feature') }}
                                    @else 
                                        {{ __('Chat feature cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 @if ($errors->has('setup_charge')) validation-failed @endif">
                            <div class="form-group mb-3">
                                <label class="form-label-multi-common" for="setup_charge">{{ __('One Time Setup Charge') }}
                                ({{config('data.currency_symbol')}})
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('setup_charge',$membershipPlan->setup_charge) }}"
                                    name="setup_charge" class="form-control "
                                    id="setup_charge" placeholder="Setup Charge" step="0.01" min="1" max="999999.99"
                                    onclick="ValidateDecimalInputs(this)" required>
                                <div class="invalid-feedback">
                                    @if ($errors->has('setup_charge'))
                                    {{ $errors->first('setup_charge') }}
                                    @else
                                    {{ __('Enter a value between 1 and 999999.99')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                   
                    <br>
                    <div class="col-12 text-right px-0">
                        <button  class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div> 
                
                </div>
                                    
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

<script>
$(document).ready(function () {
    @if($membershipPlan->plan_type == 1)
        $('#plan_monthly').attr('checked',true);
        $('.year').addClass('d-none');
        $('.month').removeClass('d-none');
        $('#monthly_amount').prop('required', true);
    @else
        $('#plan_yearly').attr('checked',true);
        $('#yearly_amount').prop('required', true);
        $('.month').addClass('d-none');
        $('.year').removeClass('d-none');
    @endif
   
   @if(empty($membershipPlan->user_type))
    $('input:radio[name="user_type"]').filter('[value="0"]').attr('checked', true);
    $('.freelancer_fields').addClass('d-none');
   @else
    $('input:radio[name="user_type"]').filter('[value="1"]').attr('checked', true);
    $('.client_fields').addClass('d-none');
   @endif

   $(".numericOnly").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
   });
});
</script>
<script>
function ValidateDecimalInputs(e) {

var beforeDecimal =6;
var afterDecimal = 2;

$('#'+e.id).on('input', function () {
    this.value = this.value
      .replace(/[^\d.]/g, '')            
      .replace(new RegExp("(^[\\d]{" + beforeDecimal + "})[\\d]", "g"), '$1') 
      .replace(/(\..*)\./g, '$1')         
      .replace(new RegExp("(\\.[\\d]{" + afterDecimal + "}).", "g"), '$1');   
});
}

function ValidateDecimalSecondInputs(e) {

var beforeDecimal =4;
var afterDecimal = 2;

$('#'+e.id).on('input', function () {
    this.value = this.value
      .replace(/[^\d.]/g, '')            
      .replace(new RegExp("(^[\\d]{" + beforeDecimal + "})[\\d]", "g"), '$1') 
      .replace(/(\..*)\./g, '$1')         
      .replace(new RegExp("(\\.[\\d]{" + afterDecimal + "}).", "g"), '$1');   
});
}


$('#user_freelance').click(function () {
        $('.client_fields').addClass('d-none');
        $('.freelancer_fields').removeClass('d-none');
});
$('#user_client').click(function () {
        $('.freelancer_fields').addClass('d-none');
        $('.client_fields').removeClass('d-none');
});
$('#plan_yearly').click(function () {
        $('.month').addClass('d-none');
        $('.year').removeClass('d-none');
       
        $('#yearly_amount').prop('required', true);
        $('#monthly_amount').prop('required', false);
       
});
$('#plan_monthly').click(function () {
        $('.year').addClass('d-none');
        $('.month').removeClass('d-none');
       
        $('#monthly_amount').prop('required', true);
        $('#yearly_amount').prop('required', false);
        
});
</script>
@endpush



