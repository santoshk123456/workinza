@extends('admin.layouts.app')
@section('title', set_page_titile($membershipPlan->name.' - Membership Plans'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('membershipPlans.show',$membershipPlan)}}
            </div>
            <h4 class="page-title">{{ucwords($membershipPlan->name)}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               <div class="row mb-3">
                <div class="col-12 text-right">
                        <div class="master-actions-inner">
                        <button onclick="location.href='{{ route('admin.membership_plans.edit',$membershipPlan->uuid) }}'"  type="button" class="btn btn-primary btn-sm-multi-common mb-3 mr-2"><i class="mdi mdi-note-outline mr-1"></i>Edit</button>
                         <button  onclick="location.href='{{ route('admin.membership_plans.index') }}'" type="button" class="btn btn-primary btn-sm-multi-common btn-white mb-3"><i class="mdi mdi-format-list-bulleted"></i>List All</button>
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Plan Name') }}</label>
                            <p>{{ucwords($membershipPlan->name)}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Monthly Plan Amount') }}  ({{config('data.currency_symbol')}})</label>
                            <p>{{ formatAmount($membershipPlan->monthly_amount) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Yearly Plan Amount') }}  ({{config('data.currency_symbol')}})</label>
                            <p>{{ formatAmount($membershipPlan->yearly_amount) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('User Type') }}</label>
                            @if(empty($membershipPlan->user_type))
                               <p>{{"Client"}}</p>
                            @else
                               <p>{{"Freelancer"}}</p>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Chat Feature') }}</label>
                            @if(empty($membershipPlan->chat_feature))
                               <p>{{__('No')}}</p>
                            @else
                               <p>{{__('Yes')}}</p>
                            @endif
                        </div>
                    </div>
                    @if(empty($membershipPlan->user_type))
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Maximum Number of Project Post') }}</label>
                            @if(!empty($membershipPlan->number_of_projects))
                               <p>{{ $membershipPlan->number_of_projects }}</p>
                            @else
                               <p>{{__('NA')}}</p>
                            @endif
                        </div>
                    </div>
                    @else
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Maximum Number of Project Can Apply') }}</label>
                            @if(!empty($membershipPlan->number_of_projects))
                               <p>{{ $membershipPlan->number_of_projects }}</p>
                            @else
                               <p>{{__('NA')}}</p>
                            @endif
                        </div>
                    </div>
                    @endif

                    @if(empty($membershipPlan->user_type))
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Maximum Number of Proposal a Project Can Receive') }}</label>
                            @if(!empty($membershipPlan->number_of_proposals))
                               <p>{{ $membershipPlan->number_of_proposals }}</p>
                            @else
                               <p>{{__('NA')}}</p>
                            @endif
                        </div>
                    </div>
                    @endif

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Service Fee (%)') }}</label>
                            @if(!empty($membershipPlan->service_percentage))
                               <p>{{$membershipPlan->service_percentage}}</p>
                            @else
                               <p>{{__('NA')}}</p>
                            @endif
                        </div>
                    </div>


                    @if(!empty($membershipPlan->user_type))
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Maximum Number of Products') }}</label>
                            @if(!empty($membershipPlan->number_of_products))
                               <p>{{ $membershipPlan->number_of_products }}</p>
                            @else
                               <p>{{__('NA')}}</p>
                            @endif
                        </div>
                    </div>
                    @endif

                    @if(!empty($membershipPlan->user_type))
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Product Commission (%)') }}</label>
                            @if(!empty($membershipPlan->product_commission))
                               <p>{{ $membershipPlan->product_commission }}</p>
                            @else
                               <p>{{__('NA')}}</p>
                            @endif
                        </div>
                    </div>
                    @endif

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('One Time Setup Charge') }} ({{config('data.currency_symbol')}})</label>
                            @if(!empty($membershipPlan->setup_charge))
                               <p>{{ formatAmount($membershipPlan->setup_charge)}}</p>
                            @else
                               <p>{{__('NA')}}</p>
                            @endif
                        </div>
                    </div>

                   
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Created At') }}</label>
                            <p>{{ hcms_date(strtotime($membershipPlan->created_at), 'date-time', false) }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Modified At') }}</label>
                            <p>{{ hcms_date(strtotime($membershipPlan->updated_at), 'date-time', false) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                     <label>Status</label>
                        <div id="approval_status">
                        @if($membershipPlan->active==1)
                        <h4><span class="badge badge-success">Active</i></span>  </h4> 
                        @else
                        <h4><span class="badge badge-danger">Inactive</span> </h4>
                        @endif
                        </div>
                    </div>
                   
                </div>

            </div>


        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
@endsection
@push('custom-scripts')




@endpush
