@extends('admin.layouts.app')
@section('title', set_page_titile(__('Wallets')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{-- {{ Breadcrumbs::render('admin.membership_plans.wallets') }} --}}
            <h4 class="page-title">{{ __('Wallets') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="row">
                    <div class="col-12">
                       
                                    <div class="row">
                               
                                        <!-- <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('From Date') }}
                                            <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" name="from_date" id="from_date" class="form-control readonly" style="caret-color: transparent;" autocomplete="off" id="validationCustom01" placeholder="From Date" required>
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('To Date') }}
                                            <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" name="to_date" id="to_date" class="form-control readonly" style="caret-color: transparent;" autocomplete="off" id="validationCustom01" placeholder="To Date" required>
                                            
                                        </div> -->


                                        <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('Date') }}
                                            <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" name="daterange" id="daterange_id" class="input-text with-border form-control readonly" style="caret-color: transparent;" placeholder="Select date range" required/>
                                        </div>
                
                                        
                                        <div class="col-sm-6 mt-3">
                                            <button type="button" class="btn btn-danger mb-3 mr-1 filter_button" name="exp_excel" value="search"> {{ __('Search') }}</button>
                                          
                                        </div>
                                    </div>
                                
                        </div>
                </div>
                {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                <div class="col-12 text-right px-0">
                    <div class="form-group row">
                        <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                        <div class="col-sm-1 mt-1">
                            <input type="text" id="pageNum" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="pay-modal" class="modal" tabindex="-1" role="dialog" style="display: none; padding-right: 15px;"
    aria-modal="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                <div class="text-center mt-2 mb-4">
                    <h4>Pay for freelancer</label></h4>
                </div>

                <form method="POST" enctype="multipart/form-data" class="pl-3 pr-3 row tagForm" id="tag-form" 
                    action="">
                    @csrf
                   
                    
                    <div class="col-lg-12">
                        <div class=" @if ($errors->has('name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="example-select">{{ __('Amount') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input id="wallet_id" type="hidden" readonly name="wallet_id" class="form-control" id="validationCustom04"  placeholder="Name" required>
                                <input id="amount" type="text" readonly name="amount" class="form-control" id="validationCustom04" minlength="3" maxlength="50" placeholder="Name" required>
                                <div class="invalid-feedback">
                                    @if ($errors->has('name'))
                                    {{ $errors->first('name') }}
                                    @else
                                    {{ __('Name cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class=" @if ($errors->has('notes')) validation-failed @endif">
                            <div class="form-group">
                                <label for="example-select">{{ __('Notes') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <textarea id="notes" type="text"  name="notes" class="form-control" id="validationCustom04" minlength="3" maxlength="50" placeholder="Name" required></textarea>
                                <div class="invalid-feedback">
                                    @if ($errors->has('name'))
                                    {{ $errors->first('name') }}
                                    @else
                                    {{ __('Name cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right col-12">
                        {{-- <button class="btn btn-secondary" type="reset">Reset</button> --}}
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>

                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}

<script>
    


    $('input[name="daterange"]').daterangepicker({
            autoUpdateInput: false,
            opens: 'right',
            locale: {
                format: 'DD/MM/YYYY'
            }
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                .format('YYYY-MM-DD'));
        });
        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
</script>


<script>

   $(document).on('click','.filter_button',function(){
       $('#basic-datatable').DataTable().draw();
   })
</script>

<script>
    $(".readonly").keydown(function(e){
        e.preventDefault();
    });
    
</script>
<script>
    $(document).on('click','#pay_now',function(){
    $('#pay-modal').modal('show');
    $('#amount').val($(this).data('name'));
    $('#wallet_id').val($(this).data('id'));
   
   
   
    var formAction = "{{ route('admin.membership_plans.pay_to_freelancer') }}";
    //set form action
    $('#tag-form').attr('action', formAction);
});
</script>


@endpush
