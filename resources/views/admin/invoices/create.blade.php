@extends('admin.layouts.app')
@section('title', set_page_titile(__('Invoice Create')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
<style> 
.was-validated .form-control:invalid .invalid-feedback{
 display: block;
}
</style>
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{ Breadcrumbs::render('invoices.create') }}
            </div>
            <h4 class="page-title">{{ __('Create Invoice') }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.invoices.store')}}"
            class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
            @csrf
            <div class="row">
                <div class="col-6 mb-3 @if ($errors->has('user_id')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom01">{{ __('User') }}</label>
                        <select name="user_id" class="form-control" id="user_id" required>
                            <option value="" selected disabled>Select user</option>
                            @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->full_name}}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            @if ($errors->has('user_id'))
                                {{ $errors->first('user_id') }}
                            @else
                                {{ __('Please choose a user') }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-6 mb-3 @if ($errors->has('type')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom02">{{ __('Type') }}</label>
                        <select name="type" class="form-control" id="type" required>
                            <option value="" selected disabled>Select Type</option>
                            <option value="0">Arbitation</option>
                            <option value="1">Project Payment</option>
                            <option value="2">Cancellation</option>
                            <option value="3">Refund</option>
                            <option value="4">Bonus</option>
                            <option value="5">Subscription Payment</option>
                        </select>
                        <div class="invalid-feedback">
                            @if ($errors->has('type'))
                                {{ $errors->first('type') }}
                            @else
                                {{ __('Please choose a type') }}
                            @endif
                        </div>
                    </div>
                </div>
               
                <div class="col-6 mb-3 project @if ($errors->has('project_id')) validation-failed @endif d-none">
                    <div class="form-group">
                        <label for="validationCustom01">{{ __('Project') }}</label>
                        <select name="project_id" class="form-control" id="project_id">
                            <option value="" selected disabled>Select Project</option>
                        </select>
                        <div class="invalid-feedback">
                            @if ($errors->has('project_id'))
                                {{ $errors->first('project_id') }}
                            @else
                                {{ __('Please choose a project') }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-6 mb-3 project_milestone @if ($errors->has('project_milestone_id')) validation-failed @endif d-none">
                    <div class="form-group">
                        <label for="validationCustom01">{{ __('Milestone') }}</label>
                        <select name="project_milestone_id" class="form-control" id="project_milestone_id">
                            <option value="" selected disabled>Select Milestone</option>
                        </select>
                        <div class="invalid-feedback">
                            @if ($errors->has('project_milestone_id'))
                                {{ $errors->first('project_milestone_id') }}
                            @else
                                {{ __('Please choose a milestone') }}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-6 mb-3 amount @if ($errors->has('amount')) validation-failed  @endif">
                    <div class="form-group">
                        <label for="validationCustom03" id="amountLabel">{{ __('Amount') }}</label>
                        <input type="number" name="amount" class="form-control" id="amount" required>
                        <div class="invalid-feedback">
                            @if ($errors->has('amount'))
                                {{ $errors->first('amount') }}
                            @else
                                {{ __('Amount cannot be empty.') }}
                            @endif
                        </div>
                    </div>
                </div>
               
               
                <div class="col-12 text-right px-0">
                    <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                    <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                </div> 
            </div>
        </form>
    </div>
</div>
@endsection
@push('custom-scripts')
<script>
    // Get course based subjects
  $(document).on('change','#type', function(){
    $('.project_milestone').addClass('d-none');
   
    $('#project_milestone_id').attr('required',true);
    
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      if($(this).val() == 1 || $(this).val() == 3){
        if($(this).val() == 3){
            $("#amountLabel").text("Dispute Amount");
                }
        $.ajax({
            url: '{!! route('admin.invoices.select_project') !!}',
            type: 'get',
            data: { user : $('#user_id').val()},
            success: function (result) {
              
                $('.project').removeClass('d-none');
                $('#project_id').find('option').not(':first').remove();
                $.each(result,function(index, item){
                $('#project_id')
                    .append($('<option>', { value : index })
                    .text(item));
                });
                $('#project_id').attr('required',true);
               
            },
            async: false
        }); 
    }
  });
  $(document).on('change','#project_id', function(){
    $('.project_milestone').addClass('d-none');
    
    $('#project_milestone_id').attr('required',true);
   
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
     
        $.ajax({
            url: '{!! route('admin.invoices.select_milestones') !!}',
            type: 'get',
            data: { user : $('#user_id').val(),'project':$(this).val()},
            success: function (result) {
                $('.project_milestone').removeClass('d-none');
                $('#project_milestone_id').find('option').not(':first').remove();
                $.each(result,function(index, item){
                $('#project_milestone_id')
                    .append($('<option>', { value : index })
                    .text(item));
                });
                $('#project_milestone_id').attr('required',true);
                
            },
            async: false
        }); 
    
  });

</script>
@endpush

