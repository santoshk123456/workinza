@extends('admin.layouts.app')
@section('title', set_page_titile(__('Roles')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('roles') }}
            <h4 class="page-title">{{ __('Roles') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @can('add roles')
                        <div class="col-sm-4">
                            <button onclick="location.href='{{ route('admin.roles.create') }}'" type="button" class="btn btn-danger mb-3"><i class="mdi mdi-plus"></i> {{ __('Role') }}</button>
                        </div>
                    @endcan
                </div>
                <div id="basic-datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            {!! $dataTable->table(['class' => 'table  nowrap table-striped','id'=>'basic-datatable','width'=>'100%'], true) !!}
                            <div class="col-12 text-right px-0">
                                <div class="form-group row">
                                    <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                                    <div class="col-sm-1 mt-1">
                                        <input type="text" id="pageNum" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}
@endpush
