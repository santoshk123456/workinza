@extends('admin.layouts.app')
@section('title', set_page_titile(__('Roles')))
@push('custom-styles')
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('roles.create') }}
            </div>
            <h4 class="page-title">{{ __('Roles') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('admin.roles.store')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <div class="row">
                        <div class="col-md-6 @if ($errors->has('name')) validation-failed @endif">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">{{ __('Role') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('name') }}" name="name" class="form-control" id="validationCustom01" placeholder="Role" required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @else 
                                        {{ __('Name cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="validationCustom01">{{ __('Permissions') }}</label>
                            <table class="table table-bordered table-centered mb-0">
                                <thead>
                                    <tr>
                                        <th>{{ __('Module Name') }}</th>
                                        <th>{{ __('Add') }}</th>
                                        <th>{{ __('Edit') }}</th>
                                        <th>{{ __('View') }}</th>
                                        <th>{{ __('Delete') }}</th>
                                        <th>{{ __('List') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1; @endphp
                                    @foreach($permissions as $permission_module=>$permission)
                                        <tr>
                                            <td>
                                                {{ ucwords($permission_module) }}
                                            </td>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    @php 
                                                        $add_permission = $permission->reject(function($element) {
                                                            return mb_strpos($element->name, 'add') === false;
                                                        })->first();
                                                        $add_permission_name = '';
                                                        if($add_permission){
                                                            $add_permission_name = $add_permission->name;
                                                        }
                                                     @endphp
                                                        
                                                        @if(!empty($add_permission_name))
                                                            <input type="checkbox" class="custom-control-input" id="customCheck1{{ $i }}" @if(old('roles')) @foreach(old('roles') as $key => $role) @if($key == $add_permission_name) checked @endif @endforeach @endif name="roles[{{ $add_permission_name }}]" >
                                                            <label class="custom-control-label" for="customCheck1{{ $i }}"></label>
                                                        @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    @php 
                                                        $edit_permission = $permission->reject(function($element) {
                                                            return mb_strpos($element->name, 'edit') === false;
                                                        })->first();
                                                        $edit_permission_name = '';
                                                        if($edit_permission){
                                                            $edit_permission_name = $edit_permission->name;
                                                        }
                                                    @endphp
                                                    @if(!empty($edit_permission_name))
                                                        <input type="checkbox" class="custom-control-input" id="customCheck2{{ $i }}" @if(old('roles')) @foreach(old('roles') as $key => $role) @if($key == $edit_permission_name) checked @endif @endforeach @endif name="roles[{{ $edit_permission_name }}]" >
                                                        <label class="custom-control-label" for="customCheck2{{ $i }}"></label>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    @php 
                                                        $view_permission = $permission->reject(function($element) {
                                                            return mb_strpos($element->name, 'view') === false;
                                                        })->first();
                                                        $view_permission_name = '';
                                                        if($view_permission){
                                                            $view_permission_name = $view_permission->name;
                                                        }
                                                    @endphp
                                                    @if(!empty($view_permission_name))
                                                        <input type="checkbox" class="custom-control-input" id="customCheck3{{ $i }}" @if(old('roles')) @foreach(old('roles') as $key => $role) @if($key == $view_permission_name) checked @endif @endforeach @endif name="roles[{{ $view_permission_name }}]" >
                                                        <label class="custom-control-label" for="customCheck3{{ $i }}"></label>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    @php 
                                                        $delete_permission = $permission->reject(function($element) {
                                                            return mb_strpos($element->name, 'delete') === false;
                                                        })->first();
                                                        $delete_permission_name = '';
                                                        if($delete_permission){
                                                            $delete_permission_name = $delete_permission->name;
                                                        }
                                                    @endphp
                                                    @if(!empty($delete_permission_name))
                                                        <input type="checkbox" class="custom-control-input" id="customCheck4{{ $i }}" @if(old('roles')) @foreach(old('roles') as $key => $role) @if($key == $delete_permission_name) checked @endif @endforeach @endif name="roles[{{ $delete_permission_name }}]" >
                                                        <label class="custom-control-label" for="customCheck4{{ $i }}"></label>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    @php 
                                                        $list_permission = $permission->reject(function($element) {
                                                            return mb_strpos($element->name, 'list') === false;
                                                        })->first();
                                                        $list_permission_name = '';
                                                        if($list_permission){
                                                            $list_permission_name = $list_permission->name;
                                                        }
                                                    @endphp
                                                    @if(!empty($list_permission_name))
                                                        <input type="checkbox" class="custom-control-input" id="customCheck5{{ $i }}" @if(old('roles')) @foreach(old('roles') as $key => $role) @if($key == $list_permission_name) checked @endif @endforeach @endif name="roles[{{ $list_permission_name }}]" >
                                                        <label class="custom-control-label" for="customCheck5{{ $i }}"></label>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                        @php $i++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div> 
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

@endpush
