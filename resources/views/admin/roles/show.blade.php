@extends('admin.layouts.app')
@section('title', set_page_titile(__('Admin Users')))
@push('custom-styles')
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('roles.show',$role) }}
            </div>
            <h4 class="page-title">{{ "Permissions of the ".ucwords($role->name) }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                
                <table class="table table-striped table-centered mb-0 table-bordered">
                    <thead>
                        <tr>
                            <th>{{ __('Module Name') }}</th>
                            <th>{{ __('Add') }}</th>
                            <th>{{ __('Edit') }}</th>
                            <th>{{ __('View') }}</th>
                            <th>{{ __('Delete') }}</th>
                            <th>{{ __('List') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach($permissions as $permission_module=>$permission)
                            <tr>
                                <td>
                                    {{ ucwords($permission_module) }}
                                </td>
                                <td>
                                    @php 
                                        $add_permission = $permission->reject(function($element) {
                                            return mb_strpos($element->name, 'add') === false;
                                        })->first();
                                        $add_permission_name = '';
                                        if($add_permission){
                                            $add_permission_name = $add_permission->name;
                                        }
                                    @endphp
                                    @if(!empty($add_permission_name))
                                        @if($role->hasPermissionTo($add_permission_name)) 
                                            <i class="dripicons-checkmark pl-2">
                                        @else 
                                            <i class="dripicons-cross pl-2">
                                        @endif
                                    @else 
                                        <span class="not-applicable">{{ 'NA' }}</span>
                                    @endif
                                </td>
                                <td>
                                    @php 
                                        $edit_permission = $permission->reject(function($element) {
                                            return mb_strpos($element->name, 'edit') === false;
                                        })->first();
                                        $edit_permission_name = '';
                                        if($edit_permission){
                                            $edit_permission_name = $edit_permission->name;
                                        }
                                    @endphp
                                    @if(!empty($edit_permission_name))
                                        @if($role->hasPermissionTo($edit_permission_name)) 
                                            <i class="dripicons-checkmark pl-2">
                                        @else 
                                            <i class="dripicons-cross pl-2">
                                        @endif
                                    @else 
                                        <span class="not-applicable">{{ 'NA' }}</span>
                                    @endif
                                </td>
                                <td>
                                    @php 
                                        $view_permission = $permission->reject(function($element) {
                                            return mb_strpos($element->name, 'view') === false;
                                        })->first();
                                        $view_permission_name = '';
                                        if($view_permission){
                                            $view_permission_name = $view_permission->name;
                                        }
                                    @endphp
                                    @if(!empty($view_permission_name))
                                        @if($role->hasPermissionTo($view_permission_name)) 
                                            <i class="dripicons-checkmark pl-2">
                                        @else 
                                            <i class="dripicons-cross pl-2">
                                        @endif
                                    @else 
                                        {{ 'NA' }}
                                    @endif
                                </td>
                                <td>
                                    @php 
                                        $delete_permission = $permission->reject(function($element) {
                                            return mb_strpos($element->name, 'delete') === false;
                                        })->first();
                                        $delete_permission_name = '';
                                        if($delete_permission){
                                            $delete_permission_name = $delete_permission->name;
                                        }
                                    @endphp
                                    @if(!empty($delete_permission_name))
                                        @if($role->hasPermissionTo($delete_permission_name)) 
                                            <i class="dripicons-checkmark pl-2">
                                        @else 
                                            <i class="dripicons-cross pl-2">
                                        @endif
                                    @else 
                                        <span class="not-applicable">{{ 'NA' }}</span>
                                    @endif
                                </td>
                                <td>
                                    @php 
                                        $list_permission = $permission->reject(function($element) {
                                            return mb_strpos($element->name, 'list') === false;
                                        })->first();
                                        $list_permission_name = '';
                                        if($list_permission){
                                            $list_permission_name = $list_permission->name;
                                        }
                                    @endphp
                                    @if(!empty($list_permission_name))
                                        @if($role->hasPermissionTo($list_permission_name)) 
                                            <i class="dripicons-checkmark pl-2">
                                        @else 
                                            <i class="dripicons-cross pl-2">
                                        @endif
                                    @else 
                                        <span class="not-applicable">{{ 'NA' }}</span>
                                    @endif
                                </td>
                            </tr>
                            @php $i++; @endphp
                        @endforeach
                    </tbody>
                </table>
                
            </div>
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

@endpush
