@extends('admin.layouts.app')
@section('title', set_page_titile(__('Roles')))
@push('custom-styles')
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('roles.edit',$role) }}
            </div>
            <h4 class="page-title">{{ __('Roles') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('admin.roles.update',$role)}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-md-6 @if ($errors->has('name')) validation-failed @endif">
                            <div class="form-group mb-3">
                                <label for="validationCustom01">{{ __('Role') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ $role->name }}" name="name" class="form-control" id="validationCustom01" placeholder="Role" required>
                                <div class="invalid-feedback">
                                    @if ($errors->has('name'))
                                    {{ $errors->first('name') }}
                                    @else
                                    {{ __('Name cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label for="validationCustom01">{{ __('Permissions') }}</label>
                            <table class="table table-bordered table-centered mb-0">
                                <thead>
                                    <tr>
                                        <th>{{ __('Module Name') }}</th>
                                        <th>{{ __('Add') }}
                                            <div class="form-check slct-all ">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input " id="add_blk_dt_chkall" onchange="add_chk_all_box();" > <small>Select All</small> <i class="input-helper"></i></label>
                                            </div>
                                        </th>
                                        <th>{{ __('Edit') }}
                                            <div class="form-check slct-all ">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input " id="edit_blk_dt_chkall" onchange="edit_chk_all_box();" > <small>Select All</small> <i class="input-helper"></i></label>
                                            </div>

                                        </th>
                                        <th>{{ __('View') }}
                                            <div class="form-check slct-all ">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input " id="view_blk_dt_chkall" onchange="view_chk_all_box();" > <small>Select All</small> <i class="input-helper"></i></label>
                                            </div>

                                        </th>
                                        <th>{{ __('Delete') }}
                                            <div class="form-check slct-all ">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input " id="delete_blk_dt_chkall" onchange="delete_chk_all_box();" > <small>Select All</small> <i class="input-helper"></i></label>
                                            </div>

                                        </th>
                                        <th>{{ __('List') }}
                                            <div class="form-check slct-all ">
                                                <label class="form-check-label">
                                                    <input type="checkbox" class="form-check-input " id="list_blk_dt_chkall" onchange="list_chk_all_box();" > <small>Select All</small> <i class="input-helper"></i></label>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i = 1; @endphp
                                    @foreach($permissions as $permission_module=>$permission)
                                    <tr>
                                        <td>
                                            {{ ucwords($permission_module) }}
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox add_cust_draw_table">
                                                @php
                                                $add_permission = $permission->reject(function($element) {
                                                return mb_strpos($element->name, 'add') === false;
                                                })->first();
                                                $add_permission_name = '';
                                                if($add_permission){
                                                $add_permission_name = $add_permission->name;
                                                }
                                                @endphp
                                                @if(!empty($add_permission_name))
                                                <input type="checkbox" class="custom-control-input form-check-input" id="customCheck1{{ $i }}" name="roles[{{ $add_permission_name }}]" @if($role->hasPermissionTo($add_permission_name)) checked @endif >
                                                <label class="custom-control-label" for="customCheck1{{ $i }}"></label>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox edit_cust_draw_table">
                                                @php
                                                $edit_permission = $permission->reject(function($element) {
                                                return mb_strpos($element->name, 'edit') === false;
                                                })->first();
                                                $edit_permission_name = '';
                                                if($edit_permission){
                                                $edit_permission_name = $edit_permission->name;
                                                }
                                                @endphp
                                                @if(!empty($edit_permission_name))
                                                <input type="checkbox" class="custom-control-input form-check-input" id="customCheck2{{ $i }}" name="roles[{{ $edit_permission_name }}]" @if($role->hasPermissionTo($edit_permission_name)) checked @endif >
                                                <label class="custom-control-label" for="customCheck2{{ $i }}"></label>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox view_cust_draw_table">
                                                @php
                                                $view_permission = $permission->reject(function($element) {
                                                return mb_strpos($element->name, 'view') === false;
                                                })->first();
                                                $view_permission_name = '';
                                                if($view_permission){
                                                $view_permission_name = $view_permission->name;
                                                }
                                                @endphp
                                                @if(!empty($view_permission_name))
                                                <input type="checkbox" class="custom-control-input form-check-input" id="customCheck3{{ $i }}" name="roles[{{ $view_permission_name }}]" @if($role->hasPermissionTo($view_permission_name)) checked @endif >
                                                <label class="custom-control-label" for="customCheck3{{ $i }}"></label>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox delete_cust_draw_table">
                                                @php
                                                $delete_permission = $permission->reject(function($element) {
                                                return mb_strpos($element->name, 'delete') === false;
                                                })->first();
                                                $delete_permission_name = '';
                                                if($delete_permission){
                                                $delete_permission_name = $delete_permission->name;
                                                }
                                                @endphp
                                                @if(!empty($delete_permission_name))
                                                <input type="checkbox" class="custom-control-input form-check-input" id="customCheck4{{ $i }}" name="roles[{{ $delete_permission_name }}]" @if($role->hasPermissionTo($delete_permission_name)) checked @endif >
                                                <label class="custom-control-label" for="customCheck4{{ $i }}"></label>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox list_cust_draw_table">
                                                @php
                                                $list_permission = $permission->reject(function($element) {
                                                return mb_strpos($element->name, 'list') === false;
                                                })->first();
                                                $list_permission_name = '';
                                                if($list_permission){
                                                $list_permission_name = $list_permission->name;
                                                }
                                                @endphp
                                                @if(!empty($list_permission_name))
                                                <input type="checkbox" class="custom-control-input form-check-input" id="customCheck5{{ $i }}" name="roles[{{ $list_permission_name }}]" @if($role->hasPermissionTo($list_permission_name)) checked @endif >
                                                <label class="custom-control-label" for="customCheck5{{ $i }}"></label>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @php $i++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
<script>
    //for add button

    function add_chk_all_box() {
        if ($("#add_blk_dt_chkall").is(':checked')) {
            $('.add_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", true);
            });
        } else {
            $('.add_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", false);
            });
        }
    }
    $('.add_cust_draw_table .form-check-input').click(function(e) {
        var checkboxCount = $('.add_cust_draw_table .form-check-input').length;
        if ($('.add_cust_draw_table .form-check-input:checked').length != checkboxCount) {
            $('#add_blk_dt_chkall').prop('checked', false);
        } else {
            $('#add_blk_dt_chkall').prop('checked', true);
        }
    });
    //for edit button

    function edit_chk_all_box() {
        if ($("#edit_blk_dt_chkall").is(':checked')) {
            $('.edit_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", true);
            });
        } else {
            $('.edit_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", false);
            });
        }
    }
    $('.edit_cust_draw_table .form-check-input').click(function(e) {
        var checkboxCount = $('.edit_cust_draw_table .form-check-input').length;
        if ($('.edit_cust_draw_table .form-check-input:checked').length != checkboxCount) {
            $('#edit_blk_dt_chkall').prop('checked', false);
        } else {
            $('#edit_blk_dt_chkall').prop('checked', true);
        }
    });

    //for view button
    function view_chk_all_box() {
        if ($("#view_blk_dt_chkall").is(':checked')) {
            $('.view_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", true);
            });
        } else {
            $('.view_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", false);
            });
        }
    }
    $('.view_cust_draw_table .form-check-input').click(function(e) {
        var checkboxCount = $('.view_cust_draw_table .form-check-input').length;
        if ($('.view_cust_draw_table .form-check-input:checked').length != checkboxCount) {
            $('#view_blk_dt_chkall').prop('checked', false);
        } else {
            $('#view_blk_dt_chkall').prop('checked', true);
        }
    });

    //for delete button

    function delete_chk_all_box() {
        if ($("#delete_blk_dt_chkall").is(':checked')) {
            $('.delete_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", true);
            });
        } else {
            $('.delete_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", false);
            });
        }
    }
    $('.delete_cust_draw_table .form-check-input').click(function(e) {
        var checkboxCount = $('.delete_cust_draw_table .form-check-input').length;
        if ($('.delete_cust_draw_table .form-check-input:checked').length != checkboxCount) {
            $('#delete_blk_dt_chkall').prop('checked', false);
        } else {
            $('#delete_blk_dt_chkall').prop('checked', true);
        }
    });


    //for list button

    function list_chk_all_box() {
        if ($("#list_blk_dt_chkall").is(':checked')) {
            $('.list_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", true);
            });
        } else {
            $('.list_cust_draw_table input[type=checkbox]').each(function() {
                $(this).prop("checked", false);
            });
        }
    }
    $('.list_cust_draw_table .form-check-input').click(function(e) {
        var checkboxCount = $('.list_cust_draw_table .form-check-input').length;
        if ($('.list_cust_draw_table .form-check-input:checked').length != checkboxCount) {
            $('#list_blk_dt_chkall').prop('checked', false);
        } else {
            $('#list_blk_dt_chkall').prop('checked', true);
        }
    });

</script>
@endpush
