@extends('admin.layouts.app')
@section('title', set_page_titile('TN'.($transaction->id).' - Transaction'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('transactions.show',$transaction) }}
            </div>
            <h4 class="page-title">{{'TN'.($transaction->id)}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               <div class="row mb-3">
                <div class="col-12 text-right">
                        <div class="master-actions-inner">
                       
                         <button  onclick="location.href='{{ route('admin.transactions.index') }}'" type="button" class="btn btn-primary btn-sm-multi-common btn-white mb-3"><i class="mdi mdi-format-list-bulleted"></i>List All</button>
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                          <label>{{ __('Paid By') }}</label>
                            <p>{{ucwords($transaction->user->full_name)}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Paid To') }}</label>
                            <p>{{__('NA')}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Amount') }}  ({{config('data.currency_symbol')}})</label>
                            <p>{{ formatAmount($transaction->amount) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Status') }}</label>
                            @if(empty($transaction->payment_status))
                            <p>{{__('Pending')}}</p>
                            @elseif($transaction->payment_status == 1)
                            <p>{{__('Paid')}}</p>
                            @else
                            <p>{{__('Failed')}}</p>
                            @endif
                        </div>
                    </div>
                   
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Transferred On') }}</label>
                            <p>{{ hcms_date(strtotime($transaction->created_at), 'date-time', false) }}</p>
                        </div>
                    </div>
                   
                </div>

            </div>


        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
@endsection
@push('custom-scripts')




@endpush
