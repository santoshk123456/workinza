@extends('admin.layouts.app')
@section('title', set_page_titile(__('Transactions')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('transactions') }}
            <h4 class="page-title">{{ __('Transactions') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

            <div class="row mb-0">
                            <div class="col-sm-4">
                            </div>         
                            <div class="col-sm-8 mb-2 m-0 filter-control">
                                <button type="button" data-target="#filter" data-toggle="collapse" class="btn btn-sm btn-primary float-right filter"><i class="mdi mdi-filter"></i> Filter</button>
                            </div>     
                        
                                <div id="filter" class="collapse mb-1">
                                    <div class="row">

                                        <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('Date') }}
                                            </label>
                                            <input type="text" name="daterange" id="daterange_id" class="input-text with-border form-control readonly" style="caret-color: transparent;" placeholder="Select date range" required/>
                                        </div>
                                        <div class="col-lg-2">
                                            <label for="validationCustom01">{{ __('Status') }}
                                            </label>
                                            <select name="payment_status" id="payment_status_id" class="form-control">
                                                <option value="" selected="">All</option>
                                                <option value="0">Pending</option>
                                                <option value="1">Paid</option>
                                                <option value="2">Failed</option>
                                            </select>
                                        </div>
                
                                        
                                        <div class="col-sm-6 mt-3">
                                            <button type="button" class="btn btn-danger mb-3 mr-1 filter_button" name="exp_excel" value="search"> {{ __('Search') }}</button>
                                            <!-- <button type="button" class="btn btn-primary mt-3 filter_button filter-button"  title="Apply"><i class="mdi mdi-check"></i> Apply</button> -->
                                        </div>
                                    </div>
                                </div>
                               
                </div>
                {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                <div class="col-12 text-right px-0">
                    <div class="form-group row">
                        <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                        <div class="col-sm-1 mt-1">
                            <input type="text" id="pageNum" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@can('edit transactions')
    <div class="modal fade" id="edit-transaction" tabindex="-1" role="dialog" aria-labelledby="addTransaction" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="addTransactionLabel">{{ __('Edit Transaction') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form method="POST" class="needs-validation edit-form" novalidate>
                @csrf
                @method('put')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="validationCustom01">{{ __('Status') }}
                        </label>
                            <select name="payment_status" id="edit_pay_status_id" class="form-control id_100">
                                <option value="0">Pending</option>
                                <option value="1">Paid</option>
                                <option value="2">Failed</option>
                            </select>
                        <span class="invalid-feedback error-txt">
                        @if ($errors->has('payment_status'))
                        {{ $errors->first('payment_status') }}
                        @else
                        {{ __('Status cannot be empty.') }}
                        @endif
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        </div>
    </div>
@endcan



@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}

<script>

    $('input[name="daterange"]').daterangepicker({
            autoUpdateInput: false,
            opens: 'right',
            locale: {
                format: 'DD/MM/YYYY'
            }
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                .format('YYYY-MM-DD'));
        });
        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
</script>


<script>

   $(document).on('click','.filter_button',function(){
       $('#basic-datatable').DataTable().draw();
   })
</script>

<script>
    $(".readonly").keydown(function(e){
        e.preventDefault();
    });
</script>

<script>
    function edit(transaction){
        $("#edit_pay_status_id").val($(transaction).data('payment_status')).change();
        $('.edit-form').attr('action',$(transaction).data('link'));
        $('#edit-transaction').modal('show');
    }
</script>


@endpush
