@extends('admin.layouts.app')
@section('title', set_page_titile(__('Open Projects')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('open_projects') }}
            <h4 class="page-title">{{ __('Open Projects') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="{{ route('admin.projects.index') }}" aria-expanded="false" class="nav-link active">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('Open Projects') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.projects.list_inprogress') }}" aria-expanded="false" class="nav-link">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('Inprogress Project') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.projects.list_completed') }}" aria-expanded="false" class="nav-link">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('Completed Project') }}</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content p-3">
                    <div class="row">
                        @can('add projects')
                            <div class="col-sm-4">
                                <button onclick="location.href='{{ route('admin.projects.create') }}'" type="button" class="btn btn-danger mb-3"><i class="mdi mdi-plus"></i> {{ __('
                                Create Project') }}</button>
                            </div>
                        @endcan
                    </div>
                    {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                    <div class="col-12 text-right px-0">
                        <div class="form-group row">
                            <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                            <div class="col-sm-1 mt-1">
                                <input type="text" id="pageNum" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}
@endpush
