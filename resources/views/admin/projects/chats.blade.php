@extends('admin.layouts.app')
@section('title', set_page_titile($project->title.' | Project Proposals'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
<style> 


</style>
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('show_projects',$project)}}
            </div>
            <h4 class="page-title">{{ucwords($project->title)}}  - Proposals</h4>
        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{ route('admin.projects.show',$project) }}" aria-expanded="false" class="nav-link ">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Project Details') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.projects.proposals',$project) }}" aria-expanded="false" class="nav-link ">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Proposals') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.projects.transactions',$project) }}" aria-expanded="false" class="nav-link ">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Transactions') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.projects.chats',$project) }}" aria-expanded="false" class="nav-link active">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Chats') }}</span>
        </a>
    </li>
</ul>
<div class="tab-content p-3">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
         



                @if(!empty($chats))
                    <div class="messages-holder bg-shadow">

                        <div id="frame">
                            <div id="sidepanel">
                                <div id="contacts">
                         
                                    <ul class="non-archive-list">
                                    

                        

                                        @foreach($non_archive_highlights as $key=>$chat_highlight)
                                        @if($user_id!=$chat_highlight->from_user_id)
                                        @php $to_user=$chat_highlight->sender;
                                        @endphp
                                        @endif
                                        @if($user_id!=$chat_highlight->to_user_id)
                                        @php $to_user= $chat_highlight->recipient;
                                        @endphp
                                        @endif
                                        
                                        <a href="{{route('message.messages',array($project->uuid,$to_user->uuid))}}">
                                        <li @if($to_user->id == $freelancer->id) class="contact active" @else class="contact" @endif>
                                            
                                                <div class="wrap">

                                                    @if($to_user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$to_user->profile_image)))
                                                    <img src=" {{ asset('storage/user/profile-images/'.$to_user->profile_image) }}" style="height: 100px; width:100px; "  alt="" />
                                                    @else
                                                    <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $to_user->username }}" style="height: 100px; width:100px; "  class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                    @endif
                                                    <div class="meta">
                                                        <p class="name">{{ucwords($to_user->username)}}<span>{{timeAgoConversion($chat_highlight->created_at)}}</span></p>
                                                        <p class="preview">{{$chat_highlight->message}}</p>
                                                        @if($user->user_type==0)
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="non-archive-list[]" class="custom-control-input non-archive-list" id="non-archive{{$to_user->id}}" value="{{$to_user->id}}">

                                                            <label class="custom-control-label" for="non-archive{{$to_user->id}}"></label>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                           
                                        </li>
                                        </a>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="content">
                                <div class="contact-profile">
                                    <p> {{ucwords($freelancer->username)}}</p>
                                    <div class="social-media">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="messages">
                                    <ul>
                                        @foreach($chats as $key=>$chat)

                                        <li @if($chat->sender->id==$user_id) class="replies" @else class="sent" @endif >
                                            @if($chat->sender->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$chat->sender->profile_image)))
                                            <img src=" {{ asset('storage/user/profile-images/'.$chat->sender->profile_image) }}" style="height: 100px; width:100px; " alt="" />
                                            @else
                                            <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $chat->sender->username }}" style="height: 100px; width:100px; "  class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                            @endif
                                            <span class="message-label">{{ucwords($chat->sender->username)}}, {{timeAgoConversion($chat->created_at)}}</span>
                                            <p>{{$chat->message}}</p>
                                            @if(!empty($chat->attachment))
                                            <p> <a href="{{asset('storage/projects/comment_attachements/'.$chat->attachment)}}" class="btn white-nm-btn download" download="{{$chat->attachment}}" title="Download Attachment"><i class="fi-flaticon flaticon-download"></i></a></p>
                                            @endif
                                        </li>

                                        <!-- <li class="sep"><p>Today</p></li> -->
                                        @endforeach
                                    </ul>
                                </div>


                            </div>
                        </div>


                    </div>
                    @else
                    <div class="proposals-list-holder bg-shadow">
                        <div class="col-list-item">                    
                            <div class="media text-center text-md-left d-block d-md-flex">
                                No messages found on this project
                            </div>
                        </div>
                    </div>
                   @endif





















                </div>
            </div>
        </div> <!-- end card body-->
    </div> <!-- end card -->
</div><!-- end col-->
@endsection
