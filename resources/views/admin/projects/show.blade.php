@extends('admin.layouts.app')
@section('title', set_page_titile($project->title.' | Projects'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('show_projects',$project)}}
            </div>
            <h4 class="page-title">{{ucwords($project->title)}}</h4>
        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{ route('admin.projects.show',$project) }}" aria-expanded="false" class="nav-link active">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Project Details') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.projects.proposals',$project) }}" aria-expanded="false" class="nav-link">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Proposals') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.projects.transactions',$project) }}" aria-expanded="false" class="nav-link ">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Transactions') }}</span>
        </a>
    </li>
    <!-- <li class="nav-item">
        <a href="{{ route('admin.projects.chats',$project) }}" aria-expanded="false" class="nav-link ">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Chats') }}</span>
        </a>
    </li> -->
</ul>
<div class="tab-content p-3">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                <div class="row mb-3">
                        <div class="col-12 text-right">
                            <div class="master-actions-inner">
                                <button onclick="location.href='{{ route('admin.projects.edit',$project->uuid) }}'"  type="button" class="btn btn-light btn-sm-multi-common mb-3 mr-2"><i class="mdi mdi-pencil-outline mr-1"></i>Edit</button>
                                @if ($project->active == 1)
                                    <button type="button" class="btn btn-danger btn-sm-multi-common mb-3 mr-2" data-toggle="modal" data-target="#exampleModal"><i class="mdi mdi-block-helper mr-1"></i>Deactivate</button>
                                @else
                                    <button type="button" class="btn btn-success btn-sm-multi-common mb-3 mr-2" data-toggle="modal" data-target="#exampleModal"><i class="mdi mdi-check mr-1"></i>Activate</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <fieldset>
                        <legend>Project Details</legend>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Title') }}</label>
                                    <p>{{ucwords($project->title)}}</p>
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Location') }}</label>
                                    <p>{{ucwords($project->location)}}</p>
                                </div>
                            </div>
                            <div class="col-md-2 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Type') }}</label>
                                    <p>{{$project->type == 0 ? 'Fixed Rate' : 'Hourly Rate'}}</p>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Industries') }}</label>
                                    <p>{{ !empty($project->industries) ? implode(', ', $project->industries->pluck('name')->toArray()) : __('NA') }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Categories') }}</label>
                                    <p>{{ !empty($project->categories) ? implode(', ', $project->categories->pluck('name')->toArray()) : __('NA') }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Sub Categories') }}</label>
                                    <p>{{ !empty($project->subCategories) ? implode(', ', $project->subCategories->pluck('name')->toArray()) : __('NA') }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Skills') }}</label>
                                    <p>{{ !empty($project->skills) ? implode(', ', $project->skills->pluck('name')->toArray()) : __('NA') }}</p>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Posted By') }}</label>
                                    <p>{!! !empty($project->user) ?  '<a href="'.route('admin.freelancers.show',$project->user).'">'.$project->user->full_name.'</a>' : __('NA') !!}</p>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Freelancer') }}</label>
                                    <p>{!! !empty($project->contracts) ?  '<a href="'.route('admin.freelancers.show',$project->contracts->user).'">'.$project->contracts->user->full_name.'</a>' : __('NA') !!}</p>
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Description') }}</label>
                                    <p>{{ !empty($project->description) ? $project->description : __('NA') }}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Amount') }}</label>
                                    <p>{{formatAmount($project->amount)}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Status') }}</label>
                                    <p>{{projectStatus($project->status)}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Created Date') }}</label>
                                    <p>{{ !empty($project->created_at) ? hcms_date(strtotime($project->created_at),'date') : __('NA') }}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Updated Date') }}</label>
                                    <p>{{ !empty($project->updated_at) ? hcms_date(strtotime($project->updated_at),'date') : __('NA') }}</p>
                                </div>
                            </div>
                            @if ($project->active == 0)
                                <div class="col-md-12 mb-3">
                                    <div class="data-view">
                                        <label>{{ __('Deactivation Reason') }}</label>
                                        <p>{{ !empty($project->inactive_reason) ? $project->inactive_reason : __('NA') }}</p>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Budget From</legend>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Budget From') }}</label>
                                    <p>{{formatAmount($project->budget_from)}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Budget To') }}</label>
                                    <p>{{formatAmount($project->budget_to)}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Timeline') }}</label>
                                    <p>{{$project->timeline_count}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Timeline Type') }}</label>
                                    <p>{{ucwords($project->timeline_type)}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Expected Start Date') }}</label>
                                    <p>{{hcms_date(strtotime($project->expected_start_date),'date')}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Expected End Date') }}</label>
                                    <p>{{hcms_date(strtotime($project->expected_end_date),'date')}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Bidding Start Date') }}</label>
                                    <p>{{hcms_date(strtotime($project->bid_start_date),'date')}}</p>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="data-view">
                                    <label>{{ __('Bidding End Date') }}</label>
                                    <p>{{hcms_date(strtotime($project->bid_end_date),'date')}}</p>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Milestone Details</legend>
                        <div class="row">
                            <div class="col-12">
                                <table class="table w-100">
                                    <thead>
                                        <th>Milestone Name</th>
                                        <th>Amount (USD)</th>
                                        <th>Description</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($project->milestones as $milestone)
                                            <tr>
                                                <td>{{$milestone->milestone_name}}</td>
                                                <td>{{$milestone->milestone_amount}}</td>
                                                <td>{{$milestone->milestone_description}}</td>
                                                <td>{{hcms_date(strtotime($milestone->start_date),'date')}}</td>
                                                <td>{{hcms_date(strtotime($milestone->end_date),'date')}}</td>
                                                <td>
                                                    @if($milestone->status == 0)
                                                        {{__('Not Started')}}
                                                    @elseif($milestone->status == 1)
                                                        {{__('Activated')}}
                                                    @elseif($milestone->status == 2)
                                                        {{__('In Progress')}}
                                                    @elseif($milestone->status == 3)
                                                        {{__('Completed')}}
                                                    @elseif($milestone->status == 4)
                                                        {{__('Approved')}}
                                                    @elseif($milestone->status == 5)
                                                        {{__('Paid')}}
                                                    @else
                                                        {{__('Rejected')}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Documents</legend>
                        @if(!empty($project->documents))
                            <div class="col-md-12 mb-3">
                                <table class="table w-100">
                                    <thead>
                                        <th>Sl No</th>
                                        <th>Documents</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($project->documents as $doc)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td><a href="{{asset('storage/projects/project_documents/'.$doc->file_name)}}" class="pr-docs">{{$doc->file_name}}</a></td>
                                                <td>
                                                    <button type="button" title="View" class="btn btn-icon btn-success btn-sm" onclick="window.location.href='{{asset('storage/projects/project_documents/'.$doc->file_name)}}'"><i class="mdi mdi-eye"></i> </button>
                                                    <a href="{{asset('storage/projects/project_documents/'.$doc->file_name)}}" class="btn btn-icon btn-info btn-sm" download="{{$doc->file_name}}"><i class="mdi mdi-download"></i></a>
                                                </td>
                                            </tr>
                                            @php
                                                $i++;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    </fieldset>
                </div>
            </div>
        </div> <!-- end card body-->
    </div> <!-- end card -->
</div><!-- end col-->
@if ($project->active == 1)
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Deactivate Project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.projects.updateStatus',$project->uuid)}}" method="POST" class="needs-validation" novalidate>
                        @method('PUT')
                        @csrf
                        <label for="inactive-reason">{{__('Reason for Deactivation')}} <span class="mandatory">*</span></label>
                        <textarea name="inactive_reason" rows="10" class="form-control" required></textarea>
                        <div class="invalid-feedback"  @if ($errors->has('title')) style="display:block" @endif>
                            @if ($errors->has('inactive_reason'))
                                {{ $errors->first('inactive_reason') }}
                            @else 
                                {{ __('Reason for Deactivation cannot be empty.') }}
                            @endif
                        </div>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Activate Project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.projects.updateStatus',$project->uuid)}}" method="POST" class="needs-validation" novalidate>
                        @method('PUT')
                        @csrf
                        Do you want to activate this project ?
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
