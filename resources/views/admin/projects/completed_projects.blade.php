@extends('admin.layouts.app')
@section('title', set_page_titile(__('Completed Projects')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('open_projects') }}
            <h4 class="page-title">{{ __('Completed Project') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="{{ route('admin.projects.index') }}" aria-expanded="false" class="nav-link">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('Open Projects') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.projects.list_inprogress') }}" aria-expanded="false" class="nav-link ">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('Inprogress Projects') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.projects.list_completed') }}" aria-expanded="false" class="nav-link active">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('Completed Projects') }}</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content p-3">
                    <div class="row">
                        @can('add projects')
                            <div class="col-sm-4">
                                <button onclick="location.href='{{ route('admin.projects.create') }}'" type="button" class="btn btn-danger mb-3"><i class="mdi mdi-plus"></i> {{ __('
                                Create Project') }}</button>
                            </div>
                        @endcan
                    </div>
                    {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                    <div class="col-12 text-right px-0">
                        <div class="form-group row">
                            <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                            <div class="col-sm-1 mt-1">
                                <input type="text" id="pageNum" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}

<script>
    $(document).ready(function () {
        $('#leads-table_wrapper .dt-buttons').hide();
    });
    $(document).on('change','input[type=checkbox]',function(){
    if(!$(this).hasClass('bulk-slct')){
        Swal.fire({
            title: 'Are you sure?',
            text: "The current status of the client will be changed.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Update Status!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "/admin/user/toggle-active-status/"+$(this).attr('data-user')
                });
                Swal.fire(
                    'Status Updated!',
                    'The status of the client has been updated.',
                    'success'
                )
                setTimeout(function(){
                $(".status-btns .btn-primary").click();
                },1);
            } else {
                if($(this).prop("checked") ==  true){
                    $(this).prop("checked",false);
                } else {
                    $(this).prop("checked",true);
                }
            }
        })
    }
});


function hideProject(id,status){


    // Swal.fire({
    //         title: 'Are you sure?',
    //         text: "The current status of the admin user will be changed.",
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, Update Status!'
    //     }).then((result) => {
    //         if (result.value) {
    //             $.ajaxSetup({
    //                 headers: {
    //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                 }
    //             });
    //             $.ajax({
    //                 type: "GET",
    //                 url: "/admin/projects/hide/{id}/{status}"
    //             });
    //             Swal.fire(
    //                 'Status Updated!',
    //                 'The status of the admin user has been updated.',
    //                 'success'
    //             )
    //             setTimeout(function(){
    //             $(".status-btns .btn-primary").click();
    //             },1);
    //         } else {
    //             if($(this).prop("checked") ==  true){
    //                 $(this).prop("checked",false);
    //             } else {
    //                 $(this).prop("checked",true);
    //             }
    //         }
    //     })




// Swal.fire({
//     title: "Are you sure?",
//     text: "Do you want to change status?",
 
//     showCancelButton: true,
//     confirmButtonColor: '#DD6B55',
//     confirmButtonText: 'Yes',
//     cancelButtonText: "No",
// })
// .then((changestatus) => {
// console.log(changestatus)
// if (changestatus.value) {
//     alert("hjgkj");
// $.get("{{route('admin.projects.hidePublic')}}", {id:id,status:status},
//     function (data, textStatus, jqXHR) {
//         location.reload();
//       //  LaravelDataTables["basic-datatable"].draw(true);
//     },
    
// );
// } 
// });


}
</script>
<script> 
 $(document).on('change','input[type=checkbox]',function(){
    if(!$(this).hasClass('bulk-slct')){
        Swal.fire({
            title: 'Are you sure?',
            text: "The status of the project will be closed.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Update Status!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "/admin/project-close/toggle-active-status/"+$(this).attr('data-user')
                });
                Swal.fire(
                    'Status Updated!',
                    'The status of the project has been closed.',
                    'success'
                )
                setTimeout(function(){
                $(".status-btns .btn-primary").click();
                },1);
            } else {
                if($(this).prop("checked") ==  true){
                    $(this).prop("checked",false);
                } else {
                    $(this).prop("checked",true);
                }
            }
        })
    }
});
</script>

@endpush
