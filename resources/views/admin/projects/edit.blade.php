@extends('admin.layouts.app')
@section('title', set_page_titile($project->title.' | Edit Project'))
@push('custom-styles')
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('edit_projects',$project) }}
            </div>
            <h4 class="page-title">{{$project->title}} - {{ __('Edit Project') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('admin.projects.update',$project)}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
                    @csrf
                    @method('PUT')
                    <fieldset>
                        <legend>Project Details</legend>
                        <div class="row">
                            {{-- <div class="col-lg-6 @if ($errors->has('user_id')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="user_id">{{ __('Select Client') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="user_id" class="form-control" id="user_id" required>
                                        <option value=""></option>
                                        @foreach ($users as $user)
                                            <option value="{{$user->id}}" {{old('user_id')== $user->id ?'selected' :''}}>{{$user->full_name.' ('.$user->email.')'}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('user_id'))
                                            {{ $errors->first('user_id') }}
                                        @else 
                                            {{ __('Client cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div> --}}
                            <div class="col-lg-6 @if ($errors->has('title')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="title">{{ __('Title') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('title',$project->title)}}" type="text" name="title" class="form-control" id="title" minlength="3" maxlength="150" placeholder="{{__('Enter Project Title')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('title')) style="display:block" @endif>
                                        @if ($errors->has('title'))
                                            {{ $errors->first('title') }}
                                        @else 
                                            {{ __('Title cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('location')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="location">{{ __('Location') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('location',$project->location)}}" type="text" name="location" class="form-control" id="location" minlength="3" maxlength="150" placeholder="{{__('Enter Location')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('location')) style="display:block" @endif>
                                        @if ($errors->has('location'))
                                            {{ $errors->first('location') }}
                                        @else 
                                            {{ __('Location cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 @if ($errors->has('description')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="description">{{ __('Description') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <textarea name="description" id="description" class="form-control" rows="20" required>{{old('description',$project->description)}}</textarea>
                                    <div class="invalid-feedback"  @if ($errors->has('description')) style="display:block" @endif>
                                        @if ($errors->has('description'))
                                            {{ $errors->first('description') }}
                                        @else 
                                            {{ __('Description cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 @if ($errors->has('project_documents')) validation-failed @endif">
                                <label for="project_documents">{{ __('Project Documents (Upto 5 Files)') }}
                                    <span class="mandatory">*</span>
                                </label>
                                    @if(!empty($project->documents))
                                        @foreach ($project->documents as $doc)
                                            @php
                                                $ext = pathinfo($doc->file_name, PATHINFO_EXTENSION);
                                            @endphp
                                            <div class="mb-2">
                                                <i class="mdi mdi-file" aria-hidden="true"></i> <a href="{{asset('storage/projects/project_documents/'.$doc->file_name)}}" class="pr-docs">{{$doc->file_name}}</a>
                                                &nbsp;<span class="bg-danger uploaded_images text-light cursor-pointer" onclick='remove_doc({{ $doc->id }},this)'>X</span>
                                            </div>
                                        @endforeach
                                    @endif
                                <div class="fallback dropzone dropzone_docs"></div>
                                <div class="imp_images"></div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="mt-3">
                        <legend>Required Expertise Details</legend>
                        <div class="row">
                            <div class="col-lg-6 @if ($errors->has('industry_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="industry_ids">{{ __('Select Industries') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="industry_ids[]" id="industry_ids" class="form-control" multiple="multiple" required>
                                        <option value=""></option>
                                        @foreach ($industries as $industry)
                                            <option value="{{$industry->id}}" {{in_array($industry->id,old('industry_ids',$slctd_industries)) ? 'selected' : ''}}>{{$industry->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('industry_ids'))
                                            {{ $errors->first('industry_ids') }}
                                        @else 
                                            {{ __('Industries cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-lg-6 @if ($errors->has('categories_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="categories_ids">{{ __('Select Categories') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="categories_ids[]" id="category_ids" class="form-control" multiple="multiple" required>
                                        <option value=""></option>
                                        @foreach ($categories_list as $category)
                                        <option value="{{$category->id}}" {{in_array($category->id,$slctd_categories) ? 'selected' : ''}}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('categories_ids'))
                                            {{ $errors->first('categories_ids') }}
                                        @else 
                                            {{ __('Categories cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php
                                if(!empty(old('categories_ids'))){
                                    $old_category = old('categories_ids');
                                    $old_sub_categories = App\SubCategory::whereHas('categories',function($q) use($old_category){
                                                $q->whereIn('id',$old_category);
                                            })->where(['active'=>1])->pluck('name','id');
                                }else{
                                    $old_sub_categories = [];
                                }
                            @endphp
                            <div class="col-lg-6 @if ($errors->has('sub_categories_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="sub_categories_ids">{{ __('Select Sub Categories') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="sub_categories_ids[]" id="sub_category_ids" class="form-control" multiple="multiple" required>
                                        <option value=""></option>
                                        @if(!empty($old_sub_categories))
                                            @foreach ($old_sub_categories as $key=>$sub_category)
                                                <option value="{{$key}}" {{!empty(old('sub_categories_ids')) ? in_array($key,old('sub_categories_ids')) ? 'selected' : '' : ''}}>{{$sub_category}}</option>
                                            @endforeach
                                        @else
                                            @foreach ($sub_categories as $sub_category)
                                                <option value="{{$sub_category->id}}" {{in_array($sub_category->id,$slctd_sub_categories) ? 'selected' : ''}}>{{$sub_category->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('sub_categories_ids'))
                                            {{ $errors->first('sub_categories_ids') }}
                                        @else 
                                            {{ __('Sub Categories cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php
                                if(!empty(old('sub_categories_ids'))){
                                    $old_sub_category = old('sub_categories_ids');
                                    $old_skills = App\Skill::whereHas('subCategories',function($q) use($old_sub_category){
                                                    $q->whereIn('id',$old_sub_category);
                                                })->where(['active'=>1])->pluck('name','id');
                                }else{
                                    $old_skills =  App\Skill::where(['active'=>1])->pluck('name','id');
                                }
                            @endphp
                            <div class="col-lg-6 @if ($errors->has('skills')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="skills">{{ __('Select Skills') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="skills[]" id="skill_ids" class="form-control" multiple="multiple" required>
                                        <option value=""></option>
                                        @if(!empty($old_skills))
                                            @foreach ($old_skills as $key=>$skill)
                                                <option value="{{$key}}" {{!empty(old('skills')) ? in_array($key,old('skills')) ? 'selected' : '' : ''}}>{{$skill}}</option>
                                            @endforeach
                                        @else
                                            @foreach ($skills as $skill)
                                                <option value="{{$skill->id}}" {{in_array($skill->id,$slctd_skills) ? 'selected' : ''}}>{{$skill->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('skills'))
                                            {{ $errors->first('skills') }}
                                        @else 
                                            {{ __('Skills cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                    </fieldset>
                    <fieldset class="mt-3">
                        <legend>Proposal Details</legend>
                        <div class="row">
                            <div class="col-lg-6 @if ($errors->has('type')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="type">{{ __('Type') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <input type="radio" id="type_fixed_rate" name="type" value="0" {{old('type',$project->type)==0?'checked':''}} required>
                                    <label for="type_fixed_rate">{{ __('Fixed Rate') }}</label>&nbsp;
                                    <input type="radio" id="type_hourly_rate" name="type" value="1" {{old('type',$project->type)==1?'checked':''}} required>
                                    <label for="type_hourly_rate">{{ __('Hourly Rate') }}</label>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('type'))
                                            {{ $errors->first('type') }}
                                        @else 
                                            {{ __('Type cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group required">
                                    <label for="title" class="control-label">{{__('Budget') }} <span class="mandatory">*</span></label>
                                    <select class="form-control" name="budget_range" id="budget_range" required>
                                        <option disabled selected value=""> Select Budget</option>
                                        <option value="0-5000" {{old('timeline_type',$project->budget_from)=="0" ? 'selected' : ''}} >{{__('Below 5000') }}</option>
                                        <option value="5000-10000" {{old('timeline_type',$project->budget_from)=="5000" ? 'selected' : ''}} >{{__('5000 - 10000') }}</option>
                                        <option value="10000-20000" {{old('timeline_type',$project->budget_from)=="10000" ? 'selected' : ''}}>{{__('10000 - 20000') }}</option>
                                        <option value="20000-50000" {{old('timeline_type',$project->budget_from)=="20000" ? 'selected' : ''}}>{{__('20000 - 50000') }}</option>
                                    </select>
        
                                    <span id="budget-from-error" class="error-txt budget-from-error"></span>
                                </div>
                            </div>
                            <!-- <div class="col-lg-6 @if ($errors->has('budget_from')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="budget_from">{{ __('Budget From') }} ({{config('data.currency_symbol')}})
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('budget_from',$project->budget_from)}}" type="number" name="budget_from" class="form-control no-inc numericOnly" id="budget_from" min="1" max="9999999999" placeholder="{{__('Enter Budget From Amount')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('budget_from')) style="display:block" @endif>
                                        @if ($errors->has('budget_from'))
                                            {{ $errors->first('budget_from') }}
                                        @else 
                                            {{ __('Budget From cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('budget_to')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="budget_to">{{ __('Budget To') }} ({{config('data.currency_symbol')}})
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('budget_to',$project->budget_to)}}" type="number" name="budget_to" class="form-control no-inc numericOnly" id="budget_to" min="1" max="9999999999" placeholder="{{__('Enter Budget To Amount')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('budget_to')) style="display:block" @endif>
                                        @if ($errors->has('budget_to'))
                                            {{ $errors->first('budget_to') }}
                                        @else 
                                            {{ __('Budget To cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-lg-6 @if ($errors->has('timeline_count')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="timeline_count">{{ __('Timeline') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('timeline_count',$project->timeline_count)}}" type="number" name="timeline_count" class="form-control no-inc numericOnly" id="timeline_count" min="1" max="9999" placeholder="{{__('Count of hour, days, week or months')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('timeline_count')) style="display:block" @endif>
                                        @if ($errors->has('timeline_count'))
                                            {{ $errors->first('timeline_count') }}
                                        @else 
                                            {{ __('Timeline cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('timeline_type')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="timeline_type">{{ __('Timeline Type') }}
                                        <span class="mandatory">*</span>
                                    </label> 
                                    <select name="timeline_type" class="form-control" id="timeline_type" required>
                                        <option value="">Select Type</option>
                                        <option value="Hours" {{old('timeline_type',$project->timeline_type)=="Hours" ? 'selected' : ''}}>Hours</option>
                                        <option value="Days" {{old('timeline_type',$project->timeline_type)=="Days" ? 'selected' : ''}}>Days</option>
                                        <option value="Weeks" {{old('timeline_type',$project->timeline_type)=="Weeks" ? 'selected' : ''}}>Weeks</option>
                                        <option value="Months" {{old('timeline_type',$project->timeline_type)=="Months" ? 'selected' : ''}}>Months</option>
                                    </select>
                                    <div class="invalid-feedback"  @if ($errors->has('timeline_type')) style="display:block" @endif>
                                        @if ($errors->has('timeline_type'))
                                            {{ $errors->first('timeline_type') }}
                                        @else 
                                            {{ __('Timeline Type cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('expected_start_date')) validation-failed @endif" >
                                <label for="expected_start_date">{{ __('Expected Start Date') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{old('expected_start_date',date('d-m-Y',strtotime($project->expected_start_date)))}}" name="expected_start_date" class="form-control no-type" required autocomplete="off" id="expected_start_date" placeholder="{{ __('Select Date') }}">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('expected_start_date'))
                                        {{ $errors->first('expected_start_date') }}
                                    @else 
                                        {{ __('Expected Start Date cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('expected_end_date')) validation-failed @endif mb-2" >
                                <label for="expected_end_date">{{ __('Expected End Date') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{old('expected_end_date',date('d-m-Y',strtotime($project->expected_end_date)))}}" name="expected_end_date" class="form-control no-type" required autocomplete="off" id="expected_end_date" placeholder="{{ __('Select Date') }}">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('expected_end_date'))
                                        {{ $errors->first('expected_end_date') }}
                                    @else 
                                        {{ __('Expected End Date cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('bid_start_date')) validation-failed @endif mb-2" >
                                <label for="bid_start_date">{{ __('Bidding Start Date') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{old('bid_start_date',date('d-m-Y',strtotime($project->bid_start_date)))}}" name="bid_start_date" class="form-control no-type" required autocomplete="off" id="bid_start_date" placeholder="{{ __('Select Date') }}">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('bid_start_date'))
                                        {{ $errors->first('bid_start_date') }}
                                    @else 
                                        {{ __('Bidding Start Date cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('bid_end_date')) validation-failed @endif" >
                                <label for="bid_end_date">{{ __('Bidding End Date') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{old('bid_end_date',date('d-m-Y',strtotime($project->bid_end_date)))}}" name="bid_end_date" class="form-control no-type" required autocomplete="off" id="bid_end_date" placeholder="{{ __('Select Date') }}">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('bid_end_date'))
                                        {{ $errors->first('bid_end_date') }}
                                    @else 
                                        {{ __('Bidding End Date cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="mt-3">
                        <legend>Milestone Details</legend>
                        <div class="col-lg-12 @if ($errors->has('milestone_name')) validation-failed @endif">
                            <div class="form-group">
                                <div class="row">
                                    <table class="table">
                                    <tbody>
                                        <tr>
                                            <td scope="row">
                                                <label for="milestone_names">Name <span class="mandatory">*</span></label>
                                            </td>
                                            <td scope="row">
                                                <label for="milestone_amount">Amount (USD) <span class="mandatory">*</span></label>
                                            </td>
                                            <td scope="row">
                                                <label for="milestone_descriptions">Description <span class="mandatory">*</span></label>
                                            </td>
                                            <td scope="row">
                                                <label for="milestone_descriptions">Start date <span class="mandatory">*</span></label>
                                            </td>
                                            <td scope="row">
                                                <label for="milestone_descriptions">End Date<span class="mandatory">*</span></label>
                                            </td>
                                        </tr>
                                        @if(!empty(old('milestone_names')))
                                            @foreach (old('milestone_names') as $key=>$milestone)
                                                <tr>
                                                    <input type="hidden" name="milestone_id[]" value="{{old('milestone_id')[$key]}}">
                                                    <td scope="row">
                                                        <input  type="text" name="milestone_names[]" class="form-control milestone_names" value="{{$milestone}}" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Name')}}" required>
                                                    </td>
                                                    <td scope="row">
                                                        <input  type="number" name="milestone_amounts[]" class="form-control no-inc numericOnly" value="{{old('milestone_amounts')[$key]}}" min="1" max="9999999999" id="milestone_amount" placeholder="{{__('Amount')}}" required>
                                                    </td>
                                                    <td scope="row">
                                                        <input type="text" name="milestone_start_dates[]" class="form-control no-type milestone_start_date" value="{{old('milestone_start_dates')[$key]}}" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                                                    </td>
                                                    <td scope="row">
                                                        <input type="text" name="milestone_end_dates[]" class="form-control no-type milestone_end_date" value="{{old('milestone_end_dates')[$key]}}" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                                                    </td>
                                                    <td scope="row">
                                                        <textarea name="milestone_descriptions[]" id="milestone_descriptions" class="form-control" placeholder="Description" required>{{old('milestone_descriptions')[$key]}}</textarea>
                                                    </td>
                                                    <td><button class="btn btn-success add_more" type="button">+</button><button type="button" class="btn btn-danger ml-2 remove_row">-</button></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            @foreach ($project->milestones as $milestone)
                                                <tr>
                                                    <input type="hidden" name="milestone_id[]" value="{{$milestone->id}}">
                                                    <td scope="row">
                                                        <input  type="text" name="milestone_names[]" value="{{$milestone->milestone_name}}" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Name')}}" required>
                                                    </td>
                                                    <td scope="row">
                                                        <input  type="number" name="milestone_amounts[]" value="{{$milestone->milestone_amount}}" class="form-control no-inc numericOnly" min="1" max="9999999999" id="milestone_amount" placeholder="{{__('Amount')}}" required>
                                                    </td>
                                                    <td scope="row">
                                                        <input type="text" name="milestone_start_dates[]" value="{{date('d-m-Y',strtotime($milestone->start_date))}}" class="form-control no-type milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                                                    </td>
                                                    <td scope="row">
                                                        <input type="text" name="milestone_end_dates[]" value="{{date('d-m-Y',strtotime($milestone->end_date))}}" class="form-control no-type milestone_end_date" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                                                    </td>
                                                    <td scope="row">
                                                        <textarea name="milestone_descriptions[]" id="milestone_descriptions" class="form-control" placeholder="Description" required>{{$milestone->milestone_description}}</textarea>
                                                    </td>
                                                    <td><button class="btn btn-success add_more" type="button">+</button><button type="button" class="btn btn-danger ml-2 remove_row">-</button></td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div> 
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
    <script>
        $('#user_id').select2({
            placeholder: "{{__('Select a Client')}}",
        });
        $('#industry_ids').select2({
            placeholder: "{{__('Select Industries')}}",
        });
        $('#category_ids').select2({
            placeholder: "{{__('Select Categories')}}",
        });
        $('#sub_category_ids').select2({
            placeholder: "{{__('Select Sub Categories')}}",
        });
        $('#skill_ids').select2({
            placeholder: "{{__('Select Skills')}}",
        });
      
        $('#category_ids').change(function(){
            var sub_category_ids = $('#sub_category_ids').val();
            $.ajax({
                type: "GET",
                url: "{{url('/get-sub-categories')}}",
                data: {'category_ids':$(this).val()},
                success: function (response) {
                    var new_options = "<option value =''></option>";
                    $.each(response, function (index, value) { 
                        if(jQuery.inArray(index, sub_category_ids) !== -1){
                            new_options += "<option value='"+index+"' selected>"+value+"</option>";
                        }else{
                            new_options += "<option value='"+index+"'>"+value+"</option>";
                        }
                    });
                    $('#sub_category_ids').html(new_options);
                    $('#sub_category_ids').change();
                    $('#sub_category_ids').select2({
                        placeholder: "{{__('Select Sub Categories')}}",
                    });
                }
            });
        });
        $('#sub_category_ids').change(function(){
            var skill_ids = $('#skill_ids').val();
            $.ajax({
                type: "GET",
                url: "{{url('/get-skills')}}",
                data: {'sub_category_ids':$(this).val()},
                success: function (response) {
                    var new_options = "<option value =''></option>";
                    $.each(response, function (index, value) { 
                        if(jQuery.inArray(index, skill_ids) !== -1){
                            new_options += "<option value='"+index+"' selected>"+value+"</option>";
                        }else{
                            new_options += "<option value='"+index+"'>"+value+"</option>";
                        }
                    });
                    $('#skill_ids').html(new_options);
                    $('#skill_ids').select2({
                        placeholder: "{{__('Select Sub Categories')}}",
                    });
                }
            });
        });
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone_docs", {
            url: "{{route('admin.projects.fileUpload') }}",
            maxFilesize: 5, // MB
            maxFiles: Number(5) - Number("{{!empty($project->documents)?count($project->documents):0}}"),
            dictDefaultMessage: "File size should be less than 5 MB",
            addRemoveLinks: true,
            headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
            success: function(file,response){
                $('#dropzone-docs-error').remove();
                $('<input>').attr({
                    type: 'hidden',
                    name: 'project_documents[]',
                    value: response.name,
                    id: response.original_name,
                    class: 'project_documents'
                }).appendTo('.imp_images');
            },
            removedfile: function(file){
                $('input[name="project_documents[]"]').each(function(){
                    if($(this).attr('id') == file.name){
                        $(this).remove();
                    }
                });
                file.previewElement.remove();
            }
        });
        $('input[name="expected_start_date"]').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker: false,
                minDate:new Date(),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                autoApply: true
        });
        $('input[name="expected_start_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('input[name="expected_end_date"]').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker: false,
                minDate:new Date(),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                autoApply: true
        });
        $('input[name="expected_end_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('input[name="bid_start_date"]').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker: false,
                minDate:new Date(),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                autoApply: true
        });
        $('input[name="bid_start_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('input[name="bid_end_date"]').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker: false,
                minDate:new Date(),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                autoApply: true
        });
        $('input[name="bid_end_date"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $(document).on("click",'.add_more',function(){
            var $tr    = $(this).closest('tr');
            var $clone = $tr.clone();
            $clone.find('input').val('');
            $clone.find('textarea').val('');
            $tr.after($clone);
            $('.milestone_start_date').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker: false,
                minDate:new Date(),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                autoApply: true
            });
            $('.milestone_start_date').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY'));
            });
            $('.milestone_end_date').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker: false,
                minDate:new Date(),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                autoApply: true
            });
            $('.milestone_end_date').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY'));
            });
        });
        $(document).on("click",'.remove_row',function(){
            if( $('.milestone_names').length == 1 ) {
                alert('Can not delete this row');
            }else{
                var $tr    = $(this).closest('tr').remove();
            }
        });
        $('#project-form').submit(function(e){
            if(($('.project_documents').length == 0 && $('.pr-docs').length == 0) || $('.milestone_names').length == 0){
                e.preventDefault();
                if($('.project_documents').length == 0){
                    $('.dropzone_docs').after('<div class="error-msg" id="dropzone-docs-error">Please add atleast one document</div>')
                }
            }
        });
        $('.milestone_start_date').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker: false,
                minDate:new Date(),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                autoApply: true
        });
        $('.milestone_start_date').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('.milestone_end_date').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            timePicker: false,
            minDate:new Date(),
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoApply: true
        });
        $('.milestone_end_date').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        function remove_doc(id,el){
            if(confirm("Do You Want to Remove This File")){
                $.get("{{ route('admin.projects.removeFile')  }}", {id:id},
                    function (data, textStatus, jqXHR) {
                        $(el).parent("div").remove();
                        $('.dropzone_docs')[0].dropzone.options.maxFiles = Number(5) - Number($('.pr-docs').length);
                    },
                );
            }
        }
    </script>
@endpush
