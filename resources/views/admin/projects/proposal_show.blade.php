@extends('admin.layouts.app')
@section('title', set_page_titile($project->title.' | Proposal Show'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('show_projects',$project)}}
            </div>
            <h4 class="page-title">{{$proposal->user->full_name}}'s Proposal - {{ucwords($project->title)}}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <div class="data-view">
                            <label>{{ __('Freelancer') }}</label>
                            <p><a href = "{{route('admin.freelancers.show',$proposal->user)}}">{{$proposal->user->full_name}}</a></p>
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <div class="data-view">
                            <label>{{ __('Proposal Amount') }}</label>
                            <p>{{formatAmount($proposal->proposal_amount)}}</p>
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <div class="data-view">
                            <label>{{ __('Proposal Date') }}</label>
                            <p>{{hcms_date(strtotime($proposal->created_at),'date-time')}}</p>
                        </div>
                    </div>
                    <div class="col-md-12 mb-3">
                        <div class="data-view">
                            <label>{{ __('Proposal Details') }}</label>
                            <p>{!!$proposal->proposal_document!!}</p>
                        </div>
                    </div>
                    <div class="col-md-3 mb-3">
                        <div class="data-view">
                            <label>{{ __('Status') }}</label>
                            <p>{{$proposal->apply_status}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
@endsection
