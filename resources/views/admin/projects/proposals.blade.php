@extends('admin.layouts.app')
@section('title', set_page_titile($project->title.' | Project Proposals'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('show_projects',$project)}}
            </div>
            <h4 class="page-title">{{ucwords($project->title)}}  - Proposals</h4>
        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a href="{{ route('admin.projects.show',$project) }}" aria-expanded="false" class="nav-link ">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Project Details') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.projects.proposals',$project) }}" aria-expanded="false" class="nav-link active">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Proposals') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.projects.transactions',$project) }}" aria-expanded="false" class="nav-link ">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Transactions') }}</span>
        </a>
    </li>
    <!-- <li class="nav-item">
        <a href="{{ route('admin.projects.chats',$project) }}" aria-expanded="false" class="nav-link ">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Chats') }}</span>
        </a>
    </li> -->
</ul>
<div class="tab-content p-3">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>Sl.No</th>
                            <th>Freelancer</th>
                            <th>Proposed Amount</th>
                            <th>Bid Date</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @if(count($project->proposals)>0)
                                @foreach ($project->proposals as $proposal)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$proposal->user->full_name}}</td>
                                        <td>{{formatAmount($proposal->proposal_amount)}}</td>
                                        <td>{{hcms_date(strtotime($proposal->created_at),'date-time') }}</td>
                                        <td>
                                            <button type="button" class="btn btn-icon btn-success btn-sm" title="View" onclick="window.location.href='{{route('admin.projects.proposal_show', ['proposal'=>$proposal,'project'=>$project])}}'"><i class="mdi mdi-eye"></i> </button>
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5" align="center">No proposal for this project</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end card body-->
    </div> <!-- end card -->
</div><!-- end col-->
@endsection
