@extends('admin.layouts.app')
@section('title', set_page_titile(__('Rating Questions')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('ratingQuestions') }}
            <h4 class="page-title">{{ __('Rating Questions') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                       @can('add rating questions')
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-danger mb-3" data-toggle="modal" data-target="#addQuestion"><i class="mdi mdi-plus"></i> {{ __('
                            Add Question') }}</button>
                        </div>
                       @endcan
                </div>
                {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                <div class="col-12 text-right px-0">
                    <div class="form-group row">
                        <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                        <div class="col-sm-1 mt-1">
                            <input type="text" id="pageNum" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@can('add rating questions')
    <div class="modal fade" id="addQuestion" tabindex="-1" role="dialog" aria-labelledby="addQuestion" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="addQuestionLabel">{{ __('Add Question') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form action="{{route('admin.rating_questions.store')}}" method="POST" class="needs-validation" novalidate>
                @csrf
                <div class="modal-body">

                    <div class="form-group mb-3">
                        <label for="validationCustom01">{{ __('User Type') }}
                            <span class="mandatory">*</span>
                        </label>
                        <div class="">
                                <input type="radio" name="user_type" value="client" class="" id="" checked required>
                                <label class="wei-400 ml-1 mr-2">Client</label>

                                <input type="radio" name="user_type" value="freelancer" class="" id=""  required>
                                <label class="wei-400 ml-1 mr-2">Freelancer</label>

                                <input type="radio" name="user_type" value="company" class="" id=""  required>
                                <label class="wei-400 ml-1">Company</label>
                        </div>
                        <div class="invalid-feedback" >
                            @if ($errors->has('user_type'))
                                {{ $errors->first('user_type') }}
                            @else 
                                {{ __('User type cannot be empty.')}}
                            @endif
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <label for="validationCustom02">{{ __('Question') }}
                            <span class="mandatory">*</span>
                        </label>
                        <textarea type="text" name="question" class="form-control" required  placeholder="Question" autocomplete="off"></textarea>
                        <span class="invalid-feedback error-txt">
                        @if ($errors->has('question'))
                        {{ $errors->first('question') }}
                        @else
                        {{ __('Question cannot be empty.') }}
                        @endif
                        </span>
                    </div>
                    
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        </div>
    </div>
@endcan


@can('edit rating questions')
    <div class="modal fade" id="edit-rating" tabindex="-1" role="dialog" aria-labelledby="addQuestion" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="addQuestionLabel">{{ __('Edit Question') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form method="POST" class="needs-validation edit-form" novalidate>
                @csrf
                @method('put')
                <div class="modal-body">

                    <div class="form-group mb-3">
                        <label for="validationCustom01">{{ __('User Type') }}
                            <span class="mandatory">*</span>
                        </label>
                        <div class="">
                                <input type="radio" name="user_type_edit" value="client" class="" id="client_id" required>
                                <label class="wei-400 ml-1 mr-2">Client</label>

                                <input type="radio" name="user_type_edit" value="freelancer" class="" id="free_id"  required>
                                <label class="wei-400 ml-1 mr-2">Freelancer</label>

                                <input type="radio" name="user_type_edit" value="company" class="" id="company_id"  required>
                                <label class="wei-400 ml-1">Company</label>
                        </div>
                        <div class="invalid-feedback" >
                            @if ($errors->has('user_type_edit'))
                                {{ $errors->first('user_type_edit') }}
                            @else 
                                {{ __('User type cannot be empty.')}}
                            @endif
                        </div>
                    </div>


                    <div class="form-group mb-3">
                        <label for="validationCustom02">{{ __('Question') }}
                            <span class="mandatory">*</span>
                        </label>
                        <textarea type="text" name="question" id="question_id" class="form-control" required  placeholder="Question" autocomplete="off"></textarea>
                        <span class="invalid-feedback error-txt">
                        @if ($errors->has('question'))
                        {{ $errors->first('question') }}
                        @else
                        {{ __('Question cannot be empty.') }}
                        @endif
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        </div>
    </div>
@endcan


@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}


<script>
    function edit(ratingQuestion){
        
        if($(ratingQuestion).data('user_type') == 'client'){
            $("#client_id").prop("checked", true); 
        }
        if($(ratingQuestion).data('user_type') == 'freelancer'){
            $("#free_id").prop("checked", true); 
        }
        if($(ratingQuestion).data('user_type') == 'company'){
            $("#company_id").prop("checked", true); 
        }
        $('#question_id').val($(ratingQuestion).data('question'));
        $('.edit-form').attr('action',$(ratingQuestion).data('link'));
        $('#edit-rating').modal('show');
    }
    
</script>

@endpush
