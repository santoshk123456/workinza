@extends('admin.layouts.app')
@section('title', set_page_titile(__($user->full_name.' | Reviews & Ratings')))
@push('custom-styles')
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('users.reviewsRatings',$user) }}
            </div>
            <h4 class="page-title">{{ "Reviews & Ratings "}}{{$user->full_name}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               <div class="row mb-3">
                <div class="col-12 text-right">
                        <div class="master-actions-inner">
                       
                         <button  onclick="location.href='{{ route('admin.users.reviewsRatings',$user) }}'" type="button" class="btn btn-primary btn-sm-multi-common btn-white mb-3"><i class="mdi mdi-format-list-bulleted"></i>List All</button>
                        </div>
                </div>
                </div>


                @if(count($all_ratings))
                    <div class="created-aside d-inline-block w-100">
                        <h4 class="card-title card-title-multi-common float-left">Ratings</h4>
                    </div>
                    <div class="vehicle-basic">
                        <div class="row">
                            <div class="col-12">
                                <table>
                                    <tr>
                                        <th>SL</th>
                                        <th>Project Name</th>
                                        <th>Question</th>
                                        <th>Rating By</th>
                                        <th>Rating To</th>
                                        <th>Ratings</th>
                                        <th>Created On</th>
                                        
                                    </tr>
                                    @foreach($all_ratings as $key=>$rating)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ !empty($rating->project->title)? ucwords($rating->project->title) : "NA"  }}</td>
                                        <td>{{ !empty($rating->ratingQuestion->question)? $rating->ratingQuestion->question : "NA"  }}</td>
                                        <td>{{ !empty($rating->rated_by)? ucwords($rating->rated_by->full_name) : "NA"  }}</td>
                                        <td>{{ !empty($user->full_name)? ucwords($user->full_name) : "NA"  }}</td>
                                        <td><div class="jobseeker-rating" data-value="<?= $rating->rating;?>"></div></td>
                                        <td>{{ !empty($rating->created_at)? hcms_date(strtotime($user->created_at), 'date-time') : "NA" }}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                
                
                    @if(count($company_ratings))
                    <div class="created-aside d-inline-block w-100 mt-5">
                        <h4 class="card-title card-title-multi-common float-left">Company Ratings</h4>
                    </div>
                    <div class="vehicle-basic">
                        <div class="row">
                            <div class="col-12">
                                <table>
                                    <tr>
                                        <th>SL</th>
                                        <th>Project Name</th>
                                        <th>Question</th>
                                        <th>Rating By</th>
                                        <th>Ratings</th>
                                        <th>Created On</th>
                                        
                                    </tr>
                                    @foreach($company_ratings as $key=>$rating)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ !empty($rating->project->title)? ucwords($rating->project->title) : "NA"  }}</td>
                                        <td>{{ !empty($rating->ratingQuestion->question)? $rating->ratingQuestion->question : "NA"  }}</td>
                                        <td>{{ !empty($rating->rated_by)? ucwords($rating->rated_by->full_name) : "NA"  }}</td>
                                        <td><div class="jobseeker-rating" data-value="<?= $rating->rating;?>"></div></td>
                                        <td>{{ !empty($rating->created_at)? hcms_date(strtotime($user->created_at), 'date-time') : "NA" }}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if(count($reviews))
                    <div class="created-aside d-inline-block w-100 mt-5">
                        <h4 class="card-title card-title-multi-common float-left">Reviews</h4>
                    </div>
                    <div class="vehicle-basic">
                        <div class="row">
                            <div class="col-12">
                                <table>
                                    <tr>
                                        <th>SL</th>
                                        <th>Project Name</th>
                                        <th>Review By</th>
                                        <th>Review</th>
                                        <th>Created On</th>
                                        
                                    </tr>
                                    @foreach($reviews as $key=>$review)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ !empty($review->project->title)? ucwords($review->project->title) : "NA"  }}</td>
                                        <td>{{ !empty($review->reviewed_by)? ucwords($review->reviewed_by->full_name) : "NA"  }}</td>
                                        <td>{{ !empty($review->review_user)? ucwords($review->review_user) : "NA"  }}</td>
                                        <td>{{ !empty($review->created_at)? hcms_date(strtotime($review->created_at), 'date-time') : "NA" }}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>


                    <div class="created-aside d-inline-block w-100 mt-5">
                        <h4 class="card-title card-title-multi-common float-left">Company Reviews</h4>
                    </div>
                    <div class="vehicle-basic">
                        <div class="row">
                            <div class="col-12">
                                <table>
                                    <tr>
                                        <th>SL</th>
                                        <th>Project Name</th>
                                        <th>Review By</th>
                                        <th>Review</th>
                                        <th>Created On</th>
                                        
                                    </tr>
                                    @foreach($reviews as $key=>$review)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ !empty($review->project->title)? ucwords($review->project->title) : "NA"  }}</td>
                                        <td>{{ !empty($review->reviewed_by)? ucwords($review->reviewed_by->full_name) : "NA"  }}</td>
                                        <td>{{ !empty($review->review_user)? ucwords($review->review_company) : "NA"  }}</td>
                                        <td>{{ !empty($review->created_at)? hcms_date(strtotime($review->created_at), 'date-time') : "NA" }}</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif
                   

            </div>


        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
@endsection
@push('custom-scripts')

<script>
	$(document).ready(function(){
		$(".jobseeker-rating").each(function (e) {
			var rateValue = $(this).attr('data-value');
			$(this).rateYo({
				rating: rateValue,
				numStars: 5,
				starWidth: "15px",
				spacing: "5px",
				readOnly: true,
			});
		});
		
    });
</script>


@endpush
