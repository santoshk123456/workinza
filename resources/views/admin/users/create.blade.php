@extends('admin.layouts.app')
@section('title', set_page_titile(__('Create Client')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444 !important;
    line-height: 35px !important;
}
.select2-container--default .select2-selection--single {
    border: 1px solid #dee2e6 !important;
    height: 36px !important;
    
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 4px !important;
}
</style>
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('users.create') }}
            </div>
            <h4 class="page-title">{{ __('Create Client') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.users.store')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <fieldset>
                        <legend>{{__('Profile Details')}}</legend>
                        <div class="row">
                            <div class="col-lg-6 @if ($errors->has('full_name')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="full_name">{{ __('Full Name') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('full_name')}}" type="text" name="full_name" class="form-control" min="3" max="100" id="full_name" placeholder="{{__('Enter Full Name')}}" required>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('full_name'))
                                            {{ $errors->first('full_name') }}
                                        @else 
                                            {{ __('Full Name cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('email')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="email">{{ __('Email') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('email')}}" type="email" name="email" class="form-control" id="email" minlength="3" maxlength="50" placeholder="{{__('Enter Email')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('email')) style="display:block" @endif>
                                        @if ($errors->has('email'))
                                            {{ $errors->first('email') }}
                                        @else 
                                            {{ __('Enter valid Email.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('username')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="username">{{ __('Username') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('username')}}" type="text" name="username" class="form-control" id="username" placeholder="{{__('Enter Username')}}" required>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('username'))
                                            {{ $errors->first('username') }}
                                        @else 
                                            {{ __('Username cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                          
                            <div class="col-lg-6 @if ($errors->has('organization_type')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="organization_type">{{ __('Type') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <input type="radio" id="organization_type_company" name="organization_type" value="0" {{old('organization_type')==0?'checked':''}} required>
                                    <label for="organization_type_company">{{ __('Company') }}</label>&nbsp;
                                    <input type="radio" id="organization_type_individual" name="organization_type" value="1" {{old('organization_type')==1?'checked':''}} required>
                                    <label for="organization_type_individual">{{ __('Individual') }}</label>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('organization_type'))
                                            {{ $errors->first('organization_type') }}
                                        @else 
                                            {{ __('Organization Type cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{__('Contact Information')}}</legend>
                        <div class="row">
                            <div class="col-lg-6 @if ($errors->has('address_line_1')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="full_name">{{ __('Address Line 1') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <textarea name="address_line_1" id="address_line_1" class="form-control" min='3' max="300" required>{{old('address_line_1')}}</textarea>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('address_line_1'))
                                            {{ $errors->first('address_line_1') }}
                                        @else 
                                            {{ __('Address Line 1 cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('address_line_2')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="full_name">{{ __('Address Line 2') }}
                                    </label>
                                    <textarea name="address_line_2" id="address_line_2" min='3' max="300" class="form-control">{{old('address_line_2')}}</textarea>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('address_line_2'))
                                            {{ $errors->first('address_line_2') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('country_id')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="country_id">{{ __('Select Country') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="country_id" class="form-control" id="country_id" required>
                                        <option value=""></option>
                                        @foreach ($countries as $country)
                                            <option value="{{$country->id}}" {{old('country_id')== $country->id ?'selected' :''}}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('country_id'))
                                            {{ $errors->first('country_id') }}
                                        @else 
                                            {{ __('Country cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('state')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="state">{{ __('State') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('state')}}" type="text" name="state" class="form-control" id="state" minlength="2" maxlength="100" placeholder="{{__('Enter State')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('state')) style="display:block" @endif>
                                        @if ($errors->has('state'))
                                            {{ $errors->first('state') }}
                                        @else 
                                            {{ __('State can not be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('city')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="city">{{ __('City') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('city')}}" type="text" name="city" class="form-control" id="city" minlength="2" maxlength="100" placeholder="{{ __('Enter City') }}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('city')) style="display:block" @endif>
                                        @if ($errors->has('city'))
                                            {{ $errors->first('city') }}
                                        @else 
                                            {{ __('City can not be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('zipcode')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="zipcode">{{ __('Zip Code') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('zipcode')}}" type="text" name="zipcode" class="form-control" id="zipcode" minlength="3" maxlength="20" placeholder="{{__('Enter Zip Code')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('zipcode')) style="display:block" @endif>
                                        @if ($errors->has('zipcode'))
                                            {{ $errors->first('zipcode') }}
                                        @else 
                                            {{ __('Zip Code can not be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('website_url')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="website_url">{{ __('Website Address') }}
                                    </label>
                                    <input value="{{old('website_url')}}" type="text" name="website_url" class="form-control" id="website_url" minlength="3" maxlength="191" placeholder="{{__('Enter Website Address')}}">
                                    <div class="invalid-feedback"  @if ($errors->has('website_url')) style="display:block" @endif>
                                        @if ($errors->has('website_url'))
                                            {{ $errors->first('website_url') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('phone')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="phone">{{ __('Mobile Number') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input type="hidden" name="country_code" id="country_code" value="{{old('country_code',1)}}">
                                    <input value="{{old('full_number')}}" type="text" name="phone" class="form-control numericOnly" id="phone" minlength="8" maxlength="15" placeholder="{{__('Enter Mobile Number')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('phone')) style="display:block" @endif>
                                        @if ($errors->has('phone'))
                                            {{ $errors->first('phone') }}
                                        @else
                                            {{ __('Phone number can not be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>{{__('Public Profile Details')}}</legend>
                        <div class="row">
                            <div class="col-lg-6 @if ($errors->has('profile_headline')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="profile_headline">{{ __('Profile Headline') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input value="{{old('profile_headline')}}" type="text" name="profile_headline" class="form-control" id="profile_headline" minlength="3" maxlength="250" placeholder="{{__('Enter Profile Headline')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('profile_headline')) style="display:block" @endif>
                                        @if ($errors->has('profile_headline'))
                                            {{ $errors->first('profile_headline') }}
                                        @else 
                                            {{ __('Profile Headline can not be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('industry_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="industry_ids">{{ __('Select Industries') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="industry_ids[]" id="industry_ids" multiple="multiple" class="form-control" required>
                                        <option value=""></option>
                                        @foreach ($industries as $industry)
                                            <option value="{{$industry->id}}" {{!empty(old('industry_ids')) ? in_array($industry->id,old('industry_ids')) ? 'selected' : '' : ''}}>{{$industry->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('industry_ids'))
                                            {{ $errors->first('industry_ids') }}
                                        @else 
                                            {{ __('Industries cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-lg-6 @if ($errors->has('categories_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="categories_ids">{{ __('Select Categories') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="categories_ids[]" id="category_ids" class="form-control" multiple="multiple" required>
                                        <option value=""></option>
                                        @if(!empty($categories_list))
                                            @foreach ($categories_list as $key=>$category)
                                                <option value="{{$category->id}}" {{!empty(old('categories_ids')) ? in_array($key,old('categories_ids')) ? 'selected' : '' : ''}}>{{$category->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('categories_ids'))
                                            {{ $errors->first('categories_ids') }}
                                        @else 
                                            {{ __('Categories cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php
                                if(!empty(old('categories_ids'))){
                                    $old_category = old('categories_ids');
                                    $old_sub_categories = App\SubCategory::whereHas('categories',function($q) use($old_category){
                                                $q->whereIn('id',$old_category);
                                            })->where(['active'=>1])->pluck('name','id');
                                }else{
                                    $old_sub_categories = [];
                                }
                            @endphp
                            <div class="col-lg-6 @if ($errors->has('sub_categories_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="sub_categories_ids">{{ __('Select Sub Categories') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="sub_categories_ids[]" id="sub_category_ids" class="form-control" multiple="multiple" required>
                                        <option value=""></option>
                                        @if(!empty($old_sub_categories))
                                            @foreach ($old_sub_categories as $key=>$sub_category)
                                                <option value="{{$key}}" {{!empty(old('sub_categories_ids')) ? in_array($key,old('sub_categories_ids')) ? 'selected' : '' : ''}}>{{$sub_category}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('sub_categories_ids'))
                                            {{ $errors->first('sub_categories_ids') }}
                                        @else 
                                            {{ __('Sub Categories cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php
                                if(!empty(old('sub_categories_ids'))){
                                    $old_sub_category = old('sub_categories_ids');
                                    $old_skills = App\Skill::whereHas('subCategories',function($q) use($old_sub_category){
                                                    $q->whereIn('id',$old_sub_category);
                                                })->where(['active'=>1])->pluck('name','id');
                                }else{
                                    $old_skills = [];
                                }
                            @endphp
                            <!-- <div class="col-lg-6 @if ($errors->has('skills')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="skills">{{ __('Select Skills') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="skills[]" id="skill_ids" class="form-control" multiple="multiple" required>
                                        <option value=""></option>
                                        @if(!empty($old_skills))
                                            @foreach ($old_skills as $key=>$skill)
                                                <option value="{{$key}}" {{!empty(old('skills')) ? in_array($key,old('skills')) ? 'selected' : '' : ''}}>{{$skill}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('skills'))
                                            {{ $errors->first('skills') }}
                                        @else 
                                            {{ __('Skills cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div> -->
                            <div id="established-div" class="col-lg-6 @if ($errors->has('established_since')) validation-failed @endif {{old('organization_type') == 1 ?'d-none':''}}" >
                                <label for="established_since">{{ __('Established Since') }}
                                </label>
                                <input type="text" value="{{old('established_since')}}" name="established_since" class="form-control no-type" autocomplete="off" id="established_since" placeholder="{{ __('Established Since') }}">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('established_since'))
                                        {{ $errors->first('established_since') }}
                                    @endif
                                </div>
                            </div>
                            <div id="experience-div" class="col-lg-6 @if ($errors->has('years_of_experience')) validation-failed @endif {{old('organization_type',0)!=1?'d-none':''}}">
                                <label for="years_of_experience">{{ __('Years of Experience') }}
                                </label>
                                <input type="number" value="{{old('years_of_experience')}}" name="years_of_experience" class="form-control no-inc numericOnly" autocomplete="off" id="years_of_experience" placeholder="{{ __('Years of Experience') }}" min="0" max="100">
                                <div class="invalid-feedback" >
                                    @if ($errors->has('years_of_experience'))
                                        {{ $errors->first('years_of_experience') }}
                                    @else 
                                        {{ __('Please enter a value between 0 to 100.') }}
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('Company Tagline')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="company_tagline">{{ __('Company Tagline') }}
                                    </label>
                                    <input value="{{old('company_tagline')}}" type="text" name="company_tagline" class="form-control" id="company_tagline" minlength="3" maxlength="300" placeholder="{{__('Enter Company Tagline')}}">
                                    <div class="invalid-feedback"  @if ($errors->has('company_tagline')) style="display:block" @endif>
                                        @if ($errors->has('company_tagline'))
                                            {{ $errors->first('company_tagline') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 @if ($errors->has('company_history')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="company_history">{{ __('Company / Individual History') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <textarea name="company_history" id="company_history" rows="20" class="form-control" required>{{old('company_history')}}</textarea>
                                    <div class="invalid-feedback"  @if ($errors->has('company_history')) style="display:block" @endif>
                                        @if ($errors->has('company_history'))
                                            {{ $errors->first('company_history') }}
                                        @else 
                                            {{ __('Company History can not be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 @if ($errors->has('corporate_presentation')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="summernote-basic">{{ __('Corporate Presentation') }}</label>
                                    <textarea id="summernote-basic" class="form-control" rows="20"  name="corporate_presentation">{!!old('corporate_presentation')!!}</textarea>
                                    <div class="invalid-feedback"  @if ($errors->has('corporate_presentation')) style="display:block" @endif>
                                        @if ($errors->has('corporate_presentation'))
                                            {{ $errors->first('corporate_presentation') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 @if ($errors->has('video_links')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="video_links">{{ __('Video Links') }}
                                    </label>
                                    <div class="row">
                                        <table class="table">
                                        <tbody>
                                            @if(!empty(old('video_links')))
                                                @foreach (old('video_links') as $old_video)
                                                    <tr>
                                                        <td scope="row"><input  type="text" name="video_links[]" class="form-control" value="{{$old_video}}" id="video_links" placeholder="Video Link"></td>
                                                        <td><button class="btn btn-success add_more" type="button">+</button><button type="button" class="btn btn-danger ml-2 remove_row">-</button></td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td scope="row"><input  type="text" name="video_links[]" class="form-control" id="video_links" placeholder="Video Link"></td>
                                                    <td><button class="btn btn-success add_more" type="button">+</button><button type="button" class="btn btn-danger ml-2 remove_row">-</button></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        </table>
                                    </div>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('video_links'))
                                            {{ $errors->first('video_links') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 @if ($errors->has('profile_image')) validation-failed @endif">
                                <div class="form-group">
                                    <label class="light-label">{{ __(' Profile Image') }} </label>
                                    <div class="input-group">
                                        <input type="file" class="dropify" id="userImage"  accept = 'image/jpeg , image/jpg, image/gif, image/png'  data-default-file="" name="profile_image"/>
                                        
                                    </div>
                                    @if ($errors->has('profile_image'))
                                        <div class="invalid-feedback" @if ($errors->has('profile_image')) style="display:block" @endif>
                                            {{ $errors->first('profile_image') }} 
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Membership Plan</legend>
                        <div class="row">
                            <div class="col-lg-6 @if ($errors->has('plan_id')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="plan_id">{{ __('Select Plan') }}
                                    </label><br>
                                    <select name="plan_id" id="plan_id">
                                        <option value=""></option>
                                        @foreach ($plans as $plan)
                                            <option value="{{$plan->id}}" {{old('plan_id') == $plan->id?'selected':''}}>{{$plan->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('plan_id'))
                                            {{ $errors->first('plan_id') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div> 
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}
<script>
  var icon = {!! json_encode(url('/storage/admin/propertytypes/')) !!} +'/'+ $(this).attr('data-image');
  var drEvent = $('#userImage').dropify();
  drEvent = drEvent.data('dropify');
  drEvent.resetPreview();
  drEvent.clearElement();
  drEvent.destroy();
  drEvent.init();

  var drEvent1 = $('.dropify').dropify();

  drEvent1.on('dropify.afterClear', function(event, element){
      if($('.edit-popup').hasClass('dropify-selected') ){
          $('input[name=remove_title]').val('removed');
          alert($('input[name=remove_title]').val());
      }
  });
    $(document).ready(function() {
        $('#country_id').select2({
            placeholder: "{{__('Select a Country')}}",
        });
        $('#plan_id').select2({
            placeholder: "{{__('Select a Plan')}}",
        });
        $('#industry_ids').select2({
            placeholder: "{{__('Select Industries')}}",
        });
        $('#category_ids').select2({
            placeholder: "{{__('Select Categories')}}",
        });
        $('#sub_category_ids').select2({
            placeholder: "{{__('Select Sub Categories')}}",
        });
        $('#skill_ids').select2({
            placeholder: "{{__('Select Skills')}}",
        });
        $('input[name="established_since"]').daterangepicker({
                singleDatePicker: true,
                autoUpdateInput: false,
                timePicker: false,
                maxDate:new Date(),
                locale: {
                    format: 'DD-MM-YYYY'
                },
                autoApply: true
        });
        $('input[name="established_since"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
    });
    var phone = document.querySelector("#phone");
    var phone_iti = window.intlTelInput(phone,({

        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = "in";//(resp && resp.country) ? resp.country : "in";
                callback(countryCode);
            });
        },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.1.0/js/utils.js",
        separateDialCode: true,
        hiddenInput: "full_number",
        autoPlaceholder: "polite",
        initialCountry: "us",
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            return selectedCountryPlaceholder.replace(/[0-9]/g,"x");
        }, 
    }));
    phone.addEventListener("countrychange", function() {
        $('#country_code').val(phone_iti.getSelectedCountryData().dialCode);
    });

    
    
    $('#category_ids').change(function(){
        var sub_category_ids = $('#sub_category_ids').val();
        $.ajax({
            type: "GET",
            url: "{{url('/get-sub-categories')}}",
            data: {'category_ids':$(this).val()},
            success: function (response) {
                var new_options = "<option value =''></option>";
                $.each(response, function (index, value) { 
                    if(jQuery.inArray(index, sub_category_ids) !== -1){
                        new_options += "<option value='"+index+"' selected>"+value+"</option>";
                    }else{
                        new_options += "<option value='"+index+"'>"+value+"</option>";
                    }
                });
                $('#sub_category_ids').html(new_options);
                $('#sub_category_ids').change();
                $('#sub_category_ids').select2({
                    placeholder: "{{__('Select Sub Categories')}}",
                });
            }
        });
    });
    $('#sub_category_ids').change(function(){
        var skill_ids = $('#skill_ids').val();
        $.ajax({
            type: "GET",
            url: "{{url('/get-skills')}}",
            data: {'sub_category_ids':$(this).val()},
            success: function (response) {
                var new_options = "<option value =''></option>";
                $.each(response, function (index, value) { 
                    if(jQuery.inArray(index, skill_ids) !== -1){
                        new_options += "<option value='"+index+"' selected>"+value+"</option>";
                    }else{
                        new_options += "<option value='"+index+"'>"+value+"</option>";
                    }
                });
                $('#skill_ids').html(new_options);
                $('#skill_ids').select2({
                    placeholder: "{{__('Select Sub Categories')}}",
                });
            }
        });
    });
    $(document).on("click",'.add_more',function(){
        var $tr    = $(this).closest('tr');
        var $clone = $tr.clone();
        $clone.find('input').val('');
        $tr.after($clone);
    });
    $(document).on("click",'.remove_row',function(){
        if( $(this).closest('tr').is('tr:only-child') ) {
            //alert('cannot delete last row');
        }else{
            var $tr    = $(this).closest('tr').remove();
        }
    });
    $('input[name="organization_type"]').click(function(){
        if($('input[name="organization_type"]:checked').val() == 0){
            $('#established-div').removeClass('d-none');
            $('#experience-div').addClass('d-none');
            $('#established_since').val('');
            $('#years_of_experience').val('');
        }else{
            $('#established-div').addClass('d-none');
            $('#experience-div').removeClass('d-none');
            $('#established_since').val('');
            $('#years_of_experience').val('');
        }
    });
</script> 
@endpush
