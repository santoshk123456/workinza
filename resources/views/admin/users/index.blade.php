@extends('admin.layouts.app')
@section('title', set_page_titile(__('Approved Clients')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('users') }}
            <h4 class="page-title">{{ __('Approved - Clients') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="{{ route('admin.users.index') }}" aria-expanded="false" class="nav-link active">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('Approved - Clients') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.users.not_approved') }}" aria-expanded="false" class="nav-link">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('Not Approved - Clients') }}</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content p-3">
                    <div class="row mb-0">
                        <div class="col-sm-4">
                            @can('add clients')
                                <button onclick="location.href='{{ route('admin.users.create') }}'" type="button" class="btn btn-danger mb-3"><i class="mdi mdi-plus"></i> {{ __('Create Client') }}</button>
                            @endcan
                            {{--export code  --}}

                            <!-- Trigger the modal with a button -->
                            {{-- <button type="button" class="btn btn-success btn mb-3 " data-toggle="modal" data-target="#myModal"><span class="mdi mdi-file-export"></span>Export</button> --}}


                            {{-- end of code --}}
                        </div>
                        {{-- <div class="col-sm-8">
                            <div class="text-sm-right">
                            <button class="btn btn-primary btn-filter-toggle mb-3 ml-1 mr-2" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-filter mr-1" aria-hidden="true"></i>Filter</button>
                                <div class="btn-group mb-3 ml-1 status-btns">
                                    <button type="button" data-type="all" class="btn btn-primary">{{ __('All') }}</button>
                                    <button type="button" data-type="active" class="btn btn-light">{{ __('Active') }}</button>
                                    <button type="button" data-type="inactive" class="btn btn-light">{{ __('In Active') }}</button>
                                </div>
                                <input type="hidden" name="status_filter" id="status-filter" value="">
                            </div>
                        </div> --}}
                        <!-- filter collapse: start -->
                        {{-- <div class="col-12 mb-1">
                            <!-- filter collapse: start -->
                            <div class="collapse" style="padding: 10px;margin-top: 7px;box-shadow: 0 0 35px 0 rgba(50, 51, 53, 0.15);" id="collapseExample">
                            <div class="row">
                            <div class="col-md-3">
                            <div class="form-group m-0">
                                    <label for="simpleinput">Name</label>
                                    <input type="text" class="form-control" placeholder="Name" name="" id="name_search" >
                                </div>
                            </div>
                            <div class="col-md-3">
                            <div class="form-group m-0">
                                    <label for="simpleinput">Email</label>
                                    <input type="text" class="form-control" placeholder="Email" name="" id="store_id" >
                                </div>
                            </div>
                            
                            <div class="col">
                            <button type="button" class="btn btn-primary mt-3 filter-button">Apply</button>
                            </div>
                            </div>
                            </div>
                            <!-- filter collapse: end -->
                        </div> --}}
                        {{-- table code --}}
                        <div class="table-responsive export_table customized-export-table">
                            <table id="leads-table" class="table datatable-common">
                                <thead>
                                    <tr>
                                        <th>Sl #</th>
                                        <th>Client Type</th>
                                        <th>Email</th>
                                        <th>Membership Plan</th>
                                        <th>Total Proj Completed</th>
                                        <th>Total Proj In Progress</th>
                                        <th>Member Since</th>
                                        <th>Wallet Balance</th>
                                        <th>Status</th>
                                        <th>Step</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div> <!-- end card body-->
                </div>
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center" id="exampleModalLongTitle">Choose The Fields To Export</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <button type="button" class="close position-absolute" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="form-check slct-all ">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input " id="blk_dt_chkall" onchange="chk_all_box();" checked> Select All <i class="input-helper"></i></label>
                    </div>
                </div>
                <div class="modal-body py-0 mt-2">
                    <div class="row cust_draw_table">
                        <div class="col-lg-4">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="name" value="name" checked>Name <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="username" value="username" checked> User Name <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="email" value="email" checked> Email <i class="input-helper"></i></label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="created_at" value="created_at" checked> Created Date <i class="input-helper"></i></label>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer mx-auto mt-5">
                    <button type="button" class="btn btn-danger blkv-pdf"><span class="mdi mdi-file-pdf"></span>PDF</button>
                    <button type="button" class="btn btn-success blkv-excel"><span class="mdi mdi-file-excel"></span>Excel</button>
                    <button type="button" class="btn btn-primary blkv-csv"><span class="mdi mdi-file"></span>CSV</button>
                    <button type="button" class="btn btn-info blkv-copy"><span class="mdi mdi-checkbox-multiple-blank"></span>Copy</button>
                    <button type="button" class="btn btn-warning blkv-print"><span class="mdi mdi-cloud-print"></span>Print</button>
                </div>
            </div>    
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}

{{-- table -ajax --}}
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('#leads-table').DataTable({
            processing: true,
            serverSide: false,
            order: [],
            bFilter: true,
            bAutoWidth: true,
            ajax: {
                type: "POST",
                url: "{{route('admin.users.list')}}",
               
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex',
                    searchable: false,
                    sortable: false,
                    },
                    { data: 'organization_type',name: 'organization_type'},
                    { data: 'email',name: 'email',sortable: true,},
                    { data: 'plan',name: 'plan'},
                    { data: 'project_completed',name: 'project_completed'},
                    { data: 'project_inprogress',name: 'project_inprogress'},
                    { data: 'created_at',name: 'created_at',searchable: false},
                    { data: 'wallet_balance',name: 'wallet_balance'},
                    {
                    data: 'active',
                    name: 'active',                    
                    render: function (data, type, row) {
                        if(data==1){
                        return $('<div>').html(data).text();
                        }else{
                        return $('<div>').html(data).text();
                    }
                    },
                    searchable: false,
                    sortable: false
                    },
                    {
                    data: 'step',
                    name: 'step',
                    searchable: false,
                    sortable: false
                    },
                    {
                    data: 'action',
                    name: 'action',
                    searchable: false,
                    sortable: false
                    },

                    ],
            dom: 'Blfrtip',
           
            lengthMenu: [
                [20, 50, 100, -1],
                ['20', '50', '100', 'Show all']
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip({
                    trigger: 'hover'
                })
            }
        });

    });

</script>

<script>
    $(document).ready(function () {
        $('#leads-table_wrapper .dt-buttons').hide();
    });
    $(document).on('change','input[type=checkbox]',function(){
    if(!$(this).hasClass('bulk-slct')){
        Swal.fire({
            title: 'Are you sure?',
            text: "The current status of the client will be changed.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Update Status!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "/admin/user/toggle-active-status/"+$(this).attr('data-user')
                });
                Swal.fire(
                    'Status Updated!',
                    'The status of the client has been updated.',
                    'success'
                )
                setTimeout(function(){
                $(".status-btns .btn-primary").click();
                },1);
            } else {
                if($(this).prop("checked") ==  true){
                    $(this).prop("checked",false);
                } else {
                    $(this).prop("checked",true);
                }
            }
        })
    }
});
</script>
@endpush



