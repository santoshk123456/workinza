@extends('admin.layouts.app')
@section('title', set_page_titile(__('Industries')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('industries') }}
            <h4 class="page-title">{{ __('Industries') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                       @can('add industries')
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-danger mb-3" data-toggle="modal" data-target="#addIndustry"><i class="mdi mdi-plus"></i> {{ __('
                            Add Industry') }}</button>
                        </div>
                       @endcan
                </div>
                {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                <div class="col-12 text-right px-0">
                    <div class="form-group row">
                        <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                        <div class="col-sm-1 mt-1">
                            <input type="text" id="pageNum" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@can('add industries')
    <div class="modal fade" id="addIndustry" tabindex="-1" role="dialog" aria-labelledby="addIndustry" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="addIndustryLabel">{{ __('Add Industry') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form action="{{route('admin.industries.store')}}" method="POST" class="needs-validation" novalidate>
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="validationCustom02">{{ __('Industry') }}
                            <span class="mandatory">*</span>
                        </label>
                        <input type="text" name="industry_name" class="form-control" minlength="2" maxlength="50" required  placeholder="Industry" autocomplete="off">
                        <span class="invalid-feedback error-txt">
                        @if ($errors->has('industry_name'))
                        {{ $errors->first('industry_name') }}
                        @else
                        {{ __('Industry name cannot be empty.') }}
                        @endif
                        </span>
                    </div>
                    
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        </div>
    </div>
@endcan


@can('edit industries')
    <div class="modal fade" id="edit-industry" tabindex="-1" role="dialog" aria-labelledby="addIndustry" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="addIndustryLabel">{{ __('Edit Industry') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <form method="POST" class="needs-validation edit-form" novalidate>
                @csrf
                @method('put')
                <div class="modal-body">
                    <div class="form-group">
                        <label for="validationCustom02">{{ __('Industry') }}
                            <span class="mandatory">*</span>
                        </label>
                        <input type="text" name="industry_name" id="industry" class="form-control" minlength="2" maxlength="50" required placeholder="Industry" autocomplete="off">
                        <span class="invalid-feedback error-txt">
                        @if ($errors->has('industry_name'))
                        {{ $errors->first('industry_name') }}
                        @else
                        {{ __('Industry name cannot be empty.') }}
                        @endif
                        </span>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
        </div>
    </div>
@endcan


@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}

<script>
    function edit(industry){
        $('#industry').val($(industry).data('industry'));
        $('.edit-form').attr('action',$(industry).data('link'));
        $('#edit-industry').modal('show');
    }
</script>

@endpush
