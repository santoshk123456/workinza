@extends('admin.layouts.app')
@section('title', set_page_titile(__('Push Notification Template Detail Page')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('push_notification_templates.show',$pushNotificationTemplate) }}
            </div>
            <h4 class="page-title">{{ __('View '.$pushNotificationTemplate->process_name) }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Process Name') }}</label>
                    <p>{{ $pushNotificationTemplate->process_name }}</p>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Description') }}</label>
                    <p>{{ $pushNotificationTemplate->description }}</p>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Created At') }}</label>
                    <p>{{ $pushNotificationTemplate->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($pushNotificationTemplate->created_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                    
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Modified At') }}</label>
                    <p>{{ $pushNotificationTemplate->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($pushNotificationTemplate->updated_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Template') }}</label>                    
                    <p>{{$pushNotificationTemplate->template}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
