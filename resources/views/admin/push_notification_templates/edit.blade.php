@extends('admin.layouts.app')
@section('title', set_page_titile(__('Push Notification Template Create')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('push_notification_templates.edit',$pushNotificationTemplate) }}
            </div>
            <h4 class="page-title">{{ __('Edit Push Notification Template') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.push_notification_templates.update',$pushNotificationTemplate)}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-12 mb-4 @if ($errors->has('process_name')) validation-failed @endif">
                            <label for="validationCustom01">{{ __('Process Name') }}
                                <span class="mandatory">*</span>
                            </label>
                            <input type="text" value="{{ $pushNotificationTemplate->process_name }}" name="process_name" class="form-control" id="validationCustom01" placeholder="Process Name" required>
                            <div class="invalid-feedback" >
                                @if ($errors->has('process_name'))
                                    {{ $errors->first('process_name') }}
                                @else 
                                    {{ __('Process Name cannot be empty.') }}
                                @endif
                            </div>
                        </div>
                        <div class="col-12 mb-4 @if ($errors->has('description')) validation-failed @endif">
                            <label for="validationCustom02">{{ __('Description') }}
                                <span class="mandatory">*</span>
                            </label>
                            <textarea name="description" class="form-control" id="validationCustom02" rows="3" required>{{ $pushNotificationTemplate->description }}</textarea>
                            <div class="invalid-feedback" >
                                @if ($errors->has('description'))
                                    {{ $errors->first('description') }}
                                @else 
                                    {{ __('Description cannot be empty.') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <label for="summernote-basic">{{ __('Template') }}
                        <span class="mandatory">*</span>
                    </label>
                    <textarea id="summernote-basic" class="form-control"  name="template" required>{{$pushNotificationTemplate->template}}</textarea>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>            
@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}
@endpush
