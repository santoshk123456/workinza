@extends('admin.layouts.app')
@section('title', set_page_titile(__('Create Invoice')))
@push('custom-styles')
<style>
.select2-container--default .select2-selection--single {
    border: 1px solid #dee2e6 !important;
    height: 36px !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444 !important;
    line-height: 35px !important;
    padding-left: 16px !important;
}
</style>
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('create_invoice') }}
            </div>
            <h4 class="page-title">{{ __('Create Invoice') }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{route('admin.user_subscriptions.StoreInvoice')}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <div class="row">

                        <div class="col-lg-6 @if ($errors->has('invoice_user')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="invoice_user">{{ __('Select User') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="invoice_user" id="invoice_user" class="form-control js-example-placeholder-single" required>
                                        <option value=""></option>
                                        @foreach ($all_users as $user)
                                            <option value="{{$user->id}}">{{$user->full_name}}({{$user->email}})</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback error-txt" >
                                        @if ($errors->has('invoice_user'))
                                            {{ $errors->first('invoice_user') }}
                                        @else 
                                            {{ __('User cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6 @if ($errors->has('invoice_type')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Type') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <select name="invoice_type" id="invoice_type" class="form-control" required>
                                        <option value="" selected disabled> Select Type </option>
                                        <option value="0"> Arbitration </option>
                                        <option value="1"> Project payment </option>
                                        <option value="2"> Cancellation </option>
                                        <option value="3"> Refund </option>
                                        <option value="4"> Bonus </option>
                                        <option value="5"> Subscription payment </option>

                                </select>
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('invoice_type'))
                                    {{ $errors->first('invoice_type') }}
                                    @else
                                    {{ __('Type cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6 @if ($errors->has('bill_amount')) validation-failed @endif ">
                            <div class="form-group mb-3">
                                <label class="form-label-multi-common" for="bill_amount">{{ __('Bill Amount') }}
                                ({{config('data.currency_symbol')}})
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('bill_amount') }}"
                                    name="bill_amount" class="form-control "
                                    id="bill_amount" placeholder="Monthly Plan Amount" step="0.01" min="1" max="999999.99"
                                    onclick="ValidateDecimalInputs(this)" required>
                                <div class="invalid-feedback">
                                    @if ($errors->has('bill_amount'))
                                    {{ $errors->first('bill_amount') }}
                                    @else
                                    {{ __('Enter a value between 1 and 999999.99')}}
                                    @endif
                                </div>
                            </div>
                        </div>
         

                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i
                                class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i
                                class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

<script>
$(".js-example-placeholder-single").select2({
    placeholder: "Select a user",
    allowClear: true
});

function ValidateDecimalInputs(e) {

var beforeDecimal =6;
var afterDecimal = 2;

$('#'+e.id).on('input', function () {
    this.value = this.value
      .replace(/[^\d.]/g, '')            
      .replace(new RegExp("(^[\\d]{" + beforeDecimal + "})[\\d]", "g"), '$1') 
      .replace(/(\..*)\./g, '$1')         
      .replace(new RegExp("(\\.[\\d]{" + afterDecimal + "}).", "g"), '$1');   
});
}

</script>


@endpush
