@extends('admin.layouts.app')
@section('title', set_page_titile(__('Subscription Payments')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
{{ style('css/admin/developer.css') }}
<style>
.select2-container--default .select2-selection--single {
    border: 1px solid #dee2e6 !important;
    height: 36px !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444 !important;
    line-height: 35px !important;
    padding-left: 16px !important;
}
.form-control.is-invalid, .was-validated .form-control:invalid{
    border-color : #e4dcde !important;
}
.user-details:hover {
    cursor: pointer !important;
    color: #2E1A47 !important;
    text-decoration: underline !important;
}
</style>
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
           {{Breadcrumbs::render('subscription_payments')}}
            <h4 class="page-title">{{ __('Subscription Payments') }}</h4>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row mb-0">
                        <div class="col-sm-4">
                            <button onclick="location.href='{{ route('admin.user_subscriptions.invoice') }}'" type="button" class="btn btn-danger mb-3"><i class="mdi mdi-plus"></i> {{ __('Create Invoice') }}</button>
                        </div>         
                        <div class="col-sm-8 mb-2 m-0 filter-control">
                            <button type="button" data-target="#filter" data-toggle="collapse" class="btn btn-sm btn-primary float-right filter"><i class="mdi mdi-filter"></i> Filter</button>
                        </div>      


                        <div id="filter" class="collapse mb-1">
                        <form method="GET" action="{{ route('admin.user_subscriptions.export_subscription') }}"
                            class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                             @csrf
                                    <div class="row">
                               
                                    <div class="col-lg-4">
                                        <label for="filter-status">Membership Plans</label>
                                        <span class="mandatory">*</span>
                                        <select name="mem_plan" id="mem_plan_id" class="form-control" required>
                                        <option value="" selected="">All</option>
                                            @foreach($membership_plans as $plan)
                                            <option value="{{$plan->id}}">
                                                {{$plan->name}}
                                            </option>
                                            @endforeach 
                                        </select>
                                        <span class="invalid-feedback error-txt">
                                            @if ($errors->has('mem_plan'))
                                            {{ $errors->first('mem_plan') }}
                                            @else
                                            {{ __('Membership plans cannot be empty.') }}
                                            @endif
                                        </span>
                                    </div> 

                                    <div class="col-lg-4">
                                        <label for="filter-status">User Type</label> 
                                        <span class="mandatory">*</span>
                                        <select name="user_type" id="user_type_id" class="form-control" required>
                                        <option value="" selected="">All</option>
                                        <option value="0">Client</option>
                                        <option value="1">Freelancer</option>
                                        </select>
                                        <span class="invalid-feedback error-txt">
                                        @if ($errors->has('user_type'))
                                        {{ $errors->first('user_type') }}
                                        @else
                                        {{ __('User type cannot be empty.') }}
                                        @endif
                                        </span>
                                    </div> 


                                        <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('Date') }}
                                            <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" name="daterange" id="daterange_id" class="input-text with-border form-control readonly" style="caret-color: transparent;" placeholder="Select date range" required/>
                                            <span class="invalid-feedback error-txt">
                                            @if ($errors->has('daterange'))
                                            {{ $errors->first('daterange') }}
                                            @else
                                            {{ __('Date cannot be empty.') }}
                                            @endif
                                            </span>
                                        </div>
                
                                        
                                        <div class="col-sm-6 mt-3">
                                            <button type="button" class="btn btn-danger mb-3 mr-1 filter_button" name="exp_excel" value="search"> {{ __('Search') }}</button>
                                            {{--export code  --}}
                                            <button type="submit"class="btn btn-success mb-3 mr-1" name="exp_excel" value="excel">Export to XLS</button>
                                            <button type="submit"class="btn btn-info mb-3" name="exp_excel" value="csv">Export to CSV</button>
                                            {{-- end of code --}}
                                        </div>
                                    </div>
                                </form>
                    </div>   
                   
                    
                </div>
                
                <div id="basic-datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            {!! $dataTable->table(['class' => 'table  nowrap table-striped','id'=>'basic-datatable','width'=>'100%'], true) !!}
                            <div class="col-12 text-right px-0">
                                <div class="form-group row">
                                    <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                                    <div class="col-sm-1 mt-1">
                                        <input type="text" id="pageNum" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}




<script>

/*  Reset the filter  - delete the cookie */
function reset_form(){
        
        $('#basic-datatable').DataTable().draw();
    }
   $(document).on('click','.filter_button',function(){
       $('#basic-datatable').DataTable().draw();
   })


    $('input[name="daterange"]').daterangepicker({
        autoUpdateInput: false,
        opens: 'right',
        locale: {
            format: 'DD/MM/YYYY'
        }
    }, function (start, end, label) {
        console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
            .format('YYYY-MM-DD'));
    });
    $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

</script>

<script>
 $(document).ready(function() {
     
        $('#mem_plan_id').select2({
            placeholder: "{{__('Select Plans')}}",
        });

    });
</script>
<script>
    $(".readonly").keydown(function(e){
        e.preventDefault();
    });
</script>
@endpush