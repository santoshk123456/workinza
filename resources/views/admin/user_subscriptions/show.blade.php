@extends('admin.layouts.app')
@section('title', set_page_titile('IN'.($invoice->id).' - Subscription Payments'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('subscription_payments.show',$invoice)}}
            </div>
            <h4 class="page-title">{{'IN'.($invoice->id)}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               <div class="row mb-3">
                <div class="col-12 text-right">
                        <div class="master-actions-inner">
                       
                         <button  onclick="location.href='{{ route('admin.user_subscriptions.index') }}'" type="button" class="btn btn-primary btn-sm-multi-common btn-white mb-3"><i class="mdi mdi-format-list-bulleted"></i>List All</button>
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                          @if(empty($invoice->user->user_type))
                            <label>{{ __('Client') }}</label>
                          @else
                          <label>{{ __('Freelancer') }}</label>
                          @endif
                            <p>{{ucwords($invoice->user->full_name)}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Subscription Plan') }}</label>
                            @if(!empty($invoice->userSubscription->membership->name))
                            <p>{{ ucwords($invoice->userSubscription->membership->name) }}</p>
                            @else
                            <p>{{__("NA")}}
                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Amount') }}  ({{config('data.currency_symbol')}})</label>
                            <p>{{ formatAmount($invoice->amount) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Status') }}</label>
                            @if(empty($invoice->status))
                            <p>{{__('Pending')}}</p>
                            @elseif($invoice->status == 1)
                            <p>{{__('Paid')}}</p>
                            @else
                            <p>{{__('Rejected')}}</p>
                            @endif
                        </div>
                    </div>
                   
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Invoice Date') }}</label>
                            <p>{{ hcms_date(strtotime($invoice->created_at), 'date-time', false) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Subscription Type') }}</label>
                            @if(!empty($invoice->userSubscription->duration_type))
                            <p>{{__('Annual')}}</p>
                            @else
                            <p>{{__('Monthly')}}</p>
                            @endif
                        </div>
                    </div>

                    
                 

                   
                </div>

            </div>


        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
@endsection
@push('custom-scripts')




@endpush
