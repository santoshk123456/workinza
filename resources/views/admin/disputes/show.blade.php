@extends('admin.layouts.app')
@section('title', set_page_titile($dispute->name.' - Categories'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('disputes.show',$dispute) }}
            </div>
            <h4 class="page-title">View {{getContractId($dispute->project_contract_id)}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               <div class="row mb-3">
                <div class="col-12 text-right">
                        <div class="master-actions-inner">
                         <button onclick="location.href='{{ route('admin.disputes.message',[$dispute->id,$dispute->milestone->project->id]) }}'"  type="button" class="btn btn-primary btn-sm-multi-common mb-3 mr-2"><i class="mdi mdi-note-outline mr-1"></i>Message</button>
                         <!-- <button  onclick="location.href='{{ route('admin.categories.index') }}'" type="button" class="btn btn-primary btn-sm-multi-common btn-white mb-3"><i class="mdi mdi-format-list-bulleted"></i>List All</button> -->
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('ContractId') }}</label>
                            <p>{{getContractId($dispute->project_contract_id)}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Client Name') }}</label>
                            <p>{{ucwords($dispute->client->username)}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Freelancer Name') }}</label>
                            <p>{{ucwords($dispute->freelancer->username)}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Raised By') }}</label>
                            <p>{{ucwords($dispute->raisedBy->username)}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Project Name') }}</label>
                            <p>{{ucwords($dispute->milestone->project->title)}}</p>
                        </div>
                    </div>  
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Milestone Name') }}</label>
                            <p>{{ucwords($dispute->milestone->milestone_name)}}</p>
                        </div>
                    </div>  
                    <div class="col-md-12 mb-3">
                        <div class="data-view">
                            <label>{{ __('Discription') }}</label>
                            <p>{{ucwords($dispute->description)}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Subject') }}</label>
                            <p>{{ucwords($dispute->subject)}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Amount') }}</label>
                            <p>{{ucwords($dispute->amount)}}</p>
                        </div>
                    </div>
                                
                  
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Created At') }}</label>
                            <p>{{ hcms_date(strtotime($dispute->created_at), 'date-time', false) }}</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3">
                        <div class="data-view">
                            <label>{{ __('Modified At') }}</label>
                            <p>{{ hcms_date(strtotime($dispute->updated_at), 'date-time', false) }}</p>
                        </div>
                    </div>

                    <div class="col-md-4 mb-3">
                     <label>Status</label>
                        <div id="approval_status">
                        @if($dispute->status==1)
                        <h4><span class="badge badge-success">Resolved</i></span>  </h4> 
                        @else
                        <h4><span class="badge badge-danger">Pending</span> </h4>
                        @endif
                        </div>
                    </div>
                   
                </div>

            </div>


        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
@endsection
@push('custom-scripts')

@endpush
