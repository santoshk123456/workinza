@extends('admin.layouts.app')
@section('title', set_page_titile(__('Disputes')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('disputes') }}
            <h4 class="page-title">{{ __('Disputes') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            <div class="row">
                    <div class="col-12">
                        <form method="GET" action="{{ route('admin.disputes.export_dispute') }}"
                            class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                             @csrf
                                    <div class="row">
                               
                                        <!-- <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('From Date') }}
                                            <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" name="from_date" id="from_date" class="form-control readonly" style="caret-color: transparent;" autocomplete="off" id="validationCustom01" placeholder="From Date" required>
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('To Date') }}
                                            <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" name="to_date" id="to_date" class="form-control readonly" style="caret-color: transparent;" autocomplete="off" id="validationCustom01" placeholder="To Date" required>
                                            
                                        </div> -->


                                        <div class="col-lg-4">
                                            <label for="validationCustom01">{{ __('Date') }}
                                            <span class="mandatory">*</span>
                                            </label>
                                            <input type="text" name="daterange" id="daterange_id" class="input-text with-border form-control readonly" style="caret-color: transparent;" placeholder="Select date range" required/>
                                        </div>
                
                                        
                                        <div class="col-sm-6 mt-3">
                                            <button type="button" class="btn btn-danger mb-3 mr-1 filter_button" name="exp_excel" value="search"> {{ __('Search') }}</button>
                                            <!-- <button type="button" class="btn btn-primary mt-3 filter_button filter-button"  title="Apply"><i class="mdi mdi-check"></i> Apply</button> -->
                                            {{--export code  --}}
                                            <button type="submit"class="btn btn-success mb-3 mr-1" name="exp_excel" value="excel">Export to XLS</button>
                                            <button type="submit"class="btn btn-info mb-3" name="exp_excel" value="csv">Export to CSV</button>
                                            {{-- end of code --}}
                                        </div>
                                    </div>
                                </form>
                        </div>
                </div>
                {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                <div class="col-12 text-right px-0">
                    <div class="form-group row">
                        <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                        <div class="col-sm-1 mt-1">
                            <input type="text" id="pageNum" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}

<script>
    // $('input[name="from_date"]').daterangepicker({
    //             singleDatePicker: false,
    //             autoUpdateInput: false,
    //             timePicker: false,
    //             // minDate:new Date(),
    //             locale: {
    //             format: 'DD-M-YYYY'
    //             }
    // });
    // $('input[name="to_date"]').daterangepicker({
    //             singleDatePicker: false,
    //             autoUpdateInput: false,
    //             timePicker: false,
    //             // minDate:new Date(),
    //             locale: {
    //             format: 'DD-M-YYYY'
    //             }
    // });
   
    
    // $('input[name="from_date"]').on('apply.daterangepicker', function(ev, picker) {
    //     $(this).val(picker.startDate.format('DD-M-YYYY'));
    // });
    // $('input[name="to_date"]').on('apply.daterangepicker', function(ev, picker) {
    //     $(this).val(picker.startDate.format('DD-M-YYYY'));
    // });


    $('input[name="daterange"]').daterangepicker({
            autoUpdateInput: false,
            opens: 'right',
            locale: {
                format: 'DD/MM/YYYY'
            }
        }, function (start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                .format('YYYY-MM-DD'));
        });
        $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });
</script>


<script>

   $(document).on('click','.filter_button',function(){
       $('#basic-datatable').DataTable().draw();
   })
</script>

<script>
    $(".readonly").keydown(function(e){
        e.preventDefault();
    });
</script>


@endpush
