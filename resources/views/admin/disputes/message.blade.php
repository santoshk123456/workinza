@extends('admin.layouts.app')
@section('title', set_page_titile('Dispute Message'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
<link rel="apple-touch-icon" sizes="57x57" href="{{asset('/images/user/favicons/apple-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{asset('/images/user/favicons/apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('/images/user/favicons/apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('/images/user/favicons/apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('/images/user/favicons/apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{asset('/images/user/favicons/apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{asset('/images/user/favicons/apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{asset('/images/user/favicons/apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('/images/user/favicons/apple-icon-180x180.png')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{asset('/images/user/favicons/android-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('/images/user/favicons/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('/images/user/favicons/favicon-96x96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('/images/user/favicons/favicon-16x16.png')}}">
<link rel="manifest" href="{{asset('/images/user/favicons/manifest.json')}}">

<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{asset('/images/user/favicons/ms-icon-144x144.png')}}">
<meta name="theme-color" content="#ffffff">

<!-- Pre-load sections -->
<link rel="preload" href="{{asset('/css/user/style.min.css')}}" as="style">
<link rel="preload" href="{{asset('/js/user/bundle.min.js')}}" as="script">
<link rel="preload" href="{{asset('/fonts/user/AmazonEmber-Bold.woff')}}" as="font" type="font/woff" crossorigin>
<link rel="preload" href="{{asset('/fonts/user/AmazonEmber-Regular.woff')}}" as="font" type="font/woff" crossorigin>  
<!-- Style sheets -->
 <link rel="stylesheet" href="{{asset('/css/user/style.min.css')}}">   
 <link rel="stylesheet" href="{{asset('/css/user/jquery.toast.min.css')}}">   
 <link rel="stylesheet" href="{{asset('/css/user/developer.css')}}">   
 <link rel="stylesheet" href="{{asset('/css/user/dropify.min.css')}}">   
 {{ style(mix('css/admin/admin.css')) }}
 <!-----toastr---------------->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" /> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.css" integrity="sha512-bbUR1MeyQAnEuvdmss7V2LclMzO+R9BzRntEE57WIKInFVQjvX7l7QZSxjNDt8bg41Ww05oHSh0ycKFijqD7dA==" crossorigin="anonymous" />
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('disputes.show',$dispute) }}
            </div>
            <h4 class="page-title">Message For {{getContractId($dispute->project_contract_id)}}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               <div class="row mb-3">
                <div class="col-12 text-right">
                        <div class="master-actions-inner">
                         <!-- <button onclick="location.href='{{ route('admin.categories.edit',$dispute->uuid) }}'"  type="button" class="btn btn-primary btn-sm-multi-common mb-3 mr-2"><i class="mdi mdi-note-outline mr-1"></i>Message</button> -->
                         <!-- <button  onclick="location.href='{{ route('admin.categories.index') }}'" type="button" class="btn btn-primary btn-sm-multi-common btn-white mb-3"><i class="mdi mdi-format-list-bulleted"></i>List All</button> -->
                        </div>
                </div>
                </div>
                <div class="row w-100">
                    
                    <!-- messages-holder:start -->
                    @if($to_count!=0 || !empty($dispute->freelancer->username) || !empty($chats))
                    <div class="messages-holder bg-shadow" style="width: inherit;">

                        <div id="frame">
                            <div id="sidepanel">
                                <div id="contacts">
                                    <ul class="non-archive-list">
                                        <div class="archive-action text-right">
                                            <h2>{{__('Chats')}}</h2>
                                        </div>

                                        @if($to_count==0 && !empty($user->username))
                                        <li class="contact active">
                                            <div class="wrap">
                                            <img src="{{ asset('images/admin/starter/group.jpeg') }}" alt="{{ $user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                <div class="meta">
                                                    <p class="name">{{ucwords($dispute->freelancer->username)}},{{ucwords($dispute->client->username)}},{{ucwords($user->username)}}</p>
                                                    <p class="preview">{{__('No Messages Found')}}</p>

                                                </div>
                                            </div>
                                        </li>
                                        @else
                                        <li class="contact active">
                                            <div class="wrap">
                                            <img src="{{ asset('images/admin/starter/group.jpeg') }}" alt="{{ $user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                <div class="meta">
                                                    <p class="name">{{ucwords($dispute->freelancer->username)}}, {{ucwords($dispute->client->username)}}, {{ucwords($user->username)}}</p>
                                                    <p class="preview">{{$last_msg}}</p>

                                                </div>
                                            </div>
                                        </li>
                                        @endif

                                        @foreach($non_archive_highlights as $key=>$chat_highlight)
                                        @if(auth('web')->user()->id!=$chat_highlight->from_user_id)
                                        @php $to_user=$chat_highlight->sender;
                                        @endphp
                                        @endif
                                        @if(auth('web')->user()->id!=$chat_highlight->to_user_id)
                                        @php $to_user= $chat_highlight->recipient;
                                        @endphp
                                        @endif
                                        <a href="{{route('message.disputeMessages',array($project->uuid,$to_user->uuid))}}">
                                        <li @if($to_user->id == $freelancer->id) class="contact active" @else class="contact" @endif>
                                            
                                                <div class="wrap">

                                                    @if($to_user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$to_user->profile_image)))
                                                    <img src=" {{ asset('storage/user/profile-images/'.$to_user->profile_image) }}" alt="" />
                                                    @else
                                                    <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $to_user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                    @endif
                                                    <div class="meta">
                                                        <p class="name">{{ucwords($to_user->username)}}<span>{{timeAgoConversion($chat_highlight->created_at)}}</span></p>
                                                        <p class="preview">{{$chat_highlight->message}}</p>
                                                        @if(auth()->user()->user_type==0)
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="non-archive-list[]" class="custom-control-input non-archive-list" id="non-archive{{$to_user->id}}" value="{{$to_user->id}}">

                                                            <label class="custom-control-label" for="non-archive{{$to_user->id}}"></label>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                           
                                        </li>
                                        </a>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="content">
                                <div class="contact-profile">
                                    <p> {{ucwords($dispute->freelancer->username)}},{{ucwords($dispute->client->username)}}, {{ucwords($user->username)}}</p>
                                    <div class="social-media">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="messages">
                                    <ul>
                                        @foreach($chats as $key=>$chat)

                                        <li @if($chat->from_user_id==$user->id && $chat->user_type == 'admin') class="replies" @else class="sent" @endif >
                                            @if($chat->user_type == 'user')
                                                @if($chat->sender->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$chat->sender->profile_image)))
                                                <img src=" {{ asset('storage/user/profile-images/'.$chat->sender->profile_image) }}" alt="" />
                                                @else
                                                <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $chat->sender->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                @endif
                                                <span class="message-label">{{ucwords($chat->sender->username)}}, {{timeAgoConversion($chat->created_at)}}</span>
                                            @else
                                                @if($chat->admin_sender->profile_image && (file_exists( public_path().'/storage/admin/profile-images/'.$chat->admin_sender->profile_image)))
                                                <img src=" {{ asset('storage/admin/profile-images/'.$chat->admin_sender->profile_image) }}" alt="" />
                                                @else
                                                <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $chat->admin_sender->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                @endif
                                                <span class="message-label">{{ucwords($chat->admin_sender->username)}}, {{timeAgoConversion($chat->created_at)}}</span>
                                            @endif
                                            <p>{{$chat->message}}</p>
                                            @if(!empty($chat->attachment))
                                            <p> <a href="{{asset('storage/projects/comment_attachements/'.$chat->attachment)}}" class="btn white-nm-btn download" download="{{$chat->attachment}}" title="Download Attachment"><i class="fi-flaticon flaticon-download"></i></a></p>
                                            @endif
                                        </li>

                                        <!-- <li class="sep"><p>Today</p></li> -->
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="message-input">
                                    <form method="POST" action="{{route('admin.disputes.store_message')}}" enctype="multipart/form-data" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="message-form" novalidate>
                                        @csrf
                                        <div class="wrap">
                                            <input required type="text" id="message" name="message" placeholder="{{__('Write your message...')}}" />

                                            <!-- <i class="fi-flaticon flaticon-attachments mr-2"></i> -->
                                            <div class="attachments">
                                                <input type="file" id="attachement" name="attachement"><i class="fi-flaticon flaticon-attachments mr-2"></i>
                                            </div>
                                            <input type="hidden" id="project_id" name="project_id" value="{{$project->id}}">
                                            <input type="hidden" id="dispute_id" name="dispute_id" value="{{$dispute->id}}">
                                            <input type="hidden" id="user_uuid" name="user_uuid" value="{{$user->uuid}}">
                                            <button class="submit" disabled id="message-submit"><i class="fi-flaticon flaticon-send mr-2"></i></button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>


                    </div>
                    @else
                    <div class="proposals-list-holder bg-shadow">
                        <div class="col-list-item">                    
                            <div class="media text-center text-md-left d-block d-md-flex">
                                No messages found on this project
                            </div>
                        </div>
                    </div>
                   @endif
                    <!-- messages-holder:end -->
                </div>

            </div>


        </div>
    </div> <!-- end card body-->
</div> <!-- end card -->
</div><!-- end col-->
@endsection
@push('custom-scripts')
 <!--- jquery file -->
 <script src="{{asset('js/user/bundle.min.js')}}"></script> 
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="{{asset('js/user/jquery.toast.min.js')}}"></script>
<script src="{{asset('js/user/developer.js?v=01')}}"></script>
<script src="{{asset('js/user/dropify.min.js?v=10')}}"></script>
<script src="{{asset('js/intlTelInput.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js" integrity="sha512-9WciDs0XP20sojTJ9E7mChDXy6pcO0qHpwbEJID1YVavz2H6QBz5eLoDD8lseZOb2yGT8xDNIV7HIe1ZbuiDWg==" crossorigin="anonymous"></script>
<!-----toastr---------------->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    $("#message-form").submit(function() {
        var message = document.getElementById('message').value;
        if (message == "") {
            return false;
        }
    });

    $("#message").keyup(function() {
        var message = document.getElementById('message').value;
        if (message != "") {
            document.getElementById("message-submit").disabled = false;
           
        } else {
            document.getElementById("message-submit").disabled = true;
        }
    });
</script>
@endpush
