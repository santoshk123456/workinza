@extends('admin.layouts.app')
@section('title', set_page_titile(__('Edit Dispute')))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('disputes.edit',$dispute) }}
            </div>
            <h4 class="page-title">{{ __(getContractId($dispute->project_contract_id).' Edit') }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{route('admin.disputes.update',$dispute->id)}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="row">

                        <div class="col-lg-6 @if ($errors->has('category_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Contract ID') }}
                                    <span class="mandatory"></span>
                                </label>
                                <input value="{{getContractId($dispute->project_contract_id)}}" type="text" readonly name="location" class="form-control" id="location" minlength="3" maxlength="150" placeholder="{{__('Enter Location')}}" required>
                            </div>
                        </div>
                        <div class="col-lg-6 @if ($errors->has('category_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Project') }}
                                    <span class="mandatory"></span>
                                </label>
                                <input value="{{$dispute->milestone->project->title}}" type="text" readonly name="location" class="form-control" id="location" minlength="3" maxlength="150" placeholder="{{__('Enter Location')}}" required>
                            </div>
                        </div>
                        <div class="col-lg-6 @if ($errors->has('category_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Milestone') }}
                                    <span class="mandatory"></span>
                                </label>
                                <input value="{{$dispute->milestone->milestone_name}}" type="text" readonly name="location" class="form-control" id="location" minlength="3" maxlength="150" placeholder="{{__('Enter Location')}}" required>
                            </div>
                        </div>
                        <div class="col-lg-6 @if ($errors->has('category_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Project Status') }}
                                    <span class="mandatory"></span>
                                </label>
                                <select class="form-control" name="status" id="status" required>
                                    <option disabled selected value=""> Select Status</option>
                                    <option value="2" >{{__('Assigned') }}</option>
                                    <option value="3" >{{__('Inprogress') }}</option>
                                    <option value="4">{{__('Completed') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="project_id" value="{{$dispute->milestone->project->id}}">
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i
                                class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i
                                class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

@endpush
