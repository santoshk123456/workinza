@extends('admin.layouts.app')
@section('title', set_page_titile(__('SMS Queue Detail Page')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('sms_queues.show', $smsQueue) }}
            </div>
            <h4 class="page-title">{{ __('SMS Queue Detail Page') }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Phone') }}</label>
                    <p>{{ $smsQueue->mobile_number }}</p>
                </div>
            </div>
            <div class="col-lg-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Status') }}</label>
                    @if($smsQueue->sent_status == 1)
                        <p><span class="badge badge-success px-2">{{ __('Sent') }}</span></p>
                    @else
                        <p><span class="badge badge-danger px-2">{{ __('Failed') }}</span></p>
                    @endif
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Message') }}</label>
                    <p>{{ $smsQueue->message }}</p>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Created At') }}</label>
                    <p>{{ $smsQueue->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($smsQueue->created_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                    
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Modified At') }}</label>
                    <p>{{ $smsQueue->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($smsQueue->updated_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
