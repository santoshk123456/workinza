<div class="page-title-right">
<a href="{{ URL::previous() }}" class="btn bk-btn"><i class="mdi mdi-menu-left"></i>{{ __('Go Back') }}</a>
    @if (count($breadcrumbs))
        <ol class="breadcrumb">
            @foreach ($breadcrumbs as $breadcrumb)
                @if ($breadcrumb->url && !$loop->last)
                    <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                @else
                    <li class="breadcrumb-item active">{{ $breadcrumb->title }}</li>
                @endif
            @endforeach
        </ol>
    @endif
</div>

