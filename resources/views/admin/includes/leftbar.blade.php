<div class="left-side-menu">
        <a href="{{ route('admin.dashboard') }}" class="logo text-center">
            <span class="logo-lg">
                <img src="{{ asset('images/user/1024_fordark_bg.png') }}" alt="" height="52">
            </span>
            <span class="logo-sm">
                <img src="{{ asset('images/user/1024_fordark_icon.png') }}" alt="" height="42">
            </span>
        </a>
        <ul class="metismenu side-nav slimscroll-menu mb-5">
            <li class="side-nav-item @if(Request::is('admin/dashboard')) active @endif">
                <a href="{{ route('admin.dashboard') }}" class="side-nav-link @if(Request::is('admin/dashboard')) active @endif">
                    <i class="dripicons-meter"></i>
                    <span> {{ __('Dashboard') }} </span>
                </a>
            </li>
            <li class="side-nav-item @if(Request::is('admin/admin-user*') || Request::is('admin/role*')) active @endif">
                <a href="javascript: void(0);" class="side-nav-link @if(Request::is('admin/admin-user*') || Request::is('admin/role*')) active @endif">
                    <i class="mdi mdi-account"></i>
                    <span> {{ __('Admins & Roles') }} </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level collapse" aria-expanded="false">
                
                    <li class="@if(Request::is('admin/admin-user*')) active @endif">
                        <a href="{{ route('admin.admins.index') }}" class="@if(Request::is('admin/admin-user*')) active @endif">
                        <i class="mdi mdi-account-multiple"></i>
                        <span>{{ __('Admins') }}</span>
                        </a>
                    </li>

                    <li class="@if(Request::is('admin/role*')) active @endif">
                        <a href="{{ route('admin.roles.index') }}" class="@if(Request::is('admin/role*')) active @endif">
                        <i class="mdi mdi-account-convert"></i>
                        <span>{{ __('Roles') }}</span>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="side-nav-item @if(Request::is('admin/client*')) active @endif">
                <a href="{{ route('admin.users.index') }}" class="side-nav-link @if(Request::is('admin/client*')) active @endif">
                    <i class="mdi mdi-account-star"></i>
                    <span> {{ __('Clients') }} </span>
                </a>
            </li>
            <li class="side-nav-item @if(Request::is('admin/freelancer*')) active @endif">
                <a href="{{ route('admin.freelancers.index') }}" class="side-nav-link @if(Request::is('admin/freelancer*')) active @endif">
                    <i class="mdi mdi-account-group"></i>
                    <span> {{ __('Freelancers') }} </span>
                </a>
            </li>
            <li class="side-nav-item @if(Request::is('admin/industries*') || Request::is('admin/categories*') || Request::is('admin/sub-categories*') || Request::is('admin/skills*') ) active @endif">
                <a href="javascript: void(0);" class="side-nav-link @if(Request::is('admin/industries*') || Request::is('admin/categories*') || Request::is('admin/sub-categories*') || Request::is('admin/skills*') ) active @endif">
                    <i class="mdi mdi-sitemap"></i>
                    <span> {{ __('Service Categories') }} </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level collapse" aria-expanded="false">

                <li class="@if(Request::is('admin/industries*') ) active @endif">
                       <a href="{{ route('admin.industries.index') }}" class="@if(Request::is('admin/industries*') ) active @endif">
                       <i class="mdi mdi-hospital-building"></i>
                      <span> {{ __('Industries')}} </span>
                      </a>
                   </li>

                   <li class="@if(Request::is('admin/categories*') ) active @endif">
                       <a href="{{ route('admin.categories.index') }}" class="@if(Request::is('admin/categories*') ) active @endif">
                       <i class="mdi mdi-microsoft"></i>
                      <span> {{ __('Categories')}} </span>
                      </a>
                   </li>

                   <li class="@if(Request::is('admin/sub-categories*') ) active @endif">
                       <a href="{{ route('admin.sub_categories.index') }}" class="@if(Request::is('admin/sub-categories*')) active @endif">
                       <i class="mdi mdi-view-module"></i>
                      <span> {{ __('Subcategories')}} </span>
                      </a>
                   </li>

                   <li class="@if(Request::is('admin/skills*') ) active @endif">
                       <a href="{{ route('admin.skills.index') }}" class="@if(Request::is('admin/skills*')) active @endif">
                       <i class="mdi mdi-view-headline"></i>
                      <span> {{ __('Skills')}} </span>
                      </a>
                   </li>

                </ul>
            </li>


            <li class="side-nav-item @if(Request::is('admin/subscription-payments*') || Request::is('admin/disputes*') || Request::is('admin/transactions*') || Request::is('admin/invoices*') || Request::is('admin/project-payments*')  ) active @endif">
                <a href="javascript: void(0);" class="side-nav-link @if(Request::is('admin/subscription-payments*') || Request::is('admin/disputes*') || Request::is('admin/transactions*') || Request::is('admin/invoices*') || Request::is('admin/project-payments*') ) active @endif">
                    <i class="mdi mdi-cash-usd"></i>
                    <span> {{ __('Finances') }} </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level collapse" aria-expanded="false">
                    <li class="@if(Request::is('admin/invoices*') ) active @endif">
                       <a href="{{ route('admin.invoices.index') }}" class="@if(Request::is('admin/invoices*') ) active @endif">
                       <i class="mdi mdi-receipt"></i>
                      <span> {{ __('Invoices')}} </span>
                      </a>
                    </li>
                    <li class="@if(Request::is('admin/project-payments*') ) active @endif">
                       <a href="{{ route('admin.project_payments.index') }}" class="@if(Request::is('admin/project-payments*') ) active @endif">
                       <i class="mdi mdi-square-inc-cash"></i>
                      <span> {{ __('Project Payments')}} </span>
                      </a>
                    </li>
                   <li class="@if(Request::is('admin/subscription-payments*') ) active @endif">
                       <a href="{{ route('admin.user_subscriptions.index') }}" class="@if(Request::is('admin/subscription-payments*') ) active @endif">
                       <i class="mdi mdi-square-inc-cash"></i>
                      <span> {{ __('Subscription Payments')}} </span>
                      </a>
                    </li>

                    <li class="@if(Request::is('admin/disputes*')) active @endif">
                        <a href="{{ route('admin.disputes.index') }}" class="@if(Request::is('admin/disputes*')) active @endif">
                        <i class="mdi mdi-view-list"></i>
                        <span> {{ __('Disputes') }} </span>
                        </a>
                    </li>

                    <li class="@if(Request::is('admin/transactions*')) active @endif">
                        <a href="{{ route('admin.transactions.index') }}" class="@if(Request::is('admin/transactions*')) active @endif">
                        <i class="mdi mdi-bell"></i>
                        <span> {{ __('Transactions') }} </span>
                        </a>
                    </li>
                    <li class="@if(Request::is('admin/wallets*')) active @endif">
                        <a href="{{ route('admin.membership_plans.wallets') }}" class="@if(Request::is('admin/wallets*')) active @endif">
                        <i class="mdi mdi-wallet-giftcard"></i>
                        <span> {{ __('Wallet Transactions') }} </span>
                        </a>
                    </li>

                </ul>
            </li>

            <!-- <li class="side-nav-item @if(Request::is('admin/disputes*')) active @endif">
                <a href="{{ route('admin.disputes.index') }}" class="side-nav-link @if(Request::is('admin/disputes*')) active @endif">
                    <i class="mdi mdi-view-list"></i>
                    <span> {{ __('Disputes') }} </span>
                </a>
            </li> -->

            <li class="side-nav-item @if(Request::is('admin/membership-plans*')) active @endif">
                <a href="{{ route('admin.membership_plans.index') }}" class="side-nav-link @if(Request::is('admin/membership-plans*')) active @endif">
                    <i class="mdi mdi-format-align-justify"></i>
                    <span> {{ __('Membership Plans') }} </span>
                </a>
            </li>

            <li class="side-nav-item @if(Request::is('admin/projects*')) active @endif">
                <a href="{{ route('admin.projects.index') }}" class="side-nav-link @if(Request::is('admin/projects*')) active @endif">
                    <i class="dripicons-copy"></i>
                    <span> {{ __('Projects') }} </span>
                </a>
            </li>
            <!-- <li class="side-nav-item @if(Request::is('admin/cms-page*')) active @endif">
                <a href="{{ route('admin.pages.index') }}" class="side-nav-link @if(Request::is('admin/cms-page*')) active @endif">
                    <i class="dripicons-copy"></i>
                    <span> {{ __('CMS Pages') }} </span>
                </a>
            </li> -->
           
         


            <!-- <li class="side-nav-item @if(Request::is('admin/frequently-asked-question*')) active @endif">
                <a href="{{ route('admin.frequently_asked_questions.index','user') }}" class="side-nav-link @if(Request::is('admin/frequently-asked-question*')) active @endif">
                    <i class="dripicons-question"></i>
                    <span> {{ __('FAQ') }} </span>
                </a>
            </li> -->
            <!-- <li class="side-nav-item @if(Request::is('admin/contact-us*')) active @endif">
                <a href="{{ route('admin.contact_us.index','user') }}" class="side-nav-link @if(Request::is('admin/contact-us*')) active @endif">
                    <i class="dripicons-question"></i>
                    <span> {{ __('Contact Us Form') }} </span>
                </a>
            </li> -->
         
            <li class="side-nav-item @if(Request::is('admin/email-template*') || Request::is('admin/sms-template*') || Request::is('admin/push-notification-template*') || Request::is('admin/in-app-notification-template*')) active @endif">
                <a href="javascript: void(0);" class="side-nav-link @if(Request::is('admin/email-template*') || Request::is('admin/sms-template*') || Request::is('admin/push-notification-template*') || Request::is('admin/in-app-notification-template*')) active @endif" aria-expanded="false">
                    <i class="dripicons-inbox"></i>
                    <span> {{ __('Templates') }} </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level collapse" aria-expanded="false">
                    <li class="@if(Request::is('admin/email-template*')) active @endif">
                        <a href="{{ route('admin.email_templates.index') }}" class="@if(Request::is('admin/email-template*')) active @endif">
                        <i class="mdi mdi-email"></i>
                        <span>{{ __('E-Mail') }}</span>
                        </a>
                    </li>
                    <!-- <li class="@if(Request::is('admin/sms-template*')) active @endif">
                        <a href="{{ route('admin.sms_templates.index') }}" class="@if(Request::is('admin/sms-template*')) active @endif">{{ __('SMS') }}</a>
                    </li>
                    <li class="@if(Request::is('admin/push-notification-template*')) active @endif">
                        <a href="{{ route('admin.push_notification_templates.index') }}" class="@if(Request::is('admin/push-notification-template*')) active @endif">{{ __('Push Notification') }}</a>
                    </li>
                    <li class="@if(Request::is('admin/in-app-notification-template*')) active @endif">
                        <a href="{{ route('admin.in_app_notification_templates.index') }}" class="@if(Request::is('admin/in-app-notification-template*')) active @endif">{{ __('In-App Notification') }}</a>
                    </li> -->
                </ul>
            </li>
            <!-- <li class="side-nav-item">
                <a href="javascript: void(0);" class="side-nav-link" aria-expanded="false">
                    <i class="mdi mdi-wrench"></i>
                    <span> {{ __('Tools') }} </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level collapse" aria-expanded="false">
                    <li>
                        <a href="/admin/telescope" target="_blank">{{ __('Track Requests') }}</a>
                    </li>
                    <li>
                        <a href="/horizon" target="_blank">{{ __('Queues') }}</a>
                    </li>
                    <li>
                        <a href="/langman" target="_blank">{{ __('Language Translator') }}</a>
                    </li>
                </ul>
            </li> -->
            <li class="side-nav-item @if(Request::is('admin/email-queue*') || Request::is('admin/sms-queue*') || Request::is('admin/push-notification-queue*')) active @endif">
                <a href="javascript: void(0);" class="side-nav-link @if(Request::is('admin/email-queue*') || Request::is('admin/sms-queue*') || Request::is('admin/push-notification-queue*')) active @endif" aria-expanded="false">
                    <i class="dripicons-archive"></i>
                    <span> {{ __('Queues') }} </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level collapse" aria-expanded="false">
                    <!-- <li class="@if(Request::is('admin/sms-queue*')) active @endif">
                        <a href="{{ route('admin.sms_queues.index') }}" class="@if(Request::is('admin/sms-queue*')) active @endif">{{ __('SMS') }}</a>
                    </li> -->
                    <li class="@if(Request::is('admin/email-queue*')) active @endif">
                        <a href="{{ route('admin.email_queues.index') }}" class="@if(Request::is('admin/email-queue*')) active @endif">
                        <i class="mdi mdi-email-variant"></i>
                        <span>{{ __('Email') }}</span>
                        </a>
                    </li>
                    <!-- <li class="@if(Request::is('admin/push-notification-queue*')) active @endif">
                        <a href="{{ route('admin.push_notification_queues.index') }}" class="@if(Request::is('admin/push-notification-queue*')) active @endif">{{ __('Push Notification') }}</a>
                    </li> -->
                </ul>
            </li>
            <!-- <li class="side-nav-item @if(Request::is('admin/setting*')) active @endif">
                <a href="{{ route('admin.settings.index','general') }}" class="side-nav-link @if(Request::is('admin/setting*')) active @endif">
                    <i class="dripicons-gear"></i>
                    <span> {{ __('Settings') }} </span>
                </a>
            </li> -->




            <li class="side-nav-item @if(Request::is('admin/cms-page*') || Request::is('admin/terms-and-conditions*') || Request::is('admin/security-questions*') || Request::is('admin/rating-questions*') || Request::is('admin/setting*')) active @endif">
                <a href="javascript: void(0);" class="side-nav-link @if(Request::is('admin/cms-page*') || Request::is('admin/terms-and-conditions*') || Request::is('admin/security-questions*') || Request::is('admin/rating-questions*') || Request::is('admin/setting*')) active @endif">
                <i class="dripicons-gear"></i>
                    <span> {{ __('Settings') }} </span>
                    <span class="menu-arrow"></span>
                </a>
                <ul class="side-nav-second-level collapse" aria-expanded="false">

                  <li class="@if(Request::is('admin/cms-page*') ) active @endif">
                       <a href="{{ route('admin.pages.index') }}" class="@if(Request::is('admin/cms-page*') ) active @endif">
                       <i class="dripicons-copy"></i>
                      <span> {{ __('CMS Pages')}} </span>
                      </a>
                   </li>

                   <li class="@if(Request::is('admin/terms-and-conditions*') ) active @endif">
                       <a href="{{ route('admin.terms_and_conditions.index') }}" class="@if(Request::is('admin/terms-and-conditions*') ) active @endif">
                       <i class="mdi mdi-pen"></i>
                      <span> {{ __('Terms And Conditions')}} </span>
                      </a>
                   </li>

                   <li class="@if(Request::is('admin/security-questions*') ) active @endif">
                       <a href="{{ route('admin.security_questions.index') }}" class="@if(Request::is('admin/security-questions*')) active @endif">
                       <i class="mdi mdi-security"></i>
                      <span> {{ __('Security Questions')}} </span>
                      </a>
                   </li>

                   <li class="@if(Request::is('admin/rating-questions*') ) active @endif">
                       <a href="{{ route('admin.rating_questions.index') }}" class="@if(Request::is('admin/rating-questions*')) active @endif">
                       <i class="mdi mdi-star-circle"></i>
                      <span> {{ __('Rating Questions')}} </span>
                      </a>
                   </li>

                   <li class="@if(Request::is('admin/setting*') ) active @endif">
                       <a href="{{ route('admin.settings.index','general') }}" class="@if(Request::is('admin/setting*')) active @endif">
                       <i class="dripicons-gear"></i>
                      <span> {{ __('General')}} </span>
                      </a>
                   </li>

                </ul>
            </li>





        </ul>
        <div class="clearfix"></div>
</div>
