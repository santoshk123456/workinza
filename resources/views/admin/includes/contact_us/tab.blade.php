<div class="tab-pane show active" id="home">
    <div id="accordion" class="custom-accordion mb-4">
        <form action="{{ route('admin.contact_us.update',$contact_us->id) }}" method="POST" class="form-horizontal">
            @csrf
            {{ method_field('PUT') }}
                <div class="form-group row mb-3">
                    <label for="name" class="col-3 col-form-label">Name</label>
                    <div class="col-9">
                        <input type="text" class="form-control" id="name" placeholder="Name" value="{{ $contact_us->name }}" name="name" required>
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label for="address" class="col-3 col-form-label">Address</label>
                    <div class="col-9">
                        <input type="text" class="form-control" id="location" placeholder="Address" name="address" value="{{ $contact_us->address }}" required>
                    </div>
                </div>
                <div class="row mb-3 form-group pl-3 pr-2">
                    <div id="geomap" style="" class="text-right ml-auto"></div>
                </div>
                <input type="hidden" name="real_address" class="search_addr" value="{{ $contact_us->address }}" size="45">
                <input type="hidden" name="latitude" class="search_latitude" value="{{ $contact_us->latitude }}" size="30">
                <input type="hidden" name="longitude" class="search_longitude" value="{{ $contact_us->longitude }}" size="30">

                <div class="form-group row mb-3 mt-3">
                    <label for="contact_email" class="col-3 col-form-label">Contact Email</label>
                    <div class="col-9">
                        <input type="email" class="form-control" id="contact_email" placeholder="Contact Email" name="contact_email" value="{{ $contact_us->contact_email }}" required>
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label for="contact_number" class="col-3 col-form-label">Contact Number</label>
                    <div class="col-9">
                        <input type="phone" class="form-control" id="contact_number" placeholder="Contact Number" name="contact_number" value="{{ $contact_us->contact_number }}" required>
                    </div>
                </div>

            <button class="btn btn-primary float-right mb-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
        </form>
    </div>
</div>
