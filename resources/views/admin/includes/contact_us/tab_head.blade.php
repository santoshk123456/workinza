<ul class="nav nav-tabs mb-3">
        <li class="nav-item">
            <a href="{{ route('admin.contact_us.index') }}" aria-expanded="false" class="nav-link active">
                <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                <span class="d-none d-lg-block">{{ __('Contact Us') }}</span>
            </a>
        </li>
</ul>