<div class="tab-pane show active" id="home">
    <div class="row mb-2">
        @can('add frequently asked questions')
            <div class="col-lg-4">
                <button onclick="window.location.href='{{ route('admin.frequently_asked_questions.create',[$type]) }}'" type="button" class="btn btn-danger mb-3 text-capitalize"><i
                    class="mdi mdi-plus"></i>{{$type}} {{ __('FAQ') }}</button>
            </div>
        @endcan
        <div class="col-lg-8">
            <div class="text-sm-right">
                <div class="btn-group mb-3 ml-1">
                    <div class="app-search d-block p-0 mr-4">
                        <form action="{{ route('admin.frequently_asked_questions.index',$type) }}">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search..." name="keyword">
                                <span class="mdi mdi-magnify"></span>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">{{ __('Search') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="accordion" class="custom-accordion mb-4">
        @php $key = 0; @endphp
        @foreach($frequently_asked_questions as $frequently_asked_question)
            <div class="card mb-0">
                <div class="card-header" id="heading-{{ $frequently_asked_question->id }}">
                    <h5 class="m-0">
                        <a class="custom-accordion-title d-block pt-2 pb-2 @if(!empty($key)) collapsed @endif" data-toggle="collapse" href="#collapseOne-{{ $frequently_asked_question->id }}"
                            aria-expanded="true" aria-controls="collapseOne">
                            {{ $frequently_asked_question->question }}
                            <i class="mdi mdi-chevron-down accordion-arrow float-right" title="View"></i>
                        </a>
                        <span class="add-edit-btn">
                            <i class="mdi mdi-square-edit-outline mr-4" title="Edit" onclick="event.preventDefault();window.location.href='{{ route('admin.frequently_asked_questions.edit',[$frequently_asked_question->id,$type]) }}'"></i>
                            <i class="mdi mdi-trash-can-outline mr-4" title="Delete" onclick="deleteForm('delete-form{{ $frequently_asked_question->id }}')"></i>
                            <form class="d-none" id="delete-form{{ $frequently_asked_question->id }}" method="post" action="{{ route('admin.frequently_asked_questions.destroy', $frequently_asked_question->id) }}"><input type="hidden" name="_method" value="delete"> @csrf </form>
                            
                        </span>
                    </h5>
                </div>
                <div id="collapseOne-{{ $frequently_asked_question->id }}" class="collapse @if(empty($key)) show @endif" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body"> {{ $frequently_asked_question->answer }} </div>
                </div>
            </div>
            @php $key++; @endphp
        @endforeach
        {{ $frequently_asked_questions->links() }}
    </div>
</div>
