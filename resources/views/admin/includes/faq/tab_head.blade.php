<ul class="nav nav-tabs mb-3">
    @foreach(config('settings.faq.types') as $setting_type)
        @if(empty($type))
            @php $type = $setting_type; @endphp
        @endif
        <li class="nav-item">
            <a href="{{ route('admin.frequently_asked_questions.index',$setting_type) }}" aria-expanded="false" class="nav-link @if($type == $setting_type) active @endif">
                <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                <span class="d-none d-lg-block">{{ ucwords($setting_type) }} {{ __('FAQ') }}</span>
            </a>
        </li>
    @endforeach
    {{-- <li class="nav-item">
        <a href="{{ route('admin.frequently_asked_questions.index','admin') }}" aria-expanded="true" class="nav-link @if($type == 'admin') active @endif">
            <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Admin FAQ') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin.frequently_asked_questions.index','guest') }}" aria-expanded="false" class="nav-link @if($type == 'guest') active @endif">
            <i class="mdi mdi-settings-outline d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block">{{ __('Guest FAQ') }}</span>
        </a>
    </li> --}}
</ul>