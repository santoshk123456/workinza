<ul class="nav nav-tabs mb-3">
    @foreach(config('settings.settings.types') as $setting_type)
        @if(empty($type))
            @php $type = $setting_type; @endphp
        @endif
        @if($setting_type != "mobile")
        <li class="nav-item">
            <a href="{{ route('admin.settings.index',$setting_type) }}" aria-expanded="false" class="nav-link @if($type == $setting_type) active @endif">
                <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                <span class="d-none d-lg-block">{{ ucwords($setting_type) }} {{ __('Setting') }}</span>
            </a>
        </li>
        @endif
    @endforeach
</ul>