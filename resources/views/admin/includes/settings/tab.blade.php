<div class="tab-pane show active" id="home">
    <div id="accordion" class="custom-accordion mb-4">
        <form action="{{ route('admin.settings.update',$type) }}" method="POST" class="form-horizontal">
            @csrf
            {{ method_field('PUT') }}
            @foreach($settings as $setting)
                <div class="form-group row mb-3">
                    <label for="input{{ $setting->uuid }}" class="col-3 col-form-label">{{ $setting->name }}</label>
                    <div class="col-9">
                        @if($setting->value_type == 'email')
                            <input type="email" class="form-control" id="input{{ $setting->uuid }}" placeholder="{{ $setting->name }}" name="{{ $setting->key }}" value="{{ $setting->value }}" required>
                        @elseif($setting->value_type == 'phone')
                            <input type="phone" class="form-control" id="input{{ $setting->uuid }}" placeholder="{{ $setting->name }}" name="{{ $setting->key }}" required>
                        @elseif($setting->value_type == 'textarea')
                            <textarea class="form-control" id="input{{ $setting->uuid }}" name="{{ $setting->key }}" required>{{ $setting->value }}</textarea>
                        @elseif($setting->value_type == 'checkbox')
                            <input type="checkbox" id="switch{{ $setting->uuid }}" @if($setting->value == 1) checked @endif data-switch="success" name="{{ $setting->key }}">
                            <label for="switch{{ $setting->uuid }}" data-on-label="On" data-off-label="Off"></label>
                        @elseif($setting->value_type == 'time')
                        <input type="time" class="form-control" id="input{{ $setting->uuid }}" placeholder="{{ $setting->name }}" name="{{ $setting->key }}"  value="{{ $setting->value }}"  required>
                        @elseif($setting->value_type == 'number')
                            <input type="number" class="form-control" id="input{{ $setting->uuid }}" placeholder="{{ $setting->name }}" value="{{ $setting->value }}" name="{{ $setting->key }}" required>
                        @else
                            <input type="text" class="form-control" id="input{{ $setting->uuid }}" placeholder="{{ $setting->name }}" value="{{ $setting->value }}" name="{{ $setting->key }}" required>
                        @endif
                    </div>
                </div>
            @endforeach

            <div class="row">
                  <div class="col-3 ">
                     <label for="admin_email" class="settting-label">Display Date format</label>
                  </div>
                  <div class="col-4 pr-2">
                     <select name="display_date_format" class="form-control">
                     @php
                     $date_format = Config::get('data.default_date_format');
                     @endphp
                     <option value="Y-m-d" @if($date_format =="Y-m-d"){{"selected"}} @endif>Y-m-d (2020-03-25)</option>
                       <option value="d-m-Y" @if($date_format =="d-m-Y"){{"selected"}} @endif>d-m-Y (25-03-2020)</option>
                       <option value="m-d-Y" @if($date_format =="m-d-Y"){{"selected"}} @endif>m-d-Y (03-25-2020)</option>
                       <option value="d-M-Y" @if($date_format =="d-M-Y"){{"selected"}} @endif >d-M-Y (25-Mar-2020)</option>
                       <option value="Y/m/d" @if($date_format =="Y/m/d"){{"selected"}} @endif>Y/m/d (2020/03/25)</option>
                       <option value="d/m/Y" @if($date_format =="d/m/Y"){{"selected"}} @endif>d/m/Y (25/03/2020)</option>
                       <option value="m/d/Y" @if($date_format =="m/d/Y"){{"selected"}} @endif>m/d/Y (03/25/2020)</option>
                       <option value="d/M/Y" @if($date_format =="d/M/Y"){{"selected"}} @endif>d/M/Y (25/Mar/2020)</option>
                       <option value="d M,Y" @if($date_format =="d M,Y"){{"selected"}} @endif>d M,Y (25 Mar,2020)</option>
                       <option value="M d,Y" @if($date_format =="M d,Y"){{"selected"}} @endif>M d,Y (Mar 25,2020)</option> 
                     </select>
                  </div>
               </div>



            <!-- <div class="row mt-3">
                  <div class="col-3">
                     <label for="curr_symbol" class="settting-label">Currency Symbol</label>
                  </div>
                  <div class="col-4 pr-2">
                     @php
                     $curr_sumbol = Config::get('data.currency_symbol');
                     @endphp
                     <input type="text" class="form-control" placeholder="Currncy Symbol" value="{{ $curr_sumbol }}" name="currency_symbol">
                  </div>
               </div> -->

            <button class="btn btn-primary float-right mb-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
        </form>
    </div>
</div>
