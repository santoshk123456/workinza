<script>
    $( document ).ready(function() {
        
        $("#pageNum").blur(function(){
            if(!$(this).val()){}
            else{
                var info = LaravelDataTables["basic-datatable"].page.info();
                total_pages = info.pages;
                current_page = $(this).val();
                current_page = parseInt(current_page) - 1;
                if(current_page <= total_pages && current_page >=0){
                    LaravelDataTables["basic-datatable"].page(parseInt(current_page)).draw(false);
                }
            }
            
        });

        $("#pageNum").keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                if(!$(this).val()){}
                else{
                    var info = LaravelDataTables["basic-datatable"].page.info();
                    total_pages = info.pages;
                    current_page = $(this).val();
                    current_page = parseInt(current_page) - 1;
                    if(current_page <= total_pages && current_page >=0){
                        LaravelDataTables["basic-datatable"].page(parseInt(current_page)).draw(false);
                    }
                }
            }
        });
    });
</script>