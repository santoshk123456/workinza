<head>
	<meta charset="utf-8" />
	<title>@yield('title', app_name())</title>
	<meta content="Login" />
	<meta content="2Base Technologies" name="author" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link rel="shortcut icon" href="{{ asset('images/user/favicon.ico') }}">
	<!-- App favicon -->
	<!-- css -->
	@stack('core-styles')
		{{ style('css/admin/jquery-jvectormap-1.2.2.css') }}
		{{ style('css/admin/icons.min.css') }}
		{{style('css/intlTelInput.min.css')}}
		{{ style(mix('css/admin/admin.css')) }}
		{{ style('css/admin/dropify.min.css') }}
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.css" integrity="sha512-bbUR1MeyQAnEuvdmss7V2LclMzO+R9BzRntEE57WIKInFVQjvX7l7QZSxjNDt8bg41Ww05oHSh0ycKFijqD7dA==" crossorigin="anonymous" />
		{{ style('css/admin/custom.css') }}
	@stack('custom-styles')
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css">
			<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
			{{ style('css/rateyo/jquery.rateyo.min.css') }}

	<!-- css -->
</head>
