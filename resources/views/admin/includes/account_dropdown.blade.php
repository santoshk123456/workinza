<div class="dropdown-menu dropdown-menu-right dropdown-menu-animated topbar-dropdown-menu profile-dropdown">
    <!-- item-->
    <div class=" dropdown-header noti-title">
        <h6 class="text-overflow m-0">{{ __('Welcome !') }}</h6>
    </div>

    <!-- item-->
    <a href="{{ route('admin.admins.show',auth()->user()->uuid) }}" class="dropdown-item notify-item">
        <i class="mdi mdi-account-circle mr-1"></i>
        <span>{{ __('My Account') }}</span>
    </a>

    <!-- item-->
    <a href="{{ route('admin.settings.index','general') }}" class="dropdown-item notify-item">
        <i class="mdi mdi-account-edit mr-1"></i>
        <span>{{ __('Settings') }}</span>
    </a>

    <!-- item-->
    <!-- <a href="javascript:void(0);" class="dropdown-item notify-item">
        <i class="mdi mdi-lifebuoy mr-1"></i>
        <span>{{ __('Support') }}</span>
    </a> -->

    <!-- item-->
    <!-- <a href="javascript:void(0);" class="dropdown-item notify-item">
        <i class="mdi mdi-lock-outline mr-1"></i>
        <span>{{ __('Lock Screen') }}</span>
    </a> -->

    <!-- item-->
    <a href="{{ route('admin.logout') }}" class="dropdown-item notify-item">
        <i class="mdi mdi-logout mr-1"></i>
        <span>{{ __('Logout') }}</span>
    </a>

</div>
