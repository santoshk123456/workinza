<div class="row">
    <div class="col-sm-12">
        <div class="card bg-primary">
            <div class="card-body profile-user-box">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="media">
                            <span class="float-left m-2 mr-4">
                                @if($admin->profile_image && (file_exists( public_path().'/storage/admin/profile-images/'.$admin->profile_image)))
                                    <!-- <img src="{{ asset('storage/admin/profile-images/'.$admin->profile_image) }}" style="height: 100px; width:100px;" alt=""
                                    class="rounded-circle img-thumbnail"> -->
                                    @php
                                        $image_with_path = 'storage/admin/profile-images'."/".$admin->profile_image;
                                        $url = thump_resize_image($image_with_path,100,100);
                                    @endphp
                                    <img src="{{ $url }}" style="" alt="" class="rounded-circle img-thumbnail">
                                @else 
                                    @php
                                        $image_with_path = 'images/admin/starter/default.png';
                                        $url = thump_resize_image($image_with_path,100,100);
                                    @endphp
                                    <img src="{{ $url }}" style="" alt=""
                                    class="rounded-circle img-thumbnail">
                                @endif
                            </span>
                            <div class="media-body align-self-center">
                                <h4 class="mt-1 mb-1 text-white">{{ $admin->name }}</h4>
                                <p class="font-13 text-white-50"> @if($admin->roles->first()) {{ ucwords($admin->roles->first()->name) }} @endif</p>
                            </div>
                        </div>
                    </div>
                    @if((auth()->user()->id == $admin->id))
                        <div class="col-sm-4">
                            <div class="text-center  mt-4 text-sm-right">
                                <button type="button" onclick="location.href='{{ route('admin.admins.profile-edit',$admin->uuid,$role) }}'" class="btn btn-light" data-toggle="modal" data-target="#myModal"> <i
                                        class="mdi mdi-account-edit mr-1"></i> {{ __('Edit Profile') }} </button>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
