<div class="tab-pane fade active show" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
    <h4 class="header-title mt-0 mb-3">{{ __('Personal Information') }}</h4>
    <div class="text-left">
        <p><strong>{{ __('Full Name :') }}</strong> <span class="ml-2">{{ $admin->name }}</span></p>
        <p><strong>{{ __('Role :') }}</strong> <span class="ml-2">@if($admin->roles->first()) {{ ucwords($admin->roles->first()->name) }} @else Not Specified @endif</span></p>
        <p><strong>{{ __('Mobile :') }}</strong><span class="ml-2">{{ $admin->FullNumber }}</span>
        </p>
        <p><strong>{{ __('Email :') }}</strong> <span class="ml-2">{{ $admin->email }}</span></p>
        <p><strong>{{ __('Username :') }}</strong> <span class="ml-2">{{ $admin->username }}</span></p>
        <p><strong>{{ __('Status :') }}</strong> <span class="ml-2">@if(!empty($admin->active)) {{ __('Active') }} @else {{ __('InActive') }} @endif </span></p>
        <p><strong>{{ __('Created At :') }}</strong> 
            <span class="ml-2">
                    {{ $admin->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($admin->created_at), timezone()->setShortDateTimeFormat())) : '' }}
            </span></p>
        </p>
        <p><strong>{{ __('Updated At :') }}</strong> 
            <span class="ml-2">
                    {{ $admin->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($admin->updated_at), timezone()->setShortDateTimeFormat())) : '' }}
            </span>
        </p>
    </div>
</div>