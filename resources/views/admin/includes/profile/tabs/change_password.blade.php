<div class="tab-pane fade active show" id="v-pills-chnagepass" role="tabpane5" aria-labelledby="v-pills-changepass-tab">
    <h4 class="header-title mt-0 mb-3">{{ __('Change Password') }}</h4>
    <form action="{{ route('admin.admins.change_password') }}" id="adminchangepassword-form" method="POST" class="col-sm-6 mx-auto needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
        @csrf
        <div class="form-group @if($errors->has('current_password')) validation-failed @endif">
            <label for="emailaddress">{{ __('Current Password') }}
                <span class="mandatory">*</span>
            </label>
            <input class="form-control" name="current_password" minlength="6" maxlength="15"  type="password" value="{{ old('current_password') }}" id="current_password" required placeholder="Enter your current password">
            <span style="color:#fa5c7c;" class="error-txt current_password-error"></span>
            <div class="invalid-feedback" >
            <!-- @if ($errors->has('current_password'))
                {{ $errors->first('current_password') }}
            @else 
                {{ __('Enter valid Password.')}}
            @endif -->
            </div>
        </div>
        <div class="form-group @if($errors->has('password')) validation-failed @endif">
            <label for="password">{{ __('New Password') }}
                <span class="mandatory">*</span>
            </label>
            <input class="form-control" value="{{ old('password') }}" name="password" data-rule-nospace="true"   data-rule-checkcapital="true"   data-rule-checknumber="true"  data-rule-checkspecial="true" type="password" required id="password" placeholder="Enter your new password">
            <span style="color:#fa5c7c;" class="error-txt password-error"></span>
            <div class="invalid-feedback" >
                <!-- @if ($errors->has('password'))
                    {{ $errors->first('password') }}
                @else 
                    {{ __('New Password cannot be empty.')}}
                @endif -->
            </div>
        </div>
        <div class="form-group @if($errors->has('password_confirmation')) validation-failed @endif">
            <label for="password">{{ __('Confirm Password') }}
                <span class="mandatory">*</span>
            </label>
            <input class="form-control" value="{{ old('password_confirmation') }}" name="password_confirmation" data-rule-nospace="true"   data-rule-checkcapital="true"   data-rule-checknumber="true"  data-rule-checkspecial="true" type="password" required id="password_confirmation" placeholder="Confirm your new password">
            <span style="color:#fa5c7c;" class="error-txt password_confirmation-error"></span>
            <div class="invalid-feedback" >
                <!-- @if ($errors->has('password_confirmation'))
                    {{ $errors->first('password_confirmation') }}
                @else 
                    {{ __('Confirm Password cannot be empty.')}}
                @endif -->
            </div>
        </div>

        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary" type="submit"> {{ __('Change Password') }} </button>
        </div>
    </form>
</div>
