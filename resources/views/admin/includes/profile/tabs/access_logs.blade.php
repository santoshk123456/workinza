<div class="tab-pane fade active show" id="v-pills-logs" role="tabpane3" aria-labelledby="v-pills-logs-tab">
    <h4 class="header-title mt-0 mb-3">{{ __('Access Logs') }}</h4>
    <div id="basic-datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">
            <div class="col-sm-12 acc_log_dtable">
                {!! $html->table(['class' => 'w-100 table nowrap table-striped','id'=>'basic-datatable','width'=>'100%'], true) !!}
            </div>
        </div>
    </div>
</div>
