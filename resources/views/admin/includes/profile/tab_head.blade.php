<div class="col-md-3">
    <div class="card">
        <div class="card-body">
            <div class="nav flex-column nav-pills" id="v-pills-tab" aria-orientation="vertical">
                <a class="px-0 nav-link @if(Route::currentRouteName() == 'admin.admins.show') active show @endif" href="{{ route('admin.admins.show',[$admin->uuid,$role]) }}"
                    aria-controls="v-pills-home" aria-selected="true">
                    <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                    <span class="d-none d-lg-block"><i class="mdi mdi-face-profile"></i> {{ __('Personal Information') }}</span>
                </a>
                <a class="px-0 nav-link @if(Route::currentRouteName() == 'admin.admins.access_logs') active show @endif" href="{{ route('admin.admins.access_logs',[$admin->uuid,$role]) }}"
                    aria-controls="v-pills-profile" aria-selected="false">
                    <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
                    <span class="d-none d-lg-block"><i class="mdi mdi-history"></i>
                        {{ __('Access Logs') }}</span>
                </a>
                <a class="px-0 nav-link @if(Route::currentRouteName() == 'admin.admins.activity_logs') active show @endif" href="{{ route('admin.admins.activity_logs',[$admin->uuid,$role]) }}"
                    aria-controls="v-pills-profile" aria-selected="false">
                    <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
                    <span class="d-none d-lg-block"><i class="mdi mdi-history"></i>
                        {{ __('Activity Logs') }}</span>
                </a>
                @if(auth()->user()->id == $admin->id)
                    <a class="px-0 nav-link @if(Route::currentRouteName() == 'admin.admins.show_change_password') active show @endif" id="v-pills-chnagepass-tab" href="{{ route('admin.admins.show_change_password',[$admin->uuid,$role]) }}" role="tab"
                        aria-controls="v-pills-settings" aria-selected="false">
                        <i class="mdi mdi-settings-outline d-lg-none d-block mr-1"></i>
                        <span class="d-none d-lg-block"><i class="mdi mdi-shield-key"></i> {{ __('Change Password') }}</span>
                    </a>
                @endif
                {{--  <a class="nav-link" id="v-pills-emailettings-tab" data-toggle="pill" href="#v-pills-emailettings"
                    role="tab" aria-controls="v-pills-emailettings" aria-selected="false">
                    <i class="mdi mdi-settings-outline d-lg-none d-block mr-1"></i>
                    <span class="d-none d-lg-block"><i class="mdi mdi-message-settings-variant"></i> Email
                        Settings</span>
                </a>  --}}
            </div>
        </div>
    </div>
</div>
