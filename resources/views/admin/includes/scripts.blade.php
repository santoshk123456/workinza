@stack('core-scripts')
	{{ script('js/admin/admin-theme.js') }}
	{{ script('js/admin.js') }}
  {{ script('js/admin/dropify.min.js') }}
  {{ script('js/admin/Chart.bundle.min.js') }}
  {{script('js/intlTelInput.min.js')}}
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.js" integrity="sha512-9WciDs0XP20sojTJ9E7mChDXy6pcO0qHpwbEJID1YVavz2H6QBz5eLoDD8lseZOb2yGT8xDNIV7HIe1ZbuiDWg==" crossorigin="anonymous"></script>
	{{ script('js/admin/custom.js') }}
  <script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qndsbka02qylh47uc3j7rvz39zthmfu52s263cw6jjv9we6z'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script> 
  //Toastr options
  toastr.options=
  {
      "closeButton":true
  };
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $('.dropify').dropify();
        });
</script>
<script>
    var drEvent1 = $('.dropify').dropify();

    drEvent1.on('dropify.afterClear', function(event, element)
	{
        if($('.edit-popup').hasClass('dropify-selected') )
		{
            $('input[name=remove_title]').val('removed');
            alert($('input[name=remove_title]').val());
        }
    });
     
</script>

<script>
function deactivateForm($element=null)
{
    Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to change status!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, change status!'
      }).then((result) => {
        if (result.value) {
            $('#'+$element).submit();
        }
      })
}
function activateForm($element=null)
{
    Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to change status!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, change status!'
      }).then((result) => {
        if (result.value) {
            $('#'+$element).submit();
        }
      })
}
</script>

@stack('custom-scripts')

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

{{script('js/rateyo/jquery.rateyo.js')}}

