<!-- Right Sidebar -->
<div class="right-bar">

    <div class="rightbar-title">
        <a href="javascript:void(0);" class="right-bar-toggle float-right">
            <i class="dripicons-cross noti-icon"></i>
        </a>
        <h5 class="m-0">{{ __('Settings') }}</h5>
    </div>

    <div class="slimscroll-menu rightbar-content">

        <!-- Settings -->
        <hr class="mt-0" />
        <h5 class="pl-3">{{ __('Basic Settings') }}</h5>
        <hr class="mb-0" />

        <div class="p-3">
            <div class="custom-control custom-checkbox mb-2">
                <input type="checkbox" class="custom-control-input" id="notifications-check" checked>
                <label class="custom-control-label" for="notifications-check">{{ __('Notifications') }}</label>
            </div>

            <div class="custom-control custom-checkbox mb-2">
                <input type="checkbox" class="custom-control-input" id="api-access-check">
                <label class="custom-control-label" for="api-access-check">{{ __('API Access') }}</label>
            </div>

            <div class="custom-control custom-checkbox mb-2">
                <input type="checkbox" class="custom-control-input" id="auto-updates-check" checked>
                <label class="custom-control-label" for="auto-updates-check">{{ __('Auto Updates') }}</label>
            </div>

            <div class="custom-control custom-checkbox mb-2">
                <input type="checkbox" class="custom-control-input" id="online-status-check" checked>
                <label class="custom-control-label" for="online-status-check">{{ __('Online Status') }}</label>
            </div>

            <div class="custom-control custom-checkbox mb-2">
                <input type="checkbox" class="custom-control-input" id="auto-payout-check">
                <label class="custom-control-label" for="auto-payout-check">{{ __('Auto Payout') }}</label>
            </div>

        </div>


        <!-- Timeline -->
        <hr class="mt-0" />
        <h5 class="pl-3">{{ __('Recent Activity') }}</h5>
        <hr class="mb-0" />
        <div class="pl-2 pr-2">
            <div class="timeline-alt">
                <div class="timeline-item">
                    <i class="mdi mdi-upload bg-info-lighten text-info timeline-icon"></i>
                    <div class="timeline-item-info">
                        <a href="#" class="text-info font-weight-bold mb-1 d-block">{{ __('You sold an item') }}</a>
                        <small>{{ __('Paul Burgess just purchased “Hyper - Admin Dashboard”!') }}</small>
                        <p class="mb-0 pb-2">
                            <small class="text-muted">{{ __('5 minutes ago') }}</small>
                        </p>
                    </div>
                </div>

                <div class="timeline-item">
                    <i class="mdi mdi-airplane bg-primary-lighten text-primary timeline-icon"></i>
                    <div class="timeline-item-info">
                        <a href="#" class="text-primary font-weight-bold mb-1 d-block">{{ __('Product on the Bootstrap
                            Market') }}</a>
                        <small>{{ __('Dave Gamache added') }}
                            <span class="font-weight-bold">{{ __('Admin Dashboard') }}</span>
                        </small>
                        <p class="mb-0 pb-2">
                            <small class="text-muted">{{ __('30 minutes ago') }}</small>
                        </p>
                    </div>
                </div>

                <div class="timeline-item">
                    <i class="mdi mdi-microphone bg-info-lighten text-info timeline-icon"></i>
                    <div class="timeline-item-info">
                        <a href="#" class="text-info font-weight-bold mb-1 d-block">{{ __('Robert Delaney') }}</a>
                        <small>{{ __('Send you message') }}
                            <span class="font-weight-bold">{{ __('"Are you there?"') }}</span>
                        </small>
                        <p class="mb-0 pb-2">
                            <small class="text-muted">{{ __('2 hours ago') }}</small>
                        </p>
                    </div>
                </div>

                <div class="timeline-item">
                    <i class="mdi mdi-upload bg-primary-lighten text-primary timeline-icon"></i>
                    <div class="timeline-item-info">
                        <a href="#" class="text-primary font-weight-bold mb-1 d-block">{{ __('Audrey Tobey') }}</a>
                        <small>{{ __('Uploaded a photo') }}
                            <span class="font-weight-bold">{{ __('"Error.jpg"') }}</span>
                        </small>
                        <p class="mb-0 pb-2">
                            <small class="text-muted">{{ __('14 hours ago') }}</small>
                        </p>
                    </div>
                </div>

                <div class="timeline-item">
                    <i class="mdi mdi-upload bg-info-lighten text-info timeline-icon"></i>
                    <div class="timeline-item-info">
                        <a href="#" class="text-info font-weight-bold mb-1 d-block">{{ __('You sold an item') }}</a>
                        <small>{{ __('Paul Burgess just purchased “Hyper - Admin Dashboard”!') }}</small>
                        <p class="mb-0 pb-2">
                            <small class="text-muted">{{ __('1 day ago') }}</small>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="rightbar-overlay"></div>
<!-- /Right-bar -->
