<div class="row">
    <div class="col-sm-12">
        <div class="card bg-primary">
            <div class="card-body profile-user-box">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="media">
                            <span class="float-left m-2 mr-4">
                                @if($user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$user->profile_image)))
                                    <img src="{{ asset('storage/user/profile-images/'.$user->profile_image) }}" style="height: 100px; width:100px;" alt="{{ $user->full_name }}"
                                    class="rounded-circle img-thumbnail">
                                @else 
                                    <img src="{{ asset('images/admin/starter/default.png') }}" style="height: 100px;" alt="{{ $user->full_name }}"
                                    class="rounded-circle img-thumbnail">
                                @endif
                            </span>
                            <div class="media-body align-self-center">
                                <h4 class="mt-1 mb-1 text-white">{{ $user->full_name }}</h4>
                            </div>
                        </div>
                    </div>
                    @if(auth()->user()->hasPermissionTo('edit clients'))
                        <div class="col-sm-6">
                            <div class="text-center  mt-4 text-sm-right">
                                <button type="button" onclick="location.href='{{ route('admin.freelancers.edit',$user->uuid) }}'" class="btn btn-light"> <i
                                        class="mdi mdi-account-edit mr-1"></i> {{ __('Edit Profile') }} </button>
                                @if($user->approved == 1)
                                    @if($user->active == 1)
                                        <button type="button" class="btn btn-danger" onclick="blockUser('Do you want to block this freelancer ?')"> <i
                                            class="mdi mdi-block-helper mr-1"></i> {{ __('Block') }} </button>
                                    @else
                                        <button type="button" class="btn btn-success" onclick="blockUser('Do you want to activate this freelancer ?')"> <i
                                            class="mdi mdi-check mr-1"></i> {{ __('Activate') }} </button>
                                    @endif
                                @else
                                    @if($user->approved == 0)
                                        <button type="button" class="btn btn-success" onclick="approveUser('Do you want to approve this freelancer ?',1)"> <i
                                            class="mdi mdi-check mr-1"></i> {{ __('Approve') }} </button>
                                        <button type="button" class="btn btn-danger" onclick="approveUser('Do you want to reject this freelancer ?',2)"> <i
                                            class="mdi mdi-block-helper mr-1"></i> {{ __('Reject') }} </button>
                                    @else
                                        <button type="button" class="btn btn-success" onclick="approveUser('Do you want to approve this freelancer ?',1)"> <i
                                            class="mdi mdi-check mr-1"></i> {{ __('Approve') }} </button>
                                    @endif
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
