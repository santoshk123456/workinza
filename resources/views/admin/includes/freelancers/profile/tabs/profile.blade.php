
<fieldset>
    <legend>Profile Details</legend>
    <div class="row">
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Full name') }}</label>
                <p>{{ $user->full_name }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Email') }}</label>
                <p>{{ $user->email }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Email Verified At') }}</label>
                <p>{{ !empty($user->email_verified_at) ? hcms_date(strtotime($user->email_verified_at),'date-time') : 'NA' }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Username') }}</label>
                <p>{{ $user->username }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Paypal Email') }}</label>
                <p>{{ !empty($user->userDetail->paypal_email) ? $user->userDetail->paypal_email : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Type') }}</label>
                <p>{{ $user->organization_type == 0 ? __('Company') : __('Individual') }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Member Since') }}</label>
                <p>{{ hcms_date(strtotime($user->created_at),'date-time') }}</p>
            </div>
        </div>
        @if(!empty($user->userDetail))
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Referal Code') }}</label>
                <p>{{ $user->userDetail->referral_code }}</p>
            </div>
        </div>
        @endif
        @if(!empty($user->userDetail->referral_user_id))
            <div class="col-md-4 mb-3">
                <div class="data-view">
                    <label>{{ __('Refered By') }}</label>
                    <p><a href="{{route('admin.freelancers.show',$user->userDetail->referedBy)}}">{{ $user->userDetail->referedBy->full_name }}</a></p>
                </div>
            </div>
        @endif
    </div>
</fieldset>
<fieldset>
    <legend>Contact Details</legend>
    <div class="row">
        <div class="col-md-6 mb-3">
            <div class="data-view">
                <label>{{ __('Address Line 1') }}</label>
                <p>

                {{ !empty($user->userDetail->address_line_1) ? $user->userDetail->address_line_1 : __('NA') }}
                </p>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <div class="data-view">
                <label>{{ __('Address Line 2') }}</label>
                <p>{{ !empty($user->userDetail->address_line_2) ? $user->userDetail->address_line_2 : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Country') }}</label>
                <p>{{ !empty($user->userDetail->country)?$user->userDetail->country->name:'NA' }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('State') }}</label>
                <p>{{ !empty($user->userDetail->state) ? $user->userDetail->state : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('City') }}</label>
                <p>{{ !empty($user->userDetail->city) ? $user->userDetail->city : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('ZipCode') }}</label>
                <p>{{ !empty($user->userDetail->zipcode) ? $user->userDetail->zipcode : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Website') }}</label>
                <p>{!! !empty($user->userDetail->website_url) ? '<a href="'.$user->userDetail->website_url.'" target="_blank">'.$user->userDetail->website_url.'</a>' : __('NA') !!}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Mobile Number') }}</label>
                <p>{{ !empty($user->phone) ? $user->full_number : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Mobile Verified At') }}</label>
                <p>{{ !empty($user->phone_verified_at) ? hcms_date(strtotime($user->phone_verified_at),'date-time') : __('NA') }}</p>
            </div>
        </div>
    </div>
</fieldset>
<fieldset>
    <legend>Public Profile Details</legend>
    <div class="row">
        <div class="col-md-6 mb-3">
            <div class="data-view">
                <label>{{ __('Profile Headline') }}</label>
                <p>{{ !empty($user->userDetail->profile_headline) ? $user->userDetail->profile_headline : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <div class="data-view">
                <label>{{ __('Industries') }}</label>
                <p>{{ !empty($user->industries) ? implode(', ', $user->industries->pluck('name')->toArray()) : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <div class="data-view">
                <label>{{ __('Categories') }}</label>
                <p>{{ !empty($user->categories) ? implode(', ', $user->categories->pluck('name')->toArray()) : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <div class="data-view">
                <label>{{ __('Sub Categories') }}</label>
                <p>{{ !empty($user->subCategories) ? implode(', ', $user->subCategories->pluck('name')->toArray()) : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <div class="data-view">
                <label>{{ __('Skills') }}</label>
                <p>{{ !empty($user->skills) ? implode(', ', $user->skills->pluck('name')->toArray()) : __('NA') }}</p>
            </div>
        </div>
        @if($user->organization_type == 0)
            <div class="col-md-4 mb-3">
                <div class="data-view">
                    <label>{{ __('Established Since') }}</label>
                    <p>{{ !empty($user->userDetail->established_since)?hcms_date(strtotime($user->userDetail->established_since),'date'):'NA' }}</p>
                </div>
            </div>
        @else
            <div class="col-md-4 mb-3">
                <div class="data-view">
                    <label>{{ __('Years of Experience') }}</label>
                    <p>{{ !empty($user->userDetail->years_of_experience) ? $user->userDetail->years_of_experience : __('NA') }}</p>
                </div>
            </div>
        @endif
        <div class="col-md-6 mb-3">
            <div class="data-view">
                <label>{{ __('Company Tagline') }}</label>
                <p>{{ !empty($user->userDetail->company_tagline) ? $user->userDetail->company_tagline : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <div class="data-view">
                <label>{{ __('Company / Individual History') }}</label>
                <p>{{ !empty($user->userDetail->user_history) ? $user->userDetail->user_history : __('NA') }}</p>
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <div class="data-view">
                <label>{{ __('Corporate Presentation') }}</label>
                <p>{!! !empty($user->userDetail->corporate_presentation) ? $user->userDetail->corporate_presentation : __('NA') !!}</p>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <div class="data-view">
                <label>{{ __('Video Links') }}</label>
                @php
                    $videos = json_decode($user->userDetail->video_urls);
                @endphp
                @if(!empty($videos))
                    @foreach ($videos as $video)
                        <ul>
                            <li><a href="{{$video}}">{{$video}}</a></li>
                        </ul>
                    @endforeach
                @else
                    <p>{{__('NA')}}</p>
                @endif
            </div>
        </div>
    </div>
</fieldset>