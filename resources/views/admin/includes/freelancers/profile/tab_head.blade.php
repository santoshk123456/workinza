<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'admin.freelancers.show') active @endif" href="{{ route('admin.users.show',[$user->uuid]) }}">
            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block"><i class="mdi mdi-face-profile"></i> {{ __('Personal Information') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'admin.users.access_logs') active @endif" href="{{ route('admin.users.access_logs',[$user->uuid]) }}">
            <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block"><i class="mdi mdi-history"></i>
                {{ __('Access Logs') }}</span>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if(Route::currentRouteName() == 'admin.users.activity_logs') active @endif" href="{{ route('admin.users.activity_logs',[$user->uuid]) }}">
            <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
            <span class="d-none d-lg-block"><i class="mdi mdi-history"></i>
                {{ __('Activity Logs') }}</span>
        </a>
    </li>
    {{--  <a class="nav-link" id="v-pills-emailettings-tab" data-toggle="pill" href="#v-pills-emailettings"
        role="tab" aria-controls="v-pills-emailettings" aria-selected="false">
        <i class="mdi mdi-settings-outline d-lg-none d-block mr-1"></i>
        <span class="d-none d-lg-block"><i class="mdi mdi-message-settings-variant"></i> Email
            Settings</span>
    </a>  --}}
</ul>
