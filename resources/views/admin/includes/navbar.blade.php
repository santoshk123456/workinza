<!-- Topbar Start -->
<div class="navbar-custom fixed-top navadmin">
    <ul class="list-unstyled topbar-right-menu float-right mb-0">
        

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user arrow-none mr-0" data-toggle="dropdown" href="#" role="button"
                aria-haspopup="false" aria-expanded="false">
                <span class="account-user-avatar">
                    @if(!empty(auth()->user()->profile_image) && (file_exists( public_path().'/storage/admin/profile-images/'.auth()->user()->profile_image)))
                        @php
                            $image_with_path = 'storage/admin/profile-images/'.auth()->user()->profile_image;
                            $url = thump_resize_image($image_with_path,32,32);
                        @endphp
                        <img src="{{ $url }}" alt="user-image" class="rounded-circle">
                    @else 
                        @php
                            $image_with_path = 'images/admin/starter/default.png';
                            $url = thump_resize_image($image_with_path,32,32);
                        @endphp
                        <img src="{{ $url }}" alt="user-image" class="rounded-circle">
                    @endif 
                </span>
                <span>
                    <span class="account-user-name">{{ auth()->user()->name }}</span>
                    <span class="account-position">{{ auth()->user()->roles()->first()->name }}</span>
                </span>
            </a>
            @include('admin.includes.account_dropdown')
            
        </li>

    </ul>
    <button class="button-menu-mobile open-left disable-btn">
        <i class="mdi mdi-menu"></i>
    </button>
</div>
<!-- end Topbar -->
