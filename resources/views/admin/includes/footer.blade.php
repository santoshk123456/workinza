<!-- Footer Start -->
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <p class="mb-0 f_15">© {{ date("Y") }} {{ __('Keyoxa Made With') }} <i class=" mdi mdi-cards-heart text-danger"></i> {{ __('by') }} <a href="https://www.2basetechnologies.com/" target="_blank" class="text-dark comp_link">{{ __('2Base Technologies Pvt Ltd') }}</a></p>
            </div>
            {{--  <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-md-block">
                    <a href="javascript: void(0);">{{ __('About') }}</a>
                    <a href="javascript: void(0);">{{ __('Support') }}</a>
                    <a href="javascript: void(0);">{{ __('Contact Us') }}</a>
                </div>
            </div>  --}}
        </div>
    </div>
</footer>
<!-- end Footer -->