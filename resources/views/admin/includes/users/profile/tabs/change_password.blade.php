<div class="tab-pane fade active show" id="v-pills-chnagepass" role="tabpane5" aria-labelledby="v-pills-changepass-tab">
    <h4 class="header-title mt-0 mb-3">{{ __('Change Password') }}</h4>
    <form action="{{ route('admin.admins.change_password') }}" method="POST" class="col-sm-6 mx-auto @if(!$errors->isEmpty()) after-form-submit @endif">
        @csrf
        <div class="form-group @if($errors->has('current_password')) validation-failed @endif">
            <label for="emailaddress">{{ __('Current Password') }}
                <span class="mandatory">*</span>
            </label>
            <input class="form-control" name="current_password" type="password" value="{{ old('current_password') }}" id="current_password" required placeholder="Enter your current password">
            @if ($errors->has('current_password'))
                <div class="invalid-feedback" >
                    {{ $errors->first('current_password') }}
                </div>
            @endif
        </div>
        <div class="form-group @if($errors->has('password')) validation-failed @endif">
            <label for="password">{{ __('New Password') }}
                <span class="mandatory">*</span>
            </label>
            <input class="form-control" value="{{ old('password') }}" name="password" type="password" required id="password" placeholder="Enter your new password">
            @if ($errors->has('password'))
                <div class="invalid-feedback" >
                    {{ $errors->first('password') }}
                </div>
            @endif
        </div>
        <div class="form-group @if($errors->has('password_confirmation')) validation-failed @endif">
            <label for="password">{{ __('Confirm Password') }}
                <span class="mandatory">*</span>
            </label>
            <input class="form-control" value="{{ old('password_confirmation') }}" name="password_confirmation" type="password" required id="password_confirmation" placeholder="Confirm your new password">
            @if ($errors->has('password_confirmation'))
                <div class="invalid-feedback" >
                    {{ $errors->first('password_confirmation') }}
                </div>
            @endif
        </div>

        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary" type="submit"> {{ __('Change Password') }} </button>
        </div>
    </form>
</div>
