@extends('admin.layouts.app')
@section('title', set_page_titile(__('Create Skills')))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('skills.create') }}
            </div>
            <h4 class="page-title">{{ __('Create Skills') }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{route('admin.skills.store')}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <div class="row">

                        <div class="col-lg-6 @if ($errors->has('skill_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Skill Name') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{old('skill_name')}}" name="skill_name" class="form-control" minlength="2"
                                    maxlength="50" required placeholder="Skill Name" autocomplete="off">
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('skill_name'))
                                    {{ $errors->first('skill_name') }}
                                    @else
                                    {{ __('Skill cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>

                        
                         <div class="col-lg-6 @if ($errors->has('categories_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="categories_ids">{{ __('Categories') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="categories_ids[]" id="category_ids" class="form-control" multiple="multiple" required>
                                        @foreach ($categories_list as $key=>$category)
                                        <option value="{{$category->id}}" @if( old('industry_id')==$category->id)
                                            selected="selected" @endif>{{ucwords($category->name)}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('categories_ids'))
                                            {{ $errors->first('categories_ids') }}
                                        @else 
                                            {{ __('Categories cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 @if ($errors->has('sub_categories_ids')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="sub_categories_ids">{{ __('Subcategories') }}
                                        <span class="mandatory">*</span>
                                    </label><br>
                                    <select name="sub_categories_ids[]" class="form-control" id="sub_category_ids" multiple="multiple" required>
                                        <option value=""></option>
                                    </select>
                                    <div class="invalid-feedback" >
                                        @if ($errors->has('sub_categories_ids'))
                                            {{ $errors->first('sub_categories_ids') }}
                                        @else 
                                            {{ __('Subcategories cannot be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                        

                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i
                                class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i
                                class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

<script>
    $(document).ready(function() {
     
        $('#industry_id').select2({
            placeholder: "{{__('Select Industries')}}",
        });
        $('#category_ids').select2({
            placeholder: "{{__('Select Categories')}}",
        });
        $('#sub_category_ids').select2({
            placeholder: "{{__('Select Subcategories')}}",
        });
    });


    
    $('#category_ids').change(function(){
        var sub_category_ids = $('#sub_category_ids').val();
        $.ajax({
            type: "GET",
            url: "{{url('/get-sub-categories')}}",
            data: {'category_ids':$(this).val()},
            success: function (response) {
                var new_options = "<option value =''></option>";
                $.each(response, function (index, value) { 
                    if(jQuery.inArray(index, sub_category_ids) !== -1){
                        new_options += "<option value='"+index+"' selected>"+value+"</option>";
                    }else{
                        new_options += "<option value='"+index+"'>"+value+"</option>";
                    }
                });
                $('#sub_category_ids').html(new_options);
                $('#sub_category_ids').change();
                $('#sub_category_ids').select2({
                    placeholder: "{{__('Select Sub Categories')}}",
                });
            }
        });
    });

</script>

@endpush
