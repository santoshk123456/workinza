@extends('admin.layouts.app')
@section('title', set_page_titile($skill->name.' - Skills'))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('skills.edit',$skill) }}
            </div>
            <h4 class="page-title">{{__($skill->name) }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{route('admin.skills.update',$skill->uuid)}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="row">

                        <div class="col-lg-6 @if ($errors->has('skill_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Skill Name') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{old('skill_name',$skill->name)}}" name="skill_name" class="form-control" minlength="2"
                                    maxlength="50" required placeholder="Skill Name" autocomplete="off">
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('skill_name'))
                                    {{ $errors->first('skill_name') }}
                                    @else
                                    {{ __('Skill cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>

                      

                        @php
                         $selected_cat = $skill->categories->pluck('id')->toArray();
                        @endphp
                        <div class="col-lg-6 @if ($errors->has('category_id')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Categories') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <select name="category_id[]" id="category_id" class="js-example-basic-multiple form-control"
                                    multiple="multiple" required>
                                    <option value=""  disabled>Select Category</option>
                                    @foreach ($categories_list as $key=>$category)
                                    <option value="{{$category->id}}" {{(in_array($category->id,$selected_cat)? "selected" : "")}}>{{ucwords($category->name)}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('category_id'))
                                    {{ $errors->first('category_id') }}
                                    @else
                                    {{ __('Category cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>
                        @php
                         $selected_subcat = $skill->subcategories->pluck('id')->toArray();
                        @endphp
                        <div class="col-lg-6 @if ($errors->has('subcategory_id')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Subcategories') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <select name="subcategory_id[]" id="subcategory_id" class="subcategories-multiple form-control"
                                    multiple="multiple" required>
                                    <option value=""  disabled>Select Category</option>
                                    @foreach ($subcategories_list as $key=>$subcategory)
                                    <option value="{{$subcategory->id}}" {{(in_array($subcategory->id,$selected_subcat)? "selected" : "")}}>{{ucwords($subcategory->name)}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('subcategory_id'))
                                    {{ $errors->first('subcategory_id') }}
                                    @else
                                    {{ __('Subcategory cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>
                        
                        <div class="col-lg-6">
                            <div class="form-group active-checkbox">
                                <input  name="active" type="checkbox" id="switch3" @if($skill->active == 1) checked @endif data-switch="success">
                                <label for="switch3" data-on-label="Active" data-off-label="Inactive"></label>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i
                                class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i
                                class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

<script>
$(document).ready(function() {
  
        $('#industry_id').select2({
            placeholder: "{{__('Select Industries')}}",
        });
        $('#category_id').select2({
            placeholder: "{{__('Select Categories')}}",
        });
        $('#subcategory_id').select2({
            placeholder: "{{__('Select Sub Categories')}}",
        });
  
     
      
    });

    $('#industry_id').change(function(){
        var category_ids = $('#category_id').val();
        $.ajax({
            type: "GET",
            url: "{{url('/get-categories')}}",
            data: {'industry_ids':$(this).val()},
            success: function (response) {
                var new_options = "<option value =''></option>";
                $.each(response, function (index, value) { 
                    if(jQuery.inArray(index, category_ids) !== -1){
                        new_options += "<option value='"+index+"' selected>"+value+"</option>";
                    }else{
                        new_options += "<option value='"+index+"'>"+value+"</option>";
                    }
                });
                $('#category_id').html(new_options);
                $('#category_id').change();
                $('#category_id').select2({
                    placeholder: "{{__('Select Categories')}}",
                });
            }
        });
    });
    $('#category_id').change(function(){
        var sub_category_ids = $('#subcategory_id').val();
        $.ajax({
            type: "GET",
            url: "{{url('/get-sub-categories')}}",
            data: {'category_ids':$(this).val()},
            success: function (response) {
                var new_options = "<option value =''></option>";
                $.each(response, function (index, value) { 
                    if(jQuery.inArray(index, sub_category_ids) !== -1){
                        new_options += "<option value='"+index+"' selected>"+value+"</option>";
                    }else{
                        new_options += "<option value='"+index+"'>"+value+"</option>";
                    }
                });
                $('#subcategory_id').html(new_options);
                $('#subcategory_id').change();
                $('#subcategory_id').select2({
                    placeholder: "{{__('Select Sub Categories')}}",
                });
            }
        });
    });
</script>

@endpush
