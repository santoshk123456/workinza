@extends('admin.layouts.app')
@section('title', set_page_titile(__('Email Templates Create')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{ Breadcrumbs::render('email_templates.create') }}
            </div>
            <h4 class="page-title">{{ __('Create Email Template') }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.email_templates.store')}}"
            class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
            @csrf
            <div class="row">
                <div class="col-12 mb-3 @if ($errors->has('static_email_heading')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom01">{{ __('Static Email Heading') }}</label>
                        <input type="text" value="{{old('static_email_heading')}}" name="static_email_heading" class="form-control" id="validationCustom01" placeholder="Static Email Heading" required>
                        @if ($errors->has('static_email_heading'))
                        <div class="invalid-feedback">
                            {{ $errors->first('static_email_heading') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-12 mb-3 @if ($errors->has('subject')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom02">{{ __('Subject') }}</label>
                        <textarea name="subject" class="form-control" id="validationCustom02" rows="3" required>{{old('subject')}}</textarea>
                        @if ($errors->has('subject'))
                        <div class="invalid-feedback" >
                            {{ $errors->first('subject') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-12 mb-3 @if ($errors->has('description')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom03">{{ __('Description') }}</label>
                        <textarea name="description" class="form-control" id="validationCustom03" rows="3" required>{{old('description')}}</textarea>
                        @if ($errors->has('description'))
                        <div class="invalid-feedback" >
                            {{ $errors->first('description') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-12 mb-3 @if ($errors->has('template')) validation-failed @endif">
                    <div class="form-group">
                        <label for="validationCustom04">{{ __('Template') }}</label>
                        <textarea name="template" class="form-control" id="validationCustom04" rows="3" required>{{old('template')}}</textarea>
                        @if ($errors->has('template'))
                        <div class="invalid-feedback" >
                            {{ $errors->first('template') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-12 text-right px-0">
                    <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                    <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                </div> 
            </div>
        </form>
    </div>
</div>
@endsection

