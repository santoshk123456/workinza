<div class="row mt-5">
    <div style="width: 700px; margin: 0 auto; height: auto; background-color: #f4f4f4; padding-bottom: 10px; font-family: 'Roboto', sans-serif; color: #666666; position: relative;"
        data-mce-style="width: 700px; margin: 0 auto; height: auto; background-color: #f4f4f4; padding-bottom: 10px; font-family: 'Roboto', sans-serif; color: #666666; position: relative;">
        <div style="height: 190px;background-color: #2E1A47;text-align: center;"
            data-mce-style="height: 250px; background-color: #354168; text-align: center;"><img
                style="margin-top: 50px;" src="{{ URL::to('/') }}/images/user/150_fordark_bg.png" width="215" height="auto" alt=""
                data-mce-style="margin-top: 15px;"></div>
        {!! $email_html !!}
        <div style="width: 590px; margin: 0 auto; font-size: 12px; padding: 10px 5px; line-height: 1.9; text-align: center;"
            data-mce-style="width: 590px; margin: 0 auto; font-size: 12px; padding: 10px 5px; line-height: 1.9; text-align: center;">
            {{-- <a href="http://fb.com" target="_blank" rel="noopener" data-mce-href="http://fb.com"><img
                    style="margin-right: 10px;"
                    src="{{ URL::to('/') }}/images/email/icon_facebook.png"
                    data-mce-src="../../../images/icon_facebook.png" data-mce-style="margin-right: 10px;"></a>
            <a href="#insta" target="_blank" rel="noopener" data-mce-href="#insta"><img style="margin-right: 10px;"
                    src="{{ URL::to('/') }}/images/email/icon_insta.png"
                    data-mce-src="../../../images/icon_insta.png" data-mce-style="margin-right: 10px;"></a>
            <a href="#twitter" target="_blank" rel="noopener" data-mce-href="#twitter"><img style="margin-right: 10px;"
                    src="{{ URL::to('/') }}/images/email/icon_twitter.png"
                    data-mce-src="../../../images/icon_twitter.png" data-mce-style="margin-right: 10px;"></a>
            <a href="#youtube" target="_blank" rel="noopener" data-mce-href="#youtube"><img style="margin-right: 10px;"
                    src="{{ URL::to('/') }}/images/email/icon_youtube.png"
                    data-mce-src="../../../images/icon_youtube.png" data-mce-style="margin-right: 10px;"></a> --}}
            <br> Copyright © <?php echo date("Y"); ?> <a href="/" target="_blank" rel="noopener" data-mce-href="#/">Keyoxa</a>.
        </div>
    </div>
</div>
