@extends('admin.layouts.app')
@section('title', set_page_titile(__('Email Template Edit')))
@push('custom-styles')
{{ style('css/admin/summernote-bs4.css') }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('email_templates.edit',$emailTemplate) }}
            </div>
            <h4 class="page-title">{{ __('Edit Email Template') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('admin.email_templates.update', $emailTemplate)}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-12 mb-3 @if ($errors->has('static_email_heading')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom01">{{ __('Static Email Heading') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{$emailTemplate->static_email_heading}}" name="static_email_heading" class="form-control" id="validationCustom01" placeholder="Static Email Heading" readonly required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('static_email_heading'))
                                        {{ $errors->first('static_email_heading') }}
                                    @else 
                                        {{ __('Email heading cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mb-3 @if ($errors->has('subject')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Subject') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <textarea name="subject" class="form-control" id="validationCustom02" rows="3" required>{{$emailTemplate->subject}}</textarea>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('subject'))
                                        {{ $errors->first('subject') }}
                                    @else 
                                        {{ __('Subject cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mb-3 @if ($errors->has('description')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom03">{{ __('Description') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <textarea name="description" class="form-control" id="validationCustom03" rows="3" required>{{$emailTemplate->description}}</textarea>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('description'))
                                        {{ $errors->first('description') }}
                                    @else 
                                        {{ __('Description cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                        </div>
{{--                         <div class="col-12 mb-3 @if ($errors->has('template')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom04">{{ __('Template') }}</label>
                                <textarea name="template" class="form-control" id="validationCustom04" rows="3" required>{{$emailTemplate->template}}</textarea>
                                @if ($errors->has('template'))
                                <div class="invalid-feedback" >
                                    {{ $errors->first('template') }}
                                </div>
                                @endif
                            </div>
                        </div> --}}
                    </div>
                        <label for="summernote-basic">{{ __('Template') }}</label>
                        <textarea id="summernote-basic"  name="template" required>{{ $emailTemplate->template }}</textarea>
                        <div class="col-12 text-right px-0">
                            <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                            <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                        </div>   
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>            
@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}
@endpush
