@extends('admin.layouts.app')
@section('title', set_page_titile(__('Email Template Detail Page')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('email_templates.show',$emailTemplate) }}
            </div>
            <h4 class="page-title">{{ __('View '.$emailTemplate->subject) }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Static Email Heading') }}</label>
                    <p>{{ $emailTemplate->static_email_heading }}</p>
                </div>
            </div>
            <div class="col-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Subject') }}</label>
                    <p>{{ $emailTemplate->subject }}</p>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Description') }}</label>
                    <p>{{ $emailTemplate->description }}</p>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Created At') }}</label>
                    <p>{{ $emailTemplate->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($emailTemplate->created_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                    
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Modified At') }}</label>
                    <p>{{ $emailTemplate->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($emailTemplate->updated_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div class="data-view">
                    <label>{{ __('Template') }}</label>
                    <p class="pt-5">{!! $emailTemplate->template !!}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
