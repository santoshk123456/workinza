@extends('admin.layouts.app')
@section('title', set_page_titile(__('Email Queues')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('email_queues') }}
            <h4 class="page-title">{{ __('Email Queues') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable','width'=>'100%']) !!}
                    <div class="col-12 text-right px-0">
                        <div class="form-group row">
                            <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                            <div class="col-sm-1 mt-1">
                                <input type="text" id="pageNum" autocomplete="off" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                    
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}

<script>
    // $( document ).ready(function() {
        
    //     $("#pageNum").blur(function(){
    //         if(!$(this).val()){}
    //         else{
    //             var info = LaravelDataTables["basic-datatable"].page.info();
    //             total_pages = info.pages;
    //             current_page = $(this).val();
    //             current_page = parseInt(current_page) - 1;
    //             if(current_page <= total_pages && current_page >=0){
    //                 LaravelDataTables["basic-datatable"].page(parseInt(current_page)).draw(false);
    //             }
    //         }
            
    //     });

    //     $("#pageNum").keypress(function(event){
    //         var keycode = (event.keyCode ? event.keyCode : event.which);
    //         if(keycode == '13'){
    //             if(!$(this).val()){}
    //             else{
    //                 var info = LaravelDataTables["basic-datatable"].page.info();
    //                 total_pages = info.pages;
    //                 current_page = $(this).val();
    //                 current_page = parseInt(current_page) - 1;
    //                 // alert(current_page+" "+total_pages);
    //                 if(current_page <= total_pages && current_page >=0){
    //                     LaravelDataTables["basic-datatable"].page(parseInt(current_page)).draw(false);
    //                 }
    //             }
    //         }
    //     });
    // });
   
</script>
@endpush
