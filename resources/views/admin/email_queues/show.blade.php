@extends('admin.layouts.app')
@section('title', set_page_titile(__('Email Queue Detail Page')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('email_queues.show', $emailQueue) }}
            </div>
            <h4 class="page-title">{{ __('Email Queue Detail Page') }}</h4>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-4 mb-3">
                <div class="data-view">
                    <label>{{ __('Email') }}</label>
                    <p>{{ $emailQueue->to_email }}</p>
                </div>
            </div>
            <div class="col-lg-4 mb-3">
                <div class="data-view">
                    <label>{{ __('Subject') }}</label>
                    <p>{{ $emailQueue->subject }}</p>
                </div>
            </div>
            <div class="col-lg-4 mb-3">
                <div class="data-view">
                    <label>{{ __('Status') }}</label>
                    @if($emailQueue->sent_status == 1)
                        <p><span class="badge badge-success px-2">{{ __('Sent') }}</span></p>
                    @else
                        <p><span class="badge badge-danger px-2">{{ __('Failed') }}</span></p>
                    @endif
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Created At') }}</label>
                    <p>{{ $emailQueue->created_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($emailQueue->created_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                    
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="data-view">
                    <label>{{ __('Modified At') }}</label>
                    <p>{{ $emailQueue->updated_at ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($emailQueue->updated_at), timezone()->setShortDateTimeFormat())) : '' }}</p>
                </div>
            </div>
            <div class="col-lg-6 mb-3 mx-auto">
                <div class="data-view template-view">
                    <p>{!! $emailQueue->body !!}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
