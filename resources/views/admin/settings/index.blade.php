@extends('admin.layouts.app')
@section('title', set_page_titile(__('Settings Listing')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            {{ Breadcrumbs::render('settings') }}
            <h4 class="page-title">{{ __('Settings') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card p-0 m-0">
            <div class="card-body p-0 m-0">
                @include('admin.includes.settings.tab_head')
                <div class="tab-content p-3">
                    @include('admin.includes.settings.tab')
                </div>
            </div>
            <!-- end card-body -->
        </div>
        <!-- end card-->
    </div>
    <!-- end col -->
</div>
<!-- end row -->
@endsection
@push('custom-scripts')
<script>
</script>
@endpush
