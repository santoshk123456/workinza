@extends('admin.layouts.app')
@section('title', set_page_titile(__('Admin Users Listing')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        
        <div class="page-title-box">        
            {{ Breadcrumbs::render('admin_users') }}
            <h4 class="page-title">{{ __('Admin Users') }}</h4>
            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills bg-nav-pills nav-justified mb-3">
                    <li class="nav-item">
                        <a href="{{ route('admin.admins.index') }}" aria-expanded="false" class="nav-link rounded-0 @if(empty($role)) active @endif">
                            <i class="mdi mdi-home-variant d-lg-none d-block mr-1"></i>
                            <span class="d-none d-lg-block">{{ __('All') }}</span>
                        </a>
                    </li>
                    @foreach($roles as $get_role)
                        <li class="nav-item border-right border-left">
                            <a href="{{ route('admin.admins.index',str_replace(" ","-",$get_role->name)) }}" aria-expanded="true" class=" nav-link rounded-0 @if(str_replace("-"," ",$role) == $get_role->name) active @endif">
                                <i class="mdi mdi-account-circle d-lg-none d-block mr-1"></i>
                                <span class="d-none d-lg-block">{{ ucwords($get_role->name) }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <div class="row">
                    <div class="col-sm-4">
                        @can('add admin users')
                            <button onclick="location.href='{{ route('admin.admins.create',$role) }}'" type="button" class="btn btn-danger mb-3"><i class="mdi mdi-plus"></i> {{ __('Admin User') }}</button>
                        @endcan
                    </div>
                    <div class="col-sm-8">
                        <div class="text-sm-right">
                            <div class="btn-group mb-3 ml-1 status-btns">
                                <button type="button" data-type="all" class="btn btn-primary">{{ __('All') }}</button>
                                <button type="button" data-type="active" class="btn btn-light">{{ __('Active') }}</button>
                                <button type="button" data-type="inactive" class="btn btn-light">{{ __('In Active') }}</button>
                            </div>
                            <input type="hidden" name="status_filter" id="status-filter" value="">
                        </div>
                    </div>
                </div>
                <div id="basic-datatable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            {!! $dataTable->table(['class' => 'table nowrap table-striped','id'=>'basic-datatable'], true) !!}
                            <div class="col-12 text-right px-0">
                                <div class="form-group row">
                                    <label class="col-sm-11 col-form-label">{{ __('Jump to page :') }}</label>
                                    <div class="col-sm-1 mt-1">
                                        <input type="text" id="pageNum" class="form-control form-control-sm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $dataTable->scripts() !!}
<script>
    $('.nav-item a').each(function(){
        if($(this).hasClass('active')){
            $(this).parent().css('background-color','#dedfe6');
        }
    });

   $(document).on('click','.btn-group button',function(){
       $('#status-filter').val($(this).attr('data-type'));
       $('#basic-datatable').DataTable().draw();
   })
   $(document).on('change','input[type=checkbox]',function(){
        Swal.fire({
            title: 'Are you sure?',
            text: "The current status of the admin user will be changed.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Update Status!'
        }).then((result) => {
            if (result.value) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "/admin/toggle-active-status/"+$(this).attr('data-admin')
                });
                Swal.fire(
                    'Status Updated!',
                    'The status of the admin user has been updated.',
                    'success'
                )
                setTimeout(function(){
                $(".status-btns .btn-primary").click();
                },1);
            } else {
                if($(this).prop("checked") ==  true){
                    $(this).prop("checked",false);
                } else {
                    $(this).prop("checked",true);
                }
            }
        })
   })
</script>
<script>
    $("document").ready(function () {
        $('.status-btns button:first-of-type').addClass('btn-primary');
    });
    $('.status-btns button').click(function(){
        $(this).addClass('btn-primary');
        $(this).siblings().removeClass('btn-primary');
        $(this).siblings().addClass('btn-light');
        $(this).removeClass('btn-light');
    });
</script>
@endpush
