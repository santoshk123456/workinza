@extends('admin.layouts.app')
@section('title', set_page_titile(__($admin->name.' - Admin Users')))
@push('custom-styles')
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box py-1">
            <div class="page-title-right">
                {{ Breadcrumbs::render('admin_users.edit',$admin) }}
            </div>
            <h4 class="page-title">{{ $admin->name }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.admins.update',[$admin,$role])}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="validationCustom01">{{ __('Name') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('name',$admin->name) }}" name="name" class="form-control" id="validationCustom01" placeholder="Name" required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @else 
                                        {{ __('Name cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 @if ($errors->has('username')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Username') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input value="{{ old('username',$admin->username) }}" type="text" name="username" class="form-control" id="validationCustom02" placeholder="Username" required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('username'))
                                        {{ $errors->first('username') }}
                                    @else 
                                        {{ __('User Name cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 @if ($errors->has('email')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom03">{{ __('Email') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input value="{{ old('email',$admin->email) }}" type="text" name="email" class="form-control" id="validationCustom03" placeholder="Email" required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('email'))
                                        {{ $errors->first('email') }}
                                    @else 
                                        {{ __('Email cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 @if ($errors->has('phone')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="phone">{{ __('Mobile Number') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    @php
                                        $full_number = '+'.$admin->country_code.$admin->mobile_number;
                                    @endphp
                                    <input type="hidden" name="country_code" id="country_code" value="{{old('country_code',$admin->country_code)}}">
                                    <input value="{{old('full_number',$full_number)}}" type="text" name="phone" class="form-control numericOnly" id="phone" minlength="8" maxlength="15" placeholder="{{__('Enter Mobile Number')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('phone')) style="display:block" @endif>
                                        @if ($errors->has('phone'))
                                            {{ $errors->first('phone') }}
                                        @else 
                                            {{ __('Phone number can not be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-select">{{ __('Role') }}
                                    <span class="mandatory">*</span>
                                </label>
                                @php 
                                    $selected_role_id = "";
                                    $selected_role = $admin->roles()->first();
                                    if($selected_role){
                                        $selected_role_id = $selected_role->id;
                                    }
                                @endphp
                                <select name="role" class="form-control" id="example-select">
                                    @foreach ($roles as $get_role)
                                        <option value="{{ $get_role->id }}" @if($selected_role_id == $get_role->id) selected @endif>{{ ucwords($get_role->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @if($admin->id != 1)
                            <div class="col-lg-6">
                                <div class="form-group active-checkbox">
                                    <input  name="active" type="checkbox" id="switch3" @if($admin->active == 1) checked @endif data-switch="success">
                                    <label for="switch3" data-on-label="Active" data-off-label="Inactive"></label>
                                </div>
                            </div>
                        @endif
                    <div class="col-lg-12 @if ($errors->has('profile_image')) validation-failed @endif edit-popup">
                        <div class="form-group">
                                <label>{{ __('Profile Image ') }} </label>
                                <div class="input-group file">
                                @php 
                                $url = asset('storage/admin/profile-images'.$admin->profile_image);
                                @endphp
                                
                                    @if(!empty($admin->profile_image))
                                    <input type="file" id="drop1" class="dropify"  data-default-file="{{ $url }}" name="profile_image" />
                                    <input type="hidden" name="remove_title">
                                    @else
                                    <input type="file" id="drop1" class="dropify" accept = 'image/jpeg , image/jpg, image/gif, image/png'  name="profile_image" />
                                    @endif
                                    @if ($errors->has('profile_image'))
                                       <div class="invalid-feedback" @if ($errors->has('profile_image')) style="display:block" @endif>
                                           {{ $errors->first('profile_image') }} 
                                       </div>
                                     @endif
                                </div>
                            </div>
                        </div> 
                        {{-- <div class="col-lg-6 @if ($errors->has('profile_image')) validation-failed @endif">
                            <div class="form-group">
                                <label>{{ __('Profile Image') }}</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="inputGroupFile04" name="profile_image">
                                        <label class="custom-file-label" for="inputGroupFile04">@if(!empty($admin->profile_image)) {{ $admin->profile_image }} @else Choose file @endif</label>
                                    </div>
                                    @if ($errors->has('profile_image'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('profile_image') }} 
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div> 
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
<script>
    var drEvent1 = $('.dropify').dropify();
    drEvent1.on('dropify.afterClear', function(event, element){
        $('input[name=remove_title]').val('removed');
    });  


    var phone = document.querySelector("#phone");
    var phone_iti = window.intlTelInput(phone,({

        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = "in";//(resp && resp.country) ? resp.country : "in";
                callback(countryCode);
            });
        },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.1.0/js/utils.js",
        separateDialCode: true,
        hiddenInput: "full_number",
        autoPlaceholder: "polite",
        initialCountry: "us",
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            return selectedCountryPlaceholder.replace(/[0-9]/g,"x");
        }, 
    }));
    phone.addEventListener("countrychange", function() {
        $('#country_code').val(phone_iti.getSelectedCountryData().dialCode);
    });
</script>
@endpush
