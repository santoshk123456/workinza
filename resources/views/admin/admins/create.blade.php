@extends('admin.layouts.app')
@section('title', set_page_titile(__('Create New Admin User')))
@push('custom-styles')
@endpush
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('admin_users.create') }}
            </div>
            <h4 class="page-title">{{ __('Admin Users') }}</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.admins.store',$role)}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <div class="row">
                        
                        <div class="col-lg-6 @if ($errors->has('username')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom01">{{ __('Name') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{old('name')}}" name="name" class="form-control" id="validationCustom01" placeholder="Name" required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @else 
                                        {{ __('Name cannot be empty.')}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 @if ($errors->has('username')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ _('Username') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input value="{{old('username')}}" type="text" name="username" class="form-control" id="validationCustom02" placeholder="Username" required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('username'))
                                        {{ $errors->first('username') }}
                                    @else 
                                        {{ __('Username cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 @if ($errors->has('email')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom03">{{ __('Email') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input value="{{old('email')}}" type="text" name="email" class="form-control" id="validationCustom03" placeholder="Email" required>
                                <div class="invalid-feedback" >
                                    @if ($errors->has('email'))
                                        {{ $errors->first('email') }}
                                    @else 
                                        {{ __('Email cannot be empty.') }}
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 @if ($errors->has('phone')) validation-failed @endif">
                                <div class="form-group">
                                    <label for="phone">{{ __('Mobile Number') }}
                                        <span class="mandatory">*</span>
                                    </label>
                                    <input type="hidden" name="country_code" id="country_code" value="{{old('country_code',1)}}">
                                    <input value="{{old('full_number')}}" type="text" name="phone" class="form-control numericOnly" id="phone" minlength="8" maxlength="15" placeholder="{{__('Enter Mobile Number')}}" required>
                                    <div class="invalid-feedback"  @if ($errors->has('phone')) style="display:block" @endif>
                                        @if ($errors->has('phone'))
                                            {{ $errors->first('phone') }}
                                        @else
                                            {{ __('Phone number can not be empty.') }}
                                        @endif
                                    </div>
                                </div>
                            </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-select">{{ __('Role') }}
                                    <span class="mandatory">*</span>
                                </label>
                                @php 
                                    $selected_role_id = "";
                                    $selected_role = $roles->where('name',str_replace("-"," ",$role))->first();
                                    if($selected_role){
                                        $selected_role_id = $selected_role->id;
                                    }
                                @endphp
                                <select name="role" class="form-control" id="example-select">
                                    @foreach ($roles as $get_role)
                                        <option value="{{ $get_role->id }}" @if(old('role') == $get_role->id) selected @endif>{{ $get_role->name  }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group active-checkbox">
                                <input name="active" type="checkbox" id="switch3" checked="" data-switch="success">
                                <label for="switch3" data-on-label="Active" data-off-label="Inactive"></label>
                            </div>
                        </div>
                              <div class="col-lg-12 @if ($errors->has('profile_image')) validation-failed @endif">
                      <div class="form-group">
                          <label class="light-label">{{ __(' Profile Image') }} </label>
                          <div class="input-group">
                              <input type="file" class="dropify" id="userImage"  accept = 'image/jpeg , image/jpg, image/gif, image/png'  data-default-file="" name="profile_image"/>
                             
                          </div>
                          @if ($errors->has('profile_image'))
                              <div class="invalid-feedback" @if ($errors->has('profile_image')) style="display:block" @endif>
                                  {{ $errors->first('profile_image') }} 
                              </div>
                              @endif
                      </div>
                    </div>
                        {{-- <div class="col-lg-6 @if ($errors->has('profile_image')) validation-failed @endif">
                            <div class="form-group">
                                <label>{{ __('Profile Image') }}</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="inputGroupFile04" name="profile_image">
                                        <label class="custom-file-label" for="inputGroupFile04">{{ __('Choose file') }}</label>
                                    </div>
                                    @if ($errors->has('profile_image'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('profile_image') }} 
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div> 
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')
<script>
  var icon = {!! json_encode(url('/storage/admin/propertytypes/')) !!} +'/'+ $(this).attr('data-image');
  var drEvent = $('#userImage').dropify();
  drEvent = drEvent.data('dropify');
  drEvent.resetPreview();
  drEvent.clearElement();
  drEvent.destroy();
  drEvent.init();

  var drEvent1 = $('.dropify').dropify();

  drEvent1.on('dropify.afterClear', function(event, element){
      if($('.edit-popup').hasClass('dropify-selected') ){
          $('input[name=remove_title]').val('removed');
          alert($('input[name=remove_title]').val());
      }
  });
  
  var phone = document.querySelector("#phone");
    var phone_iti = window.intlTelInput(phone,({

        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = "in";//(resp && resp.country) ? resp.country : "in";
                callback(countryCode);
            });
        },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.1.0/js/utils.js",
        separateDialCode: true,
        hiddenInput: "full_number",
        autoPlaceholder: "polite",
        initialCountry: "us",
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            return selectedCountryPlaceholder.replace(/[0-9]/g,"x");
        }, 
    }));
    phone.addEventListener("countrychange", function() {
        $('#country_code').val(phone_iti.getSelectedCountryData().dialCode);
    });
  
</script> 
@endpush
