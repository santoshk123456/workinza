@extends('admin.layouts.app')
@section('title', set_page_titile(__('Change Password')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{ Breadcrumbs::render('admin_users.change_password',$admin) }}
            </div>
            <h4 class="page-title">{{ ucwords($admin->roles()->first()->name)."'s Details"}}</h4>
        </div>
    </div>
</div>
@include('admin.includes.profile.profile_head')
<div class="row">
    @include('admin.includes.profile.tab_head')
    <div class="col-md-9">
        <div class="card">
            <div class="card-body">
                <div class="tab-content" id="v-pills-tabContent">
                    @include('admin.includes.profile.tabs.change_password')
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@push('custom-scripts')
<script>
$("#adminchangepassword-form").validate({
  
  onfocusout: function(element){
  

    if ($(element).attr("name") == "email" ){
      var usr_name = $(".email").val();
      console.log(usr_name);
      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
      });
      $.ajax({
              type: "POST",
              url: "{{route('emailExist')}}",
              data:{ 
                  '_token': "{{ csrf_token() }}",
                  'email':usr_name
                   },
              success: function (data) { 
                  if((data['email_exist']) == 1){
                       $(".email-error").text('Email id already exists');
                  }else{
                    // $(".email-error").text(data['msg']);
                  }
              }
      });
  }
      $(element).valid();
      // $(element).closest("form-group + *").prev().find('input').valid;
     
  },
  onkeyup: function(element){
    $(element).next().text('');
    
     
  },
  rules: {
      password:  {
          required: true,
          minlength: 7,
         
      },
   
      password_confirmation:  {
          required: true,
          minlength: 7,
         
      },
       
      current_password:  {
          required: true,
          minlength: 7,
        
      },
  },

  errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
          $(placement).html(error);
      } 
      else if (element.attr("name") == "current_password" ) {
        $(".current_password-error").html(error); 
      }
      else if (element.attr("name") == "password" ) {
        $(".password-error").html(error); 
      }
      else if (element.attr("name") == "password_confirmation" ) {
        $(".password_confirmation-error").html(error); 
      }
      else {
          error.insertAfter(element);
      }
 },

 messages: {
   
    'current_password': {
              required: "Current Password is required.",
          },
          'password': {
              required: "Password is required.",
          },
          'password_confirmation': {
              required: "Confirm Password is required.",
          },
      }

});

jQuery.validator.addMethod("checkspecial", function(value, element) {
  var pass = new RegExp("^(?=.*[!@#\$%\^&\*])")
 return this.optional(element) || (pass.test(value));
}, "* password must contain one special character");
jQuery.validator.addMethod("checkcapital", function (value, element) {
  var pass = new RegExp("^(?=.*[A-Za-z])")
  return this.optional(element) || (pass.test(value));
}, "Password must contain atleast one character");
jQuery.validator.addMethod("nospace", function (value, element) {
  return value.indexOf(" ") < 0 && value != "";
}, "No space please and don't leave it empty");
jQuery.validator.addMethod("checknumber", function (value, element) {
  var pass = new RegExp("^(?=.*[0-9])")
  return this.optional(element) || (pass.test(value));
}, "Password must contain  one number");
</script>

<script>

$('.pass-icons').click(function(e){
  $('#pass_icon').hasClass('flaticon-eye-1')?hidePassword():showPassword()
})
function hidePassword(){
  $('#pass_icon').removeClass('flaticon-eye-1').addClass('flaticon-visibility')
  $('#password').attr('type','text')
}
function showPassword(){
  $('#pass_icon').removeClass('flaticon-visibility').addClass('flaticon-eye-1')
  $('#password').attr('type','password')
}

</script>


@endpush
