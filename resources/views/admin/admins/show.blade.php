@extends('admin.layouts.app')
@section('title', set_page_titile(__($admin->name.' - Admin Users')))
@push('custom-styles')
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">            
            {{ Breadcrumbs::render('admin_users.show',$admin) }} 
            @if(auth()->user()->id != $admin->id)           
            <h4 class="page-title">{{$admin->name}}{{ __("'s Profile")}}</h4>
            @else 
                <h4 class="page-title">{{ "My Account"}}</h4>
            @endif
        </div>
    </div>
</div>
@include('admin.includes.profile.profile_head')
<div class="row">
    @include('admin.includes.profile.tab_head')
    <div class="col-md-9">
        <!-- Personal-Information -->
        <div class="card">
            <div class="card-body">
                <div class="tab-content" id="v-pills-tabContent">
                    @include('admin.includes.profile.tabs.profile')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
@endpush
