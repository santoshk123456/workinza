@extends('admin.layouts.app')
@section('title', set_page_titile(__('Access Logs')))
@push('custom-styles')
{{ style(mix('vendor/datatable/css/datatable.css')) }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                {{ Breadcrumbs::render('admin_users.access_logs',$admin) }}
            </div>
            <h4 class="page-title">{{ ucwords($admin->roles()->first()->name)."'s Details"}}</h4>
        </div>
    </div>
</div>
@include('admin.includes.profile.profile_head')
<div class="row">
    @include('admin.includes.profile.tab_head')
    <div class="col-md-9">
        <div class="card">
            <div class="card-body">
                <div class="tab-content" id="v-pills-tabContent">
                    @include('admin.includes.profile.tabs.access_logs')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
{!! script(mix('vendor/datatable/js/datatable.js')) !!}
{!! $html->scripts() !!}
@endpush
