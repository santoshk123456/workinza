<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('admin.includes.head')
<body class="authentication-bg">
    <div class="account-pages mt-3">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card mb-0">
                        <!-- Logo -->
                        <div class="card-header logo-auth py-3 text-center bg-primary">
                            <a href="javascript:void();">
                                <span><img src="{{ asset('images/user/1024_fordark_bg.png') }}" alt=""></span>
                            </a>
                        </div>
                        <div class="card-body px-4 py-3">
                            @yield('content')
                        </div>
                    </div>
                    @if(Route::currentRouteName() != "admin.login.form")
                        <div class="row mt-3">
                            <div class="col-12 text-center ">
                                <p class="text-dark">{{ __('Back to') }} <a href="{{ route('admin.login.form') }}" class=" text-dark ml-1"><b>{{ __('Log In') }}</b></a></p>
                            </div> <!-- end col -->
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('admin.includes.auth-footer')
    <!-- App js -->
    @include('admin.includes.scripts')
    @if(Session::has('toastr'))
        <script>
            toastr.{{ Session::get('toastr.type') }}('{{ Session::get("toastr.text") }}','{{ Session::get("toastr.title") }}');
        </script>
        @elseif(isset($toastr))
        <script>
            toastr.{{ $toastr['type'] }}('{{ $toastr["text"] }}','{{ $toastr["title"] }}');
        </script>
    @endif
</body>
</html>
