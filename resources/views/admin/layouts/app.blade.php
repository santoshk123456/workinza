<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('admin.includes.head')
<body class="@if (Cookie::get('enable_full_leftbar') !== false && Cookie::get('enable_full_leftbar') == 'disabled') enlarged @else sidebar-enable @endif" data-keep-enlarged="true">
    <!-- Begin page -->
    <div class="wrapper">
        @include('admin.includes.leftbar')
        <!-- Start Page Content here -->
        <div class="content-page">
            <div class="content">
                @include('admin.includes.navbar')
                <!-- Start Content-->
                <div class="container-fluid">
                    @yield('content')
                </div>
                <!-- container -->
            </div>
            <!-- content -->
            @include('admin.includes.footer')
        </div>
        <!-- End Page content -->
    </div>
    <!-- END wrapper -->
    @include('admin.includes.rightbar')
    <!-- bundle -->
    {{-- for ie changes --}}

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.3.5/bluebird.min.js"></script> 
    @include('admin.includes.scripts') 
    @include('admin.includes.pagination') 
    @if(Session::has('toastr'))
        <script>
            toastr.{{ Session::get('toastr.type') }}('{{ Session::get("toastr.text") }}','{{ Session::get("toastr.title") }}');
        </script>
        @elseif(isset($toastr))
        <script>
            toastr.{{ $toastr['type'] }}('{{ $toastr["text"] }}','{{ $toastr["title"] }}');
        </script>
    @endif
</body>
</html>
