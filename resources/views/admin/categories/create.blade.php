@extends('admin.layouts.app')
@section('title', set_page_titile(__('Create Categories')))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
                {{ Breadcrumbs::render('categories.create') }}
            </div>
            <h4 class="page-title">{{ __('Create Categories') }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{route('admin.categories.store')}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    <div class="row">

                        <div class="col-lg-6 @if ($errors->has('sub_category_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Category') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" name="category_name" class="form-control" minlength="2"
                                    maxlength="50" required placeholder="Category" autocomplete="off">
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('category_name'))
                                    {{ $errors->first('category_name') }}
                                    @else
                                    {{ __('Category name cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>

                        <div class="col-lg-12 @if ($errors->has('icon')) validation-failed @endif">
                            <div class="form-group">
                                <label>{{ __('Icon') }}</label>
                                <div class="input-group">
                                    <input type="file" class="dropify" id="drop" accept = 'image/jpeg , image/jpg, image/gif, image/png'  data-default-file="" name="icon"/>
                                    @if ($errors->has('icon'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('icon') }} 
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>

            

                        <div class="col-lg-6">
                        <div class="form-group">
                                <label>{{ __('Home Active') }}</label>
                            <div class="form-group active-checkbox">
                                <input name="home_active" type="checkbox" id="switch3" data-switch="success">
                                <label for="switch3" class="mt-0" data-on-label="Active" data-off-label="Inactive"></label>
                            </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i
                                class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i
                                class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

<script>
    $('.js-example-basic-multiple').select2({
        placeholder: "Select Industry",
        allowClear: true
    });

</script>

@endpush
