@extends('admin.layouts.app')
@section('title', set_page_titile(__($category->name.' - Categories')))
@push('custom-styles')
{{ style('css/admin/developer.css') }}
@endpush
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right py-1">
            {{ Breadcrumbs::render('categories.edit',$category) }}
            </div>
            <h4 class="page-title">{{ __($category->name) }}</h4>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="{{route('admin.categories.update',$category->uuid)}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="row">

                        <div class="col-lg-6 @if ($errors->has('category_name')) validation-failed @endif">
                            <div class="form-group">
                                <label for="validationCustom02">{{ __('Category') }}
                                    <span class="mandatory">*</span>
                                </label>
                                <input type="text" value="{{ old('category_name',$category->name)}}" name="category_name" class="form-control" minlength="2"
                                    maxlength="50" required placeholder="Sub Category" autocomplete="off">
                                <span class="invalid-feedback error-txt">
                                    @if ($errors->has('category_name'))
                                    {{ $errors->first('category_name') }}
                                    @else
                                    {{ __('Sub category cannot be empty.') }}
                                    @endif
                                </span>
                            </div>
                        </div>


                        <div class="col-lg-12 @if ($errors->has('icon')) validation-failed @endif edit-popup">
                        <div class="form-group">
                                <label>{{ __('Profile Image ') }} </label>
                                <div class="input-group file">
                                @php 
                                $url = asset('storage/admin/categories'.$category->icon);
                                @endphp
                              
                                    @if(!empty($category->icon))
                                    <input type="file" id="drop1" class="dropify"  data-default-file="{{ $url }}" name="icon" />
                                    <input type="hidden" name="remove_title">
                                    @else
                                    <input type="file" id="drop1" class="dropify" accept = 'image/jpeg , image/jpg, image/gif, image/png'  name="icon" />
                                    @endif
                                    @if ($errors->has('icon'))
                                       <div class="invalid-feedback" @if ($errors->has('icon')) style="display:block" @endif>
                                           {{ $errors->first('icon') }} 
                                       </div>
                                     @endif
                                </div>
                            </div>
                        </div> 
                      


                        <div class="col-lg-6">
                        <div class="form-group">
                                <label>{{ __('Home Active') }}</label>
                            <div class="form-group active-checkbox">
                                <input  name="home_active" type="checkbox" id="switch3" @if($category->home_active == 1) checked @endif data-switch="success">
                                <label for="switch3" class="mt-0" data-on-label="Active" data-off-label="Inactive"></label>
                            </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                        <div class="form-group">
                                <label>{{ __('Status') }}</label>
                            <div class="form-group active-checkbox">
                                <input  name="active" type="checkbox" id="switch3" @if($category->active == 1) checked @endif data-switch="success">
                                <label for="switch3" class="mt-0" data-on-label="Active" data-off-label="Inactive"></label>
                            </div>
                            </div>
                        </div>
                        

                    </div>
                    <div class="col-12 text-right px-0">
                        <button class="btn btn-secondary mt-2" type="reset"><i
                                class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                        <button class="btn btn-primary mt-2" type="submit"><i
                                class="mdi mdi-content-save-edit mr-1"></i>{{ __('Submit') }}</button>
                    </div>
                </form>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
@endsection
@push('custom-scripts')

<script>
$('.js-example-basic-multiple').select2({
    placeholder: "Select Category",
    allowClear: true
});

$('.industries-multiple').select2({
    placeholder: "Select Industry",
    allowClear: true
});
</script>
<script>
    $("input[name='heading']").on('input', function() {
        $("input[name='slug']").val(convertToSlug($(this).val()));
    })

    var drEvent1 = $('.dropify').dropify();
    drEvent1.on('dropify.afterClear', function(event, element){
        $('input[name=remove_title]').val('removed');
    });  


    var icon = {!! json_encode(url('storage/admin/categories/')) !!} +'/'+ $(this).attr('data-image');
  var drEvent = $('#icon').dropify();
  drEvent = drEvent.data('dropify');
  drEvent.resetPreview();
  drEvent.clearElement();
  drEvent.destroy();
  drEvent.init();

  var drEvent1 = $('.dropify').dropify();

  drEvent1.on('dropify.afterClear', function(event, element){
      if($('.edit-popup').hasClass('dropify-selected') ){
          $('input[name=remove_title]').val('removed');
          alert($('input[name=remove_title]').val());
      }
  });
</script>
@endpush
