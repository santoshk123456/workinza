@extends('admin.layouts.auth')
@section('title', set_page_titile(__('Sign In')))
@section('content')
<div class="text-center w-75 m-auto">
    <h4 class="text-dark-50 text-center mt-0 font-weight-bold">{{ __('Sign In') }}</h4>
    <p class="text-muted mb-4">{{ __('Enter your email address and password to access admin panel.') }}</p>
</div>

<!-- form -->
<form method="POST" action="{{ route('admin.login') }}">
    @csrf
    <div class="form-group">
        <label for="emailaddress">{{ __('Email Address') }}</label>
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
            name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
        @if ($errors->has('email'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group">
        <label for="password">{{ __('Password') }}</label>
        <input id="password" type="password"
            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
        @if ($errors->has('password'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group mb-2">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" name="remember" id="checkbox-signin" class="custom-control-input"
                {{ old('remember') ? 'checked' : '' }}>
            <label class="custom-control-label" for="checkbox-signin">{{ __('Remember Me') }}</label>
            <a href="{{ route('admin.password.request') }}"
            class="text-muted float-right frgt-link" style="text-decoration: underline;"><small>{{ __('Forgot Your Password?') }}</small></a>
        </div>
    </div>
    <div class="form-group mb-0 text-center">
        <button class="btn btn-primary" type="submit">{{ __('Sign In') }} </button>
    </div>
    <!-- social-->
    <!-- <div class="text-center mt-2">
        <p class="text-muted font-16">{{ __('Sign in with') }}</p>
        <ul class="social-list list-inline mt-2 mb-0">
            <li class="list-inline-item">

                <a href="{{ url('auth/facebook') }}" class="social-list-item border-primary text-primary" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i
                        class="mdi mdi-facebook"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="{{ url('auth/google') }}"class="social-list-item border-danger text-danger" data-toggle="tooltip" data-placement="bottom" title="Google"><i
                        class="mdi mdi-google"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="{{ url('auth/twitter') }}" class="social-list-item border-info text-info" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i
                        class="mdi mdi-twitter"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="{{ url('auth/github') }}" class="social-list-item border-secondary text-secondary" data-toggle="tooltip" data-placement="bottom" title="Github"><i
                        class="mdi mdi-github-circle"></i></a>
            </li>
        </ul>
    </div> -->
</form>
<!-- end form-->
@endsection

