@extends('admin.layouts.auth')
@section('content')
<div class="text-center w-75 m-auto">
    <h4 class="text-dark-50 text-center mt-0 font-weight-bold">{{ __('Reset Password') }}</h4>
    <p class="text-muted mb-4">{{ __("Please enter your new password and confirm.") }}</p>
</div>
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<!-- form -->
<form method="POST" action="{{ route('admin.password.request') }}">
    {{ csrf_field() }}
    <input type="hidden" name="token" value="{{ $token }}">
    <div class="form-group mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email">{{ __('Email address') }}</label>
        <input id="email" type="email" class="form-control" name="email" value="{{ $email }}" required
            autofocus>
        @if ($errors->has('email'))
        <span class="help-block">
            <strong style="color:red !important;">{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password">{{ __('Password') }}</label>
        <input id="password" type="password" class="form-control" name="password" required>
        @if ($errors->has('password'))
        <span class="help-block">
            <strong style="color:red !important;">{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group mb-3{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
        <label for="password-confirm">{{ __('Confirm Password') }}</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        @if ($errors->has('password_confirmation'))
        <span class="help-block">
            <strong style="color:red !important;">{{ $errors->first('password_confirmation') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group mb-0 text-center">
        <button class="btn btn-primary" type="submit">{{ __('Reset Password') }}</button>
    </div>
</form>
<!-- end form-->
@endsection
