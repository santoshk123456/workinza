@extends('admin.layouts.auth')
@section('content')
<div class="align-items-center d-flex h-100">
    <div class="card-body">
        <!-- Logo -->
        <div class="auth-brand text-center text-lg-left">
            <a href="index.html">
                <span><img src="{{ asset('images/admin/starter/logo-light.png') }}" alt="" height="50"></span>
            </a>
        </div>
        <!-- email send icon with text-->
        <div class="text-center m-auto">
            <img src="{{ asset('images/admin/starter/mail_sent.svg') }}" alt="mail sent image" height="64">
            <h4 class="text-dark-50 text-center mt-4 font-weight-bold">{{ __('Please check your email') }}</h4>
            <p class="text-muted mb-4">
                {{ __('Please check for an email from company and click on the included link to
                reset your password') }}.
            </p>
        </div>
        <!-- form -->
        <form action="{{ route('admin-login-screen') }}">
            <div class="form-group mb-0 text-center">
                <button class="btn btn-primary btn-block" type="submit"><i class="mdi mdi-home mr-1"></i> {{ __('Back to Home') }}
                </button>
            </div>
        </form>
        <!-- end form-->
        @include('admin.includes.auth-footer')
    </div> <!-- end .card-body -->
</div>
@endsection
