@extends('admin.layouts.auth')
@section('content')
<div class="text-center w-75 m-auto">
    <h4 class="text-dark-50 text-center mt-0 font-weight-bold">{{ __('Reset Password') }}</h4>
    <p class="text-muted mb-4">{{ __("Enter your email address and we'll send you an email with instructions to reset your password.") }}</p>
</div>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<!-- form -->
<form method="POST" action="{{ route('admin.password.email') }}">
    {{ csrf_field() }}
    <div class="form-group mb-3">
        <label for="email">{{ __('Email address') }}</label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
            placeholder="Enter your email">
        @if ($errors->has('email'))
        <span class="help-block" style="color: red;">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif

    </div>
    <div class="form-group mb-0 text-center">
        <button class="btn btn-primary" type="submit">{{ __('Reset Password') }}</button>
    </div>
</form>
@endsection
