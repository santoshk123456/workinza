@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Confirm Membership Plan')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
<style>
  .error {
  color: #F00;
  background-color: #FFF;
}

.questionborder:last-of-type{ border-bottom:0 !important; }

</style>
@endpush

@section('content')
<!--start: contact information page -->
   
@php
    $amount = '';
    if(empty($membershipPlan->monthly_amount))
    $amount = $membershipPlan->yearly_amount;
    else
    $amount = $membershipPlan->monthly_amount;
    @endphp

<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')
        <div class="col-lg-9">
            <div class="right-aside">
            <div class="card-top mb-4">
              <h4>Review Your Membership Plan</h4>
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eu metus quam. </p>  
            </div>     
            <div class="card-content">
              <div class="membership-details">
                <!-- <h4>Membership Details</h4> -->
                <ul>
                  <li><p>Membership plan selected:</p><span><strong>{{ ucwords($membershipPlan->name) }}</strong></span></li>
                  <li><p>Amount: </p><span><strong>{{config('data.currency_symbol')}}@if(!empty($membershipPlan->monthly_amount)){{ ($membershipPlan->monthly_amount) }}/month @else {{$membershipPlan->yearly_amount }}/year @endif</strong></span></li>
                  <li><p>Number of projects to post: </p><span><strong>@if(!empty($membershipPlan->monthly_amount)){{ ($membershipPlan->number_of_projects) }}/month @else {{$membershipPlan->number_of_projects }}/year @endif</strong></span></li>
                  @if(auth()->user()->user_type == 0)
                    <li><p>Number of proposals user can receive:</p><span><strong>{{ ucwords($membershipPlan->number_of_proposals) }}</strong></span></li>
                  @endif
                  <li><p>Chat:</p><span><strong>@if(!empty($membershipPlan->chat_feature)){{ __('Yes') }} @else {{__('No') }}@endif</strong></span></li>
                  <li><p>Setup Charge:</p><span><strong>@if(!empty($membershipPlan->setup_charge)){{ formatAmount($membershipPlan->setup_charge) }} @else {{__('NA') }}@endif</strong></span></li>
                  <li class="total-amount"><p>Total:</p><span><strong> {{config('data.currency_symbol')}}@if(!empty($membershipPlan->monthly_amount)){{ ($membershipPlan->monthly_amount) }}/month @else {{$membershipPlan->yearly_amount }}/year @endif</strong></span></li>            
                </ul>                
                <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lacinia augue a bibendum rutrum. Donec ullamcorper fermentum tortor eu tincidunt. Vestibulum mattis diam non lacus pulvinar, ut laoreet nunc rhoncus. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
                {{-- <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="renew-plan" checked>
                  <label class="custom-control-label" for="renew-plan">Auto renew subscription plan</label>
                </div>  --}}
                <div class="btn-cols text-lg-right text-center">
                  {{-- <button class="btn cancel-btn mr-2 mb-3 mb-md-0" type="submit">Pay with wallet balence <span>$1000</span></button> --}}
                  <form method="POST" >
                    @csrf
                    <input type="hidden" name="plan_id" value="{{$membershipPlan->plan_id}}">
                    <input type="hidden" name="membership_plan_id" value="{{$membershipPlan->id}}">
                   
                    {{-- <script
                    
                      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                      data-key="{{$payment_keys->authentication_key}}"
                      data-amount="{{$amount*100 }}"
                      data-name="Membership Payment"
                      data-description="You need to subscribe"
                      data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                      data-locale="auto"
                      data-zip-code="false">
                    </script>
                    <script>
                      // Hide default stripe button, be careful there if you
                      // have more than 1 button of that class
                      document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                  </script> --}}

                   
                  
                    </form>
                    <button class="btn submit-btn" id="checkout-button">Pay with card</button>
                </div>   
              </div>
            </div>
        </div>
    </div>
    </div>
  </div>
</main>



    <div class="mt-2">
        

    </div>


@endsection
@push('custom-scripts')
<script src="https://js.stripe.com/v3/"></script>

	 <script>
	var key = '{{$payment_keys->authentication_key}}';
	var stripe = window.Stripe(key);
	
	
	var checkoutButton = document.getElementById('checkout-button');

	checkoutButton.addEventListener('click', function() {
	stripe.redirectToCheckout({
		
		sessionId: '{{$checkout_session->id}}'
	}).then(function(result) {
         
          if (result.error) {
            alert(result.error.message);
          }
        })
        .catch(function(error) {
          console.error('Error:', error);
        });
	});
</script> 
@endpush