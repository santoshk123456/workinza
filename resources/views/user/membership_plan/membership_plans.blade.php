
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Membership Plans')))

@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
<style>
  .error {
  color: #F00;
  background-color: #FFF;
}
</style>
@endpush

@push('custom-styles')
<style>
#container {
  display: flex;                  /* establish flex container */
  flex-direction: row;            /* default value; can be omitted */
  flex-wrap: nowrap;              /* default value; can be omitted */
  justify-content: space-between; /* switched from default (flex-start, see below) */
}



</style>
@endpush
@section('content')

       

        <!-- Header:end -->
<!--start: contact information page -->

<main class="main-wrapper">
    <div class="container inner-pg-layout">
      <div class="row mx-0">
        @include('user.layouts.includes.profile_tab')
          <div class="col-lg-9">
              <div class="right-aside">
              <div class="card-top mb-4">
                <h4>@if(auth()->user()->step >= 5) Upgrade @endif {{__('Membership Plans')}}</h4>
                <p class="mb-0">{{__('Review Your Membership Plan')}}</p>  
                @if(auth()->user()->step >= 5)
                <button class="btn submit-btn float-right" id="otp-verify-btn" style="margin-top: -45px;" type="button" onclick="location.href='{{ route('user.redirectBack') }}'">Back</button>
                @endif
              </div>        
                @php
                    $plan_amount = 0;
                    $active_status = 0;
                    $member_plan_type = 1;
                    $member_plan_id = 0;
                    $subscription_status = $subscription->where('user_id',auth()->user()->id)->where('active',1)->first();
                    if($subscription_status){
                      $active_status = 1;
                      $member_plan_type = $subscription_status->membership->plan_type;
                      $member_plan_id = $subscription_status->membership_plan_id;
                      $plan_amount = $subscription_status->amount;
                    }
                @endphp   
              <div class="card-content">   
                <div class="plans-billable d-none d-sm-flex">
                <div class="check-cols billing-cols">
                      <div class="custom-control custom-radio radio-cols mb-2">
                        <input type="hidden" value="{{$member_plan_type}}" id="member_plan_type">
                        <input type="radio" id="month_billing" name="plan_type" value="0" class="custom-control-input" @if($member_plan_type == 1) checked="checked" @endif>
                        <label class="custom-control-label" for="month_billing"> Monthly Billing </label>
                    </div>
                    <div class="custom-control custom-radio radio-cols">
                        <input type="radio" id="year_billing" name="plan_type" value="1" class="custom-control-input" @if($member_plan_type == 2) checked="checked" @endif>
                        <label class="custom-control-label" for="year_billing">Annual Billing</label>                     
                    </div>     
                    </div>         
              
                @foreach($monthly_plans as $key=>$plan)
                    <div class="price-cols text-center recommended-col monthly-billing @if($member_plan_type == 2)  d-none @endif">
                      <h4>{{str_limit(ucwords($plan->name), $limit = 15, $end = '...')}}</h4>  
                      @if(auth()->user()->step >= 5 && $active_status==1)
                        @if($member_plan_id == $plan->id)
                          <span class="badge badge-success">Current</span>
                        @else
                        <button class="btn submit-btn rounded-0" onclick="location.href='{{ route('membership_plan.planReview',$plan->uuid) }}'" type="button">@if($plan_amount < $plan->monthly_amount){{ __('Upgrade') }} @else {{ __('Select') }} @endif </button> 
                        @endif
                      @else
                      <button class="btn submit-btn rounded-0" onclick="location.href='{{ route('membership_plan.planReview',$plan->uuid) }}'" type="button">{{ __('Select') }}</button> 
                      @endif       
                      <p class="price-tag"><span> {{config('data.currency_symbol')}}{{ucwords($plan->monthly_amount)}}</span>/month</p>   
                    </div>
                @endforeach
                 

              
                    @foreach($yearly_plans as $key=>$plan)
                    <div class="price-cols text-center recommended-col yearly-billing  @if($member_plan_type == 1)  d-none @endif">
                     
                      <h4>{{str_limit(ucwords($plan->name), $limit = 15, $end = '...')}}</h4>  
                      @if(auth()->user()->step >= 5 && $active_status==1)
                      @if($member_plan_id == $plan->id)
                        <span class="badge badge-success">Current</span>
                      @else
                      <button class="btn submit-btn rounded-0" onclick="location.href='{{ route('membership_plan.planReview',$plan->uuid) }}'" type="button">
                      @if($plan_amount < $plan->yearly_amount){{ __('Upgrade') }} @else {{ __('Select') }} @endif </button>
                      @endif
                      @else
                      <button class="btn submit-btn rounded-0" onclick="location.href='{{ route('membership_plan.planReview',$plan->uuid) }}'" type="button">{{ __('Select') }}</button>        
                      @endif 
                      <p class="price-tag"><span> {{config('data.currency_symbol')}}{{ucwords($plan->yearly_amount)}}</span>/Year</p>   
                    </div>
                    @endforeach
<!-- 
                    <div class="price-cols text-center">
                      <h4>Silver</h4>  
                      <button class="btn submit-btn rounded-0">Select Silver</button>        
                      <p class="price-tag"><span> $49.99</span>/month</p>   
                    </div>      -->

                    <!-- <div class="price-cols text-center recommended-col">
                      <h4>Gold</h4>  
                      <button class="btn submit-btn rounded-0" type="submit">Select Gold</button>        
                      <p class="price-tag"><span>$75.99</span>/month</p>    
                      <span class="notify-col">Recommended</span>          
                    </div>
              
                    <div class="price-cols text-center last-col">
                      <h4>Diamond</h4>  
                      <button class="btn submit-btn rounded-0" type="submit">Select Diamond</button>        
                      <p class="price-tag"><span> $110.99</span>/month</p>           
                    </div>  -->
                    
                    



                </div>   
              <div class="month-details @if($member_plan_type == 2)  d-none @endif">
                <table class="table table-bordered information_table @if($member_plan_type == 2)  d-none @endif d-sm-table ">
                  <thead>
                    <tr>
                      <th scope="col" colspan="4">Compare the features</th>                    
                    </tr>
                  </thead>

                  @php
                      $recNew = [];
                      foreach($monthly_plans as $row) {
                        $recNew['number_of_projects'][] = $row->number_of_projects;
                        $recNew['number_of_proposals'][] = $row->number_of_proposals;
                        $recNew['chat_feature'][] = $row->chat_feature;  
                        $recNew['setup_charge'][] = $row->setup_charge;
                        $recNew['service_percentage'][] = $row->service_percentage;
                        $recNew['number_of_products'][] = $row->number_of_products;
                      }
                  @endphp



                  <tbody>
                  @foreach($recNew as $key1=>$pl)
                                    <tr>
                                         @if(($key1 == 'number_of_projects'))
                                          <td> {{__('Maxmum number of projects to post')}} </td>
                                          @elseif($key1 == 'number_of_proposals')
                                            @if($user->user_type == 0)
                                            <td> {{__('Maximum no of proposals a project can receive')}} </td>
                                            @endif
                                          @elseif($key1 == 'chat_feature')
                                          <td> {{__('Chats')}} </td>
                                          @elseif($key1 == 'setup_charge')
                                          <td> {{__('One time setup charge')}} </td>
                                          @elseif($key1 == 'service_percentage')
                                          <td> {{__('Service charge')}} </td>
                                          @elseif($key1 == 'number_of_products')
                                          <td> {{__('Product to post')}} </td>
                                          @endif

                                      @foreach($pl as $key=>$pd)
                                          @if(($key1 == 'number_of_projects'))
                                            <td> @if(!empty($pd)) {{ $pd }} {{__('/ Project posts')}} @else {{'NA'}} @endif </td>
                                          @elseif($key1 == 'number_of_proposals')
                                            @if($user->user_type == 0)
                                              <td> @if(!empty($pd)) {{ $pd }} {{__('/ Project posts')}} @else {{'NA'}} @endif </td>
                                            @endif
                                          @elseif($key1 == 'chat_feature')
                                          <td> @if(!empty($pd)) {{ __('YES') }}@else {{__('No')}} @endif </td>
                                          @elseif($key1 == 'setup_charge')
                                          <td> @if(!empty($pd)) {{ formatAmount($pd) }}@else {{__('NA')}} @endif </td>
                                          @elseif($key1 == 'service_percentage')
                                          <td> @if(!empty($pd)) {{ $pd }}% @else {{__('NA')}} @endif </td>
                                          @elseif($key1 == 'number_of_products')
                                          <td> @if(!empty($pd)) {{ $pd }} @else {{__('NA')}} @endif </td>
                                          @endif
                                      @endforeach
                                    </tr>
                                    @endforeach
                  </tbody>
                </table>
              </div>
 

              <div class="year-details @if($member_plan_type == 1)  d-none @endif">
                 
                <table class="table table-bordered information_table @if($member_plan_type == 1)  d-none @endif d-sm-table ">
                  <thead>
                    <tr>
                      <th scope="col" colspan="4">Compare the features</th>                    
                    </tr>
                  </thead>

                  @php
                      $recNew = [];
                      foreach($yearly_plans as $row) {
                        $recNew['number_of_projects'][] = $row->number_of_projects;
                        $recNew['number_of_proposals'][] = $row->number_of_proposals;
                        $recNew['chat_feature'][] = $row->chat_feature;  
                        $recNew['setup_charge'][] = $row->setup_charge;
                        $recNew['service_percentage'][] = $row->service_percentage;
                        $recNew['number_of_products'][] = $row->number_of_products;
                      }
                      @endphp



                  <tbody>
                  @foreach($recNew as $key1=>$pl)
                                    <tr>
                                         @if(($key1 == 'number_of_projects'))
                                          <td> {{__('Maxmum number of projects to post')}} </td>
                                          @elseif($key1 == 'number_of_proposals')
                                            @if($user->user_type == 0)
                                              <td> {{__('Maximum no of proposals a project can receive')}} </td>
                                            @endif
                                          @elseif($key1 == 'chat_feature')
                                          <td> {{__('Chats')}} </td>
                                          @elseif($key1 == 'setup_charge')
                                          <td> {{__('One time setup charge')}} </td>
                                          @elseif($key1 == 'service_percentage')
                                          <td> {{__('Service charge')}} </td>
                                          @elseif($key1 == 'number_of_products')
                                          <td> {{__('Product to post')}} </td>
                                          @endif

                                      @foreach($pl as $key=>$pd)
                                          @if(($key1 == 'number_of_projects'))
                                              <td> @if(!empty($pd)) {{ $pd }} {{__('/ Project posts')}} @else {{'NA'}} @endif </td>
                                          @elseif($key1 == 'number_of_proposals')
                                            @if($user->user_type == 0)
                                              <td> @if(!empty($pd)) {{ $pd }} {{__('/ Project posts')}} @else {{'NA'}} @endif </td>
                                            @endif
                                          @elseif($key1 == 'chat_feature')
                                          <td> @if(!empty($pd)) {{ __('YES') }}@else {{__('No')}} @endif </td>
                                          @elseif($key1 == 'setup_charge')
                                          <td> @if(!empty($pd)) {{config('data.currency_symbol')}}{{ $pd }}@else {{__('NA')}} @endif </td>
                                          @elseif($key1 == 'service_percentage')
                                          <td> @if(!empty($pd)) {{ $pd }}% @else {{__('NA')}} @endif </td>
                                          @elseif($key1 == 'number_of_products')
                                          <td> @if(!empty($pd)) {{ $pd }} @else {{__('NA')}} @endif </td>
                                          @endif
                                      @endforeach
                                    </tr>
                                    @endforeach
                  </tbody>
                </table>
              </div>
                 <div class="plans-billable-mob p-2 d-sm-none">
                    <div class="price-cols text-center mb-3">
                      <h4>Silver</h4>  
                      <button class="btn submit-btn rounded-0" type="submit">"Select" Silver</button>     
                      <p class="price-tag"><span> $49.99</span>/month</p> 
                      <div class="check-cols billing-cols">
                        <div class="custom-control custom-radio radio-cols mb-2">
                          <input type="radio" id="silver-month-bill" name="silver-month-bill" class="custom-control-input" checked>
                          <label class="custom-control-label" for="silver-month-bill"> Monthly Billing </label>
                        </div>
                        <div class="custom-control custom-radio radio-cols">
                            <input type="radio" id="silver-annual-bill" name="silver-month-bill" class="custom-control-input">
                            <label class="custom-control-label" for="silver-annual-bill">Annual Billing</label>                     
                        </div>  
                      </div> 
                      <ul class="features-list">
                        <li>Maxium number of projects is 15 / Project Posts.</li>
                        <li>Maxium number of proposals a project can receive is 24 / Project Posts.</li>                       
                        <li>One time setup charge	is $45.</li>
                        <li>Service charge is 3%.</li>
                        <li>Product to post	is 35.</li>
                      </ul>              
                    </div>                  
                    <div class="price-cols text-center recommended-col mb-3">
                      <h4>Gold</h4>  
                      <p class="price-tag"><span>$75.99</span>/month</p>  
                      <button class="btn submit-btn rounded-0" type="submit">"Select" Gold</button> 
                      <div class="text-center">
                        <span class="notify-col mb-0">Recommended</span>
                     </div> 
                      <div class="check-cols billing-cols">
                        <div class="custom-control custom-radio radio-cols mb-2">
                          <input type="radio" id="gold-month-bill" name="gold-month-bill" class="custom-control-input" checked>
                          <label class="custom-control-label" for="gold-month-bill"> Monthly Billing </label>
                        </div>
                        <div class="custom-control custom-radio radio-cols">
                            <input type="radio" id="gold-annual-bill" name="gold-month-bill" class="custom-control-input">
                            <label class="custom-control-label" for="gold-annual-bill">Annual Billing</label>                     
                        </div>  
                      </div> 
                      <ul class="features-list">
                        <li>Maxium number of projects is @55 / Project Posts.</li>
                        <li>Maxium number of proposals a project can receive is @75 / Project Posts.</li> 
                        <li>Chats.</li>                      
                        <li>One time setup charge	is $30.</li>
                        <li>Service charge is 2%.</li>
                        <li>Product to post	is 75.</li>
                      </ul>              
                    </div>                           
                    <div class="price-cols text-center">
                      <h4>Diamond</h4>  
                      <button class="btn submit-btn rounded-0" type="submit">"Select" Diamond</button>   
                      <p class="price-tag"><span> $110.99</span>/month</p> 
                      <div class="check-cols billing-cols">
                        <div class="custom-control custom-radio radio-cols mb-2">
                          <input type="radio" id="diamond-month-bill" name="diamond-month-bill" class="custom-control-input" checked>
                          <label class="custom-control-label" for="diamond-month-bill"> Monthly Billing </label>
                        </div>
                        <div class="custom-control custom-radio radio-cols">
                            <input type="radio" id="diamond-annual-bill" name="diamond-month-bill" class="custom-control-input">
                            <label class="custom-control-label" for="diamond-annual-bill">Annual Billing</label>                     
                        </div>  
                      </div>                            
                      <ul class="features-list">
                        <li>Unlimited number of projects.</li>
                        <li>Maxium number of proposals a project can receive is 175.</li>
                        <li>Chats.</li>                          
                        <li>One time setup charge	is $15.</li>
                        <li>Service charge is 1%.</li>
                        <li>Product to post	is 105.</li>
                      </ul>                           
                    </div>              
                 </div>
              </div>
          </div>
      </div>
      </div>
    </div>
  </main>
  
  <!--end: contact information page -->
  
@endsection
@push('custom-scripts')
<script>
$(document).ready(function(){
  var member_plan_type = $("#member_plan_type").val();
  if(member_plan_type == 1){
    $('input:radio[name="plan_type"]').filter('[value="0"]').attr('checked', true);
  }
});
</script>

<script>
$('#year_billing').click(function () {
        $('.monthly-billing').addClass('d-none');
        $('.month-details').addClass('d-none');

        $('.yearly-billing').removeClass('d-none');
        $('.year-details').removeClass('d-none');
});

$('#month_billing').click(function () {
        $('.yearly-billing').addClass('d-none');
        $('.year-details').addClass('d-none');

        $('.monthly-billing').removeClass('d-none');
        $('.month-details').removeClass('d-none');
});
</script>
@endpush
