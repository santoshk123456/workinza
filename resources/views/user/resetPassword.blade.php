@extends('user.layouts.user_one')
@section('title', set_page_titile(__('Create Your Account')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
<style>
    #confirm_password-error{
        font-size: 13px;
    }
</style>
@endpush
@section('content')


<div class="content-area">
    <div class="container">
        <div class="login-signup-cols accounts-cols">
			<div class="login-col login-register-page">
				<!-- Welcome Text -->
				<div class="welcome-text">
					<h2>Reset your password</h2>
				</div>
				<!-- Form -->

            <form method="post" id="forgot-password-form" action="{{route('resetVerification')}}">
            @csrf

                <div class="form-group">  

                
						<input type="hidden" name="email" value="{{ $email }}">
						<div class="input-field">
                            <input type="text" class="form-control" id="forgot-otp"  name="otp" placeholder="Enter Received Code" />
                            <span class="error-txt otp-error"></span>
                            <!-- <label for="forgot-otp">Enter Received Code</label> -->
                            @if (isset($otp))
                            <span class="error-txt">
                                {{ $otp }}
                            </span>
                            @endif
						</div>
					</div>
					<div class="form-group">  
						<div class="input-field ">
                            <input type="password" name="password" id="password" minlength="7"  class="form-control " placeholder="New Password" 
                            data-rule-nospace="true"   data-rule-checkcapital="true"   data-rule-checknumber="true"  data-rule-checkspecial="true" 
                            aria-label="New Password" aria-describedby="basic-addon5">
                            <span class="error-txt password-error"></span>
							<!-- <label for="new-password">New Password</label> -->
						</div>
					</div>
					<div class="form-group">  
						<div class="input-field ">
              <input type="password" class="form-control" minlength="7"  class="form-control " placeholder="Confirm Password" 
              data-rule-nospace="true"   data-rule-checkcapital="true"   data-rule-checknumber="true"  data-rule-checkspecial="true" 
              aria-label="Confirm Password" aria-describedby="basic-addon5" id="confirm_password" name="password_confirmation"   placeholder="Enter Confirm Password"/>
               <span class="error-txt confirm-password-error"></span>
							<!-- <label for="confirm_password">Confirm Password</label> -->
						</div>
					</div>
						
              <button type="submit" class="btn log-reg-btn w-100" >Update and Login</button>
              
							<!-- <p class="buttontext"><span class="signup code_text">Not received your verificarion code</span>
								| <a href="#" class="forgot" id="resend-otp"> Resend</a></p> -->
                   
                           <!-- ............................................. -->
         
              
                            <!-- <input type="password" name="password" id="password" minlength="7" maxlength="12" class="form-control border-left-0 border-right-0" placeholder="Create A Password" 
                            data-rule-nospace="true"   data-rule-checkcapital="true"   data-rule-checknumber="true"  data-rule-checkspecial="true" 
                            aria-label="Create A Password" aria-describedby="basic-addon5">
                            <span class="error-txt password-error"></span>
                       
                          </div>
                    
              
                <div class="btn-cols mb-2">                
                    <button class="btn log-reg-btn" type="submit">Create My Account</button>
                </div>  -->
            </form>
            </div>
		</div>
	</div>
</div>
<!--end: create account page -->

@endsection
@push('custom-scripts')
<script>
    $(document).ready(function () {
        $(".txtOnly").keypress(function (event) {
            var key = event.keyCode;
            return ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || key == 8 || key == 32);
        });
    });

</script>

<script>


 $("#forgot-password-form").validate({
  
    onfocusout: function(element){
    

      if ($(element).attr("name") == "email" ){
        var usr_name = $(".email").val();
        console.log(usr_name);
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('emailExist')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'email':usr_name
                     },
                success: function (data) { 
                    if((data['email_exist']) == 1){
                         $(".email-error").text('Email id already exists');
                    }else{
                      // $(".email-error").text(data['msg']);
                    }
                }
        });
    }
        $(element).valid();
        // $(element).closest("form-group + *").prev().find('input').valid;
   	
    },
    onkeyup: function(element){
      $(element).next().text('');
      
   	
    },
    rules: {
        // password:  {
        //     required: true,
        //     minlength: 7,
        //     maxlength: 12,
        // },
        otp:  {
            required: true,
            number: true
        },
		// password_confirmation:  {
        //     required: true,
        //     minlength: 7,
        //     maxlength: 12,
        // },
        password_confirmation : {
                    minlength : 7,
                    equalTo : "#password"
                }
       
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } else if (element.attr("name") == "otp" ) {
          $(".otp-error").html(error);
        } 
        else if (element.attr("name") == "full_name" ) {
          $(".full-name-error").html(error);
        }
        else if (element.attr("name") == "email" ) {
          $(".email-error").html(error); 
        }
        else if (element.attr("name") == "password" ) {
          $(".password-error").html(error); 
        }
        else if (element.attr("name") == "company_name" ) {
          $(".company-name-error").html(error); 
        }
        else if (element.attr("name") == "agree_terms" ) {
          $(".agree-terms-error").html(error); 
        }else if(element.attr("name") == "password_confirmation"){
            $(".confirm-password-error").html(error); 
        }
        else {
            error.insertAfter(element);
        }
   },

   messages: {
           'agree_terms': {
                required: "You must agree the Terms & Conditions and Privacy Policy",
            },
            'full_name': {
                required: "Full name is required.",
            },
            'email': {
                required: "Email is required.",
            },
            'password': {
                required: "Password is required.",
            },
            'company_name': {
                required: "Company name is required.",
            },
            'password_confirmation':{
                equalTo : "Password mismatch"
            }
        }

});
jQuery.validator.addMethod("fullname", function(value, element) {
  if (/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/.test(value)) {
    return true;
  } else {
    return false;
  };
}, 'Please enter your full name. ( Eg: Thomas Edison)');
 jQuery.validator.addMethod("checkspecial", function(value, element) {
    var pass = new RegExp("^(?=.*[!@#\$%\^&\*])")
   return this.optional(element) || (pass.test(value));
}, "* password must contain one special character");
jQuery.validator.addMethod("checkcapital", function (value, element) {
    var pass = new RegExp("^(?=.*[A-Za-z])")
    return this.optional(element) || (pass.test(value));
}, "Password must contain atleast one character");
jQuery.validator.addMethod("nospace", function (value, element) {
    return value.indexOf(" ") < 0 && value != "";
}, "No space please and don't leave it empty");
jQuery.validator.addMethod("checknumber", function (value, element) {
    var pass = new RegExp("^(?=.*[0-9])")
    return this.optional(element) || (pass.test(value));
}, "Password must contain  one number");
</script>

<script>

$('.pass-icons').click(function(e){
    $('#pass_icon').hasClass('flaticon-eye-1')?hidePassword():showPassword()
})
function hidePassword(){
    $('#pass_icon').removeClass('flaticon-eye-1').addClass('flaticon-visibility')
    $('#password').attr('type','text')
}
function showPassword(){
    $('#pass_icon').removeClass('flaticon-visibility').addClass('flaticon-eye-1')
    $('#password').attr('type','password')
}

</script>


@endpush
