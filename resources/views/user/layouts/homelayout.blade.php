<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage">

<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <title>@yield('title', app_name())</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#233D63">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#233D63">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#233D63">

    @stack('meta-tags')
    @include('user.layouts.includes.head_one')
    <!-- favicons -->
</head>

<body itemscope="" itemtype="http://schema.org/WebPage">

    <!--Start: Browser update alert -->
    <!--[if lt IE 10]>
           <div class="browser-update">
           <div class="update-info-wrap">
           <img src="./images/logo.svg" alt="">
           <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
           </div>
            </div>
    <![endif]-->
    <!-- End: Browser update alert -->
          <!-- Header:start -->
         
          <header class="main-header home-header">          
            <div class="hdr-wrapper">                                      
                <nav class="navbar navbar-expand-lg main-navbar navbar-light">
                  <div class="container">  

                  <a class="navbar-brand" href="{{route('home')}}"><img src={{asset('/images/user/150_fordark_bg.png')}} alt="ProjectX-logo"></a>                        
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target=".mobcollapse" aria-label="Toggle navigation">
                      <span> </span>
                      <span> </span>
                      <span> </span>
                    </button>                  
                    <div class="collapse navbar-collapse mobcollapse">
                      <!-- <form class="form-inline menu-search">
                        <input class="form-control" type="search" placeholder="Search by title" aria-label="Search by title">
                        <button class="btn btn-search" type="submit"><span class="sprites-col search-icon"></span></button>
                      </form> -->
                      <ul class="navbar-nav px-0 ml-auto @if(auth('web')->user() && !empty(auth('web')->user())) logged-in @endif">                   
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('userPublicProfile','how-it-works')}}">How it works?</a>
                        </li>                       
                        @if(auth('web')->user() && !empty(auth('web')->user()))
                        <li class="nav-item dropdown"> 
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(!empty(auth('web')->user()->profile_image) && (file_exists( public_path().'/storage/admin/profile-images/'.auth('web')->user()->profile_image)))
                                  @php
                                      $image_with_path = 'storage/admin/profile-images/'.auth('web')->user()->profile_image;
                                      $url = thump_resize_image($image_with_path,32,32);
                                  @endphp
                                  <img src="{{ $url }}" alt="user-image" class="rounded-circle mr-2">
                              @else 
                                  @php
                                      $image_with_path = 'images/admin/starter/default.png';
                                      $url = thump_resize_image($image_with_path,32,32);
                                  @endphp
                                  <img src="{{ $url }}" alt="user-image" class="rounded-circle mr-2">
                              @endif </span>My Account
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              @if(auth('web')->user()->step >= 6)
                              <a class="dropdown-item" href="{{route('account.myProfile',auth('web')->user()->id)}}">  {{ ucfirst(auth('web')->user()->full_name) }}</a>
                              @endif
                              <a class="dropdown-item" href="{{route('logout')}}">  Logout</a>
                          
                             <!-- <a class="dropdown-item" href="#">Action</a>
                             <a class="dropdown-item" href="#">Another action</a>
                             <div class="dropdown-divider"></div> 
                             <a class="dropdown-item" href="#">Something else here</a> -->
                           </div>
                          </li>
                        @else
                        <li class="nav-item">
                          <button class="btn btn-sign-up my-2 my-sm-0" onclick="location.href='{{route('user.register')}}'" type="button"><i class="fi-flaticon flaticon-add-user mr-2"></i>Sign Up</button>
                          <button class="btn btn-login my-2 my-sm-0" onclick="location.href='{{route('showLogin')}}'" type="button"><i class="fi-flaticon flaticon-login mr-2"></i>Login</button>
                        </li>
                        @endif
                      </ul>                  
                    </div>
                  </div>
                </nav>
                <nav class="navbar navbar-expand-lg sub-menus">                
                  <div class="container">  
                  <div class="collapse navbar-collapse mobcollapse">
                    <ul class="navbar-nav">
                      <!-- <li class="nav-item active">
                        <a class="nav-link" href="#">Programming & Development</a>
                      </li> -->
                      <?php 
                  $homecategories_list =  App\Category::where('active',1)->where('home_active',1)->get();
?>

                      @foreach($homecategories_list as $Categoryname)
                      <li class="nav-item">

                      <?php
                      $category= $Categoryname->name;
                    ?>


                        <a class="nav-link" href="{{route('homeservices',["#$category"])}}">{{ $Categoryname->name }}</a>
                      </li> 
                      @endforeach
                      <!-- <li class="nav-item">
                        <a class="nav-link" href="#">Design & Art</a>
                      </li>               
                      <li class="nav-item">
                        <a class="nav-link" href="#">Content Writing & Translation</a>
                      </li> 
                      <li class="nav-item">
                        <a class="nav-link" href="#">Administrative & Secretarial</a>
                      </li> 
                      <li class="nav-item">
                        <a class="nav-link" href="#">Sales & Marketing</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#">Business & Finance</a>
                      </li>                    -->
                    </ul>               
                  </div>
                </div>
              </nav>           
          </div>

        </header>
        <!-- Header:end -->







    @yield('content')
     <!-- The core Firebase JS SDK is always required and must be listed first -->
     <script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-app.js"></script>

     <!-- TODO: Add SDKs for Firebase products that you want to use
    https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-analytics.js"></script>

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-auth.js"></script>

    <script>
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
        apiKey: "AIzaSyAWpJD8wumatQvS3DX7BfYsl31TSm_2HGw",
        authDomain: "keyoxa-569b6.firebaseapp.com",
        databaseURL: "https://keyoxa-569b6.firebaseio.com",
        projectId: "keyoxa-569b6",
        storageBucket: "keyoxa-569b6.appspot.com",
        messagingSenderId: "426344085813",
        appId: "1:426344085813:web:ff175809816132ac1fb47c",
        measurementId: "G-VQG9E1H5S6"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    </script>
    @include('user.layouts.includes.footer_one')
    @include('user.layouts.includes.scripts_one')
    @stack('custom-scripts')
    @include('user.layouts.includes.toastr')
</body>

</html>
