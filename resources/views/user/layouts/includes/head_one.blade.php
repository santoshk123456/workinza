<!-- favicons -->
<link rel="apple-touch-icon" sizes="57x57" href="{{asset('/images/user/favicons/apple-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="60x60" href="{{asset('/images/user/favicons/apple-icon-60x60.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('/images/user/favicons/apple-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="76x76" href="{{asset('/images/user/favicons/apple-icon-76x76.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('/images/user/favicons/apple-icon-114x114.png')}}">
<link rel="apple-touch-icon" sizes="120x120" href="{{asset('/images/user/favicons/apple-icon-120x120.png')}}">
<link rel="apple-touch-icon" sizes="144x144" href="{{asset('/images/user/favicons/apple-icon-144x144.png')}}">
<link rel="apple-touch-icon" sizes="152x152" href="{{asset('/images/user/favicons/apple-icon-152x152.png')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('/images/user/favicons/apple-icon-180x180.png')}}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{asset('/images/user/favicons/android-icon-192x192.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('/images/user/favicons/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('/images/user/favicons/favicon-96x96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('/images/user/favicons/favicon-16x16.png')}}">
<link rel="manifest" href="{{asset('/images/user/favicons/manifest.json')}}">

<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{asset('/images/user/favicons/ms-icon-144x144.png')}}">
<meta name="theme-color" content="#ffffff">

<!-- Pre-load sections -->
<link rel="preload" href="{{asset('/css/user/style.min.css')}}" as="style">
<link rel="preload" href="{{asset('/js/user/bundle.min.js')}}" as="script">
<link rel="preload" href="{{asset('/fonts/user/AmazonEmber-Bold.woff')}}" as="font" type="font/woff" crossorigin>
<link rel="preload" href="{{asset('/fonts/user/AmazonEmber-Regular.woff')}}" as="font" type="font/woff" crossorigin>  
<!-- Style sheets -->
 <link rel="stylesheet" href="{{asset('/css/user/style.min.css')}}">   
 <link rel="stylesheet" href="{{asset('/css/user/jquery.toast.min.css')}}">   
 <link rel="stylesheet" href="{{asset('/css/user/developer.css')}}">   
 <link rel="stylesheet" href="{{asset('/css/user/dropify.min.css')}}">   
 <!-----toastr---------------->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" /> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/min/dropzone.min.css" integrity="sha512-bbUR1MeyQAnEuvdmss7V2LclMzO+R9BzRntEE57WIKInFVQjvX7l7QZSxjNDt8bg41Ww05oHSh0ycKFijqD7dA==" crossorigin="anonymous" />
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /> -->
{{ style('css/intlTelInput.min.css') }}
{{ style('css/rateyo/jquery.rateyo.min.css') }}
@stack('custom-styles')
