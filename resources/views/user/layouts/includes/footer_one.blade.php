<footer>
    <div class="main-footer">
        <div class="container">           
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="foot-cols">
                        <h4>Company Info</h4>
                        <ul>
                            <li><a href="{{route('userPublicProfile','about-us')}}">About Us</a></li>
                            <li><a href="#">Investor Relations</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Upwork Foundation</a></li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-sm-6 col-md-3">
                    <div class="foot-cols"> -->
                        <!-- <h4>Resources</h4>
                        <ul>
                            <li><a href="#">Resources</a></li>
                            <li><a href="#">Customer Support</a></li>
                            <li><a href="#">Customer Stories</a></li>
                            <li><a href="#">Business Resources</a></li>                        
                        </ul> -->
                    <!-- </div>
                </div>   
                <div class="col-sm-6 col-md-3">
                    <div class="foot-cols"> -->
                        <!-- <h4>Resources</h4>
                        <ul>
                            <li><a href="#">Resources</a></li>
                            <li><a href="#">Customer Support</a></li>
                            <li><a href="#">Customer Stories</a></li>
                            <li><a href="#">Business Resources</a></li>                        
                        </ul> -->
                    <!-- </div>   
                </div>     -->
                <div class="col-sm-6 col-md-3">
                    <div class="foot-cols">
                        <h4>Contact</h4>
                        <ul>
                        <li><a href="{{route('homecontact')}}">Contact Us</a></li>
                            <li><a href="tel:{{getSettingValue('contact_number')}}">{{getSettingValue('contact_number') }}</a></li>
                            <li><a href="mailto:{{getSettingValue('application_admin_email')}} ">{{ getSettingValue('application_admin_email')  }}</a></li>                             
                        </ul>
                    </div>      
            </div>         
            </div>
        </div>
    </div>
    <div class="footer_btm">
        <div class="container">
        <div class="row">
            <div class="col-md-4 text-center text-lg-left mb-lg-0 mb-2">
                <!-- <ul class="bottom-links">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Feedback</a></li>
                    <li><a href="#">Community</a></li>
                </ul> -->
            </div>
            <div class="col-md-4 text-center mb-lg-0 mb-2 order-3 order-lg-2">
                <p class="mb-0 p-2">© 2020 - 2021 Keyoxa.</p>
            </div>
            <div class="col-md-4 text-lg-right text-center mb-lg-0 mb-2 order-2">
                <ul class="social-links">
                    <li><a target="blank" href="{{ getSettingValue('facebook_url') }}"><span class="sprites-col fb-icon"></span></a></li>
                    <li><a target="blank" href="{{ getSettingValue('twitter_url') }}"><span class="sprites-col twitter-icon"></span></a></li>
                    <li><a target="blank" href="{{ getSettingValue('linkedin_url')}}"><span class="sprites-col linkedin-icon"></span></a></li>
                    <li><a target="blank" href="{{ getSettingValue('pinterest_url')}}"><span class="sprites-col pinterest-icon"></span></a></li>
                </ul>
            </div>
        </div>        
    </div>
</div>
</footer>