<div class="col-lg-3 left-aside">
    <!-- <h3>My Profile</h3> -->
      <ul class="d-block">
 
          <li class="@if(auth()->user()->step > 1) active @else disabled @endif"><a href=""><i class="fi-flaticon flaticon-contact"></i>Contact Information</a></li>
          <li class="@if(auth()->user()->step > 2) active @else disabled @endif"><a @if(auth()->user()->step >= 2) href="{{route('user.viewSecurityQuestions')}}" @else href="" @endif"><i class="fi-flaticon flaticon-shield"></i>Security Questions</a></li>
          <li class="@if(auth()->user()->step >= 4) active @else disabled @endif"><a @if(auth()->user()->step >= 4) href="{{route('user.backgroundVerification')}}" @else href="" @endif><i class="fi-flaticon flaticon-clipboard"></i>Background Verification</a></li>
          @if(auth()->user()->user_type == 1)
          <li class="@if(auth()->user()->step >= 5) active @else disabled @endif"><a @if(auth()->user()->step >= 5) href="{{route('membership_plan.membershipPlans')}}" @else href="" @endif><i class="fi-flaticon flaticon-premium"></i> Membership Plans</a></li>
          @endif
          <li class="@if(auth()->user()->step >= 6) active @else disabled @endif"><a @if(auth()->user()->step >= 6) href="{{route('account.viewPublicProfile')}}" @else href="" @endif"><i class="fi-flaticon flaticon-user-shape"></i>Profile Card</a></li>
          <li class="@if(auth()->user()->step >= 6) active @else disabled @endif"><a @if(auth()->user()->step >= 6) href="{{route('userPublicProfile',auth()->user()->username)}}" @else href="" @endif" target="_blank"><i class="fi-flaticon flaticon-user-shape"></i>Public Profile Details</a></li>
          <li class="@if(auth()->user()->step >= 6) active @else disabled @endif"><a href="{{route('rating.myRating')}}"><i class="fi-flaticon flaticon-user-shape"></i>Ratings and Reviews</a></li>
        </ul>
  </div>