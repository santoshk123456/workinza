<!-- Header:start -->

<header class="main-header">          
    <div class="hdr-wrapper">                                      
        <nav class="navbar navbar-expand-lg main-navbar navbar-light">
          <div class="container">  
          <a class="navbar-brand" href="{{route('home')}}"><img src={{asset('/images/user/150_fordark_bg.png')}} alt="ProjectX-logo"></a>    
            
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target=".mobcollapse" aria-label="Toggle navigation">
              <span> </span>
              <span> </span>
              <span> </span>
            </button>                  
            <div class="collapse navbar-collapse mobcollapse">
              <!-- <form class="form-inline menu-search">
                <input class="form-control" type="search" placeholder="Search by title" aria-label="Search by title">
                <button class="btn btn-search" type="submit"><span class="sprites-col search-icon"></span></button>
              </form> -->
              <ul class="navbar-nav px-0 ml-auto logged-in">                   
                <li class="nav-item">
                  <a class="nav-link" href="{{route('userPublicProfile','how-it-works')}}">How it works?</a>
                </li>                     
                <li class="nav-item dropdown"> 
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  @if(!empty(auth()->user()->profile_image) && (file_exists( public_path().'/storage/admin/profile-images/'.auth()->user()->profile_image)))
                        @php
                            $image_with_path = 'storage/admin/profile-images/'.auth()->user()->profile_image;
                            $url = thump_resize_image($image_with_path,32,32);
                        @endphp
                        <img src="{{ $url }}" alt="user-image" class="rounded-circle mr-2">
                    @else 
                        @php
                            $image_with_path = 'images/admin/starter/default.png';
                            $url = thump_resize_image($image_with_path,32,32);
                        @endphp
                        <img src="{{ $url }}" alt="user-image" class="rounded-circle mr-2">
                    @endif </span>My Account
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @if(auth('web')->user()->step >= 6)
                     <a class="dropdown-item" href="{{route('account.myProfile',auth('web')->user()->id)}}">  {{ ucfirst(auth()->user()->full_name) }}</a>
                     @endif
                     <a class="dropdown-item" href="{{route('user.change_password')}}">  Change Password</a>
                     <a class="dropdown-item" href="{{route('logout')}}">  Logout</a>

                    <!-- <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div> 
                    <a class="dropdown-item" href="#">Something else here</a> -->
                  </div>
                </li>   
                <!-- <li class="nav-item"> <button class="btn btn-post my-2 my-sm-0" type="submit"><span class="sprites-col post-icon"></span>Post a Project</button></li>                   -->
                @if(auth()->user()->user_type == 0 && auth()->user()->step == 6)
                <li class="nav-item mt-3"> <a class="btn btn-post my-2 my-sm-0" type="button" href="{{ route('project.createProject') }}"><i class="fi-flaticon flaticon-project mr-2"></i>Post a Project</a></li>
               
                @endif
                @if(auth()->user()->user_type == 1 && auth()->user()->step == 6)
                <li class="nav-item mt-3"> <a class="btn btn-post my-2 my-sm-0" type="button" href="{{ route('project.findProject') }}"><i class="fi-flaticon flaticon-project mr-2"></i>Find a Project</a></li>
               
                @endif
              </ul>                  
            </div>
          </div>
        </nav>
       
        <nav class="navbar navbar-expand-lg sub-menus">                
          <div class="container">  
          <div class="collapse navbar-collapse mobcollapse">
            <ul class="navbar-nav inner-pg-menu">

            <li class="nav-item @if(Request::is('dashboard') ) active @endif">
              <a class="nav-link" href="{{route('user.viewdashboard')}}">Dashboard</a>
              </li> 

              <li class="nav-item @if((Request::is('register*') && !(Request::is('register/dashboard/view'))) || Request::is('membership-plan*') || Request::is('account*') || Request::is('rating*') ) active @endif">
                <a class="nav-link" @if(auth()->user()->step >= 3) href="{{route('user.viewContactInformation')}}" @else href="" @endif">Profile</a>
              </li>

      
              <!-- <li class="nav-item">
                <a class="nav-link" href="#">Hire</a>
              </li>                -->
              <li class="nav-item @if(Request::is('project*') ||  ((Request::is('message*') && !(Request::is('message/public-message*')) && !(Request::is('message/message-detail*'))))) active @endif">
                  @if(auth()->user()->user_type == 0)
                    @if(auth()->user()->step == 6)
                      <a class="nav-link" href="{{route('project.listOpenProjects')}}">Projects</a>
                    @else
                      <a class="nav-link" href="">Projects</a>
                    @endif
                  @else 
                    @if(auth()->user()->step == 6)
                      <a class="nav-link" href="{{route('project.listInvitations')}}">Projects</a>
                    @else
                      <a class="nav-link" href="">Projects</a>
                    @endif
                  @endif
              </li> 

          




              <li class="nav-item @if(Request::is('finance*') ) active @endif">
                @if(auth()->user()->user_type == 1)
                  <a class="nav-link" href="{{route('finance.listTransactions')}}">Finances</a>
                @else
                  <a class="nav-link" href="{{route('finance.listTransactions')}}">Finances</a>
                @endif
              </li> 
              <li class="nav-item @if(Request::is('message/public-message*') || Request::is('message/message-detail*') ) active @endif">
                <a class="nav-link" href="{{route('message.publicMessages')}}">Messages</a>
              </li>                       
            </ul>               
          </div>
        </div>
      </nav>           
  </div>

</header>

<!-- Header:end -->