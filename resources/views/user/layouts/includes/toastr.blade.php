@if(Session::has('toastr'))
<script>
    toastr.{{ Session::get('toastr.type') }}('{{ Session::get("toastr.text") }}','{{ Session::get("toastr.title") }}');
</script>
@elseif(isset($toastr))
<script>
    toastr.{{ $toastr['type'] }}('{{ $toastr["text"] }}');
</script>
@endif