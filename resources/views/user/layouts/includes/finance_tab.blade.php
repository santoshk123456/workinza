<div class="col-lg-3 left-aside left-projects-list">         
    <ul class="d-block icons-list">               
        <li class=@if(Request::is('finance/transactions*')) active @endif><a href="{{ route('finance.listTransactions') }}"><i class="fi-flaticon flaticon-wallet mr-2"></i>Transactions</a></li>
        <li class=@if(Request::is('finance/invoices*') || Request::is('finance/invoice-details*')) active @endif><a href="{{ route('finance.listInvoices') }}"><i class="fi-flaticon flaticon-invoice mr-2"></i>Invoices</a></li>
        @if($user->user_type == 1)
            <li class=@if(Request::is('finance/my-wallet*')) active @endif><a href="{{ route('finance.myWallet') }}"><i class="fi-flaticon flaticon-wallet mr-2"></i>My Wallet</a></li>
        @endif
        <li class=@if(Request::is('finance/my-vault*')) active @endif><a href="{{ route('finance.myVault') }}"><i class="fi-flaticon flaticon-wallet mr-2"></i>Keyoxa Vault</a></li>
        <li class=@if(Request::is('finance/dispute*')) active @endif><a href="{{ route('finance.ListDisputes') }}"><i class="fi-flaticon flaticon-dispute mr-2"></i>Disputes</a></li>  
        @if($user->user_type == 1)
            <li class=@if(Request::is('finance/referrals*') || Request::is('finance/bonus*')) active @endif><a href="{{ route('finance.listReferrals') }}"><i class="fi-flaticon flaticon-user mr-2"></i>Referral Incentives / Bonus</a></li>
        @endif 
        @if($user->user_type == 1)
            <li class=@if(Request::is('finance/current-clients*') || Request::is('finance/invited-clients*')) active @endif><a href="{{ route('finance.invitedClients') }}"><i class="fi-flaticon flaticon-user mr-2"></i>Clients</a></li>
        @endif 
          
    </ul>
</div>