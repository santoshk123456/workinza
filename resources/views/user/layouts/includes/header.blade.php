<!-- Signin Header:start -->
<header class="signin-header">
    <div class="hdr-wrapper">
        <nav class="navbar navbar-expand-lg main-navbar navbar-light">
            <div class="container">
                <a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('/images/user/150_forwhite_bg.png')}}"
                        alt="ProjectX-logo"></a>
                        @if ((!Request::is('login')) && (!Request::is('forgot-password') ) && (!Request::is('reset-password') ))
                        <ul class="navbar-nav px-0 ml-auto">
                            <li class="nav-item">
                                Already have an account?<a class="nav-link" href="{{route('login')}}"> Log In</a>
                            </li>
                        </ul>
                        @endif
            </div>
        </nav>
    </div>
</header>
<!--Signin Header:end -->
