<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage">
    <head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="description" content="">
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#233D63">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#233D63">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#233D63">
        <!-- CSRF -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <!---bootstrap css ----->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

        @stack('meta-tags')
        @include('user.layouts.includes.head_one')
        <!-- favicons -->
    </head>
    <body itemscope="" itemtype="http://schema.org/WebPage" onload="startTime()">
        <!-- Loading -->
        <div class="se-pre-con"></div>
        <!-- Loading -->
      
        <!-- End: Browser update alert -->
       
        @stack('custom-styles')
        @yield('content')


       
        <!---bootstrap js ----->
        <!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> -->
      <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>  -->
        <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        ------------- -->
        @include('user.layouts.includes.scripts_one')
        @stack('custom-scripts')
        @include('user.layouts.includes.toastr')
       
    </body>
</html>