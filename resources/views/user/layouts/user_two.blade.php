<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage">

<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <title>@yield('title', app_name())</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#233D63">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#233D63">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#233D63">
    @stack('meta-tags')
    @include('user.layouts.includes.head_one')
    <!-- favicons -->
</head>

<body itemscope="" itemtype="http://schema.org/WebPage">
    @include('user.layouts.includes.head_two')
    @yield('content')
     <!-- The core Firebase JS SDK is always required and must be listed first -->
     <script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-app.js"></script>

     <!-- TODO: Add SDKs for Firebase products that you want to use
    https://firebase.google.com/docs/web/setup#available-libraries -->
    <script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-analytics.js"></script>

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-auth.js"></script>

    <script>
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
        apiKey: "AIzaSyAWpJD8wumatQvS3DX7BfYsl31TSm_2HGw",
        authDomain: "keyoxa-569b6.firebaseapp.com",
        databaseURL: "https://keyoxa-569b6.firebaseio.com",
        projectId: "keyoxa-569b6",
        storageBucket: "keyoxa-569b6.appspot.com",
        messagingSenderId: "426344085813",
        appId: "1:426344085813:web:ff175809816132ac1fb47c",
        measurementId: "G-VQG9E1H5S6"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    </script>
    @include('user.layouts.includes.footer_one')
    @include('user.layouts.includes.scripts_one')
    @stack('custom-scripts')
    @include('user.layouts.includes.toastr')
</body>

</html>
