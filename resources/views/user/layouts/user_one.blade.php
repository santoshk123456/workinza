<!DOCTYPE html>
<html lang="en" itemscope="" itemtype="http://schema.org/WebPage">

<head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="description" content="">
        <title>@yield('title', app_name())</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
         <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#233D63">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#233D63">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#233D63">
        @stack('meta-tags')
        @include('user.layouts.includes.head_one')
        @include('user.layouts.includes.header')
        <!-- favicons -->
    </head>
    <body itemscope="" itemtype="http://schema.org/WebPage">
       
        @yield('content')
        @include('user.layouts.includes.scripts_one')
        @stack('custom-scripts')
        @include('user.layouts.includes.toastr')
    </body>
</html>