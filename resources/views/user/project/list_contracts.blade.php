@extends('user.layouts.user_layout')
@section('content')

<div class="row">
    <h5>My Proposals</h5>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form method="GET" action="{{route('project.listProposals')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
                    <input value="{{old('title')}}" type="text" name="contract_id" class="form-control" id="contract_id" placeholder="{{__('Search contract id')}}" required>
                    
                    <div class="mt-2">
                        <button type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-12">
        
        @if(!$contracts->isEmpty())
        <table>
            <tr>
                <td>{{__('Contract Id')}}</td>
                <td>{{__('Client')}}</td>
                <td>{{__('Contract Amount')}}</td>
                {{-- <td>{{__('Contract Start Date')}}</td>
                <td>{{__('Contract End Date')}}</td> --}}
                <td>{{__('Contract Status')}}</td>
                <td>Action</td>
            </tr>
            @foreach ($contracts as $contract)
                <tr>
                    <td> {{getContractId($contract->id)}}
                    </td>
                    <td>{{$contract->user->username}}</td>
                    <td>{{formatAmount($contract->amount)}}</td>
                    {{-- <td>{{hcms_date($contract)}}</td>
                    <td>{{hcms_date($proposal_end_date)}}</td> --}}
                    <td>{{getContractStatus($user->type,$contract->status,$contract->freelancer_acceptance)}}</td>
                    <td>
                        <a href="{{route('project.prepareContract',$contract)}}">View</a>
                    </td>
                  
                    
                </tr>
               
                   
               
                <tr>
                   
                </tr>
                <br>
            @endforeach
        </table>
        @else
            {{__('No Contract found')}} 
        @endif
    </div>
    <div>
                <!-- Pagination -->
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
            <!-- Pagination -->
            <div class="pagination-container margin-top-40 margin-bottom-60">
                <nav class="pagination">
                     <ul>
                        {{ $contracts->links('vendor.pagination.default') }}
                    </ul>
                </nav>
            </div>
        </div>
        </div>
    </div>

</div>


