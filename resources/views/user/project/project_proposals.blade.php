@extends('user.layouts.user_two')
@section('title', set_page_titile('List Proposals'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">
        <h3 class="title">{{$project->title}}</h3>
        <div class="row mx-0">
            <div class="col-lg-3 left-aside left-projects-list">         
                <ul class="d-block">
                    <li class="active"><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
                    <li><a href="{{route('project.listInProgressProjects')}}">In Progress Projects</a></li>
                    <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
                </ul>
            </div>
            <div class="col-lg-9">
                <div class="right-aside">         
                    <div class="bg-shadow mb-3 projects-links-menu">
                        <ul>
                            <li><a href="{{ route('project.projectDetails',$project) }}" ><i class="fi-flaticon flaticon-blank-page mr-2"></i>Project Details </a></li>
                            <li><a href="{{ route('project.inviteFreelancers',$project) }}"><i class="fi-flaticon flaticon-send mr-2"></i>Invite Freelancers</a></li>
                            <li><a href="{{ route('project.projectProposals',$project) }}" class="active"><i class="fi-flaticon flaticon-auction mr-2"></i>Proposals</a></li>
                            @if(App\ProjectChat::ChatFeature()==1)      
                            <li><a href="{{ route('message.messages',$project) }}"><i class="fi-flaticon flaticon-comments mr-2"></i>Messages</a></li>  
                               @endif          
                        </ul>                
                    </div> 
                
                    <!-- proposals-list:start -->
                    <div class="proposals-list-holder bg-shadow">
                        @if(!$proposals->isEmpty())
                        <h2 class="heading-sm">Proposals</h2>
                       
                            @foreach ($proposals as $proposal)
                      
                                <!-- proposals-list-item:start -->       
                                <div class="col-list-item">                    
                                    <div class="media text-center text-md-left d-block d-md-flex">
            @if($proposal->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$proposal->user->profile_image)))
<img class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail"  src="{{ asset('storage/user/profile-images/'.$proposal->user->profile_image) }}" alt="{{ $proposal->user->username }}">
            @else 
  <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $proposal->user->username }}"
                        class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
         @endif
                      <!-- <img class="mr-md-3 thumb mb-3 mb-md-0" src="https://randomuser.me/api/portraits/women/63.jpg" alt="Generic placeholder image"> -->


                                        <!-- <img class="mr-3 thumb" src="https://randomuser.me/api/portraits/women/63.jpg" alt="Generic placeholder image"> -->
                                        <div class="media-body">
                                            <div class="row">
                                                <div class="col-lg-7">

                                                <?php 
                            $status ="null";
                            ?>
                                                    <a href="{{ route('account.viewUserDetail',['user'=>$proposal->user,'project'=>$proposal->project,'status'=>$status]) }}"><h5 class="mt-0 name">{{$proposal->user->username}}</a>
                                                        
                                                    @if($proposal->user->organization_type == 0 )
                                                    
                                                    <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                                                @else
                                                    <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                                                @endif
                                                
                                                </h5>
                                                    @php 
                                                        $industries = $proposal->user->industries->pluck('name')->toArray();
                                                        $industry = ucwords(implode(',  ', $industries)); 
                                                    @endphp
                                                    <span class="industry">{{$industry}}</span>
                                             
                                                    @if($proposal->user->userDetail)
                                                    <h6 class="designation">{{$proposal->user->userDetail->profile_headline}}</h6>
                                           
                                                    <p class="description">{{str_limit(ucwords($proposal->user->userDetail->about_me), $limit = 260, $end = '...')}}</p>
                                                    @endif
                                                </div>
                                               
                                                <div class="col-lg-5">
                                                    <div class="list-right-aside">
                                                        
                                                        @if(!$proposal->projectContract->isNotEmpty())
                                                            <form method="POST" action="{{route('project.acceptProposal')}}" class="d-inline-block accept-form needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="accept-form" novalidate>
                                                                @csrf
                                                                <input value="{{$proposal->user_id}}" type="hidden" name="freelancer_id" class="form-control" id="freelancer_id" >
                                                                <input value="{{$proposal->project_id}}" type="hidden" name="project_id" class="form-control" id="project_id" >
                                                                <input value="{{$proposal->proposal_amount}}" type="hidden" name="proposal_amount" class="form-control" id="proposal_amount" >
                                                                <input value="{{$proposal->id}}" type="hidden" name="project_proposal_id" class="form-control" id="project_proposal_id" >
                                                               
                                                                <button class="btn bg-nm-btn mr-2" type="submit"><i class="fi-flaticon flaticon-double-tick-indicator mr-2"></i>Accept</button>
                                                                
                                                            </form>
                                                        @else
                                                        
                                                            @if( $proposal->projectContract[0]->status == 0)
                                                            <form method="POST" action="{{route('project.acceptProposal')}}" class="d-inline-block accept-form needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="accept-form" novalidate>
                                                                @csrf
                                                                <input value="{{$proposal->user_id}}" type="hidden" name="freelancer_id" class="form-control" id="freelancer_id" >
                                                                <input value="{{$proposal->project_id}}" type="hidden" name="project_id" class="form-control" id="project_id" >
                                                                <input value="{{$proposal->proposal_amount}}" type="hidden" name="proposal_amount" class="form-control" id="proposal_amount" >
                                                                <input value="{{$proposal->id}}" type="hidden" name="project_proposal_id" class="form-control" id="project_proposal_id" >
                                                                <input value="{{$proposal->projectContract[0]->id}}" type="hidden" name="contract_id" class="form-control"  >
                                                                <button class="btn bg-nm-btn mr-2" type="submit"><i class="fi-flaticon flaticon-double-tick-indicator mr-2"></i>Accept</button>
                                                                
                                                            </form>
                                                            @else
                                                            <a href="" class="btn bg-nm-btn mr-2"><i class="fi-flaticon flaticon-double-tick-indicator mr-2"></i>{{__('Accepted')}}</a>
                                                            @endif
                                                        @endif
                                                        <button class="btn bg-nm-btn mr-1" onclick="location.href='{{route('project.viewProposal',$proposal)}}'"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button> 
                                                        @if(App\ProjectChat::ChatFeature()==1)
                                                        <button class="btn white-nm-btn" onclick="location.href='{{route('message.messages',array($proposal->project->uuid,$proposal->user->uuid))}}'" type="submit"><i class="fi-flaticon flaticon-send mr-2"></i>Message</button>
                                                        @endif
                                                        <div class="box-grey">
                                                            
                                                            <div class="item">
                                                                <label class="data">Proposed Amount:</label>
                                                                <span class="value bld">{{formatAmount($proposal->proposal_amount)}}</span>
                                                            </div>
                                                            <div class="item">
                                                                <label class="data">Submited Date:</label>
                                                                <span class="value">{{hcms_date(strtotime($proposal->created_at),'date')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="additional-data">
                                                        <div class="skils-cols">
                                                            @php 
                                                                $skills_all = $proposal->user->skills->pluck('name')->toArray();
                                                            @endphp
                                                            @foreach( $skills_all as $skill)
                                                                <span class="notify-col">{{$skill}}</span>
                                                            @endforeach   
                                                        </div>
                                                        <div class="basic-info">
                                                            <ul>
                                                                <li><p><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>@if(isset($proposal->user->userDetail->city) && isset($proposal->user->userDetail->state) && $proposal->user->userDetail->country->name){{$proposal->user->userDetail->city}} , {{$proposal->user->userDetail->state}}, {{$proposal->user->userDetail->country->name}} @else {{__('NA')}}@endif</p></li>
                                                                <li><p>Exp:<span>@if(!empty($proposal->user->userDetail->years_of_experience)){{$proposal->user->userDetail->years_of_experience}} Yrs @else {{__('NA')}}  @endif</span></p></li>
                                                                <li><div class="ratings"><div class="rate-count">@if(!empty($proposal->user->userDetail->avg_rating)){{$proposal->user->userDetail->avg_rating}}@else 0 @endif</div> <div class="user-rating" data-value="@if(!empty($proposal->user->userDetail->avg_rating)){{$proposal->user->userDetail->avg_rating}}@else 0 @endif"></div></div></li>
                                                                <li><p>Member Since:<span>{{hcms_date(strtotime($proposal->user->created_at),'date')}}</span></p></li>
                                                               
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>             
                                <!-- proposals-list-item:end -->
  
                                <hr>
                            @endforeach
                        @else
                            <div class="col-list-item">                    
                                <div class="media text-center text-md-left d-block d-md-flex">
                                    {{__('No proposal found')}}
                                </div>
                            </div>
                        @endif
                         @if(!$proposals->isEmpty())
                        <div class="pagination-col mt-4 mb-3">
                    <nav aria-label="page navigation">
                      <ul class="pagination justify-content-end pagination-sm flex-wrap">
        
                      {{ $proposals->links('vendor.pagination.customnew') }}

                      </ul>
                    </nav>
                  </div>
                  @endif


                    </div>
                    <!-- proposals-list:end -->



       


                </div>
            </div>
        </div>
    </div> 
</main>
@endsection
@push('custom-scripts')
<script>
 $(document).ready(function(){
    

    $('.accept-form').submit(function(e) {  
               
        e.preventDefault();
        var $button = $(this).find('button');
      
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('project.createContract')}}",
                data: $(this).serialize(),
                success: function (data) { 
                   if(data['success'] == 1){
                        $button.attr('disabled', 'disabled').text('Accepted');
                        var contract_id = data['uuid'];
                        console.log(contract_id);
                        let url = "{{ route('project.prepareContract', ':id') }}"; 
                        url = url.replace(':id', contract_id)
                     
                        window.location.href = url;
                   }else{
                       alert(data['msg']);
                       $button.removeAtr("disabled");
                   }
                },
                beforeSend: function(){
                    //$button.attr('disabled', 'disabled').text('Accepting...');
                    $button.attr('disabled', 'disabled').html('<i class="fi-flaticon flaticon-double-tick-indicator mr-2"></i>Accepting...');
                }
        });
    });
 });
</script>

@endpush



