@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Create Proposal')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush
@section('content')
<!-- <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet"> -->


<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">

        <!-- main page title:start -->
        <div class="row m-0">
            <div class="col-sm-6">
                <h3 class="title-main">Accept Project - {{ucwords($project->title)}}</h3>
            </div>
            <div class="col-sm-6 text-sm-right mt-sm-0 mt-3">
                <button class="btn cancel-shadow-btn" type="submit" onclick="window.location.href='{{route('project.listInvitations')}}'"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>{{__('List all Invitations')}}</button>
            </div>
        </div>
        <!-- main page title:end -->

        <div class="row m-0 mt-4">
            <!-- right section:start -->
            <div class="col-md-8">
                <form method="post" class="form-validate" enctype="multipart/form-data" id="create-proposal-form" action="{{route('project.storeProposal',$project)}}">
                    @csrf
                    <!-- form - Required Expertise Details:start -->
                    <div class="form-block bg-shadow">
                        <div class="block-header-apl">
                            <div class="inner">
                                <h4 class="block-title">{{__('Proposal Milestone Details')}}</h4>
                            </div>
                        </div>
                        <div class="block-content">

                            <!-- milestone input:start -->
                            <div class="milestone-box border-0 p-0">

                                <table class="table">
                                    <tbody>
                                        <tr class="milestone-inner-col">
                                            <td scope="row" class="border-table">


                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="title" class="control-label">{{__('Milestone')}}</label>

                                                            <input type="text" name="milestone_names[]" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('eg Milestone 2')}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="project-start-date" class="control-label">{{__('Start Date')}}</label>
                                                            <!-- <input type="text" id="datepicker-ml-1" class="form-control"  aria-describedby="datepicker" placeholder="DD/MM/YY"> -->
                                                            <input type="text" id="milestone_start_date" name="milestone_start_dates[]" class="form-control no-type milestone_start_date" required autocomplete="off" placeholder="{{ __('DD/MM/YY') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="project-end-date" class="control-label">{{__('End Date')}}</label>
                                                            <!-- <input type="text" id="datepicker-ml-2" class="form-control"  aria-describedby="datepicker" placeholder="DD/MM/YY"> -->
                                                            <input type="text" id="milestone_end_date" name="milestone_end_dates[]" class="form-control no-type milestone_end_date" required autocomplete="off" placeholder="{{ __('DD/MM/YY') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="amount" class="control-label">{{__('Amount')}}</label>
                                                            <!-- <input type="text" class="form-control" id="title"  placeholder="Enter the amount"> -->
                                                            <input type="number" name="milestone_amounts[]" class="form-control no-inc numericOnly milestone_amount" min="1" max="9999999999" id="milestone_amount" placeholder="{{__('Enter the amount')}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group required">
                                                            <label for="description" class="control-label">{{__('Description')}}</label>
                                                            <!-- <textarea class="form-control" id="details" rows="1" placeholder="Milestone desription"></textarea> -->
                                                            <textarea name="milestone_descriptions[]" id="milestone_description" class="form-control milestone_description" placeholder="{{__('Milestone desription')}}" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>


                                            </td>
                                            <td class="remove-buttons border-0 text-right">
                                                <button class="btn bg-nm-btn add_more mr-2" type="button">{{__('Add Milestone')}}</button>
                                                <button type="button" class="btn remove_row white-nm-btn border">Remove Milestone</button>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- milestone input:end -->



                            <!-- milestone add buttons:start -->
                            <!-- <div class="row mt-4 mb-3">
                            <div class="col-12 text-right">
                                <button class="btn bg-nm-btn py-1" type="submit">Add Milestone</button>
                            </div>
                        </div> -->
                            <!-- milestone add buttons:end -->

                            <!-- desabled inputs:start -->
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <div class="form-group readonly-on-sbt">
                                        <label for="amount">{{__('Total')}}</label>
                                        <input class="form-control" type="text" readonly id="total_amount" name="total_amount">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group readonly-on-sbt">
                                        <label for="amount">{{__('Service Fee')}}</label>
                                        <input name="service_fee" id="service_fee" value="{{$subscription_details->service_percentage}}" class="form-control" type="text" value="" readonly>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group readonly-on-sbt">
                                        <label for="amount">{{__('My Earnings')}}</label>
                                        <input id="my_earnings" name="my_earnings" class="form-control" type="text" value="" readonly>
                                    </div>
                                </div>
                            </div>
                            <!-- desabled inputs:start -->

                        </div>
                    </div>
                    <!-- form - Required Expertise Details:end -->



                    <!-- form - project details:start -->
                    <div class="form-block bg-shadow mt-4">
                        <div class="block-header-apl">
                            <div class="inner">
                                <h4 class="block-title">{{__('Proposal Details')}}</h4>
                            </div>
                        </div>
                        <div class="block-content">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group required">
                                        <label for="summernote-basic" class="control-label">{{__('Description')}}</label>
                                        <textarea name="comments"  id="summernote-basic" class="form-control" rows="3" placeholder="{{__('Project Description')}}"></textarea>
                                        <input type="hidden" value="">
                                        <span id="comment-error" class="error-txt comment-error"></span>
                                        <!-- <div class="attachments mt-3">
                                        <p><input type="file" name="files" multiple><i class="fi-flaticon flaticon-attachments mr-2"></i>Add Attachement</p>
                                    </div> -->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- form - project details:end -->



                    <div class="row">
                        <div class="col-12 text-right">
                            <button class="btn cancel-shadow-btn  mt-4 mb-4 px-4 mr-3" type="submit">{{__('Cancel')}}</button>
                            <button class="btn bg-nm-btn mt-4 mb-4 px-4" type="submit">{{('Submit Proposal')}}</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- right section:start -->


            <!-- left section:start -->
            <div class="col-md-4">

                <!-- right widget:start -->
                <div class="form-block bg-shadow apl-right-aside">
                    <div class="block-header">
                        <div class="inner">
                            <h4 class="block-title">{{ucwords($project->title)}}</h4>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="description">
                            <h2 class="title">{{__('Description')}}</h2>
                            <p>{{$project->description}}</p>
                        </div>
                        <div class="info">
                            <p>{{__('Project Start Date')}}: <span>{{$project->expected_start_date ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($project->expected_start_date), timezone()->setShortDateFormat())) : ''}}</span></p>
                            <p>{{__('Project End Date')}}: <span>{{$project->expected_end_date ? with(timezone()->convertToLocal(\Carbon\Carbon::parse($project->expected_end_date), timezone()->setShortDateFormat())) : ''}}</span></p>
                            <p>{{__('Budget')}}: <span>{{formatAmount($project->budget_from)}} - {{formatAmount($project->budget_to)}}</span></p>
                            <p>{{__('Type')}}: <span>@if($project->type==0) {{__('Fixed Rate')}} @else {{__('Hourly Rate')}} @endif </span></p>
                        </div>

                        <hr class="sep">

                        <div class="media applicant-info text-center text-md-left d-block d-md-flex mb-3">
                        @if($project->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$project->user->profile_image)))
                                        <img class="mr-md-3 thumb" src="{{ asset('storage/user/profile-images/'.$project->user->profile_image) }}"  alt="{{ $project->user->username }}">
                                    @else 
                                        <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $project->user->username }}" class="mr-md-3 thumb">
                                    @endif
                        <!-- <img class="mr-3 thumb" src="https://randomuser.me/api/portraits/women/63.jpg" alt="Generic placeholder image"> -->
                            <div class="media-body">
                            <?php 
                            $status ="null";
                            ?>

                                <a href="{{ route('account.viewUserDetail',['user'=>$project->user,'project'=>$project,'status'=>$status]) }}"><h5 class="mt-0 name">{{ucwords($project->user->username)}} @if($project->user->organization_type == 0 )
                                                    
                                    <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                                @else
                                    <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                                @endif</h5></a>
                              
                                <span class="location"><i class="fi-flaticon flaticon-location-pin filled mr-1"></i>@if(!empty($project->user->userDetail->city)){{ucwords($project->user->userDetail->city)}}@else NA @endif, @if(!empty($project->user->userDetail->state)){{ucwords($project->user->userDetail->state)}}@else NA @endif, @if(!empty($project->user->userDetail->country->name)){{ucwords($project->user->userDetail->country->name)}} @else NA @endif</span>
                                <div class="ratings">
                                    <div class="rate-count">@if(!empty($project->user->userDetail->avg_rating)){{$project->user->userDetail->avg_rating}} @else 0 @endif</div> <div class="user-rating" data-value="@if(!empty($project->user->userDetail->avg_rating)){{$project->user->userDetail->avg_rating}}@else 0 @endif"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- right widget:end -->


            </div>
            <!-- left section:start -->
        </div>







    </div>
</main>
<!-- <form method="post" enctype="multipart/form-data" id="public-profile-form" action="{{route('project.storeProposal',$project)}}">
    @csrf
    <h1>Application for {{$project->title}}</h1>
    <div class="row">
        <h5>Propsal milestone details</h5>
        
        <div class="col-lg-6 ">
            <div class="form-group">
                <table>
                    <tr>
                        <td scope="row">
                            <input  type="text" name="milestone_names[]" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Name')}}" required>
                        </td>
                        <td scope="row">
                            <input  type="number" name="milestone_amounts[]" class="form-control no-inc numericOnly amount" min="1" max="9999999999" id="milestone_amount" placeholder="{{__('Amount')}}" required>
                        </td>
                        <td scope="row">
                            <input type="text" name="milestone_start_dates[]" class="form-control no-type milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                        </td>
                        <td scope="row">
                            <input type="text" name="milestone_end_dates[]" class="form-control no-type milestone_end_date" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                        </td>
                        <td scope="row">
                            <textarea name="milestone_descriptions[]" id="milestone_descriptions" class="form-control" placeholder="Description" required></textarea>
                        </td>
                        <td><button class="btn btn-success add_more" type="button">+</button>
                        <button type="button" class="btn btn-danger ml-2 remove_row">-</button></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <table>
        <tr>
            <div class="row">
                <input  type="number" name="total_amount" class="form-control no-inc numericOnly " min="1" max="9999999999" id="total_amount" placeholder="{{__('Total Amount')}}" required>
            </div>
        </tr>
        <tr>
            <div class="row">
                <input   type="number" name="service_fee" class="form-control no-inc numericOnly " min="1" max="9999999999" id="service_fee" placeholder="{{__('Service Fee')}}" required>
                <input  value="{{$subscription_details->service_percentage}}"type="hidden" name="service_percentage" class="form-control no-inc numericOnly " min="1" max="9999999999" id="service_percentage" >
            </div>
        </tr>
        <tr>
            <div class="row">
                <input  type="number" name="my_earnings" class="form-control no-inc numericOnly" min="1" max="9999999999" id="my_earnings" placeholder="{{__('My Earnings')}}" required>
            </div>
        </tr>
    </table>
    <div class="row">
        <textarea name="comments" id="comments" class="form-control" placeholder="Description" ></textarea>
    </div>
    <div class="row">
        <input type="file" name="proposal_document" id="proposal_document" class="form-control" placeholder="Description" ></textarea>
    </div>
    <div class="mt-2">
        <button type="submit">Submit</button>
    </div>
</form> -->

@endsection

@push('custom-scripts')

<!-- <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script> -->

<script>
    //  $('#summernote-basic').summernote({
    //     height: 100
    // });
   
    $(document).on("blur", '.milestone_amount', function() {

        var inputs = $(".milestone_amount");
        var total_amount = 0;

        for (var i = 0; i < inputs.length; i++) {
            total_amount = parseFloat(total_amount) + parseFloat($(inputs[i]).val());

        }
        $('#total_amount').val(total_amount);
        var service_percentage = <?php echo $subscription_details->service_percentage; ?>;
        var service_fee = (service_percentage * total_amount) / 100;
        $('#service_fee').val(service_fee);
        var my_earnings = total_amount - service_fee;
        $('#my_earnings').val(my_earnings);

    });
    function datepickerRemover(input){
        $(input).parent().children('i').remove();
        $(input).removeClass('gj-textbox-md');
        $(input).removeAttr('data-type');
        $(input).removeAttr('data-guid');
        $(input).removeAttr('data-datepicker');
        $(input).parent().parent().append($(input).parent().html());
        $(input).parent().parent().children('div').remove();
    }
    $('.milestone-box').find('.remove_row').hide();
    $(document).on("click", '.add_more', function() {

        var $tr = $(this).closest('tr');
        var $clone = $tr.clone();
        $clone.find('input').val('');
        $clone.find('textarea').val('');

        var count = $('.milestone_start_date').length;
        var start = $('.milestone_start_date').length;
        var end = $('.milestone_end_date').length;
 
        if(count>0)
        {
        
        $('.remove_row').show();
        }
      
        $clone.find('input.milestone_start_date')
            .attr('id', 'dd_date_start' + count); //newly added line
        $clone.find('input.milestone_end_date')
            .attr('id', 'dd_date_end' + count); //newly added line
        $clone.find('input.milestone_start_date').
        removeAttr('data-guid').removeAttr('data-datepicker');

        $clone.find('input.milestone_end_date').
        removeAttr('data-guid').removeAttr('data-datepicker');



        $clone.find('input').prop('required', false);
        $clone.find('textarea').prop('required', false);

        $clone.find('input.milestone_names')
            .attr('id', 'dd_title' + count).prop('required', true);
        $clone.find('input.milestone_amount')
            .attr('id', 'dd_amount' + count).prop('required', true);
        $clone.find('textarea.milestone_description')
            .attr('id', 'dd_description' + count).prop('required', true);


        $(this).hide();
        
        $('.milestone-box table tr').not('.milestone-box table tr:last').find('.remove_row').show();

        $tr.after($clone);
        
        console.log(start);
        if(start <= 1){
            var last_val = $('#milestone_end_date').datepicker().value();
        }
        else{
            new_start = start-1;
            var last_val = $('#dd_date_end'+new_start).datepicker().value();
        }
        console.log(last_val);
        var startDate = new Date(last_val);
        startDate.setDate(startDate.getDate() + 1);
        // $('#dd_date_start' + count).datepicker();
        // $('#dd_date_end' + count).datepicker();
        console.log(startDate);
        $('#dd_date_start' + count).datepicker({
            minDate: startDate,
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            change: function (e) {
                datepickerRemover('#dd_date_end' + count);
                var startDate = new Date(e.target.value);
                $('#dd_date_end' + count).datepicker({
                    value:'',
                    minDate: startDate,
                });
            }
        });
        $('#dd_date_end' + count).datepicker({
            minDate: startDate,
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
        });



    });
    $(document).on("click", '.remove_row', function() {
        if ($('.milestone_names').length == 1) {
            alert('Can not delete this row');
        } else {
            var $tr = $(this).closest('tr').remove();
        }
    });
    
    $('#milestone_start_date').datepicker({
        icons: {
            rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'

        },
        change: function (e) {
            datepickerRemover('#milestone_end_date');
            var startDate = new Date(e.target.value);
            $('#milestone_end_date').datepicker({
                value:'',
                minDate: startDate,
            });
        }

    });

    $('#milestone_end_date').datepicker({
        icons: {
            rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'

        },
        //   format: 'DD-MM-YYYY'

    });
    $("#create-proposal-form").submit(function(){
   
   var isFormValid = true;
   $('#create-proposal-form').find("input,textarea").each(function(){
       if(this.type=="text" || this.type=="number" || this.type=="textarea")
       {
   
   if(!$(this).val()){
       $(this).addClass("error");
       isFormValid = false;

   } else{
       $(this).removeClass("error");
   }
}
});
//   alert(isFormValid);
return isFormValid;
});
    $("#create-proposal-form").validate({
       
        onfocusout: function(element) {
            if($(element).attr("id") != "bid_start_date" && $(element).attr("id") != "bid_end_date" && $(element).attr("id") != "expected_start_date" && $(element).attr("id") != "expected_end_date" && $(element).attr("id") != "milestone_start_date" && $(element).attr("id") != "milestone_end_date"){
                $(element).valid();
            }

        },
        onkeyup: function(element) {
            if($(element).attr("id") == "bid_start_date" || $(element).attr("id") == "bid_end_date" || $(element).attr("id") == "expected_start_date" || $(element).attr("id") == "expected_end_date" || $(element).attr("id") == "milestone_start_date" || $(element).attr("id") == "milestone_end_date"){
                $(element).valid();
            }

        },
       onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        // onfocusin: function(element){
        // $(element).removeClass('error');
        // $(element).next().text('');
        // },
       rules: {
        comments: {
               required: true,
           },
        
           milestone_names: {
               required: true,

           },
           milestone_start_dates: {
               required: true,

           },
           milestone_end_dates: {
               required: true,

           },
           milestone_amounts: {
               required: true,

           },
           milestone_descriptions: {
               required: true,

           }
          
       },

       errorPlacement: function(error, element) {
           var placement = $(element).data('error');
           if (placement) {
               $(placement).html(error);
           } else if (element.attr("id") == "summernote-basic") {
               $(".comment-error").html(error);
           } 
           else if (element.attr("id") == "milestone_names") {
               $(".milestone-title-error").html(error);
           } 
           
           else if (element.attr("id") == "milestone_start_date") {
               $(".milestone-start-error").html(error);
           } 
           else if (element.attr("id") == "milestone_end_date") {
               $(".milestone-end-error").html(error);
           } 
           else if (element.attr("id") == "milestone_amount") {
               $(".milestone-amount-error").html(error);
           } 
          else if (element.attr("id") == "milestone_description") {
               $(".milestone-description-error").html(error);
           } 
        else {
               error.insertAfter(element);
           }
       },

       messages: {
           'comments': {
               required: "Project description is required.",
           },
           'milestone_names': {
               required: "Milestone name is required.",
           },


       }

   });
</script>
@endpush