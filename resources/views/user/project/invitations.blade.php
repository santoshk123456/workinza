@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Invitations')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content')
<main class="main-wrapper">
    <div class="container">
      <div class="inner-pg-layout invitaion-list">
        <h3 class="title">Invitations</h3>
        <div class="row">
            <div class="col-lg-3 left-aside left-projects-list">         
                <ul class="d-block">
                <li ><a href="{{route('project.listProposals')}}">My Proposals</a></li>
                    <li><a href="{{route('project.freelancerlistInProgressProjects')}}">Assigned / In Progress Projects</a></li>
                    <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
                    <li class="active"><a href="{{route('project.listInvitations')}}">Invitations</a></li>
                </ul>
            </div>
            <div class="col-lg-9">
                <div class="right-aside">
                    @if(!$invitations->isEmpty())    
                        <!-- freelancer-invitation-list-item:start -->
                        @foreach ($invitations as $invitation)
                            <div class="col-list-item bg-shadow mb-3">    
                                <div class="row">
                                    <div class="col-xl-6">
                                        <h4 class="">{{ucwords($invitation->project->title)}}</h4>
                                        @php
                                           $industry =  !empty($invitation->project->industries) ? implode(', ', $invitation->project->industries->pluck('name')->toArray()) : __('NA') 
                                        @endphp
                                        <span class="industry">{{$industry}}</span>
                                        <ul class="duration-list">                   
                                        <li><p class="seperator mb-2"> {{projectType($invitation->project->project_type)}} </p></li>
                                        <li><p class="seperator mb-2">{{formatAmount($invitation->project->budget_from)}} - {{formatAmount($invitation->project->budget_to)}}</p></li>                            
                                        <li><p class="mb-2">Timeline: <strong>{{getTimeline($invitation->project->timeline_count,$invitation->project->timeline_type)}}</strong> </p></li>                            
                                        </ul>    
                                    </div>
          
                                    <div class="col-xl-6">                       
                                        <ul class="posted-list">                   
                                        <li><p class="seperator mb-2">Received {{timeAgoConversion(($invitation->created_at))}}</p></li>
                                        <li><p class="mb-2">Proposal End Date : {{hcms_date(strtotime($invitation->project->bid_end_date), 'date', false)}}</p></li>                            
                                        </ul>                        
                                        <div class="btn-cols text-xl-right mb-3 ">
                                        <a class="btn bg-nm-btn mr-2" href="{{route('project.viewInvitation',$invitation->project)}}"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</a>
                                            <button class="btn bg-nm-btn mr-2" type="submit" onclick="window.location.href='{{route('project.createProposal',$invitation->project->uuid)}}'"><i class="fi-flaticon flaticon-send mr-2"></i>Accept</button>
                                            @if(App\ProjectChat::ChatFeature()==1)
                                            <button class="btn white-nm-btn" type="submit" onclick="location.href='{{ route('message.messages',array($invitation->project->uuid,$invitation->project->user->uuid)) }}'"><i class="fi-flaticon flaticon-comments mr-2"></i>Message</button>
                                         @endif
                                        </div>
                                    </div>
                                </div>                     
                                <p class="description"> @if (strlen($invitation->project->description) > 300)
                                    {{(substr($invitation->project->description, 0, 300) . '...')}}
                                @else
                                    {{$invitation->project->description}}
                                @endif</p>
                                <div class="skils-cols mb-3">
                                    @php
                                        $slctd_skills = $invitation->project->skills->pluck('name')->toArray();
                                    @endphp
                                    @foreach ($slctd_skills as $key=>$skill)
                                        <span class="notify-col">{{$skill}}</span>
                                    @endforeach
                                       
                                </div>  
                                <div class="media text-center text-md-left d-block d-md-flex">
                                    @if($invitation->project->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$invitation->project->user->profile_image)))
                                        <img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('storage/user/profile-images/'.$invitation->project->user->profile_image) }}"  alt="{{ $invitation->project->user->username }}">
                                    @else 
                                        <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $invitation->project->user->full_name }}" class="mr-md-3 thumb mb-3 mb-md-0">
                                    @endif
                                   
                                    <div class="media-body">   
                                    <?php 
                            $status ="null";
                            ?>                    
                                        <a href="{{ route('account.viewUserDetail',['user'=>$invitation->project->user,'project'=>$invitation->project,'status'=>$status]) }}"> <h5 class="mt-0 name">{{$invitation->project->user->username}} 
                                            @if($invitation->project->user->organization_type == 0 )
                                                    
                                            <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                                        @else
                                            <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                                        @endif</i></h5>     </a>
                                        <p><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>{{$invitation->project->location}}</p>
                                        <div class="ratings"><div class="rate-count">@if(!empty($invitation->project->user->userDetail->avg_rating)){{$invitation->project->user->userDetail->avg_rating}}@else 0 @endif</div><div class="user-rating" data-value="@if(!empty($invitation->project->user->userDetail->avg_rating)){{$invitation->project->user->userDetail->avg_rating}}@else 0 @endif"></div></div>
                                    </div>                                            
                                </div>  
                            </div>
                        @endforeach
                    @else
                        <div class="col-list-item bg-shadow mb-3">    
                            <div class="row">
                                {{__('No invitation found')}}
                            </div>
                        </div>
                    @endif
                </div>  
                {{-- <!--pagination:start -->
                <div class="pagination-col mt-4 mb-3">
                  <nav aria-label="page navigation">
                    <ul class="pagination justify-content-end pagination-sm flex-wrap">
                      <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fi-flaticon flaticon-angle-pointing-to-left"></i> Previous</a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item"><a class="page-link" href="#">4</a></li>
                      <li class="page-item"><a class="page-link" href="#">5</a></li>                       
                      <li class="page-item active" aria-current="page">
                        <a class="page-link" href="#">6 <span class="sr-only">(current)</span></a>
                      </li>                      
                      <li class="page-item"><a class="page-link" href="#">7</a></li>
                      <li class="page-item"><a class="page-link" href="#">8</a></li>
                      <li class="page-item"><a class="page-link" href="#">9</a></li>
                      <li class="page-item"><a class="page-link" href="#">10</a></li>
                      <li class="page-item">
                        <a class="page-link" href="#">Next <i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
                      </li>
                    </ul>
                  </nav>
                </div> --}}
                  <!--pagination:end -->
            </div>                
        </div>
      </div>
    </div>  
  </main>
@endsection





