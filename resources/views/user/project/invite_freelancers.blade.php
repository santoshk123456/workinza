@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Invite Freelancers')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')

        <!-- Header:end -->
<!--start: contact information page -->
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container inner-pg-layout project-layouts">
    <h3 class="title">{{ucwords($project->title)}}</h3>
    <div class="row mx-0">
        <div class="col-lg-3 left-aside left-projects-list">         
            <ul class="d-block">
                <li class="active"><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
                <li><a href="{{route('project.listInProgressProjects')}}">In Progress Projects</a></li>
                <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="right-aside">
              <div class="freelancer-list-col p-0">         
              <div class="bg-shadow mb-4 projects-links-menu">
                <ul>
                  <li><a href="{{ route('project.projectDetails',$project) }}"><i class="fi-flaticon flaticon-blank-page mr-2"></i>Project Details </a></li>
                  <li><a href="{{ route('project.inviteFreelancers',$project) }}" class="active"><i class="fi-flaticon flaticon-send mr-2"></i>Invite Freelancers</a></li>
                  <li><a href="{{ route('project.projectProposals',$project) }}" ><i class="fi-flaticon flaticon-auction mr-2"></i>Proposals</a></li>
                  <li><a href="{{ route('message.messages',$project) }}"><i class="fi-flaticon flaticon-comments mr-2"></i>Messages</a></li>              
                </ul>                
              </div> 
              
              <!-- freelancer-list:start -->
              <div class="proposals-list-holder bg-shadow">
                <div class="project-search-top mb-3">  
                    
                <?php
			$search = '';
			if (isset($_GET['search'])) {
                $search = htmlentities($_GET['search']);
			} else {
				$search = '';
			}
	?>


                <form action="{{route('project.searchFreelancers',$project)}}" method="GET" class="">
          <div class="input-group project-search w-100">
                    <input name="search" type="search" class="form-control border-right-0" placeholder="Search Freelancer Name" aria-label="Search Freelancer Name" value="{{ $search }}">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                    </div>
                  </div> 
                  <!-- <button class="btn white-btn filter-btn" type="submit"><span class="sprites-col filter-icon"></span> Filters</button>  -->
	</form>
  
              </div>


              @if($freelancers->count())
              @foreach($freelancers as $freelancer)
              <!-- freelancer-list-item:start -->
                  <div class="col-list-item">                    
                    <div class="media text-center text-md-left d-block d-md-flex">
                    @if($freelancer->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$freelancer->profile_image)))
<img class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail"  src="{{ asset('storage/user/profile-images/'.$freelancer->profile_image) }}" alt="{{ $freelancer->full_name }}">
            @else 
  <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $freelancer->username }}"
                        class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
         @endif
                      <!-- <img class="mr-md-3 thumb mb-3 mb-md-0" src="https://randomuser.me/api/portraits/women/63.jpg" alt="Generic placeholder image"> -->
                      <div class="media-body">
                        <div class="row">
                          <div class="col-md-8 mb-2 mb-md-0">
                            <?php 
                            $status ="invitation";
                            ?>
                          <a href="{{ route('account.viewUserDetail',['user'=>$freelancer,'project'=>$project,'status'=>$status ]) }}">   <h5 class="mt-0 name">  {{$freelancer->username}} 
                            @if($freelancer->organization_type == 0 )
                                                    
                            <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                        @else
                            <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                        @endif</i></h1>     </a>
                            <!-- <span class="industry">Banking, Gaming</span> -->

                            @php
                $industries_all = $freelancer->industries->pluck('name')->toArray();
                $industry = ucwords(implode(', ', $industries_all));
                @endphp
                
                <span class="industry">{{$industry}}</span>

                          </div>
 @if($freelancer->projectInvitation->isEmpty())
  
   <div class="col-md-4 mb-2 mb-md-0">    
                     
        <form method="POST" action="{{route('project.sendInvitationToFreelancer')}}" class="invite-form needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="invite-form" novalidate>
                    @csrf
        <input value="{{$freelancer->id}}" type="hidden" name="freelancer_id" class="form-control" id="freelancer_id" >
         <input value="{{$project->id}}" type="hidden" name="project_id" class="form-control" id="project_id" >

                            <div class="list-right-aside">
                              <button class="btn bg-nm-btn mr-2 invite "  id="invite" type="submit"><i class="fi-flaticon flaticon-send mr-2"></i>Invite</button>
                           
                          

       </form>
       @if(App\ProjectChat::ChatFeature()==1)
                              <button class="btn white-nm-btn" type="button" onclick="location.href='{{ route('message.messages',array($project->uuid,$freelancer->uuid)) }}'"><i class="fi-flaticon flaticon-comments mr-2"></i>{{__('Message')}}</button>
                              @endif
                              </div>
     </div>


  


            @else
 <div class="col-md-4 mb-2 mb-md-0">
                            <div class="list-right-aside">
                              <button class="btn bg-nm-btn mr-2 invite disable-invite-btn"  id="invite" type="submit"><i class="fi-flaticon flaticon-send mr-2"></i>Invited</button>
                              @if(App\ProjectChat::ChatFeature()==1)
                              <button class="btn white-nm-btn" onclick="location.href='{{ route('message.messages',array($project->uuid,$freelancer->uuid)) }}'" ><i class="fi-flaticon flaticon-comments mr-2"></i>{{__('Message')}}</button>
                             @endif
                            </div>
                          </div>

        @endif
                        </div>
                          <h6 class="designation"> @if(!empty($freelancer->userDetail->profile_headline)){{$freelancer->userDetail->profile_headline}}@else NA @endif</h6>
                          <p class="description"> @if(!empty($freelancer->userDetail->about_me)){{str_limit(ucwords($freelancer->userDetail->about_me),$limit = 260, $end = '...')}}@else NA @endif  </p>
                          <div class="additional-data">
                            <div class="skils-cols">
                              <!-- <span class="notify-col">Angular</span>
                              <span class="notify-col">Node.js</span>
                              <span class="notify-col">PHP</span>
                              <span class="notify-col">Pythone</span>
                              <span class="notify-col">ReactJs</span>  -->

                             
                               @foreach($freelancer->skills->pluck('name') as $skill)
                               <span class="notify-col">{{$skill}}</span>
                             @endforeach


                              </div>
                              <div class="basic-info">
                                <ul>
                                  <li><p><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>@if(!empty($freelancer->userDetail->city)){{$freelancer->userDetail->city}},@endif
                                  @if(!empty($freelancer->userDetail->state)){{$freelancer->userDetail->state}},@endif
                                  @if(!empty($freelancer->userDetail->country->name)){{$freelancer->userDetail->country->name}}@endif
                                </p></li>
                                  <!-- <li><div class="ratings"><div class="rate-count">4.9</div><i class="fi-flaticon flaticon-star filled mr-1"></i><i class="fi-flaticon flaticon-star filled mr-1"></i><i class="fi-flaticon flaticon-star filled mr-1"></i><i class="fi-flaticon flaticon-star filled mr-1"></i><i class="fi-flaticon flaticon-star  mr-1"></i></div></li> -->
                                  <li><div class="ratings">
                                  <div class="rate-count">  @if(!empty($freelancer->userDetail->avg_rating)){{$freelancer->userDetail->avg_rating}} @else 0 @endif</div>   
                                    <div class="user-rating" data-value="@if(!empty($freelancer->userDetail->avg_rating)){{$freelancer->userDetail->avg_rating}}@else 0 @endif"></div>
                                  </div></li>
                                  <li><p>Exp : <span> @if(!empty($freelancer->userDetail->years_of_experience)){{$freelancer->userDetail->years_of_experience}} {{'Yrs'}} @else {{'NA'}}@endif</span></p></li>
                                  <li><p>Member Since : <span> {{hcms_date(strtotime($freelancer->created_at),'date')}} </span></p></li>
                                 
                                </ul>
                              </div>
                          </div>                           
                      </div>  
                    </div>
                  </div>           
                <!-- freelancer-list-item:end -->
                <hr>    
                    <!-- freelancer-list-item:start -->
    @endforeach
     @else
    <h5 class="dashboard-box transactions-table-container text-center"> No freelancers to show </h5>
    @endif
    <!-- Freelancers Container / End -->
       
                <!-- freelancer-list-item:end -->
                 <!--pagination:start -->
                 @if($freelancers->count())
                 <div class="pagination-col mt-4 mb-3">
                    <nav aria-label="page navigation">
                      <ul class="pagination justify-content-end pagination-sm flex-wrap">
        
                      {{ $freelancers->links('vendor.pagination.customnew') }}

                      </ul>
                    </nav>
                  </div>
                  @endif
                   <!--pagination:end -->
              </div>
               <!-- freelancer-list:end -->  
            </div>
          </div>
        </div>
    </div>
  </div> 
</main>




<!-- Pagination -->
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12">
        <!-- Pagination -->
        <div class="pagination-container margin-top-40 margin-bottom-60">
            <nav class="pagination">
                <ul>
                    {{ $freelancers->links('vendor.pagination.default') }}
                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- Pagination / End -->


@endsection
@push('custom-scripts')
<script>
  $(document).ready(function(){
     
 
     $('.invite-form').submit(function(e) {            
         e.preventDefault();
         var $button = $(this).find('.invite');
         $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
         });
         $.ajax({
                 type: "POST",
                 url: "{{route('project.sendInvitationToFreelancer')}}",
                 data: $(this).serialize(),
                 success: function (data) { 
                    if(data['success'] == 1){
                         $button.attr('disabled', 'disabled').html('<i class="fi-flaticon flaticon-send mr-2"></i>Invited');
                         showToast('Success', "Invited successfully", 'success');
                    }else{
                       
                    }
                 },
                 beforeSend: function(){
                     $button.attr('disabled', 'disabled').html('<i class="fi-flaticon flaticon-send mr-2"></i>Inviting...');
                    
                 }
         });
     });

  });
 </script>

@endpush
