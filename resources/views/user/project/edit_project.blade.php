@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Edit Project')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush
@section('content')

<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">

        <!-- main page title:start -->
        <div class="row m-0">
            <div class="col-sm-6">
                <h3 class="title-main">{{__('Edit Project') }}</h3>
            </div>
            <div class="col-sm-6 text-sm-right mt-sm-0 mt-3">
                <button class="btn cancel-shadow-btn" onclick="window.location.href='{{route('project.listOpenProjects')}}'" type="submit"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Project</button>
            </div>
        </div>
        <!-- main page title:end -->

        <div class="row m-0 mt-4">
            <!-- right section:start -->
            <div class="col-lg-8">
                <form method="post" class="form-validate" enctype="multipart/form-data" id="edit-project-form" action="{{route('project.updateProject',$project)}}">
                    @csrf
                    @method('PUT')
                    <!-- form - project details:start -->
                    <div class="form-block bg-shadow">
                        <div class="block-header">
                            <div class="inner">
                                <h4 class="block-title">{{__('Project Details') }}</h4>
                            </div>
                        </div>
                        <div class="block-content">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="title" class="control-label">{{__('Title') }}</label>

                                        <input value="{{old('location',$project->title)}}" type="text" name="title" id="title" class="form-control" id="title" minlength="3" maxlength="150" placeholder="{{__('Enter Project Title')}}" required>
                                        <span id="title-error" class="error-txt title-error"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="location" class="control-label">{{__('Location') }}</label>
                                        <input value="{{old('location',$project->location)}}" type="text" name="location" class="form-control" id="location" minlength="3" maxlength="150" placeholder="{{__('Enter Location')}}" required>
                                        <span id="title-location" class="error-txt location-error"></span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group required">
                                        <label for="description" class="control-label">{{__('Description') }}</label>

                                        <textarea name="description" id="description" class="form-control" rows="3" required>{{old('description',$project->description)}}</textarea>
                                        <span id="description-error" class="error-txt description-error"></span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="project_documents" class="control-label">{{__('Project Documents (Upto 5 Files)') }}</label>
                                        @if(!empty($project->documents))

                                        @foreach ($project->documents as $doc)
                                        @php
                                        $ext = pathinfo($doc->file_name, PATHINFO_EXTENSION);
                                        @endphp
                                        <div class="document-col my-3">
                                            <span class="file-name"><i class="sprites-col acrobit-icon mr-2"></i>{{str_limit(ucwords($doc->file_name), $limit = 15, $end = '...')}}</span>
                                            <div class="btns-col">
                                                <button class="btn white-nm-btn view" onclick='remove_doc({{ $doc->id }},this)'><i class="fi-flaticon flaticon-bin"></i></button>
                                            </div>
                                            
                                        
                                        </div>
                                       
                                        @endforeach
                                        @endif
                                        <div class="fallback dropzone dropzone_docs"></div>
                                        <div class="imp_images"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- form - project details:end -->

                    <!-- form - Required Expertise Details:start -->
                    <div class="form-block bg-shadow mt-4">
                        <div class="block-header">
                            <div class="inner">
                                <h4 class="block-title">{{__('Required Expertise Details') }}</h4>
                            </div>
                        </div>
                        <div class="block-content">
                            <!-- <form> -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="title" class="control-label">{{__('Industry') }}</label>
                                        <select name="industry_ids[]" id="industry_ids" multiple="multiple" class="form-control" >

                                            @foreach ($industries as $industry)
                                            <option value="{{$industry->id}}" {{in_array($industry->id,old('industry_ids',$slctd_industries)) ? 'selected' : ''}}>{{$industry->name}}</option>
                                            @endforeach
                                        </select>
                                        <span id="industries-error" class="error-txt industries-error"></span>
                                    </div>
                                </div>
                              
                                @php
                                if(!empty(old('categories_ids'))){
                                $old_category = old('categories_ids');
                                $old_sub_categories = App\SubCategory::whereHas('categories',function($q) use($old_category){
                                $q->whereIn('id',$old_category);
                                })->where(['active'=>1])->pluck('name','id');
                                }else{
                                $old_sub_categories = [];
                                }
                                @endphp
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="title" class="control-label">{{__('Category')}}</label>
                                       
                                        <select name="categories_ids[]" id="category_ids" class="form-control" multiple="multiple" >

                                            <option value=""></option>
                                           
                                            @foreach ($categories_list as $category)
                                                <option value="{{$category->id}}" {{in_array($category->id,$slctd_categories) ? 'selected' : ''}}>{{$category->name}}</option>
                                            @endforeach
                                           
                                        </select>
                                        <span id="categories-error" class="error-txt categories-error"></span>
                                    </div>
                                </div>
                                @php
                                if(!empty(old('categories_ids'))){
                                $old_category = old('categories_ids');
                                $old_sub_categories = App\SubCategory::whereHas('categories',function($q) use($old_category){
                                $q->whereIn('id',$old_category);
                                })->where(['active'=>1])->pluck('name','id');
                                }else{
                                $old_sub_categories = [];
                                }
                                @endphp
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="title" class="control-label">{{__('Sub Category') }}</label>
                                        <select name="sub_categories_ids[]" id="sub_category_ids" class="form-control" multiple="multiple" >

                                            @if(!empty($old_sub_categories))
                                            @foreach ($old_sub_categories as $key=>$sub_category)
                                            <option value="{{$key}}" {{!empty(old('sub_categories_ids')) ? in_array($key,old('sub_categories_ids')) ? 'selected' : '' : ''}}>{{$sub_category}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($sub_categories as $sub_category)
                                            <option value="{{$sub_category->id}}" {{in_array($sub_category->id,$slctd_sub_categories) ? 'selected' : ''}}>{{$sub_category->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span id="sub-categories-error" class="error-txt sub-categories-error"></span>
                                    </div>
                                </div>
                                @php
                                if(!empty(old('sub_categories_ids'))){
                                $old_sub_category = old('sub_categories_ids');
                                $old_skills = App\Skill::whereHas('subCategories',function($q) use($old_sub_category){
                                $q->whereIn('id',$old_sub_category);
                                })->where(['active'=>1])->pluck('name','id');
                                }else{
                                $old_skills = App\Skill::where(['active'=>1])->pluck('name','id');
                                }
                                @endphp
                              
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label for="title" class="control-label">{{__('Skills') }}</label>
                                        <select name="skills[]" id="skill_ids" class="form-control" multiple="multiple" >
                                            @if(!empty($old_skills))
                                            @foreach ($old_skills as $key=>$skill)
                                            <option value="{{$key}}" {{!empty(old('skills')) ? in_array($key,old('skills')) ? 'selected' : '' : ''}}>{{$skill}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($skills as $skill)
                                            <option value="{{$skill->id}}" {{in_array($skill->id,$slctd_skills) ? 'selected' : ''}}>{{$skill->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span id="skills-error" class="error-txt skills-error"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- form - Required Expertise Details:end -->



                    <!-- form - Proposal Details:start -->
                    <div class="form-block bg-shadow mt-4">
                        <div class="block-header">
                            <div class="inner">
                                <h4 class="block-title">{{__('Proposal Details') }}</h4>
                            </div>
                        </div>
                        <div class="block-content">
                            <!-- <form> -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="title" class="w-100">{{__('Project Type') }}</label>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="type" id="type_fixed_rate" value="0" {{old('type',$project->type)==0?'checked':''}} required>
                                            <label class="form-check-label" for="type_fixed_rate">{{__('Fixed rate') }}</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="type" id="type_hourly_rate" value="1" {{old('type',$project->type)==1?'checked':''}} required>
                                            <label class="form-check-label" for="type_hourly_rate">{{__('Hourly rate') }}</label>
                                        </div>




                                    </div>
                                </div>

         

                                <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="title" class="control-label">{{__('Budget') }}</label>
                                            <select class="form-control" name="budget_range" id="budget_range" required>
                                              <option disabled selected value=""> Select Budget</option>
                                              <option value="0-100000" {{old('timeline_type',$project->budget_from)=="0" ? 'selected' : ''}}>{{__('Below USD 100000') }}</option>
                                                <option value="100000-200000" {{old('timeline_type',$project->budget_from)=="100000" ? 'selected' : ''}}>{{__('USD 100000 - USD 200000') }}</option>
                                                <option value="200000-400000" {{old('timeline_type',$project->budget_from)=="200000" ? 'selected' : ''}}>{{__('USD 200000 - USD 400000') }}</option>
                                                <option value="400000-600000" {{old('timeline_type',$project->budget_from)=="400000" ? 'selected' : ''}}>{{__('USD 400000 - USD 600000') }}</option>
                                                <option value="600000-1000000" {{old('timeline_type',$project->budget_from)=="600000" ? 'selected' : ''}}>{{__('USD 600000 - USD 1000000') }}</option>
                                                <option value="1000000-2000000" {{old('timeline_type',$project->budget_from)=="1000000" ? 'selected' : ''}}>{{__('USD 1000000 - USD 2000000') }}</option>
                                                <option value="2000000-4000000" {{old('timeline_type',$project->budget_from)=="2000000" ? 'selected' : ''}} >{{__('USD 2000000 - USD 4000000') }}</option>
                                                <option value="4000000-100000000" {{old('timeline_type',$project->budget_from)=="4000000" ? 'selected' : ''}}>{{__('Above USD 4000000') }}</option>
                                              {{-- <option value="0-5000" {{old('timeline_type',$project->budget_from)=="0" ? 'selected' : ''}} >{{__('Below 5000') }}</option>
                                                <option value="5000-10000" {{old('timeline_type',$project->budget_from)=="5000" ? 'selected' : ''}} >{{__('5000 - 10000') }}</option>
                                                <option value="10000-20000" {{old('timeline_type',$project->budget_from)=="10000" ? 'selected' : ''}}>{{__('10000 - 20000') }}</option>
                                                <option value="20000-50000" {{old('timeline_type',$project->budget_from)=="20000" ? 'selected' : ''}}>{{__('20000 - 50000') }}</option> --}}
                                            </select>
             
                                            <span id="errorbudget" class="error-txt budget-from-error"></span>
                                        </div>
                                    </div>



<!-- 
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="title" class="control-label">{{__('Budget From') }}</label>

                                        <input value="{{old('budget_from',$project->budget_from)}}" aria-describedby="datepicker" type="number" name="budget_from" class="form-control no-inc numericOnly" id="budget_from" min="1" max="9999999999" placeholder="{{__('Enter Budget From Amount')}}" required>
                                        <span id="budget-from-error" class="error-txt budget-from-error"></span>
                                    </div>
                                </div> -->

                                <!-- <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="title" class="control-label">{{__('Budget To') }} </label>

                                        <input value="{{old('budget_to',$project->budget_to)}}" type="number" name="budget_to" class="form-control no-inc numericOnly" id="budget_to" min="1" max="9999999999" placeholder="{{__('Enter Budget To Amount')}}" required>
                                        <span id="budget-to-error" class="error-txt budget-to-error"></span>
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="project-start-date" class="control-label">{{__('Proposal Start Date') }}</label>

                                        <input type="text" value="{{old('bid_start_date',date('m/d/Y',strtotime($project->bid_start_date)))}}" name="bid_start_date" class="form-control common-date-pick" required autocomplete="off" id="bid_start_date" placeholder="{{ __('Select Date') }}">
                                        <span id="bid-start-error" class="error-txt bid-start-error"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="project-end-date" class="control-label">{{__('Proposal End Date') }}</label>

                                        <input type="text" value="{{old('bid_end_date',date('m/d/Y',strtotime($project->bid_end_date)))}}" name="bid_end_date" class="form-control common-date-pick" required autocomplete="off" id="bid_end_date" placeholder="{{ __('Select Date') }}">
                                        <span id="bid-end-error" class="error-txt bid-end-error"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="title" class="control-label">{{__('Project Start Date') }}</label>
                                        <input type="text" value="{{old('expected_start_date',date('m/d/Y',strtotime($project->expected_start_date)))}}" name="expected_start_date" class="form-control" required autocomplete="off" id="expected_start_date" placeholder="{{ __('Select Date') }}">
                                        <span id="expected-start-error" class="error-txt expected-start-error"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="title" class="control-label">{{__('Project End Date') }} </label>

                                        <input type="text" value="{{old('expected_end_date',date('m/d/Y',strtotime($project->expected_end_date)))}}" name="expected_end_date" class="form-control common-date-pick" required autocomplete="off" id="expected_end_date" placeholder="{{ __('Select Date') }}">
                                        <span id="expected-end-error" class="error-txt expected-end-error"></span>
                                    </div>
                                </div>
                          

                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="time-line" class="control-label">{{__('Timeline')}}</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input value="{{old('timeline_count',$project->timeline_count)}}" type="number" name="timeline_count" class="form-control no-inc numericOnly" id="timeline_count" min="1" max="9999" placeholder="{{__('Count of hour, days, week or months')}}" required>
                                                <span id="timeline-count-error" class="error-txt timeline-count-error"></span>

                                            </div>
                                            <div class="col-md-6">

                                                <select name="timeline_type" class="form-control mb-2" id="timeline_type" required>
                                                    <option value="">{{__('Timeline Type') }}</option>
                                                    <option value="Hours" {{old('timeline_type',$project->timeline_type)=="Hours" ? 'selected' : ''}}>{{__('Hours') }}</option>
                                                    <option value="Days" {{old('timeline_type',$project->timeline_type)=="Days" ? 'selected' : ''}}>{{__('Days') }}</option>
                                                    <option value="Weeks" {{old('timeline_type',$project->timeline_type)=="Weeks" ? 'selected' : ''}}>{{__('Weeks') }}</option>
                                                    <option value="Months" {{old('timeline_type',$project->timeline_type)=="Months" ? 'selected' : ''}}>{{__('Months') }}</option>
                                                </select>
                                                <span id="timeline-type-error" class="error-txt timeline-type-error"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                    <!-- form - Proposal Details:end -->



                    <div class="form-block bg-shadow mt-4">
                        <div class="block-header">
                            <div class="inner">
                                <h4 class="block-title">{{__('Milestone Details') }}</h4>
                            </div>
                        </div>
                        <div class="block-content">





                            <!-- milestone input:start -->
                            <div class="milestone-box">
                                <!-- <form> -->

                                <table class="table">
                                    <tbody>

                                        @if(!empty(old('milestone_names')))
                                        @foreach(old('milestone_names') as $key=>$milestone)
                                        <tr>
                                            <input type="hidden" name="milestone_id[]" value="{{old('milestone_id')[$key]}}">
                                            <td scope="row" class="border-0">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="title" class="control-label">{{__('Title') }}</label>

                                                            <input type="text" name="milestone_names[]" class="form-control milestone_names" value="{{$milestone}}" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Eg:Milestone1')}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="" class="control-label">{{__('Start Date') }}</label>

                                                            <input type="text" name="milestone_start_dates[]" class="form-control milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Start Date') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="" class="control-label">{{__('End Date') }}</label>

                                                            <input type="text" name="milestone_end_dates[]" class="form-control milestone_end_date common-date-pick" value="{{old('milestone_end_dates')[$key]}}" required autocomplete="off" placeholder="{{ __('Select End Date') }}" id="milestone_end">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="" class="control-label">{{__('Amount') }}</label>

                                                            <input type="number" name="milestone_amounts[]" class="form-control no-inc numericOnly" value="{{old('milestone_amounts')[$key]}}" min="1" max="9999999999" placeholder="{{__('Enter the Amount')}}" required>
                                                            <span  class="errorhtml"></span> 
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group required">
                                                            <label for="description" class="control-label">{{__('Description')}}</label>

                                                            <textarea name="milestone_descriptions[]" id="milestone_descriptions" class="form-control" placeholder="Milestone Description" required>{{old('milestone_descriptions')[$key]}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="border-0 text-right">
                                                <button class="btn btn-success add_more mr-2" type="button"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Milestone</button><button type="button" class="btn ml-2 remove_row white-nm-btn white-nm-btn border">Remove Milestone</button>
                                            </td>
                                        </tr>

                                        @endforeach
                                        @else
                                        @php $i=1;@endphp
                                        @foreach ($project->milestones as $milestone)
            
                                        <tr>
                                            <input type="hidden" name="milestone_id[]" value="{{$milestone->id}}">
                                            <td scope="row" class="border-0">
                                                <div class="row milestone">
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="title" class="control-label">{{__('Milestone') }}</label>

                                                            <input type="text" value="{{$milestone->milestone_name}}" name="milestone_names[]" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Eg:Milestone1')}}" required>
                                                            <!-- <span  id="milestone-title-error" class="error-txt milestone-title-error"></span> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="project-start-date" class="control-label">{{__('Start Date') }}</label>
                                                            <!-- <input type="text" name="milestone_names[]" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Name')}}" required> -->
                                                            <input type="text" value="{{date('m/d/Y',strtotime($milestone->start_date))}}" name="milestone_start_dates[]" class="form-control no-type milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Start Date') }}" data-id="{{$i}}" id="milestone_start_date{{$i}}">
                                                            <!-- <span  class="error-txt milestone_start_date"></span>  -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="project-end-date" class="control-label">{{__('End Date') }}</label>

                                                            <input type="text" value="{{date('m/d/Y',strtotime($milestone->end_date))}}" name="milestone_end_dates[]" class="form-control no-type milestone_end_date" required autocomplete="off" placeholder="{{ __('Select End Date') }}" id="milestone_end_date{{$i}}">
                                                            <!-- <span  class="error-txt milestone-end-error"></span> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <label for="amount" class="control-label">{{__('Amount') }}</label>

                                                            <input type="number" value="{{$milestone->milestone_amount}}" name="milestone_amounts[]" class="form-control no-inc numericOnly milestone_amount" min="1" max="9999999999" placeholder="{{__('Enter the Amount')}}" required id="milestone_amount">
                                                            <span  class="errorhtml"></span> 
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="form-group required">
                                                            <label for="description" class="control-label">{{__('Description') }}</label>

                                                            <textarea name="milestone_descriptions[]" class="form-control milestone_description" placeholder="Milestone Description" required id="milestone_description">{{$milestone->milestone_description}}</textarea>
                                                            <!-- <span  id="milestone-description-error"class="error-txt milestone-description-error"></span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="remove-buttons border-0 text-right">
                                            <button @if(!$loop->last) style="display:none;" @endif class="btn bg-nm-btn add_more mr-2" type="button"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Milestone</button>
                                            <button type="button" class="btn remove_row white-nm-btn border">Remove Milestone</button>
                                            </td>

                                        </tr>
                                        @php $i++;@endphp
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>




                            </div>
                            <!-- milestone input:end -->
                            <!-- <div class="row mt-4 mb-3">
                                <div class="col-12 text-right">
                                    <button class="btn bg-nm-btn add_more" type="button">{{__('Add Milestone') }}</button>
                                </div>
                            </div> -->

                        </div>
                    </div>
                    <!-- form - Required Expertise Details:end -->
                    <div class="row final-submit">
                        <div class="col-12 text-center">
                            <button class="btn bg-nm-btn mt-4 mb-4" id="project-submit" type="submit">{{__('Submit Project') }}</button>
                        </div>
                    </div>

                </form>
            </div>
            <!-- right section:start -->


            <!-- left section:start -->
            <div class="col-lg-4">

                <!-- benifits:start -->
                {{-- @if((count(Auth()->user()->userSubscription)!=0))
                <div class="form-block bg-shadow">
                    <div class="block-header">
                        <div class="inner">
                            <h4 class="block-title">{{__('Benefits of This Project') }}</h4>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="benifit-list">
                            <p><i class="fi-flaticon flaticon-tick-sign mr-2"></i>Maximum Number of Projects Post: <span>{{$user_subscription->remaining_project_count}}/{{$user_subscription->membership->number_of_projects}}</span></p>
                            <p><i class="fi-flaticon flaticon-tick-sign mr-2"></i>Maximum Number of Proposal a Project Can Receive : <span>{{$user_subscription->remaining_proposal_count}}/{{$user_subscription->membership->number_of_proposals}}</span></p>
                            <p><i class="fi-flaticon flaticon-tick-sign mr-2"></i>Chats: <span>@if($user_subscription->membership->chat_feature==1) {{__('Yes')}} @else {{__('No')}} @endif</span></p>
                        </div>
                    </div>
                </div>
                @endif --}}
                <!-- benifits:end -->


                <!-- contact:start -->
                <div class="form-block bg-shadow ">
                    <div class="block-header">
                        <div class="inner">
                            <h4 class="block-title">{{__('Contact Details') }}</h4>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="contact-list">
                            <p><a href="tel:{{getSettingValue('contact_number')}}"><i class="fi-flaticon flaticon-phone-call mr-2"></i>@if(!empty(App\Setting::getSettingValue('contact_number'))) {{App\Setting::getSettingValue('contact_number')}} @else NA @endif</a></p>
                            <p><a href="mailto:{{getSettingValue('application_admin_email')}} "><i class="fi-flaticon flaticon-email mr-2"></i>@if(!empty(App\Setting::getSettingValue('application_admin_email'))) {{App\Setting::getSettingValue('application_admin_email')}} @else NA @endif </a></p>
                        </div>
                    </div>
                </div>
                <!-- contact:end -->

            </div>
            <!-- left section:start -->
        </div>
    </div>
</main>

@endsection
@push('custom-scripts')
<script>
   
    $(document).on("blur", '.milestone_amount', function() {
    
	// for validating milestone amount with budget amount

	var inputs = $(".milestone_amount");
	var total_amount = 0;

	for (var i = 0; i < inputs.length; i++) {
		total_amount = parseFloat(total_amount) + parseFloat($(inputs[i]).val());
	}
	

	$('#total_amount').val(total_amount);
	var budgets = $('#budget_range').find(":selected").val();
	var budget = budgets.split('-');
	if(budget[1] < total_amount){
		
		var test = document.getElementsByClassName('errorhtml');
		document.getElementsByClassName('errorhtml')[test.length-1].innerHTML = "<span style='color: #fe4848; font-size:13px;'>"+ "Amount is greater than budget </span>"
		//error.innerHTML = "<span style='color: #fe4848; font-size:13px;'>"+ "Amount is greater than budget </span>"
        $('#project-submit').attr('disabled', 'disabled');
	}else{
        $('#project-submit').removeAttr("disabled");
        var test = document.getElementsByClassName('errorhtml');
		document.getElementsByClassName('errorhtml')[test.length-1].innerHTML = ""
    }

	// for reverse validating of budget amount with total milestone amount

	$( "#budget_range" ).on('change', function() {
      
		if(budget[1] < total_amount){

	errorbudget.innerHTML = "<span style='color: #fe4848; font-size:13px;'>"+ 
					"Budget is less than total amount of milestones </span>"
                    $('#project-submit').attr('disabled', 'disabled');
	}else{
		$('#errorbudget').remove();
        $('#project-submit').removeAttr("disabled");
	}
	});


});
    $(document).ready(function(){
        $( "#budget_range" ).on('change', function() {
            var budgets = $(this).find(":selected").val();
            var budget = budgets.split('-');
            var inputs = $(".milestone_amount");
	        var total_amount = 0;

            for (var i = 0; i < inputs.length; i++) {
                total_amount = parseFloat(total_amount) + parseFloat($(inputs[i]).val());
            }
	

	        $('#total_amount').val(total_amount);
            if(budget[1] < total_amount){

            errorbudget.innerHTML = "<span style='color: #fe4848; font-size:13px;'>"+ 
                "Budget is less than total amount of milestones </span>"

            }else{
                $('#errorbudget').remove();
            }
        });
        // $('.milestone-box table tr:last').find('.remove_row').hide();
        $('.milestone-box table tr').not('.milestone-box table tr:last').find('.remove_row').show();
    });
    $('#industry_ids').select2({
        placeholder: "{{__('Select Industries')}}",
    });
    $('#category_ids').select2({
        placeholder: "{{__('Select Categories')}}",
    });
    $('#sub_category_ids').select2({
        placeholder: "{{__('Select Sub Categories')}}",
    });
    $('#skill_ids').select2({
        placeholder: "{{__('Select Skills')}}",
    });
    function datepickerRemover(input){
            $(input).parent().children('i').remove();
            $(input).removeClass('gj-textbox-md');
            $(input).removeAttr('data-type');
            $(input).removeAttr('data-guid');
            $(input).removeAttr('data-datepicker');
            $(input).parent().parent().append($(input).parent().html());
            $(input).parent().parent().children('div').remove();
        }

        var datepicker2 = $('#bid_end_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            minDate: new Date(),
            // format: 'dd-mm-yyyy' ,
            change: function (e) {
                var endDateVal1 = $('#expected_start_date').datepicker().value();
                        var endDate1 = new Date(endDateVal1);
                        var endDateVal2 = $('#expected_end_date').datepicker().value();
                        var endDate2 = new Date(endDateVal2);
                        datepickerRemover('#expected_start_date');
                        datepickerRemover('#expected_end_date');
                        // var selected_val = $('#bid_start_date').datepicker().value();
                        // console.log(selected_val);
                        var startDate = new Date(e.target.value);
                        startDate.setDate(startDate.getDate() + 1);
                        if(startDate > endDate1)
                        {
                            var newVal1= '';
                        }
                        else{
                            var newVal1= endDateVal1;
                        }
                        if(startDate > endDate2)
                        {
                            var newVal2= '';
                        }
                        else{
                            var newVal2= endDateVal2;
                        }
                        $('#expected_end_date').datepicker({
                            minDate: startDate,
                            value: newVal2,
                            // format: 'dd-mm-yyyy' 
                        });
                        $('#expected_start_date').datepicker({
                            minDate: startDate,
                            value: newVal1,
                            // format: 'dd-mm-yyyy' ,
                            change: function (e) {
                                datepickerRemover('#milestone_start_date1');
                                datepickerRemover('#milestone_end_date1');
                                datepickerRemover('#expected_end_date');
                                var startDate = new Date(e.target.value);
                                $('#milestone_start_date1').datepicker({
                                    minDate: startDate,
                                    // format: 'dd-mm-yyyy' 
                                });
                                $('#milestone_end_date1').datepicker({
                                    minDate: startDate,
                                    // format: 'dd-mm-yyyy' 
                                });
                                $('#expected_end_date').datepicker({
                                    minDate: startDate,
                                    value: newVal2,
                                    // format: 'dd-mm-yyyy' 
                                });
                            }
                        });
                    }
           
        });
        $('#bid_start_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            minDate: new Date(),
            // format: 'dd-mm-yyyy' ,
            change: function (e) {
                var endDateVal = $('#bid_end_date').datepicker().value();
                var endDate = new Date(endDateVal);
                datepickerRemover('#bid_end_date');
                var startDate = new Date(e.target.value);
                var selected_val = $('#bid_start_date').datepicker().value();
                var startDate = new Date(selected_val);
                if(startDate > endDate)
                {
                    var newVal= '';
                }
                else{
                    var newVal= endDateVal;
                }
                $('#bid_end_date').datepicker({
                    minDate: startDate,
                    value: newVal,
                    // format: 'dd-mm-yyyy' ,
                    change: function (e) {
                        var endDateVal1 = $('#expected_start_date').datepicker().value();
                        var endDate1 = new Date(endDateVal1);
                        var endDateVal2 = $('#expected_end_date').datepicker().value();
                        var endDate2 = new Date(endDateVal2);
                        datepickerRemover('#expected_start_date');
                        datepickerRemover('#expected_end_date');
                        // var selected_val = $('#bid_start_date').datepicker().value();
                        // console.log(selected_val);
                        var startDate = new Date(e.target.value);
                        startDate.setDate(startDate.getDate() + 1);
                        if(startDate > endDate1)
                        {
                            var newVal1= '';
                        }
                        else{
                            var newVal1= endDateVal1;
                        }
                        if(startDate > endDate2)
                        {
                            var newVal2= '';
                        }
                        else{
                            var newVal2= endDateVal2;
                        }
                        $('#expected_end_date').datepicker({
                            minDate: startDate,
                            value: newVal2,
                            // format: 'dd-mm-yyyy' 
                        });
                        $('#expected_start_date').datepicker({
                            minDate: startDate,
                            value: newVal1,
                            // format: 'dd-mm-yyyy' ,
                            change: function (e) {
                                datepickerRemover('#milestone_start_date1');
                                datepickerRemover('#milestone_end_date1');
                                datepickerRemover('#expected_end_date');
                                var startDate = new Date(e.target.value);
                                $('#milestone_start_date1').datepicker({
                                    minDate: startDate,
                                    // format: 'dd-mm-yyyy' 
                                });
                                $('#milestone_end_date1').datepicker({
                                    minDate: startDate,
                                    // format: 'dd-mm-yyyy' 
                                });
                                $('#expected_end_date').datepicker({
                                    minDate: startDate,
                                    value: newVal2,
                                    // format: 'dd-mm-yyyy' 
                                });
                            }
                        });
                    }
                });
            }
        });

        $('#expected_start_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            // format: 'dd-mm-yyyy' ,
            change: function (e) {
                var mileStart = $('#milestone_start_date1').datepicker().value();
                var mileDate = new Date(mileStart);
                var mileEnd = $('#milestone_end_date1').datepicker().value();
                var mileEndDate = new Date(mileEnd);
                var endDateVal2 = $('#expected_end_date').datepicker().value();
                var endDate2 = new Date(endDateVal2);
                var startDate = new Date(e.target.value);
                startDate.setDate(startDate.getDate() + 1);
                
                if(startDate > endDate2)
                {
                    var newVal2= '';
                }
                else{
                    var newVal2= endDateVal2;
                }
                if(startDate > mileDate)
                {
                    var mileStartVal= '';
                }
                else{
                    var mileStartVal= mileStart;
                }
                if(startDate > mileEndDate)
                {
                    var mileEndVal= '';
                }
                else{
                    var mileEndVal= mileEnd;
                }
                datepickerRemover('#milestone_start_date1');
                datepickerRemover('#milestone_end_date1');
                datepickerRemover('#expected_end_date');
                var startDate = new Date(e.target.value);
                $('#milestone_start_date1').datepicker({
                    minDate: startDate,
                    value: mileStartVal,
                    // format: 'dd-mm-yyyy' 
                });
                $('#milestone_end_date1').datepicker({
                    minDate: startDate,
                    value: mileEndVal,
                    // format: 'dd-mm-yyyy' 
                });
                $('#expected_end_date').datepicker({
                    minDate: startDate,
                    value: newVal2,
                    // format: 'dd-mm-yyyy' 
                });
            }

        });

        $('#expected_end_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            // format: 'dd-mm-yyyy' 
        });
       
        function changeDate(input,startDate,valCount){
            console.log(input);
            var mileStart = $(input).datepicker().value();
            var mileDate = new Date(mileStart);
            startDate.setDate(startDate.getDate() + 1);
            if(startDate > mileDate)
            {
                var newVal2= '';
            }
            else{
                var newVal2= mileStart;
            }
            datepickerRemover(input);
            $(input).datepicker({
                value:newVal2,
                minDate: startDate,
                change: function (e) {
                    // changeDate('#milestone_end_date'+valCount,startDate,valCount);
                    var count_val = $(this).data('id');
                    var mileStart = $('#milestone_end_date'+count_val).datepicker().value();
                    var mileDate = new Date(mileStart);
                    var startDate1 = new Date(e.target.value);
                    startDate1.setDate(startDate1.getDate() + 1);
                    if(startDate1 > mileDate)
                    {
                        var newVal3= '';
                    }
                    else{
                        var newVal3= mileStart;
                    }
                    datepickerRemover('#milestone_end_date'+valCount);
                    $('#milestone_end_date'+valCount).datepicker({
                        value:newVal3,
                        minDate: startDate1,
                        
                    });
                    if(valCount < $('.milestone_start_date').length){
                        changeDate('#milestone_start_date'+(valCount+1),startDate,valCount+1);
                    }
                }
                
            });
        }
        for (i = 1; i <= $('.milestone_start_date').length; i++) {
             $("#milestone_start_date"+i).datepicker({
                icons: {
                    rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
                },
                change: function (e) {
                    var count_val = $(this).data('id');
                    var mileStart = $('#milestone_end_date'+count_val).datepicker().value();
                    var mileDate = new Date(mileStart);
                    var startDate = new Date(e.target.value);
                    startDate.setDate(startDate.getDate() + 1);
                    if(startDate > mileDate)
                    {
                        var newVal2= '';
                    }
                    else{
                        var newVal2= mileStart;
                    }
                    datepickerRemover('#milestone_end_date'+count_val);
                    var startDate = new Date(e.target.value);
                    $('#milestone_end_date'+count_val).datepicker({
                        value:newVal2,
                        minDate: startDate,
                        
                    });
                    if(count_val < $('.milestone_start_date').length){
                        changeDate('#milestone_start_date'+(count_val+1),startDate,count_val+1);
                    }
                }
            });
            
            $("#milestone_end_date"+i).datepicker({
                icons: {
                    rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
                },
                // format: 'dd-mm-yyyy' 
            });
        }
        
    $('#category_ids').change(function() {
        var sub_category_ids = $('#sub_category_ids').val();
        $.ajax({
            type: "GET",
            url: "{{url('/get-sub-categories')}}",
            data: {
                'category_ids': $(this).val()
            },
            success: function(response) {
                var new_options = "<option value =''></option>";
                $.each(response, function(index, value) {
                    if (jQuery.inArray(index, sub_category_ids) !== -1) {
                        new_options += "<option value='" + index + "' selected>" + value + "</option>";
                    } else {
                        new_options += "<option value='" + index + "'>" + value + "</option>";
                    }
                });
                $('#sub_category_ids').html(new_options);
                $('#sub_category_ids').change();
                $('#sub_category_ids').select2({
                    placeholder: "{{__('Select Sub Categories')}}",
                });
            }
        });
    });
    $('#sub_category_ids').change(function() {
        var skill_ids = $('#skill_ids').val();
        $.ajax({
            type: "GET",
            url: "{{url('/get-skills')}}",
            data: {
                'sub_category_ids': $(this).val()
            },
            success: function(response) {
                var new_options = "<option value =''></option>";
                $.each(response, function(index, value) {
                    if (jQuery.inArray(index, skill_ids) !== -1) {
                        new_options += "<option value='" + index + "' selected>" + value + "</option>";
                    } else {
                        new_options += "<option value='" + index + "'>" + value + "</option>";
                    }
                });
                $('#skill_ids').html(new_options);
                $('#skill_ids').select2({
                    placeholder: "{{__('Select Sub Categories')}}",
                });
            }
        });
    });
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone(".dropzone_docs", {
        url: "{{route('project.fileUpload') }}",
        maxFilesize: 5, // MB
        maxFiles: 5,
        dictDefaultMessage: "File size should be less than 5 MB",
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        success: function(file, response) {
            $('#dropzone-docs-error').remove();
            $('<input>').attr({
                type: 'hidden',
                name: 'project_documents[]',
                value: response.name,
                id: response.original_name,
                class: 'project_documents'
            }).appendTo('.imp_images');
        },
        removedfile: function(file) {
            $('input[name="project_documents[]"]').each(function() {
                if ($(this).attr('id') == file.name) {
                    $(this).remove();
                }
            });
            file.previewElement.remove();
        }
    });




    $(document).on("click", '.add_more', function() {
        var $tr = $(this).closest('tr');
        var $clone = $tr.clone();
        $clone.find('input').val('');
        $clone.find('textarea').val('');
      
        var count = $('.milestone_start_date').length;
        var start = $('.milestone_start_date').length;
        var end   = $('.milestone_end_date').length;
        var new_count = count+1;

        $clone.find('input.milestone_start_date')
            .attr('id', 'milestone_start_date' + new_count); //newly added line
        $clone.find('input.milestone_end_date')
            .attr('id', 'milestone_end_date' + new_count); //newly added line
        ///////////new/////////////

        $clone.find('input').prop('required', false);
        $clone.find('textarea').prop('required', false);



        $clone.find('input.milestone_names')
            .attr('id', 'dd_title' + count).prop('required', true);
        $clone.find('input.milestone_amount')
            .attr('id', 'dd_amount' + count).prop('required', true);
        $clone.find('textarea.milestone_description')
            .attr('id', 'dd_description' + count).prop('required', true);
        /////////////end////////////

        $clone.find('input.milestone_start_date').
        removeAttr('data-guid').removeAttr('data-datepicker');

        $clone.find('input.milestone_end_date').
        removeAttr('data-guid').removeAttr('data-datepicker');

        $tr.after($clone);

        if(start <= 1){
            var last_val = $('#milestone_end_date1').datepicker().value();
        }
        else{
            new_start = start-1;
            console.log(new_start+'  '+start);
            var last_val = $('#milestone_end_date'+start).datepicker().value();
        }
        var startDate = new Date(last_val);
        startDate.setDate(startDate.getDate() + 1);
        $(this).hide();
        // $('.milestone-box table tr:last').find('.remove_row').hide();
        $('.milestone-box table tr').not('.milestone-box table tr:last').find('.remove_row').show();
        // var new_count = count+1;
        $('#milestone_start_date' + new_count).datepicker(
            {
                minDate: startDate,
                value:'',
                icons: {
                    rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
                }, 
                change: function (e) {
                    console.log(new_count);
                    datepickerRemover('#milestone_end_date' + new_count);
                    var startDate = new Date(e.target.value);
                    $('#milestone_end_date' + new_count).datepicker({
                        value:'',
                        minDate: startDate,
                    });
                }
            
            }
        );
        $('#milestone_end_date' + new_count).datepicker(   {
            // format: 'dd-mm-yyyy' 
                minDate: startDate,
                value:'',
                icons: {
                    rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
                },                  
            });


    });



    $(document).on("click", '.remove_row', function() {
        if($('.remove_row').length==2)
  {
    $('.remove_row').hide();
  }
        if ($('.milestone_names').length == 1) {
            alert('Can not delete this row');
        } else {
            var $tr = $(this).closest('tr').remove();
        }
        $('.milestone-box table tr:last').find('.add_more').show();
    });


    $("#edit-project-form").submit(function() {

        var isFormValid = true;
        $('#edit-project-form').find("input,textarea").each(function() {
            if (this.type == "text" || this.type == "number" || this.type == "textarea") {
                //  console.log(this.type);
                if (!$(this).val()) {
                    $(this).addClass("error");
                    isFormValid = false;

                } else {
                    $(this).removeClass("error");
                }
            }
        });

        // if ($('.project_documents').length == 0 || $('.milestone_names').length == 0) {
        //     e.preventDefault();
        //     if ($('.project_documents').length == 0) {
        //         $('.dropzone_docs').after('<div class="error-msg" id="dropzone-docs-error">Please add atleast one document</div>')
        //     }
        //     isFormValid = false;
        // }
        //   alert(isFormValid);
        return isFormValid;
    });
    $("#edit-project-form").validate({
       
        onfocusout: function(element) {
            if($(element).attr("id") != "bid_start_date" && $(element).attr("id") != "bid_end_date" && $(element).attr("id") != "expected_start_date" && $(element).attr("id") != "expected_end_date" && $(element).attr("id") != "milestone_start_date" && $(element).attr("id") != "milestone_end_date"){
                $(element).valid();
            }

        },
        onkeyup: function(element) {
            if($(element).attr("id") == "bid_start_date" || $(element).attr("id") == "bid_end_date" || $(element).attr("id") == "expected_start_date" || $(element).attr("id") == "expected_end_date" || $(element).attr("id") == "milestone_start_date" || $(element).attr("id") == "milestone_end_date"){
                $(element).valid();
            }

        },
       onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        // onfocusin: function(element){
        // $(element).removeClass('error');
        // $(element).next().text('');
        // },
       rules: {
           title: {
               required: true,
           },
           location: {
               required: true,
               maxlength: 300,
           },
           description: {
               required: false,
              
           },
           industry_ids: {
               required: false,
               maxlength: 500,
           },
           categories_ids: {
               required: true,
           },
           sub_categories_ids: {
               required: true,

           },
           budget_from: {
               required: true,

           },
           budget_to: {
               required: true,

           },
           expected_start_date: {
               required: true,

           },
           expected_end_date: {
               required: true,

           },
           bid_start_date: {
               required: true,

           },
           bid_end_date: {
               required: true,

           },
           timeline_count: {
               required: true,

           },
           timeline_type: {
               required: true,

           },
           // milestone_names: {
           //     required: true,

           // },
           // milestone_start_dates: {
           //     required: true,

           // },
           // milestone_end_dates: {
           //     required: true,

           // },
           // milestone_amounts: {
           //     required: true,

           // },
           milestone_descriptions: {
               required: true,

           },
           skills: {
               required: true,

           }
       },

       errorPlacement: function(error, element) {
           var placement = $(element).data('error');
           if (placement) {
               $(placement).html(error);
           } else if (element.attr("id") == "title") {
               $(".title-error").html(error);
           } else if (element.attr("id") == "location") {
               $(".location-error").html(error);
           } else if (element.attr("id") == "description") {
               $(".description-error").html(error);
           } else if (element.attr("id") == "industry_ids") {
               //error.insertAfter(".error-jqu-whats");
               $(".industries-error").html(error);
           } else if (element.attr("id") == "category_ids") {

               $(".categories-error").html(error);
           } else if (element.attr("id") == "sub_category_ids") {
               $(".sub-categories-error").html(error);
           } else if (element.attr("id") == "budget_from") {
               $(".budget-from-error").html(error);
           } else if (element.attr("id") == "budget_to") {
               $(".budget-to-error").html(error);
           } else if (element.attr("id") == "expected_start_date") {
               $(".expected-start-error").html(error);
           } else if (element.attr("id") == "expected_end_date") {
               $(".expected-end-error").html(error);
           } else if (element.attr("id") == "bid_start_date") {
               $(".bid-start-error").html(error);
           } else if (element.attr("id") == "bid_end_date") {
               $(".bid-end-error").html(error);
           } else if (element.attr("id") == "timeline_count") {
               $(".timeline-count-error").html(error);
           } else if (element.attr("id") == "timeline_type") {
               $(".timeline-type-error").html(error);
           } 
           else if (element.attr("id") == "milestone_names") {
               $(".milestone-title-error").html(error);
           } 
           
           else if (element.attr("id") == "milestone_start_date") {
               $(".milestone-start-error").html(error);
           } 
           else if (element.attr("id") == "milestone_end_date") {
               $(".milestone-start-error").html(error);
           } 
           else if (element.attr("id") == "milestone_amount") {
               $(".milestone-start-error").html(error);
           } 
           else if (element.attr("id") == "skill_ids") {
               $(".skills-error").html(error);
           } 
           else if (element.attr("id") == "milestone_amount") {
               $(".milestone-amount-error").html(error);
           } 
           else if (element.attr("id") == "milestone_description") {
               $(".milestone-description-error").html(error);
           } 
        
          
           
           else {
               error.insertAfter(element);
           }
       },

       messages: {
           'title': {
               required: "Title is required.",
           },
           'location': {
               required: "Location is required.",
           },
           'description': {
               required: "Description is required.",
           },
           'budget_from': {
               required: "Budget from amount required.",
           },
           'budget_to': {
               required: "Budget to amount required.",
           },
           'expected_start_date': {
               required: "Project start date required.",
           },
           'expected_end_date': {
               required: "Project end date required.",
           },
           'bid_start_date': {
               required: "Proposal start date required.",
           },
           'bid_end_date': {
               required: "Proposal end date required.",
           },
           'timeline_count': {
               required: "Timeline count required.",
           },
           'timeline_type': {
               required: "Timeline type required.",
           },
           'milestone_names': {
               required: "Milestone name is required.",
           },


       }

   });

   function remove_doc(id,el){
            if(confirm("Do You Want to Remove This File")){
                $.get("{{ route('project.RemoveFile')}}", {id:id},
                    function (data, textStatus, jqXHR) {
                        $(el).parent("div").remove();
                        $('.dropzone_docs')[0].dropzone.options.maxFiles = Number(5) - Number($('.pr-docs').length);
                    },
                );
            }
        }
</script>

@endpush