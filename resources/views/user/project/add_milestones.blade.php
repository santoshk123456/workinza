@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Add Milestones')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush
@section('content')

<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">

        <!-- main page title:start -->
        <div class="row m-0">
            <div class="col-sm-6">
                <h3 class="title-main">{{__('Add Milestones') }}</h3>
            </div>

        </div>
        <form method="post" enctype="multipart/form-data" class="form-validate" id="add-milestone-form" action="{{route('project.StoreMilestone')}}">
            @csrf

            <div class="form-block bg-shadow mt-4">

                <div class="block-content">



                    <input type="hidden" name="project_id" id="project_id" value="{{$project->id}}">
                    <input type="hidden" name="uuid" id="uuid" value="{{$project->uuid}}">
                    <input type="hidden" id="startDate" value="{{$startDate}}">
                    <!-- milestone input:start -->
                    <div class="milestone-box border-0 p-0">
                        <!-- <form> -->

                        <table class="table">
                            <tbody>

                                @if(!empty(old('milestone_names')))
                                @foreach(old('milestone_names') as $key=>$milestone)
                                <tr class="milestone-inner-col">
                                    <td scope="row" class="border-table">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="title">{{__('Title') }}</label>

                                                    <input type="text" name="milestone_names[]" class="form-control milestone_names" value="{{$milestone}}" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Name')}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="project-end-date">{{__('Start Date') }}</label>

                                                    <input type="text" name="milestone_start_dates[]" class="form-control milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Date') }}" id="milestone_start_date">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="project-end-date">{{__('End Date') }}</label>

                                                    <input type="text" name="milestone_end_dates[]" class="form-control milestone_end_date common-date-pick" value="{{old('milestone_end_dates')[$key]}}" required autocomplete="off" placeholder="{{ __('Select Date') }}" id="milestone_end_date">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="amount">{{__('Amount') }}</label>

                                                    <input type="number" name="milestone_amounts[]" class="form-control no-inc numericOnly" value="{{old('milestone_amounts')[$key]}}" min="1" max="9999999999" placeholder="{{__('Amount')}}" required id="milestone_amount">
                                                    <span  class="errorhtml"></span> 
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="description">{{__('Description')}}</label>

                                                    <textarea name="milestone_descriptions[]" id="milestone_description" class="form-control milestone_description" placeholder="Description" required>{{old('milestone_descriptions')[$key]}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="border-0 text-right">
                                        <button class="btn btn-success add_more mr-2" type="button">Add Milestone</button><button type="button" class="btn ml-2 remove_row white-nm-btn white-nm-btn border">Remove Milestone</button>
                                    </td>
                                </tr>

                                @endforeach
                                @else
                                <tr class="milestone-inner-col">

                                    <td scope="row" class="border-table">
                                        <div class="row milestone">
                                            <div class="col-md-4">
                                                <div class="form-group required">
                                                    <label for="title" class="control-label">{{__('Title') }}</label>

                                                    <input type="text" name="milestone_names[]" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Name')}}" required>
                                                    <!-- <span id="milestone-title-error" class="error-txt milestone-title-error"></span> -->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group required">
                                                    <label for="project-end-date" class="control-label">{{__('Start Date') }}</label>

                                                    <input type="text" id="milestone_start_date" name="milestone_start_dates[]" class="form-control no-type milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                                                    <!-- <span class="error-txt milestone-start-error"></span> -->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group required">
                                                    <label for="project-end-date" class="control-label">{{__('End Date') }}</label>

                                                    <input type="text" id="milestone_end_date" name="milestone_end_dates[]" class="form-control no-type milestone_end_date" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                                                    <!-- <span  class="error-txt milestone-end-error"></span> -->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group required">
                                                    <label for="amount" class="control-label">{{__('Amount') }}</label>

                                                    <input type="number" id="milestone_amount" name="milestone_amounts[]" class="form-control no-inc numericOnly milestone_amount" min="1" max="9999999999" id="milestone_amount" placeholder="{{__('Amount')}}" required>
                                                    <span  class="errorhtml"></span> 
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group required">
                                                    <label for="description" class="control-label">{{__('Description') }}</label>

                                                    <textarea name="milestone_descriptions[]" id="milestone_description" class="form-control milestone_description" placeholder="Description" required></textarea>
                                                    <!-- <span class="error-txt milestone-description-error"></span> -->
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="remove-buttons border-0 text-right">
                                        <button class="btn bg-nm-btn add_more mr-2" type="button">Add Milestone</button>
                                        <button type="button" class="btn remove_row white-nm-btn border">Remove Milestone</button>
                                    </td>
                                 
                                </tr>


                                @endif
                            </tbody>
                        </table>




                    </div>
                    <!-- milestone input:end -->
                    <!-- <div class="row mt-4 mb-3">
                                <div class="col-12 text-right">
                                    <button class="btn bg-nm-btn add_more" type="button">{{__('Add Milestone') }}</button>
                                </div>
                            </div> -->

                </div>

            </div>

            <div class="row final-submit">
                <div class="col-12 text-center">
                    <button  class="btn bg-nm-btn mt-4 mb-4" type="button" id="form-submit">{{__('Add') }}</button>
                 
                </div>
            </div>

        </form>
    </div>

    <!-- left section:start -->
    </div>
    </div>

@endsection
@push('custom-scripts')
<script>
    $(document).keyup(function(e) {
        $(e.target).removeClass('error');
    });
    $('.milestone-box table tr:last').find('.remove_row').hide();
    $(document).on("blur", '.milestone_amount', function() {
    
	    // for validating milestone amount with budget amount

        var inputs = $(".milestone_amount");
        var total_amount = {{$total_milestone_amount}};

        for (var i = 0; i < inputs.length; i++) {
            total_amount = parseFloat(total_amount) + parseFloat($(inputs[i]).val());
        }
        

        $('#total_amount').val(total_amount);
       
        var budget = {{$project->budget_to}};
        if(budget < total_amount){
            
            var test = document.getElementsByClassName('errorhtml');
            document.getElementsByClassName('errorhtml')[test.length-1].innerHTML = "<span style='color: #fe4848; font-size:13px;'>"+ "Amount is greater than budget </span>"
            //error.innerHTML = "<span style='color: #fe4848; font-size:13px;'>"+ "Amount is greater than budget </span>"
            //project 
            $('#form-submit').attr('disabled', 'disabled');
        }else{
            $('#form-submit').removeAttr("disabled");
            var test = document.getElementsByClassName('errorhtml');
            document.getElementsByClassName('errorhtml')[test.length-1].innerHTML = ""
        }
    });
  // function submitForm(){
   // $("#add-milestone-form").submit(function(){
        $('#form-submit').click( function() {
        var isFormValid = true;
        $('#add-milestone-form').find("input,textarea").each(function() {
            if (this.type == "text" || this.type == "number" || this.type == "textarea") {
                //  console.log(this.type);
                if (!$(this).val()) {
                    $(this).addClass("error");
                    isFormValid = false;

                } else {
                    $(this).removeClass("error");
                }
            }
        });
           if(isFormValid)
           {
            $("#add-milestone-form").submit();
           }
           else{
            return isFormValid;
           }
      
   });
   function datepickerRemover(input){
            $(input).parent().children('i').remove();
            $(input).removeClass('gj-textbox-md');
            $(input).removeAttr('data-type');
            $(input).removeAttr('data-guid');
            $(input).removeAttr('data-datepicker');
            $(input).parent().parent().append($(input).parent().html());
            $(input).parent().parent().children('div').remove();
        }
    $(document).on("click", '.add_more', function() {

        var $tr = $(this).closest('tr');

        var $clone = $tr.clone();
        $clone.find('input').val('');
        $clone.find('textarea').val('');
        $clone.find('input.milestone_start_date')
        $clone.find('input').prop('required',false); 
$clone.find('textarea').prop('required',false); 
        var start = $('.milestone_start_date').length;
        var end = $('.milestone_end_date').length;


        $clone.find('input.milestone_start_date')
            .attr('id', 'dd_date_start' + start); //newly added line
        $clone.find('input.milestone_end_date')
            .attr('id', 'dd_date_end' + end); //newly added line

        $clone.find('input.milestone_start_date').
        removeAttr('data-guid').removeAttr('data-datepicker');

        $clone.find('input.milestone_end_date').
        removeAttr('data-guid').removeAttr('data-datepicker');

        $clone.find('input.milestone_names')
            .attr('id', 'dd_title' + start).prop('required',true); 
        $clone.find('input.milestone_amount')
            .attr('id', 'dd_amount' + start).prop('required',true);
            $clone.find('textarea.milestone_description')
            .attr('id', 'dd_description' + start).prop('required',true);

        $tr.after($clone);
        var count = $('.add_more').length;
 
        if(count>0)
        {
        
        $('.remove_row').show();
        }
        $(this).hide();
        $('.milestone-box table tr').not('.milestone-box table tr:last').find('.remove_row').show();
        // $('#dd_date_start' + start).datepicker();
        // $('#dd_date_end' + end).datepicker();
        if(start <= 1){
            var last_val = $('#milestone_end_date').datepicker().value();
        }
        else{
            new_start = start-1;
            var last_val = $('#dd_date_end'+new_start).datepicker().value();
        }
        var startDate = new Date(last_val);
        startDate.setDate(startDate.getDate() + 1);

        $('#dd_date_start' + start).datepicker({
            minDate: startDate,
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            change: function (e) {
                datepickerRemover('#dd_date_end' + end);
                var startDate = new Date(e.target.value);
                $('#dd_date_end' + end).datepicker({
                    value:'',
                    minDate: startDate,
                });
            }
        });
        $('#dd_date_end' + end).datepicker({
            minDate: startDate,
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
        });


    });

    $(document).on("click", '.remove_row', function() {
        if ($('.milestone_names').length == 1) {
            alert('Can not delete this row');
        } else {
            var $tr = $(this).closest('tr').remove();
        }
    });

    $(document).ready(function() {
        var minDate = new Date($('#startDate').val());
        $('#milestone_start_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            // value: $('#startDate').val(),
            minDate: minDate,
            change: function (e) {
                datepickerRemover('#milestone_end_date');
                var startDate = new Date(e.target.value);
                $('#milestone_end_date').datepicker({
                    value:'',
                    minDate: startDate,
                });
            }

        });

        $('#milestone_end_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            minDate: minDate,

        });

    });
    

    // $("#add-milestone-form").validate({
    //     onkeyup: function(element) {
    //         $(element).valid();

    //     },
    //     rules: {

            // milestone_names: {
            //     required: true,

            // },
            // milestone_start_dates: {
            //     required: true,

            // },
            // milestone_end_dates: {
            //     required: true,

            // },
            // milestone_amounts: {
            //     required: true,

            // },
            // milestone_descriptions: {
            //     required: true,

            // },

            // errorPlacement: function(error, element) {
            //     var placement = $(element).data('error');
            //     if (placement) {
            //         $(placement).html(error);
            //     } else if (element.attr("id") == "milestone_names") {
            //         alert("dd");
            //         $(".milestone-title-error").html(error);
            //     } else if (element.attr("id") == "milestone_start_date") {
            //         $(".milestone-start-error").html(error);
            //     } else if (element.attr("id") == "milestone_end_date") {
            //         $(".milestone-end-error").html(error);
            //     } else if (element.attr("id") == "milestone_amount") {
            //         $(".milestone-amount-error").html(error);
            //     } else if (element.attr("id") == "milestone_description") {
            //         $(".milestone-description-error").html(error);
            //     } else {
            //         error.insertAfter(element);
            //     }
            // },

            //     messages: {
            //         'title': {
            //             required: "Title is required.",
            //         },
            //         'location': {
            //             required: "Location is required.",
            //         },
            //         'description': {
            //             required: "Description is required.",
            //         },
            //         'budget_from': {
            //             required: "Budget from amount required.",
            //         },
            //         'budget_to': {
            //             required: "Budget to amount required.",
            //         },
            //         'expected_start_date': {
            //             required: "Expected start date required.",
            //         },
            //         'expected_end_date': {
            //             required: "Expected end date required.",
            //         },
            //         'bid_start_date': {
            //             required: "Bid start date required.",
            //         },
            //         'bid_end_date': {
            //             required: "Bid end date required.",
            //         },
            //         'timeline_count': {
            //             required: "Timeline count required.",
            //         },
            //         'timeline_type': {
            //             required: "Timeline type required.",
            //         },
            //         'milestone_names': {
            //             required: "Milestone name is required.",
            //         },


    //     }

    // });
</script>

@endpush