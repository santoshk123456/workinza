
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('In Progress Projects')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')

        <!-- Header:end -->
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout in-progress-project-layouts project-layouts">
      <h3 class="title">My Projects</h3>
    <div class="row">      
        <div class="col-lg-3 left-aside left-projects-list">          
            <ul class="d-block">
            <li><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
                <li class="active"><a href="">In Progress Projects</a></li>
                <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="right-aside">         
              <div class="contact-info mb-4">
                             
            

                  <form method="GET" action="{{route('project.listInProgressProjects')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
                  <div class="project-search-top mb-3">              
                      <div class="input-group project-search-inprogress">
                        <!-- <input type="text" class="form-control border-right-0" placeholder="Search project by name or keyword" aria-label="Search project  by name or keyword"> -->

                        <input value="{{old('title')}}" type="text" name="title" class="form-control border-right-0" id="title" minlength="3" maxlength="200" placeholder="{{__('Search project by name or keyword')}}" required aria-label="Search project  by name or keyword">

                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                        </div>
                      </div>  
                      {{-- <button class="btn white-btn filter-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><span class="sprites-col filter-icon"></span> Filters</button> --}}
                      {{-- <div class="filter-col collapse" id="collapseExample">
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="status">Status</label>
                              <select name="status" id="status"  class="form-control"  required>
                                <option disabled selected value="">Select Filter</option>
                                <option  value="2"> {{__('Assigned')}}</option>
                                <option value="3">   {{__('InProgress')}}</option>
                                                     
                              </select>
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <label for="datepicker">Proposal End Date</label>
                              <input type="text" id="datepicker" name="proposal_end_date" class="form-control"  aria-describedby="datepicker" placeholder="DD/MM/YY">
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="btn-cols">                              
                              <button class="btn submit-btn" type="submit">Apply</button>
                              <button class="btn btn-secondary mt-2" type="reset"><i class="mdi mdi-refresh mr-1"></i>{{ __('Reset') }}</button>
                            </div>
                          </div>
                        </div>
                      </div> --}}
                  </div>
                </form>


                @if(!$inprogress_projects->isEmpty())
                @foreach ($inprogress_projects as $inprogress_project)

                  <div class="projects-list-col in-progress-list-col mb-3">
                    <p class="low-font-weight no-line">  {{ucwords($inprogress_project->title)}}</p>
                    <ul class="projects-dates-col">
                      <li> <p>Amount: <strong> {{formatAmount($inprogress_project->amount)}}</strong></p></li>
                     
                      <a href="{{ route('project.prepareContract', $inprogress_project->contracts) }}"><li><p>Contract ID:<strong>{{getContractId($inprogress_project->contracts->id)}}</strong></p></li></a>
                   </ul>
                    <ul class="in-progress-list">
                      <li>
                        <div class="col-list-item">                    
                          <div class="media">
                          @if($inprogress_project->contracts->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$inprogress_project->contracts->user->profile_image)))
                        <img class="mr-3 thumb"  src="{{ asset('storage/user/profile-images/'.$inprogress_project->contracts->user->profile_image) }}" alt="{{ $inprogress_project->contracts->user->username }}">
                        @else 
                            <img class="mr-3 thumb"  src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $inprogress_project->contracts->user->username }}"
                        class="rounded-circle img-thumbnail">
                        @endif
                            <div class="media-body">
                              <h6 class="mb-2">{{$inprogress_project->contracts->user->username}} </h6>     
                              <p class="mb-0 loc-icon">{{$inprogress_project->location}}</p>
                            </div>  
                          </div>
                        </div> 
                      </li>
                      <li> <p>Project Start Date</p><p><strong>{{hcms_date(strtotime($inprogress_project->expected_start_date), 'date', false)}}</strong></p></li>
                      <li><p>Project End Date </p><p><strong>{{hcms_date(strtotime($inprogress_project->expected_end_date), 'date', false)}}</strong></p></li>
                      
                     
                      {{-- @if(!empty($inprogress_project->milestones))
                       
                        @foreach($inprogress_project->milestones as $key =>$milestone)
                          @if($key <= 1)
                            <li>
                            <p class="mb-1"> {{$milestone->milestone_name}} <span class="notify-col">Active</span></p>
                            <p class="mb-0">Due date:<strong>{{ hcms_date(strtotime($milestone->end_date), 'date', false)}}</strong></p>
                            </li>
                          @endif
                        @endforeach
                        @if(count($inprogress_project->milestones) == 1)
                        <li>
                          <p class="mb-1"></p>
                          <p class="mb-0"></p>
                        </li>
                        @endif
                      @else 
                      <li>
                        <p class="mb-1"></p>
                        <p class="mb-0"></p>
                      </li>
                      <li>
                        <p class="mb-1"></p>
                        <p class="mb-0"></p>
                      </li>
                      @endif --}}
                     
                      <li class="last">
                        <div class="btns-div">                       
                        <a href="{{ route('project.inProgressProjectDetails' ,$inprogress_project->uuid) }}" ><button class="btn bg-nm-btn" type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></a>
                          <a href="{{ route('project.editProject' ,$inprogress_project->uuid) }}" > <button class="btn white-nm-btn"><i class="fi-flaticon flaticon-pen mr-2"></i>Edit</button></a>
                        </div>
                      </li>
                    </ul>
                  </div>

           @endforeach
           @else
            {{__('No project found')}} 
           @endif
           @if(!$inprogress_projects->isEmpty())
                  <div class="pagination-col mt-4 mb-3">
                    <nav aria-label="page navigation">
                      <ul class="pagination justify-content-end pagination-sm flex-wrap">
        
                      {{ $inprogress_projects->links('vendor.pagination.customnew') }}

                      </ul>
                    </nav>
                  </div>
                  @endif
              </div>           
            </div> 
        </div>        
    </div>
  </div>
</div>
</main>





@endsection
@push('custom-scripts')


@endpush

