@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Find A Project')))
@push('meta-tags')
<link rel="apple-touch-icon" sizes="57x57" href="images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicons/favicon-16x16.png">
    <link rel="manifest" href="images/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Pre-load sections -->
    <link rel="preload" href="css/style.min.css" as="style">
    <link rel="preload" href="js/bundle.min.js" as="script">
    <link rel="preload" href="fonts/AmazonEmber-Bold.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="fonts/AmazonEmber-Regular.woff" as="font" type="font/woff" crossorigin>      
    <!-- Style sheets -->
    <link rel="stylesheet" href="css/style.min.css">
@endpush

@section('content')
@php
   if(!empty(Session::get('min_value'))){
      $from_price =Session::get('min_value');
   }else{
      $from_price=$min_price;
   }
   if(!empty(Session::get('max_value'))){
      $to_price =Session::get('max_value');
   }else{
      $to_price=$max_price;
   }
@endphp
<main class="main-wrapper">
  <div class="container inner-pg-layout project-layouts">
     <!-- main page title:start -->
     <div class="row m-0">
        <div class="col-lg-12">
           <h3 class="title-main">Find Project</h3>
        </div>
     </div>
     <!-- main page title:end -->
     <div class="row m-0 mt-4">
        <!-- side filter panel:start -->
        <div class="col-lg-3" id="openFilter">
      @php
         $session_types = !empty(Session::get('types'))?Session::get('types'):[];
         $session_categories = !empty(Session::get('categories'))?Session::get('categories'):[];
         $session_sub_categories = !empty(Session::get('sub_categories'))?Session::get('sub_categories'):[];
         $session_industries = !empty(Session::get('industries'))?Session::get('industries'):[];
         $session_skills = !empty(Session::get('skills'))?Session::get('skills'):[];
         $session_sort_value = !empty(Session::get('sort_value'))?Session::get('sort_value'):'latest';
      @endphp
           <form method="GET" action="{{route('project.findProject')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif findProjectForm" id="project-form1" novalidate>
           <div class="filter-side-panel mb-3">
               <div class="row">
               <div class="col-12">
               <button class="btn btn-close"><i class="fi-flaticon flaticon-close-cross-in-circular-outlined-interface-button" onclick="filterClose()"></i></button>
               </div>
               </div>
              <!-- panel header:start -->
              <div class="panel-head">
                 <div class="row">
                    <div class="col-6">
                       <h4 class="title">Filter</h4>
                    </div>
                    <div class="col-6 text-right">
                       <button class="btn btn-reset reset_session" type="button">Reset</button>
                    </div>
                 </div>
              </div>
              <!-- panel header:end -->
              <!-- panel item:start -->
              <div class="panel-item">
                 <h2 class="item-title">Project Type</h2>
                 <div class="list-items" data-simplebar data-simplebar-auto-hide="false">
                    <div class="custom-control custom-checkbox text-left">
                       
                       <input type="checkbox" class="custom-control-input" id="fixed" name="types[]" value="0" @foreach($session_types as $typ) @foreach($typ as $val) @if($val == '0')) checked @endif @endforeach @endforeach>
                       <label class="custom-control-label" for="fixed">Fixed project</label>
                    </div>
                    <div class="custom-control custom-checkbox text-left">
                       <input type="checkbox" class="custom-control-input" id="hourly" name="types[]" value="1" @foreach($session_types as $typ) @foreach($typ as $val) @if($val == '1')) checked @endif @endforeach @endforeach>
                       <label class="custom-control-label" for="hourly">Hourly Projects</label>
                    </div>
                 </div>
              </div>
              <!-- panel item:end -->
              <hr class="sep">
              <!-- panel item:start -->
              <div class="panel-item">
                 <h2 class="item-title">Industry</h2>
                 <div class="input-group item-search">
                    <input type="text" class="form-control border-right-0" id="industry_name" placeholder="Search industry" aria-label="Search industry">
                    <div class="input-group-append">
                       <button class="btn btn-outline-secondary border-left-0" id="searchByIndus" type="button" onclick="searchByIndusName()"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                    </div>
                 </div>
                 <div class="list-items indusDiv" data-simplebar data-simplebar-auto-hide="false">
                    @foreach($industries as $industry)
                    <div class="custom-control custom-checkbox text-left">
                       <input type="checkbox" class="custom-control-input" name="industries[]" value="{{$industry->id}}" @foreach($session_industries as $indus) @foreach($indus as $val) @if($industry->id == $val)) checked @endif @endforeach @endforeach id="information-technology{{$loop->iteration}}">
                       <label class="custom-control-label" for="information-technology{{$loop->iteration}}">{{$industry->name}}</label>
                    </div>
                    @endforeach
                 </div>
              </div>
              <!-- panel item:end -->
              <hr class="sep">
              <!-- panel item:start -->
              <div class="panel-item">
                 <h2 class="item-title">Category</h2>
                 <div class="input-group item-search">
                    <input type="text" class="form-control border-right-0" id="category_name" placeholder="Search category" aria-label="Search category">
                    <div class="input-group-append">
                       <button class="btn btn-outline-secondary border-left-0" id="searchByCat" type="button" onclick="searchByCatName()"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                    </div>
                 </div>
                 <div class="list-items catDiv" data-simplebar data-simplebar-auto-hide="false">
                 @foreach($categories as $category)
                    <div class="custom-control custom-checkbox text-left">
                       <input type="checkbox" class="custom-control-input" name="categories[]" value="{{$category->id}}" id="programming{{$loop->iteration}}" @foreach($session_categories as $categ) @foreach($categ as $val) @if($category->id == $val)) checked @endif @endforeach @endforeach>
                       <label class="custom-control-label" for="programming{{$loop->iteration}}">{{$category->name}}</label>
                    </div>
                  @endforeach
                 </div>
              </div>
              <!-- panel item:end -->
              <hr class="sep">
              <!-- panel item:start -->
              <div class="panel-item">
                 <h2 class="item-title">Sub Category</h2>
                 <div class="input-group item-search">
                    <input type="text" class="form-control border-right-0" id="sub_category_name" placeholder="Search sub category" aria-label="Search sub category">
                    <div class="input-group-append">
                       <button class="btn btn-outline-secondary border-left-0" type="button" id="searchBySubCat" onclick="searchBySubCatName()"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                    </div>
                 </div>
                 <div class="list-items subCatDiv" data-simplebar data-simplebar-auto-hide="false">
                 @foreach($sub_categories as $sub_category)
                    <div class="custom-control custom-checkbox text-left">
                       <input type="checkbox" class="custom-control-input" name="sub_categories[]" value="{{$sub_category->id}}" id="consulting{{$loop->iteration}}" @foreach($session_sub_categories as $subCat) @foreach($subCat as $val) @if($sub_category->id == $val)) checked @endif @endforeach @endforeach>
                       <label class="custom-control-label" for="consulting{{$loop->iteration}}">{{$sub_category->name}}</label>
                    </div>
                  @endforeach
                 </div>
              </div>
              <!-- panel item:end -->
              <hr class="sep">
              <!-- panel item:start -->
              <div class="panel-item">
                 <h2 class="item-title">Skills</h2>
                 <div class="input-group item-search">
                    <input type="text" class="form-control border-right-0" id="skill_name" placeholder="Search skills" aria-label="Search skills">
                    <div class="input-group-append">
                       <button class="btn btn-outline-secondary border-left-0" type="button" id="searchBySkill" onclick="searchBySkillName()"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                    </div>
                 </div>
                 <div class="list-items skillDiv" data-simplebar data-simplebar-auto-hide="false">
                 @foreach($skills as $skill)
                    <div class="custom-control custom-checkbox text-left">
                       <input type="checkbox" class="custom-control-input" name="skills[]" value="{{$skill->id}}" id="php{{$loop->iteration}}" @foreach($session_skills as $seSkill) @foreach($seSkill as $val) @if($skill->id == $val)) checked @endif @endforeach @endforeach>
                       <label class="custom-control-label" for="php{{$loop->iteration}}">{{$skill->name}}</label>
                    </div>
                  @endforeach
                 </div>
              </div>
              <!-- panel item:end -->
              <hr class="sep">
              <div class="price-range-slider">
                 <div class="row">
                    <div class="col-sm-12">
                       <div id="slider-range"></div>
                    </div>
                 </div>
                 <div class="row slider-labels">
                    <div class="col-6 caption">
                       <strong>Min:</strong> <span id="slider-range-value1" class="range-values"></span>
                    </div>
                    <div class="col-6 text-right caption">
                       <strong>Max:</strong> <span id="slider-range-value2" class="range-values"></span>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-sm-12">
                       <form>
                          <input type="hidden" name="min_value" value="{{ $from_price }}" id="min_price">
                          <input type="hidden" name="max_value" value="{{ $to_price }}" id="max_price">
                       </form>
                    </div>
                 </div>
              </div>
              <input type="hidden" name="submitValue" value="1" id="submitValue">
              <div class="filter-submit">
                 <button class="btn bg-nm-btn w-100 py-1" type="submit">Apply</button>
              </div>
           </div>
           </form>
        </div>
        <!-- side filter panel:start -->
        <!-- projects search listing: start -->
        <div class="col-lg-9">
         <div id="filter-overlay"></div>
           <div class="project-search-listing invitaion-list">
              <div class="search-bar bg-shadow">
                 <div class="row">
                    <div class="col-md-9">
                     @php 
                        $sort_value = '';
                        $title = '';
                        $url = $_SERVER['REQUEST_URI'];
                        $parts = parse_url($url);
                        if(isset($parts['query']))
                        {
                           parse_str($parts['query'], $query);
                           if(isset($query['sort_value'])){
                              $sort_value = $query['sort_value'];
                           }
                           if(isset($query['title'])){
                              $title = $query['title'];
                           }
                        }
                     @endphp
                     <form method="GET" action="{{route('project.findProject')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif findProjectForm" id="project-form" novalidate>
                        <div class="input-group project-search">
                        <input value="{{$title}}" type="text" name="title" class="form-control border-right-0" id="title" minlength="3" maxlength="200" placeholder="{{__('Search project by name or keyword')}}" required aria-label="Search project  by name or keyword">
                           <div class="input-group-append">
                              <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                           </div>
                        </div>
                     
                    </div>
                    <div class="col-md-3">
                       <div class="form-group sort">
                          <label for="industry">Sort by</label>
                          <select class="form-control" name="sort_value" id="project_sort">
                             <option value="latest" @if($session_sort_value == "latest") selected @endif>Latest</option>
                             <option value="lowest_price" @if($session_sort_value == "lowest_price") selected @endif>Lowest Price</option>
                             <option value="highest_price" @if($session_sort_value == "highest_price") selected @endif>Highest Price</option>
                          </select>
                       </div>
                    </div>
                    </form>
                 </div>
                 <div class="row filter-mob-btn mt-3 mb-4">
                    <div class="col-12">
                     <button class="btn bg-nm-btn px-4 py-1" type="submit" onclick="filterSildeNav()">Filter</button>
                    </div>
                 </div>
              </div>
              @if(!$projects->isEmpty())
                @foreach ($projects as $project)
              <!-- search list item:end -->
              <div class="col-list-item bg-shadow mb-3">
                 <div class="row">
                    <div class="col-lg-6">

                       <h4 class="">{{$project->title}}</h4>
                       <span class="industry">{{ !empty($project->industries) ? implode(', ', $project->industries->pluck('name')->toArray()) : __('NA') }}</span>
                       <ul class="duration-list">
                          <li>
                             <p class="seperator mb-2">{{ ($project->type == 0)?'Fixed':'Hourly'}} Price </p>
                          </li>
                          <li>
                             <p class="seperator mb-2">${{$project->budget_from}}-${{$project->budget_to}}</p>
                          </li>
                          <li>
                             <p class="mb-2">Timeline: <strong>{{$project->timeline_count}}{{$project->timeline_type}}</strong> </p>
                          </li>
                       </ul>
                    </div>
                    <div class="col-lg-6 text-lg-right">
                       <ul class="posted-list">
                          <li>
                             <p class="seperator mb-2">Posted {{$project->created_at->diffForHumans()}}</p>
                          </li>
                          <li>
                             <p class="mb-2">Proposal End Date : {{date('d-M-Y', strtotime($project->bid_end_date))}}</p>
                          </li>
                       </ul>
                       <div class="btn-cols text-xl-right mb-3 ">
                           @php 
                              $invite = $projectInvitations->where('project_id',$project->id)->first();
                           @endphp  
                          <button class="btn bg-nm-btn mr-2" type="button" onclick="window.location.href='{{ route('project.viewInvitation',$project) }}'"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button>
                          <button class="btn bg-nm-btn mr-2" type="button" onclick="window.location.href='{{route('project.createProposal',$project->uuid)}}'"><i class="fi-flaticon flaticon-send mr-2"></i>@if(isset($invite))Accept @else Apply @endif</button>
                          @if(App\ProjectChat::ChatFeature()==1)
                          <button class="btn white-nm-btn" type="button" onclick="window.location.href='{{route('message.messages',array($project->uuid,$project->user->uuid))}}'"><i class="fi-flaticon flaticon-comments mr-2"></i>Message</button>
                          @endif
                        </div>
                    </div>
                 </div>
                 <p class="description">{{strlen($project->description) > 50 ? substr($project->description,0,150)."..." : $project->description}}</p>
                 <div class="skils-cols mb-3">
                    @foreach($project->skills as $skill)
                    <span class="notify-col">{{$skill->name}}</span>
                    @endforeach
                 </div>
                 <div class="media text-center text-md-left d-block d-md-flex">
                     @if($project->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$project->user->profile_image)))
                           <img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('storage/user/profile-images/'.$project->user->profile_image) }}"  alt="{{ $project->user->username }}">
                     @else 
                           <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $project->user->full_name }}" class="mr-md-3 thumb mb-3 mb-md-0">
                     @endif
                    <div class="media-body">
                     <?php 
                        $status ="null";
                     ?>                    
                     <a href="{{ route('account.viewUserDetail',['user'=>$project->user,'project'=>$project,'status'=>$status]) }}"> <h5 class="mt-0 name">{{$project->user->username}} 
                        @if($project->user->organization_type == 0 )
                                 
                        <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                        @else
                        <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                        @endif</i></h5>     </a>
                       <p><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>{{$project->location}}</p>
                       <div class="ratings">
                          <div class="rate-count">@if(!empty($project->user->userDetail->avg_rating)){{$project->user->userDetail->avg_rating}} @else 0 @endif</div>
                          <div class="user-rating" data-value="@if(!empty($project->user->userDetail->avg_rating)){{$project->user->userDetail->avg_rating}}@else 0 @endif"></div>
                       </div>
                    </div>
                 </div>
              </div>
              <!-- search list item:end -->
              @endforeach
              @endif
              <div class="pagination-col mt-4 mb-3">
                 <nav aria-label="page navigation">
                    <ul class="pagination justify-content-end pagination-sm flex-wrap">
                    {{ $projects->links('vendor.pagination.customnew') }}
                    </ul>
                 </nav>
              </div>
           </div>
        </div>
        <!-- projects search listing: start -->
     </div>
  </div>
</main>
@endsection
@push('custom-scripts')
<script>
    $(document).on("change", '#project_sort', function() {
      $('#project-form').submit();
    });
    $(document).on("click",".reset_session",function(){
      reset_session();
   });

   function reset_session(){

      $.get("{{route('project.resetFilter')}}",    { },
         function (data, textStatus, jqXHR) {
            window.location = '/project/find-a-project';
         },

      );
   }
   $('.findProjectForm').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
         e.preventDefault();
         return false;
      }
   });
   $("#industry_name").keypress(function(e) {
      if(e.which == 13) {
         console.log('You pressed enter!');
         $("#searchByIndus").click();
      }
   });
   function searchByIndusName(){

      $.get("{{route('project.searchByName')}}",    {industry : $("#industry_name").val() },
         function (data, textStatus, jqXHR) {
            var indus_html = '';
            $.each(data.industries , function(index, val) { 
               indus_html += '<div class="custom-control custom-checkbox text-left">';
               indus_html += '   <input type="checkbox" class="custom-control-input" name="industries[]" value="'+val+'" id="information-technology'+val+'">';
               indus_html += '   <label class="custom-control-label" for="information-technology'+val+'">'+index+'</label>';
               indus_html += '</div>';
            });
            $(".indusDiv").html(indus_html);
         },

      );
   }

   $("#category_name").keypress(function(e) {
      if(e.which == 13) {
         console.log('You pressed enter!');
         $("#searchByCat").click();
      }
   });
   function searchByCatName(){

      $.get("{{route('project.searchByName')}}",    {category :$("#category_name").val() },
         function (data, textStatus, jqXHR) {
            
            var cat_html = '';
            $.each(data.categories , function(index, val) { 
               cat_html += '<div class="custom-control custom-checkbox text-left">';
               cat_html += '   <input type="checkbox" class="custom-control-input" name="categories[]" value="'+val+'" id="programming'+val+'">';
               cat_html += '   <label class="custom-control-label" for="programming'+val+'">'+index+'</label>';
               cat_html += '</div>';
            });
            $(".catDiv").html(cat_html);

         },

      );
   }

   $("#sub_category_name").keypress(function(e) {
      if(e.which == 13) {
         console.log('You pressed enter!');
         $("#searchBySubCat").click();
      }
   });
   function searchBySubCatName(){

      $.get("{{route('project.searchByName')}}",    {sub_category : $("#sub_category_name").val() },
         function (data, textStatus, jqXHR) {

            var sub_cat_html = '';
            $.each(data.sub_categories , function(index, val) { 
               sub_cat_html += '<div class="custom-control custom-checkbox text-left">';
               sub_cat_html += '   <input type="checkbox" class="custom-control-input" name="sub_categories[]" value="'+val+'" id="consulting'+val+'">';
               sub_cat_html += '   <label class="custom-control-label" for="consulting'+val+'">'+index+'</label>';
               sub_cat_html += '</div>';
            });
            $(".subCatDiv").html(sub_cat_html);

         },

      );
   }

   $("#skill_name").keypress(function(e) {
      if(e.which == 13) {
         console.log('You pressed enter!');
         $("#searchBySkill").click();
      }
   });
   function searchBySkillName(){

      $.get("{{route('project.searchByName')}}",    {skill :$("#skill_name").val() },
         function (data, textStatus, jqXHR) {

            var skill_html = '';
            $.each(data.skills , function(index, val) { 
               skill_html += '<div class="custom-control custom-checkbox text-left">';
               skill_html += '   <input type="checkbox" class="custom-control-input" name="skills[]" value="'+val+'" id="php'+val+'">';
               skill_html += '   <label class="custom-control-label" for="php'+val+'">'+index+'</label>';
               skill_html += '</div>';
            });
            $(".skillDiv").html(skill_html);
         },

      );
   }

   $("#title").keypress(function(e) {
      if(e.which == 13) {
         $('#project-form').submit();
      }
   });
   var from_price=parseFloat("{{ $from_price }}");
     var to_price=parseFloat("{{ $to_price }}");
     var min_price=parseFloat("{{ $min_price }}");
     var max_price=parseFloat("{{ $max_price }}");
     
     if(from_price==to_price){
        //to_price=to_price+1;
     }
     
     if(min_price==max_price){
        max_price=max_price+1;
     }
   // Price range slider:
   $(document).ready(function() {
      // noUiSlider.destroy();
        $('.noUi-handle').on('click', function() {
          $(this).width(50);
        });
        var rangeSlider = document.getElementById('slider-range');
        var moneyFormat = wNumb({
          decimals: 0,
          thousand: ',',
          prefix: 'USD '
        });
        noUiSlider.create(rangeSlider, {
          start: [from_price, to_price],
          step: 1,
          range: {
            'min': [min_price],
            'max': [max_price]
          },
          format: moneyFormat,
          connect: true
        });
        
        // Set visual min and max values and also update value hidden form inputs
        rangeSlider.noUiSlider.on('update', function(values, handle) {
          document.getElementById('slider-range-value1').innerHTML = values[0];
          document.getElementById('slider-range-value2').innerHTML = values[1];
          document.getElementById('min_price').value = moneyFormat.from(
            values[0]);
          document.getElementById('max_price').value = moneyFormat.from(
            values[1]);
        });
      });
</script>
@endpush