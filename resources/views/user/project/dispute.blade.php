@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Dispute List')))
@push('meta-tags')
@endpush
<!--start: contact information page -->
@section('content')
<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout project-layouts pt-4">
         <!-- main page title:start -->
         <div class="row">
          <div class="col-sm-6">
          <h3 class="title-main mb-3">Disputes</h3>
          </div>
          <div class="col-sm-6 text-sm-right mt-sm-0 mt-3">
            <!-- <button class="btn bg-nm-btn" type="button" onclick="location.href=''"><i class="fi-flaticon flaticon-plus mr-2"></i>Raise a Dispute</button>          -->
          </div>
        </div>
       <!-- main page title:end -->
    <div class="row mt-2">
    @include('user.layouts.includes.finance_tab',['user'=>$user])
        <div class="col-lg-9 pl-xl-0">
            <div class="right-aside">         
                    <!-- freelancer-list:start -->
              <div class="invoices-col bg-shadow">
                <div class="row">
                  <div class="col-md-6">
                    <p class="table-text pl-2">Disputes</p>
                  </div>
                  <div class="col-md-6">
                    <div class="search-bar p-0">
                    @php 
                        $title = '';
                        $url = $_SERVER['REQUEST_URI'];
                        $parts = parse_url($url);
                        if(isset($parts['query']))
                        {
                           parse_str($parts['query'], $query);
                           if(isset($query['title'])){
                              $title = $query['title'];
                           }
                        }
                     @endphp
                     <form method="GET" action="{{route('finance.ListDisputes')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif findProjectForm" id="project-form" novalidate>
                    <div class="input-group project-search">
                      <input value="{{$title}}" type="text" class="form-control border-right-0" name="title" id="title" placeholder="Search project by contract" aria-label="Search project  by contract">
                      <div class="input-group-append">
                         <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                      </div>
                    </div>
                     </form>
                   </div>
                  </div>
                </div>

                <div class="table-multi-common table-responsive mt-3">
                  <table class="table table-striped">
                    <thead>
                    @if($disputes->isNotEmpty())
                      <tr>
                        <th scope="col">Contract ID  </th>
                        <th scope="col">Milestone</th>                       
                        <th scope="col"> Amount</th> 
                        @if(auth()->user()->user_type == 0)
                        <th scope="col">Freelancer UserName</th>
                        @else
                        <th scope="col">Client UserName</th>
                        @endif 
                        <th scope="col">Dispute Date</th>
                        <th scope="col">Status</th>                       
                        <th scope="col">Action</th>
                      </tr>
                      @else
                        <tr>
                            <td>{{__('No data found')}}</td>
                        </tr>
                    @endif
                    </thead>
                    <tbody>
                        @foreach($disputes as $dispute)
                      <tr>
                        <td>{{getContractId($dispute->project_contract_id)}}</td>
                        <td>{{$dispute->milestone->milestone_name}}</td>
                        <td>{{formatAmount($dispute->amount)}}</td>
                        @if(auth()->user()->user_type == 0)
                        <td>{{$dispute->freelancer->username}}</td>  
                        @else
                        <td>{{$dispute->client->username}}</td> 
                        @endif   
                        <td>{{date('d-M-Y',strtotime($dispute->created_at))}}</td>         
                        <td><span class="badge badge-multi-common badge-primary">{{$dispute->status==1?'Resolved':'Pending'}}</span></td>                      
                        <td>  <button class="btn bg-nm-btn mr-2" type="button" onclick="location.href='{{ route('finance.viewDispute',$dispute)}}'"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></td>
                      </tr> 
                      @endforeach
                    </tbody>
                  </table>
                </div>
                @if($disputes->isNotEmpty())
                 <!--pagination:start -->
                <div class="pagination-col mt-4 mb-3">
                  <nav aria-label="page navigation">
                  <ul class="pagination justify-content-end pagination-sm flex-wrap">
                    {{ $disputes->links('vendor.pagination.customnew') }}
                    </ul>
                  </nav>
                </div>
                @endif
                   <!--pagination:end -->
              </div>
               <!-- freelancer-list:end -->  
            
            
              </div>
        </div>
    </div>
    </div> 
  </div>
</main>
@endsection
<!--end: contact information page -->

@push('custom-scripts')
@endpush
