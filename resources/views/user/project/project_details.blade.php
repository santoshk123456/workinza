@extends('user.layouts.user_two')
@section('title', set_page_titile($project->title))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')


<main class="main-wrapper">
  <div class="container inner-pg-layout project-layouts">
    <h3 class="title">
    {{ucwords($project->title)}}

    </h3>
    <div class="row mx-0">
        <div class="col-lg-3 left-aside left-projects-list">         
            <ul class="d-block">
              <li class="active"><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
              <li><a href="{{route('project.listInProgressProjects')}}">In Progress Projects</a></li>
              <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="right-aside">         
              <div class="bg-shadow mb-3 projects-links-menu">
                <ul>
                  <li><a href="{{ route('project.projectDetails',$project) }}" class="active"><i class="fi-flaticon flaticon-blank-page mr-2"></i>Project Details </a></li>
                  <li><a href="{{ route('project.inviteFreelancers',$project) }}"><i class="fi-flaticon flaticon-send mr-2"></i>Invite Freelancers</a></li>
                  <li><a href="{{ route('project.projectProposals',$project) }}"><i class="fi-flaticon flaticon-auction mr-2"></i>Proposals</a></li>
                  @if(App\ProjectChat::ChatFeature()==1)
                  <li><a href="{{ route('message.messages',$project) }}"><i class="fi-flaticon flaticon-comments mr-2"></i>Messages</a></li>              
                 @endif
                </ul>                
              </div> 
              <div class="edits-col text-right mb-3">
                <a href="{{route('project.editProject',$project)}}" class="mr-2"><i class="fi-flaticon flaticon-pen mr-2"></i>Edit</a>
                <a  class="pointer-del" onclick="deleteProject({{$project->status}})"><i class="fi-flaticon flaticon-delete mr-2"></i>{{__('Delete')}}</a>
              </div>
              <div class="details-col  mb-3">
                <div class="row">
                  <div class="col-xl-8 col-lg-7 pr-lg-0 mb-3 mb-lg-0">
                    <div class="bg-shadow project-desp-col">
                      <h4 class="mb-2">{{ucwords($project->title)}} </h4>  
                      <p>{{$project->description}}</p>
                      <div class="row">
                        <div class="col-md-4 mb-2 mb-md-0">                        
                          <span class="location-area"><i class="fi-flaticon flaticon-location-pin mr-1"></i>{{$project->location}}</span>
                        </div>
                        <div class="col-md-8 text-md-right">
                          <p>Average Proposal Amount :<span> {{formatAmount($average_proposal_amount)}} </span> </p>
                          <div class="status-col">                          
                            <p class="d-inline-block mb-0">Status<span class="notify-col  @if(count($proposals) == 0) last-col @endif">{{projectStatus($project->status)}}</span></p>
                            @if(!empty($proposals) && count($proposals) != 0) 
                            <span class="count-proposals">
                              @if(!empty($proposals) && count($proposals) == 1)
                                {{count($proposals)}} Proposal
                              @elseif(!empty($proposals) && count($proposals) > 2)
                                {{count($proposals)}} Proposals
                              @else 
                                  {{__('No proposal found')}}
                              @endif
                            </span>
                            @endif
                          </div>
                        </div>
                      </div>                    
                    </div>
                  </div>
                  <div class="col-xl-4 col-lg-5">
                    <div class="bg-shadow project-proposals-col">
                      <ul class="d-block">
                        <li><p>Project Type</p><span>{{projectType($project->type)}}</span></li>
                        <li><p>Budget</p><span>{{formatAmount($project->budget_from)}} - {{formatAmount($project->budget_to)}}</span></li>
                        <li><p>Proposal Start Date</p><span>{{hcms_date(strtotime($project->bid_start_date), 'date', false)}}</span></li>
                        <li><p>Proposal End Date</p><span>{{hcms_date(strtotime($project->bid_end_date), 'date', false)}}</span></li>
                      <li><p>Total Proposals</p><span>@if(!empty($proposals)){{count($proposals)}}@else {{__('0')}}@endif</span></li>
                      </ul>
                    </div>

                  </div>
                </div>
              </div>
              <div class="seperated-cols bg-shadow mb-3 delivery-expect">
                <div class="row">
                  <div class="col-sm-4 seperator mb-2 mb-sm-0">
                    <p class="mb-0">Project Start Date <span><strong> {{hcms_date(strtotime($project->expected_start_date), 'date', false)}}</strong></span></p>
                  </div>
                  <div class="col-sm-4 seperator mb-2 mb-sm-0">
                    <p class="mb-0">Project End Date <span><strong> {{hcms_date(strtotime($project->expected_end_date), 'date', false)}}</strong></span></p>
                  </div>
                  <div class="col-sm-4">
                    <p class="mb-0">Time Line <span><strong>{{getTimeline($project->timeline_count,$project->timeline_type)}}</strong></span></p>
                  </div>
                </div>
              </div>
              <div class="seperated-cols bg-shadow mb-3 features-special">
                <div class="row">
                  <div class="col-sm-4 seperator mb-2 mb-sm-0">
                    <p class="mb-0">Industry : <span>{{ !empty($project->industries) ? implode(', ', $project->industries->pluck('name')->toArray()) : __('NA') }}</span></p>
                  </div>
                  <div class="col-sm-4 seperator mb-2 mb-sm-0">
                    <p class="mb-0">Category : <span> {{ !empty($project->categories) ? implode(', ', $project->categories->pluck('name')->toArray()) : __('NA') }}</span></p>
                  </div>
                  <div class="col-sm-4">
                    <p class="mb-0">Sub Category : <span> {{ !empty($project->subCategories) ? implode(', ', $project->subCategories->pluck('name')->toArray()) : __('NA') }}</span></p>
                  </div>
                </div>
              </div>
              @if(count($project->skills)>0)
              <div class="seperated-cols bg-shadow mb-3 feature-skills">
              <h5 class="sub-title">Skills and Expertise</h5>   
                 <div class="skils-cols">
                    @foreach($project->skills->pluck('name') as $skill)
                 
                      <span class="notify-col">{{$skill}}</span>
                    @endforeach
                 </div>                     
              </div>
              @endif
              <div class="seperated-cols bg-shadow mb-3">
				<h5 class="sub-title">{{__('Documents')}}</h5>
				@if(!empty($documents))
					@foreach($documents as $document)
						<div class="document-col my-3">
							<span class="file-name"><i class="sprites-col acrobit-icon mr-2"></i>{{str_limit(ucwords($document->file_name), $limit = 15, $end = '...')}}</span>
							
							
							<div class="btns-col">
								<button class="btn white-nm-btn view" onclick="NewTab('{{asset('storage/projects/project_documents/'.$document->file_name)}}')"><i class="fi-flaticon flaticon-eye"></i></button>
								{{-- <a class="btn white-nm-btn download" href="{{asset('/images/user/440KLC1lR7zZI5Ur69L0dlzM8hLM25jLct4csCV6.jpeg')}}" target="_blank">Download</a> --}}
								<a href="{{asset('storage/projects/project_documents/'.$document->file_name)}}" class="btn white-nm-btn download" download="{{$document->file_name}}" ><i class="fi-flaticon flaticon-download"></i></a>
               <a class="btn white-nm-btn delete" href="{{route('project.DestroyDocuments',$document->id)}}"><i class="fi-flaticon flaticon-bin"></i></a>
             </div>
						</div>
					@endforeach
				@else
					<div class="document-col my-3">
						{{__("No document found")}}
					</div>
				@endif
					
                <button class="btn bg-nm-btn add-document" onclick="window.location.href='{{route('project.AddDocument',$project)}}'"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Documents</button>
              </div>

              
              {{-- <div class="seperated-cols bg-shadow mb-3">
                <h5 class="sub-title">{{__('Milestones')}}</h5>
				@if(count($project->milestones)>0)
					@foreach($project->milestones as $milestone)
						<div class="milestones-col">
							<div class="head-top">
							<h6>{{ $milestone->milestone_name}}  <span class="notify-col">{{getMilestoneStatus($milestone->status)}}</span></h6>
							<span class="milestone-price">{{ formatAmount($milestone->milestone_amount)}}</span>
							</div>
							<p>{{$milestone->milestone_description}}</p>
							
						</div>
					@endforeach
                @else
                	<div class="head-top">
                  		<h6>{{__("No milestone available")}} 
                	</div>
                @endif
               
                <button onclick="window.location.href='{{route('project.AddMilestone',$project)}}'" class="btn bg-nm-btn" type="submit"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Milestone</button>
              </div> --}}
              @if(count($project->milestones)>0)
              <div class="bg-shadow descp-table milestone-table-col">
                <div class="table-multi-common table-responsive">
                  <table class="table table-striped mb-0">
                  <thead>
                    <tr>
                    <th scope="col" class="sub-title">Milestones</th>
                    <th scope="col"  class="sub-title">Amount</th>        
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($project->milestones as $milestone)
                    <tr>
                      <td>      
                        <div class="milestones-col">
                          <div class="head-top mb-3">
                            <p class="d-inline-block mr-3">{{ ucwords($milestone->milestone_name)}}</p>       
                            <div class="btns-div d-inline-block">   
                              @if($milestone->status == 0 && ($project->status != 0 &&  $project->status != 1) ) 
                              <form method="POST" action="{{route('project.ActivateMilestone')}}" class="d-inline-block needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
                                @csrf
                                <input type="hidden" name="id" value={{$milestone->id}}>
                                <input type="hidden" name="project_id" value={{$milestone->project_id}}>
                                <input type="hidden" name="milestone_name" value={{$milestone->milestone_name}}>
                              <button class="btn bg-nm-btn mr-2 btn-small" type="submit"><i class="fi-flaticon flaticon-double-tick-indicator mr-2"></i>Activate</button>
                              </form>             
                              @endif	
                              <button onclick="window.location.href='{{route('project.MilestoneDetails',$milestone)}}'" class="btn white-nm-btn btn-small" type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button>                        
                            </div>              
                          </div>
                          <p>{{$milestone->milestone_description}}.</p>
                          </div>
                        <ul class="dates-col my-2 my-md-3">
                          <li>Start Date {{hcms_date(strtotime($milestone->start_date), 'date', false)}}</li>
                          <li>End Date {{hcms_date(strtotime($milestone->end_date), 'date', false)}}</li>
                        </ul>
                      </td>
                      <td>{{ formatAmount($milestone->milestone_amount)}}</td>               
                    </tr>
                    @endforeach
                    
                    <tr class="footer-table">
                    <td>Total Amount:</td>
                    <th>{{ formatAmount($total_milestone_amount)}}</th>
                    </tr>
                  </tbody>
                  </table>
                </div>
                <p class="d-sm-none d-block pt-3">Scroll to view amount</p>
                @endif
                <button class="btn bg-nm-btn ml-2" onclick="window.location.href='{{route('project.AddMilestone',$project)}}'" type="submit"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Milestone</button>
                </div>
                
                

            </div>
        </div>
    </div>
  </div> 
</main>
@push('custom-scripts')
    <script>
        function NewTab(url) { 
            window.open( url, "_blank");
        }
        function deleteProject(status){
         
          swal({
            title: 'Are you sure?',
            text: "You want to delete the project",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            closeOnConfirm: false,
           
          },function(isConfirm){

            if (isConfirm){
              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "GET",
                    url: "{{route('project.DestroyProject',$project)}}",
                    success:function(){
                        swal(
                            'Project deleted!',
                            'The project has been deleted successfully .',
                            'success'
                        )
                        swal({
                            title: "Project deleted!",
                            text: "The project has been deleted successfully!",
                            type: "success"
                        },function(isConfirm){

                          if (isConfirm){
                            if(status == 0 ||  status == 1){
                              window.location.href = "{{route('project.listOpenProjects')}}";
                            }else if(status == 2 ||  status == 3){
                              window.location.href = "{{route('project.listInProgressProjects')}}";
                            }
                           
                          }
                        });
                        
                        
                    }
                });
             

            } 
            
       
        })
    }
        
    </script>

@endpush
@endsection
