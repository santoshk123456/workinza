@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Raise Dispute')))
@push('meta-tags')
@endpush   
<!--start: contact information page -->
@section('content')
<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout project-layouts raise-dispute">
         <!-- main page title:start -->
         <div class="row">
          <div class="col-md-8">
             <h3 class="title-main mb-3">Finances</h3>
          </div>
          <div class="col-md-4 text-right">
            <button class="btn cancel-shadow-btn" type="button" onclick="location.href='{{ route('finance.ListDisputes')}}'"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Disputes</button> 
          </div>
       </div>
       <!-- main page title:end -->
    <div class="row mt-3">
    @include('user.layouts.includes.finance_tab',['user'=>$user])
        <div class="col-lg-9 pl-xl-0">
            <div class="right-aside bg-shadow">  
              <h4 class="mb-0 sub-headtitle">Raise Dispute</h4>   
              <div class="form-filable"> 
              <form method="POST" id="raise_dispute_form" action="{{route('finance.storeDispute')}}"> 
              @csrf
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="contact-id">Contact ID <span class="mandatory error">*</span></label>
                          <input type="text" value="{{getContractId($milestone->project->contracts->id)}}" class="form-control" readonly name="project_contract_id" id="project_contract_id" aria-describedby="contact-id" placeholder="Enter contract ID">
                          <input type="hidden" name="project_contract_id" value="{{$milestone->project->contracts->id}}">
                          <span id="contract_error"  class="error-txt contract-error"></span>
                          </div>
                      </div>                        
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="industry">Project <span class="mandatory error">*</span></label>
                          <select class="form-control" name="project_id" id="project_id" required disabled>
                            <option disabled selected value="">Select</option>
                            @foreach($available_projects as $key=>$project)
                            <option value="{{$key}}" @if($milestone->project->id == $key) selected @endif>{{$project}}</option>
                            @endforeach
                          </select>
                          <input type="hidden" name="project_id" value="{{$milestone->project->id}}">
                          <input type="hidden" name="project_milestone_id" value="{{$milestone->id}}">
                          <span id="project_error"  class="error-txt project-error"></span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group required">
                          @if(auth()->user()->user_type == 0)
                          <label for="username">Freelancer Username <span class="mandatory error">*</span></label>
                          <input type="text" class="form-control" id="freelancer_name" value="{{$freelacer->contracts->user->username}}" name="freelancer_name" readonly aria-describedby="username" placeholder="Enter username">
                          <input type="hidden" name="freelancer_id" value="{{$freelacer->contracts->user->id}}">
                          @else
                          <label for="username">Client Username <span class="mandatory error">*</span></label>
                          <input type="text" class="form-control" id="freelancer_name" value="{{$milestone->project->user->username}}" name="freelancer_name" readonly aria-describedby="username" placeholder="Enter username">
                          <input type="hidden" name="freelancer_id" value="{{$freelacer->contracts->user->id}}">
                          @endif
                          <span id="freelancer_error"  class="error-txt freelancer-error"></span>  
                        </div>
                      </div> 
                      <div class="col-md-3">
                        <div class="form-group required">
                          <label for="amount">Dispute Amount (USD) <span class="mandatory error">*</span></label>
                          <input type="text" value="{{$milestone->milestone_amount}}" class="form-control" name="amount" id="amount" aria-describedby="amount" placeholder="Enter amount">
                          <span id="amount_error"  class="error-txt amount-error"></span>  
                        </div>
                      </div> 
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="subject">Subject <span class="mandatory error">*</span></label>
                          <textarea class="form-control" id="subject" name="subject" rows="2" placeholder="Enter subject"></textarea>
                          <span id="subject_error"  class="error-txt subject-error"></span>
                        </div>   
                      </div> 
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="description">Description <span class="mandatory error">*</span></label>
                          <textarea class="form-control" name="description" id="description" rows="4" placeholder="Enter description"></textarea>
                          <span id="description_error"  class="error-txt description-error"></span>
                        </div>   
                      </div>   
                    </div>
                    <div class="btn-cols text-right mb-2">
                      <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ url()->previous() }}'">Cancel</button>
                      <button class="btn submit-btn" type="submit">Save</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </div> 
  </div>
</main>
@endsection
<!--end: contact information page -->

@push('custom-scripts')
<script>
 $("#raise_dispute_form").validate({
  onfocusout: function(element){
         $(element).valid();
   	
    },
    onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        
    rules: {
        project_contract_id: {
            required: true,
        },
        project_id: {
            required: true,
        },
        freelancer_name: {
            required: true,
        },
        amount: {
            required: true,
        },
        subject: {
            required: true,
        },
        description: {
            required: true,
        }
        
        
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } 
        else if (element.attr("id") == "project_contract_id" ) {
          $(".contrct-error").html(error);
        }
        else if (element.attr("id") == "project_id" ) {
          $(".project-error").html(error);
        }
        else if (element.attr("id") == "freelancer_name" ) {
          $(".freelancer-error").html(error);
        }
        else if (element.attr("id") == "amount" ) {
          $(".amount-error").html(error);
        }
        else if (element.attr("id") == "subject" ) {
          $(".subject-error").html(error);
        }
        else if (element.attr("id") == "description" ) {
          $(".description-error").html(error);
        }
        else {
           error.insertAfter(element);
        }
   },

   messages: {
            'project_contract_id': {
                required: "Contract id is required.",
            },
            'project_id': {
                required: "project is required.",
            },
            'freelancer_name': {
                required: "Freelancer username is required.",
            },
            'amount': {
                required: "Amount is required.",
            },
            'subject': {
                required: "Subject is required.",
            },
           'description': {
                required: "Description is required.",
            }
        }

});
</script>
<script>
$(".reset").click(function() {
    $(this).closest('form').find("input[type=text], textarea,select").val("");
});
</script>
@endpush