
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('In Progress Projects')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')

        <!-- Header:end -->
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout in-progress-project-layouts">
      <h3 class="title">My Projects</h3>
    <div class="row">      
        <div class="col-lg-3 left-aside left-projects-list">          
            <ul class="d-block">
              <li ><a href="{{route('project.listProposals')}}">My Proposals</a></li>
              <li class="active"><a href="{{route('project.freelancerlistInProgressProjects')}}">Assigned / In Progress Projects</a></li>
              <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
              <li ><a href="{{route('project.listInvitations')}}">Invitations</a></li>
            </ul>
        </div>
        <div class="col-lg-9">
            <div class="right-aside">         
              <div class="contact-info mb-4">
              @php 
              $status = '';
              $firstDate = date("m-d-Y");
              $secondDate = date("m-d-Y");
              $url = $_SERVER['REQUEST_URI'];
              $parts = parse_url($url);
              if(isset($parts['query']))
              {
                parse_str($parts['query'], $query);
                if(isset($query['status'])){
                  $status = $query['status'];
                }
                  $date = $query['proposal_end_date'];
                  $new_arr = array_map('trim', explode('-', $date));
                  $firstDate = $new_arr[0];
                  $secondDate = $new_arr[1];
              }
              @endphp  
                  <form method="GET" action="{{route('project.freelancerlistInProgressProjects')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
                  <div class="project-search-top mb-3">              
                      <div class="input-group project-search">
                        <!-- <input type="text" class="form-control border-right-0" placeholder="Search project by name or keyword" aria-label="Search project  by name or keyword"> -->

                        <input value="{{old('title')}}" type="text" name="title" class="form-control border-right-0" id="title" minlength="3" maxlength="200" placeholder="{{__('Search project by name or keyword')}}" required aria-label="Search project  by name or keyword">

                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                        </div>
                      </div>  
                      <button class="btn white-btn filter-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><span class="sprites-col filter-icon"></span> Filters</button>
                      <div class="filter-col collapse" id="collapseExample">
                        <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                              <label for="status">Status</label>
                              <select name="status" id="status"  class="form-control"  required>
                                <option disabled selected value="">Select Filter</option>
                                <option value="2"  @if($status == "2") selected @endif> {{__('Assigned')}}</option>
                                <option value="3"  @if($status == "3") selected @endif>   {{__('InProgress')}}</option>
                                                     
                              </select>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="datepicker">Project End Date</label>
                              <!-- <input type="text" id="datepicker" name="proposal_end_date" class="form-control"  aria-describedby="datepicker" placeholder="DD/MM/YY"> -->
                              <input value="{{$firstDate}} - {{$secondDate}}" class="form-control custom-date float-right datepick " id="datepicker" name="proposal_end_date" placeholder="Choose Date" placeholder="DD/MM/YY" autocomplete="off" value="" >
                            </div>

                           

                          </div>
                          <div class="col-md-2">
                            <div class="btn-cols">                              
                              <button class="btn submit-btn" type="submit">Apply</button>
                            </div>
                            
                          </div>

                          <div class="col-md-3">
                            <div class="btn-cols">                             
                            <button class="btn white-btn" id="reset-btn" type="reset">{{ __('Clear') }}</button>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                  </div>
                </form>


                @if(!$inprogress_projects->isEmpty())
                @foreach ($inprogress_projects as $inprogress_project)
                 
                  <div class="projects-list-col in-progress-list-col mb-3">
                    <p class="low-font-weight no-line">  {{ucwords($inprogress_project->title)}}</p>
                    <ul class="row projects-dates-col mb-3">
                      <li class="col-md-3 m-0"> <p>Amount : <strong> {{formatAmount($inprogress_project->amount)}}</strong></p></li>
                     
                      <li class="col-md-3 m-0"><p>Contract ID : <a href="{{ route('project.prepareContract', $inprogress_project->contracts) }}"><strong>{{getContractId($inprogress_project->contracts->id)}}</strong></p></li>
                      <li class="col-md-6 m-0"> <div class="btns-div mt-0 text-lg-right">                       
                        <a href="{{ route('project.inProgressProjectDetails' ,$inprogress_project->uuid) }}" ><button class="btn bg-nm-btn mr-2" type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></a>
                        @if($inprogress_project->contracts->freelancer_acceptance == 0)
                          <a href="{{ route('project.prepareContract' ,$inprogress_project->contracts) }}" > <button class="btn white-nm-btn">Accept Contract</button></a>
                        @else
                        <a href="" > <button class="btn white-nm-btn">Contract Accepted</button></a>
                        @endif
                        </div></li>
                   </ul>
                    <ul class="row in-progress-list">
                      <li class="col-md-4 m-0">
                        <div class="col-list-item">                    
                          <div class="media">
                          @if($inprogress_project->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$inprogress_project->user->profile_image)))
                        <img class="mr-3 thumb"  src="{{ asset('storage/user/profile-images/'.$inprogress_project->user->profile_image) }}" alt="{{ $inprogress_project->user->username }}">
                        @else 
                            <img class="mr-3 thumb"  src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $inprogress_project->user->username }}"
                        class="rounded-circle img-thumbnail">
                        @endif
                            <div class="media-body">
                            <?php 
                            $status ="null";
                            ?>
                              <a href="{{ route('account.viewUserDetail',['user'=>$inprogress_project->user,'project'=>$inprogress_project,'status'=>$status]) }}"> <h6 class="mt-0 name">{{$inprogress_project->user->username}} 
                                @if($inprogress_project->user->organization_type == 0 )
                                                    
                                <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                            @else
                                <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                            @endif</i></h6>     </a>
                              <p class="mb-0 loc-icon">{{$inprogress_project->location}}</p>
                            </div>  
                          </div>
                        </div> 
                      </li>
                      <li class="col-md-4 m-0"> <p>Project Start Date</p><p><strong>{{hcms_date(strtotime($inprogress_project->expected_start_date), 'date', false)}}</strong></p></li>
                      <li class="last col-md-4 m-0"><p>Project End Date </p><p><strong>{{hcms_date(strtotime($inprogress_project->expected_end_date), 'date', false)}}</strong></p></li>
                      
                     
                    </ul>
                  </div>

           @endforeach
           @else
            {{__('No project found')}} 
           @endif

                  <div class="pagination-col mt-4 mb-3">
                    <nav aria-label="page navigation">
                      <ul class="pagination justify-content-end pagination-sm flex-wrap">
        
                      {{ $inprogress_projects->links('vendor.pagination.customnew') }}

                      </ul>
                    </nav>
                  </div>

              </div>           
            </div> 
        </div>        
    </div>
  </div>
</div>
</main>





@endsection
@push('custom-scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script> 

$(".datepick").daterangepicker({
opens: 'left',
locale: {
},
showDropdowns: true,
// format: 'dd-mm-yyyy',
autoclose: true,
});



$( "#reset-btn" ).click(function() {
    $('#status').val('').change();

    //setting specific value in select2
    $('#status').val('').change();
    window.location.href = "{{route('project.freelancerlistInProgressProjects')}}";
});
</script>


@endpush

