@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Open Projects')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<!-- Header:end -->
<!--start: contact information page -->

<main class="main-wrapper">

  <div class="container">
    <div class="inner-pg-layout project-layouts">
    <h3 class="title">My Projects</h3>
    <div class="row">
      <div class="col-lg-3 left-aside left-projects-list">
        <ul class="d-block">
          <li class="active"><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
          <li><a href="{{route('project.listInProgressProjects')}}">In Progress Projects</a></li>
          <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
        </ul>
      </div>
      <div class="col-lg-9">
        <div class="right-aside">
          <div class="contact-info mb-4">
            @php 
              $status = '';
              $firstDate = date("d-m-Y");
              $secondDate = date("d-m-Y");
              $url = $_SERVER['REQUEST_URI'];
              $parts = parse_url($url);
              if(isset($parts['query']))
              {
                parse_str($parts['query'], $query);
                if(isset($query['status'])){
                  $status = $query['status'];
                }
                $date = $query['proposal_end_date'];
                $new_arr = array_map('trim', explode('-', $date));
                $firstDate = $new_arr[0];
                $secondDate = $new_arr[1];
              }
              @endphp
            <form method="GET" action="{{route('project.listOpenProjects')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
              <div class="project-search-top mb-3">
                <div class="input-group project-search">
                  <!-- <input type="text" class="form-control border-right-0" placeholder="Search project by name or keyword" aria-label="Search project  by name or keyword"> -->

                  <input value="{{old('title')}}" type="text" name="title" class="form-control border-right-0" id="title" minlength="3" maxlength="200" placeholder="{{__('Search project by name or keyword')}}" required aria-label="Search project  by name or keyword">

                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                  </div>
                </div>
                <button class="btn white-btn filter-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><span class="sprites-col filter-icon"></span> Filters</button>
                <div class="filter-col collapse" id="collapseExample">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="status">Project Status</label>
                        <select name="status" id="status" class="form-control" required>
                          <option disabled selected value="">Select</option>
                          <option value="0" @if($status == "0") selected @endif> {{__('New')}}</option>
                          <option value="1" @if($status == "1") selected @endif> {{__('Active')}}</option>

                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="datepicker">Proposal End Date</label>
                        <!-- <input type="text" id="datepicker" name="proposal_end_date" class="form-control"  aria-describedby="datepicker" placeholder="DD/MM/YY"> -->
                        <input value="{{$firstDate}} - {{$secondDate}}" class="form-control custom-date float-right datepick " id="pie_chart_one" name="proposal_end_date" placeholder="Choose Date" autocomplete="off" >
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div class="btn-cols">
                        <button class="btn submit-btn mr-2" type="submit">Apply</button>
                        <button class="btn white-btn" id="reset-btn" type="reset">{{ __('Clear') }}</button>
                      </div>
                    </div>


                  </div>
                </div>
              </div>
            </form>

            <!-- <div class="contact-info mb-4">
                  <div class="project-search-top mb-3">              
                      <div class="input-group project-search">
                        <input type="text" class="form-control border-right-0" placeholder="Search project by name or keyword" aria-label="Search project  by name or keyword">
                        <div class="input-group-append">
                          <button class="btn btn-outline-secondary border-left-0" type="button"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                        </div>
                      </div>  
                      <button class="btn white-btn filter-btn" type="submit"><span class="sprites-col filter-icon"></span> Filters</button>
                  </div> -->
            <!-- <div class="projects-list-col">
                    <div class="row">
                      <div class="col-xl-7">
                        <h5><a href="">Banking app development</a><span class="notify-col active">New</span></h5>                      
                        <div class="projects-periods">
                          <span>Posted On: Jan 10, 2020</span>
                          <span>Proposal End Date: Mar 17, 2020</span>
                        </div>
                      </div>
                      <div class="col-xl-5">
                        <div class="btns-div">
                        <button class="btn white-nm-btn mr-2" type="submit">View 10 Proposals</button>
                        <button class="btn bg-nm-btn" type="submit"><i class="fi-flaticon flaticon-send mr-2"></i>Invite Freelancers</button>
                      </div>
                      </div>
                    </div>
                  </div> -->
            @if(!$open_projects->isEmpty())
            @foreach ($open_projects as $open_project)

            <div class="projects-list-col">
              <div class="row">
                <div class="col-xl-7">
                  <p class="low-font-weight no-line"><a href="{{ route('project.projectDetails',$open_project) }}">{{ucwords($open_project->title)}}</a><span class="notify-col">{{projectStatus($open_project->status)}}</span></p>
                  <div class="projects-periods">
                    <span>{{__('Posted On')}} : {{hcms_date(strtotime($open_project->created_at), 'date', false)}}</span>
                    <span> {{__('Proposal End Date')}} : {{hcms_date(strtotime($open_project->bid_end_date), 'date', false)}}</span>
                  </div>
                </div>
                <div class="col-xl-5">
                  <div class="btns-div">
                    @if($open_project->status == 1)
                    <a href="{{ route('project.projectProposals',$open_project) }}"> <button class="btn white-nm-btn mr-2">{{__('View ')}}{{ count($open_project->proposals)}} {{__('Proposals')}}</button></a>

                    @endif
                    <a href="{{ route('project.inviteFreelancers',$open_project) }}"><button class="btn bg-nm-btn"><i class="fi-flaticon flaticon-send mr-2"></i>Invite Freelancers</button></a>
                  </div>
                </div>
              </div>
            </div>

            @endforeach
            @else
            <div class="center">
             {{__('No project found')}} 
            </div>
            @endif

            @if(!$open_projects->isEmpty())
            <div class="pagination-col mt-4 mb-3">
              <nav aria-label="page navigation">
                <ul class="pagination justify-content-end pagination-sm flex-wrap">

                  {{ $open_projects->links('vendor.pagination.customnew') }}

                </ul>
              </nav>
            </div>

           @endif




          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</main>



@endsection
@push('custom-scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
  $(".datepick").daterangepicker({
    opens: 'left',
    locale: {
      format: 'DD/MM/YYYY',
    },
    showDropdowns: true,
    endDate: moment().add(1, 'month'),
    minDate: moment(),
    // format: 'dd-mm-yyyy',
    autoclose: true,
  });


  $("#reset-btn").click(function() {
    $('#status').val('').change();

    //setting specific value in select2
    $('#status').val('').change();

   // $('#pie_chart_one').val(chosen_date.format('YYYY-MM-DD'));

    // $('#pie_chart_one').val('').change();
    $(".datepick").daterangepicker({
    opens: 'left',
    locale: {
      format: 'DD/MM/YYYY',
    },
    // startDate: moment().subtract(1, 'days'),
    // endDate: moment().subtract(1, 'days'),
    showDropdowns: true,
    
    // format: 'dd-mm-yyyy',
    autoclose: true,
  });
    $(this).val('');
    window.location.href = "{{route('project.listOpenProjects')}}";
//setting specific value in select2



  });
</script>

@endpush