@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Add Document')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush
@section('content')

<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">

        <!-- main page title:start -->
        <div class="row m-0">
            <div class="col-sm-6">
                <h3 class="title-main">{{__('Add Documents') }}</h3>
            </div>

        </div>
        <!-- main page title:end -->

        <div class="row m-0 mt-4">
            <!-- right section:start -->
            <div class="col-md-12">
                <form method="post" enctype="multipart/form-data" id="add-document-form" action="{{route('project.StoreDocument')}}">
                    @csrf

                    <!-- form - project details:start -->
                    <div class="form-block bg-shadow">

                        <div class="block-content">
                           
                                <div class="row">

                                    <input type="hidden" name="project_id" id="project_id" value="{{$project->id}}">
                                    <input type="hidden" name="uuid" id="uuid" value="{{$project->uuid}}">
                                    <input type="hidden" name="status" id="status" value="{{$project->status}}">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="project_documents">{{__('Project Documents (Upto 5 Files)') }}</label>
                                            <!-- <div class="custom-file">
                      <input type="file" class="custom-file-input" id="validatedCustomFile" placeholder="Uplaod project related documents" required>
                      <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                    </div> -->
                                            <div class="fallback dropzone dropzone_docs"></div>
                                            <div class="imp_images"></div>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                    <!-- form - project details:end -->
                    <div class="final-submit">
                        <div class="btn-cols text-center">
                        <button class="btn white-btn mr-2" onclick="window.location.href='{{ url()->previous() }}'" type="button">{{ __('Back') }}</button>
                            <button class="btn bg-nm-btn mt-4 mb-4" type="submit">{{__('Upload') }}</button>
                        </div>
                    </div>

                </form>
            </div>

            <!-- left section:start -->
        </div>
    </div>
</main>


@endsection
@push('custom-scripts')
<script>
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone(".dropzone_docs", {
        url: "{{route('project.fileUpload') }}",
        maxFilesize: 5, // MB
        maxFiles: 5,
        dictDefaultMessage: "File size should be less than 5 MB",
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        success: function(file, response) {
            $('#dropzone-docs-error').remove();
            $('<input>').attr({
                type: 'hidden',
                name: 'project_documents[]',
                value: response.name,
                id: response.original_name,
                class: 'project_documents'
            }).appendTo('.imp_images');
        },
        removedfile: function(file) {
            $('input[name="project_documents[]"]').each(function() {
                if ($(this).attr('id') == file.name) {
                    $(this).remove();
                }
            });
            file.previewElement.remove();
        }
    });






    // $("#create-project-form").validate({
    //     onkeyup: function(element) {
    //         $(element).valid();

    //     },
    //     rules: {
    //         title: {
    //             required: true,
    //         },
    //         location: {
    //             required: true,
    //             maxlength: 300,
    //         },
    //         description: {
    //             required: false,
    //             maxlength: 300,
    //         },
    //         industry_ids: {
    //             required: false,
    //             maxlength: 300,
    //         },
    //         categories_ids: {
    //             required: true,
    //         },
    //         sub_categories_ids: {
    //             required: true,

    //         },
    //         budget_from: {
    //             required: true,

    //         },
    //         budget_to: {
    //             required: true,

    //         },
    //         expected_start_date: {
    //             required: true,

    //         },
    //         expected_end_date: {
    //             required: true,

    //         },
    //         bid_start_date: {
    //             required: true,

    //         },
    //         bid_end_date: {
    //             required: true,

    //         },
    //         timeline_count: {
    //             required: true,

    //         },
    //         timeline_type: {
    //             required: true,

    //         },
    //         milestone_names: {
    //             required: true,

    //         },
    //         milestone_start_dates: {
    //             required: true,

    //         },
    //         milestone_end_dates: {
    //             required: true,

    //         },
    //         milestone_amounts: {
    //             required: true,

    //         },
    //         milestone_descriptions: {
    //             required: true,

    //         },
    //         skills: {
    //             required: true,

    //         }
    //     },

    //     errorPlacement: function(error, element) {
    //         var placement = $(element).data('error');
    //         if (placement) {
    //             $(placement).html(error);
    //         } else if (element.attr("id") == "title") {
    //             $(".title-error").html(error);
    //         } else if (element.attr("id") == "location") {
    //             $(".location-error").html(error);
    //         } else if (element.attr("id") == "description") {
    //             $(".description-error").html(error);
    //         } else if (element.attr("id") == "industry_ids") {
    //             //error.insertAfter(".error-jqu-whats");
    //             $(".industries-error").html(error);
    //         } else if (element.attr("id") == "category_ids") {

    //             $(".categories-error").html(error);
    //         } else if (element.attr("id") == "sub_category_ids") {
    //             $(".sub-categories-error").html(error);
    //         } else if (element.attr("id") == "budget_from") {
    //             $(".budget-from-error").html(error);
    //         } else if (element.attr("id") == "budget_to") {
    //             $(".budget-to-error").html(error);
    //         } else if (element.attr("id") == "expected_start_date") {
    //             $(".expected-start-error").html(error);
    //         } else if (element.attr("id") == "expected_end_date") {
    //             $(".expected-end-error").html(error);
    //         } else if (element.attr("id") == "bid_start_date") {
    //             $(".bid-start-error").html(error);
    //         } else if (element.attr("id") == "bid_end_date") {
    //             $(".bid-end-error").html(error);
    //         } else if (element.attr("id") == "timeline_count") {
    //             $(".timeline-count-error").html(error);
    //         } else if (element.attr("id") == "timeline_type") {
    //             $(".timeline-type-error").html(error);
    //         } else if (element.attr("id") == "milestone_names") {
    //             $(".milestone-title-error").html(error);
    //         } else if (element.attr("class") == "milestone_start_date") {
    //             $(".milestone-start-error").html(error);
    //         } else if (element.attr("id") == "skill_ids") {
    //             $(".skills-error").html(error);
    //         } else if (element.attr("id") == "milestone_amount") {
    //             $(".milestone-amount-error").html(error);
    //         } else if (element.attr("id") == "milestone_description") {
    //             $(".milestone-description-error").html(error);
    //         } else {
    //             error.insertAfter(element);
    //         }
    //     },

    //     messages: {
    //         'title': {
    //             required: "Title is required.",
    //         },
    //         'location': {
    //             required: "Location is required.",
    //         },
    //         'description': {
    //             required: "Description is required.",
    //         },
    //         'budget_from': {
    //             required: "Budget from amount required.",
    //         },
    //         'budget_to': {
    //             required: "Budget to amount required.",
    //         },
    //         'expected_start_date': {
    //             required: "Expected start date required.",
    //         },
    //         'expected_end_date': {
    //             required: "Expected end date required.",
    //         },
    //         'bid_start_date': {
    //             required: "Bid start date required.",
    //         },
    //         'bid_end_date': {
    //             required: "Bid end date required.",
    //         },
    //         'timeline_count': {
    //             required: "Timeline count required.",
    //         },
    //         'timeline_type': {
    //             required: "Timeline type required.",
    //         },
    //         'milestone_names': {
    //             required: "Milestone name is required.",
    //         },


    //     }

    // });
</script>

@endpush