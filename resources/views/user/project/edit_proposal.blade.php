@extends('user.layouts.user_layout')
@section('content')
<form method="post" enctype="multipart/form-data" id="public-profile-form" action="{{route('project.updateProposal',$projectProposal)}}">
    @csrf
    @method('PUT')
    <div class="row">
        <h5>Proposal milestone details</h5>
        
        <div class="col-lg-6 ">
            <div class="form-group">
                <table>
                  
                        @foreach ($projectProposal->proposalMilestones as $milestone)
                            <tr>
                                <input type="hidden" name="milestone_id[]" value="{{$milestone->id}}">
                                <td scope="row">
                                    <input  type="text" name="milestone_names[]" value="{{$milestone->milestone_name}}" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Name')}}" required>
                                </td>
                                <td scope="row">
                                    <input  type="number" name="milestone_amounts[]" value="{{$milestone->milestone_amount}}"  class="form-control no-inc numericOnly amount" min="1" max="9999999999" id="milestone_amount" placeholder="{{__('Amount')}}" required>
                                </td>
                                <td scope="row">
                                    <input type="text" name="milestone_start_dates[]" value="{{date('d-m-Y',strtotime($milestone->start_date))}}" class="form-control no-type milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                                </td>
                                <td scope="row">
                                    <input type="text" name="milestone_end_dates[]" value="{{date('d-m-Y',strtotime($milestone->end_date))}}" class="form-control no-type milestone_end_date" required autocomplete="off" placeholder="{{ __('Select Date') }}">
                                </td>
                                <td scope="row">
                                    <textarea name="milestone_descriptions[]" id="milestone_descriptions" class="form-control" placeholder="Description" required>{{$milestone->milestone_description}}</textarea>
                                </td>
                                <td><button class="btn btn-success add_more" type="button">+</button>
                                <button type="button" class="btn btn-danger ml-2 remove_row">-</button></td>
                            </tr>
                        @endforeach
                    
                    
                </table>
            </div>
        </div>
    </div>
    <table>
        <tr>
            <div class="row">
                <input   name="total_amount" class="form-control no-inc numericOnly " min="1" max="9999999999" id="total_amount" placeholder="{{__('Total Amount')}}" required>
            </div>
        </tr>
        <tr>
            <div class="row">
                <input    name="service_fee" class="form-control no-inc " min="1" max="9999999999" id="service_fee" placeholder="{{__('Service Fee')}}" required>
                <input  value="{{$subscription_details->service_percentage}}"type="hidden" name="service_percentage" class="form-control no-inc numericOnly " min="1" max="9999999999" id="service_percentage" >
            </div>
        </tr>
        <tr>
            <div class="row">
                <input   name="my_earnings" class="form-control no-inc numericOnly" min="1" max="9999999999" id="my_earnings" placeholder="{{__('My Earnings')}}" required>
            </div>
        </tr>
    </table>
    <div class="row">
        <textarea  name="proposal_document" id="proposal_document" class="form-control" placeholder="Description" >{{old('proposal_document',$projectProposal->proposal_document)}}</textarea>
    </div>
    {{-- <div class="row">
        <input type="file" name="proposal_document" id="proposal_document" class="form-control" placeholder="Description" ></textarea>
    </div> --}}
    <div class="mt-2">
        <button type="submit">Submit</button>
    </div>
</form>

@endsection
@push('custom-scripts')
<script>
    var inputs = $(".amount");
    var total_amount = 0;
    
    for(var i = 0; i < inputs.length; i++){
        total_amount = parseFloat(total_amount) + parseFloat($(inputs[i]).val());
        
    }
    
    $('#total_amount').val(total_amount);
    var service_percentage = <?php echo $subscription_details->service_percentage;?>;
    var service_fee = (service_percentage * total_amount)/100;        
    $('#service_fee').val(service_fee);
    var my_earnings = total_amount - service_fee;
    $('#my_earnings').val(my_earnings);
    
    $(document).on("blur",'.amount',function(){
        var inputs = $(".amount");
        var total_amount = 0;
       
        for(var i = 0; i < inputs.length; i++){
            total_amount = parseFloat(total_amount) + parseFloat($(inputs[i]).val());
            
        }
        
        $('#total_amount').val(total_amount);
        var service_percentage = <?php echo $subscription_details->service_percentage;?>;
        var service_fee = (service_percentage * total_amount)/100;        
        $('#service_fee').val(service_fee);
        var my_earnings = total_amount - service_fee;
        $('#my_earnings').val(my_earnings);
       
    });
   
   
    $(document).on("click",'.add_more',function(){
       
        var $tr    = $(this).closest('tr');
        var $clone = $tr.clone();
        $clone.find('input').val('');
        $clone.find('textarea').val('');
        $tr.after($clone);
       
        $('.milestone_start_date').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            timePicker: false,
            minDate:new Date(),
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoApply: true
        });
        $('.milestone_start_date').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
        $('.milestone_end_date').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            timePicker: false,
            minDate:new Date(),
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoApply: true
        });
        $('.milestone_end_date').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });
    });
    $(document).on("click",'.remove_row',function(){
        if( $('.milestone_names').length == 1 ) {
            alert('Can not delete this row');
        }else{
            var $tr    = $(this).closest('tr').remove();
        }
    });
</script>
@endpush
