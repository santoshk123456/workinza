 
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('In Progress Projects')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')  
<main class="main-wrapper">
	<div class="container">
		<div class="inner-pg-layout in-progress-project-details-layouts">
			<h3 class="title">{{ucwords($project->title)}}</h3>
			<div class="row">
				<div class="col-lg-3 left-aside left-projects-list">
				
					@if($user->user_type == 0)
						<ul class="d-block">
							<li><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
							<li ><a href="{{route('project.listInProgressProjects')}}">In Progress Projects</a></li>
							<li class="active"><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
						</ul>
					@else
						<ul class="d-block">
							<li ><a href="{{route('project.listProposals')}}">My Proposals</a></li>
							<li ><a href="{{route('project.freelancerlistInProgressProjects')}}">Assigned / In Progress Projects</a></li>
							<li class="active"><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
							<li ><a href="{{route('project.listInvitations')}}">Invitations</a></li>
						</ul>
					@endif
				</div>
				<div class="col-lg-9">
					<div class="right-aside">
						<div class="bg-shadow mb-3 projects-links-menu">
							<ul>
								<li><a href="" class="active"><i class="fi-flaticon flaticon-blank-page mr-2"></i>Project Details </a></li>
							
							</ul>
						</div>
						@if($user->user_type == 0)
							<div class="edits-col text-right mb-3">
								<!-- <a href="{{route('project.editProject',$project)}}" class="mr-2"><i class="fi-flaticon flaticon-pen mr-2"></i>Edit</a> -->
								@if($project->status < 2)
									<a href="{{route('project.DestroyProject',$project)}}"><i class="fi-flaticon flaticon-delete mr-2"></i>{{__('Delete')}}</a>
								@endif
								
							</div>
						@endif
						<div class="details-col  mb-3 freelancer-view-details">
							<div class="row">
								<div class="col-xl-8 col-lg-7 pr-lg-0 mb-3 mb-lg-0">
									<div class="col-list-item bg-shadow">
										<div class="media text-center text-md-left d-block d-md-flex">
									@if($user->user_type == 1)
											@if($project->contracts->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$project->contracts->user->profile_image)))
												<img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('storage/user/profile-images/'.$project->contracts->user->profile_image) }}" alt="{{$project->contracts->user->username}}">
											@else
												<img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('images/admin/starter/default.png') }}" alt="{{$project->contracts->user->username}}">
											@endif 

									  @else	
									  
									        @if($project->contracts->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$project->user->profile_image)))
												<img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('storage/user/profile-images/'.$project->user->profile_image) }}" alt="{{$project->user->username}}">
											@else
												<img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('images/admin/starter/default.png') }}" alt="{{$project->user->username}}">
											@endif 

									@endif		
											<div class="media-body">
												<h5 class="mt-0 name">{{$project->contracts->user->username}} </h5>
												<h6 class="designation">@if(!empty($project->contracts->user->userDetail->profile_headline)){{$project->contracts->user->userDetail->profile_headline}}@else NA @endif </h6>
												<div class="ratings">
													<div class="rate-count">@if(!empty($project->contracts->user->userDetail->avg_rating)){{$project->contracts->user->userDetail->avg_rating}}@else 0 @endif</div>
													<div class="user-rating" data-value="@if(!empty($project->contracts->user->userDetail->avg_rating)){{$project->contracts->user->userDetail->avg_rating}}@else 0 @endif"></div>
												</div>
												<span class="industry">{{ !empty($project->contracts->user->industries) ? implode(', ', $project->contracts->user->industries->pluck('name')->toArray()) : __('NA') }}</span>
												<p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>@if(isset($project->contracts->user->userDetail->city) && isset($project->contracts->user->userDetail->state) && $project->contracts->user->userDetail->country->name){{$project->contracts->user->userDetail->city}} , {{$project->contracts->user->userDetail->state}}, {{$project->contracts->user->userDetail->country->name}} @else {{__('NA')}}@endif</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-4 col-lg-5">
									<div class="bg-shadow project-proposals-col">
										<ul class="d-block">
											<li>
												<p>Contact ID</p><span>{{getContractId($project->contracts->id)}}</span></li>
											<li>
												<p>Project Start Date</p><span>{{ hcms_date(strtotime($project->expected_start_date), 'date', false)}}</span></li>
											<li>
												<p>Project End Date</p><span>{{ hcms_date(strtotime($project->expected_end_date), 'date', false)}}</span></li>
											<li>
												<p>Amount</p><span>{{formatAmount($project->amount)}}</span></li>
										</ul>
									</div>

								</div>
							</div>
						</div>
						
						<div class="details-col  mb-3">
							<div class="row">
								<div class="col-xl-8 col-lg-7 pr-lg-0 mb-3 mb-lg-0">
									<div class="bg-shadow project-desp-col">
										<h4 class="mb-2">{{$project->title}} </h4>
										<p>{{$project->description}}</p>
										<div class="row">
											<div class="col-md-4 mb-2 mb-md-0">
												<span class="location-area"><i class="fi-flaticon flaticon-location-pin mr-2"></i>{{$project->location}}</span>
											</div>
											<div class="col-md-8 text-md-right">
												<p>Average Proposal Amount :<span> {{formatAmount($average_proposal_amount)}} </span> </p>
												<div class="status-col">
													<p class="d-inline-block mb-0">Status<span class="notify-col">{{projectStatus($project->status)}}</span></p>
													<span class="count-proposals">
														@if(!empty($proposals) && count($proposals) == 1)
															{{count($proposals)}} Proposal
													  	@elseif(!empty($proposals) && count($proposals) > 2)
															{{count($proposals)}} Proposals
													  	@else 
														  {{__('No proposal found')}}
														@endif
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xl-4 col-lg-5">
									<div class="bg-shadow project-proposals-col">
										<ul class="d-block">
											<li>
												<p>Project Type</p><span>{{projectType($project->type)}}</span></li>
											<li>
												<p>Budget</p><span>{{formatAmount($project->budget_from)}}-{{formatAmount($project->budget_to)}}</span></li>
											<li>
												<p>Proposal Start Date</p><span>{{hcms_date(strtotime($project->bid_start_date), 'date', false)}}</span></li>
											<li>
												<p>Proposal End Date</p><span>{{hcms_date(strtotime($project->bid_end_date), 'date', false)}}</span></li>
											<li>
												<p>Total Proposals</p><span>
													@if(!empty($proposals))
														{{count($proposals)}}
													@else
														{{'NA'}}
													@endif
												</span></li>
										</ul>
									</div>

								</div>
							</div>
						</div>
						<div class="seperated-cols bg-shadow mb-3 delivery-expect">
							<div class="row">
								<div class="col-sm-4 seperator mb-2 mb-sm-0">
									<p class="mb-0">Project Start Date <span><strong>  {{hcms_date(strtotime($project->expected_start_date), 'date', false)}}</strong></span></p>
								</div>
								<div class="col-sm-4 seperator mb-2 mb-sm-0">
									<p class="mb-0">Project End Date <span><strong>  {{hcms_date(strtotime($project->expected_end_date), 'date', false)}}</strong></span></p>
								</div>
								<div class="col-sm-4">
									<p class="mb-0">Time Line <span><strong> {{getTimeline($project->timeline_count,$project->timeline_type)}}</strong></span></p>
								</div>
							</div>
						</div>
						<div class="seperated-cols bg-shadow mb-3 features-special">
							<div class="row">
								<div class="col-sm-4 seperator mb-2 mb-sm-0">
									<p class="mb-0">Industry : <span> {{ !empty($project->industries) ? implode(', ', $project->industries->pluck('name')->toArray()) : __('NA') }}</span></p>
								</div>
								<div class="col-sm-4 seperator mb-2 mb-sm-0">
									<p class="mb-0">Category : <span> {{ !empty($project->categories) ? implode(', ', $project->categories->pluck('name')->toArray()) : __('NA') }}</span></p>
								</div>
								<div class="col-sm-4">
									<p class="mb-0">Sub Category : <span> {{ !empty($project->subCategories) ? implode(', ', $project->subCategories->pluck('name')->toArray()) : __('NA') }}</span></p>
								</div>
							</div>
						</div>
						<div class="seperated-cols bg-shadow mb-3 feature-skills">
							<h6 class="mb-0">Skills and Expertise</h6>
							<div class="skils-cols">
								@foreach($project->skills->pluck('name') as $skill)
								<span class="notify-col">{{$skill}}</span>
							  @endforeach   
							</div>
						</div>
						<div class="seperated-cols bg-shadow mb-3">
							<h5 class="sub-title">Documents</h5>
							@if(!empty($project->documents))
								@foreach($project->documents as $document)
									<div class="document-col my-3">
									<span class="file-name"><i class="sprites-col acrobit-icon mr-2"></i> {{ str_limit(ucwords($document->file_name), $limit = 15, $end = '...') }} </span>
									<div class="btns-col">
					
										<button class="btn white-nm-btn view" onclick="NewTab('{{asset('storage/projects/project_documents/'.$document->file_name)}}')"><i class="fi-flaticon flaticon-eye"></i></button>
										{{-- <a class="btn white-nm-btn download" href="{{asset('/images/user/440KLC1lR7zZI5Ur69L0dlzM8hLM25jLct4csCV6.jpeg')}}" target="_blank">Download</a> --}}
										<a href="{{asset('storage/projects/project_documents/'.$document->file_name)}}" class="btn white-nm-btn download" download="{{$document->file_name}}" ><i class="fi-flaticon flaticon-download"></i></a>
					   					<!-- <a class="btn white-nm-btn delete" href="{{route('project.DestroyDocuments',$document->id)}}"><i class="fi-flaticon flaticon-bin"></i></a> -->
									</div>
									</div>
								@endforeach
							@else
								<div class="document-col my-3">
									{{__("No document found")}}
								</div>
							@endif
							@if($user->user_type == 0)
								<!-- <button class="btn bg-nm-btn add-document" onclick="window.location.href='{{route('project.AddDocument',$project)}}'"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Documents</button> -->
							@endif
							
						</div>
						{{-- <div class="seperated-cols bg-shadow mb-3">
							<h5 class="sub-title">Milestones</h5>
							@if(count($project->milestones)>0)
								@foreach($project->milestones as $milestone)
									<div class="milestones-col">
										<div class="head-top">
											<h6>{{ $milestone->milestone_name}} <span class="notify-col">{{getMilestoneStatus($milestone->status)}}</span></h6>
											<span class="milestone-price">{{ formatAmount($milestone->milestone_amount)}}</span>
										</div>
										<p>{{$milestone->milestone_description}}</p>
										<div class="row">
											<div class="col-md-7">
												<ul class="dates-col my-2 my-md-3">
													<li>Start Date {{hcms_date(strtotime($milestone->start_date), 'date', false)}}</li>
													<li>End Date {{hcms_date(strtotime($milestone->end_date), 'date', false)}}</li>
												</ul>
											</div>
											<div class="col-md-5">
												<div class="btns-div text-md-right my-2 my-md-3">
													@if($milestone->status == 0)
													<form method="POST" action="{{route('project.ActivateMilestone')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
														@csrf
														<input type="hidden" name="id" value={{$milestone->id}}>
														<input type="hidden" name="project_id" value={{$milestone->project_id}}>
														<input type="hidden" name="milestone_name" value={{$milestone->milestone_name}}>
													<button class="btn bg-nm-btn mr-2" type="submit"><i class="fi-flaticon flaticon-double-tick-indicator mr-2"></i>Activate</button>
													</form>
													@endif
													<button onclick="window.location.href='{{route('project.MilestoneDetails',$milestone)}}'" class="btn white-nm-btn" type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>view</button>                        
												</div>
											</div>
										</div>

									</div>
								@endforeach
							@else
								<div class="head-top">
									<h6>{{__("No milestone available")}} 
								</div>
							@endif
						</div> --}}
						@if(count($project->milestones)>0)
						<div class="bg-shadow descp-table milestone-table-col">
							<div class="table-multi-common table-responsive">
							  <table class="table table-striped mb-0">
								<thead>
								  <tr>
									<th scope="col" class="sub-title">Milestones</th>
									<th scope="col"  class="sub-title">Amount</th>        
								  </tr>
								</thead>
								<tbody>
									@foreach($project->milestones as $milestone)
									<tr>
										<td>      
											<div class="milestones-col">
												<div class="head-top mb-3">
													<p class="d-inline-block mr-3">{{ ucwords($milestone->milestone_name)}}
														
														<span class="notify-col">{{getMilestoneStatus($milestone->status)}}</span>
														
													</p>       
													<div class="btns-div d-inline-block">   
														
														@if($milestone->status == 0 && $user->user_type == 0)
														{{-- <form method="POST" action="{{route('project.ActivateMilestone')}}" class="d-inline-block needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
															@csrf
															<input type="hidden" name="id" value={{$milestone->id}}>
															<input type="hidden" name="project_id" value={{$milestone->project_id}}>
															<input type="hidden" name="milestone_name" value={{$milestone->milestone_name}}>
														<!-- <button class="btn bg-nm-btn mr-2 btn-small" type="submit"><i class="fi-flaticon flaticon-double-tick-indicator mr-2"></i>Activate</button> -->
														</form>              --}}
														<!-- <button class="btn bg-nm-btn mr-2 btn-small" onclick="window.location.href='{{route('finance.addMoney',$milestone)}}'" type="submit" id="checkout-button"><i class="fi-flaticon flaticon-double-tick-indicator mr-2"></i>Activate</button> -->
														
														@endif	
														<!-- <button onclick="window.location.href='{{route('project.MilestoneDetails',$milestone)}}'" class="btn white-nm-btn btn-small" type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>view</button>                         -->
													</div>              
												</div>
												<p>{{$milestone->milestone_description}}.</p>
									  		</div>
											<ul class="dates-col my-2 my-md-3">
												<li>Start Date {{hcms_date(strtotime($milestone->start_date), 'date', false)}}</li>
												<li>End Date {{hcms_date(strtotime($milestone->end_date), 'date', false)}}</li>
											</ul>
										</td>
										<td>{{ formatAmount($milestone->milestone_amount)}}</td>               
									</tr>
									@endforeach
								  
								  <tr class="footer-table">
									<td>Total Amount:</td>
									<th>{{ formatAmount($total_milestone_amount)}}</th>
								  </tr>
								</tbody>
							  </table>
							</div>
							<p class="d-sm-none d-block pt-3">Scroll to view amount</p>
						  </div>
						  @endif
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection
@push('custom-scripts')
<script>
	function NewTab(url) { 
		window.open( url, "_blank");
	}
	
</script>

@endpush