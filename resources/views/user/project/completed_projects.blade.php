@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Open Projects')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<!-- Header:end -->
<!--start: contact information page -->

<main class="main-wrapper">

  <div class="container">
    <div class="inner-pg-layout project-layouts">
    <h3 class="title">My Projects</h3>
    <div class="row">
      <div class="col-lg-3 left-aside left-projects-list">
        <!-- <ul class="d-block">
          <li ><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
          <li><a href="{{route('project.listInProgressProjects')}}">In Progress Projects</a></li>
          <li class="active"><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
        </ul> -->


        @if($user->user_type == 0)
						<ul class="d-block">
							<li><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
							<li ><a href="{{route('project.listInProgressProjects')}}">In Progress Projects</a></li>
							<li class="active"><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
						</ul>
					@else
						<ul class="d-block">
							<li ><a href="{{route('project.listProposals')}}">My Proposals</a></li>
							<li ><a href="{{route('project.freelancerlistInProgressProjects')}}">Assigned / In Progress Projects</a></li>
							<li class="active"><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
							<li ><a href="{{route('project.listInvitations')}}">Invitations</a></li>
						</ul>
					@endif

      </div>
      <div class="col-lg-9">
        <div class="right-aside">
          <div class="contact-info mb-4">
 
            <form method="GET" action="{{route('project.listCompletedProjects')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
              <div class="project-search-top mb-3">
                <div class="input-group project-search">
                  <!-- <input type="text" class="form-control border-right-0" placeholder="Search project by name or keyword" aria-label="Search project  by name or keyword"> -->

                  <input value="{{old('title')}}" type="text" name="title" class="form-control border-right-0" id="title" minlength="3" maxlength="200" placeholder="{{__('Search project by name or keyword')}}" required aria-label="Search project  by name or keyword">

                  <div class="input-group-append">
                    <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                  </div>
                </div>
                </form>

                @if(!$completed_projects->isEmpty())
            <div class="table-multi-common table-responsive mt-3">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Contract ID  </th>
                          <th scope="col">Project Name</th>                       
                          <th scope="col">User Name</th>
                          <th scope="col">Amount</th> 
                          <th scope="col">Start Date</th>
                          <th scope="col">End Date</th>                       
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                  
                @foreach ($completed_projects as $completed_project)
              
                        <tr>
                          <td>{{ $completed_project->contracts->id }}</td>
                          <td>{{ $completed_project->title }}</td>
                          <td>{{ $completed_project->user->username }}</td> 
                          <td>@if($completed_project->amount) {{ $completed_project->amount  }} @else NA @endif</td>    
                          <td> {{hcms_date(strtotime($completed_project->expected_start_date), 'date', false)}}</td>         
                          <td>{{hcms_date(strtotime($completed_project->expected_end_date), 'date', false)}}</td>                     
                          <td><button class="btn bg-nm-btn py-1" onclick="location.href='{{ route('project.completedProjectDetails' ,$completed_project->uuid) }}'" type="button"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></td>
                         
                          @if($completed_project->hide_from_public== 0)
                          <td><button class="btn white-nm-btn mr-2 py-1" type="button" onclick="hideProject({{$completed_project}})"><i class="fi-flaticon flaticon-visibility mr-2"></i>Add To Public</button></td>
                        @else
                        <td><button class="btn white-nm-btn mr-2 py-1" type="button" onclick="unhideProject({{$completed_project}})"><i class="fi-flaticon flaticon-visibility mr-2"></i>Added</button></td>
                        @endif
                        @php
                        $rating=$completed_project->UserRating->where('rated_by_user_id',Auth::user()->id)->first();
                        @endphp
                        @if(empty($rating))
                        <td><button class="btn white-nm-btn mr-2 py-1" type="button" onclick="location.href='{{ route('rating.addRating' ,$completed_project->uuid) }}'"><i class="fi-flaticon flaticon-visibility mr-2"></i>Rate</button></td>
                        @else
                        <td><button class="btn white-nm-btn mr-2 py-1" type="button" onclick="location.href='{{ route('rating.ratingDetail' ,array($rating,1)) }}'" title="View Rating"><i class="fi-flaticon flaticon-visibility mr-2"></i>View Rating</button></td>
                        @endif
                      </tr> 
                @endforeach
                      </tbody>
                    </table>
                  </div>

            @else
            <div class="center mt-3">
             {{__('No project found')}} 
            </div>
            @endif

                  @if(!$completed_projects->isEmpty())
            <div class="pagination-col mt-4 mb-3">
              <nav aria-label="page navigation">
                <ul class="pagination justify-content-end pagination-sm flex-wrap">

                  {{ $completed_projects->links('vendor.pagination.customnew') }}

                </ul>
              </nav>
            </div>

           @endif


                  </div>
                </div>
              </div>
           
           

              </div>           
            </div> 
        </div>        
    </div>
  </div>
</div>
</main>









@endsection
@push('custom-scripts')

<script>
     
        function hideProject(completed_project){
         console.log(completed_project);
          swal({
            title: 'Are you sure?',
            text: "You want to add the project to public",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            closeOnConfirm: false,
           
          },function(isConfirm){

            if (isConfirm){
              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
               
                $.ajax({
                    type: "GET",
                    url: "{{route('project.HideProject',$completed_project)}}",
                    success:function(){
                        swal(
                            'Project Added To Public!',
                            'The project has been added to public successfully .',
                            'success'
                        )
                        swal({
                            title: "Project Added To Public!",
                            text: "The project has been added to public successfully!",
                            type: "success"
                        },function(isConfirm){

                          if (isConfirm){
                            window.location.href = "{{route('project.listCompletedProjects')}}";
                          }
                        });
                        
                        
                    }
                });
                

            } 
            
       
        })
    }
        


    function unhideProject(completed_project){
      console.log(completed_project);
         swal({
           title: 'Are you sure?',
           text: "You want to hide the project",
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',
           cancelButtonColor: '#d33',
           confirmButtonText: 'Yes',
           closeOnConfirm: false,
          
         },function(isConfirm){

           if (isConfirm){
             $.ajaxSetup({
                   headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
               });
        
               $.ajax({
                   type: "GET",
                   url: "{{route('project.unHideProject',$completed_project)}}",
                   success:function(){
                       swal(
                           'Project hided!',
                           'The project has been hided successfully .',
                           'success'
                       )
                       swal({
                           title: "Project hided!",
                           text: "The project has been hided successfully!",
                           type: "success"
                       },function(isConfirm){

                         if (isConfirm){
                          
                             window.location.href = "{{route('project.listCompletedProjects')}}";
                          
                            
                           
                          
                         }
                       });
                       
                       
                   }
               });
            
               
           } 
           
      
       })
   }
       
        
    </script>


@endpush




