@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Invite Freelancers')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')

<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout">
    <!-- <h3 class="title pl-0 mb-4">@if(!empty($user->userDetail->profile_headline)){{$user->userDetail->profile_headline}}@else NA @endif </h3> -->
      <div class="freelancer-view-details">  
      <div class="btns-col text-right mb-3">           
              <button class="btn white-nm-btn " type="button" onclick="window.location.href='{{ url()->previous() }}'">Back</button>       
            </div>  
          <div class="row mb-3">
            <div class="col-lg-9 pr-lg-0">
              <div class="col-list-item bg-shadow">                    
                <div class="media text-center text-md-left d-block d-md-flex">
            @if($user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$user->profile_image)))
<img class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail"  src="{{ asset('storage/user/profile-images/'.$user->profile_image) }}" alt="{{ $user->username }}">
            @else 
  <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $user->username }}"
                        class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
         @endif
                  <!-- <img class="mr-md-3 thumb mb-3 mb-md-0" src="https://randomuser.me/api/portraits/women/46.jpg" alt="Generic placeholder image"> -->
                  <div class="media-body">
                    <div class="row">
                      <div class="col-md-7 mb-2 mb-md-0">
                        <h5 class="mt-0 name">@if(!empty($user->username)){{$user->username}}@else NA @endif</h5>
                        <h6 class="designation">@if(!empty($user->userDetail->profile_headline)){{$user->userDetail->profile_headline}}@else NA @endif</h6>
                        <div class="ratings"><div class="rate-count">@if(!empty($user->userDetail->avg_rating)){{$user->userDetail->avg_rating}}@else 0 @endif</div><div class="user-rating" data-value="@if(!empty($user->userDetail->avg_rating)){{$user->userDetail->avg_rating}}@else 0 @endif"></div></div>
                        <span class="industry">{{ !empty($user->industries) ? implode(', ', $user->industries->pluck('name')->toArray()) : __('NA') }}</span>
                        <p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>{{ $user->userDetail->city }}, {{ $user->userDetail->state }}, {{ $user->userDetail->country->name }}</p>
                      </div>

                        <div class="col-md-5 mb-2 mb-md-0">
                           <div class="list-right-aside1 float-right">
                             <button class="btn bg-nm-btn mr-3 invite"  id="invite" type="button" onclick="location.href='{{ route('account.editPublicProfile') }}'"><i class="fi-flaticon flaticon-pen mr-2"></i>Edit</button>
                         </div>
                         </div>

                      <!-- <div class="col-md-5 mb-2 mb-md-0">
                        <div class="list-right-aside">
                          <button class="btn bg-nm-btn mr-3" type="submit"><i class="fi-flaticon flaticon-send mr-2"></i>Invite</button>
                          <button class="btn white-nm-btn" type="submit"><i class="fi-flaticon flaticon-comments mr-2"></i>Message</button>
                      </div>
                      </div> -->
                    </div>           
                  </div>  
                </div>
              </div>    
            </div>
     
            <div class="col-lg-3">
              <div class="bg-shadow project-proposals-col">
                <ul class="d-block">
                  <li><p>Years of Experienece</p><span> @if(!empty($user->userDetail->years_of_experience)){{$user->userDetail->years_of_experience}}@else NA @endif</span></li>
                  <li><p>Projects in Progress</p><span>0</span></li>
                  <li><p>Projects Delivered</p><span>0</span></li>
                  <li><p>Total Earnings</p><span>0</span></li>
                  <li><p>Active Since</p><span>{{ !empty($user->created_at)?hcms_date(strtotime($user->created_at),'date'):'NA' }}</span></li>
                </ul>
              </div>
            </div>
          </div>        
          <div class="seperated-cols bg-shadow mb-3 feature-skills">
            <h6 class="">About</h6>   
            <p class="mb-2">@if(!empty($user->userDetail->about_me)){{$user->userDetail->about_me}}@else NA @endif</p>                     
             <!-- <p class="mb-2">I have more than 7 years of experience in designing meaningful, user-centered and functional UI and in improving UX across the web and mobile All that using the whole pile of a designing tool such as Figma, Sketch, Balsamiq, Adobe Photoshop CC, Adobe Illustrator CC.</p> -->
          </div>
          @if(count($user->skills))
            <div class="seperated-cols bg-shadow mb-3 feature-skills">
                <h6 class="">Skills and Expertise</h6>   
                <div class="skils-cols">
                  
                  
                  @foreach($user->skills->pluck('name') as $skill)
                    <span class="notify-col">{{$skill}}</span>
                  @endforeach
                 
                
            
                </div>                     
            </div>
            @endif
            @if(count($user->categories))
              <div class="row">
                <div class="col-md-6 pr-md-0">
                  <div class="seperated-cols bg-shadow mb-3 feature-skills">
                    <h6 class="">Category</h6>   
                    <div class="skils-cols">
                      @foreach($user->categories->pluck('name') as $category)
                      <span class="notify-col">{{$category}}</span>
                    @endforeach
                  
                    </div>                     
                </div>
                </div>
                <div class="col-md-6">
                  <div class="seperated-cols bg-shadow mb-3 feature-skills">
                    <h6 class="">Sub Category</h6>   
                    <div class="skils-cols">
                      @foreach($user->subCategories->pluck('name') as $subCategories)
                      <span class="notify-col">{{$subCategories}}</span>
                    @endforeach
                   
                    <!-- <span class="notify-col">WordPress</span>          -->
                    </div>                     
                </div>
                </div>
              </div>
            @endif
        <!-- <div class="seperated-cols bg-shadow mb-3">
          <h6 class="">Recent Top Projects</h6>   
          <ul>
            <li>Banking Penetarion Test</li>
            <li>Donec facilisis ligula et ornare luctus. In pretium sit amet est ac dapibus. </li>
            <li>Banking Penetarion Test</li>
            <li>Donec facilisis ligula et ornare luctus. In pretium sit amet est ac dapibus. </li>
          </ul>                    
        </div> -->
        @if(isset($user->userDetail->video_urls) && !empty($user->userDetail->video_urls))  
        <div class="seperated-cols bg-shadow mb-3">
          <h6 class="">Videos</h6> 
       
          <ul class="videos-listing">                  
          @php
         $videos = json_decode($user->userDetail->video_urls);
         @endphp

 @if(!empty($videos))
     @foreach ($videos as $video)
            @php
              $video_extract = explode("v=",$video);
              $embed_code = "";
              if(isset($video_extract[1])){
                $embed_code = explode("&",$video_extract[1]) ;
              }
            @endphp
             @if(isset($embed_code[0]))
            <li>
            
            
            <iframe width="100" height="100" src="https://www.youtube.com/embed/{{$embed_code[0]}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </li>
            @endif
       @endforeach
                @else
                 <li>{{__('NA')}}  </li>
                @endif
            <!-- <li>
              <video width="70" height="70" controls>
                <source src="./images/videos/sample-video.mp4" type="video/mp4">             
              </video>
            </li>
            <li>
              <video width="70" height="70" controls>
                <source src="./images/videos/sample-video.mp4" type="video/mp4">             
              </video>
            </li> -->
          </ul>  
          
         

        </div>
        @endif  
        <!-- Contact information -->
        <div class="seperated-cols bg-shadow mb-3 feature-skills">
          <div class="row">
            <div class="col-sm-8">
              <h6>Contact information</h6>
            </div>
            <div class="col-sm-4 text-sm-right">
              <button class="btn bg-nm-btn invite"  id="invite" type="button" onclick="location.href='{{ route('user.editContactInformation') }}'"><i class="fi-flaticon flaticon-pen mr-2"></i>Edit</button>
            </div>
          </div>
       
       
        <div class="form-row mt-3">
        
                  <div class="col-md-6">
                    <div class="form-group required mb-0">
                    <p class="">Username: <span class="title-sub bold-span"><a href="{{route('userPublicProfile',$user->username)}}" target="_blank">{{str_limit(request()->root(), $limit = 35 )}}/{{ !empty($user->username) ? $user->username : __('NA') }}</a></span></p>
                    </div>
                  </div>
           
                  <!-- <div class="col-md-6">
                    <div class="form-group required">
                    <p class="mb-0">Address 1: 
                      <span class="title-sub bold-span">{{ !empty($user->userDetail->address_line_1) ? $user->userDetail->address_line_1 : __('NA') }}</span></p><br>
                      @if(!empty($user->userDetail->address_line_2)) <p class="mb-0">Address 2: <span class="title-sub bold-span">{{ $user->userDetail->address_line_2  }}</span></p><br>@endif
                      <p class="mb-0">City: <span class="title-sub bold-span">{{ !empty($user->userDetail->city) ? $user->userDetail->city : __('NA') }}</span></p><br>
                      <p class="mb-0">State: <span class="title-sub bold-span">{{ !empty($user->userDetail->state) ? $user->userDetail->state : __('NA') }} - {{ !empty($user->userDetail->zipcode) ? $user->userDetail->zipcode : __('NA') }}</span></p><br>
                      <p class="mb-0">Country: <span class="title-sub bold-span">{{ !empty($user->userDetail->country) ? $user->userDetail->country->name : __('NA') }}</span></p><br>
                   
                    <p class="mb-0">Phone: <span class="title-sub bold-span">{{ !empty($user->phone) ? $user->full_number : __('NA') }}</span></p><br>
                    <p class="mb-0">Personal Website: <span class="title-sub bold-span">{{ !empty($user->userDetail->website_url) ? $user->userDetail->website_url : __('NA') }}</span></p>
                  </div>
                  </div> -->
                  <div class="col-md-6">
                    <div class="form-group required mb-0">
                    <p class="">Address 1: 
                      <span class="title-sub bold-span">{{ !empty($user->userDetail->address_line_1) ? $user->userDetail->address_line_1 : __('NA') }}</span></p>
                     
                    </div>
                  </div>
                  @if(!empty($user->userDetail->address_line_2))
                  <div class="col-md-6">
                    <div class="form-group required mb-0">
                    <p class="">Address 2: 
                     
                     {{ $user->userDetail->address_line_2  }}</p>
                    </div>
                  </div>
                  @endif
                  <div class="col-md-6">
                    <div class="form-group required mb-0">
                    <p class="">City: <span class="title-sub bold-span">{{ !empty($user->userDetail->city) ? $user->userDetail->city : __('NA') }}</span></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group required mb-0">
                    <p class="">State: <span class="title-sub bold-span">{{ !empty($user->userDetail->state) ? $user->userDetail->state : __('NA') }} - {{ !empty($user->userDetail->zipcode) ? $user->userDetail->zipcode : __('NA') }}</span></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group required mb-0">
                    <p class="">Country: <span class="title-sub bold-span">{{ !empty($user->userDetail->country) ? $user->userDetail->country->name : __('NA') }}</span></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group required mb-0">
                    <p class="">Phone: <span class="title-sub bold-span">{{ !empty($user->phone) ? $user->full_number : __('NA') }}</span></p>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group required mb-0">
                    <p class="">Personal Website: <span class="title-sub bold-span">{{ !empty($user->userDetail->website_url) ? $user->userDetail->website_url : __('NA') }}</span></p>
                    </div>
                  </div>

        </div></div>

        <!-- Security Questions -->
        <div class="seperated-cols bg-shadow mb-3 feature-skills">
          <div class="row">
            <div class="col-sm-8">
             <h6>Security Questions</h6>
            </div>
            <div class="col-sm-4 text-sm-right">
              @if(!$security_questions->isEmpty())
              <button class="btn bg-nm-btn invite" id="otp-verify-btn" type="button" onclick="location.href='{{ route('user.editSecurityQuestions') }}'"><i class="fi-flaticon flaticon-pen mr-2"></i>Edit</button>
            @else
              <button class="btn bg-nm-btn invite" id="otp-verify-btn" type="button" onclick="location.href='{{ route('user.securityQuestions') }}'"><i class="fi-flaticon flaticon-pen mr-2"></i>Add</button>
            @endif
            </div>
          </div>
        {{-- <div class="col-md-12 mb-2 mb-md-0">
        
            <div class="list-right-aside1 float-right">
            @if(!$security_questions->isEmpty())
                      <button class="btn bg-nm-btn mr-3 invite" id="otp-verify-btn" type="button" onclick="location.href='{{ route('user.editSecurityQuestions') }}'"><i class="fi-flaticon flaticon-pen mr-2"></i>Edit</button>
                    @else
                      <button class="btn bg-nm-btn mr-3 invite" id="otp-verify-btn" type="button" onclick="location.href='{{ route('user.securityQuestions') }}'"><i class="fi-flaticon flaticon-pen mr-2"></i>Add</button>
                    @endif
            </div>
        </div> --}}
        @if(!$security_questions->isEmpty())
      
              @foreach($security_questions as $key=>$question)
                    <div class="form-row border-bottom questionborder pt-3">
                        <div class="col-md-12 mb-lg-2">
                        <div class="form-group mb-0">
                            <p  for="quesans_{{$key}}" class="control-label">{{ $question->securityQuestions->question }}</p> 
                            <p  for="quesans_{{$key}}" class="control-label">{{ $question->answer }}</p> 
                          </div>
                        </div>
                    </div>
              @endforeach
              @else
        <div class="form-row border-bottom">
                <div class="col-md-6 mb-lg-2">
                <div class="form-group required">
                <div class="center">
            <i><p> {{__('No security questions added.')}} </p></i>
            </div>
                
                    </div>
                </div>
            </div>
        @endif</div>
        @if($user->user_type == 1) 
        <div class="seperated-cols bg-shadow mb-3 feature-skills">
         <div class="row mb-3">
           <div class="col-sm-8">
            <h6>Membership Plan</h6>
           </div>
           <div class="col-sm-4 text-sm-right">
            <button class="btn bg-nm-btn invite"  id="invite" type="button" onclick="location.href='{{route('membership_plan.membershipPlans')}}'"><i class="fi-flaticon flaticon-send mr-2"></i>Upgrade</button>
           </div>
         </div>
       
         
        <div class="contact-info ">
       
              <div class="row">
                <div class="col-md-3 mb-3 mb-lg-0">
                  <p class="mb-0">Membership plan selected : <strong class="title-sub">{{$user_subscription->membership->name}}</strong></p>
                </div>
                <div class="col-md-3 mb-3 mb-lg-0">
                  <p class="mb-0 last-col">Expiry Date : <strong class="title-sub">{{date('d-M-Y',strtotime($user_subscription->end_date))}}</strong></p>
                </div>
                <div class="col-md-3 mb-3 mb-lg-0">
                  <p class="mb-0 last-col">Purchased On : <strong class="title-sub">{{date('d-M-Y',strtotime($user_subscription->created_at))}}</strong></p>
                </div>
                <div class="col-md-3 mb-3 mb-lg-0">              
                  <p class="mb-0 last-col">Amount : <strong class="title-sub">{{config('data.currency_symbol')}}@if(!empty($user_subscription->amount)){{ ($user_subscription->amount) }} @else 0 @endif</strong></p>
                </div>

              </div>
            </div>
         
    </div>
    @endif
        @if(isset($user->userDetail->corporate_presentation) && !empty($user->userDetail->corporate_presentation))  
        @php
         $document = $user->userDetail->corporate_presentation;
         @endphp
         @if($user->user_type == 0)
        <div class="seperated-cols bg-shadow mb-3">
          <h6 class="">Documents</h6>
            @if(!empty($document))
            <div class="document-col my-3">
            <span class="file-name"><i class="sprites-col acrobit-icon mr-2"></i>{{str_limit(ucwords($document), $limit = 15, $end = '...')}}</span>													
            <div class="btns-col">
            <a href="{{asset('storage/projects/project_documents/'.$document)}}" class="btn white-nm-btn download" download="{{$document}}" ><i class="fi-flaticon flaticon-download"></i></a>
                    </div>
            </div>
            @else
                <div class="document-col my-3">
                    {{__("No document found")}}
                </div>
            @endif

          <!-- <div class="document-col my-3">
            <span class="file-name"><i class="sprites-col acrobit-icon mr-2"></i>corporate presentation.pdf</span>
            <div class="btns-col">           
              <button class="btn white-nm-btn" type="submit"><i class="fi-flaticon flaticon-download"></i></button>       
            </div>
          </div>  
           -->
          
        </div>
        @endif
        @endif

        @if(count($portfolio_list)>0)
        <div class="seperated-cols bg-shadow mb-3">
          <h6 class="">Recent Top Projects</h6>   
          @if($user->user_type == 1)
          <div class="row mt-3">
            @foreach($portfolio_list as $project)
            <div class="col-md-3 mb-3">
            <div class="card w-100">
            @if(!empty($project->file_name) && (file_exists( public_path().'/storage/user/portfolio-images/'.$project->file_name)))
              <img class="card-img-top" src="{{asset('storage/user/portfolio-images/'.$project->file_name)}}" alt="Avatar" style="width:100%;height: 140px;">
              @else
                      <img class="card-img-top" src="{{asset('/images/user/img_noimage.png')}}" style="height: 147px;
    width: 263px;" alt="image-post-job">
                      @endif
              <div class="card-body" style="height: 170px;">
                <h6 class="card-title">{{ucfirst(Str::limit($project->portfolio_title, 45)) }}</h6>
                <p class="card-text">{{ucfirst(Str::limit($project->portfolio_description, 160)) }}</p>
              </div>
            </div>
            </div>
            @endforeach
          </div>
          @else
          @foreach($portfolio_list as $project)
          <div class="row">
            <div class="col-md-4 mb-3">
              <p class="mb-0">Project : <span class="title-sub">{{$project->title}}</span></p>
            </div>
            <div class="col-md-4">
              <p class="mb-0 last-col">Expiry Date : <span class="title-sub">{{date('d-M-Y',strtotime($project->end_date))}}</span></p>
            </div>
          </div>
          @endforeach
          @endif
        </div>
        @endif

        @if(count($ratings) > 0)
        <div class="seperated-cols bg-shadow mb-3 ratings-cols profile-rating-col">
          <h6 class="rating-title">All Ratings </h6>
          @foreach($ratings as $rating)
          <div class="media text-center text-md-left d-block d-md-flex mt-3">
            @if($rating->rated_by->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$rating->rated_by->profile_image)))
            <img  class="mr-md-3 thumb mb-3 mb-md-0"  src="{{ asset('storage/user/profile-images/'.$rating->rated_by->profile_image) }}"  alt="{{ $rating->rated_by->username }}">
            @else 
            <img class="mr-md-3 thumb mb-3 mb-md-0"  src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $rating->rated_by->full_name }}" class="mr-md-3 thumb mb-3 mb-md-0">
            @endif
           
            
            <div class="media-body">    
              <h5 class="mt-0 name">{{ucwords($rating->rated_by->username)}}</h5>
              <h5 class="mt-0 name">{{__('Project')}} : {{ucwords($rating->project->title)}}</h5>
              <div class="ratings mb-2"><div class="rate-count">@if(!empty($rating->rating)){{$rating->rating}}@else 0 @endif</div> <div class="user-rating" data-value="@if(!empty($rating->rating)){{$rating->rating}} @else 0 @endif"></div></div>
              @php $review=App\UserReview::where('reviewed_to_user_id',$user->id)->where('project_id',$rating->project->id)->first(); @endphp
             
              <h6 class="designation">@if(!empty($review->review_user)){{$review->review_user}} @else NA @endif</h6>
          </div>   
          </div>  
          @endforeach  
        </div> 
        @endif
      </div> 
  </div> 
  </div>
</main>









@endsection
@push('custom-scripts')
<script>
  $(document).ready(function(){
     
 
     $('.invite-form').submit(function(e) {            
         e.preventDefault();
         var $button = $(this).find('.invite');
         $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
         });
         $.ajax({
                 type: "POST",
                 url: "{{route('project.sendInvitationToFreelancer')}}",
                 data: $(this).serialize(),
                 success: function (data) { 
                    if(data['success'] == 1){
                         $button.attr('disabled', 'disabled').html('<i class="fi-flaticon flaticon-send mr-2"></i>Invited');
 
                    }else{
                       
                    }
                 },
                 beforeSend: function(){
                     $button.attr('disabled', 'disabled').html('<i class="fi-flaticon flaticon-send mr-2"></i>Inviting...');
                    
                 }
         });
     });
  });
 </script>

@endpush
