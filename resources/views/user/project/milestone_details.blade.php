@extends('user.layouts.user_two')
@section('title', set_page_titile('Milestone Details'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout milestone-details-layout">
      <div class="row mb-3">
        <div class="col-sm-6">
          <h3 class="title-main">{{__('Milestone Details')}}</h3>
        </div>
        <div class="col-sm-6 text-sm-right mt-sm-0 mt-3">
          @if($milestone->status  > 0)
            <button class="btn bg-nm-btn mr-3" type="button" onclick="location.href='{{ route('finance.raiseDispute',[$milestone->id,$milestone->project->id])}}'"><i class="fi-flaticon flaticon-plus mr-2"></i>{{__('Raise a Dispute')}}</button>
          @endif
          @if(App\ProjectChat::ChatFeature()==1)
          <button class="btn bg-nm-btn mr-3" type="submit"  onclick="location.href='{{ route('message.messages',array($milestone->project->uuid))}}'"><i class="fi-flaticon flaticon-comments mr-2"></i>{{__('Message')}}</button>
          @endif
          	@if($user->user_type == 0 && ($milestone->project->status == 2 || $milestone->project->status == 3))
			<button onclick="window.location.href='{{route('project.listInProgressProjects')}}'" class="btn cancel-shadow-btn" type="submit"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>{{__('List All In Progress Projects')}}</button>
			@elseif($user->user_type == 0 && ($milestone->project->status == 0 || $milestone->project->status == 1))
            	<button onclick="window.location.href='{{route('project.listOpenProjects')}}'" class="btn cancel-shadow-btn" type="submit"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>{{__('List All Open Projects')}}</button>
          	@endif
        </div>
      </div>
      <div class="freelancer-view-details">
        <div class="seperated-cols bg-shadow mb-3">
          <h5 class="sub-title">{{ucwords($milestone->milestone_name)}}</h5>
          <div class="seperated-cols-content">
            <p>{{$milestone->milestone_description}}</p>
            <p class="mb-0">{{__('Milestone End Date')}}:<strong class="ml-1"> {{date('d-m-Y',strtotime($milestone->end_date))}}</strong></p>
          </div>
        </div>
        <div class="seperated-cols bg-shadow mb-3">
          <p>{{__('Amount')}}: <strong>  {{formatAmount($milestone->milestone_amount)}}</strong></p>
          <p>{{__('Status')}}<span class="notify-col">{{getMilestoneStatus($milestone->status)}}</span></p>
          
        </div>
        
        <div class="btns-div text-md-right my-2 my-md-3">
		  @if($milestone->status == 0  && $user->user_type == 0)
		  	<form method="GET" action="{{route('finance.addMoney',$milestone)}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
				<button class="btn log-reg-btn" type="submit">Activate</button>
		  	</form>
            {{-- <button class="btn signup-btn mr-2" type="submit">Reject</button> --}}
          @endif
          <form method="POST" action="{{route('project.milestoneStatusChange')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="milestone_submit" novalidate>
            @csrf
            <input type="hidden" value={{$milestone->id}} name="id">
            <input type="hidden" value={{$milestone->project_id}} name="project_id">
            <input type="hidden" value={{$milestone->milestone_amount}} name="milestone_amount">
            <input type="hidden" value={{$milestone->milestone_name}} name="milestone_name">
            @if(($milestone->status == 1 || $milestone->status == 2) && $user->user_type == 1)
              <input type="hidden" value="3" name="status">
              <button class="btn log-reg-btn"  type="submit">Mark As Completed</button>
            @elseif($milestone->status == 3 && $user->user_type == 0)
              <input type="hidden" value="4" name="status">
              <button class="btn log-reg-btn" type="submit">Approve</button>
            
            @elseif(($milestone->status == 0 || $milestone->status == 1) && $user->user_type == 0)
              <input type="hidden" value="6" name="status">
              {{-- <button class="btn signup-btn mr-2" type="submit">Reject</button> --}}
            @endif
          
          </form>
         
        </div>
      </div>
    </div>
  </div>
</main>
@endsection
@push('custom-scripts')
<script>
  $("#comment-form").validate({

    onfocusout: function(element) {
      $(element).valid();

    },
    onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
      // onfocusin: function(element){
      // $(element).removeClass('error');
      // $(element).next().text('');
      // },
    rules: {
      comment: {
        required: true,
      }

    },
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).html(error);
      } else if (element.attr("id") == "comment") {
        $(".comment-error").html(error);
      }
    },
    messages: {
      'comment': {
        required: "Comment is required.",
      }
    }
  });

	function NewTab(url) { 
		window.open( url, "_blank");
	}

	
   

</script>
<script>
	$('#milestone_submit').submit(function(e){
		e.preventDefault();
    
    var msg = "";
    var success_msg = "";
    var notification_message= "";
    if($('input[name=status]').val() == 3){
      msg = "You want to complete the milestone";
      success_msg = 'Milestone Completed!';
      notification_message = 'You have successfully completed the milestone!';
    }else if($('input[name=status]').val() == 4){
      msg = "You want to approve the milestone";
      success_msg = 'Milestone Approved!';
      notification_message = 'You have successfully approved the milestone!';
    }
		swal({
			title: 'Are you sure?',
			text: msg,
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes',
			closeOnConfirm: false,
			
			},function(isConfirm){
				if (isConfirm){
         
					$.ajaxSetup({
						headers: {'X-CSRF-TOKEN': $('input[name=_token]').val()}
					});
					$.ajax({
						type: "POST",
						url: "{{route('project.milestoneStatusChange')}}",
						data: $('#milestone_submit').serialize(),
						success:function(){
						
							swal({
								title: success_msg,
								text:  notification_message,
								type: "success"
							},function(isConfirm){

								if (isConfirm){
                  
                  location.reload();
								}
							});
							
							
						}
					});
				

      			} 
			}
		);

		
	});
</script>
@endpush