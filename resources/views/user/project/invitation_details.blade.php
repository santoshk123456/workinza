@extends('user.layouts.user_two')
@section('title', set_page_titile($project->title))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')

<main class="main-wrapper">
    <div class="container">
        <div class="inner-pg-layout freelancer-project-view">      
            <div class="details-col mb-3">
                <div class="row">
                    <div class="col-lg-8 pr-lg-0 mb-3 mb-lg-0">
                        <div class="bg-shadow project-desp-col">
                            <h4 class="mb-2">{{ucwords($project->title)}} </h4>  
                            <p>{{$project->description}}</p>
                            <div class="row">
                                <div class="col-md-5 mb-3 mb-xl-0">                        
                                    <div class="col-list-item">    
                                        <div class="media">
                                            @if($project->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$project->user->profile_image)))
                                                <img class="mr-3 thumb"  src="{{ asset('storage/user/profile-images/'.$project->user->profile_image) }}" alt="{{ $project->user->username }}">
                                            @else 
                                                <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $project->user->username }}" class="mr-3 thumb">
                                            @endif
                         
                                            <div class="media-body">  
                                            <?php 
                            $status ="null";
                            ?>
                                                <a href="{{ route('account.viewUserDetail',['user'=>$project->user,'project'=>$project,'status'=>$status]) }}"> <h5 class="mt-0 name">{{$project->user->username}} 
                                                    @if($project->user->organization_type == 0 )
                                                    
                                                    <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                                                @else
                                                    <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                                                @endif</h5></a>
                                                <p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>{{$project->location}}</p>
                                                <div class="ratings"><div class="rate-count">@if(!empty($project->user->userDetail->avg_rating)){{$project->user->userDetail->avg_rating}}@else 0 @endif</div>	<div class="user-rating" data-value="@if(!empty($project->user->userDetail->avg_rating)){{$project->user->userDetail->avg_rating}}@else 0 @endif"></div></div>
                                            </div>                                            
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-md-7 text-md-right">
                                    <div class="row">
                                        <div class="col-xl-8 col-md-12 mb-3 mb-xl-0">
                                            <p>Average Proposal Amount :<span> {{formatAmount($average_proposal_amount)}} </span> </p>
                                            <div class="">                          
                                                <p class="d-inline-block mb-0">Status<span class="notify-col">{{projectStatus($project->status)}}</span></p>
                                                
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-md-12">
                                            <div class="btn-cols text-xl-left"> 
                                                @php 
                                                $invite = $projectInvitations->where('project_id',$project->id)->first();
                                                @endphp        
                                                <button class="btn bg-nm-btn mb-xl-2" type="submit" onclick="window.location.href='{{route('project.createProposal',$project->uuid)}}'"><i class="fi-flaticon flaticon-send mr-2"></i>
                                                 @if(isset($invite))Accept @else Apply @endif
                                                </button>
                                                @if(App\ProjectChat::ChatFeature()==1)
                                                <button class="btn white-nm-btn" type="submit" onclick="window.location.href='{{route('message.messages',array($project->uuid,$project->user->uuid))}}'"><i class="fi-flaticon flaticon-comments mr-2"></i>Message</button>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="bg-shadow project-proposals-col">
                            <ul class="d-block">
                                <li><p>Project Type</p><span>{{projectType($project->type)}}</span></li>
                                <li><p>Budget</p><span>{{formatAmount($project->budget_from)}} - {{formatAmount($project->budget_to)}}</span></li>
                                <li><p>Proposal Start Date</p><span>{{hcms_date(strtotime($project->bid_start_date), 'date', false)}}</span></li>
                                <li><p>Proposal End Date</p><span>{{hcms_date(strtotime($project->bid_end_date), 'date', false)}}</span></li>
                                <li><p>Total Proposals</p><span>@if(!empty($proposals)){{count($proposals)}}@else {{__('0')}}@endif</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="seperated-cols bg-shadow mb-3 delivery-expect">
                <div class="row">
                    <div class="col-sm-4 seperator mb-2 mb-sm-0">
                        <p class="mb-0">Project Start Date <span><strong> {{hcms_date(strtotime($project->expected_start_date), 'date', false)}}</strong></span></p>
                    </div>
                    <div class="col-sm-4 seperator mb-2 mb-sm-0">
                        <p class="mb-0">Project End Date <span><strong>{{hcms_date(strtotime($project->expected_end_date), 'date', false)}}</strong></span></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="mb-0">Time Line <span><strong> {{getTimeline($project->timeline_count,$project->timeline_type)}}</strong></span></p>
                    </div>
                </div>
            </div>
            <div class="seperated-cols bg-shadow mb-3 features-special">
                <div class="row">
                    <div class="col-sm-4 seperator mb-2 mb-sm-0">
                        <p class="mb-0">Industry : <span> {{ !empty($project->industries) ? implode(', ', $project->industries->pluck('name')->toArray()) : __('NA') }}</span></p>
                    </div>
                    <div class="col-sm-4 seperator mb-2 mb-sm-0">
                        <p class="mb-0">Category : <span> {{ !empty($project->categories) ? implode(', ', $project->categories->pluck('name')->toArray()) : __('NA') }}</span></p>
                    </div>
                    <div class="col-sm-4">
                        <p class="mb-0">Sub Category : <span> {{ !empty($project->subCategories) ? implode(', ', $project->subCategories->pluck('name')->toArray()) : __('NA') }}</span></p>
                </div>
            </div>
        </div>
        <div class="seperated-cols bg-shadow mb-3 features-special">
            <div class="row">
                <div class="col-sm-4  mb-2 mb-sm-0">
                    <p class="mb-0">Skills and Expertise : <span> {{ !empty($project->skills) ? implode(', ', $project->skills->pluck('name')->toArray()) : __('NA') }}</span></p>
                </div>
            </div>
            {{-- <h6 class="mb-0">Skills and Expertise</h6>   
            <div class="skils-cols">
                @foreach($project->skills->pluck('name') as $skill)
                    <span >{{$skill}}</span>
                @endforeach  
            </div>                      --}}
        </div>
        <div class="seperated-cols bg-shadow mb-3">
            <h5 class="sub-title">Documents</h5>
            @if(!empty($documents))
					@foreach($documents as $document)
                        <div class="document-col my-3">
                            <span class="file-name"><i class="sprites-col acrobit-icon mr-2"></i>{{str_limit(ucwords($document->file_name), $limit = 20, $end = '...')}}</span>
                            <div class="btns-col">
                                <button class="btn white-nm-btn view" onclick="NewTab('{{asset('storage/projects/project_documents/'.$document->file_name)}}')"><i class="fi-flaticon flaticon-eye"></i></button>
                                <a href="{{asset('storage/projects/project_documents/'.$document->file_name)}}" class="btn white-nm-btn download" download="{{$document->file_name}}" ><i class="fi-flaticon flaticon-download"></i></a>         
                            </div>
                        </div>
                    @endforeach
             @else
                <div class="document-col my-3">
                    {{__("No document found")}}
                </div>
            @endif
        </div>
        {{-- <div class="seperated-cols bg-shadow mb-3">
            <h5 class="sub-title">Milestones</h5>
            @if(count($project->milestones)>0)
				@foreach($project->milestones as $milestone)
                    <div class="milestones-col">
                        <div class="head-top">
                            <h6>{{ $milestone->milestone_name}}  <span class="notify-col">{{getMilestoneStatus($milestone->status)}}</span></h6>
                            <span class="milestone-price">{{ formatAmount($milestone->milestone_amount)}}</span>
                        </div>
                        <p>{{$milestone->milestone_description}}</p>
                    </div>
                @endforeach
            @else
                <div class="head-top">
                    <h6>{{__("No milestone available")}} 
                </div>
            @endif
                
          </div>  --}}
          @if(count($project->milestones)>0)
          <div class="bg-shadow descp-table milestone-table-col">
              <div class="table-multi-common table-responsive">
                <table class="table table-striped mb-0">
                  <thead>
                    <tr>
                      <th scope="col" class="sub-title">Milestones</th>
                      <th scope="col"  class="sub-title">Amount</th>        
                    </tr>
                  </thead>
                  <tbody>
                      @foreach($project->milestones as $milestone)
                      <tr>
                          <td>      
                              <div class="milestones-col">
                                  <div class="head-top mb-3">
                                      <p class="d-inline-block mr-3">{{ ucwords($milestone->milestone_name)}}</p>       
                                      <div class="btns-div d-inline-block">   
                                          
                                          <button onclick="window.location.href='{{route('project.MilestoneDetails',$milestone)}}'" class="btn white-nm-btn btn-small" type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button>                        
                                      </div>              
                                  </div>
                                  <p>{{$milestone->milestone_description}}.</p>
                                </div>
                              <ul class="dates-col my-2 my-md-3">
                                  <li>Start Date {{hcms_date(strtotime($milestone->start_date), 'date', false)}}</li>
                                  <li>End Date {{hcms_date(strtotime($milestone->end_date), 'date', false)}}</li>
                              </ul>
                          </td>
                          <td>{{ formatAmount($milestone->milestone_amount)}}</td>               
                      </tr>
                      @endforeach
                    
                    <tr class="footer-table">
                      <td>Total Amount:</td>
                      <th>{{ formatAmount($total_milestone_amount)}}</th>
                    </tr>
                  </tbody>
                </table>
              </div>
              <p class="d-sm-none d-block pt-3">Scroll to view amount</p>
            </div>
            @endif
             
        </div>
    </div> 
</main>
@push('custom-scripts')
    <script>
        function NewTab(url) { 
            window.open( url, "_blank");
        }
        
    </script>

@endpush
@endsection