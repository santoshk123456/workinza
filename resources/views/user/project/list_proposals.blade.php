

@extends('user.layouts.user_two')
@section('title', set_page_titile(__('In Progress Projects')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')

<main class="main-wrapper">
  <div class="container inner-pg-layout project-layouts">
     <!-- main page title:start -->
     <div class="row m-0">
        <div class="col-lg-12">
           <h3 class="title-main">My Proposals</h3>
        </div>
     </div>
     <!-- main page title:end -->
     <div class="row m-0 mt-4">
      <div class="col-lg-3 left-aside left-projects-list">         
         <ul class="d-block">
          <li class="active"><a href="{{route('project.listProposals')}}">My Proposals</a></li>
          <li><a href="{{route('project.freelancerlistInProgressProjects')}}">Assigned / In Progress Projects</a></li>
          <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
          <li><a href="{{route('project.listInvitations')}}">Invitations</a></li>
         </ul>
     </div>
        <!-- projects search listing: start -->
        <div class="col-lg-9">
         <div id="filter-overlay"></div>
           <div class="project-search-listing invitaion-list">
            @php 
               $status = '';
               $firstDate = '';
               $url = $_SERVER['REQUEST_URI'];
               $parts = parse_url($url);
               if(isset($parts['query']))
               {
                  parse_str($parts['query'], $query);
                  if(isset($query['status'])){
                  $status = $query['status'];
                  }
                  if(isset($query['big_end_date'])){
                  $firstDate = $query['big_end_date'];
                  }
               }
              @endphp 
            <form method="GET" action="{{route('project.listProposals')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
              <div class="search-bar bg-shadow">
                 <div class="row">
                    <div class="col-md-10">
                       <div class="input-group project-search">
                        <input value="{{old('title')}}" type="text" minlength="3" maxlength="200"  name="title" class="form-control border-right-0" placeholder="Search project by name or keyword" aria-label="Search project  by name or keyword">
                          <div class="input-group-append">
                             <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-2 project-search-top">
                     <button class="btn white-btn filter-btn" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><span class="sprites-col filter-icon"></span> Filters</button>
                
                    </div>
                    
                 </div>
                 <div class="filter-col collapse" id="collapseExample">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" name="status" id="status" required>
                  
                          <option  value="" disabled selected value="">Select Status</option>
                          <option value="0" @if($status == "0") selected @endif>{{__('Submitted')}}</option>
                          <option value="1" @if($status == "1") selected @endif>{{__('Accepted')}}</option>
                          <option value="2" @if($status == "2") selected @endif>{{__('Rejected')}}</option>                         
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="datepicker">Proposal End Date</label>
                        <input value="{{$firstDate}}" type="text" id="datepicker" name="big_end_date" class="form-control"  aria-describedby="datepicker" placeholder="DD/MM/YY">
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="btn-cols">                              
                        <button class="btn submit-btn" type="submit">Apply</button>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="btn-cols">                              
                        <button class="btn white-btn" id="reset-btn" type="button">Clear</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
      

              @if(!$proposals->isEmpty())
                @foreach ($proposals as $proposal)
  
                <div class="col-list-item invitaion-list bg-shadow mb-3">
                   <div class="row">
                      <div class="col-xl-8">
                         <h4 class="">{{ucwords($proposal->project->title)}}</h4>
                         @php
                        $industry =  !empty($proposal->project->industries) ? implode(', ', $proposal->project->industries->pluck('name')->toArray()) : __('NA') 
                          @endphp
                         <span class="industry"> {{$industry}}</span>
                         <ul class="duration-list">
                            <li>
                             <p class="mb-2 seperator">Budget: <strong>{{formatAmount($proposal->project->budget_from)}} - {{formatAmount($proposal->project->budget_to)}}</strong> </p>
                            </li>                     
                            <li>
                               <p class="mb-2 seperator">Proposed Amount: <strong>{{formatAmount($proposal->proposal_amount)}}</strong> </p>
                            </li>
                            <li>
                             <p class="mb-2">Status: @if($proposal->status == 1)<span class="notify-col green-notify">@else<span class="notify-col">@endif{{getProposalStatus($proposal->status)}}</span>                                
                            </p>
                          </li>
                         </ul>
                      </div>
                      <div class="col-xl-4">
                         <ul class="posted-list text-right">
                            <li>
                               <p class="mb-2">Submitted On  {{timeAgoConversion(($proposal->created_at))}}</p>
                            </li>              
                         </ul>
                         <div class="btn-cols text-xl-right mb-3 ">
                            <button class="btn bg-nm-btn mr-2" onclick="location.href='{{route('project.viewProposal',$proposal)}}'"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button>    
                            @if(App\ProjectChat::ChatFeature()==1)             
                            <button class="btn white-nm-btn" onclick="location.href='{{route('message.messages',array($proposal->project->uuid,$proposal->project->user->uuid))}}'" type="submit"><i class="fi-flaticon flaticon-comments mr-2"></i>Message</button>
                           @endif
                           </div>
                      </div>
                   </div>
                   <p class="description">{{$proposal->project->description}}</p>
   
                   <div class="media">
  
        
  
           @if($proposal->project->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$proposal->project->user->profile_image)))
              <img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('storage/user/profile-images/'.$proposal->project->user->profile_image) }}"  alt="{{ $proposal->project->user->full_name }}">
          @else 
              <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $proposal->project->user->full_name }}" class="mr-md-3 thumb mb-3 mb-md-0">
          @endif
  
  
                      <!-- <img class="mr-3 thumb mb-3 mb-md-0" src="https://randomuser.me/api/portraits/women/63.jpg" alt="Generic placeholder image"> -->
                      <div class="media-body">

                           <?php 
                            $status ="null";
                            ?>
                        <a href="{{ route('account.viewUserDetail',['user'=>$proposal->project->user,'project'=>$proposal->project,'status'=>$status]) }}">  <h5 class="mt-0 name">
                         {{$proposal->project->user->username}} 
                         @if($proposal->project->user->organization_type == 0 )
                                                    
                         <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                     @else
                         <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                     @endif</i></h5>     </a>
                         
                         <p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-1"></i></span>@if(isset($proposal->user->userDetail->city) && isset($proposal->user->userDetail->state) && $proposal->user->userDetail->country->name){{$proposal->user->userDetail->city}} , {{$proposal->user->userDetail->state}}, {{$proposal->user->userDetail->country->name}} @else {{__('NA')}}@endif</p>
                         <div class="ratings">
                            <div class="rate-count">@if(!empty($proposal->project->user->userDetail->avg_rating)){{$proposal->project->user->userDetail->avg_rating}}@else 0 @endif</div>
                            <div class="user-rating" data-value="@if(!empty($proposal->project->user->userDetail->avg_rating)){{$proposal->project->user->userDetail->avg_rating}}@else 0 @endif"></div>
                         </div>
                      </div>
  
  
  
                      
                   </div>
                </div>
  
  
                @endforeach
  
                @else
                <div class="col-list-item invitaion-list bg-shadow mb-3">
                   <div class="row">
                      <div class="col-xl-8">
              <i>  {{__('No Propsal found')}} </i>
                </div>
              </div>
              </div>
                @endif
  
                <!-- search list item:end -->
  
               
              <!-- search list item:end -->
              
         
              <!-- search list item:end -->
  
  
  
              <div class="pagination-col mt-4 mb-3">
                      <nav aria-label="page navigation">
                        <ul class="pagination justify-content-end pagination-sm flex-wrap">
          
                        {{ $proposals->links('vendor.pagination.customnew') }}
  
                        </ul>
                      </nav>
                    </div>
  
             </div>
          </div>
          <!-- projects search listing: start -->
       </div>
    </div>
  </main>
  <!--end: contact information page -->
  
  
  
  
  
  @endsection
  @push('custom-scripts')
  <script> 
  $( "#reset-btn" ).click(function() {
      $('#status').val('').change();
  
      //setting specific value in select2
      $('#status').val('').change();
      window.location.href = "{{route('project.listProposals')}}";
  });
  </script>
  
  @endpush
  
 
  