@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Create Project')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush
@section('content')

<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">

        <!-- main page title:start -->
        <div class="row m-0">
            <div class="col-sm-6">
                <h3 class="title-main">{{__('Post a Project') }}</h3>
            </div>
            <div class="col-sm-6 text-sm-right mt-sm-0 mt-3">
                <button class="btn cancel-shadow-btn" onclick="window.location.href='{{route('project.listOpenProjects')}}'" type="submit"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Project</button>
            </div>
        </div>
        <!-- main page title:end -->

        <div class="row m-0 mt-4">
            <!-- right section:start -->
            <div class="col-lg-8">
                <form method="post" class="form-validate" enctype="multipart/form-data" id="create-project-form" action="{{route('project.storeProject')}}">
                    @csrf

                    <!-- form - project details:start -->
                    <div class="form-block bg-shadow">
                        <div class="block-header">
                            <div class="inner">
                                <h4 class="block-title">{{__('Project Details') }}</h4>
                            </div>
                        </div>
                        <div class="block-content">
                          
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="title" class="control-label">{{__('Title') }}</label>

                                            <input value="{{old('title')}}" type="text" name="title" id="title" class="form-control" id="title" minlength="3" maxlength="150" placeholder="{{__('Enter Project Title')}}" required>
                                            <span id="title-error" class="error-txt title-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="location" class="control-label">{{__('Location') }}</label>
                                            <input value="{{old('location')}}" type="text" name="location" class="form-control" id="location" minlength="3" maxlength="150" placeholder="{{__('Enter Location')}}" required>
                                            <span id="title-location" class="error-txt location-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            <label for="description" class="control-label">{{__('Description') }}</label>

                                            <textarea name="description" placeholder="{{__('Enter Description')}}" id="description" class="form-control" rows="3" required>{{old('description')}}</textarea>
                                            <span id="description-error" class="error-txt description-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group required">
                                            <label for="project_documents" class="control-label">{{__('Project Documents (Upto 5 Files)') }}</label>
                            
                                            <div class="fallback dropzone dropzone_docs"></div>
                                            <div class="imp_images"></div>
                                        </div>
                                    </div>
                                </div>
                          
                        </div>
                    </div>
                    <!-- form - project details:end -->

                    <!-- form - Required Expertise Details:start -->
                    <div class="form-block bg-shadow mt-4">
                        <div class="block-header">
                            <div class="inner">
                                <h4 class="block-title">{{__('Required Expertise Details') }}</h4>
                            </div>
                        </div>
                        <div class="block-content">
                         
                          
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label for="title" class="control-label">{{__('Industry') }}</label>
                                            <select name="industry_ids[]" id="industry_ids" multiple="multiple" class="form-control" >
                                              
                                                @foreach ($industries as $industry)
                                                <option value="{{$industry->id}}" {{!empty(old('industry_ids')) ? in_array($industry->id,old('industry_ids')) ? 'selected' : '' : ''}}>{{$industry->name}}</option>
                                                @endforeach
                                            </select>
                                            <span id="industries-error" class="error-txt industries-error"></span>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label for="title" class="control-label">{{__('Category')}}</label>
                                            <select name="categories_ids[]" id="category_ids" class="form-control" multiple="multiple" >
                                             
                                             
                                                @if(!empty($categories))
                                                @foreach ($categories as $key=>$category)
                                                <option value="{{$category->id}}" {{!empty(old('categories_ids')) ? in_array($key,old('categories_ids')) ? 'selected' : '' : ''}}>{{$category->name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            <span id="categories-error" class="error-txt categories-error"></span>
                                        </div>
                                    </div>
                                    @php
                                    if(!empty(old('categories_ids'))){
                                    $old_category = old('categories_ids');
                                    $old_sub_categories = App\SubCategory::whereHas('categories',function($q) use($old_category){
                                    $q->whereIn('id',$old_category);
                                    })->where(['active'=>1])->pluck('name','id');
                                    }else{
                                    $old_sub_categories = [];
                                    }
                                    @endphp
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label for="title" class="control-label">{{__('Sub Category') }}</label>
                                            <select name="sub_categories_ids[]" id="sub_category_ids" class="form-control" multiple="multiple" >
                                             
                                                @if(!empty($old_sub_categories))
                                                @foreach ($old_sub_categories as $key=>$sub_category)
                                                <option value="{{$key}}" {{!empty(old('sub_categories_ids')) ? in_array($key,old('sub_categories_ids')) ? 'selected' : '' : ''}}>{{$sub_category}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            <span id="sub-categories-error" class="error-txt sub-categories-error"></span>
                                        </div>
                                    </div>
                                    @php
                                    if(!empty(old('sub_categories_ids'))){
                                    $old_sub_category = old('sub_categories_ids');
                                    $old_skills = App\Skill::whereHas('subCategories',function($q) use($old_sub_category){
                                    $q->whereIn('id',$old_sub_category);
                                    })->where(['active'=>1])->pluck('name','id');
                                    }else{
                                    $old_skills = App\Skill::where(['active'=>1])->pluck('name','id');
                                    }
                                    @endphp
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <label for="title" class="control-label">{{__('Skills') }}</label>
                                            <select name="skills[]" id="skill_ids" class="form-control" multiple="multiple" >
                                            
                                                @if(!empty($old_skills))
                                                @foreach ($old_skills as $key=>$skill)
                                                <option value="{{$key}}" {{!empty(old('skills')) ? in_array($key,old('skills')) ? 'selected' : '' : ''}}>{{$skill}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            <span id="skills-error" class="error-txt skills-error"></span>
                                        </div>
                                    </div>
                                </div>
                           
                        </div>
                    </div>
                    <!-- form - Required Expertise Details:end -->



                    <!-- form - Proposal Details:start -->
                    <div class="form-block bg-shadow mt-4">
                        <div class="block-header">
                            <div class="inner">
                                <h4 class="block-title">{{__('Proposal Details') }}</h4>
                            </div>
                        </div>
                        <div class="block-content">
                         
                           
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="title" class="w-100">{{__('Project Type') }}</label>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="type" id="type_fixed_rate" value="0" {{old('type')==0?'checked':''}} required>
                                                <label class="form-check-label" for="type_fixed_rate">{{__('Fixed rate') }}</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="type" id="type_hourly_rate" value="1" {{old('type')==1?'checked':''}} required>
                                                <label class="form-check-label" for="type_hourly_rate">{{__('Hourly rate') }}</label>
                                            </div>




                                        </div>
                                    </div>

                         
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="title" class="control-label">{{__('Budget') }}</label>
                                            <select class="form-control" name="budget_range" id="budget_range" required>
                                              <option disabled selected value=""> Select Budget</option>
                                              <option value="0-100000" >{{__('Below USD 100000') }}</option>
                                                <option value="100000-200000" >{{__('USD 100000 - USD 200000') }}</option>
                                                <option value="200000-400000">{{__('USD 200000 - USD 400000') }}</option>
                                                <option value="400000-600000" >{{__('USD 400000 - USD 600000') }}</option>
                                                <option value="600000-1000000" >{{__('USD 600000 - USD 1000000') }}</option>
                                                <option value="1000000-2000000" >{{__('USD 1000000 - USD 2000000') }}</option>
                                                <option value="2000000-4000000" >{{__('USD 2000000 - USD 4000000') }}</option>
                                                <option value="4000000-100000000" >{{__('Above USD 4000000') }}</option>
                                            </select>
                                            
                                            <span id="errorbudget"></span>
                                            <span id="budget-from-error" class="error-txt budget-from-error"></span>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="title" class="control-label">{{__('Budget To') }} </label>
                                          
                                            <input value="{{old('budget_to')}}" type="number" name="budget_to" class="form-control no-inc numericOnly" id="budget_to" min="1" max="9999999999" placeholder="{{__('Enter Budget To Amount')}}" required>
                                            <span id="budget-to-error" class="error-txt budget-to-error"></span>
                                        </div>
                                    </div> -->

                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="project-start-date" class="control-label">{{__('Proposal Start Date') }}</label>
                                           
                                            <input type="text" value="{{old('bid_start_date')}}" name="bid_start_date" class="form-control common-date-pick" required autocomplete="off" id="bid_start_date" placeholder="{{ __('Select Date') }}">
                                            <span id="bid-start-error" class="error-txt bid-start-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="project-end-date" class="control-label">{{__('Proposal End Date') }}</label>
                                          
                                            <input type="text" value="{{old('bid_end_date')}}" name="bid_end_date" class="form-control common-date-pick" required autocomplete="off" id="bid_end_date" placeholder="{{ __('Select Date') }}">
                                            <span id="bid-end-error" class="error-txt bid-end-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="title" class="control-label">{{__('Project Start Date') }}</label>
                                          
                                            <input type="text" value="{{old('expected_start_date')}}" name="expected_start_date" class="form-control" required autocomplete="off" id="expected_start_date" placeholder="{{ __('Select Date') }}">
                                            <span id="expected-start-error" class="error-txt expected-start-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="title" class="control-label">{{__('Project End Date') }} </label>
                                          
                                            <input type="text" value="{{old('expected_end_date')}}" name="expected_end_date" class="form-control common-date-pick" required autocomplete="off" id="expected_end_date" placeholder="{{ __('Select Date') }}">
                                            <span id="expected-end-error" class="error-txt expected-end-error"></span>
                                        </div>
                                    </div>
                           
                                    <div class="col-md-6">
                                        <div class="form-group required">
                                            <label for="time-line" class="control-label">{{__('Timeline')}}</label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input value="{{old('timeline_count')}}" type="number" name="timeline_count" class="form-control no-inc numericOnly" id="timeline_count" min="1" max="9999" placeholder="{{__('Count of hour, days, week or months')}}" required>
                                                    <span id="timeline-count-error" class="error-txt timeline-count-error"></span>
                                                   
                                                </div>
                                                <div class="col-md-6">

                                                    <select name="timeline_type" class="form-control mb-2" id="timeline_type" required>
                                                        <option value="">{{__('Timeline Type') }}</option>
                                                        <option value="Hours" {{old('timeline_type')=="Hours" ? 'selected' : ''}}>{{__('Hours') }}</option>
                                                        <option value="Days" {{old('timeline_type')=="Days" ? 'selected' : ''}}>{{__('Days') }}</option>
                                                        <option value="Weeks" {{old('timeline_type')=="Weeks" ? 'selected' : ''}}>{{__('Weeks') }}</option>
                                                        <option value="Months" {{old('timeline_type')=="Months" ? 'selected' : ''}}>{{__('Months') }}</option>
                                                    </select>
                                                    <span id="timeline-type-error" class="error-txt timeline-type-error"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>
                                <!-- </form> -->
                          
                        </div>
                    </div>
                    <!-- form - Proposal Details:end -->



                    <!-- form - Required Expertise Details:start -->
                    <div class="form-block bg-shadow mt-4">
                        <div class="block-header">
                            <div class="inner">
                                <h4 class="block-title">{{__('Milestone Details') }}</h4>
                            </div>
                        </div>
                        <div class="block-content">





                            <!-- milestone input:start -->
                            <div class="milestone-box">
                                <!-- <form> -->
                               
                                    <table class="">
                                        <tbody>

                                            @if(!empty(old('milestone_names')))
                                            @foreach(old('milestone_names') as $key=>$milestone)
                                            <tr class="milestone-inner-col">
                                                <td scope="row" class="">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group required">
                                                                <label for="title" class="control-label">{{__('Title') }}</label>

                                                                <input type="text" name="milestone_names[]" class="form-control milestone_names" value="{{$milestone}}" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Eg:Milestone1')}}" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group required">
                                                                <label for="" class="control-label">{{__('Start Date') }}</label>

                                                                <input type="text" name="milestone_start_dates[]" class="form-control milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Start Date') }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group required">
                                                                <label for="" class="control-label">{{__('End Date') }}</label>

                                                                <input type="text" name="milestone_end_dates[]" class="form-control milestone_end_date common-date-pick" value="{{old('milestone_end_dates')[$key]}}" required autocomplete="off" placeholder="{{ __('Select End Date') }}" id="milestone_end">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group required">
                                                                <label for="" class="control-label">{{__('Amount') }}</label>

                                                                <input type="number" name="milestone_amounts[]" class="form-control no-inc numericOnly" value="{{old('milestone_amounts')[$key]}}" min="1" max="9999999999"  placeholder="{{__('Enter the Amount')}}" required>
                                                                <span id="error"></span> 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group required">
                                                                <label for="description" class="control-label">{{__('Description')}}</label>

                                                                <textarea name="milestone_descriptions[]" id="milestone_descriptions" class="form-control" placeholder="Milestone Description" required>{{old('milestone_descriptions')[$key]}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="border-0 text-right">
                                                    <button class="btn btn-success add_more mr-2" type="button"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Milestone</button><button type="button" class="btn ml-2 remove_row white-nm-btn white-nm-btn border">Remove Milestone</button>
                                                </td>
                                            </tr>

                                            @endforeach
                                            @else
                                            <tr class="">
                                                <td scope="row" class="">
                                                    <div class="row milestone">
                                                        <div class="col-md-4">
                                                            <div class="form-group required">
                                                                <label for="title" class="control-label">{{__('Milestone') }}</label>

                                                                <input type="text" name="milestone_names[]" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Eg:Milestone1')}}" required>
                                                                <!-- <span  id="milestone-title-error" class="error-txt milestone-title-error"></span> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group required">
                                                                <label for="project-start-date" class="control-label">{{__('Start Date') }}</label>
                                                                <!-- <input type="text" name="milestone_names[]" class="form-control milestone_names" minlength="2" maxlength="191" id="milestone_names" placeholder="{{__('Name')}}" required> -->
                                                                <input type="text" name="milestone_start_dates[]" class="form-control no-type milestone_start_date" required autocomplete="off" placeholder="{{ __('Select Start Date') }}" id="milestone_start_date">
                                                                 <!-- <span  class="error-txt milestone_start_date"></span>  -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group required">
                                                                <label for="project-end-date" class="control-label">{{__('End Date') }}</label>

                                                                <input type="text" name="milestone_end_dates[]" class="form-control no-type milestone_end_date" required autocomplete="off" placeholder="{{ __('Select End Date') }}" id="milestone_end_date">
                                                                <!-- <span  class="error-txt milestone-end-error"></span> -->
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group required">
                                                                <label for="amount" class="control-label">{{__('Amount') }}</label>

                                                                <input type="number" name="milestone_amounts[]" class="form-control no-inc numericOnly milestone_amount" min="1" max="9999999999"  placeholder="{{__('Enter the Amount')}}" required id="milestone_amount">
                                                                <!-- <span id="milestone-amount-error" class="error-txt milestone-amount-error"></span> -->
                                                                <span id="error"></span> 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group required">
                                                                <label for="description" class="control-label">{{__('Description') }}</label>

                                                                <textarea name="milestone_descriptions[]"  class="form-control milestone_description" placeholder="Milestone Description" required id="milestone_description"></textarea>
                                                                <!-- <span  id="milestone-description-error"class="error-txt milestone-description-error"></span> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="remove-buttons border-0 text-right">
                                                    <button class="btn bg-nm-btn add_more mr-2" type="button"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Milestone</button>
                                                    <button type="button" style="display:none" class="btn remove_row white-nm-btn border">Remove Milestone</button>
                                                </td>

                                            </tr>

                                            @endif
                                        </tbody>
                                    </table>



                               
                            </div>
                            <!-- milestone input:end -->
                         

                        </div>
                    </div>
                    <!-- form - Required Expertise Details:end -->
                    <div class="row final-submit">
                        <div class="col-12 text-center">
                            <button class="btn bg-nm-btn mt-4 mb-4" type="submit">{{__('Submit Project') }}</button>
                        </div>
                    </div>

                </form>
            </div>
            <!-- right section:start -->


            <!-- left section:start -->
            <div class="col-lg-4">

                <!-- benifits:start -->
                @if((count(Auth()->user()->userSubscription)!=0))
                <div class="form-block bg-shadow">
                    <div class="block-header">
                        <div class="inner">
                            <h4 class="block-title">{{__('Benefits of this membership plan') }}</h4>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="benifit-list">
                        <p><i class="fi-flaticon flaticon-tick-sign mr-2"></i>Number of Projects Post: <span>{{Auth()->user()->userSubscription[0]->remaining_project_count}}/{{Auth()->user()->userSubscription[0]->membership->number_of_projects}}</span></p>
                            <p><i class="fi-flaticon flaticon-tick-sign mr-2"></i>Remining Number of Proposal a Project Can Receive : <span>{{$user_subscription->remaining_proposal_count}}/{{Auth()->user()->userSubscription[0]->membership->number_of_proposals}}</span></p>
                            <p><i class="fi-flaticon flaticon-tick-sign mr-2"></i>Chats: <span>@if(Auth()->user()->userSubscription[0]->membership->chat_feature==1) {{__('Yes')}} @else {{__('No')}} @endif</span></p>
                        </div>
                    </div>
                </div>
                @endif
                <!-- benifits:end -->


                <!-- contact:start -->
                <div class="form-block bg-shadow mt-4">
                    <div class="block-header">
                        <div class="inner">
                            <h4 class="block-title">{{__('Contact Details') }}</h4>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="contact-list">
                            <p><i class="fi-flaticon flaticon-phone-call mr-2"></i>@if(!empty(App\Setting::getSettingValue('contact_number'))) {{App\Setting::getSettingValue('contact_number')}} @else NA @endif</p>
                            <p><i class="fi-flaticon flaticon-email mr-2"></i>@if(!empty(App\Setting::getSettingValue('application_admin_email'))) {{App\Setting::getSettingValue('application_admin_email')}} @else NA @endif </p>
                        </div>
                    </div>
                </div>
                <!-- contact:end -->

            </div>
            <!-- left section:start -->
        </div>
    </div>
</main>


@endsection
@push('custom-scripts')
<script>
$(document).on("blur", '.milestone_amount', function() {
    
// for validating milestone amount with budget amount

var inputs = $(".milestone_amount");
var total_amount = 0;

for (var i = 0; i < inputs.length; i++) {
    total_amount = parseFloat(total_amount) + parseFloat($(inputs[i]).val());
}

$('#total_amount').val(total_amount);
var budgets = $('#budget_range').find(":selected").val();
var budget = budgets.split('-');
if(budget[1] < total_amount){

        error.innerHTML = "<span style='color: #fe4848; font-size:13px;'>"+ 
                        "Amount is greater than budget </span>"

}

 // for reverse validating of budget amount with total milestone amount

$( "#budget_range" ).change(function() {
    if(budget[1] < total_amount){

errorbudget.innerHTML = "<span style='color: #fe4848; font-size:13px;'>"+ 
                "Budget is less than total amount of milestones </span>"

}else{
    $('#errorbudget').remove();
}
});


});



    //   function dateselector(id)
    //   {
    //       alert(id);
    //       $('#'+id).datepicker().datepicker( "show" )

    //   }
    $(document).ready(function() {
      
        $('#industry_ids').select2({
            
            placeholder: "{{__('Select Industries')}}",
        });
        $('#category_ids').select2({
            placeholder: "{{__('Select Categories')}}",
        });
        $('#sub_category_ids').select2({
            placeholder: "{{__('Select Sub Categories')}}",
        });
        $('#skill_ids').select2({
            placeholder: "{{__('Select Skills')}}",
        });

        function datepickerRemover(input){
            $(input).parent().children('i').remove();
            $(input).removeClass('gj-textbox-md');
            $(input).removeAttr('data-type');
            $(input).removeAttr('data-guid');
            $(input).removeAttr('data-datepicker');
            $(input).parent().parent().append($(input).parent().html());
            $(input).parent().parent().children('div').remove();
        }

        var datepicker2 = $('#bid_end_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            minDate: new Date(),
            change: function (e) {
                datepickerRemover('#expected_start_date');
                var startDate = new Date(e.target.value);
                console.log(startDate);
                $('#expected_start_date').datepicker({
                    minDate: startDate,
                });
            }
           
        });
        $('#bid_start_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            minDate: new Date(),
            change: function (e) {
                datepickerRemover('#bid_end_date');
                var startDate = new Date(e.target.value);
                $('#bid_end_date').datepicker({
                    minDate: startDate,
                    change: function (e) {
                        datepickerRemover('#expected_start_date');
                        datepickerRemover('#expected_end_date');
                        var startDate = new Date(e.target.value);
                        console.log(startDate);
                        startDate.setDate(startDate.getDate() + 1);
                        $('#expected_end_date').datepicker({
                            minDate: startDate,
                        });
                        $('#expected_start_date').datepicker({
                            minDate: startDate,
                            change: function (e) {
                                datepickerRemover('#milestone_start_date');
                                datepickerRemover('#milestone_end_date');
                                datepickerRemover('#expected_end_date');
                                var startDate = new Date(e.target.value);
                                $('#milestone_start_date').datepicker({
                                    minDate: startDate,
                                    change: function (e) {
                                        datepickerRemover('#milestone_end_date');
                                        var startDate = new Date(e.target.value);
                                        $('#milestone_end_date').datepicker({
                                            value:'',
                                            minDate: startDate,
                                        });
                                    }
                                });
                                $('#milestone_end_date').datepicker({
                                    minDate: startDate,
                                });
                                $('#expected_end_date').datepicker({
                                    minDate: startDate,
                                });
                            }
                        });
                    }
                });
            }
        });

        


        $('#expected_start_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            change: function (e) {
                datepickerRemover('#milestone_start_date');
                datepickerRemover('#milestone_end_date');
                var startDate = new Date(e.target.value);
                $('#milestone_start_date').datepicker({
                    minDate: startDate,
                    change: function (e) {
                        datepickerRemover('#milestone_end_date');
                        var startDate = new Date(e.target.value);
                        $('#milestone_end_date').datepicker({
                            value:'',
                            minDate: startDate,
                        });
                    }
                });
                $('#milestone_end_date').datepicker({
                    minDate: startDate,
                });
            }

        });

        $('#expected_end_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            
        });

        $('#milestone_start_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            change: function (e) {
                datepickerRemover('#milestone_end_date');
                var startDate = new Date(e.target.value);
                $('#milestone_end_date').datepicker({
                    value:'',
                    minDate: startDate,
                });
            }
            
        });

        $('#milestone_end_date').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            

        });




        
        $('#category_ids').change(function() {
            var sub_category_ids = $('#sub_category_ids').val();
            $.ajax({
                type: "GET",
                url: "{{url('/get-sub-categories')}}",
                data: {
                    'category_ids': $(this).val()
                },
                success: function(response) {
                    var new_options = "<option value =''></option>";
                    $.each(response, function(index, value) {
                        if (jQuery.inArray(index, sub_category_ids) !== -1) {
                            new_options += "<option value='" + index + "' selected>" + value + "</option>";
                        } else {
                            new_options += "<option value='" + index + "'>" + value + "</option>";
                        }
                    });
                    $('#sub_category_ids').html(new_options);
                    $('#sub_category_ids').change();
                    $('#sub_category_ids').select2({
                        placeholder: "{{__('Select Sub Categories')}}",
                    });
                }
            });
        });
        $('#sub_category_ids').change(function() {
            var skill_ids = $('#skill_ids').val();
            $.ajax({
                type: "GET",
                url: "{{url('/get-skills')}}",
                data: {
                    'sub_category_ids': $(this).val()
                },
                success: function(response) {
                    var new_options = "<option value =''></option>";
                    $.each(response, function(index, value) {
                        if (jQuery.inArray(index, skill_ids) !== -1) {
                            new_options += "<option value='" + index + "' selected>" + value + "</option>";
                        } else {
                            new_options += "<option value='" + index + "'>" + value + "</option>";
                        }
                    });
                    $('#skill_ids').html(new_options);
                    $('#skill_ids').select2({
                        placeholder: "{{__('Select Sub Categories')}}",
                    });
                }
            });
        });
        $('#sub_category_ids').change(function() {
            var skill_ids = $('#skill_ids').val();
            $.ajax({
                type: "GET",
                url: "{{url('/get-skills')}}",
                data: {
                    'sub_category_ids': $(this).val()
                },
                success: function(response) {
                    var new_options = "<option value =''></option>";
                    $.each(response, function(index, value) {
                        if (jQuery.inArray(index, skill_ids) !== -1) {
                            new_options += "<option value='" + index + "' selected>" + value + "</option>";
                        } else {
                            new_options += "<option value='" + index + "'>" + value + "</option>";
                        }
                    });
                    $('#skill_ids').html(new_options);
                    $('#skill_ids').select2({
                        placeholder: "{{__('Select Sub Categories')}}",
                    });
                }
            });
        });
    });



    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone(".dropzone_docs", {
        url: "{{route('project.fileUpload') }}",
        maxFilesize: 5, // MB
        maxFiles: 5,
        dictDefaultMessage: "File size should be less than 5 MB",
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        success: function(file, response) {
            $('#dropzone-docs-error').remove();
            $('<input>').attr({
                type: 'hidden',
                name: 'project_documents[]',
                value: response.name,
                id: response.original_name,
                class: 'project_documents'
            }).appendTo('.imp_images');
        },
        removedfile: function(file) {
            $('input[name="project_documents[]"]').each(function() {
                if ($(this).attr('id') == file.name) {
                    $(this).remove();
                }
            });
            file.previewElement.remove();
        }
    });




    $(document).on("click", '.add_more', function() {

        var $tr = $(this).closest('tr');

        var $clone = $tr.clone();
        $clone.find('input').val('');
        $clone.find('textarea').val('');
        $clone.find('input.milestone_start_date')
        var start = $('.milestone_start_date').length;
        var end = $('.milestone_end_date').length;


        $clone.find('input.milestone_start_date')
            .attr('id', 'dd_date_start' + start); //newly added line
        $clone.find('input.milestone_end_date')
            .attr('id', 'dd_date_end' + end); //newly added line
///////////new/////////////

$clone.find('input').prop('required',false); 
$clone.find('textarea').prop('required',false); 


// $clone.find('input.milestone_names')
//             .attr('id', 'dd_title' + start).prop('required',false); 
//         $clone.find('input.milestone_amount')
//             .attr('id', 'dd_amount' + start).prop('required',false);
//             $clone.find('textarea.milestone_description')
//             .attr('id', 'dd_description' + start).prop('required',false);

            $clone.find('input.milestone_names')
            .attr('id', 'dd_title' + start).prop('required',true); 
        $clone.find('input.milestone_amount')
            .attr('id', 'dd_amount' + start).prop('required',true);
            $clone.find('textarea.milestone_description')
            .attr('id', 'dd_description' + start).prop('required',true);
            /////////////end////////////

        $clone.find('input.milestone_start_date').
        removeAttr('data-guid').removeAttr('data-datepicker');

        $clone.find('input.milestone_end_date').
        removeAttr('data-guid').removeAttr('data-datepicker');
    
        $tr.after($clone);
        var count = $('.add_more').length;
 
 if(count>0)
 {
  
   $('.remove_row').show();
 }
        
        if(start <= 1){
            var last_val = $('#milestone_end_date').datepicker().value();
        }
        else{
            new_start = start-1;
            var last_val = $('#dd_date_end'+new_start).datepicker().value();
        }
        var startDate = new Date(last_val);
        startDate.setDate(startDate.getDate() + 1);
        $(this).hide();
        $('.milestone-box table tr:last').find('.remove_row').hide();
        $('.milestone-box table tr').not('.milestone-box table tr:last').find('.remove_row').show();
        $('#dd_date_start' + start).datepicker({
            minDate: startDate,
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            change: function (e) {
                datepickerRemover('#dd_date_end' + end);
                var startDate = new Date(e.target.value);
                $('#dd_date_end' + end).datepicker({
                    value:'',
                    minDate: startDate,
                });
            }
        });
        $('#dd_date_end' + end).datepicker({
            minDate: startDate,
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
        });


    });

    $(document).on("click", '.remove_row', function() {
        if($('.remove_row').length==2)
  {
    $('.remove_row').hide();
  }
        if ($('.milestone_names').length == 1) {
            alert('Can not delete this row');
        } else {
            var $tr = $(this).closest('tr').remove();
        }
    });
    $("#create-project-form").submit(function(){
   
        var isFormValid = true;
        $('#create-project-form').find("input,textarea").each(function(){
            if(this.type=="text" || this.type=="number" || this.type=="textarea")
            {
          //  console.log(this.type);
        if(!$(this).val()){
            $(this).addClass("error");
            isFormValid = false;

        } else{
            $(this).removeClass("error");
        }
     }
    });
 //   alert(isFormValid);
    return isFormValid;
    });
    $("#create-project-form").validate({
       
        onkeyup: function(element) {
            $(element).valid();

        },
        onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
    },
        rules: {
            title: {
                required: true,
            },
            location: {
                required: true,
                maxlength: 300,
            },
            description: {
                required: false,
               
            },
            industry_ids: {
                required: false,
                maxlength: 500,
            },
            categories_ids: {
                required: true,
            },
            sub_categories_ids: {
                required: true,

            },
            budget_from: {
                required: true,

            },
            budget_to: {
                required: true,

            },
            expected_start_date: {
                required: true,

            },
            expected_end_date: {
                required: true,

            },
            bid_start_date: {
                required: true,

            },
            bid_end_date: {
                required: true,

            },
            timeline_count: {
                required: true,

            },
            timeline_type: {
                required: true,

            },
            // milestone_names: {
            //     required: true,

            // },
            // milestone_start_dates: {
            //     required: true,

            // },
            // milestone_end_dates: {
            //     required: true,

            // },
            // milestone_amounts: {
            //     required: true,

            // },
            milestone_descriptions: {
                required: true,

            },
            skills: {
                required: true,

            }
        },

        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).html(error);
            } else if (element.attr("id") == "title") {
                $(".title-error").html(error);
            } else if (element.attr("id") == "location") {
                $(".location-error").html(error);
            } else if (element.attr("id") == "description") {
                $(".description-error").html(error);
            } else if (element.attr("id") == "industry_ids") {
                //error.insertAfter(".error-jqu-whats");
                $(".industries-error").html(error);
            } else if (element.attr("id") == "category_ids") {

                $(".categories-error").html(error);
            } else if (element.attr("id") == "sub_category_ids") {
                $(".sub-categories-error").html(error);
            } else if (element.attr("id") == "budget_from") {
                $(".budget-from-error").html(error);
            } else if (element.attr("id") == "budget_to") {
                $(".budget-to-error").html(error);
            } else if (element.attr("id") == "expected_start_date") {
                $(".expected-start-error").html(error);
            } else if (element.attr("id") == "expected_end_date") {
                $(".expected-end-error").html(error);
            } else if (element.attr("id") == "bid_start_date") {
                $(".bid-start-error").html(error);
            } else if (element.attr("id") == "bid_end_date") {
                $(".bid-end-error").html(error);
            } else if (element.attr("id") == "timeline_count") {
                $(".timeline-count-error").html(error);
            } else if (element.attr("id") == "timeline_type") {
                $(".timeline-type-error").html(error);
            } 
            else if (element.attr("id") == "milestone_names") {
                $(".milestone-title-error").html(error);
            } 
            
            else if (element.attr("id") == "milestone_start_date") {
                $(".milestone-start-error").html(error);
            } 
            else if (element.attr("id") == "milestone_end_date") {
                $(".milestone-start-error").html(error);
            } 
            else if (element.attr("id") == "milestone_amount") {
                $(".milestone-start-error").html(error);
            } 
            else if (element.attr("id") == "skill_ids") {
                $(".skills-error").html(error);
            } 
            else if (element.attr("id") == "milestone_amount") {
                $(".milestone-amount-error").html(error);
            } 
            else if (element.attr("id") == "milestone_description") {
                $(".milestone-description-error").html(error);
            } 
         
           
            
            else {
                error.insertAfter(element);
            }
        },

        messages: {
            'title': {
                required: "Title is required.",
            },
            'location': {
                required: "Location is required.",
            },
            'description': {
                required: "Description is required.",
            },
            'budget_from': {
                required: "Budget from amount required.",
            },
            'budget_to': {
                required: "Budget to amount required.",
            },
            'expected_start_date': {
                required: "Expected start date required.",
            },
            'expected_end_date': {
                required: "Expected end date required.",
            },
            'bid_start_date': {
                required: "Proposal start date required.",
            },
            'bid_end_date': {
                required: "Proposal end date required.",
            },
            'timeline_count': {
                required: "Timeline count required.",
            },
            'timeline_type': {
                required: "Timeline type required.",
            },
            'milestone_names': {
                required: "Milestone name is required.",
            },


        }

    });
</script>

@endpush