@extends('user.layouts.user_two')
@section('title', set_page_titile('Milestone Details'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<main class="main-wrapper">
    <div class="container">
        <div class="inner-pg-layout myproposals-view">      
            <div class="details-col mb-3">
                <div class="btns-col text-right mb-3">           
                    <button class="btn white-nm-btn px-4" type="button" onclick="window.location.href='{{ url()->previous() }}'">Back</button>       
                  </div> 
                <div class="row">
                    <div class="col-lg-8 pr-lg-0 mb-3 mb-lg-0">
                        <div class="bg-shadow project-desp-col">
                            <h4 class="mb-2">{{ucwords($proposal->project->title)}} </h4>  
                            <p>{{str_limit(ucwords($proposal->project->description), $limit = 300, $end = '...')}}</p>
                            <div class="row">
                                <div class="col-md-5 mb-3 mb-xl-0">                        
                                    <div class="col-list-item">    
                                        <div class="media  d-block d-md-flex">
                                        
                                     @if(auth()->user()->user_type == 1 )
                                            @if($proposal->project->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$proposal->project->user->profile_image)))
                                                <img class="mr-md-3 thumb mb-3 mb-md-0"  src="{{ asset('storage/user/profile-images/'.$proposal->project->user->profile_image) }}" alt="{{ $proposal->project->user->username }}">
                                            @else 
                                                <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $proposal->project->user->username }}" class="mr-md-3 thumb mb-3 mb-md-0">
                                            @endif
                                            <div class="media-body">   
                                     @else
                                     @if($proposal->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$proposal->user->profile_image)))
                                                <img class="mr-md-3 thumb mb-3 mb-md-0"  src="{{ asset('storage/user/profile-images/'.$proposal->user->profile_image) }}" alt="{{ $proposal->user->username }}">
                                            @else 
                                                <img src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $proposal->user->username }}" class="mr-md-3 thumb mb-3 mb-md-0">
                                            @endif
                                            <div class="media-body"> 

                                     @endif           
                                            
                                            <?php 
                            $status ="null";
                            ?>
                                                @if(auth()->user()->user_type == 1 )
                                                    <a href="{{ route('account.viewUserDetail',['user'=>$proposal->project->user,'project'=>$proposal->project,'status'=>$status]) }}"><h5 class="mt-0 name">{{$proposal->project->user->username}}
                                                        @if($proposal->project->user->organization_type == 0)
                                                        <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                                                        @else
                                                            <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                                                        @endif
                                                    </h5></a>
                                                    <p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>@if(isset($proposal->project->user->userDetail->city) && isset($proposal->project->user->userDetail->state) && $proposal->project->user->userDetail->country->name){{$proposal->project->user->userDetail->city}} , {{$proposal->project->user->userDetail->state}}, {{$proposal->project->user->userDetail->country->name}} @else {{__('NA')}}@endif</p>
                                                    <div class="ratings"><div class="rate-count">@if(!empty($proposal->project->user->userDetail->avg_rating)){{$proposal->project->user->userDetail->avg_rating}} @else 0 @endif</div> <div class="user-rating" data-value="@if(!empty($proposal->project->user->userDetail->avg_rating)){{$proposal->project->user->userDetail->avg_rating}}@else 0 @endif"></div></div>
                                                @else
                                                <a href="{{ route('account.viewUserDetail',['user'=>$proposal->user,'project'=>$proposal->project,'status'=>$status]) }}"><h5 class="mt-0 name">{{$proposal->user->username}}
                                                    @if($proposal->user->prganization_type == 0)
                                                    <i class="fi-flaticon flaticon-group ico-company ml-2"></i>
                                                    @else
                                                        <i class="fi-flaticon flaticon-user ico-individual ml-2"></i>
                                                    @endif
                                                </h5></a>
                                                <p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i></span>@if(isset($proposal->user->userDetail->city) && isset($proposal->user->userDetail->state) && $proposal->user->userDetail->country->name){{$proposal->user->userDetail->city}} , {{$proposal->user->userDetail->state}}, {{$proposal->user->userDetail->country->name}} @else {{__('NA')}}@endif</p>
                                                <div class="ratings"><div class="rate-count">@if(!empty($proposal->user->userDetail->avg_rating)){{$proposal->user->userDetail->avg_rating}} @else 0 @endif</div><div class="user-rating" data-value="@if(!empty($proposal->user->userDetail->avg_rating)){{$proposal->user->userDetail->avg_rating}}@else 0 @endif"></div></div>
                                                @endif
                                            </div>                                            
                                        </div>  
                                    </div>
                                </div>
                                <div class="col-md-7 text-md-right"> 
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="status-col text-md-right mb-3 mb-sm-0">                          
                                            <p class="d-inline-block mb-0 no-border">Status<span class="notify-col">{{getProposalStatus($proposal->status)}}</span></p>                       
                                            </div>
                                        </div>
                                        @if(App\ProjectChat::ChatFeature()==1)      
                                        <div class="col-sm-4">
                                            <div class="btn-cols">       
                                                <button class="btn white-nm-btn btn-small" onclick="location.href='{{route('message.messages',array($proposal->project->uuid,$proposal->project->user->uuid))}}'" type="submit"><i class="fi-flaticon flaticon-comments mr-2"></i>Message</button>             
                                    
                                            </div>    
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>                    
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="bg-shadow project-proposals-col proposal-details-col-right-aside">
                        <h2 class="proposal-tilte">Proposal Details</h2>
                        <ul class="d-block">
                            <li><p>Proposed Date  </p><span>{{hcms_date(strtotime($proposal->created_at), 'date', false)}}</span></li>
                            <li><p>Proposed Amount</p><span>{{formatAmount($proposal->proposal_amount)}}</span></li>                 
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="seperated-cols bg-shadow mb-3">
                <h5 class="sub-title">Description</h5>
                <div class="seperated-cols-content">
                    <p>{{$proposal->proposal_document}}</p>
                </div>
            </div>   
         
            @if(count($proposal->proposalMilestones) > 0)
                <div class="bg-shadow descp-table milestone-table-col">
                    <div class="table-multi-common table-responsive">
                        <table class="table table-striped mb-0">
                        <thead>
                            <tr>
                            <th scope="col" class="sub-title">Milestones</th>
                            <th scope="col"  class="sub-title">Amount</th>        
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($proposal->proposalMilestones as $milestone)
                            <tr>
                                <td>      
                                    <div class="milestones-col">
                                        <div class="head-top mb-3">
                                            <p class="d-inline-block mr-3">{{ ucwords($milestone->milestone_name)}}
                                                @if($milestone->status > 0)
                                                <span class="notify-col">{{getMilestoneStatus($milestone->status)}}</span>
                                                @endif
                                            </p>       
                                                   
                                        </div>
                                        <p>{{$milestone->milestone_description}}.</p>
                                    </div>
                                    <ul class="dates-col my-2 my-md-3">
                                        <li>Start Date {{hcms_date(strtotime($milestone->milestone_start_date), 'date', false)}}</li>
                                        <li>End Date {{hcms_date(strtotime($milestone->milestone_end_date), 'date', false)}}</li>
                                    </ul>
                                </td>
                                <td>{{ formatAmount($milestone->milestone_amount)}}</td>               
                            </tr>
                            @endforeach
                            
                            <tr class="footer-table">
                            <td>Total Amount:</td>
                            <th>{{ formatAmount($total_milestone_amount)}}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <p class="d-sm-none d-block pt-3">Scroll to view amount</p>
                </div>
	        @endif
     
</div>
</div> 
</main>
@endsection
@push('custom-scripts')

@endpush