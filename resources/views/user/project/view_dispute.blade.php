@extends('user.layouts.user_two')
@section('title', set_page_titile(__('View Dispute')))
@push('meta-tags')
@endpush   
<!--start: contact information page -->
@section('content')
<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout project-layouts dispute-view">
         <!-- main page title:start -->
         <div class="row">
          <div class="col-md-8">
             <h3 class="title-main mb-3">Finances</h3>
          </div>
          <div class="col-md-4 text-right">
            <button class="btn cancel-shadow-btn" type="button" onclick="location.href='{{ route('finance.ListDisputes')}}'"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Disputes</button> 
                <button class="btn white-nm-btn" type="button" onclick="location.href='{{ route('message.disputeMessages',array($dispute->id,$dispute->milestone->project->id)) }}'"><i class="fi-flaticon flaticon-comments mr-2"></i>Message</button>
          </div>
       </div>
       <!-- main page title:end -->
    <div class="row mt-3">
    @include('user.layouts.includes.finance_tab',['user'=>$user])
        <div class="col-lg-9 pl-xl-0">
            <div class="right-aside  bg-shadow">  
                  <h4 class="mb-0 sub-headtitle">Dispute for Contract ID <strong> {{getContractId($dispute->project_contract_id)}}</strong></h4>    
                <div class="details-col mb-3 invitaion-list">      
                  <ul class="duration-list">
                    <li>
                    <p class="mb-2 seperator">Amount: <strong>USD{{$dispute->amount}}</strong> </p>
                    </li>                     
                    <li>
                    @if(auth()->user()->user_type == 0)
                      <p class="mb-2 seperator">Freelancer username: <strong>{{$dispute->freelancer->username}}</strong> </p>
                    @else
                      <p class="mb-2 seperator">Client username: <strong>{{$dispute->client->username}}</strong> </p>
                    @endif 
                    </li>
                    <li>
                      <p class="mb-2 seperator">Posted on: <strong>{{date('d M Y',strtotime($dispute->created_at))}}</strong> </p>   
                    </li>
                    <li>
                      <p class="mb-2">Milestone: <strong> {{$dispute->milestone->milestone_name}}</strong> </p>
                  </li>
                  
                  </ul> 
                  <p class="mb-2">Project: <strong> {{$dispute->milestone->project->title}}</strong> </p>  
                  <h6 class="sub-title">Subject</h6>
                  <p class="sub-desp">{{$dispute->subject}}</p>
                  <!-- <h6 class="sub-title">Project</h6>
                  <p class="sub-desp">{{$dispute->milestone->project->title}}</p> -->
                  <h6 class="sub-title">Description</h6>
                  <p class="sub-desp">{{$dispute->description}}</p>
                  <!-- <div class="comments-cols my-3">                               
                    <div class="media">
                      <img class="mr-3 thumb" src="https://randomuser.me/api/portraits/women/63.jpg" alt="Generic placeholder image">
                      <div class="media-body mt-2">                       
                        <h5 class="name">Admin <span class="float-right"> Yesterday, 8:19 PM</span></h5>  
                      </div>                                            
                    </div>  
                    <p class="mt-3">I thought this would be an awesome way to interact with the interface.</p>     
                  </div>    -->
                  <!-- <div class="replied-col">
                    <div class="form-group">
                    <label class="d-block">Reply:</label>
                    <div class="input-group">                      
                      <input type="text" class="form-control" placeholder="Post your reply" aria-label="Post your reply">
                      <div class="input-group-append">
                        <button class="btn reply-btn" type="button"><span class="sprites-col reply-icon"></span></button>
                      </div>
                    </div>
                  </div>
                  </div>               -->
                </div>  
            </div>
        </div>
    </div>
    </div> 
  </div>
</main>
@endsection
<!--end: contact information page -->

@push('custom-scripts')
@endpush