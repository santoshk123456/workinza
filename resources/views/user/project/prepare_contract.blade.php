@extends('user.layouts.user_two')
@section('title', set_page_titile('Contract Details'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container">
        <div class="inner-pg-layout contract-details">
            <!-- main page title:start -->
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="title-main">{{__('Create Contract : ')}} {{getContractId($contract_details->id)}}</h3>
                </div>
                <div class="col-sm-6 text-sm-right mt-sm-0 mt-3">
                    @if($user->user_type == 0)
                        <button class="btn bg-nm-btn" type="submit" onclick="location.href='{{ route('project.listInProgressProjects') }}'"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Project</button>
                    @else
                        <button class="btn bg-nm-btn" type="submit" onclick="location.href='{{ route('project.freelancerlistInProgressProjects') }}'"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Project</button>
                    @endif
                    <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ url()->previous() }}'">Back</button>
                </div>
            </div>
            <!-- main page title:end -->
            <!-- right section:start -->
            <div class="bg-shadow mt-4">
                <div class="block-header">
                    <div class="row">
                        <div class="col-md-3">
                            <p>{{__('Project Name')}}: <span><strong> {{ucwords($contract_details->project->title)}}</strong></span></p>
                        </div>
                        <div class="col-md-3 text-lg-center">
                            <p>{{__('Contract Id')}}: <span><strong>{{getContractId($contract_details->id)}}</strong></span></p>
                        </div>
                        <div class="col-md-3 text-lg-center">
                            <p>{{__('Amount')}}: <span><strong>USD {{$contract_details->amount}}</strong></span></p>
                        </div>
                        <div class="col-md-3">
                            <p>{{__('Created On')}}: <span><strong>{{ hcms_date(strtotime($contract_details->created_at),'date') }}</strong></span></p>
                        </div>
                    </div>
                </div>
                <div class="block-content p-0">
                    <div class="detail-sep">
                        <h4> {{__('Participant Details')}}</h4>
                        <p> {{__('Client Name')}}: <strong>{{ucwords($contract_details->client->username)}}</strong></p>
                        <p>{{__('Freelancer Name')}}: <strong> {{ucwords($contract_details->user->username)}}</strong></p>

                        <h5>{{__('Platform Terms and conditions')}}</h5>
                        <a href="{{route('userPublicProfile','terms-and-conditions')}}" target="_blank" class="terms-link"> Terms&Conditions</a> 
                      
                    </div>
                    <div class="detail-sep">
                        <h4>{{__('Contract Details')}}</h4>
                        <p>{{__('Project Start Date')}}: <strong> {{date('d-m-Y',strtotime($contract_details->project->expected_start_date))}}</strong></p>
                        <p>{{__('Project End Date')}}: <strong>{{date('d-m-Y',strtotime($contract_details->project->expected_end_date))}}</strong></p>
                        @foreach ($proposal_milestones as $proposal)
                        <div class="detail-sep-inner">
                            <h5 class="sub-title"> {{__('Milestone Details')}} </h5>
                            <div class="milestones-col">
                                <div class="head-top">
                                    <h6>{{ucwords($proposal->milestone_name)}}</h6>
                                    <span class="milestone-price"> {{formatAmount($proposal->milestone_amount)}}</span>
                                </div>
                                <p>{{$proposal->milestone_description}}</p>
                                <ul class="dates-col">
                                    <li>{{__('Start Date')}} {{date('d-m-Y',strtotime($proposal->milestone_start_date))}}</li>
                                    <li>{{__('End Date')}} {{date('d-m-Y',strtotime($proposal->milestone_end_date))}}</li>
                                </ul>
                            </div>
                            <!-- <p class="mt-3">{{__('Deliverables')}}: <strong>Autofetched Delivery Details</strong></p> -->
                        </div>
                        @endforeach
                        <div class="terms-col">
                            <div class="row justify-content-between">
                                <div class="col-sm-6 col-lg-3 mb-3 mb-sm-0">
                                   
                                        <h4>{{__('Client Agreement Contract')}}</h4>
                                      
                                   
                                    <div class="check-cols">
                                        <div class="custom-control custom-checkbox pl-0">
                                            
                                                @if($user->user_type == 0 && $contract_details->client_acceptance == 0)
                                                    <form method="POST" action="{{route('project.acceptProposal')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form">
                                                        @csrf
                                                    
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="client-agree" name="client-agree" value="" required>
                                                            <label class="custom-control-label" for="client-agree">{{__('I agree to the terms and conditions')}}</label>
                                                        </div>
                                                        <input type="hidden" class="" id="contract_id" name="contract_id" value="{{$contract_details->id}}">
                                                        <input type="hidden" class="" id="proposal_id" name="proposal_id" value="{{$contract_details->project_proposal_id}}">
                                                        <div class="btn-cols mt-3">
                                                            <button class="btn log-reg-btn" type="submit">{{__('Accept')}}</button>
                                                        </div>
                                                    </form>
                                                @elseif($user->user_type == 1 && $contract_details->client_acceptance == 1)
                                                    <label class="" for="client-agree">{{__('Client agreed the contract on ')}}{{hcms_date(strtotime($contract_details->updated_at), 'date', false)}}</label>
                                                @elseif($user->user_type == 0 && $contract_details->client_acceptance == 1)
                                                    <label class="" for="client-agree">{{__('You agreed the contract on ')}}{{hcms_date(strtotime($contract_details->updated_at), 'date', false)}}</label>
                                                @elseif($user->user_type == 1 && $contract_details->client_acceptance == 0)
                                                    <label class="" for="client-agree">{{__('Client need to agree the contract')}}</label>
                                                @elseif($user->user_type == 0 && $contract_details->client_acceptance == 0)
                                                    <label class="" for="client-agree">{{__('You need to agree the contract')}}</label>
                                                @endif
                                            
                                        </div>
                                    </div>
                                </div>
                                <!--freelancer agrreement-->
                                <div class="col-sm-6 col-lg-3">
                                    <h4>{{__('Freelancer Agreement Contract')}}</h4>
                                    <div class="check-cols">
                                        <div class="custom-control custom-checkbox pl-0">
                                            @if($user->user_type == 1 && $contract_details->freelancer_acceptance == 0)
                                                <form method="POST" action="{{route('project.acceptProposal')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form">
                                                    @csrf
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="client-agree" name="client-agree" value="" required>
                                                        <label class="custom-control-label" for="client-agree">{{__('I agree to the terms and conditions')}}</label>
                                                    </div>
                                                    <input type="hidden" class="" id="contract_id" name="contract_id" value="{{$contract_details->id}}">
                                                    <input type="hidden" class="" id="proposal_id" name="proposal_id" value="{{$contract_details->project_proposal_id}}">
                                                    <div class="btn-cols mt-3">
                                                        <button class="btn log-reg-btn" type="submit">{{__('Accept')}}</button>
                                                    </div>
                                                </form>
                                            @elseif($user->user_type == 0 && $contract_details->freelancer_acceptance == 1)
                                                <label class="" for="client-agree">{{__('Freelancer agreed the contract on ')}}{{hcms_date(strtotime($contract_details->updated_at), 'date', false)}}</label>
                                            @elseif($user->user_type == 1 && $contract_details->freelancer_acceptance == 1)
                                                <label class="" for="client-agree">{{__('You agreed the contract on ')}}{{hcms_date(strtotime($contract_details->updated_at), 'date', false)}}</label>
                                            @elseif($user->user_type == 0 && $contract_details->freelancer_acceptance == 0)
                                                <label class="" for="client-agree">{{__('Freelancer need to agree the contract')}}</label>
                                            @elseif($user->user_type == 1 && $contract_details->freelancer_acceptance == 0)
                                                <label class="" for="client-agree">{{__('You need to agree the contract')}}</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="signature-col">
                        <div class="row justify-content-between">
                            <div class="col-sm-3 mb-3 mb-sm-0">
                                <h4>{{__('Client Signature')}}</h4>
                                <div class="media">
                                    @if($contract_details->client->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$contract_details->client->profile_image)))
                                    <img src="{{ asset('storage/user/profile-images/'.$contract_details->client->profile_image) }}" alt="{{ $contract_details->client->full_name }}" class="mr-3 thumb">
                                    @else
                                    <img src="{{ asset('images/admin/starter/default.png') }}"  class="mr-3 thumb" alt="{{ $contract_details->client->full_name }}">
                                    @endif
                                    <div class="media-body">
                                        <h5>{{ucwords($contract_details->client->username)}}</h5>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h4>{{__('Freelancer Signature')}}</h4>
                                <div class="media">
                                @if($contract_details->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$contract_details->user->profile_image)))
                                    <img src="{{ asset('storage/user/profile-images/'.$contract_details->user->profile_image) }}" alt="{{ $contract_details->user->full_name }}" class="mr-3 thumb">
                                    @else
                                    <img src="{{ asset('images/admin/starter/default.png') }}"  class="mr-3 thumb" alt="{{ $contract_details->client->full_name }}">
                                    
                                    @endif
                                    <div class="media-body">
                                        <h5>{{ucwords($contract_details->user->username)}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- right section:start -->
        </div>
    </div>
</main>

@endsection
