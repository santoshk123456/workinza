@extends('user.layouts.user_two')
@section('title', set_page_titile('View Invoice'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container">
      <div class="inner-pg-layout project-layouts">
           <!-- main page title:start -->
           <div class="row">
            <div class="col-md-8">
               <h3 class="title-main">Invoices</h3>
            </div>
            <div class="col-md-4 text-right">
              <button class="btn cancel-shadow-btn" onclick="window.location.href='{{route('finance.listInvoices')}}'" type="button"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all invoices</button> 
            </div>
         </div>
         <!-- main page title:end -->
      <div class="row mt-3">
          
            @include('user.layouts.includes.finance_tab')
          <div class="col-lg-9 pl-xl-0">
              <div class="right-aside">                   
                <div class="card-top mb-4">
                  <h4>Invoice - <span> {{getInvoice($invoice->id)}}</span></h4>                
                </div>   
                <div class="details-col mb-3">
                  <div class="row">
                    <div class="col-lg-8 pr-lg-0 mb-3 mb-lg-0">
                      <div class="bg-shadow invoice-address-col">  
                            <div class="col-list-item mb-3">   
                              <p>From</p> 
                              <div class="media">
                                        @if($invoice->type == 6 || $invoice->type == 5)
                                          	<img class="mr-3 thumb"  src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $invoice->user->username }}">
                                          	<div class="media-body">                       
                                        		<h5 class="mt-0 name">{{ucwords($admin_name)}} </h5>
											  </div>
										@elseif($invoice->type == 1)
											@if($invoice->contracts->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$invoice->contracts->user->profile_image)))
												<img class="mr-3 thumb"  src="{{ asset('storage/user/profile-images/'.$invoice->contracts->user->profile_image) }}" alt="{{ $invoice->contracts->user->username }}">
											@else 
												<img class="mr-3 thumb"  src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $invoice->contracts->user->username }}">
											@endif
											<div class="media-body">     
												<h5 class="mt-0 name">{{$invoice->contracts->user->username}} </h5>
                        <p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i>
                          @if(!empty($invoice->contracts->user->userDetail->city)){{$invoice->contracts->user->userDetail->city}},@endif
                          @if(!empty($invoice->contracts->user->userDetail->state)){{$invoice->contracts->user->userDetail->state}},@endif
													@if(!empty($invoice->contracts->user->userDetail->country->name)){{$invoice->contracts->user->userDetail->country->name}}@endif
													</span></p>
											</div>
                                    	@else
                                        	@if($invoice->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$invoice->user->profile_image)))
                                        		<img class="mr-3 thumb"  src="{{ asset('storage/user/profile-images/'.$invoice->user->profile_image) }}" alt="{{ $invoice->user->username }}">
                                        	@else 
                                            	<img class="mr-3 thumb"  src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $invoice->user->username }}">
                                        	@endif
                                        	<div class="media-body">     
                                        		<h5 class="mt-0 name">{{$invoice->user->username}} </h5>
                                        		<p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i>@if(!empty($invoice->user->userDetail->city)){{$invoice->user->userDetail->city}}@endif
                                            		@if(!empty($invoice->user->userDetail->state)), {{$invoice->user->userDetail->state}}@endif
                                            		@if(!empty($invoice->user->userDetail->country->name)), {{$invoice->user->userDetail->country->name}}@endif</span></p>
                                        	</div>
                                    	@endif
                                                                            
                              </div>  
                            </div>
                            <div class="col-list-item">   
                              <p>To</p> 
                              <div class="media">
                              
                                @if($invoice->user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$invoice->user->profile_image)))
                                <img class="mr-3 thumb"  src="{{ asset('storage/user/profile-images/'.$invoice->user->profile_image) }}" alt="{{ $invoice->user->username }}">
                                @else 
                                    <img class="mr-3 thumb"  src="{{ asset('images/admin/starter/default.png') }}"  alt="{{ $invoice->user->username }}">
                                @endif
                                <div class="media-body">     
                                <h5 class="mt-0 name">{{$invoice->user->username}} </h5>
                                <p class="mb-2"><span class="loc-ico"><i class="fi-flaticon flaticon-location-pin mr-2"></i>@if(!empty($invoice->user->userDetail->city)){{$invoice->user->userDetail->city}},@endif
                                    @if(!empty($invoice->user->userDetail->state)){{$invoice->user->userDetail->state}},@endif
                                    @if(!empty($invoice->user->userDetail->country->name)){{$invoice->user->userDetail->country->name}}@endif</span></p>
                                </div> 
                                                                      
                              </div>  
                            </div>
                          </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="bg-shadow project-proposals-col">
                        <ul class="d-block">
                          <li><p>Contact ID</p><span>@if(isset($invoice->project_contract_id) && $invoice->project_contract_id !="")<a href="{{ route('project.prepareContract', $invoice->contracts) }}">{{getContractId($invoice->project_contract_id)}}</a>@else{{__('NA')}}@endif </span></li>
                          <li><p>Project Name</p><span>@if(isset($invoice->milestones->project) && $invoice->milestones->project !=""){{ucwords($invoice->milestones->project->title)}}@else{{__('NA')}}@endif </span></li>
                          <li><p>Invoiced On</p><span>{{hcms_date(strtotime($invoice->created_at), 'date', false)}}</span></li> 
                          <li><p>Status</p><span>@if($invoice->status == 0)
                            <span class="badge badge-multi-common badge-warning">Pending</span>
                        @elseif($invoice->status == 1)
                            <span class="badge badge-multi-common badge-success">Paid</span>
                        @elseif($invoice->status == 2)
                            <span class="badge badge-multi-common badge-danger">Failed</span>
                        @endif</span></li>   
                                        
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="bg-shadow descp-table">
                  <div class="table-multi-common table-responsive">
                    <table class="table table-striped mb-0">
                      <thead>
                        <tr>
                          <th scope="col">Description  </th>
                          <th scope="col">Amount</th>        
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{$invoice->notes}}</td>
                          <td>{{formatAmount($invoice->amount)}}</td>                        
                        </tr>
                       
                        <tr class="footer-table">
                          <td>Total Amount:</td>
                          <th>{{formatAmount($invoice->amount)}}</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              
                </div>
          </div>
      </div>
      </div> 
    </div>
  </main>
  
  <!--end: contact information page -->

@endsection
@push('custom-scripts')
@endpush