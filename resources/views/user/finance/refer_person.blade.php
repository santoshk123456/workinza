@extends('user.layouts.user_two')
@section('title', set_page_titile('Refer A Person'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts pt-4">
           <!-- main page title:start -->
           <div class="row mx-0">
            <div class="col-md-6">
               <h3 class="title-main mb-3">Incentives / Bonus</h3>
            </div>
            <div class="col-md-6 text-right">
              <button class="btn cancel-shadow-btn" onclick="window.location.href='{{route('finance.listReferrals')}}'" type="submit"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Referrals</button> 
            </div>
         </div>
         <!-- main page title:end -->
      <div class="row mx-0 mt-2">
            @include('user.layouts.includes.finance_tab',['user'=>$user])
          <div class="col-lg-9 pl-xl-0">
            <div class="right-aside">         
                    <!-- freelancer-list:start -->
              <div class="invoices-col bg-shadow">
                <div class="row">
                  <div class="col-12">
                    <p class="table-text pl-2">Refer a Person</p>
                  </div>
                </div>
                <form method="POST" id="contact_details_form" enctype="multipart/form-data" action="{{ route('finance.sendReferralInvitation')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif intl-form" novalidate>
                  @csrf
                <div class="refer-form mt-4">
                  <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="user-name">Client Username</label>
                      <input type="text" name="username" class="form-control" id="user-name" placeholder="Enter username">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="user-type">Client User Type</label>
                      <select name="user_type" class="form-control" id="user-type" required="">
                        <option disabled="" selected="" value="">Select usertype</option>
                        <option value="0"> Company</option>
                        <option value="1"> Individual</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" id="register-email"  name="email" id="email" maxlength="50"  class="form-control email"  placeholder="Email"  aria-label="Email" aria-describedby="basic-addon3">
                            <span class="invalid-feedback error-txt email-error">
                              @if ($errors->has('email'))
                              {{ $errors->first('email') }}
                              @endif
                            </span>   
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="phone">Phone</label>
                      <input type="hidden" name="country_code" id="country_code" value="{{old('country_code',1)}}">
                      <input name="mobile_number"  minlength="8" maxlength="15" type="tel" id="phone" class="form-control numericOnly intl-input" placeholder="Mobile number">  
                      <span class="error-txt phone-error" id="register-mobile-error"></span>
                    </div>
                  </div>
                </div>
                <div class="row mt-4">
                  <div class="col-12 mt-4 text-right">
                    <button class="btn cancel-btn mr-2" type="button">Cancel</button>
                    <button class="btn submit-btn" type="submit">Refer</button>
                  </div>
                </div>
                </div>
                </form>
              </div>
               <!-- freelancer-list:end -->    
              </div>
        </div>
      </div>
    </div> 
  </main>
@endsection
@push('custom-scripts')
<script>
   window.i = 0;
   $(document).ready(function(){
   var phone = document.querySelector("#phone");
    var phone_iti = window.intlTelInput(phone,({

        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = "in";//(resp && resp.country) ? resp.country : "in";
                callback(countryCode);
            });
        },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.1.0/js/utils.js",
        separateDialCode: true,
        hiddenInput: "full_number",
        autoPlaceholder: "polite",
        initialCountry: "us",
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            return selectedCountryPlaceholder.replace(/[0-9]/g,"x");
        }, 
    }));
    phone.addEventListener("countrychange", function() {
    
        $('#country_code').val(phone_iti.getSelectedCountryData().dialCode);
        if(window.i == 0){
          $('.flag-container').first().remove();
        }
        
       window.i = window.i + 1;
    });
   });
</script>
<script>
  $("#contact_details_form").validate({
   onfocusout: function(element){
          $(element).valid();
      
     },
     onkeyup: function(element){
         $(element).next().text('');
         $(element).removeClass('error');
      
         },
         // onfocusin: function(element){
         // $(element).removeClass('error');
         // $(element).next().text('');
         // },
     rules: {
        email: {
            required: true,
            minlength: 3,
            maxlength: 50,
            email: true,
          
        },
        phone: {
          
            maxlength : 10,
            minlength : 8,
        }, 
        
       
        
         
     },
 
     errorPlacement: function(error, element) {
         var placement = $(element).data('error');
         if (placement) {
             $(placement).html(error);
         }    else if (element.attr("name") == "email" ) {
            $(".email-error").html(error); 
        } else if (element.attr("id") == "phone" ) {
          $(".phone-error").html(error); 
        }
        
         else {
            error.insertAfter(element);
         }
    },
 
    messages: {
            'email': {
                 required: "Email is required.",
             },
             'Phone': {
                required: "Phone is required.",
            },
             
         }
 
 });
 </script>
@endpush