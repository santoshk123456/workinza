@extends('user.layouts.user_two')
@section('title', set_page_titile('Transactions'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout project-layouts">
         <!-- main page title:start -->  
        <div class="row">
          <div class="col-md-6">
             <h3 class="title-main mb-3">Transaction - {{generateTransactionId($transaction->id)}}</h3>
          </div>
          <div class="col-md-6 text-right">
            <button class="btn bg-nm-btn" onclick="window.location.href='{{route('finance.listTransactions')}}'" type="submit"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Transactions</button> 
            <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ url()->previous() }}'">Back</button>
          </div>
       </div>
       <!-- main page title:end -->
    <div class="row mt-3">
        @include('user.layouts.includes.finance_tab',['user'=>$user])
        <div class="col-lg-9 pl-xl-0">
            <div class="right-aside">         
                    <!-- freelancer-list:start -->
              <div class="invoices-col bg-shadow">
                <div class="row">
                  <div class="col-12">
                    <p class="table-text pl-0">Transaction - {{generateTransactionId($transaction->id)}}</p>
                  </div>
                </div>
                
                <div class="transaction-details">
                  @if(isset($transaction->milestones->project) && $transaction->milestones->project !="")
                  <div class="row mb-md-3">
                    <div class="col-md-12">
                      <p class="label">Project: <span class="data">{{ucwords($transaction->milestones->project->title)}}</span></p>
                    </div>
                  </div>
                  @endif
                  <div class="row mb-md-3">
                    <div class="col-md-4">
                      <p class="label">Transaction Type: <span class="data">{{generateTransactionType($transaction->type)}}</span></p>
                    </div>
                    <div class="col-md-3">
                      <p class="label">Tranferred on: <span class="data">{{hcms_date(strtotime($transaction->created_at), 'date', false)}}</span></p>
                    </div>
                  </div>
                  @if(isset($transaction->milestones->project) && $transaction->milestones->project !="")
                  <div class="row mb-md-3">
                    <div class="col-md-3">
                      <p class="label">Contract Id: <span class="data"><a href="{{ route('project.prepareContract', $transaction->contracts) }}">@if($transaction->project_contract_id !=""){{getContractId($transaction->project_contract_id)}}@else{{'NA'}}@endif</a></span></p>
                    </div>
                    <div class="col-md-3">
                      <p class="label">Milestone: <span class="data">@if($transaction->milestones !=""){{$transaction->milestones->milestone_name}}@else{{'NA'}}@endif</span></p>
                    </div>
                  </div>
                  @endif
                  <div class="row mb-md-3">
                    @if($transaction->transaction_type == 0)
                        <div class="col-md-3">
                            <p class="label">Credit: <span class="data">{{formatAmount($transaction->amount)}}</span></p>
                        </div>
                    @else
                        <div class="col-md-3">
                            <p class="label">Debit: <span class="data">{{formatAmount($transaction->amount)}}</span></p>
                        </div>
                    @endif
                    <div class="col-md-3">
                        <p class="label">Transaction Fee: <span class="data">{{formatAmount($transaction->service_charge)}}</p>
                    </div>
                    <div class="col-md-3">
                        <p class="label">Total Amount: <span class="data">{{formatAmount($transaction->total_amount)}}</p>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-12">
                      <p class="label w-100 mb-2">Description</p>
                      <div class="desc-box pt-3">
                        <p>{{$transaction->notes}}</p>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
               <!-- freelancer-list:end -->    
              </div>
        </div>
    </div>
    </div> 
  </div>
</main>

  
@endsection
@push('custom-scripts')
@endpush