@extends('user.layouts.user_two')
@section('title', set_page_titile('Transactions'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container">
        <div class="inner-pg-layout project-layouts pt-4">
           <!-- main page title:start -->  
            <div class="row">
                <div class="col-md-6">
                <h3 class="title-main mb-3">Transactions</h3>
                </div>
                <div class="col-md-6 text-right">
                    <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ url()->previous() }}'">Back</button>
                {{-- <span class="wallet-balance mr-4"><i class="fi-flaticon flaticon-wallet mr-2"></i>Wallet Balance: <span class="amount-count"> {{formatAmount($user->wallet_balance)}}</span></span>
                <button class="btn bg-nm-btn" onclick="window.location.href='{{route('finance.addMoney')}}'" ><i class="fi-flaticon flaticon-plus mr-2"></i>Add Wallet Balance</button>   --}}
                </div>
            </div>
            <!-- main page title:end -->
            <div class="row mt-2">
                @include('user.layouts.includes.finance_tab',['user'=>$user])
                <div class="col-lg-9 pl-xl-0">
                    <div class="right-aside">         
                            <!-- freelancer-list:start -->
                        <div class="invoices-col bg-shadow">
                        <div class="row">
                            <div class="col-md-6">
                               
                                    <p class="table-text pl-2">Transactions</p>
                                
                            </div>
                            @php
								$title = '';
								$url = $_SERVER['REQUEST_URI'];
								$parts = parse_url($url);
								if(isset($parts['query']))
								{
									parse_str($parts['query'], $query);
									if(isset($query['contract_id'])){
										$title = $query['contract_id'];
									}
								}	
								
							@endphp
                                <div class="col-md-6">
                                    <div class="search-bar p-0">
                                        <form method="GET" action="{{route('finance.listTransactions')}}">
                                            <div class="input-group project-search">
                                            
                                                    <input type="text"  value="{{$title}}" name="contract_id" class="form-control border-right-0" placeholder="Search By Transaction / Contract ID" aria-label="Search By Transaction/Contract ID">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                                            
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                           
                        </div>
                      
                            <div class="table-multi-common table-responsive mt-3">
                                <table class="table table-striped">
                                <thead>
                                    @if($transactions->isNotEmpty())
                                    <tr>
                                    <th scope="col">Transaction ID</th>
                                    <th scope="col">Transaction Type</th>
                                    <th scope="col">Contract ID </th>
                                    <th scope="col">Milestone</th>
                                    <th scope="col">Date Paid</th>
                                    <th scope="col">Amount </th>  
                                    <th scope="col">Transaction Fee</th>
                                    <th scope="col">Total amount </th>  
                                    <th scope="col">Transaction Status </th>                                                         
                                    <th scope="col">Action</th>
                                    </tr>
                                    @else
                                    <tr>
                                        <td>{{__('No transaction found')}}</td>
                                    </tr>
                                  @endif
                                </thead>
                                <tbody>
                                    @foreach($transactions as $transaction)
                                        <tr>
                                            <td><a href="{{route('finance.viewTransaction',$transaction)}}">{{generateTransactionId($transaction->id)}}</a></td>
                                            <td>{{generateTransactionType($transaction->type)}}</td>
                                            <td>@if($transaction->project_contract_id !="")
                                                <a href="{{ route('project.prepareContract', $transaction->contracts) }}">{{getContractId($transaction->project_contract_id)}}</a>
                                                @else{{'NA'}}@endif
                                            </td>
                                        
                                            <td>@if($transaction->milestones !="")
                                                {{$transaction->milestones->milestone_name}}
                                                @else
                                                    {{'NA'}}
                                                @endif
                                            </td>
                                            <td>{{hcms_date(strtotime($transaction->created_at), 'date', false)}}</td>     
                                            <td>{{formatAmount($transaction->amount)}}</td>         
                                            <td>{{formatAmount($transaction->service_charge)}}</td>                                                     
                                            <td>{{formatAmount($transaction->total_amount)}}</td>  
                                            <td>
                                                @if($transaction->payment_status == 0)
                                                    <span class="badge badge-multi-common badge-warning">Pending</span>
                                                @elseif($transaction->payment_status == 1)
                                                    <span class="badge badge-multi-common badge-success">Success</span>
                                                @elseif($transaction->payment_status == 2)
                                                    <span class="badge badge-multi-common badge-danger">Failed</span>
                                                @endif
                                            </td>                                                     
                                            <td><button class="btn bg-nm-btn mr-2" onclick="location.href='{{route('finance.viewTransaction',$transaction)}}'"  type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></td>
                                        </tr>
                                    @endforeach 
                                    
                        
                                                
                                </tbody>
                                </table>
                            </div>
                       
                        @if(!$transactions->isEmpty())
                        <div class="pagination-col mt-4 mb-3">
                          <nav aria-label="page navigation">
                            <ul class="pagination justify-content-end pagination-sm flex-wrap">
            
                              {{ $transactions->links('vendor.pagination.customnew') }}
            
                            </ul>
                          </nav>
                        </div>
            
                       @endif
                        </div>
                        <!-- freelancer-list:end -->    
                        </div>
                    </div>
                </div>
        </div> 
    </div>
</main>
  
@endsection
@push('custom-scripts')
@endpush