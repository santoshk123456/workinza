@extends('user.layouts.user_two')
@section('title', set_page_titile('Transactions'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<main class="main-wrapper">
	<div class="container">
		<div class="inner-pg-layout in-progress-project-details-layouts">
			<h3 class="title">{{ucwords($project->title)}}</h3>
			<div class="row">
				<div class="col-lg-3 left-aside left-projects-list">
					@if($user->user_type == 0)
						<ul class="d-block">
							<li><a href="{{route('project.listOpenProjects')}}">Open Projects</a></li>
							<li class="active"><a href="{{route('project.listInProgressProjects')}}">In Progress Projects</a></li>
							<li><a href="{{route('project.listInProgressProjects')}}">Completed Projects</a></li>
						</ul>
					@else
						<ul class="d-block">
							<li ><a href="{{route('project.listProposals')}}">My Proposals</a></li>
							<li class="active"><a href="{{route('project.freelancerlistInProgressProjects')}}">Assigned / In Progress Projects</a></li>
							<li><a href="#">Completed Projects</a></li>
							<li ><a href="{{route('project.listInvitations')}}">Invitations</a></li>
						</ul>
					@endif
				</div>
				<div class="col-lg-9">
					<div class="right-aside">
                    
						<div class="bg-shadow mb-3 projects-links-menu">
							<ul>
								<li><a href="{{ route('project.inProgressProjectDetails',$project) }}" ><i class="fi-flaticon flaticon-blank-page mr-2"></i>Project Details </a></li>
								<li><a class="active" href="{{ route('finance.projectTransactions',$project) }}"><i class="fi-flaticon flaticon-dollar-sign-with-rotating-arrows mr-2"></i>Transactions</a></li>
								@if(App\ProjectChat::ChatFeature()==1)      
								<li><a href="{{ route('message.messages',$project) }}"><i class="fi-flaticon flaticon-comments mr-2"></i>Messages</a></li>
							    @endif
							</ul>
                        </div>
                        <div class="invoices-col bg-shadow">
                        <div class="table-multi-common table-responsive mt-3">
                            <table class="table table-striped">
                            <thead>
                                @if($transactions->isNotEmpty())
                                <tr>
                                <th scope="col">Transaction ID</th>
                                <th scope="col">Transaction Type</th>
                                <th scope="col">Contract ID </th>
                                <th scope="col">Milestone</th>
                                <th scope="col">Date Paid</th>
                                <th scope="col">Amount </th>  
                                <th scope="col">Transaction Fee</th>
                                <th scope="col">Total amount </th>  
                                <th scope="col">Transaction Status </th>                                                         
                                <th scope="col">Action</th>
                                </tr>
                                @else
                                    <tr>
                                        <td>{{__('No transaction found')}}</td>
                                    </tr>
                                  @endif
                             
                            </thead>
                            <tbody>
                                @foreach($transactions as $transaction)
                                    <tr>
                                        <td>{{generateTransactionId($transaction->id)}}</td>
                                        <td>{{generateTransactionType($transaction->type)}}</td>
                                        <td>@if($transaction->project_contract_id !=""){{getContractId($transaction->project_contract_id)}}@else{{'NA'}}@endif</td>
                                    
                                        <td>@if($transaction->milestones !=""){{$transaction->milestones->milestone_name}}@else{{'NA'}}@endif</td>
                                        <td>{{hcms_date(strtotime($transaction->created_at), 'date', false)}}</td>     
                                        <td>{{formatAmount($transaction->amount)}}</td>         
                                        <td>{{formatAmount($transaction->service_charge)}}</td>                                                     
                                        <td>{{formatAmount($transaction->total_amount)}}</td>  
                                        <td>
                                            @if($transaction->payment_status == 0)
                                                <span class="badge badge-multi-common badge-warning">Pending</span>
                                            @elseif($transaction->payment_status == 1)
                                                <span class="badge badge-multi-common badge-success">Success</span>
                                            @elseif($transaction->payment_status == 2)
                                                <span class="badge badge-multi-common badge-danger">Failed</span>
                                            @endif
                                        </td>                                                     
                                        <td><button class="btn bg-nm-btn mr-2" onclick="location.href='{{route('finance.viewTransaction',$transaction)}}'"  type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></td>
                                    </tr>
                                @endforeach 
                                
                    
                                            
                            </tbody>
                            </table>
                        </div>
                        </div>
              </div>
          </div>
        </div>
      </div> 
    </div>
  </main>
@endsection
@push('custom-scripts')
@endpush