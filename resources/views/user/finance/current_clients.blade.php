@extends('user.layouts.user_two')
@section('title', set_page_titile('Current Clients'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">
           <!-- main page title:start -->
           <div class="row m-0">
            <div class="col-lg-12">
               <h3 class="title-main">Clients</h3>
            </div>
         </div>
         <!-- main page title:end -->
      <div class="row mx-0 mt-3">
            @include('user.layouts.includes.finance_tab',['user'=>$user])
          <div class="col-lg-9">
              <div class="right-aside">         
                <div class="bg-shadow mb-3 projects-links-menu">
                  <ul>
                    <li><a href="{{route('finance.invitedClients')}}">Invited Clients</a></li>
                    <li><a href="" class="active">Current Clients</a></li>          
                  </ul>                
                </div> 
                
                <!-- freelancer-list:start -->
                <div class="freelancer-finance-holder project-search-listing bg-shadow">
                  @if(isset($resultArr) && count($resultArr) > 0)
                  @foreach($resultArr as $key=>$contract)
					
					<div class="table-multi-common table-group table-responsive mb-3">
						<table class="table table-striped">
						<thead>
							<tr class="table-group-header">
							<th class="title" colspan="7"><a href="{{ route('account.viewUserDetail',['user'=>$contract[0]->project->user,'project'=>$contract[0]->project,'status'=>"null"]) }}">{{$key}}</a></th>
							</tr>
							<tr>
							<th scope="col">Contact ID</th>
							<th scope="col">Start Date</th>
							<th scope="col">End Date</th>
							<th scope="col">Contract Amount</th>
							<th scope="col">Service Fee</th>
							<th scope="col">Paid Amount </th>
							<th scope="col">Status</th>
							</tr>
						</thead>
						<tbody>
              @php
                $total_contract_amount = 0;
                $total_service_charge = 0;
                $total_paid_amount = 0;
              @endphp
							@foreach($contract as $key => $value)
								<tr>
									<td> <a href="{{ route('project.prepareContract', $value) }}">{{getContractId($value->id)}}</a></td>
									<td>{{hcms_date(strtotime($value->project->expected_start_date),'date', false)}}</td>
									<td>{{hcms_date(strtotime($value->project->expected_end_date), 'date', false)}}</td>
									<td>{{formatAmount($value->amount)}}</td>
									<td>
                      @php
                        $service_charge = 0;
                        $paid_amount  = 0;
                        if(isset($value->freelancer_service_charge) && $value->freelancer_service_charge){
                            $service_charge = ($value->amount * $value->freelancer_service_charge)/100;
                            $paid_amount    = $value->amount - $service_charge;
                        }
                      @endphp
                      {{formatAmount($service_charge)}}
                    </td>
									<td>  {{formatAmount($paid_amount)}}</td>
									<td><span class="badge badge-multi-common badge-primary">Signed</span></td>
                </tr>
                @php
                  $total_contract_amount = $total_contract_amount + $value->amount;
                  $total_service_charge = $total_service_charge + $service_charge;
                  $total_paid_amount = $total_paid_amount + $paid_amount;
                @endphp
							@endforeach
							<tr class="total">
                <td></td>
                <td></td>      
                        
                <td>Total</td>
                <td class="amount">{{formatAmount($total_contract_amount)}}</td>
                <td class="amount">{{formatAmount($total_service_charge)}}</td>
                <td class="amount">{{formatAmount($total_paid_amount)}}</td>
                <td></td>
              </tr>
							
						</tbody>
						</table>
					</div>
                @endforeach
                @else
                {{__('No data found')}}
               @endif
  
                <!--pagination:start -->
                {{-- @if(!$contracts->isEmpty())
                  <div class="pagination-col mt-4 mb-3">
                    <nav aria-label="page navigation">
                      <ul class="pagination justify-content-end pagination-sm flex-wrap">
      
                        {{ $contracts->links('vendor.pagination.customnew') }}
      
                      </ul>
                    </nav>
                  </div>
      
                @endif --}}
                     <!--pagination:end -->
                </div>
                 <!-- freelancer-list:end -->  
              
              
                </div>
          </div>
      </div>
    </div> 
  </main>
@endsection
@push('custom-scripts')
@endpush