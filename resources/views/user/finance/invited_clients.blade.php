@extends('user.layouts.user_two')
@section('title', set_page_titile('Current Clients'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts pt-4">
           <!-- main page title:start -->
           <div class="row m-0">
            <div class="col-lg-7">
               <h3 class="title-main">Clients</h3>
            </div>
            <div class="col-5 text-right">
              <button class="btn bg-nm-btn" onclick="window.location.href='{{route('finance.referPerson')}}'" type="submit">Refer a Person</button>
             </div>
         </div>
         <!-- main page title:end -->
      <div class="row mx-0 mt-2">
        @include('user.layouts.includes.finance_tab',['user'=>$user])
          <div class="col-lg-9">
              <div class="right-aside">         
                <div class="bg-shadow mb-3 projects-links-menu">
                  <ul>
                    <li><a href="" class="active">Invited Clients</a></li>
                  <li><a href="{{route('finance.currentClients')}}">Current Clients</a></li>          
                  </ul>                
                </div> 
                
                <!-- freelancer-list:start -->
                <div class="freelancer-finance-holder bg-shadow">
                    @if($invited_clients->isNotEmpty())
                  <div class="table-multi-common table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col">Client Name</th>
                          <th scope="col">Client Email</th>
                          <th scope="col">Invited On</th>
                          <th scope="col">Join On</th>
                          <th scope="col">Referral Bonus Availed</th>
                          <th scope="col">Status</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($invited_clients as $client)
                        <tr>
                          <td>{{ucwords($client->user_name)}}</td>
                          <td>{{$client->email}}</td>
                          <td>{{hcms_date(strtotime($client->created_at), 'date', false)}}</td>
                        <td>@if($client->joined_date != ""){{hcms_date(strtotime($client->joined_date), 'date', false)}}@else {{__('NA')}}@endif</td>
                        <td>
                          @if($client->is_availed == 0)
                            <span class="badge badge-multi-common badge-secondary">Registration not completed</span>
                          @elseif($client->is_availed == 1)
                            <span class="badge badge-multi-common badge-warning">Pending</span>
                          @elseif($client->is_availed == 2)
                            <span class="badge badge-multi-common badge-success">Availed</span>
                          @endif
                        </td>
                          <td>
                              @if($client->status == 0)
                                <span class="badge badge-multi-common badge-warning">Pending</span>
                              @elseif($client->status == 1)
                                <span class="badge badge-multi-common badge-primary">Joined</span>
                              @endif
                          </td>
                        
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
  
                   <!--pagination:start -->
                   @if(!$invited_clients->isEmpty())
                        <div class="pagination-col mt-4 mb-3">
                          <nav aria-label="page navigation">
                            <ul class="pagination justify-content-end pagination-sm flex-wrap">
            
                              {{ $invited_clients->links('vendor.pagination.customnew') }}
            
                            </ul>
                          </nav>
                        </div>
            
                       @endif
                  @else
                    {{__('No client found')}}
                  @endif
                     <!--pagination:end -->
                </div>
                 <!-- freelancer-list:end -->  
              
              
                </div>
          </div>
      </div>
    </div> 
  </main>
@endsection
@push('custom-scripts')
@endpush