@extends('user.layouts.user_two')
@section('title', set_page_titile('My Vault'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container">
        <div class="inner-pg-layout project-layouts pt-4">
            <!-- main page title:start -->  
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title-main mb-3">Keyoxa Vault</h3>
                </div>
                <div class="col-md-6 text-right">
                    {{-- <span class="wallet-balance mr-4"><i class="fi-flaticon flaticon-wallet mr-2"></i>Vault Balance: <span class="amount-count"> $5575</span></span> --}}
                    <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ url()->previous() }}'">Back</button>
                </div>
            </div>
            <!-- main page title:end -->
            <div class="row mt-2">
                @include('user.layouts.includes.finance_tab')
                <div class="col-lg-9 pl-xl-0">
                    <div class="right-aside">         
                    <!-- freelancer-list:start -->
                    <div class="invoices-col bg-shadow">
                        <div class="row">
                            <div class="col-md-6">
                            
                                <p class="table-text pl-2">My Vault</p>
                             
                           
							</div>
						
							@php
								$title = '';
								$url = $_SERVER['REQUEST_URI'];
								$parts = parse_url($url);
								if(isset($parts['query']))
								{
									parse_str($parts['query'], $query);
									if(isset($query['title'])){
										$title = $query['title'];
									}
								}	
								
							@endphp
						
                            <div class="col-md-6">
								<form method="GET" action="{{route('finance.myVault')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
									<div class="search-bar p-0">
										<div class="input-group project-search">
										<input type="text"  value="{{$title}}" name="title" class="form-control border-right-0" placeholder="Search project name or milestone name" aria-label="Search project name or milestone name">
											<div class="input-group-append">
												<button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
											</div>
										</div>
									</div>
								</form>
                            </div>
                          
						</div>
						
                          <div class="table-multi-common table-responsive mt-3">
                          <table class="table table-striped">
                            <thead>
                              @if($vaults->isNotEmpty())
                              <tr>
								<th scope="col">Date</th>
								<th scope="col">Project</th>  
                                <th scope="col">Milestone</th>  
                                
                                <th scope="col">Credit</th>
                                <th scope="col">Debit</th>                     
                                {{-- <th scope="col">Action</th> --}}
                              </tr>
                              @else
                                        <tr>
                                            <td>{{__('No data found')}}</td>
                                        </tr>
                                    @endif
                            </thead>
                            <tbody>
								
								@foreach($vaults as $vault)
		
									<tr>
										<td>{{hcms_date(strtotime($vault->created_at), 'date', false)}}</td>
										<td> <a href="{{ route('project.inProgressProjectDetails' ,$vault->milestones->project->uuid) }}">{{str_limit(ucwords($vault->milestones->project->title), $limit = 50, $end = '...')}}</a></td>
										<td>{{str_limit(ucwords($vault->milestones->milestone_name), $limit = 50, $end = '...')}}</td>
									
										<td>@if($vault->transaction_type == 0){{formatAmount($vault->amount)}}@else {{formatAmount("0")}}@endif</td>         
                                        <td>@if($vault->transaction_type == 1){{formatAmount($vault->amount)}}@else {{formatAmount("0")}}@endif</td>                                         
										{{-- <td>  <button class="btn bg-nm-btn mr-2" type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></td> --}}
									</tr> 
								@endforeach               
								         
                            </tbody>
                          </table>
						  </div>
                        
                          @if(!$vaults->isEmpty())
                        <div class="pagination-col mt-4 mb-3">
                          <nav aria-label="page navigation">
                            <ul class="pagination justify-content-end pagination-sm flex-wrap">
            
                              {{ $vaults->links('vendor.pagination.customnew') }}
            
                            </ul>
                          </nav>
                        </div>
            
                       @endif
                	</div>
                    <!-- freelancer-list:end -->    
                    </div>
                </div>
            </div>
        </div> 
    </div>
</main>
@endsection
@push('custom-scripts')
@endpush