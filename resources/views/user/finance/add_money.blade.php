@extends('user.layouts.user_two')
@section('title', set_page_titile('Add Money'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container">
        <div class="inner-pg-layout project-layouts wallet-amount">
            <!-- main page title:start -->
            <div class="row">
            <div class="col-md-6">
                <h3 class="title-main">Finances</h3>
            </div>
            <div class="col-md-6 text-right">
                {{-- <span class="wallet-balance mr-4"><i class="fi-flaticon flaticon-wallet mr-2"></i>Keyoxa Balance: <span class="amount-count"> $5575</span></span> --}}
                <button class="btn cancel-shadow-btn" onclick="window.location.href='{{route('project.listInProgressProjects')}}'" type="submit"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all InProgress Projects</button> 
            </div>
        </div>
        <!-- main page title:end -->
        <div class="row mt-3">
            @include('user.layouts.includes.finance_tab')
            <div class="col-lg-9 pl-xl-0">
                <div class="right-aside bg-shadow">                
                    <div class="amount-filable-col"> 
                        @if($user->user_type == 0)
                            <h3>Add Vault Amount</h3>
                        @else
                            <h3>Add Wallet Amount</h3>
                        @endif
                        <form>
                            <div class="row mb-4">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wallet-amount">Amount</label>
                                    @if(isset($milestone) && $milestone != null)
                                        <input type="text" value="{{$milestone->milestone_amount}}" readonly class="form-control numericOnly"  aria-describedby="wallet-amount" placeholder="Enter the amount">
                                    @else
                                        <input type="text"   class="form-control numericOnly" id="wallet-amount" aria-describedby="wallet-amount" placeholder="Enter the amount">
                                    @endif
                                </div>
                            </div>  
                            </div>
                            <div class="desp mb-5">
                                @if(isset($milestone) && $milestone != null)
                                    <p>Please note that there will be a {{$transaction_fee}}% of transaction fee. So the <strong>total amount {{formatAmount($total_amount)}}</strong>  will be debited by Keyoxa for activating the milestone. After successfull payment, this will be reflected in your keyoxa vault.</p>
                                @endif
                            </div>                   
                            <div class="btn-cols text-right mb-2">
                            <button class="btn cancel-btn mr-2"  onclick="window.location.href='{{ url()->previous() }}'" >Cancel</button>
                            <button class="btn submit-btn" type="submit" id="checkout-button">Add Amount</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
  </div>
</main>

@endsection
@push('custom-scripts')
<script src="https://js.stripe.com/v3/"></script>

	 <script>
    @if(isset($milestone) && $milestone != null)
	var key = '{{$payment_keys->authentication_key}}';
	var stripe = window.Stripe(key);
	
	
	var checkoutButton = document.getElementById('checkout-button');

	checkoutButton.addEventListener('click', function() {
	stripe.redirectToCheckout({
		
		sessionId: '{{$checkout_session->id}}'
	}).then(function(result) {
         
          if (result.error) {
            alert(result.error.message);
          }
        })
        .catch(function(error) {
          console.error('Error:', error);
        });
	});
    @endif
    $('#wallet-amount').blur(function() {
        var inputString = $("#wallet-amount").val(); 
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
                type: "GET",
                url: "{{route('finance.createCheckoutSession')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'amount': inputString
                     },
                success: function (data) { 
                    console.log(data.payment_gateways);
                    // var key = data.payment_gateways.authentication_key;
                    // var stripe = window.Stripe(key);
                   
                    
                    // var checkoutButton = document.getElementById('checkout-button');

                    // checkoutButton.addEventListener('click', function() {
                    // stripe.redirectToCheckout({
                        
                    //     sessionId: data.checkout_session.id
                    // }).then(function(result) {
                        
                    //     if (result.error) {
                    //         alert(result.error.message);
                    //     }
                    //     })
                    //     .catch(function(error) {
                    //     console.error('Error:', error);
                    //     });
                    // });
                
                }
        });
    });
</script> 
@endpush