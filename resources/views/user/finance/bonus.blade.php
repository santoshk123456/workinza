@extends('user.layouts.user_two')
@section('title', set_page_titile('Bonus'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts pt-4">
           <!-- main page title:start -->
           <div class="row m-0">
            <div class="col-7">
               <h3 class="title-main">Referral Incentives / Bonus</h3>
            </div>
            <div class="col-5 text-right">
              <button class="btn bg-nm-btn" onclick="window.location.href='{{route('finance.referPerson')}}'" type="submit">Refer a Person</button>
             </div>
         </div>
         <!-- main page title:end -->
      
         <div class="row mx-0 mt-2">
            @include('user.layouts.includes.finance_tab',['user'=>$user])
          <div class="col-lg-9">
              <div class="right-aside">         
                <div class="bg-shadow mb-3 projects-links-menu">
                  <ul>
                  <li><a href="{{route('finance.listReferrals')}}">Referrals</a></li>
                    <li><a href="" class="active">Bonus</a></li>          
                  </ul>                
                </div> 
                
                <!-- freelancer-list:start -->
                <div class="freelancer-finance-holder project-search-listing bg-shadow">
               
                  @if(isset($resultArr) && count($resultArr) > 0)
                  @foreach($resultArr as $key=>$contract)
                  <div class="table-multi-common table-group table-responsive mb-3">
                    <table class="table table-striped">
                      <thead>
                        <tr class="table-group-header">
                          
                        <th class="title" colspan="8"><a href="{{ route('account.viewUserDetail',['user'=>$contract[0]->transactions->contracts->client,'project'=>$contract[0]->transactions->contracts->project,'status'=>"null"]) }}">{{$key}}</a></th>
                        </tr>
                        <tr>
                          <th scope="col">Joined On</th>
                          <th scope="col">Amount</th>
                          <th scope="col">Service Charge</th>
                          <th scope="col">Bonus</th>
                          <th scope="col">Paid Service Charge</th>
                          <th scope="col">Amount Received</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php
                           $total_amount = 0;
                           $total_freelancer_service_charge = 0;
                           $total_bonus = 0;
                           $total_amount_received = 0;
                           $total_service = 0;
                        @endphp
                        @foreach($contract as $key1 => $wallet)
                        <tr>
                         
                         
                          <td>@if(isset($wallet->transactions->contracts->client) && $wallet->transactions->contracts->client !="" ){{hcms_date(strtotime($wallet->transactions->contracts->client->created_at), 'date', false)}}@else{{__('NA')}}@endif</td>
                          <td>{{formatAmount($wallet->transactions->amount)}}</td>
                          <td>{{formatAmount($wallet->freelancer_service_charge)}}</td>
                          <td>{{formatAmount($wallet->referral_bonus_charge)}}</td>
                          <td><?php
                          $total_service_charge = "";
                          $total_service_charge = $wallet->freelancer_service_charge - $wallet->referral_bonus_charge;
                          ?>{{formatAmount($total_service_charge)}}</td>
                          <td>{{formatAmount($wallet->amount)}}</td>
                        </tr>
                       @php
                        
                         $total_amount = $total_amount + $wallet->transactions->amount;
                         $total_freelancer_service_charge = $total_freelancer_service_charge + $wallet->freelancer_service_charge;
                         $total_bonus = $total_bonus + $wallet->referral_bonus_charge;
                         $total_service = $total_service + $total_service_charge;
                         $total_amount_received = $total_amount_received + $wallet->amount;
                       @endphp
                       
                      @endforeach
                        <tr class="total">
                        
                        
                          <td>Total</td>
                          <td class="amount">{{formatAmount($total_amount)}}</td>
                          <td class="amount">{{formatAmount($total_freelancer_service_charge)}}</td>
                          <td class="amount">{{formatAmount($total_bonus)}}</td>
                          <td class="amount">{{formatAmount($total_service)}}</td>
                          <td class="amount">{{formatAmount($total_amount_received)}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  @endforeach
                  @else
                   {{__('No data found')}}
                  @endif
                  <!--pagination:start -->
                  {{-- <div class="pagination-col mt-4 mb-3">
                    <nav aria-label="page navigation">
                      <ul class="pagination justify-content-end pagination-sm flex-wrap">
                        <li class="page-item disabled">
                          <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fi-flaticon flaticon-angle-pointing-to-left"></i> Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>                       
                        <li class="page-item active" aria-current="page">
                          <a class="page-link" href="#">6 <span class="sr-only">(current)</span></a>
                        </li>                      
                        <li class="page-item"><a class="page-link" href="#">7</a></li>
                        <li class="page-item"><a class="page-link" href="#">8</a></li>
                        <li class="page-item"><a class="page-link" href="#">9</a></li>
                        <li class="page-item"><a class="page-link" href="#">10</a></li>
                        <li class="page-item">
                          <a class="page-link" href="#">Next <i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
                        </li>
                      </ul>
                    </nav>
                  </div> --}}
                     <!--pagination:end -->
                </div>
                 <!-- freelancer-list:end -->  
              
              
                </div>
          </div>
      </div>
    </div> 
  </main>
@endsection
@push('custom-scripts')
@endpush