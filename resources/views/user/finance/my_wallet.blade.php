@extends('user.layouts.user_two')
@section('title', set_page_titile('My Wallet'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container">
        <div class="inner-pg-layout project-layouts pt-4">
            <!-- main page title:start -->  
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title-main mb-3">My Wallet</h3>
                </div>
                <div class="col-md-6 text-right">
                    <span class="wallet-balance mr-4"><i class="fi-flaticon flaticon-wallet mr-2"></i>Wallet Balance: <span class="amount-count"> {{formatAmount($user->wallet_balance)}}</span></span>
                    {{-- <button onclick="window.location.href='{{route('finance.addMoney')}}'" class="btn bg-nm-btn" type="submit"><i class="fi-flaticon flaticon-plus mr-2"></i>Add Wallet Balance</button>   --}} <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ url()->previous() }}'">Back</button>                </div>
            </div>
            <!-- main page title:end -->
            <div class="row mt-2">
                @include('user.layouts.includes.finance_tab',['user'=>$user])
                <div class="col-lg-9 pl-xl-0">
                    <div class="right-aside">         
                    <!-- freelancer-list:start -->
                    <div class="invoices-col bg-shadow">
                        <div class="row">
                            <div class="col-md-6">
                              
                                <p class="table-text pl-2">My Wallet</p>
                              
                            </div>
                            <div class="col-md-6">
                                <div class="search-bar p-0">
                                    {{-- <div class="input-group project-search">
                                        <input type="text" class="form-control border-right-0" placeholder="Search project by name or keyword" aria-label="Search project  by name or keyword">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary border-left-0" type="button"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                       
                        <div class="table-multi-common table-responsive mt-3">
                    <table class="table table-striped">
                      <thead>
                        @if($wallets->isNotEmpty())
                        <tr>
                          <th scope="col">Date</th>
                          <th scope="col">Description</th>  
                          <th scope="col">Debit</th>
                          <th scope="col">Credit</th>
                          <th scope="col">Service Charge</th>
                          <th scope="col">Referral/Bonus Deduction</th>
                          <th scope="col">Balance</th>                                        
                          {{-- <th scope="col">Action</th> --}}
                        </tr>
                        @else
                          <tr>
                              <td>{{__('No data found')}}</td>
                          </tr>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($wallets as $wallet)

                            <tr>
                                <td>{{hcms_date(strtotime($wallet->created_at), 'date', false)}}</td>
                                <td>{{$wallet->notes}}</td>
                                <td>@if($wallet->transaction_type == 1){{formatAmount($wallet->amount)}}@else {{formatAmount("0")}}@endif</td>     
                                <td>@if($wallet->transaction_type == 0){{formatAmount($wallet->amount)}}@else {{formatAmount("0")}}@endif</td>         
                                <td>{{formatAmount($wallet->freelancer_service_charge)}}</td>
                                <td>@if(isset($wallet->referral_bonus_charge) && $wallet->referral_bonus_charge !=""){{formatAmount($wallet->referral_bonus_charge)}}@else{{formatAmount("0")}}@endif</td>
                                <td>{{formatAmount($wallet->new_balance)}}</td>         
                                                                                
                                {{-- <td>  <button class="btn bg-nm-btn mr-2" type="submit"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></td> --}}
                            </tr> 
                        @endforeach               
                      </tbody>
                    </table>
                  </div>
                
                  @if(!$wallets->isEmpty())
                  <div class="pagination-col mt-4 mb-3">
                    <nav aria-label="page navigation">
                      <ul class="pagination justify-content-end pagination-sm flex-wrap">
      
                        {{ $wallets->links('vendor.pagination.customnew') }}
      
                      </ul>
                    </nav>
                  </div>
      
                 @endif
                </div>
                
                    <!-- freelancer-list:end -->    
                    </div>
                </div>
            </div>
        </div> 
    </div>
</main>
@endsection
@push('custom-scripts')
@endpush