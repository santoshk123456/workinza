@extends('user.layouts.user_two')
@section('title', set_page_titile('Referrals'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts pt-4">
           <!-- main page title:start -->
           <div class="row m-0">
            <div class="col-7">
               <h3 class="title-main">Referral Incentives / Bonus</h3>
            </div>
            <div class="col-5 text-right">
              <button class="btn bg-nm-btn" onclick="window.location.href='{{route('finance.referPerson')}}'" type="submit">Refer a Person</button>
             </div>
         </div>
         <!-- main page title:end -->
      <div class="row mx-0 mt-2">
            @include('user.layouts.includes.finance_tab',['user'=>$user])
          <div class="col-lg-9">
              <div class="right-aside">         
                <div class="bg-shadow mb-3 projects-links-menu">
                  <ul>
                    <li><a href="" class="active">Referrals</a></li>
                    <li><a href="{{route('finance.listBonus')}}">Bonus</a></li>        
                  </ul>                
                </div> 
                
                <!-- freelancer-list:start -->
                @php
								$title = '';
								$url = $_SERVER['REQUEST_URI'];
								$parts = parse_url($url);
								if(isset($parts['query']))
								{
									parse_str($parts['query'], $query);
									if(isset($query['username'])){
										$title = $query['username'];
									}
								}	
								
							@endphp
                <div class="freelancer-finance-holder project-search-listing bg-shadow">
                  <form method="GET" action="{{route('finance.listReferrals')}}">
                  <div class="search-bar mb-3">
                    <div class="row">
                       <div class="col-12">
                          <div class="input-group project-search">
                             <input type="text" value="{{$title}}" name="username" class="form-control border-right-0" placeholder="Search by username" aria-label="Search by username">
                             <div class="input-group-append">
                                <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
                  </form>
                 <div class="table-multi-common table-responsive">
                  <table class="table table-striped">
                    <thead>
                      @if($wallets->isNotEmpty())
                      
                      <tr>
                        <th scope="col">Referred Client</th>
                        <th scope="col">Joined On</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Service Charge</th>
                        <th scope="col">Referral Bonus</th>
                        <th scope="col">Paid Service Charge</th>
                        <th scope="col">Amount Received</th>
                      </tr>
                      @else
                      <tr>
                          <td>{{__('No data found')}}</td>
                      </tr>
                    @endif
                    </thead>
                    <tbody>
                      @foreach($wallets as $wallet)
                      <tr>
                      <td>
                        <?php 
                        $status ="null";
                        ?>       
                        @if(isset($wallet->transactions->contracts->client) && $wallet->transactions->contracts->client !="" )
                        <a href="{{ route('account.viewUserDetail',['user'=>$wallet->transactions->contracts->client,'project'=>$wallet->transactions->contracts->project,'status'=>$status]) }}">{{$wallet->transactions->contracts->client->username}}</a>
                        @else{{__('NA')}}@endif</td>
                        <td>@if(isset($wallet->transactions->contracts->client) && $wallet->transactions->contracts->client !="" ){{hcms_date(strtotime($wallet->transactions->contracts->client->created_at), 'date', false)}}@else{{__('NA')}}@endif</td>
                        <td>{{formatAmount($wallet->transactions->amount)}}</td>
                        <td>{{formatAmount($wallet->freelancer_service_charge)}}</td>
                        <td>{{formatAmount($wallet->referral_bonus_charge)}}</td>
                        <td><?php
                        $total_service_charge = "";
                        $total_service_charge = $wallet->freelancer_service_charge - $wallet->referral_bonus_charge;
                        ?>{{formatAmount($total_service_charge)}}</td>
                        <td>{{formatAmount($wallet->amount)}}</td>
                      </tr>
                    @endforeach   
                    
                     
                    </tbody>
                  </table>
                </div>
   
                   <!--pagination:start -->
                   @if(!$wallets->isEmpty())
                        <div class="pagination-col mt-4 mb-3">
                          <nav aria-label="page navigation">
                            <ul class="pagination justify-content-end pagination-sm flex-wrap">
            
                              {{ $wallets->links('vendor.pagination.customnew') }}
            
                            </ul>
                          </nav>
                        </div>
            
                       @endif
                     <!--pagination:end -->
                </div>
                 <!-- freelancer-list:end -->  
              
              
                </div>
          </div>
      </div>
    </div> 
  </main>
@endsection
@push('custom-scripts')
@endpush