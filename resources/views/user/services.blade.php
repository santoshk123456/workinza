@extends('user.layouts.homelayout')
@section('title', set_page_titile(__('Services')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content') 

<body itemscope="" itemtype="http://schema.org/WebPage">
 
  

<?php 
$categories_list =  App\Category::with('subcategories')->where('active',1)->get();
?>



<main class="category-service-wrapper">
<!-- banner section: start -->
<section class="banner-contact-us">
    <div class="container">
        <div class="content text-center">
                <h2 class="title">Services</h2>
        </div>
    </div>
</section>
<!-- banner section: end -->

<!-- top freelancer section :start -->
<section class="top-freelancer">
    <div class="container">
        <!-- <div class="section-title">
            <h2 class="mx-auto">Let help tackle your to-do list!</h2>           
        </div> -->
        <div class="icon-cards">
            <div class="row">     
              
            
            @foreach($categories_list as $Categoryname)

          




                <div class="col-md-4">
                    <div class="card item text-left">
 
                      @if(!empty($Categoryname->icon) && (file_exists( public_path().'/storage/admin/categories/'.$Categoryname->icon)))
                        <img class="card-img-top" src="{{asset('storage/admin/categories'.$Categoryname->icon)}}" style="height: 60px; width:60px; margin: auto;" alt="image-post-job">
                      @else
                      <div class="icon icon-default-cat"></div>
                      @endif
                        <!-- <div class="icon icon-01"></div> -->
                        <div class="card-body">
                            <div class="card-top border-bottom pb-2">
                            <?php
                      $category= $Categoryname->name;
                          ?>
                                <h5 class="card-title text-center" id="{{ $category }}"><a href="{{route('homeservices')}}"> {{$Categoryname->name}} </a> </h5>
                                <!-- <p class="card-text">Software development is the process of conceiving, specifying, designing, programming,.</p> -->
                            </div>
                          <ul class="pt-3 sub-category-list">
                                 
                             @foreach($Categoryname->subcategories as $subCategoryname)
                    
                                  <li><a href="{{route('user.register')}}">{{$subCategoryname->name}} </a></li>
                                 
                              @endforeach
                          </ul>
                        </div>
                      </div>
                </div>   
                
                @endforeach




       
            </div>
        </div>
    </div>
</section>



<!-- top freelancer section :end -->

</main>

@endsection
@push('custom-scripts')

@endpush