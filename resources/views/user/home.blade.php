<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Keyoxa Coming Soon</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="{{asset('/css/home/assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('/css/home/assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('/css/home/assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('/css/home/assets/css/style.css')}}" rel="stylesheet">

  <!-- =======================================================
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <!-- <h1 class="text-light"><a href="index.html"><span>Keyoxa</span></a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="#"><img src="{{asset('/css/home/assets/img/logo.png')}}" alt="" class="img-fluid"></a>
      </div>

    </div>
  </header><!-- End #header -->

  <!-- ======= Hero Section ======= -->
  <div class="main-wrapper">
  <section id="hero">
    <div class="hero-container container">
      <h1>Coming Soon</h1>
      <p class="info">Hire authentic and trusted key experts. Manage your complete projects online.<br> KEYOXA - The one-stop solution for all your freelancing services.<br></p>
      

      <!-- <div class="countdown" data-count="2020/11/17" data-template="%w weeks %d days %H:%M:%S"></div> -->
      <p class="info">Be the first to get notified when Keyoxa.com is launched and avail exclusive early discounts.<br><br> Subscribe to KEYOXA now!</p>
      <!-- <form action="{{asset('/css/home/assets/forms/notify.php')}}" method="post" role="form" class="php-email-form"> -->
      <form id="feedback_form"  method="POST" enctype="multipart/form-data" class="php-email-form" action="{{ route('send_email_notification')}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf
                    
        <div class="row no-gutters">
          <div class="col-md-6 form-group pr-md-1">
            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
            <div class="validate"></div>
          </div>
          <div class="col-md-6 form-group pl-md-1">
            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
            <div class="validate"></div>
          </div>
        </div>

        <div class="mb-1">
          <div class="loading">Loading</div>
          <div class="error-message"></div>
          <div class="sent-message">Thank you for subscribing with KEYOXA. <br> Will send you an email about the launch.<br> Stay tuned!</div>
        </div>
        <div class="text-center"><button type="submit">Get notified!</button>
        </div>
      </form>
    </div>
  </section><!-- End Hero -->



  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>keyoxa</span></strong>. All Rights Reserved
      </div>

    </div>
  </footer><!-- End #footer -->
  </div>
  <!-- Vendor JS Files -->
  <script src="{{asset('/css/home/assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('/css/home/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('/css/home/assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('/css/home/assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{asset('/css/home/assets/vendor/jquery-countdown/jquery.countdown.min.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('/css/home/assets/js/main.js')}}"></script>

</body>

</html>
