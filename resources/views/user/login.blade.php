@extends('user.layouts.user_one')
@section('title', set_page_titile(__('Login')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content') 
<!--start: create account page -->


<div class="content-area">
    <div class="container">
        <div class="login-signup-cols accounts-cols">
          <div class="login-col">
            <h2>Login to Account</h2>
            <p class="desptn">This is a secure system and you will need to provide your login details to access the site.</p>
         
            <form method="post" id="login_form" action="{{route('login')}}">
            @csrf 
            <div class="form-group">               
            <input value="{{old('email')}}" type="email" name="email" class="form-control" id="email" minlength="3" maxlength="250" placeholder="{{__('Enter Email')}}" required>
            <span class="invalid-feedback error-txt email-error">
                @if ($errors->has('email'))
                {{ $errors->first('email') }}
                @endif
             </span>        
              </div>

              <div class="form-group">               
                <input value="{{old('password')}}" type="password" name="password" class="form-control" id="password" minlength="6" maxlength="250" placeholder="{{__('Enter Password')}}" required>
                <span class="invalid-feedback error-txt password-error">
                  @if ($errors->has('password'))
                  {{ $errors->first('password') }}
                  @endif
                </span>        
              </div>


              <div class="row mb-3 no-gutters">
                <div class="col-6">
                  <div class="custom-control custom-checkbox text-left">
                    <input type="checkbox" name="remember" class="custom-control-input" id="backVerification" {{ old('remember') ? 'checked' : '' }}>
                    <label class="custom-control-label" for="backVerification">Remember me</label>
                  </div>
                </div>
                <div class="col-6 text-right">
                  <a href="{{route('forgotPassword')}}">Forgot password?</a>
                </div>
              </div>   
              <div class="btn-cols mb-2">                
                <button class="btn log-reg-btn w-100" type="submit">Sign In</button>
              </div> 
            </form>


          </div>
            <div class="signup-col">
              <p><span>Don’t have an account?</span></p>
              <button class="btn signup-btn mb-2" onclick="location.href='{{route('user.register')}}'" type="button">Sign Up</button>
            </div>         </div>
        
    </div>
</div>


<!--end: create account page -->
@endsection
@push('custom-scripts')
<script>
 $("#login_form").validate({
  onfocusout: function(element){
        $(element).valid();
   	
    },
    onkeyup: function(element){
        $(element).next().text('');
   	
    },
    rules: {
        email: {
            required: true,
            maxlength: 50,
            email: true,
        },
        password:  {
            required: true,
            minlength: 6,
           
        }
       
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } 
        else if (element.attr("name") == "email" ) {
          $(".email-error").html(error); 
        }
        else if (element.attr("name") == "password" ) {
          $(".password-error").html(error); 
        }
        else {
            error.insertAfter(element);
        }
   },

   messages: {
            'email': {
                required: "Email is required.",
            },
            'password': {
                required: "Password is required.",
            },
        }

});
</script>

<script>

$('.pass-icons').click(function(e){
    $('#pass_icon').hasClass('flaticon-eye-1')?hidePassword():showPassword()
})
function hidePassword(){
    $('#pass_icon').removeClass('flaticon-eye-1').addClass('flaticon-visibility')
    $('#password').attr('type','text')
}
function showPassword(){
    $('#pass_icon').removeClass('flaticon-visibility').addClass('flaticon-eye-1')
    $('#password').attr('type','password')
}

</script>
@endpush