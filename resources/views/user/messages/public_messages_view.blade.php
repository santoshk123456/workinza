@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Messages Detail')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
  <div class="container inner-pg-layout project-layouts">

                  <!-- main page title:start -->
                  <div class="row m-0 mb-3">
                    <div class="col-sm-6">
                    <h3 class="title-main">{{ucwords($project->title)}}</h3>
                    </div>
                    <div class="col-sm-6 text-sm-right mt-sm-0 mt-3">
                    <button class="btn cancel-shadow-btn" type="button" onclick="location.href='{{url()->previous()}}'"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>{{__('List all Messages')}}</button>
                    </div>
                   </div>
                    <!-- main page title:end -->
    
    
    <div class="row mx-0 mt-4">
    <div class="col-lg-3 left-aside left-projects-list">
        <ul class="d-block">
          <li class="active"><a href="{{route('message.publicMessages')}}">{{__('Messages')}}</a></li>
      
        </ul>
      </div>
        <div class="col-lg-9">
              
              <!-- messages-holder:start -->
              <div class="public-messages-holder bg-shadow">
              
                <div id="frame">
                  <div class="content">
                    <div class="contact-profile">
                      <p> {{ucwords($user->username)}}</p>
                      <!-- <div class="social-media">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                         <i class="fa fa-instagram" aria-hidden="true"></i>
                      </div> -->
                    </div>
                    <div class="messages">
                      <ul>
                      @foreach($chats as $key=>$chat)
                        <li @if($chat->sender->id==auth('web')->user()->id) class="replies" @else class="sent" @endif>
                        @if($chat->sender->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$chat->sender->profile_image)))
                                            <img src=" {{ asset('storage/user/profile-images/'.$chat->sender->profile_image) }}" alt="" />
                                            @else
                                            <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $chat->sender->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                            @endif
                          <span class="message-label">{{ucwords($chat->sender->username)}}, {{timeAgoConversion($chat->created_at)}}</span>
                          <p>{{$chat->message}}</p>
                          @if(!empty($chat->attachment))
                                            <p> <a href="{{asset('storage/projects/comment_attachements/'.$chat->attachment)}}" class="btn white-nm-btn download" download="{{$chat->attachment}}" title="Download Attachment"><i class="fi-flaticon flaticon-download"></i></a></p>
                                            @endif
                        </li>
                        @endforeach
                      </ul>
                    </div>
                    <div class="message-input">
                    <form method="POST" action="{{route('message.PostMessage')}}" enctype="multipart/form-data" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="message-form" novalidate>
                                        @csrf
                      <div class="wrap">
                      <input required type="text" id="message" name="message" placeholder="{{__('Write your message...')}}" />
                      <!-- <i class="fi-flaticon flaticon-attachments mr-2"></i> -->
                      <div class="attachments">
                        <input type="file" id="attachement" name="attachement"><i class="fi-flaticon flaticon-attachments mr-2"></i>
                        <input type="hidden" id="to_user_id" name="to_user_id" value="{{$user->id}}">
                                            <input type="hidden" id="project_id" name="project_id" value="{{$project->id}}">
                                            <input type="hidden" id="user_uuid" name="user_uuid" value="{{$user->uuid}}">
                                            <input type="hidden" id="page_status" name="page_status" value="1">
                                            <input type="hidden" id="project_uuid" name="project_uuid" value="{{$project->uuid}}">
                      </div>
                      <button class="submit" disabled id="message-submit"><i class="fi-flaticon flaticon-send mr-2"></i></button>
                      </div>
                    </form>
                    </div>
                  </div>
                </div>
              

              </div>
               <!-- messages-holder:end -->

  
            </div>
        </div>
    </div>
  </div> 
</main>

<!--end: contact information page -->

@endsection
@push('custom-scripts')
<script>
      $("#message-form").submit(function() {
        var message = document.getElementById('message').value;
        if (message == "") {
            return false;
        }
    });

    $("#message").keyup(function() {
        var message = document.getElementById('message').value;
        if (message != "") {
            document.getElementById("message-submit").disabled = false;
           
        } else {
            document.getElementById("message-submit").disabled = true;
        }
    });
</script>
@endpush