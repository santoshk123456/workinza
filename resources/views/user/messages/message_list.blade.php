@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Messages')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
@php
$left_menu = !empty(Session::get('left_page'))?Session::get('left_page')[0]:0;
@endphp
<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">
        <h3 class="title">{{ucwords($project->title)}}</h3>
        <div class="row mx-0">
            @if(Auth()->user()->user_type==0)
            <div class="col-lg-3 left-aside left-projects-list">
                <ul class="d-block">
                    
                    <li @if($project->status==1) class="active" @endif><a href="{{route('project.listOpenProjects')}}">{{__('Open Projects')}}</a></li>
                   
                    <li @if($project->status==2) class="active" @endif><a href="{{route('project.listInProgressProjects')}}">{{__('In Progress Projects')}}</a></li>
                 
                    <li><a href="{{route('project.listCompletedProjects')}}">{{__('Completed Projects')}}</a></li>
                   
                </ul>
            </div>
            @else
            <div class="col-lg-3 left-aside left-projects-list">         
                <ul class="d-block">
                <li ><a href="{{route('project.listProposals')}}" @if($left_menu==1) class="active" @endif>My Proposals</a></li>
                    <li ><a href="{{route('project.freelancerlistInProgressProjects')}}" @if($project->status==2) class="active" @endif @if($left_menu==2) class="active" @endif>Assigned / In Progress Projects</a></li>
                    <li><a href="{{route('project.listCompletedProjects')}}" @if($left_menu==3) class="active" @endif>Completed Projects</a></li>
                    <li ><a href="{{route('project.listInvitations')}}" @if($left_menu==4) class="active" @endif>Invitations</a></li>
                </ul>
            </div>
            @endif
            <div class="col-lg-9">
                <div class="right-aside">
                    <div class="bg-shadow mb-3 projects-links-menu">
                       
                        @if(Auth()->user()->user_type==0)
                        <ul>
                            <li><a href="{{ route('project.projectDetails',$project) }}"><i class="fi-flaticon flaticon-blank-page mr-2"></i>{{__('Project Details')}} </a></li>
                           @if($project->status==1 || $project->status==0)
                            <li><a href="{{ route('project.inviteFreelancers',$project) }}"><i class="fi-flaticon flaticon-send mr-2"></i>{{__('Invite Freelancers')}}</a></li>
                          
                            <li><a href="{{ route('project.projectProposals',$project) }}"><i class="fi-flaticon flaticon-auction mr-2"></i>{{__('Proposals')}}</a></li>
                            @endif
                            @if($project->status==2 || $project->status==3)
                            <li><a href="{{ route('finance.projectTransactions',$project) }}"><i class="fi-flaticon flaticon-dollar-sign-with-rotating-arrows mr-2"></i>Transactions</a></li>
                            @endif
                          
                            @if(App\ProjectChat::ChatFeature()==1)
                            <li><a href="{{ route('message.messages',$project) }}" class="active"><i class="fi-flaticon flaticon-comments mr-2"></i>{{__('Messages')}}</a></li>
                           @endif
                        </ul>
                            @else
                            <ul>
                            @if($left_menu==2)
                            <li><a href="{{ route('project.inProgressProjectDetails',$project) }}"><i class="fi-flaticon flaticon-blank-page mr-2"></i>{{__('Project Details')}} </a></li>
                           
                            <li><a href="{{ route('finance.projectTransactions',$project) }}"><i class="fi-flaticon flaticon-dollar-sign-with-rotating-arrows mr-2"></i>Transactions</a></li>
                            @endif
                            <li><a href="{{ route('message.messages',$project) }}" class="active"><i class="fi-flaticon flaticon-comments mr-2"></i>{{__('Messages')}}</a></li>
                            </ul>
                            @endif
                       
                    </div>

                    <!-- messages-holder:start -->
                    @if($to_count!=0 || !empty($freelancer->username) || !empty($chats))
                    <div class="messages-holder bg-shadow">

                        <div id="frame">
                            <div id="sidepanel">
                                <div id="contacts">
                                    @if(auth()->user()->user_type==0 && count($archive_highlights)>0)
                                    <div class="archive-action" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                        <h2>{{__('Archived Chats')}}</h2>
                                        <button class="btn btn-archived-count btn-primary">{{__('View All')}}</button>
                                    </div>
                                    <div class="collapse" id="collapseExample">
                                        <div class="row un-acrcive-button">
                                            <div class="col-12 text-right">
                                                <button class="btn btn-primary" id="un-archive">{{__('Un Archive')}}</button>
                                            </div>
                                        </div>
                                        <ul class="archived-list">
                                            <!-- <div class="archive-action">
                                            <h2>{{__('Archived Chats')}}</h2>
                                            <button class="btn btn-primary" id="un-archive">{{__('Un Archive')}}</button>
                                        </div> -->
                                            @foreach($archive_highlights as $key=>$archive_highlight)
                                            @if(auth('web')->user()->id!=$archive_highlight->from_user_id)
                                            @php $to_user=$archive_highlight->sender;
                                            @endphp
                                            @endif
                                            @if(auth('web')->user()->id!=$archive_highlight->to_user_id)
                                            @php $to_user= $archive_highlight->recipient;
                                            @endphp
                                            @endif
                                            <a href="{{route('message.messages',array($project->uuid,$to_user->uuid))}}">
                                                <li @if($to_user->id == $freelancer->id) class="contact active" @else class="contact" @endif>
                                                    <div class="wrap">
                                                        @if($to_user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$to_user->profile_image)))
                                                        <img src="{{ asset('storage/user/profile-images/'.$to_user->profile_image) }}" alt="" />
                                                        @else
                                                        <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $freelancer->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                        @endif
                                                        <div class="meta">
                                                            <p class="name">{{ucwords($to_user->username)}}<span>{{timeAgoConversion($archive_highlight->created_at)}}</span></p>
                                                            <p class="preview">{{$archive_highlight->message}}</p>
                                                            <div class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="archive-list[]" class="custom-control-input archive-list" id="archive{{$to_user->id}}" value="{{$to_user->id}}">
                                                                <label class="custom-control-label" for="archive{{$to_user->id}}"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </a>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <ul class="non-archive-list">
                                        <div class="archive-action text-right">
                                            <h2>{{__('Chats')}}</h2>
                                            @if(auth()->user()->user_type==0)
                                            <button class="btn btn-primary" id="archive">{{__('Archive')}}</button>
                                            @endif
                                        </div>

                                        @if($to_count==0 && !empty($freelancer->username))
                                        <li class="contact active">
                                            <div class="wrap">
                                                @if($freelancer->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$freelancer->profile_image)))
                                                <img src="{{ asset('storage/user/profile-images/'.$freelancer->profile_image) }}" alt="" />
                                                @else
                                                <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $freelancer->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                @endif
                                                <div class="meta">
                                                    <p class="name">{{ucwords($freelancer->username)}}</p>
                                                    {{-- <p class="preview">{{__('No Messages Found')}}</p> --}}

                                                </div>
                                            </div>
                                        </li>
                                        @endif

                                        @foreach($non_archive_highlights as $key=>$chat_highlight)
                                        @if(auth('web')->user()->id!=$chat_highlight->from_user_id)
                                        @php $to_user=$chat_highlight->sender;
                                        @endphp
                                        @endif
                                        @if(auth('web')->user()->id!=$chat_highlight->to_user_id)
                                        @php $to_user= $chat_highlight->recipient;
                                        @endphp
                                        @endif
                                        
                                        <a href="{{route('message.messages',array($project->uuid,$to_user->uuid))}}">
                                        <li @if($to_user->id == $freelancer->id) class="contact active" @else class="contact" @endif>
                                            
                                                <div class="wrap">

                                                    @if($to_user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$to_user->profile_image)))
                                                    <img src=" {{ asset('storage/user/profile-images/'.$to_user->profile_image) }}" alt="" />
                                                    @else
                                                    <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $to_user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                    @endif
                                                    <div class="meta">
                                                        <p class="name">{{ucwords($to_user->username)}}<span>{{timeAgoConversion($chat_highlight->created_at)}}</span></p>
                                                        <p class="preview">{{$chat_highlight->message}}</p>
                                                        @if(auth()->user()->user_type==0)
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="non-archive-list[]" class="custom-control-input non-archive-list" id="non-archive{{$to_user->id}}" value="{{$to_user->id}}">

                                                            <label class="custom-control-label" for="non-archive{{$to_user->id}}"></label>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                           
                                        </li>
                                        </a>
                                        @endforeach

                                        @foreach($dispute_chats as $key=>$dispute_chat)
                                        
                                        <a href="{{route('message.disputeMessages',array($dispute_chat->dispute_id,$dispute_chat->project_id))}}">
                                        <li class="contact">
                                                <div class="wrap">
                                                <img src="{{ asset('images/admin/starter/group.jpeg') }}" alt="{{ $freelancer->id }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                    <div class="meta">
                                                        <p class="name">{{ucwords($dispute_chat->dispute->freelancer->username)}}, {{ucwords($dispute_chat->dispute->client->username)}}, {{ucwords($admin->username)}}<span>{{timeAgoConversion($dispute_chat->created_at)}}</span></p>
                                                        <p class="preview">{{$dispute_chat->message}}</p>
                                                        @if(auth()->user()->user_type==0)
                                                        <div class="custom-control custom-checkbox">
                                                            <!-- <input type="checkbox" name="non-archive-list[]" class="custom-control-input non-archive-list" id="non-archive{{$dispute_chat->sender->id}}" value="{{$dispute_chat->sender->id}}"> -->

                                                            <!-- <label class="custom-control-label" for="non-archive{{$dispute_chat->sender->id}}"></label> -->
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                           
                                        </li>
                                        </a>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="content">
                                <div class="contact-profile">
                                    <p> {{ucwords($freelancer->username)}}</p>
                                    <!-- <div class="social-media">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div> -->
                                </div>
                                <div class="messages">
                                    <ul>
                                        @foreach($chats as $key=>$chat)

                                        <li @if($chat->sender->id==auth('web')->user()->id) class="replies" @else class="sent" @endif >
                                            @if($chat->sender->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$chat->sender->profile_image)))
                                            <img src=" {{ asset('storage/user/profile-images/'.$chat->sender->profile_image) }}" alt="" />
                                            @else
                                            <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $chat->sender->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                            @endif
                                            <span class="message-label">{{ucwords($chat->sender->username)}}, {{timeAgoConversion($chat->created_at)}}</span>
                                            <p>{{$chat->message}}</p>
                                            @if(!empty($chat->attachment))
                                            <p> <a href="{{asset('storage/projects/comment_attachements/'.$chat->attachment)}}" class="btn white-nm-btn download" download="{{$chat->attachment}}" title="Download Attachment"><i class="fi-flaticon flaticon-download"></i></a></p>
                                            @endif
                                        </li>

                                        <!-- <li class="sep"><p>Today</p></li> -->
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="message-input">
                                    <form method="POST" action="{{route('message.PostMessage')}}" enctype="multipart/form-data" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="message-form" novalidate>
                                        @csrf
                                        <div class="wrap">
                                            <input required type="text" id="message" name="message" placeholder="{{__('Write your message...')}}" />

                                            <!-- <i class="fi-flaticon flaticon-attachments mr-2"></i> -->
                                            <div class="attachments">
                                                <input type="file" id="attachement" name="attachement"><i class="fi-flaticon flaticon-attachments mr-2"></i>
                                            </div>
                                            <input type="hidden" id="to_user_id" name="to_user_id" value="{{$freelancer->id}}">
                                            <input type="hidden" id="project_id" name="project_id" value="{{$project->id}}">
                                            <input type="hidden" id="user_uuid" name="user_uuid" value="{{$freelancer->uuid}}">
                                            <input type="hidden" id="project_uuid" name="project_uuid" value="{{$project->uuid}}">
                                            <button class="submit" disabled id="message-submit"><i class="fi-flaticon flaticon-send mr-2"></i></button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>


                    </div>
                    @else
                    <div class="proposals-list-holder bg-shadow">
                        <div class="col-list-item">                    
                            <div class="media text-center text-md-left d-block d-md-flex">
                                No messages found on this project
                            </div>
                        </div>
                    </div>
                   @endif
                    <!-- messages-holder:end -->


                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@push('custom-scripts')
<script>
    $("#message-form").submit(function() {
        var message = document.getElementById('message').value;
        if (message == "") {
            return false;
        }
    });

    $("#message").keyup(function() {
        var message = document.getElementById('message').value;
        if (message != "") {
            document.getElementById("message-submit").disabled = false;
           
        } else {
            document.getElementById("message-submit").disabled = true;
        }
    });
    $("#archive").click(function() {

        var to_users = [];
        var project_id = document.getElementById('project_id').value;
        var inputElements = document.getElementsByClassName('non-archive-list');

        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                to_users.push(inputElements[i].value);
            }
        }
        if (to_users.length > 0) {

            $.ajax({
                type: "GET",
                url: "{{route('message.ChangeArchive')}}",
                data: {
                    'to_users': to_users,
                    'project_id': project_id,
                    'type': 1
                },
                success: function(response) {
                    //console.log(response);
                    location.reload();
                }
            });
        } else {
            swal({
            title: 'List is Empty',
            type: 'warning',
            text: "You want to choose the users",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
           
            closeOnConfirm: false,
           
          })

        }

    });


    $("#un-archive").click(function() {
        var to_users = [];
        var project_id = document.getElementById('project_id').value;
        var inputElements = document.getElementsByClassName('archive-list');

        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                to_users.push(inputElements[i].value);
            }
        }
        if (to_users.length > 0) {

            $.ajax({
                type: "GET",
                url: "{{route('message.ChangeArchive')}}",
                data: {
                    'to_users': to_users,
                    'project_id': project_id,
                    'type': 0
                },
                success: function(response) {
                    location.reload();

                }
            });
        } else {
            swal({
            title: 'List is Empty',
            type: 'warning',
            text: "You want to choose the users",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
           
            closeOnConfirm: false,
           
          })

        }

    });
</script>
@endpush