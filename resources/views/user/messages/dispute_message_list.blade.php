@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Messages')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">
        <h3 class="title">{{ucwords($project->title)}}</h3>
        <div class="row mx-0">
            @if(Auth()->user()->user_type==0)
            <div class="col-lg-3 left-aside left-projects-list">
                <ul class="d-block">
                    
                    <li @if($project->status==1) class="active" @endif><a href="{{route('project.listOpenProjects')}}">{{__('Open Projects')}}</a></li>
                   
                    <li @if($project->status==2) class="active" @endif><a href="{{route('project.listInProgressProjects')}}">{{__('In Progress Projects')}}</a></li>
                 
                    <li><a href="{{route('project.listCompletedProjects')}}">{{__('Completed Projects')}}</a></li>
                   
                </ul>
            </div>
            @else
            <div class="col-lg-3 left-aside left-projects-list">         
                <ul class="d-block">
                <li ><a href="{{route('project.listProposals')}}">My Proposals</a></li>
                    <li><a href="{{route('project.freelancerlistInProgressProjects')}}">Assigned / In Progress Projects</a></li>
                    <li><a href="{{route('project.listCompletedProjects')}}">Completed Projects</a></li>
                    <li class="active"><a href="{{route('project.listInvitations')}}">Invitations</a></li>
                </ul>
            </div>
            @endif
            <div class="col-lg-9">
                <div class="right-aside">
                    <div class="bg-shadow mb-3 projects-links-menu">
                        <ul>
                            @if(Auth()->user()->user_type==0)
                            <!-- <li><a href="{{ route('project.projectDetails',$project) }}"><i class="fi-flaticon flaticon-blank-page mr-2"></i>{{__('Project Details')}} </a></li>
                            <li><a href="{{ route('project.inviteFreelancers',$project) }}"><i class="fi-flaticon flaticon-send mr-2"></i>{{__('Invite Freelancers')}}</a></li>
                            <li><a href="{{ route('project.projectProposals',$project) }}"><i class="fi-flaticon flaticon-auction mr-2"></i>{{__('Proposals')}}</a></li> -->
                           @endif
                            @if(App\ProjectChat::ChatFeature()==1)
                            <li><a href="{{ route('message.messages',$project) }}" class="active"><i class="fi-flaticon flaticon-comments mr-2"></i>{{__('Messages')}}</a></li>
                            @endif
                        </ul>
                    </div>

                    <!-- messages-holder:start -->
                    @if($to_count!=0 || !empty($user->username) || !empty($chats))
                    <div class="messages-holder bg-shadow">

                        <div id="frame">
                            <div id="sidepanel">
                                <div id="contacts">
                                    
                                    <ul class="non-archive-list">
                                        <div class="archive-action text-right">
                                            <h2>{{__('Chats')}}</h2>
                                        </div>

                                        @if($to_count==0 && !empty($user->username))
                                        <li class="contact active">
                                            <div class="wrap">
                                            <img src="{{ asset('images/admin/starter/group.jpeg') }}" alt="{{ $user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                <div class="meta">
                                                    <p class="name">{{ucwords($dispute->freelancer->username)}}, {{ucwords($dispute->client->username)}}, {{ucwords($admin->username)}}</p>
                                                    {{-- <p class="preview">{{__('No Messages Found')}}</p> --}}

                                                </div>
                                            </div>
                                        </li>
                                        @else
                                        <li class="contact active">
                                            <div class="wrap">
                                                <img src="{{ asset('images/admin/starter/group.jpeg') }}" alt="{{ $user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                <div class="meta">
                                                    <p class="name">{{ucwords($dispute->freelancer->username)}}, {{ucwords($dispute->client->username)}}, {{ucwords($admin->username)}}</p>
                                                    <p class="preview">{{$last_msg}}</p>

                                                </div>
                                            </div>
                                        </li>
                                        @endif
                                        
                                        @foreach($non_archive_highlights as $key=>$chat_highlight)
                                        @if(auth('web')->user()->id!=$chat_highlight->from_user_id)
                                        @php $to_user=$chat_highlight->sender;
                                        @endphp
                                        @endif
                                        @if(auth('web')->user()->id!=$chat_highlight->to_user_id)
                                        @php $to_user= $chat_highlight->recipient;
                                        @endphp
                                        @endif
                                        <a href="{{route('message.messages',array($project->uuid,$to_user->uuid))}}">
                                        
                                        <li @if($to_user->id == $user->id) class="contact active" @else class="contact" @endif>
                                            
                                                <div class="wrap">

                                                    @if($to_user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$to_user->profile_image)))
                                                    <img src=" {{ asset('storage/user/profile-images/'.$to_user->profile_image) }}" alt="" />
                                                    @else
                                                    <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $to_user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                    @endif
                                                    <div class="meta">
                                                        <p class="name">{{ucwords($to_user->username)}}<span>{{timeAgoConversion($chat_highlight->created_at)}}</span></p>
                                                        <p class="preview">{{$chat_highlight->message}}</p>
                                                        @if(auth()->user()->user_type==0)
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" name="non-archive-list[]" class="custom-control-input non-archive-list" id="non-archive{{$to_user->id}}" value="{{$to_user->id}}">

                                                            <label class="custom-control-label" for="non-archive{{$to_user->id}}"></label>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                           
                                        </li>
                                        </a>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <div class="content">
                                <div class="contact-profile">
                                    <p> {{ucwords($dispute->freelancer->username)}}, {{ucwords($dispute->client->username)}}, {{ucwords($admin->username)}}</p>
                                    <!-- <div class="social-media">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </div> -->
                                </div>
                                <div class="messages">
                                    <ul>
                                        @foreach($chats as $key=>$chat)

                                        <li @if($chat->from_user_id==auth('web')->user()->id && $chat->user_type == 'user') class="replies" @else class="sent" @endif >
                                            @if($chat->user_type == 'user')
                                                @if($chat->sender->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$chat->sender->profile_image)))
                                                <img src=" {{ asset('storage/user/profile-images/'.$chat->sender->profile_image) }}" alt="" />
                                                @else
                                                <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $chat->sender->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                @endif
                                                <span class="message-label">{{ucwords($chat->sender->username)}} , {{timeAgoConversion($chat->created_at)}}</span>
                                            @else
                                                @if($chat->sender->profile_image && (file_exists( public_path().'/storage/admin/profile-images/'.$chat->sender->profile_image)))
                                                <img src=" {{ asset('storage/admin/profile-images/'.$chat->admin_sender->profile_image) }}" alt="" />
                                                @else
                                                <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $chat->admin_sender->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                @endif
                                                <span class="message-label">{{ucwords($chat->admin_sender->username)}}, {{timeAgoConversion($chat->created_at)}}</span>
                                            @endif
                                            <p>{{$chat->message}}</p>
                                            @if(!empty($chat->attachment))
                                            <p> <a href="{{asset('storage/projects/comment_attachements/'.$chat->attachment)}}" class="btn white-nm-btn download" download="{{$chat->attachment}}" title="Download Attachment"><i class="fi-flaticon flaticon-download"></i></a></p>
                                            @endif
                                        </li>

                                        <!-- <li class="sep"><p>Today</p></li> -->
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="message-input">
                                    <form method="POST" action="{{route('message.PostDisputeMessage')}}" enctype="multipart/form-data" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="message-form" novalidate>
                                        @csrf
                                        <div class="wrap">
                                            <input required type="text" id="message" name="message" placeholder="{{__('Write your message...')}}" />

                                            <!-- <i class="fi-flaticon flaticon-attachments mr-2"></i> -->
                                            <div class="attachments">
                                                <input type="file" id="attachement" name="attachement"><i class="fi-flaticon flaticon-attachments mr-2"></i>
                                            </div>
                                            <input type="hidden" id="project_id" name="project_id" value="{{$project->id}}">
                                            <input type="hidden" id="dispute_id" name="dispute_id" value="{{$dispute->id}}">
                                            <input type="hidden" id="user_uuid" name="user_uuid" value="{{$user->uuid}}">
                                            <input type="hidden" id="project_id" name="project_id" value="{{$project->id}}">
                                            <button class="submit" disabled id="message-submit"><i class="fi-flaticon flaticon-send mr-2"></i></button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>


                    </div>
                    @else
                   {{__('No Messages Found on this Dispute')}}
                   @endif
                    <!-- messages-holder:end -->


                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@push('custom-scripts')
<script>
    $("#message-form").submit(function() {
        var message = document.getElementById('message').value;
        if (message == "") {
            return false;
        }
    });

    $("#message").keyup(function() {
        var message = document.getElementById('message').value;
        if (message != "") {
            document.getElementById("message-submit").disabled = false;
           
        } else {
            document.getElementById("message-submit").disabled = true;
        }
    });
    $("#archive").click(function() {

        var to_users = [];
        var project_id = document.getElementById('project_id').value;
        var inputElements = document.getElementsByClassName('non-archive-list');

        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                to_users.push(inputElements[i].value);
            }
        }
        if (to_users.length > 0) {

            $.ajax({
                type: "GET",
                url: "{{route('message.ChangeArchive')}}",
                data: {
                    'to_users': to_users,
                    'project_id': project_id,
                    'type': 1
                },
                success: function(response) {
                    //console.log(response);
                    location.reload();
                }
            });
        } else {
            alert("List is empty");

        }

    });


    $("#un-archive").click(function() {
        var to_users = [];
        var project_id = document.getElementById('project_id').value;
        var inputElements = document.getElementsByClassName('archive-list');

        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                to_users.push(inputElements[i].value);
            }
        }
        if (to_users.length > 0) {

            $.ajax({
                type: "GET",
                url: "{{route('message.ChangeArchive')}}",
                data: {
                    'to_users': to_users,
                    'project_id': project_id,
                    'type': 0
                },
                success: function(response) {
                    location.reload();

                }
            });
        } else {
            alert("List is empty");

        }

    });
</script>
@endpush