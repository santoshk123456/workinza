@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Messages')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<main class="main-wrapper">
    <div class="container inner-pg-layout project-layouts">


        <!-- main page title:start -->
        <div class="row m-0 mb-3">
            <div class="col-12">
                <h3 class="title-main">{{__('Messages')}}</h3>
            </div>
        </div>
        <!-- main page title:end -->


        <div class="row mx-0 mt-4">
        <div class="col-lg-3 left-aside left-projects-list">
        <ul class="d-block">
          <li class="active"><a href="{{route('message.publicMessages')}}">Messages</a></li>
      
        </ul>
      </div>
            <div class="col-lg-9">
                <div class="right-aside">
                    <form method="GET" action="{{route('message.publicMessages')}}" class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" id="project-form" novalidate>
                        <div class="search-bar bg-shadow search-messages">
                            <div class="row">
           
                                <div class="col-12">
                                    <div class="input-group project-search">
                                        <input type="text" id="keyword" name="keyword" minlength="3" maxlength="200" class="form-control border-right-0" placeholder="Search messages by name, project" aria-label="Search messages by name, project">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary border-left-0" type="submit"><i class="fi-flaticon flaticon-magnifying-search-lenses-tool"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- messages-holder:start -->
                    <div class="public-messages-holder bg-shadow">

                        <div id="frame">
                            <div id="sidepanel">
                                <div id="contacts" class="p-3">
                                    @if(auth()->user()->user_type==0 && count($archive_highlights)>0)
                                    <div class="archive-action" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                        <h2>Archived Chats</h2>
                                        <button class="btn btn-archived-count btn-primary">View All</button>
                                    </div>

                                    <div class="collapse" id="collapseExample">
                                        <div class="row un-acrcive-button">
                                            <div class="col-12 text-right">
                                                <button class="btn btn-primary" id="un-archive">Un Archive</button>
                                            </div>
                                        </div>

                                        <ul class="archived-list">
                                            @foreach($archive_highlights as $key=>$archive_highlight)
                                            @if(auth('web')->user()->id!=$archive_highlight->from_user_id)
                                            @php $to_user=$archive_highlight->sender;
                                            @endphp
                                            @endif
                                            @if(auth('web')->user()->id!=$archive_highlight->to_user_id)
                                            @php $to_user= $archive_highlight->recipient;
                                            @endphp
                                            @endif
                                            <a href="{{route('message.PublicMessageDetail',array($archive_highlight->project->uuid,$to_user->uuid))}}">
                                                <li class="contact">
                                                    <div class="wrap">
                                                        @if($to_user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$to_user->profile_image)))
                                                        <img src="{{ asset('storage/user/profile-images/'.$to_user->profile_image) }}" alt="" />
                                                        @else
                                                        <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $to_user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                        @endif <div class="meta">
                                                            <p class="name">{{ucwords($to_user->username)}}<span>{{timeAgoConversion($archive_highlight->created_at)}}</span></p>
                                                            <div class="grp-msg">
                                                                <p class="preview">{{$archive_highlight->message}}</p>
                                                                <p class="project">project:<span> {{$archive_highlight->project->title}}</span></p>
                                                            </div>
                                                            <div class="custom-control custom-checkbox">

                                                                <input type="checkbox" name="archive-list[]" class="custom-control-input archive-list" id="archive{{$archive_highlight->id}}" value="{{$to_user->id}}_{{$archive_highlight->project_id}}">
                                                                <label class="custom-control-label" for="archive{{$archive_highlight->id}}"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </a>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    @if(!$non_archive_highlights->isEmpty())
                                    <ul>
                                        <div class="archive-action text-right">
                                            <h2>Chats</h2>
                                            @if(auth()->user()->user_type==0)
                                            <button class="btn btn-primary" id="archive">Archive</button>
                                            @endif
                                        </div>
                                        @foreach($non_archive_highlights as $key=>$chat_highlight)
                                        @if(auth('web')->user()->id!=$chat_highlight->from_user_id)
                                        @php $to_user=$chat_highlight->sender;
                                        @endphp
                                        @endif
                                        @if(auth('web')->user()->id!=$chat_highlight->to_user_id)
                                        @php $to_user= $chat_highlight->recipient;
                                        @endphp
                                        @endif
                                        <a href="{{route('message.PublicMessageDetail',array($chat_highlight->project->uuid,$to_user->uuid))}}">
                                            <li class="contact">
                                                <div class="wrap">
                                                    @if($to_user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$to_user->profile_image)))
                                                    <img src="{{ asset('storage/user/profile-images/'.$to_user->profile_image) }}" alt="" />
                                                    @else
                                                    <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $to_user->username }}" class="mr-md-3 thumb mb-3 mb-md-0 rounded-circle img-thumbnail">
                                                    @endif
                                                    <div class="meta">
                                                        <p class="name">{{ucwords($to_user->username)}}<span>{{timeAgoConversion($chat_highlight->created_at)}}</span></p>
                                                        <div class="grp-msg">
                                                            <p class="preview">{{$chat_highlight->message}}</p>
                                                            <p class="project">{{__('Project')}}:<span> {{$chat_highlight->project->title}}</span></p>
                                                        </div>
                                                        @if(auth()->user()->user_type==0)
                                                        <div class="custom-control custom-checkbox">

                                                            <input type="checkbox" name="non-archive-list[]" class="custom-control-input non-archive-list" id="non-archive{{$chat_highlight->id}}" value="{{$to_user->id}}_{{$chat_highlight->project_id}}">

                                                            <label class="custom-control-label" for="non-archive{{$chat_highlight->id}}"></label>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </li>
                                        </a>
                                        @endforeach
                   
                                    </ul>
                                    @else
                                  
                                    <font color="black">{{__('No Chats Found')}} </font>
                                    @endif
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- messages-holder:end -->


                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@push('custom-scripts')
<script>
    $("#archive").click(function() {

        var to_users_projects = [];
        var inputElements = document.getElementsByClassName('non-archive-list');

        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                to_users_projects.push(inputElements[i].value);
            }
        }

        if (to_users_projects.length > 0) {

            $.ajax({
                type: "GET",
                url: "{{route('message.changePublicArchive')}}",
                data: {
                    'to_users_projects': to_users_projects,
                    'type': 1
                },
                success: function(response) {
                    // console.log(response);
                    location.reload();
                }
            });
        } else {
            swal({
            title: 'List is Empty',
            type: 'warning',
            text: "You want to choose the users",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
           
            closeOnConfirm: false,
           
          })

        }

    });
    $("#un-archive").click(function() {
        var to_users_projects = [];

        var inputElements = document.getElementsByClassName('archive-list');

        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                to_users_projects.push(inputElements[i].value);
            }
        }
        if (to_users_projects.length > 0) {

            $.ajax({
                type: "GET",
                url: "{{route('message.changePublicArchive')}}",
                data: {
                    'to_users_projects': to_users_projects,
                    'type': 0
                },
                success: function(response) {
                    location.reload();

                }
            });
        } else {
            swal({
            title: 'List is Empty',
            type: 'warning',
            text: "You want to choose the users",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
           
            closeOnConfirm: false,
           
          })

        }

    });
</script>
@endpush