@extends('user.layouts.user_two')
@section('title', set_page_titile('Add Rating'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')

<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout project-layouts">
      <!-- main page title:start -->
      @php
      if($type==1)
      {
      $profile_image=$rating_details[0]->rated_to->profile_image;
      $full_name=$rating_details[0]->rated_to->full_name;
      }
      else
      {
      $profile_image=$rating_details[0]->rated_by->profile_image;
      $full_name=$rating_details[0]->rated_by->full_name;
      }
      @endphp
      <div class="row">
        <div class="col-md-6 p-0">
          <h3 class="title-main mb-3">{{__('Feedback')}} - {{ucwords($full_name)}}-View</h3>
        </div>
        <div class="col-md-6 text-right">
          <button class="btn cancel-shadow-btn" type="submit" onclick="location.href='{{url()->previous()}}'"><i class="fi-flaticon flaticon-list-with-dots mr-2"></i>List all Feedbacks</button>
        </div>
      </div>
      <!-- main page title:end -->

      <div class="row mt-3">

        <div class="col-lg-12 pl-xl-0">
          <div class="right-aside">
            <!-- freelancer-list:start -->

            <div class="row">
              <div class="col-12">
                <div class="ratings-media bg-shadow">
                  <div class="media media-ratings text-center text-md-left d-block d-md-flex">

                    @if($profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$profile_image)))
                    <img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('storage/user/profile-images/'.$profile_image) }}" alt="{{ $full_name }}">
                    @else
                    <img class="mr-md-3 thumb mb-3 mb-md-0" src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $full_name }}" class="mr-md-3 thumb mb-3 mb-md-0">
                    @endif
                    <div class="media-body">
                      <div class="row">
                        <div class="col-md-7 mb-2 mb-md-0">
                          <h5 class="mt-0 name">{{ucwords($full_name)}}</h5>
                          <p class="label">{{__('project')}}: <span class="data">{{ucwords($rating_details[0]->project->title)}}</span></p>
                          <p class="label">{{__('Date')}}: <span class="data">13 Sep 2020</span></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="invoices-col bg-shadow position-relative ratings-col mb-4">
              <div class="sub-head-with-bg head-rating">
                <h2 class="title-detail m-0 p-0">{{__('Feedback')}}</h2>
              </div>



              @foreach($rating_details as $rating_detail)


              <div class="row">
                <div class="col-md-6">
                  <p class="question">{{$rating_detail->ratingQuestion->question}}</p>
                </div>
                <div class="col-md-6">

                  <div class="ratings">
                    <div class="jobseeker-rating" data-value="{{$rating_detail->rating}}">
                    </div>


                  </div>
                </div>
              </div>
              <div class="re-seperator"></div>



              @endforeach

              <div class="row">
                <div class="col-md-12">
                  <p class="label w-100 mb-2">{{__('Feedback')}}</p>
                  <div class="desc-box">
                    <p>@if(!empty($user_review->review_user)) {{$user_review->review_user}} @else NA @endif</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="invoices-col bg-shadow position-relative ratings-col">
              <div class="sub-head-with-bg head-rating">
                <h2 class="title-detail m-0 p-0">{{__('Koyexa Feedback')}}</h2>
              </div>



              @foreach($company_ratings as $company_rating)


              <div class="row">
                <div class="col-md-6">
                  <p class="question">{{$company_rating->ratingQuestion->question}}</p>
                </div>
                <div class="col-md-6">
                  <div class="ratings">
                    <div class="jobseeker-rating" data-value="{{$company_rating->rating}}"></div>
                  </div>


                </div>
              </div>
              <div class="re-seperator"></div>

              @endforeach
              <div class="row">
                <div class="col-md-12">
                  <p class="label w-100 mb-2">{{__('Feedback')}}</p>
                  <div class="desc-box">
                    <p> @if(!empty($user_review->review_company)) {{$user_review->review_company}} @else NA @endif</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</main>

@endsection
@push('custom-scripts')
<script>
  $(".jobseeker-rating").each(function(e) {
    var rateValue = $(this).attr('data-value');
    $(this).rateYo({
      rating: rateValue,
      numStars: 5,
      starWidth: "15px",
      spacing: "5px",
      readOnly: true,
    });
  });
</script>
@endpush