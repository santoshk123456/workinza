@extends('user.layouts.user_two')
@section('title', set_page_titile('Recived Rating'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')

      <div class="col-lg-9">
        <div class="right-aside">
          <div class="card-top mb-4">
            <h4>Ratings and Reviews </h4>
            <p class="mb-0">See your recieved reviews and ratings</p>
          </div>

          <div class="bg-shadow mb-3 projects-links-menu">
            <ul>
              <li><a href="{{route('rating.myRating')}}">Feedback Provided to @if($user->user_type==0) Freelancers @else Client @endif</a></li>
              <li><a href="{{route('rating.myRatingReceived')}}" class="active">Feedback Recieved from @if($user->user_type==0) Freelancers @else Client @endif</a></li>
            </ul>

          </div>

          <div class="bg-shadow p-4 basic-company-col">
            <div class="table-multi-common table-responsive">
              @if(!$rating_received->isEmpty())
              <table class="table table-striped">
                <thead>
                  <tr>
                    <!-- <th scope="col">Sl</th> -->
                    <th scope="col">Project Name</th>
                    <th scope="col">@if($user->user_type==0)Freelancer Name @else Client Name @endif</th>
                    <th scope="col">Date</th>
                    <th scope="col">@if($user->user_type==0)Rating From Freelancer @else Rating From Client @endif</th>
                    <th scope="col">Rating to Keyoxa</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
               
                  @foreach($rating_received as $rating_received)
                  <tr>

                    <td>{{ucwords($rating_received->project->title)}}</td>
                    <td>
                      @if($rating_received->rated_by->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$rating_received->rated_by->profile_image)))
                      <img class="user-thumb" src="{{ asset('storage/user/profile-images/'.$rating_received->rated_by->profile_image) }}" alt="{{ $rating_received->rated_by->full_name }}">
                      @else
                      <img class="user-thumb" src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $rating_received->rated_by->full_name }}" class="mr-md-3 thumb mb-3 mb-md-0">
                      @endif
                      {{ucwords($rating_received->rated_by->full_name)}}</td>
                    <td> {{$rating_received->created_at}}</td>
                    <td>
                      <div class="ratings">
                        <div class="rate-count">{{getReceiveRating($rating_received->project->id,1,$rating_received->rated_by_user_id)}}</div>
                        <div class="jobseeker-rating" data-value="{{getReceiveRating($rating_received->project->id,1,$rating_received->rated_by_user_id)}}"></div>
                      </div>
                    </td>
                    <td>
                      <div class="ratings">
                        <div class="rate-count">{{getReceiveRating($rating_received->project->id,2,$rating_received->rated_by_user_id)}}</div>
                        <div class="jobseeker-rating" data-value="{{getReceiveRating($rating_received->project->id,2,$rating_received->rated_by_user_id)}}"></div>
                      </div>
                    </td>
                    <td><button class="btn bg-nm-btn mr-2 py-0" type="submit" onclick="location.href='{{ route('rating.ratingDetail',array($rating_received,2))}}'"><i class="fi-flaticon flaticon-eye-1 mr-2"></i>View</button></td>
                  </tr>
                  @endforeach
                  
                </tbody>
              </table>
              @else

                  {{__('No ratings found')}} 
                  
                  @endif
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</main>


@endsection
@push('custom-scripts')
<script>
  $(".jobseeker-rating").each(function(e) {
    var rateValue = $(this).attr('data-value');
    $(this).rateYo({
      rating: rateValue,
      numStars: 5,
      starWidth: "15px",
      spacing: "5px",
      readOnly: true,
    });
  });
</script>
@endpush