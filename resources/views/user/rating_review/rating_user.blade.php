@extends('user.layouts.user_two')
@section('title', set_page_titile('Add Rating'))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
@endpush

@section('content')
<style>
@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
  </style>
<main class="main-wrapper">
  <div class="container">
    <div class="inner-pg-layout project-layouts">
      <!-- main page title:start -->
    
      <!-- main page title:end -->
      <div class="row mt-3">

        <div class="col-lg-12 pl-xl-0">
          <div class="right-aside">
            <!-- freelancer-list:start -->
            <form method="post" class="form-validate" enctype="multipart/form-data" id="add-suer-rating" action="{{route('rating.addUserRating',$project)}}">
              @csrf
              @method('PUT')
              <div class="invoices-col bg-shadow position-relative ratings-col">
                <div class="sub-head-with-bg head-rating">
                  <h2 class="title m-0 p-0">@if(Auth()->user()->user_type==0)Feedback to Freelancer @else Feedback to Client @endif</h2>
                </div>

              

                  @foreach($questions as $question)
                  <input name="questions[]" type="hidden" value="{{$question->id}}">

                  <div class="row">
                    <div class="col-md-6">
                      <p class="question">{{$question->question}}</p>
                    </div>
                    <div class="col-md-6">


                      <fieldset class="rating">
                        <input type="radio" id="field{{$question->id}}_star5" name="rating{{$question->id}}" value="5" /><label class="full" for="field{{$question->id}}_star5"></label>

                        <input type="radio" id="field{{$question->id}}_star4" name="rating{{$question->id}}" value="4" /><label class="full" for="field{{$question->id}}_star4"></label>

                        <input type="radio" id="field{{$question->id}}_star3" name="rating{{$question->id}}" value="3" /><label class="full" for="field{{$question->id}}_star3"></label>

                        <input type="radio" id="field{{$question->id}}_star2" name="rating{{$question->id}}" value="2" /><label class="full" for="field{{$question->id}}_star2"></label>

                        <input type="radio" id="field{{$question->id}}_star1" name="rating{{$question->id}}" value="1" /><label class="full" for="field{{$question->id}}_star1"></label>

                      </fieldset>


                    </div>
                  </div>
                <div class="re-seperator"></div>

                  @endforeach


                  @if(Auth::user()->user_type==1)
                  <input type="hidden" value="{{$project->user_id}}" name="rating_to_user_id">
                  @else
                  <input type="hidden" value="{{$project->contracts->freelancer_id}}" name="rating_to_user_id">
                  @endif
                  <div class="row">
                  <div class="col-md-12">

                 <p class="label"> If you have any other feedback please tell us here</p>

                  <textarea name="user_review" placeholder="{{__('Add Review')}}" id="user_review" class="form-control" rows="5" required>{{old('review')}}</textarea>
                </div>
                </div>
                <div class="row mt-4">
                <div class="col-12 mt-4 text-right">
                  <button class="btn cancel-btn mr-2" type="button">Cancel</button>
                  <button class="btn submit-btn" id="rating-submit" type="submit">{{__('Continue') }}</button>
                </div>
              </div>
              
            
            </form>
            <!-- freelancer-list:end -->
          </div>
        </div>
      </div>
    </div>
  </div>
</main>

@endsection
@push('custom-scripts')
<script>
  $("label").click(function() {
    $(this).parent().find("label").css({
      "background-color": "#D8D8D8"
    });
    $(this).css({
      "background-color": "#f77e0f"
    });
    $(this).nextAll().css({
      "background-color": "#f77e0f"
    });
  });
</script>
@endpush