@extends('user.layouts.user_one')
@section('title', set_page_titile(__('Email OTP Verification')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content')
<!--start: create account page -->

<div class="content-area">
    <div class="container">
        <div class="accounts-cols px-lg-5 email-verificaions">
            <h2>Verification</h2>
            <p class="desptn">Please enter 6 digit verification code sent to your email<br> <strong> {{$user->email}}</strong></p>
      
            <form class="form-otp mb-2" method="POST" id="otp_verification" action="{{ route('user.otpVerification', $user)}}">
                @csrf
                @method('PUT')
                <div class="form-group mb-1">
                  <input tabindex="1" name="otp_one" id="otp_one" type="number" class="form-control inputs" onKeyPress="if(this.value.length==1) return false;">
                </div>
                <div class="form-group mb-1">
                  <input tabindex="2" name="otp_two" id="otp_two" type="number" class="form-control inputs" onKeyPress="if(this.value.length==1) return false;">
                </div>
                <div class="form-group mb-1">
                  <input name="otp_three" id="otp_three" type="number" class="form-control inputs" onKeyPress="if(this.value.length==1) return false;">
                </div>
                <div class="form-group mb-1">
                  <input name="otp_four" id="otp_four" type="number" class="form-control inputs" onKeyPress="if(this.value.length==1) return false;">
                </div>
                <div class="form-group mb-1">
                  <input name="otp_five" id="otp_five" type="number" class="form-control inputs" onKeyPress="if(this.value.length==1) return false;">
                </div>
                <div class="form-group mb-1">
                  <input name="otp_six" id="otp_six" type="number" class="form-control inputs" onKeyPress="if(this.value.length==1) return false;">
                </div>
                <div class="resend-col py-4 text-center text-lg-left">
                  <p class="mb-0">Don't receive OTP yet? <a href="javascript:void(0);" class="resend-code ml-2" onclick="resendPassword()"><strong> Resend Code</strong></a>  <a href="{{route('user.accountDetails',['organization_type'=>$user->organization_type,'user_type'=>$user->user_type,'user'=>$user])}}" class="email-change">Change Email</a></p>
                </div>
                <span class="error-txt otp-error pb-4"></span>   
                <input type="hidden" id="user_email" value="{{ $user->email }}">
                <div class="btn-cols mb-2">          
                <button class="btn cancel-btn mr-2 reset" type="button">Clear</button>      
                <button class="btn submit-btn" id="otp_verify" type="submit">Verify</button>
                </div> 
          </form>
         
        </div>
    </div>
</div>

@endsection
@push('custom-scripts')


<script>
 $("#otp_verification").validate({
    onfocusout: function(element){
        // $(element).valid();
    },
    onkeyup: function(element){
        // $(element).next().text('');
   	
    },
    rules: {
        otp_one:  {
            required: true,
            minlength: 1,
            maxlength: 1,
        },
        otp_two:  {
            required: true,
            minlength: 1,
            maxlength: 1,
        },
        otp_three:  {
            required: true,
            minlength: 1,
            maxlength: 1,
        },
        otp_four:  {
            required: true,
            minlength: 1,
            maxlength: 1,
        },
        otp_five:  {
            required: true,
            minlength: 1,
            maxlength: 1,
        },
        otp_six:  {
            required: true,
            minlength: 1,
            maxlength: 1,
        },
        
       
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } else if (element.attr("name") == "otp_one" || element.attr("name") == "otp_two" || element.attr("name") == "otp_three" || element.attr("name") == "otp_four" || element.attr("name") == "otp_five" || element.attr("name") == "otp_six") {
          $(".otp-error").html(error);
        }
        else {
            error.insertAfter(element);
        }
   },

   messages: {
           'otp_one': {
                required: "Enter OTP",
            },
            'otp_two': {
                required: "Enter OTP",
            },
            'otp_three': {
                required: "Enter OTP",
            },
            'otp_four': {
                required: "Enter OTP",
            },
            'otp_five': {
                required: "Enter OTP",
            },
            'otp_six': {
                required: "Enter OTP",
            },
        }

});
</script>
<script>
function resendPassword(){
        var user_email = $('#user_email').val();
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
                type: "POST",
                url: "{{route('user.resendEmail')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'email': user_email
                     },
                success: function (data) { 
                    console.log(data);
                    if((data['email_resend']) == 1){
                        showToast('Success', "OTP has been sent to your registered email", 'success');
                        return false;
                    }else{
                        showToast('Error', "OTP can not be sent to your registered email", 'error');
                        return false;
                    }
                }
        });
};
</script>
<script>
$(".reset").click(function() {
    $(this).closest('form').find("input[type=number]").val("");
});
</script>
<script>
      $('.inputs').on("keyup", function(e) {
        
        var $input = $(this);
      if ($input.val().length == 0 && e.which == 8) {
       
        $(this).prev("input[type='number']").focus();
      }
      
  });
    $('#otp_one').focus();
$('#otp_one').keyup(function(e){
    if($(this).val().length == 1)
        $('#otp_two').focus();
});
$('#otp_two').keyup(function(e){
    if($(this).val().length == 1)
    $('#otp_three').focus();
    if ($(this).val().length == 0 && e.which == 8) {
       
        $('#otp_one').focus();
    }
});
$('#otp_three').keyup(function(e){
    if($(this).val().length == 1)
    $('#otp_four').focus();
    if ($(this).val().length == 0 && e.which == 8) {
       
       $('#otp_two').focus();
   }
});
$('#otp_four').keyup(function(e){
    if($(this).val().length == 1)
    $('#otp_five').focus();
    if ($(this).val().length == 0 && e.which == 8) {
       
       $('#otp_three').focus();
   }
});
$('#otp_five').keyup(function(e){
    if($(this).val().length == 1)
    $('#otp_six').focus();
    if ($(this).val().length == 0 && e.which == 8) {
       
       $('#otp_four').focus();
   }
});
$('#otp_six').keyup(function(e){
   
    if ($(this).val().length == 0 && e.which == 8) {
       
       $('#otp_five').focus();
   }
});

</script>
@endpush
