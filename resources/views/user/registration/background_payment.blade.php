
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Background Verification Payment')))

@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >

@endpush

@section('content')
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
        <div class="col-lg-3 left-aside">
            <h3>My Profile</h3>
            <ul class="d-block">
   
                <li class="active"><a href="{{route('user.viewContactInformation')}}"><i class="fi-flaticon flaticon-contact"></i>Contact Information</a></li>
                <li class="active"><a href="{{route('user.viewSecurityQuestions')}}"><i class="fi-flaticon flaticon-shield"></i>Security Questions</a></li>
                <li class="active"><a href="#"><i class="fi-flaticon flaticon-clipboard"></i>Background Verification</a></li>
                <li class="@if(auth()->user()->step >= 5) active @else disabled @endif"><a @if(auth()->user()->step >= 5) href="{{route('membership_plan.membershipPlans')}}" @else href="#" @endif"><i class="fi-flaticon flaticon-premium"></i> Membership Plans</a></li>
                <li class="@if(auth()->user()->step >= 6) active @else disabled @endif"><a @if(auth()->user()->step >= 6) href="{{route('account.viewPublicProfile')}}" @else href="#" @endif"><i class="fi-flaticon flaticon-user-shape"></i>Public Profile Details</a></li>
                <li class="active"><a href="{{route('rating.myRating')}}"><i class="fi-flaticon flaticon-user-shape"></i>Rating and Review</a></li>
            </ul>
        </div>

        <div class="col-lg-9">
            <div class="right-aside">
                <div class="card-top mb-4">
                    <h4>{{__("Transfer Payment")}}</h4>
                    <p class="mb-0">{{__("Payment for the background verification")}}</p>  
                </div>           
                <div class="card-content">
                    @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif
                    <form role="form" action="{{ route('user.storeOnePayment') }}" method="post" class="validation" data-cc-on-file="false"
                    data-stripe-publishable-key="{{$payment_keys->authentication_key}}"
                    id="payment-form">
                        @csrf
                        <div class="form-row border-bottom questionborder pb-3 mb-3">
                            <div class="col-md-6 mb-lg-2">
                                <div class="form-group mb-0 required">
                                    <label  for="credit_card" class="control-label">{{ __('Credit Card Number') }}</label> 
                                    <input autocomplete='off' class='form-control card-num' size='20' type='text' placeholder="Credit Card Number">                        
                            
                                </div>
                            </div>
                        </div>
                        <div class="form-row border-bottom questionborder pb-3 mb-3">
                            <div class="col-md-6 mb-lg-2">
                                <div class="form-group mb-0 required">
                                    <label  for="credit_card" class="control-label">{{ __('Credit Card Holder Name') }}</label> 
                                    <input class='form-control card-name' size='50' type='text' placeholder="Credit Card Holder Name">                        
                            
                                </div>
                            </div>
                        </div>
                        <div class="form-row border-bottom questionborder pb-3 mb-3">
                            <div class="col-md-6 mb-lg-2">
                                <div class="form-group mb-0 required">
                                    <label  for="credit_card" class="control-label">{{ __('CVV Code') }}</label> 
                                    <input autocomplete='off' class='form-control card-cvc' placeholder='e.g 415' size='4' type='text'>                 
                            
                                </div>
                            </div>
                        </div>
                        <div class="form-row border-bottom questionborder pb-3 mb-3">
                            <div class="col-md-3 mb-lg-2">
                                <div class="form-group mb-0 required">
                                    <label  for="credit_card" class="control-label">{{ __('Expiry Month') }}</label> 
                                    <input class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text'>                       
                            
                                </div>
                            </div>
                            <div class="col-md-3 mb-lg-2">
                                <div class="form-group mb-0 required">
                                    <label  for="credit_card" class="control-label">{{ __('Expiry Year') }}</label> 
                                    <input class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>                        
                                   
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                        <div class="col-md-6 hide error mb-lg-2">
                   
                            <span class="error-txt alert" ><label id="agreeBackground-error" class="error" for="agreeBackground"></label></span>
                        
                        </div>
                        </div>
                       
                        <div class="btn-cols text-right mb-2">
                            {{-- <button class="btn cancel-btn mr-2" onclick="location.href='{{route('user.profileAcknowledgement')}}'"   type="submit">Cancel</button> --}}
                            <button class="btn submit-btn" type="submit">Pay Now</button>
                          </div> 
                    </form>
                </div>
                
            </div>
        </div>
    </div>
  </div>
</main>






@endsection
@push('custom-scripts')

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
  <script type="text/javascript">
    $(function() {
      
    var $form         = $(".validation");
    $('form.validation').bind('submit', function(e) {
       
          var $form         = $(".validation"),
          inputVal = ['input[type=email]', 'input[type=password]',
                           'input[type=text]', 'input[type=file]',
                           'textarea'].join(', '),
          $inputs       = $form.find('.required').find(inputVal),
          $errorStatus = $form.find('div.error'),
          valid         = true;
          $errorStatus.addClass('hide');
   
          $('.has-error').removeClass('has-error');
      $inputs.each(function(i, el) {
        var $input = $(el);
        console.log($input.val());
        if ($input.val() === '') {
          $input.parent().addClass('has-error');
          $errorStatus.removeClass('hide');
          e.preventDefault();
        }
      });
    
      if (!$form.data('cc-on-file')) {
        e.preventDefault();
        Stripe.setPublishableKey($form.data('stripe-publishable-key'));
        Stripe.createToken({
         
          number: $('.card-num').val(),
          cvc: $('.card-cvc').val(),
          exp_month: $('.card-expiry-month').val(),
          exp_year: $('.card-expiry-year').val()
        }, stripeHandleResponse);
      }
    
    });
    
    function stripeHandleResponse(status, response) {
          if (response.error) {
              $('.error')
                  .removeClass('hide')
                  .find('.alert')
                  .text(response.error.message);
          } else {
              var token = response['id'];
              $form.find('input[type=text]').empty();
              $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
              $form.get(0).submit();
          }
      }
    
  });


</script>

@endpush
