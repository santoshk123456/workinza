@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Contact Information')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content')
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')
        <div class="col-lg-9">
            <div class="right-aside">
            <div class="card-top mb-4">
              <h4>Contact Information</h4>
              <p class="mb-0">
              These Contact Details are verified by the admin and if you are willing to edit the contact details, please contact the support team to change this information as the information is linked to your background verification.
              </p>  
            </div>
            
            <div class="card-content">
              <form method="POST" id="contact_details_form" action="{{route('user.updateContactInformation')}}"> 
              @csrf
              <input type="hidden" value="{{$user->full_number}}" name="phone">
              <input type="hidden" value="{{$user->full_name}}" name="name">
              <input type="hidden" value="{{$user->email}}" name="email">
                <div class="form-row">
                  
                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="description" class="control-label">Content For Update Request</label>
                      <textarea class="form-control note-codable" id="description" name="description" role="textbox" placeholder="{{__('Enter the content for contact information update request')}}">{{old('description')}}</textarea>
                      <span id="username_error"  class="error-txt description-error"></span>
                    </div>
                    
                  </div>

                  </div> 
                         
                <div class="btn-cols text-right mb-2">
                  <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ route('user.redirectBack') }}'">Cancel</button>
                  <button class="btn submit-btn" id="otp-verify-btn" type="submit">Send</button>
                </div>                
              </form>   
            </div>
        </div>
    </div>
    </div>
  </div>
</main>

<!--end: contact information page -->

@endsection
@push('custom-scripts')
<script>
 $("#contact_details_form").validate({
  onfocusout: function(element){
         $(element).valid();
   	
    },
    onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        
    rules: {
        description: {
            required: true,
            // maxlength : 6,
        }
        
        
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } else if (element.attr("id") == "description" ) {
          $(".description-error").html(error);
        }
        else {
           error.insertAfter(element);
        }
   },

   messages: {
           'description': {
                required: "Description is required.",
            }
        }

});
</script>
<script>
$(".reset").click(function() {
    $(this).closest('form').find("input[type=text], textarea,select").val("");
});
</script>
@endpush
