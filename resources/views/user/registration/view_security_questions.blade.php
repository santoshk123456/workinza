
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Security Questions')))

@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')
        <div class="col-lg-9">
              <div class="right-aside">
              <div class="card-top mb-4">
                <h4>Security Questions</h4>
                <p class="mb-0">Your have answered the following security questions</p>  
              </div>           
              <div class="card-content">
              <form method="post" id="security-questions-form" action="{{route('user.storeSecurityQuestions')}}">
              @csrf  
              @if(!$security_questions->isEmpty())
              @foreach($security_questions as $key=>$question)


                    <div class="form-row border-bottom questionborder pb-3 mb-3">
                        <div class="col-md-12 mb-lg-2">
                        <div class="form-group mb-0">
                            <label  for="quesans_{{$key}}" class="control-label">{{ $question->securityQuestions->question }}</label> 
                            <label  for="quesans_{{$key}}" class="control-label">{{ $question->answer }}</label> 
                            
                    
                          </div>
                        </div>
                    </div>


              @endforeach
              @else
              <div class="form-row border-bottom">
                        <div class="col-md-6 mb-lg-2">
                        <div class="form-group required">
                        <div class="center">
                  <i><p> {{__('No security questions added.')}} </p></i>
                  </div>
                       
                          </div>
                        </div>
                    </div>
              @endif
         
                  <div class="btn-cols text-right mb-2">
                    <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ route('user.redirectBack') }}'">Back</button>
                    @if(!$security_questions->isEmpty())
                      <button class="btn submit-btn" id="otp-verify-btn" type="button" onclick="location.href='{{ route('user.editSecurityQuestions') }}'">Edit</button>
                    @else
                      <button class="btn submit-btn" id="otp-verify-btn" type="button" onclick="location.href='{{ route('user.securityQuestions') }}'">Add</button>
                    @endif
                  </div>                
                </form>   
              </div>
          </div>
      </div>






    </div>
  </div>
</main>






@endsection
@push('custom-scripts')

<script>
$(document).ready(function () {

$('#security-questions-form').validate({
    // options, etc.
});

$('input[id^="quesans"]').each(function () {
    $(this).rules('add', {
        required: true,
        // another rule, etc.
    });
});

});


</script>

@endpush
