
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Dashboard')))

@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >

@endpush

@push('custom-styles')
<style>
#container {
  display: flex;                  /* establish flex container */
  flex-direction: row;            /* default value; can be omitted */
  flex-wrap: nowrap;              /* default value; can be omitted */
  justify-content: space-between; /* switched from default (flex-start, see below) */
}



</style>
@endpush
@section('content')

       

        <!-- Header:end -->
<!--start: contact information page -->

<!-- <main class="main-wrapper">
    <div class="container inner-pg-layout"> -->
      <!-- <div class="row mx-0">
          <div class="col-lg-12 left-aside">         -->
<!--            
          </div>
          <div class="col-lg-9"> -->
            


      <!-- Header:end -->
      <main class="dashboard">
<!-- top freelancer section :start -->
<section class="top-freelancer dashborad-wrapper">
    <div class="container">  
        <div class="section-title">
            <h2>Dashboard</h2>           
        </div> 
        <div class="icon-cards">
            <div class="row">
         

            @if($user->user_type == 0)   
             <div class="col-md-3">
                <a href="{{route('project.listOpenProjects')}}">
                        <div class="item card-items">    
                            <div class="row mx-0">
                                <div class="col-9 text-left px-0"><h5>Open Projects</h5></div>
                                <div class="col-3 p-0">
                                    <div class="icons-col">
                                        <i class='fas fa-file'></i>                                  
                                    </div>                                    
                                </div>
                            </div>                       
                            <p class="counter">{{ $open_projects}}</p>                           
                        </div>
                     </a>
                </div>   
               

              
                <div class="col-md-3">
                    <a href="{{route('project.listInProgressProjects')}}">
                        <div class="item card-items">    
                            <div class="row mx-0">
                                <div class="col-9 text-left px-0"><h5>Inporgress Projects</h5></div>
                                <div class="col-3 p-0">
                                    <div class="icons-col">
                                        <i class='fas fa-file-signature'></i>                                  
                                    </div>                                    
                                </div>
                            </div>                       
                            <p class="counter">{{ $inprogress_projects }}</p>                           
                        </div>
                     </a>
                </div> 
               

               
                <div class="col-md-3">
                    <a href="{{route('project.listCompletedProjects')}}">
                        <div class="item card-items">    
                            <div class="row mx-0">
                                <div class="col-9 text-left px-0"><h5>Completed Projects </h5></div>
                                <div class="col-3 p-0">
                                    <div class="icons-col">
                                        <i class='fas fa-clipboard-check'></i>                                  
                                    </div>                                    
                                </div>
                            </div>                       
                            <p class="counter">{{ $completed_projects }}</p>                           
                        </div>
                     </a>
                </div> 
               
                @else


                
                <div class="col-md-3">
                    <a href="{{route('project.listProposals')}}"> 
                        <div class="item card-items">    
                            <div class="row mx-0">
                                <div class="col-9 text-left px-0"><h5>My Proposals</h5></div>
                                <div class="col-3 p-0">
                                    <div class="icons-col">
                                        <i class='fas fa-business-time'></i>                                  
                                    </div>                                    
                                </div>
                            </div>                       
                            <p class="counter">{{ $proposals_count}}</p>                           
                        </div>
                     </a>
                </div>   
              

                
                <div class="col-md-3">
                    <a href="{{route('project.freelancerlistInProgressProjects')}}">
                        <div class="item card-items">    
                            <div class="row mx-0">
                                <div class="col-9 text-left px-0"><h5>Assigned / Inporgress Projects</h5></div>
                                <div class="col-3 p-0">
                                    <div class="icons-col">
                                        <i class='fas fa-file-signature'></i>                                  
                                    </div>                                    
                                </div>
                            </div>                       
                            <p class="counter">{{ $inprogress_projects }}</p>                           
                        </div>
                     </a>
                </div> 


                
                <div class="col-md-3">
                    <a href="{{route('project.listCompletedProjects')}}">
                        <div class="item card-items">    
                            <div class="row mx-0">
                                <div class="col-9 text-left px-0"><h5> Completed Projects </h5></div>
                                <div class="col-3 p-0">
                                    <div class="icons-col">
                                        <i class='fas fa-clipboard-check'></i>                                  
                                    </div>                                    
                                </div>
                            </div>                       
                            <p class="counter">{{ $completed_projects }}</p>                           
                        </div>
                     </a>
                </div> 
               

               
                <div class="col-md-3">
                    <a href="{{route('project.listInvitations')}}">
                        <div class="item card-items">    
                            <div class="row mx-0">
                                <div class="col-9 text-left px-0"><h5>  Invitations </h5></div>
                                <div class="col-3 p-0">
                                    <div class="icons-col">
                                        <i class='fas fa-envelope-open'></i>                                  
                                    </div>                                    
                                </div>
                            </div>                       
                            <p class="counter">{{ $invitations }}</p>                           
                        </div>
                     </a>
                </div> 
             

               
                <div class="col-md-3">
                    <a href="{{ route('finance.myWallet') }}">
                        <div class="item card-items">    
                            <div class="row mx-0">
                                <div class="col-9 text-left px-0"><h5>Wallet Balance</h5></div>
                                <div class="col-3 p-0">
                                    <div class="icons-col">
                                        <i class="fa fa-wallet"></i>                                     
                                    </div>                                    
                                </div>
                            </div>                       
                            <p class="counter">{{   formatAmount($user->wallet_balance) }} </p>                           
                        </div>
                     </a>
                </div> 
               
@endif


            </div>
        </div>
    </div>
</section>
<!-- top freelancer section :end -->
</main>


<!--           
          </div>
      </div> -->
    <!-- </div>
  </main> -->
  
  <!--end: contact information page -->
  
@endsection
@push('custom-scripts')
<script>
$(document).ready(function(){
  var member_plan_type = $("#member_plan_type").val();
  if(member_plan_type == 1){
    $('input:radio[name="plan_type"]').filter('[value="0"]').attr('checked', true);
  }
});
</script>

<script>
$('#year_billing').click(function () {
        $('.monthly-billing').addClass('d-none');
        $('.month-details').addClass('d-none');

        $('.yearly-billing').removeClass('d-none');
        $('.year-details').removeClass('d-none');
});

$('#month_billing').click(function () {
        $('.yearly-billing').addClass('d-none');
        $('.year-details').addClass('d-none');

        $('.monthly-billing').removeClass('d-none');
        $('.month-details').removeClass('d-none');
});
</script>
@endpush
