@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Edit Public Profile')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content')

<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')
        <div class="col-lg-9">
            <div class="right-aside">
            <div class="card-top mb-4">
              <h4>Public Profile Details</h4>
              <p class="mb-0">Update Your Public Profile Info</p>  
            </div>     
            <form method="post" enctype="multipart/form-data" id="public-profile-form" class="form-validate" action="{{route('account.updatePublicProfile')}}">
           @csrf
            <div class="bg-shadow basic-detail-col mb-3">
              <h3 class="sub-headtitle">Basic Details</h3>
             
              <div class="title-heading">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group custom-file-choose required">
                      <label for="photo/logo" class="control-label">{{ __('Photo/Logo') }}</label>
                      <div class="view-upload">
                        <!-- <div class="file-upload-message">
                          <i class="fi-flaticon flaticon-cloud-backup-up-arrow"></i>
                          <p class="mb-0">{{ __('Drop your image here or Browse') }}</p>
                        </div> -->
                        @php 
                        $url = asset('storage/user/profile-images'.$user->profile_image);
                        @endphp
                        @if(!empty($user->profile_image))
                        <input type="file" id="profile_image" class="form-control-file dropify"  data-default-file="{{ $url }}" name="profile_image" accept = 'image/jpeg , image/jpg, image/gif, image/png'/>
                        <input type="hidden" name="remove_title">
                        @else
                        <input type="file" class="form-control-file dropify" id="profile_image"  accept = 'image/jpeg , image/jpg, image/gif, image/png'  data-default-file="" name="profile_image"/>
                        @endif
                        <span id="profile-error"  class="error-txt profile-error"></span>
                      </div>                      
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="profile-headline" class="control-label">{{ __('Profile Headline') }}</label>
               
                      <input value="{{old('profile_headline',$user->userDetail->profile_headline)}}" type="text" aria-describedby="profile_headline" name="profile_headline" class="form-control" id="profile_headline" minlength="3" maxlength="250" placeholder="{{__('Enter Profile Headline')}}" required>
                      <span id="profile-headline-error"  class="error-txt profile-headline-error"></span>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group required">
                      <label for="about_me" class="control-label">{{ __('About Me') }}</label>
                      <textarea id="about_me" class="form-control" rows="3" name="about_me" placeholder="{{__('Enter details')}}">{{old('about_me',$user->userDetail->about_me)}}</textarea>
                      <span id="about-me-error" class="error-txt about-me-error"></span>
                    </div>
                  </div>
                  <div class="col-md-12">
                  <div class="form-group">
                  <label for="details">{{ __('Video url') }}</label>
                  <table class="table video-link-col">
                <tbody>
                @php
                    $videos = json_decode($user->userDetail->video_urls);
                    
                @endphp
                    @if(!empty(old('video_links')))
                        @foreach (old('video_links') as $old_video)
                            <tr>
                                <td scope="row"><input  type="text" name="video_links[]" class="form-control" value="{{$old_video}}" id="video_links" placeholder="Video Link"></td>
                                <!-- <td><button class="btn btn-success add_more" type="button">+</button><button type="button" class="btn btn-danger ml-2 remove_row">-</button></td> -->
                            <td class="border-0 ">
                                            <button class="btn btn-success add_more mr-2" type="button">Add Milestone</button><button type="button" class="btn ml-2 remove_row white-nm-btn white-nm-btn border">Remove Milestone</button>
                                        </td>
                            </tr>
                        @endforeach
                    @elseif(!empty($videos))
                        @foreach ($videos as $video)
                            <tr>
                                <td scope="row"><input  type="text" name="video_links[]" value="{{$video}}" class="form-control" id="video_links" placeholder="Video Link"></td>
                                <td class="remove-buttons border-0 ">
                                @if($loop->last)<button class="btn bg-nm-btn add_more mr-2" type="button">Add Video</button>@endif
                                  <button type="button" class="btn remove_row white-nm-btn border">Remove Video</button>
                                </td>
                            </tr>
                        @endforeach
                       
                    @else
                        <tr>
                            <td scope="row"><input  type="text" name="video_links[]" class="form-control" id="video_links" placeholder="Video Link"></td>
                            <!-- <td><button class="btn btn-success add_more" type="button">+</button><button type="button" class="btn btn-danger ml-2 remove_row">-</button></td> -->
                            <td class="remove-buttons border-0 ">
                            <button class="btn bg-nm-btn add_more mr-2" type="button">Add Video</button>
                                            <button type="button" class="btn remove_row white-nm-btn border">Remove Video</button>
                                        </td>
                        </tr>
                    @endif
                </tbody>
                  </table>
                </div> 

                  
                </div>               
            
             </div>
            </div>
            </div>
            <div class="bg-shadow experience-detail-col mb-3">
              <h3 class="sub-headtitle">{{ __('Experience') }}</h3>
              <!-- <form> -->
              <div class="title-heading">
                <div class="row">
              
                  <div class="col-md-4">
                    <div class="form-group required">
                      <label for="industry" class="control-label">{{ __('Industry') }}</label>
                      <select name="industry_ids[]" id="industry_ids" multiple="multiple" class="form-control" required>
                <option value=""></option>
                @foreach ($industries as $industry)
                    <option value="{{$industry->id}}" {{in_array($industry->id,old('industry_ids',$slctd_industries)) ? 'selected' : ''}}>{{$industry->name}}</option>
                @endforeach
            </select>
            <span id="industries-error"  class="error-txt industries-error"></span>
                    </div>
                  </div>
                  @php
        if(!empty(old('categories_ids'))){
            $old_category = old('categories_ids');
            $old_sub_categories = App\SubCategory::whereHas('categories',function($q) use($old_category){
                        $q->whereIn('id',$old_category);
                    })->where(['active'=>1])->pluck('name','id');
        }else{
            $old_sub_categories = [];
        }
        @endphp
                  <div class="col-md-4">
                    <div class="form-group required">
                      <label for="industry" class="control-label">{{ __('Category') }}</label>
                      <select name="categories_ids[]" id="category_ids" class="form-control" multiple="multiple" required>
                <option value=""></option>
                @if(!empty($old_categories))
                    @foreach ($old_categories as $key=>$category)
                        <option value="{{$key}}" {{!empty(old('categories_ids')) ? in_array($key,old('categories_ids')) ? 'selected' : '' : ''}}>{{$category}}</option>
                    @endforeach
                @else
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}" {{in_array($category->id,$slctd_categories) ? 'selected' : ''}}>{{$category->name}}</option>
                    @endforeach
                @endif
            </select>
            <span id="categories-error"  class="error-txt categories-error"></span>
                    
                    </div>
                  </div>      
                  <div class="col-md-4">
                    <div class="form-group required" >
                      <label for="industry" class="control-label">{{ __('Sub Category') }}</label>
                      <select name="sub_categories_ids[]" id="sub_category_ids" class="form-control" multiple="multiple" required>
                <option value=""></option>
                @if(!empty($old_sub_categories))
                    @foreach ($old_sub_categories as $key=>$sub_category)
                        <option value="{{$key}}" {{!empty(old('sub_categories_ids')) ? in_array($key,old('sub_categories_ids')) ? 'selected' : '' : ''}}>{{$sub_category}}</option>
                    @endforeach
                @else
                    @foreach ($sub_categories as $sub_category)
                        <option value="{{$sub_category->id}}" {{in_array($sub_category->id,$slctd_sub_categories) ? 'selected' : ''}}>{{$sub_category->name}}</option>
                    @endforeach
                @endif
            </select>
            <span id="sub_categories-error"  class="error-txt sub_categories-error"></span>
                    
                    </div>
                  </div>   
                  @php
                                if(!empty(old('sub_categories_ids'))){
                                $old_sub_category = old('sub_categories_ids');
                                $old_skills = App\Skill::whereHas('subCategories',function($q) use($old_sub_category){
                                $q->whereIn('id',$old_sub_category);
                                })->where(['active'=>1])->pluck('name','id');
                                }else{
                                $old_skills = [];
                                }
                                @endphp
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label for="title" class="control-label">{{__('Skills') }}</label>
                                        <select name="skills[]" id="skill_ids" class="form-control" multiple="multiple" required>
                                            @if(!empty($old_skills))
                                            @foreach ($old_skills as $key=>$skill)
                                            <option value="{{$key}}" {{!empty(old('skills')) ? in_array($key,old('skills')) ? 'selected' : '' : ''}}>{{$skill}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($skills as $skill)
                                            <option value="{{$skill->id}}" {{in_array($skill->id,$slctd_skills) ? 'selected' : ''}}>{{$skill->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span id="skills-error" class="error-txt skills-error"></span>
                                    </div>
                                </div>                    
                </div>  
                <div class="row">
                  @if($user->organization_type == 1 && $user->user_type == 1)
                <div class="col-md-6">
                    <div class="form-group">
                      <label for="profile-headline">{{ __('Years of Experience') }}</label>
                      <input type="text" value="{{old('years_of_experience',$user->userDetail->years_of_experience)}}" class="form-control" id="years_of_experience" name="years_of_experience" placeholder="{{ __('Years of Experience') }}">
                      <span id="experience-error" class="error-txt experience-error"></span>
                    </div>
                  </div>
                  @endif
                  <!-- <div class="col-md-6">
                    <div class="form-group required">
                      <label for="profile-headline"  class="control-label">{{ __('Portfolio of Projects/Works') }}</label>
                      <input type="text" name="website_url" value="{{old('website_url',$user->userDetail->website_url)}}" class="form-control" id="profile-headline" aria-describedby="profile-headline" placeholder="Enter profile headline">
                    </div>
                  </div> -->
                </div>    
                <div class="portfolio-projects">
                  <label for="industry">{{__('Portfolio of Projects / Works')}}</label>
                  <table class="table">
                    <tbody>

                      @if(!empty(old('portfolio_images')))
                      @foreach(old('portfolio_images') as $key=>$portfolio)
                      <input type="hidden" name="portfolio_id[]" value="{{old('portfolio_id')[$key]}}">
                      <tr>
                        <td scope="row" class="border-0 p-0">
                          <div class="row">
                          <div class="col-md-6">
                              <div class="form-group required">
                                <label for="portfolio_title" class="control-label">{{ __('Portfolio Title') }}</label>

                                <input type="text" name="portfolio_titles[]" class="form-control" placeholder="{{__('Portfolio Title')}}" required>

                              </div>
                            </div>
                            <div class="col-md-5">
                              <div class="form-group custom-file-choose">
                                <label for="photo/logo">{{ __('Project Photo') }}</label>
                                <div class="view-upload">

                                  <input type="file" class="form-control-file dropify portfolio_images" id="portfolio_image" accept='image/jpeg , image/jpg, image/gif, image/png' data-default-file="" name="portfolio_images[]" />

                                </div>
                              </div>
                            </div>
                            <div class="col-md-7">
                              <div class="form-group required">
                                <label for="description" class="control-label">{{__('Project Description')}}</label>
                                <textarea class="form-control portfolio_description" id="portfolio_description" rows="5" placeholder="{{__('Project description')}}" name="portfolio_descriptions[]" required></textarea>
                              </div>
                            </div>

                          </div>
                        </td>
                        <td class="border-0 text-right">
                          <button class="btn btn-success add_portfolio mr-2" type="button"><i class="fi-flaticon flaticon-plus mr-2"></i>{{__('Add Portfolio')}}</button><button type="button" class="btn ml-2 remove_portfolio white-nm-btn white-nm-btn border">{{__('Remove Portfolio')}}</button>
                        </td>
                      </tr>

                      @endforeach
                      @else


                      <tr class="duplicateable-content model" style="display:none">
                      <input type="hidden" class="portfolio_ids">
                        <td scope="row" class="border-0 p-0">
                          <div class="row">
                          <div class="col-md-12">
                              <div class="form-group required">
                                <label for="portfolio_title" class="control-label">{{ __('Portfolio Title') }}</label>

                                <input type="text" class="form-control portfolio_title" placeholder="{{__('Portfolio Title')}}" required>

                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group custom-file-choose">
                                <label for="photo/logo">{{ __('Project Photo') }}</label>
                                <div class="view-upload">

                                  <input type="file" class="form-control-file portfolio_images" accept='image/jpeg , image/jpg, image/gif, image/png' />

                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group required">
                                <label for="description" class="control-label">{{__('Description of Project')}}</label>
                                <textarea class="form-control portfolio_description" rows="5" placeholder="{{__('Enter project description')}}" required></textarea>
                              </div>
                            </div>

                          </div>
                        </td>
                        <td class="remove-buttons border-0 text-right">
                          <button class="btn bg-nm-btn add_portfolio mr-2" type="button"><i class="fi-flaticon flaticon-plus mr-2"></i>{{__('Add Portfolio')}}</button>
                          <button type="button" class="btn remove_portfolio white-nm-btn border">{{__('Remove Portfolio')}}</button>
                        </td>

                      </tr>
                      @if(count($user->portfolio)>0)
                      @foreach ($user->portfolio as $portfolio)
                     
                      <tr class="duplicateable-content">
                      <input type="hidden" name="portfolio_id[]" value="{{$portfolio->id}}">
                        <td scope="row" class="border-0 p-0">
                          <div class="row">
                          <div class="col-md-12">
                              <div class="form-group required">
                                <label for="portfolio_title" class="control-label">{{ __('Portfolio Title') }}</label>

                                <input value="{{$portfolio->portfolio_title}}" type="text" name="portfolio_titles[]" class="form-control portfolio_title" placeholder="{{__('Portfolio Title')}}" required>

                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group custom-file-choose">
                                <label for="photo/logo">{{ __('Project Photo') }}</label>
                                <div class="view-upload">
                                @php 
                        $url = asset('storage/user/portfolio-images/'.$portfolio->file_name);
                        @endphp
                        @if(!empty($portfolio->file_name))
                                  <input type="file" class="form-control-file dropify portfolio_images" id="portfolio_image" accept='image/jpeg , image/jpg, image/gif, image/png' data-default-file="{{ $url }}" name="portfolio_images[]" />
@else
<input type="file" class="form-control-file dropify portfolio_images" id="portfolio_image" accept='image/jpeg , image/jpg, image/gif, image/png' data-default-file="" name="portfolio_images[]" />
@endif
                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group required">
                                <label for="description" class="control-label">{{__('Description of Project')}}</label>
                                <textarea class="form-control portfolio_description" id="portfolio_description" rows="5" placeholder="{{__('Enter project description')}}" required name="portfolio_descriptions[]">{{$portfolio->portfolio_description}}</textarea>
                              </div>
                            </div>

                          </div>
                        </td>
                        <td class="remove-buttons border-0 text-right">
                          <button class="btn bg-nm-btn add_portfolio mr-2" type="button"><i class="fi-flaticon flaticon-plus mr-2"></i>{{__('Add Portfolio')}}</button>
                          <button type="button" class="btn remove_portfolio white-nm-btn border">{{__('Remove Portfolio')}}</button>
                        </td>

                      </tr>
@endforeach

@else

<tr class="duplicateable-content">
                        <td scope="row" class="border-0 p-0">
                          <div class="row">

                          <div class="col-md-12">
                              <div class="form-group">
                                <label for="portfolio_title" class="control-label">{{ __('Portfolio Title') }}</label>

                                <input value="{{old('portfolio_title')}}" type="text" name="portfolio_titles[]" class="form-control portfolio_title" placeholder="{{__('Portfolio Title')}}">

                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group custom-file-choose">
                                <label for="photo/logo">{{ __('Project Photo') }}</label>
                                <div class="view-upload">
                             
                                  <input type="file" class="form-control-file dropify portfolio_images" id="portfolio_image" accept='image/jpeg , image/jpg, image/gif, image/png' data-default-file="" name="portfolio_images[]" />

                                </div>
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="description" class="control-label">{{__('Description of Project')}}</label>
                                <textarea class="form-control portfolio_description" id="portfolio_description" rows="5" placeholder="{{__('Enter project description')}}" name="portfolio_descriptions[]"></textarea>
                              </div>
                            </div>

                          </div>
                        </td>
                        <td class="remove-buttons border-0 text-right">
                          <button class="btn bg-nm-btn add_portfolio mr-2" type="button">{{__('Add Portfolio')}}</button>
                          <button type="button" class="btn remove_portfolio white-nm-btn border">{{__('Remove Portfolio')}}</button>
                        </td>

                      </tr>

                      @endif
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- </form>            -->
            </div>
            <div class="bg-shadow basic-company-col mb-3">
            @if($user->organization_type == 0)
              <h3 class="sub-headtitle">{{ __('Company Information') }}</h3>
              @else
              <h3 class="sub-headtitle">{{ __('Background Information') }}</h3>
              @endif
              <!-- <form> -->
              <div class="title-heading">
                <div class="row">
                @if($user->organization_type == 0)
                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="website-urls" class="control-label">{{ __('Company Tagline') }}</label>
                <input value="{{old('company_tagline',$user->userDetail->company_tagline)}}" type="text" aria-describedby="company_tagline" name="company_tagline" class="form-control" id="company_tagline" minlength="3" maxlength="300" placeholder="{{__('Enter Company Tagline')}}">
                <span id="company-tagline-error"  class="error-txt company-tagline-error"></span> 
              </div>
                  </div>
                  @endif
                  <div class="col-md-6">
                    <div class="form-group">
                    @if($user->organization_type == 0)
                      <label for="company_history" class="control-label">{{ __('Company / Individual Background') }}</label>

                      <textarea class="form-control note-codable" id="company_history" name="company_history" role="textbox" placeholder="{{__('Enter Company History')}}">{{old('company_history',$user->userDetail->user_history)}}</textarea>
                      @else
                      <label for="individual_history">{{ __('Background History') }}</label>

                      <textarea class="form-control note-codable" id="individual_history" name="individual_history" role="textbox" placeholder="{{__('Enter Background History')}}">{{old('company_history',$user->userDetail->user_history)}}</textarea>
                      @endif
                
                      <span id="company-history-error"  class="error-txt company-history-error"></span> 
                    </div>
                  </div>
                  @if($user->organization_type == 0) 
                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="established_date" class="control-label">{{ __('Established Date') }}</label>
                <input value="{{old('established_date',date('d-m-Y',strtotime($user->userDetail->established_since)))}}" type="text" aria-describedby="established_date" name="established_date" class="form-control" id="established_date"  placeholder="{{__('Established Date')}}">
                <span id="established-date-error"  class="error-txt established-date-error"></span> 
              </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="corporate_presentation" class="control-label">{{ __('Corporate Presentation (Youtube URL)') }}</label>
                      <input id="corporate_presentation" value="{{old('corporate_presentation',$user->userDetail->corporate_presentation)}}" class="form-control"   name="corporate_presentation">
                      <span id="corporate-presentation-error"  class="error-txt corporate-presentation-error"></span> 
              </div>
                  </div>
                  @endif
                </div>               
                     
              </div>        
            </div>
            <!-- </div>         -->
            <div class="btn-cols text-right mb-2">
              <button class="btn cancel-btn mr-2" type="submit">Reset</button>
              <button class="btn submit-btn" type="submit">Save Changes</button>
            </div>
            </form>
        </div>
    </div>
    </div>
  </div>
</main>
 <!-- <form method="post" enctype="multipart/form-data" id="public-profile-form" action="{{route('account.storePublicProfile')}}">
    @csrf
    <h1>Public Profile Details</h1>
    <div class="row">
        <div class="col-lg-6">
            <label class="light-label">{{ __(' Photo / Logo') }} </label>
            <input type="file" class="dropify" id="profile_image"  accept = 'image/jpeg , image/jpg, image/gif, image/png'  data-default-file="" name="profile_image"/>
        </div>
        <div class="col-lg-6">
            <label for="profile_headline">{{ __('Profile Headline') }}
                <span class="mandatory">*</span>
            </label>
            <input value="{{old('profile_headline')}}" type="text" name="profile_headline" class="form-control" id="profile_headline" minlength="3" maxlength="250" placeholder="{{__('Enter Profile Headline')}}" required>
        </div>
        <div class="col-lg-6">
            <label for="industry_ids">{{ __('Select Industries') }}
                <span class="mandatory">*</span>
            </label><br>
            <select name="industry_ids[]" id="industry_ids" multiple="multiple" class="form-control" required>
                <option value=""></option>
                @foreach ($industries as $industry)
                    <option value="{{$industry->id}}" {{!empty(old('industry_ids')) ? in_array($industry->id,old('industry_ids')) ? 'selected' : '' : ''}}>{{$industry->name}}</option>
                @endforeach
            </select>
        </div>
        @php
        if(!empty(old('categories_ids'))){
            $old_category = old('categories_ids');
            $old_sub_categories = App\SubCategory::whereHas('categories',function($q) use($old_category){
                        $q->whereIn('id',$old_category);
                    })->where(['active'=>1])->pluck('name','id');
        }else{
            $old_sub_categories = [];
        }
        @endphp
        <div class="col-lg-6">
            <label for="categories_ids">{{ __('Select Categories') }}
                <span class="mandatory">*</span>
            </label><br>
            <select name="categories_ids[]" id="category_ids" class="form-control" multiple="multiple" required>
                <option value=""></option>
                @if(!empty($old_categories))
                    @foreach ($old_categories as $key=>$category)
                        <option value="{{$key}}" {{!empty(old('categories_ids')) ? in_array($key,old('categories_ids')) ? 'selected' : '' : ''}}>{{$category}}</option>
                    @endforeach
                @endif
            </select>
        </div>
       
        <div class="col-lg-6">
            <label for="sub_categories_ids">{{ __('Select Sub Categories') }}
                <span class="mandatory">*</span>
            </label><br>
            <select name="sub_categories_ids[]" id="sub_category_ids" class="form-control" multiple="multiple" required>
                <option value=""></option>
                @if(!empty($old_sub_categories))
                    @foreach ($old_sub_categories as $key=>$sub_category)
                        <option value="{{$key}}" {{!empty(old('sub_categories_ids')) ? in_array($key,old('sub_categories_ids')) ? 'selected' : '' : ''}}>{{$sub_category}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-lg-6">
            <label for="company_tagline">{{ __('Prtfolio of Projects/Works') }}
            </label>
            <input value="{{old('website_url')}}" type="text" name="website_url" class="form-control" id="website_url" minlength="3" placeholder="{{__('Enter Website URLs')}}">
        </div>
        <div class="col-lg-6">
            <label for="company_tagline">{{ __('Company Tagline') }}
            </label>
            <input value="{{old('company_tagline')}}" type="text" name="company_tagline" class="form-control" id="company_tagline" minlength="3" maxlength="300" placeholder="{{__('Enter Company Tagline')}}">
        </div>
        @if($user->organization_type == 0) 
        <div class="col-lg-6">
            <label for="established_date">{{ __('Established Date') }}
            </label>
            <input type="text" value="{{old('established_date')}}" name="established_date" class="form-control no-type" autocomplete="off" id="established_date" placeholder="{{ __('Established Date') }}">
        </div>
        @endif
        <div class="col-lg-6">
            <label for="company_history">{{ __('Company / Individual Background') }}
            <span class="mandatory">*</span>
            </label>
            <textarea name="company_history" id="company_history" rows="20" class="form-control" required>{{old('company_history')}}</textarea>
        </div>
        @if($user->organization_type == 1) 
        <div class="col-lg-6 @if ($user->organization_type == 1) @endif">
            <label for="years_of_experience">{{ __('Years of Experience') }}
            </label>
            <input type="number" value="{{old('years_of_experience')}}" name="years_of_experience" class="form-control no-inc numericOnly" autocomplete="off" id="years_of_experience" placeholder="{{ __('Years of Experience') }}" min="0" max="100">
        </div>
        @endif
        <div class="col-lg-6">
            <label for="summernote-basic">{{ __('Corporate Presentation') }}</label>
            <textarea id="summernote-basic" class="form-control" rows="20"  name="corporate_presentation">{!!old('corporate_presentation')!!}</textarea>
        </div>
        <div class="col-lg-6">
            <label for="video_links">{{ __('Video Links') }}
            </label>
            <div class="row">
                <table class="table">
                <tbody>
                    @if(!empty(old('video_links')))
                        @foreach (old('video_links') as $old_video)
                            <tr>
                                <td scope="row"><input  type="text" name="video_links[]" class="form-control" value="{{$old_video}}" id="video_links" placeholder="Video Link"></td>
                                <td><button class="btn btn-success add_more" type="button">+</button><button type="button" class="btn btn-danger ml-2 remove_row">-</button></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td scope="row"><input  type="text" name="video_links[]" class="form-control" id="video_links" placeholder="Video Link"></td>
                            <td><button class="btn btn-success add_more" type="button">+</button><button type="button" class="btn btn-danger ml-2 remove_row">-</button></td>
                        </tr>
                    @endif
                </tbody>
                </table>
            </div>
        </div>
    </div>
       
        
    <div class="mt-2">
        <button type="submit">Submit</button>
    </div>
</form>  -->

@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}


<script>
$(document).ready(function(){
        //$('table.video-link-col tr:last').find('.remove_row').hide();
        $('table.video-link-col tr').not('table.video-link-col tr:last').find('.remove_row').show();
    });
      var icon = {!! json_encode(url('/storage/admin/propertytypes/')) !!} +'/'+ $(this).attr('data-image');
  var drEvent = $('#profile_image').dropify();
  drEvent = drEvent.data('dropify');
  drEvent.resetPreview();
  drEvent.clearElement();
  drEvent.destroy();
  drEvent.init();

  var drEvent1 = $('.dropify').dropify();

  drEvent1.on('dropify.afterClear', function(event, element){
      if($('.edit-popup').hasClass('dropify-selected') ){
          $('input[name=remove_title]').val('removed');
          alert($('input[name=remove_title]').val());
      }
  });
    // $(document).ready(function(){
      
    //     $('input[name="established_date"]').daterangepicker({
    //             singleDatePicker: true,
    //             autoUpdateInput: false,
    //             timePicker: false,
    //             maxDate:new Date(),
    //             locale: {
    //                 format: 'DD-MM-YYYY'
    //             },
    //             autoApply: true
    //     });
    //     $('input[name="established_date"]').on('apply.daterangepicker', function(ev, picker) {
    //         $(this).val(picker.startDate.format('DD-MM-YYYY'));
    //     });
    // })
      
    $(document).ready(function(){
        $('input[name="established_date"]').datepicker({
            icons: {
                rightIcon: '<i class="fi-flaticon flaticon-calendar"></i>'
            },
            format: 'dd-mm-yyyy' 

        });
        
      $('#industry_ids').select2({
            placeholder: "{{__('Select Industries')}}",
        });
        $('#category_ids').select2({
            placeholder: "{{__('Select Categories')}}",
        });
        $('#sub_category_ids').select2({
            placeholder: "{{__('Select Sub Categories')}}",
        });
        $('#skill_ids').select2({
      placeholder: "{{__('Select Skills')}}",
    });
      
        $('#industry_ids').change(function(){
            var category_ids = $('#category_ids').val();
            $.ajax({
                type: "GET",
                url: "{{url('/get-categories')}}",
                data: {'industry_ids':$(this).val()},
                success: function (response) {
                    var new_options = "<option value =''></option>";
                    $.each(response, function (index, value) { 
                        if(jQuery.inArray(index, category_ids) !== -1){
                            new_options += "<option value='"+index+"' selected>"+value+"</option>";
                        }else{
                            new_options += "<option value='"+index+"'>"+value+"</option>";
                        }
                    });
                    $('#category_ids').html(new_options);
                    $('#category_ids').change();
                    $('#category_ids').select2({
                        placeholder: "{{__('Select Categories')}}",
                    });
                }
            });
        });
        $('#category_ids').change(function(){
            var sub_category_ids = $('#sub_category_ids').val();
            $.ajax({
                type: "GET",
                url: "{{url('/get-sub-categories')}}",
                data: {'category_ids':$(this).val()},
                success: function (response) {
                    var new_options = "<option value =''></option>";
                    $.each(response, function (index, value) { 
                        if(jQuery.inArray(index, sub_category_ids) !== -1){
                            new_options += "<option value='"+index+"' selected>"+value+"</option>";
                        }else{
                            new_options += "<option value='"+index+"'>"+value+"</option>";
                        }
                    });
                    $('#sub_category_ids').html(new_options);
                    $('#sub_category_ids').change();
                    $('#sub_category_ids').select2({
                        placeholder: "{{__('Select Sub Categories')}}",
                    });
                }
            });
        });


    $('#sub_category_ids').change(function() {
      var skill_ids = $('#skill_ids').val();
      $.ajax({
        type: "GET",
        url: "{{url('/get-skills')}}",
        data: {
          'sub_category_ids': $(this).val()
        },
        success: function(response) {
          var new_options = "<option value =''></option>";
          $.each(response, function(index, value) {
            if (jQuery.inArray(index, skill_ids) !== -1) {
              new_options += "<option value='" + index + "' selected>" + value + "</option>";
            } else {
              new_options += "<option value='" + index + "'>" + value + "</option>";
            }
          });
          $('#skill_ids').html(new_options);
          $('#skill_ids').select2({
            placeholder: "{{__('Select Sub Categories')}}",
          });
        }
      });
    });
        
        
    });

    $(document).on("click", '.add_portfolio', function() {
    var $tr = $(this).closest('tr');
    var c = $(".model").clone(true, true);
    var start = $('.portfolio_title').length;
    c.removeClass('model');
    c.find('input').prop('required',false); 
    c.find('textarea').prop('required',false); 
    c.find('input.portfolio_images').addClass('dropify');
    c.find('input.portfolio_title').attr('name','portfolio_titles[]');
    c.find('input.portfolio_ids').attr('name','portfolio_id[]');
    c.find('input.portfolio_images').attr('name','portfolio_images[]');
    c.find('textarea.portfolio_description').attr('name','portfolio_descriptions[]');
    c.find('input.portfolio_title')
        .attr('id', 'portfolio_title' + start).prop('required',true); 
    c.find('textarea.portfolio_description')
        .attr('id', 'portfolio_description' + start).prop('required',true);
    $tr.after(c);
    c.show();
    $('.dropify').dropify();
    var count = $('.add_portfolio').length;
 
 if(count>0)
 {
  
   $('.remove_portfolio').show();
 }
  });
  
  $(document).on("click", '.remove_portfolio', function() {
    if($('.remove_portfolio').length==3)
  {
    $('.remove_portfolio').hide();
  }
    if ($('.portfolio_images').length - 1 == 1) {
      alert('Cannot delete this row');
    } else {
      var $tr = $(this).closest('tr').remove();
    }
  });
    $(document).on("click",'.add_more',function(){
      
        var $tr    = $(this).closest('tr');
        var $clone = $tr.clone();
        $clone.find('input').val('');
        $tr.after($clone);
        $(this).hide();
        $('table.video-link-col tr:last').find('.remove_row').hide();
        $('table.video-link-col tr').not('table.video-link-col tr:last').find('.remove_row').show();
    });
    $(document).on("click",'.remove_row',function(){
        if( $(this).closest('tr').is('tr:only-child') ) {
            //alert('cannot delete last row');
        }else{
            var $tr    = $(this).closest('tr').remove();
        }
    });
    $("#public-profile-form").submit(function(){
   
   var isFormValid = true;
   $('#public-profile-form').find("input.required,textarea.required").each(function(){
       console.log(this.id);
       if((this.type=="text" || this.type=="number" || this.type=="textarea"))
       {
     //  console.log(this.type);
   if(!$(this).val()){
       $(this).addClass("error");
       isFormValid = false;

   } else{
       $(this).removeClass("error");
   }
}
});
//   alert(isFormValid);
return isFormValid;
});

    $("#public-profile-form").validate({
      onfocusout: function(element){
         $(element).valid();
   	
    },
    onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        // onfocusin: function(element){
        // $(element).removeClass('error');
        // $(element).next().text('');
        // },
    rules: {
    //   profile_image:  {
    //         required: true,
    //     },
        profile_headline: {
            required: true,
            maxlength:300,
        },
        industry_ids: {
            required: false,
            maxlength:300,
        },
        categories_ids: {
            required: true,
        },
        sub_categories_ids: {
            required: true,
          
        },
        company_tagline: {
            required: true,
          
        },
        
        skills: {
        required: true,

      },
      about_me: {
        required: true,

      },
      portfolio_titles: {
        required: true,
      },
      portfolio_descriptions: {
        required: true,
      }
       
     
        
        
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } else if (element.attr("id") == "profile_image" ) {
          $(".profile-error").html(error);
        }
        else if (element.attr("id") == "profile_headline" ) {
          $(".profile-headline-error").html(error); 
        }
        else if (element.attr("id") == "industry_ids" ) {
          //error.insertAfter(".error-jqu-whats");
          $(".industries-error").html(error);
        }
        else if (element.attr("id") == "category_ids" ) {
          
          $(".categories-error").html(error);
        }
        else if (element.attr("id") == "sub_category_ids" ) {
          $(".sub_categories-error").html(error); 
        }
        else if (element.attr("id") == "company_tagline" ) {
          $(".company-tagline-error").html(error); 
        }
        else if (element.attr("id") == "company_history" ) {
          $(".company-history-error").html(error); 
        }
        else if (element.attr("id") == "skill_ids") {
        $(".skills-error").html(error);
      } 
      else if (element.attr("id") == "about_me") {
        $(".about-me-error").html(error);
      }
        else {
           error.insertAfter(element);
        }
   },

   messages: {
           'profile_image': {
                required: "Profile image is required.",
            },
            'profile_headline': {
                required: "Profile headline is required.",
            },
            'industry_ids': {
                required: "Choose an industry.",
            },
            'company_tagline': {
                required: "Company tag line is required.",
            },
            'company_history': {
                required: "Company history is required.",
            },
             'about_me': {
          required: "About your self required.",
           }
            // 'Phone': {
            //     required: "Phone is required.",
            // },
            // 'otp': {
            //   required: "OTP is required.",
            // }
        }

});
</script>


@endpush