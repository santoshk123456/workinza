@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Contact Information')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
<style>
    .bold-span{
        font-weight: 700!important;
    }
</style>
@endpush
@section('content')
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')
        <div class="col-lg-9">
            <div class="right-aside">
            <div class="card-top mb-4">
            <div class="btn-cols">
              <h4>Contact Information</h4>
              <p class="mb-0">View your contact information</p>  
              </div>
              <div class="btn-cols text-right" style="margin-top: -45px;">
                  <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ route('user.redirectBack') }}'">Back</button>
                  <button class="btn submit-btn" id="otp-verify-btn" type="button" onclick="location.href='{{ route('user.editContactInformation') }}'">Edit</button>
                </div> 
            </div>
            <div class="contact-info mb-4">
              <div class="row">
                <div class="col-md-6 mb-3">
                  <p class="mb-0">Name: <span class="title-sub">@if(!empty($user->full_name)) {{ ucwords($user->full_name) }} @else {{__('NA')}} @endif</span></p>
                </div>
                <div class="col-md-6">
                  <p class="mb-0 last-col">Account Type: <span class="title-sub">@if(empty($user->organization_type)) {{__('Company')}} @else {{__('Individual')}} @endif</span></p>
                </div>
                <div class="col-md-6 mb-3">
                  <p class="mb-0 @if(!empty($user->organization_type)) last-col @endif">Email: <span class="title-sub">@if(!empty($user->email)) {{ ($user->email) }} @else {{__('NA')}} @endif</span> <span class="verify-icon">Verified</span></p>
                </div>
                @if(empty($user->organization_type))
                <div class="col-md-6">
                  <p class="mb-0 last-col">Company Name: <span class="title-sub">@if(!empty($user->userDetail->company_name)) {{ ucwords($user->userDetail->company_name)}} @else {{__('NA')}} @endif</span></p>
                </div>
                @endif

              </div>
            </div>
            <div class="card-content">
              <form method="POST" id="contact_details_form" action="{{route('user.storeContactInformation')}}"> 
              @csrf
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-group required">
                    <p class="mb-0">Username: <span class="title-sub bold-span"><a href="{{route('userPublicProfile',$user->username)}}">{{str_limit(request()->root(), $limit = 35 )}}/{{ !empty($user->username) ? $user->username : __('NA') }}</a></span></p>
                    </div>
                  </div>
           
                  <div class="col-md-12">
                    <div class="form-group required">
                    <p class="mb-0">Address 1: 
                      <span class="title-sub bold-span">{{ !empty($user->userDetail->address_line_1) ? $user->userDetail->address_line_1 : __('NA') }}</span></p><br>
                      @if(!empty($user->userDetail->address_line_2)) <p class="mb-0">Address 2: <span class="title-sub bold-span">{{ $user->userDetail->address_line_2  }}</span></p><br>@endif
                      <p class="mb-0">City: <span class="title-sub bold-span">{{ !empty($user->userDetail->city) ? $user->userDetail->city : __('NA') }}</span></p><br>
                      <p class="mb-0">State: <span class="title-sub bold-span">{{ !empty($user->userDetail->state) ? $user->userDetail->state : __('NA') }} - {{ !empty($user->userDetail->zipcode) ? $user->userDetail->zipcode : __('NA') }}</span></p><br>
                      <p class="mb-0">Country: <span class="title-sub bold-span">{{ !empty($user->userDetail->country) ? $user->userDetail->country->name : __('NA') }}</span></p><br>
                   
                    <p class="mb-0">Phone: <span class="title-sub bold-span">{{ !empty($user->phone) ? $user->full_number : __('NA') }}</span></p><br>
                    <p class="mb-0">Personal Website: <span class="title-sub bold-span">{{ !empty($user->userDetail->website_url) ? $user->userDetail->website_url : __('NA') }}</span></p>
                  </div>
                  </div>

                  <!-- <div class="col-md-6">
                    <div class="form-group">
                    <p class="mb-0">Address 2: <span class="title-sub bold-span">{{ !empty($user->userDetail->address_line_2) ? $user->userDetail->address_line_2 : __('NA') }}</span></p>
                    </div>
                  </div> -->

                  <!-- <div class="col-md-6">
                    <div class="form-group required">
                    <p class="mb-0">Country: <span class="title-sub bold-span">{{ !empty($user->userDetail->country) ? $user->userDetail->country->name : __('NA') }}</span></p>
                    </div>
                  </div> -->

                  <!-- <div class="col-md-6">
                    <div class="form-group required">
                    <p class="mb-0">State: <span class="title-sub bold-span">{{ !empty($user->userDetail->state) ? $user->userDetail->state : __('NA') }}</span></p>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group required">
                      <p class="mb-0">City: <span class="title-sub bold-span">{{ !empty($user->userDetail->city) ? $user->userDetail->city : __('NA') }}</span></p>
                    </div>
                  </div> -->

               

                  <!-- <div class="col-md-6">
                    <div class="form-group required">
                    <p class="mb-0">Zip Code: <span class="title-sub bold-span">{{ !empty($user->userDetail->zipcode) ? $user->userDetail->zipcode : __('NA') }}</span></p>
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                    <p class="mb-0">Personal Website: <span class="title-sub bold-span">{{ !empty($user->userDetail->website_url) ? $user->userDetail->website_url : __('NA') }}</span></p>
                    </div>
                  </div>       -->
                  
                  <!-- <div class="col-md-12">
                    <div class="form-group required">
                    <p class="mb-0">Phone: <span class="title-sub bold-span">{{ !empty($user->phone) ? $user->full_number : __('NA') }}</span></p>
                    </div>
                  </div>  -->

                  <!-- <div class="col-md-12">
                    <div class="form-group">
                    <p class="mb-0">Personal Website: <span class="title-sub bold-span">{{ !empty($user->userDetail->website_url) ? $user->userDetail->website_url : __('NA') }}</span></p>
                    </div>
                  </div> -->
                         
                </div> 
                <!-- <div class="btn-cols text-right mb-2">
                  <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ route('user.redirectBack') }}'">Back</button>
                  <button class="btn submit-btn" id="otp-verify-btn" type="button" onclick="location.href='{{ route('user.editContactInformation') }}'">Edit</button>
                </div>                 -->
              </form>   
            </div>
        </div>
    </div>
    </div>
  </div>
</main>

<!--end: contact information page -->

@endsection
@push('custom-scripts')
<script>
 $(document).ready(function(){
    generateFirebaseInstant('register-otp');

    $('#user_name_id').on('blur', function(){
        var usr_name = $("#user_name_id").val();
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('user.usernameExist')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'username':usr_name
                     },
                success: function (data) { 
                    if((data['username_exist']) == 1){
                        // user name already exist message
                        $('#username_error').text('Sorry..Username already taken');
                    }else{
                        $('#username_error').text('');
                    }
                }
        });
    });
 });

function verifyMobile(){
    if($('#phone').val().length >= 8){

        var phoneNumber = '+'+$('#country_code').val()+$('#phone').val()
     
        
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('user.phoneNumberExist')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'phone':$('#phone').val()
                     },
                success: function (data) { 
                    if((data['phone_exist']) == 1){
                        // user name already exist message
                        $('#register-mobile-error').html("Mobile number already exist");
                    }else{
                      sendFirebaseOTP(phoneNumber);
                      $('#register-otp-div').removeClass("d-none");
                      $(".btn-verify").hide();
                      $(".resend-button").removeClass('d-none');
                      showToast('Success', "OTP has been sent to your mobile number", 'success');
                      $('#otp-verify-btn').removeAttr("disabled");
                      $('#register-mobile-error').addClass('d-none');
                    
                    }
                }
          });
    }else{
      $('#register-mobile-error').html("Invalid mobile number");
    }
    
       
}


function confirmVerification(){
  if($("#contact_details_form").valid()){
    var code = $('#inputOPT').val();
    if(code.length != 0){
        confirmationResult
        .confirm(code)
        .then(function(result) {
            var user = result.user;
            $('#contact_details_form').submit();
        })
        .catch(function(error) {
            $('#register-otp-error').html("Invalid OTP");
        });
    }else{
        $('#register-otp-error').html("OTP is required");
    }
  }
};
</script>

<script>
$('#user_name_id').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    e.preventDefault();
    return false;
});
</script>
<script>
   $(document).ready(function(){
    var phone = document.querySelector("#phone");


    var phone_iti = window.intlTelInput(phone,({

        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = "in";//(resp && resp.country) ? resp.country : "in";
                callback(countryCode);
            });
        },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.1.0/js/utils.js",
        separateDialCode: true,
        hiddenInput: "full_number",
        autoPlaceholder: "polite",
        initialCountry: "us",
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            return selectedCountryPlaceholder.replace(/[0-9]/g,"x");
        }, 
    }));
   
    phone.addEventListener("countrychange", function() {
     
        $('#country_code').val(phone_iti.getSelectedCountryData().dialCode);
       
    });
  });
</script>

<script>
 $("#contact_details_form").validate({
  onfocusout: function(element){
         $(element).valid();
   	
    },
    onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        // onfocusin: function(element){
        // $(element).removeClass('error');
        // $(element).next().text('');
        // },
    rules: {
         username:  {
            required: true,
            maxlength:50,
        },
        address_line_1: {
            required: true,
            maxlength:300,
        },
        address_line_2: {
            required: false,
            maxlength:300,
        },
        country_id: {
            required: true,
        },
        city: {
            required: true,
            maxlength : 100,
        },
        state: {
            required: true,
            maxlength : 100,
        },
        zipcode: {
            required: true,
            maxlength : 20,
        },
        website_url: {
            required: false,         
            url: true,
        },   
        phone: {
            required: true,
            maxlength : 10,
            minlength : 8,
        }, 
        otp: {
            required: true,
            maxlength : 6,
        }
        
        
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } else if (element.attr("id") == "user_name_id" ) {
          $(".username-error").html(error);
        }
        else if (element.attr("id") == "address_line_1" ) {
          $(".address-one-error").html(error); 
        }
        else if (element.attr("id") == "country_id" ) {
          //error.insertAfter(".error-jqu-whats");
          $(".country-error").html(error);
        }
        else if (element.attr("id") == "city" ) {
          $(".city-error").html(error); 
        }
        else if (element.attr("id") == "state" ) {
          $(".state-error").html(error); 
        }
        else if (element.attr("id") == "zipcode" ) {
          $(".zipcode-error").html(error); 
        }
        else if (element.attr("id") == "website_url" ) {
          $(".website-error").html(error); 
        }
        else if (element.attr("id") == "phone" ) {
          $(".phone-error").html(error); 
        }
        else if (element.attr("id") == "inputOPT" ) {
          $(".motp-error").html(error); 
        }
        else {
           error.insertAfter(element);
        }
   },

   messages: {
           'username': {
                required: "User name is required.",
            },
            'address_line_1': {
                required: "Address is required.",
            },
            'country_id': {
                required: "Country is required.",
            },
            'state': {
                required: "State is required.",
            },
            'city': {
                required: "City is required.",
            },
            'zipcode': {
                required: "Zipcode is required.",
            },
            'Phone': {
                required: "Phone is required.",
            },
            'otp': {
              required: "OTP is required.",
            }
        }

});
</script>
<script>
$(".reset").click(function() {
    $(this).closest('form').find("input[type=text], textarea,select").val("");
});
</script>
@endpush
