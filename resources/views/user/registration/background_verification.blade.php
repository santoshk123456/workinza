
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Background Verification')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')
<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')
      <div class="col-lg-9">
        @if(auth()->user()->approved == 0  && (Request::get('success') == 1))
             <div class="right-aside">                 
                <div class="card-content text-center">
                  <div class="thankyou-col">
                    <div class="success-tick mb-4">
                      <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
                      </svg>
                    </div>
                    <h5 class="mb-5">You are almost there !</h5>
               
                    <p class="mb-4">Your profile is submited for background verification. Once the verification is completed,
                      you will be notified by email and you can use the application</p>
                      <p class="thank-note">Thank you for your patience!</p>               
                </div>
                </div>
              </div>
          @elseif(auth()->user()->approved == 1)
          <div class="right-aside">                 
                <div class="card-content text-center">
                  <div class="thankyou-col">
                    <div class="success-tick mb-4">
                      <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
                      </svg>
                    </div>
                    <h5 class="mb-5">Background Verification Completed</h5>
                    <p class="mb-4">Your profile background verification is completed</p>                                 
                </div>
                  <div class="btn-cols">   
                    @if(auth()->user()->step > 4)
                    <a href="{{ route('user.redirectBack') }}" class="btn submit-btn" role="button">Back</a>
                    @else              
                      <a href="{{route('membership_plan.membershipPlans')}}" class="btn submit-btn" role="button">Continue</a>
                    @endif
                </div> 
                </div>
              </div>
          @elseif(auth()->user()->approved == 0)
          <div class="right-aside">                 
                <div class="card-content text-center">
                  <div class="thankyou-col">
                    <div class="success-tick mb-4">
                      <svg width="63px" height="78px" viewBox="0 0 73 88" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="hourglass">
                          <path d="M63.8761664,86 C63.9491436,84.74063 64,83.4707791 64,82.1818182 C64,65.2090455 57.5148507,50.6237818 48.20041,44 C57.5148507,37.3762182 64,22.7909545 64,5.81818182 C64,4.52922091 63.9491436,3.25937 63.8761664,2 L10.1238336,2 C10.0508564,3.25937 10,4.52922091 10,5.81818182 C10,22.7909545 16.4851493,37.3762182 25.79959,44 C16.4851493,50.6237818 10,65.2090455 10,82.1818182 C10,83.4707791 10.0508564,84.74063 10.1238336,86 L63.8761664,86 Z" id="glass" fill="#ECF1F6"></path>
                          <rect id="top-plate" fill="#BFBFBF" x="0" y="0" width="74" height="8" rx="2"></rect>
                          <rect id="bottom-plate" fill="#BFBFBF" x="0" y="80" width="74" height="8" rx="2"></rect>
                    
                          <g id="top-sand" transform="translate(18, 21)">
                            <clipPath id="top-clip-path" fill="white">
                              <rect x="0" y="0" width="38" height="21"></rect>
                            </clipPath>
                    
                            <path fill="#BFBFBF" clip-path="url(#top-clip-path)" d="M38,0 C36.218769,7.51704545 24.818769,21 19,21 C13.418769,21 1.9,7.63636364 0,0 L38,0 Z"></path>
                          </g>
                    
                          <g id="bottom-sand" transform="translate(18, 55)">
                            <clipPath id="bottom-clip-path" fill="white">
                              <rect x="0" y="0" width="38" height="21"></rect>
                            </clipPath>
                    
                            <g clip-path="url(#bottom-clip-path)">
                              <path fill="#BFBFBF" d="M0,21 L38,21 C36.1,13.3636364 24.581231,0 19,0 C13.181231,0 1.781231,13.4829545 0,21 Z"></path>
                            </g>
                          </g>
                        </g>
                      </svg>
                    </div>
                    <h5 class="mb-3">Please Hold On</h5>
                    <p class="thank-note mb-5">Your Account is under verification</p>   
                    <p class="mb-4">Your verification is in under progress by our team. Please waite for 48 hours to complete verification
                      and you will be notified via email once the verification is completed.</p>
                      <p class="thank-note">Thank you for your patience!</p>               
                </div>
                </div>
              </div>
            @elseif(auth()->user()->approved == 2)
              <div class="right-aside">                 
                <div class="card-content text-center">
                  <div class="thankyou-col">
                    <div class="success-tick mb-4">
                      <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                        <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
                      </svg>
                    </div>
                    <h5 class="mb-5">Background Verification Rjected</h5>
                    <p class="mb-4">Your profile background verification is rejected</p>                                 
                </div>
                  <div class="btn-cols">   
                    
                              
                      <a href="{{route('user.viewContactInformation')}}" class="btn submit-btn" role="button">Continue</a>
                   
                </div> 
                </div>
              </div>
          @endif
      </div>
    </div>
  </div>
</main>

@endsection