@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Contact Information')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
<style>
  .verify-btn:hover {
    border-color: #285e8e!important;
}
</style>
@endpush
@section('content')
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')
        <div class="col-lg-9">
            <div class="right-aside">
            <div class="card-top mb-4">
              <h4>Contact Information</h4>
              <p class="mb-0">Enter your contact information</p>  
            </div>
            <div class="contact-info mb-4">
              <div class="row">
                <div class="col-md-6 mb-3">
                  <p class="mb-0">Name: <span class="title-sub">@if(!empty($user->full_name)) {{ ucwords($user->full_name) }} @else {{__('NA')}} @endif</span></p>
                </div>
                <div class="col-md-6">
                  <p class="mb-0 last-col">Account Type: <span class="title-sub">@if(empty($user->organization_type)) {{__('Company')}} @else {{__('Individual')}} @endif</span></p>
                </div>
                <div class="col-md-6 mb-3">
                  <p class="mb-0 @if(!empty($user->organization_type)) last-col @endif">Email: <span class="title-sub">@if(!empty($user->email)) {{ ($user->email) }} @else {{__('NA')}} @endif</span> <span class="verify-icon">Verified</span></p>
                </div>
                @if(empty($user->organization_type))
                <div class="col-md-6">              
                  <p class="mb-0 last-col">Company Name: <span class="title-sub">@if(!empty($user->userDetail->company_name)) {{ ucwords($user->userDetail->company_name)}} @else {{__('NA')}} @endif</span></p>
                </div>
                @endif

              </div>
            </div>
            <div class="card-content">
              <form method="POST" id="contact_details_form" action="{{route('user.storeContactInformation')}}"> 
              @csrf
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="user_name_id" class="control-label">Username</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="inputGroupPrepend">{{str_limit(request()->root(), $limit = 35 )}}/</span>
                        </div>
                        <input type="text" autofocus="autofocus" tabindex="1" value="{{old('username')}}" name="username" id="user_name_id" maxlength="50" class="form-control" placeholder="username" aria-describedby="inputGroupPrepend">
                      </div>
                      <span id="username_error"  class="error-txt username-error"></span>
                    </div>
                  </div>
           
                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="address_line_1" class="control-label">Address 1</label>
                      <input type="text" tabindex="2" value="{{old('address_line_1')}}" name="address_line_1" class="form-control" id="address_line_1" placeholder="Enter address line 1" maxlength="300">
                       <span class="error-txt address-one-error"></span>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="address_line_2" class="control-label">Address 2</label>
                      <input type="text" tabindex="3" value="{{old('address_line_2')}}" class="form-control" name="address_line_2" id="address_line_2" placeholder="Enter address line 2">
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="country_id" class="control-label">Country</label>  
                      <select tabindex="4" class="country-select form-control" name="country_id" id="country_id" required>
                      <option value=""  disabled selected>Select Country</option>
                          @foreach ($countries as $country)
                              <option value="{{$country->id}}" {{old('country_id')== $country->id ?'selected' :''}} required>{{ucwords($country->name)}}</option>
                          @endforeach                 
                      </select>        
                      <span class="error-txt country-error"></span>            
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="state" class="control-label">State</label>
                      <input tabindex="5" value="{{old('state')}}" class="form-control" type="text" name="state"  id="state" placeholder="Your state">
                      <span class="error-txt state-error"></span>    
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="city" class="control-label">City</label>
                      <input tabindex="6" value="{{old('city')}}" class="form-control" type="text" name="city"  id="city" placeholder="Your city">
                      <span class="error-txt city-error"></span>    
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="zipcode" class="control-label">Zip Code</label>
                      <input tabindex="7" value="{{old('zipcode')}}" class="form-control numericOnly" type="text" name="zipcode"  id="zipcode"  placeholder="Enter postal code">
                      <span class="error-txt zipcode-error"></span>   
                    </div>
                  </div>
                  
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="validationCustom07">Personal Website</label>
                      <input tabindex="8" name="website_url" id="website_url" type="text" class="form-control" placeholder="Enter website url">   
                      <span class="error-txt website-error"></span>    
                    </div>
                  </div>      
                  
                  <div class="col-md-6">
                    <div class="form-group required">
                      <label for="validationCustom07" class="d-block control-label">Phone</label>
                      <input type="hidden" name="country_code" id="country_code" value="{{old('country_code',1)}}">
                      <input value="{{old('full_number')}}"  tabindex="9" type="text" name="phone" class="form-control numericOnly" id="phone" minlength="8" maxlength="15" placeholder="{{__('Enter Mobile Number')}}" required>
                      <span class="error-txt phone-error" id="register-mobile-error"></span>
                      <div class="row pt-2">
                        <div class="col-md-6 pr-lg-0 mb-3 mb-lg-0">
                          <a href="javascript:void(0);" type="button" class="btn submit-btn w-100 verify-btn btn-verify" id="register-otp" onclick="verifyMobile()">Verify Mobile Number</a>

                          <a href="javascript:void(0);" type="button" class="btn submit-btn w-100 verify-btn resend-button d-none" onclick="verifyMobile()">Resend</a>

                        </div>                        
                        <div class="col-md-6 otp-cols d-none" id="register-otp-div">
                          <input type="text" class="form-control numericOnly"  id="inputOPT" name="otp" maxlength="6" placeholder="Enter the OTP here">
                          <span class="error-txt motp-error" id="register-otp-error"></span>
                        </div>
                      </div>  

                    </div>
                  </div> 
                         
                </div> 
                <div class="btn-cols text-right mb-2">
                  <button class="btn cancel-btn mr-2 reset" type="button">Cancel</button>
                  <button class="btn submit-btn" disabled id="otp-verify-btn" type="button" onclick="confirmVerification()">Continue</button>
                </div>                
              </form>   
            </div>
        </div>
    </div>
    </div>
  </div>
</main>

<!--end: contact information page -->

@endsection
@push('custom-scripts')
<script>
  $(document).on('focus', '.select2.select2-container', function (e) {
  
  var isOriginalEvent = e.originalEvent // don't re-open on closing focus event
  var isSingleSelect = $(this).find(".select2-selection--single").length > 0 // multi-select will pass focus to input

  if (isOriginalEvent && isSingleSelect) {
    $(this).siblings('select:enabled').select2('open');
  } 

});
 $(document).ready(function(){
    generateFirebaseInstant('register-otp');

    $('#user_name_id').on('blur', function(){
        var usr_name = $("#user_name_id").val();
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('user.usernameExist')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'username':usr_name
                     },
                success: function (data) { 
                    if((data['username_exist']) == 1){
                        // user name already exist message
                        $('#username_error').text('Sorry..Username already taken');
                    }else{
                        $('#username_error').text('');
                    }
                }
        });
    });
 });

function verifyMobile(){
    if($('#phone').val().length >= 8){

        var phoneNumber = '+'+$('#country_code').val()+$('#phone').val()
     
        
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('user.phoneNumberExist')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'phone':$('#phone').val()
                     },
                success: function (data) { 
                    if((data['phone_exist']) == 1){
                        // user name already exist message
                        $('#register-mobile-error').html("Mobile number already exist");
                    }else{
                      sendFirebaseOTP(phoneNumber);
                      $('#register-otp-div').removeClass("d-none");
                      $(".btn-verify").hide();
                      $(".resend-button").removeClass('d-none');
                      showToast('Success', "OTP has been sent to your mobile number", 'success');
                      $('#otp-verify-btn').removeAttr("disabled");
                      $('#register-mobile-error').addClass('d-none');
                    
                    }
                }
          });
    }else{
      $('#register-mobile-error').html("Invalid mobile number");
    }
    
       
}


function confirmVerification(){
  if($("#contact_details_form").valid()){
    var code = $('#inputOPT').val();
    if(code.length != 0){
        confirmationResult
        .confirm(code)
        .then(function(result) {
            var user = result.user;
            $('#contact_details_form').submit();
        })
        .catch(function(error) {
            // $('#register-otp-error').html("Invalid OTP");
            showToast('Error', "Invalid OTP!", 'error');
        });
    }else{
        $('#register-otp-error').html("OTP is required");
    }
  }
};
</script>

<script>
$('#user_name_id').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    e.preventDefault();
    return false;
});
$("#phone").keypress(function(){
  
  $('#inputOPT').val('');
  // $('#register-otp-div').addClass("d-none");
  $(".btn-verify").show();
  $(".btn-verify").html('Verify Mobile Number');
  $(".resend-button").addClass('d-none');
  $('#otp-verify-btn').attr('disabled', 'disabled');
  $('#register-mobile-error').removeClass('d-none');
                    
});
</script>
<script>
  window.i = 0;
   $(document).ready(function(){

    var phone = document.querySelector("#phone");


    var phone_iti = window.intlTelInput(phone,({

        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = "in";//(resp && resp.country) ? resp.country : "in";
                callback(countryCode);
            });
        },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.1.0/js/utils.js",
        separateDialCode: true,
        hiddenInput: "full_number",
        
        autoPlaceholder: "polite",
        initialCountry: "us",
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            return selectedCountryPlaceholder.replace(/[0-9]/g,"x");
        }, 
    }));
   
    phone.addEventListener("countrychange", function() {
        
        $('#country_code').val(phone_iti.getSelectedCountryData().dialCode);
        if(window.i == 0){
          $('.flag-container').first().remove();
        }
        
       window.i = window.i + 1;
    });
  });
</script>

<script>
 $("#contact_details_form").validate({
  onfocusout: function(element){
         $(element).valid();
   	
    },
    onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        // onfocusin: function(element){
        // $(element).removeClass('error');
        // $(element).next().text('');
        // },
    rules: {
         username:  {
            required: true,
            maxlength:50,
        },
        address_line_1: {
            required: true,
            maxlength:300,
        },
        address_line_2: {
            required: false,
            maxlength:300,
        },
        country_id: {
            required: true,
        },
        city: {
            required: true,
            maxlength : 100,
        },
        state: {
            required: true,
            maxlength : 100,
        },
        zipcode: {
            required: true,
            maxlength : 20,
        },
        website_url: {
            required: false,         
            url: true,
        },   
        phone: {
            required: true,
            maxlength : 10,
            minlength : 8,
        }, 
        otp: {
            required: true,
            maxlength : 6,
        }
        
        
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } else if (element.attr("id") == "user_name_id" ) {
          $(".username-error").html(error);
        }
        else if (element.attr("id") == "address_line_1" ) {
          $(".address-one-error").html(error); 
        }
        else if (element.attr("id") == "country_id" ) {
          //error.insertAfter(".error-jqu-whats");
          $(".country-error").html(error);
        }
        else if (element.attr("id") == "city" ) {
          $(".city-error").html(error); 
        }
        else if (element.attr("id") == "state" ) {
          $(".state-error").html(error); 
        }
        else if (element.attr("id") == "zipcode" ) {
          $(".zipcode-error").html(error); 
        }
        else if (element.attr("id") == "website_url" ) {
          $(".website-error").html(error); 
        }
        else if (element.attr("id") == "phone" ) {
          $(".phone-error").html(error); 
        }
        else if (element.attr("id") == "inputOPT" ) {
          $(".motp-error").html(error); 
        }
        else {
           error.insertAfter(element);
        }
   },

   messages: {
           'username': {
                required: "User name is required.",
            },
            'address_line_1': {
                required: "Address is required.",
            },
            'country_id': {
                required: "Country is required.",
            },
            'state': {
                required: "State is required.",
            },
            'city': {
                required: "City is required.",
            },
            'zipcode': {
                required: "Zipcode is required.",
            },
            'Phone': {
                required: "Phone is required.",
            },
            'otp': {
              required: "OTP is required.",
            }
        }

});
</script>
<script>
$(".reset").click(function() {
    $(this).closest('form').find("input[type=text], textarea,select").val("");
});
</script>
@endpush
