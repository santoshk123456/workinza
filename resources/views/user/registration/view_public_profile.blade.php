@extends('user.layouts.user_two')
@section('title', set_page_titile(__('View Public Profile')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
<style>
    .view-detail-col .descrption-col .notify-col {
        padding: 1px 4px!important;
        font-size: 11px!important;
        border-radius: 4px!important;
        margin-left: 5px!important;
    }
</style>
@endpush
@section('content')


<main class="main-wrapper">
    <div class="container inner-pg-layout">
        <div class="row mx-0">
            @include('user.layouts.includes.profile_tab')
            <div class="col-lg-9">
                <div class="right-aside">
                    <div class="card-top mb-4">
                        <h4>Profile Card</h4>
                        <p class="mb-0">Your Profile Card Informations</p>
                        <button class="btn submit-btn float-right" id="otp-verify-btn" style="margin-top: -45px;" type="button" onclick="location.href='{{ route('account.editPublicProfile') }}'">Edit</button>
                    </div>
                    <!-- <div class="btn-col text-right mb-3">
              <button class="btn back-btn" type="submit"><i class="fi-flaticon flaticon-download mr-2"></i>Download Card</button>

            </div>    -->
                    <div class="bg-shadow view-detail-col mb-3">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="user-image-col">
                                <figure class="figure">
                                    @if($user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$user->profile_image)))
                                    <img src="{{ asset('storage/user/profile-images/'.$user->profile_image) }}" alt="{{ $user->full_name }}" class="figure-img img-fluid rounded">
                                    @else
                                    <img src="{{ asset('images/admin/starter/default.png') }}" alt="{{ $user->full_name }}" class="figure-img img-fluid rounded">
                                    @endif

                                </figure>
                                </div>
                            </div>
                            <div class="col-md-8 descrption-col">
                                <h4 class="text-white"> {{ ucwords($user->full_name) }}</h4>
                                <!-- <span class="text-white">samual1337x <i class="fi-flaticon flaticon-user ml-1"></i></span> -->
                                <p class="text-white"><i class="fi-flaticon flaticon-location-pin mr-1"></i>{{ !empty($user->userDetail->address_line_1) ? $user->userDetail->address_line_1 : __('NA') }} </p>

                                <div class="feature-skills">
                                    <p class="text-white d-inline-flex mr-2">{{ __(' Industry') }}:</p>
                                    <div class="skils-cols d-inline-flex">

                                        @forelse($user->industries->pluck('name')->toArray() as $industry)
                                        <span class="notify-col">{{$industry}}</span>
                                        @empty

                                        <p class="text-white">{{ __('NA')}}</p>
                                        @endforelse

                                    </div>
                                </div>
                                <div class="feature-skills">
                                    <p class="text-white d-inline-flex mr-2">{{ __(' Categories') }}:</p>
                                    <div class="skils-cols d-inline-flex">

                                        @forelse($user->categories->pluck('name')->toArray() as $category)
                                        <span class="notify-col">{{$category}}</span>
                                        @empty

                                        <p class="text-white">{{ __('NA')}}</p>
                                        @endforelse

                                    </div>
                                </div>
                                <div class="feature-skills">
                                    <p class="text-white d-inline-flex mr-2">{{ __(' Sub Categories') }}:</p>
                                    <div class="skils-cols d-inline-flex">

                                        @forelse($user->subcategories->pluck('name')->toArray() as $sub_category)
                                        <span class="notify-col">{{$sub_category}}</span>
                                        @empty

                                        <p class="text-white">{{ __('NA')}}</p>
                                        @endforelse

                                    </div>
                                </div>
                                <div class="feature-skills">
                                    <p class="text-white d-inline-flex mr-2">{{ __(' Skills') }}:</p>
                                    <div class="skils-cols d-inline-flex">

                                        @forelse($user->skills->pluck('name')->toArray() as $skill)
                                        <span class="notify-col">{{$skill}}</span>
                                        @empty

                                        <p class="text-white">{{ __('NA')}}</p>
                                        @endforelse

                                    </div>
                                </div>
                                @if($user->organization_type == 1 && $user->user_type == 1)
                                <p class="text-white">{{ __('Years of Experience') }}:<strong> {{ !empty($user->userDetail->years_of_experience) ? $user->userDetail->years_of_experience : __('NA') }}</strong></p>
                                @endif
                                @if(!empty($user->userDetail->about_me))
                                <div class="about-me-col">
                                <p class="text-white">{{ __('About Me') }}</p>
                                    <p class="text-white">{{ !empty($user->userDetail->about_me) ? $user->userDetail->about_me : __('NA') }}</p>
                                </div>
                                @endif
                                @if($user->organization_type == 0)
                                <p class="text-white">{{ __('Established Since') }}:<strong> {{ !empty($user->userDetail->established_since)?hcms_date(strtotime($user->userDetail->established_since),'date'):'NA' }}</strong></p>
                                @endif
                                @if(count($user->portfolio)>0)
                                <p class="text-white">{{ __('Portfolio of project/work') }}:<strong> 
                             
                                    @php
                                        $portfolio_list = implode(', ',$user->portfolio->pluck('portfolio_title')->toArray());
                                    @endphp
                                    {{ucfirst($portfolio_list)}} 
                                </strong></p>
                                @endif 
                                @if($user->organization_type == 0 && !empty($user->userDetail->company_tagline))
                                <p class="text-white">{{ __('Company Tagline') }}:<strong> {{ !empty($user->userDetail->company_tagline) ? $user->userDetail->company_tagline : __('NA') }}</strong></p>
                                @endif
                              

                                @if(!empty($user->userDetail->user_history))
                                <div class="about-me-col">
                                    @if($user->organization_type == 0)
                                    <p class="text-white">{{ __('Company Information') }}</p>
                                    @else
                                    <p class="text-white">{{ __('Background Information') }}</p>
                                    @endif
                                    <p class="text-white">{{ !empty($user->userDetail->user_history) ? $user->userDetail->user_history : __('NA') }}</p>
                                </div>
                                @endif
                                <div class="open-project-view">
                                    @if($user->user_type == 0)
                                        <p class="text-white">Click here to check my open projects</p>
                                        <a href="{{route('project.listOpenProjects')}}" class="text-white">{{str_limit(route('project.listOpenProjects'), $limit = 70, $end = '...')}}</a>
                                    @else
                                        <p class="text-white">Click here to check my project invitations</p>
                                        <a href="{{route('project.listInvitations')}}" class="text-white">{{str_limit(route('project.listInvitations'), $limit = 70, $end = '...')}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- <div class="bg-shadow basic-company-col">
              <h3 class="sub-headtitle">My Work</h3>
              <form>
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group disabled">
                      <label for="website-urls">Projects in progress</label>
                      <input type="text" class="form-control" id="website-urls" aria-describedby="website-urls" placeholder="4">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group disabled">
                      <label for="profile-headline">Projects Completed</label>
                      <input type="text" class="form-control" id="profile-headline" aria-describedby="profile-headline" placeholder="14">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group disabled">
                      <label for="profile-headline">Total Earnings</label>
                      <input type="text" class="form-control" id="profile-headline" aria-describedby="profile-headline" placeholder="$14000">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group disabled">
                      <label for="profile-headline">Member Since</label>
                      <input type="text" class="form-control" id="profile-headline" aria-describedby="profile-headline" placeholder="24-Aug-2019">
                    </div>
                  </div>
                </div> 
              </form>  
            </div> -->
                </div>
            </div>
        </div>
    </div>
</main>
<!-- <h1>My Public Profile Details</h1>  <a href="{{route('account.editPublicProfile')}}">Edit</a>
    <div class="row">
        <div class="col-lg-6">
            {{ __(' Photo/Logo') }} :  @if($user->profile_image && (file_exists( public_path().'/storage/user/profile-images/'.$user->profile_image)))
            <img src="{{ asset('storage/user/profile-images/'.$user->profile_image) }}" style="height: 100px; width:100px;" alt="{{ $user->full_name }}"
            class="rounded-circle img-thumbnail">
        @else 
            <img src="{{ asset('images/admin/starter/default.png') }}" style="height: 100px;" alt="{{ $user->full_name }}"
            class="rounded-circle img-thumbnail">
        @endif

        </div>
        <div class="col-lg-6">
            {{ __(' Profile Headline') }} : {{ !empty($user->userDetail->profile_headline) ? ucwords($user->userDetail->profile_headline) : __('NA') }}
            
        </div>
        <div class="col-lg-6">
            {{ __(' Industry') }} : {{ !empty($user->industries) ? implode(', ', $user->industries->pluck('name')->toArray()) : __('NA') }}</p>
            
        </div>
        <div class="col-lg-6">
            {{ __(' Categories') }} :{{ !empty($user->categories) ? implode(', ', $user->categories->pluck('name')->toArray()) : __('NA') }}</p>
            
        </div>
        <div class="col-lg-6">
            {{ __(' Subcategories') }} : {{ !empty($user->subCategories) ? implode(', ', $user->subCategories->pluck('name')->toArray()) : __('NA') }}</p>
            
        </div>
        <div class="col-lg-6">
            {{ __(' Portfolio of project/work') }} : {{ !empty($user->userDetail->website_url) ? $user->userDetail->website_url : __('NA') }}
            
        </div>
        <div class="col-lg-6">
            {{ __(' Company Tagline') }} : {{ !empty($user->userDetail->company_tagline) ? $user->userDetail->company_tagline : __('NA') }}
            
        </div>
        
      
        <div class="col-lg-6">
            {{ __('Company / Individual Background') }} : {{ !empty($user->userDetail->user_history) ? $user->userDetail->user_history : __('NA') }}
            
        </div>
        <div class="col-lg-6">
            {{ __('Corporate Presentation') }} : {{ !empty($user->userDetail->corporate_presentation) ? $user->userDetail->corporate_presentation : __('NA') }}
            
        </div>
        @if($user->organization_type == 1)
        <div class="col-lg-6">
            {{ __('Years of Experience') }} :  {{ !empty($user->userDetail->years_of_experience) ? $user->userDetail->years_of_experience : __('NA') }}</p>
            
        </div>
        @endif
        @if($user->organization_type == 0)
        <div class="col-lg-6">
            {{ __('Established Since') }} : {{ !empty($user->userDetail->established_since)?hcms_date(strtotime($user->userDetail->established_since),'date'):'NA' }}</p>
            
        </div>
        @endif
        
    </div> -->
@endsection
@push('custom-scripts')


@endpush