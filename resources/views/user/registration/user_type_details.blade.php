@extends('user.layouts.user_one')
@section('title', set_page_titile(__('Create Your Account')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')

<!--start: create account page -->

<div class="content-area">
    <div class="container">
        <div class="accounts-cols">
          <form  method="POST" id="user_type_form" action="{{route('user.userType')}}" >
            @csrf
            <h2>Create your Account</h2>
            <p class="desptn">This is a secure system and you will need to provide your login details to access the
                site.</p>
            <div class="selection-col">
                <p class="mb-3">I Want to:</p>
                <div class="custom-control custom-radio radio-cols">
                    <input type="radio" name="user_type" value="0" id="freelancer" class="custom-control-input">
                    <label class="custom-control-label freelancer-user" for="freelancer"> <img
                            src="{{asset('/images/user/user-freelancer.png')}}" alt="user-freelancer"
                            class="d-block mx-auto mb-3 mt-2"><span>Hire a Freelancer</span></label>
                </div>
                <div class="custom-control custom-radio radio-cols">
                    <input type="radio" name="user_type" value="1" id="hired" class="custom-control-input">
                    <label class="custom-control-label hired-user" for="hired"><img
                            src="{{asset('/images/user/user-hired.png')}}" alt="user-hired"
                            class="d-block mx-auto mb-3 mt-2"><span>Get Hired</span></label>
                </div>
            </div>
            <div class="selection-col">
                <p class="mb-3">Account Type:</p>
                <div class="custom-control custom-radio radio-cols">
                    <input type="radio" name="org_type" value="1" id="individual" class="custom-control-input">
                    <label class="custom-control-label individual-user" for="individual"><img
                            src="{{asset('/images/user/user-individual.png')}}" alt="user-individual"
                            class="d-block mx-auto mb-3 mt-2"><span>I am an Individual</span></label>
                </div>
                <div class="custom-control custom-radio radio-cols">
                    <input type="radio" id="company" name="org_type" value="0" class="custom-control-input">
                    <label class="custom-control-label company-user" for="company"><img
                            src="{{asset('/images/user/user-company.png')}}" alt="user-company"
                            class="d-block mx-auto mb-3 mt-2"><span>I am a Company</span></label>
                </div>
            </div>
            <div class="error-message">
            <span class="error-txt common-type-error"></span>
            </div>
            <div class="btn-cols">
                <button class="btn submit-btn log-reg-btn" id="reg_submit" type="submit">Proceed</button>
            </div>    
        </form>
        </div>
    </div>
</div>
<!--end: create account page -->
@endsection
@push('custom-scripts')


<script>
 $("#user_type_form").validate({
    onfocusout: function(element){
        $(element).valid();
        // $(element).closest("form-group + *").prev().find('input').valid;
   	
    },
    onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        // onfocusin: function(element){
        // $(element).removeClass('error');
        // $(element).next().text('');
        // },
    rules: {
        user_type:  {
            required: true,
        },
        org_type: {
            required: true,
        },
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        }
        else if (element.attr("name") == "org_type" ) {
          $(".common-type-error").html(error); 
           // error.insertAfter(".user-type-error");
        } else if (element.attr("name") == "user_type" ) {
          $(".common-type-error").html(error);
        }
       
        else {
            error.insertAfter(".user-type-error");
        }
   },

   messages: {
            'org_type': {
                required: "You must check both user and account type",
            },
            'user_type': {
                required: "You must enable both user and account type",
            },
        }

});
</script>

@endpush