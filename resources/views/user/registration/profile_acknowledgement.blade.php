
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Background Check')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush

@section('content')

<!--start: contact information page -->

<main class="main-wrapper">
    <div class="container inner-pg-layout">
      <div class="row mx-0"> 
          <div class="col-lg-3 left-aside">
          
              <ul class="d-block">
      
                  <li class="active"><a href="{{route('user.viewContactInformation')}}"><i class="fi-flaticon flaticon-contact"></i>Contact Information</a></li>
                  <li class="active"><a href="{{route('user.viewSecurityQuestions')}}"><i class="fi-flaticon flaticon-shield"></i>Security Questions</a></li>
                  <li class="disabled"><a href="#"><i class="fi-flaticon flaticon-clipboard"></i>Background Verification</a></li>
                  <li class="disabled"><a href="#"><i class="fi-flaticon flaticon-premium"></i> Membership Plans</a></li>
                  <li class="disabled"><a href="#"><i class="fi-flaticon flaticon-user-shape"></i>Public Profile Details</a></li>
              </ul>
          </div>
          <div class="col-lg-9">
              <div class="right-aside">
              <div class="card-top mb-4">
                <h4>Background Check</h4>
                <p class="mb-0">Review the following Disclosure</p>  
              </div>           
              <div class="card-content">
              <form   id="background_form"  >
              @csrf
                 <div class="para-col">
                    <p class="text-justify">Thank you for providing the basic profile details</p>
                    <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lacinia augue a bibendum rutrum. Donec ullamcorper fermentum tortor eu tincidunt. Vestibulum mattis diam non lacus pulvinar, ut laoreet nunc rhoncus. Interdum et malesuada fames ac ante ipsum primis in faucibus. </p>
                    <p class="text-justify">Nam imperdiet tristique molestie. Aenean commodo molestie lectus in euismod. Nam at nulla neque. Maecenas bibendum nibh lacinia risus fringilla, at blandit ipsum consectetur. Donec sapien est, iaculis et pharetra quis, accumsan sed sapien. Phasellus ultrices orci a lorem finibus, a consectetur turpis placerat. Nulla faucibus sapien nisi, non eleifend sapien porta eget. Sed in hendrerit risus. Nulla eu eleifend sapien. Quisque pellentesque ac lorem quis accumsan. Donec neque sapien, sodales vitae lectus nec, ornare pulvinar turpis. Pellentesque dictum ex in accumsan tristique.</p>
                    <p class="text-justify">Maecenas ligula felis, tempus luctus elit ut, egestas rutrum ligula. In hac habitasse platea dictumst. Nulla facilisi. Pellentesque vel fermentum ante. Nunc at mauris condimentum, iaculis tellus quis, venenatis tellus. In ullamcorper ultrices tortor, ut tempor arcu convallis vitae. Suspendisse lectus turpis, lobortis ut mattis a, efficitur eget tortor. Proin lobortis egestas est, vel aliquam risus euismod eu. Donec pulvinar semper ullamcorper. Sed quis laoreet lectus. Ut condimentum varius nisi, vitae ullamcorper nisi hendrerit ut.</p>
                  </div>
                 
                  <div class="check-cols">
                  <div class="form-group">
                    <div class="custom-control custom-checkbox mb-3">
                      <input type="checkbox" name="agreeBackground" class="custom-control-input" id="backVerification" checked>
                      <label class="custom-control-label" for="backVerification"> I agree to the <a href="{{route('userPublicProfile','background-verification')}}" target="_blank">Background  Verification</a> carried out by <strong> Keyoxa </strong></label>
                      <span class="error-txt background-error"></span> 
                    </div>
                  </div>
                  </div>
                  <p class="notice text-center text-lg-left">A one time charge {{ formatAmount($back_ground_charge) }} will be debited by "<strong> Keyoxa </strong>" for verifying the address and the background details provided.</p>
                  <div class="btn-cols text-center mb-2" id="btn-pay">
                     {{-- <script
                    
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="{{$payment_keys->authentication_key}}"
                    data-amount="{{$back_ground_charge*100 }}"
                    data-name="Background verfication Payment"
                    data-description="You need pay for the background verification"
                    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                    data-locale="auto"
                    data-zip-code="false">
                  </script>
                  <script>
                    // Hide default stripe button, be careful there if you
                    // have more than 1 button of that class
                    document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
                </script>  --}}
                    
                     <button class="btn submit-btn pay-submit" id="checkout-button" >Pay & Submit</button>
                     
                  </div>                

              </form>   
			  {{-- <button id="checkout-button">Checkout</button> --}}

              </div>
          </div>
      </div>
      </div>
    </div>
  </main>
  
  <!--end: contact information page -->

@endsection

@push('custom-scripts')
<script src="https://js.stripe.com/v3/"></script>

	 <script>
	var key = '{{$payment_keys->authentication_key}}';
	var stripe = window.Stripe(key);
	
	
	var checkoutButton = document.getElementById('checkout-button');

	checkoutButton.addEventListener('click', function() {
	stripe.redirectToCheckout({
		
		sessionId: '{{$checkout_session->id}}'
	}).then(function(result) {
         
          if (result.error) {
            alert(result.error.message);
          }
        })
        .catch(function(error) {
          console.error('Error:', error);
        });
	});
</script> 
	<script>
 $('#backVerification').click(function(){
  	if($(this).prop("checked") == true){
        $('#btn-pay').show();
		$(".background-error").html("");
    }
    else if($(this).prop("checked") == false){
        $('#btn-pay').hide();
		var error = "You must agree the Background Verification";
		$(".background-error").html(error);
    }
 });
  
 

</script>

@endpush