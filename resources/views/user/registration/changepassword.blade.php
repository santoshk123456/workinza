@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Create Public Profile')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:type" content="website" />
<meta property="og:image" content=" " />
<meta property="og:image:url" content="" />
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
<style> 
.selection__choice { font-size:3px!important; }
/* .select_join option{
    font-size:13px;
} */
</style>
@endpush
@section('content')


<main class="main-wrapper">
    <div class="container">
      <div class="inner-pg-layout">
          <div class="row">
            <div class="col-md-6">
                {{-- <h3 class="title-main">My Profile</h3> --}}
            </div>
            @if(auth('web')->user()->step >= 6)
            <div class="col-md-6 text-right">
              <button class="btn cancel-shadow-btn" onclick="location.href='{{route('account.myProfile',auth('web')->user()->id)}}'" type="button">Back to Profile</button> 
            </div>
            @endif
          </div>
        <div class="accounts-cols">
            <h2>Change Password</h2>  

            <form id="change-password-form" action="{{ route('user.user.change_password') }}" method="POST" class=" @if(!$errors->isEmpty()) after-form-submit @endif">
        @csrf
        <div class="change-pass-col text-left"> 
        <div class="form-group required @if($errors->has('current_password')) validation-failed @endif">
            <label for="emailaddress" class="control-label">{{ __('Current Password') }}
            
            </label>
            <input class="form-control" name="current_password" type="password" value="{{ old('current_password') }}" id="current_password" required placeholder="Enter your current password">
            
            <span class="error-txt current_password-error"></span>
            @if ($errors->has('current_password'))
                <div class="invalid-feedback" >
                    {{ $errors->first('current_password') }}
                </div>
            @endif
        </div>
        <div class="form-group required @if($errors->has('password')) validation-failed @endif">
            <label for="password" class="control-label">{{ __('New Password') }}
                
            </label>
            <input class="form-control" value="{{ old('password') }}" data-rule-nospace="true"   data-rule-checkcapital="true"   data-rule-checknumber="true"  data-rule-checkspecial="true" name="password" type="password" required id="password" placeholder="Enter your new password">
            <span class="error-txt password-error"></span>
            @if ($errors->has('password'))
                <div class="invalid-feedback" >
                    {{ $errors->first('password') }}
                </div>
            @endif
        </div>
        <div class="form-group required @if($errors->has('password_confirmation')) validation-failed @endif">
            <label for="password" class="control-label">{{ __('Confirm Password') }}
                
            </label>
            <input class="form-control" value="{{ old('password_confirmation') }}" data-rule-nospace="true"   data-rule-checkcapital="true"   data-rule-checknumber="true"  data-rule-checkspecial="true" name="password_confirmation" type="password" required id="password_confirmation" placeholder="Confirm your new password">
            <span class="error-txt password_confirmation-error"></span>
            @if ($errors->has('password_confirmation'))
                <div class="invalid-feedback" >
                    {{ $errors->first('password_confirmation') }}
                </div>
            @endif
        </div>

        <div class="btn-cols text-right mb-2">
                <button type="reset" class="btn cancel-btn mr-2" >Cancel</button>
                <button class="btn submit-btn" type="submit">Change Password</button>
              </div>   

    
        </div>
    </form>




        </div>
      </div>
    </div>
</main>




@endsection
@push('custom-scripts')
{{ script('js/admin/summernote-bs4.min.js') }}
{{ script('js/admin/demo.summernote.js') }}
<script> 

$("#change-password-form").validate({
  
  onfocusout: function(element){
  

    if ($(element).attr("name") == "email" ){
      var usr_name = $(".email").val();
      console.log(usr_name);
      $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
      });
      $.ajax({
              type: "POST",
              url: "{{route('emailExist')}}",
              data:{ 
                  '_token': "{{ csrf_token() }}",
                  'email':usr_name
                   },
              success: function (data) { 
                  if((data['email_exist']) == 1){
                       $(".email-error").text('Email id already exists');
                  }else{
                    // $(".email-error").text(data['msg']);
                  }
              }
      });
  }
      $(element).valid();
      // $(element).closest("form-group + *").prev().find('input').valid;
     
  },
  onkeyup: function(element){
    $(element).next().text('');
    
     
  },
  rules: {
      password:  {
          required: true,
          minlength: 7,
         
      },
   
      password_confirmation:  {
          required: true,
          minlength: 7,
          
      },
       
      password_confirmation:  {
          required: true,
          minlength: 7,
          equalTo : "#password"
         
      },
  },

  errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
          $(placement).html(error);
      } 
      else if (element.attr("name") == "current_password" ) {
        $(".current_password-error").html(error); 
      }
      else if (element.attr("name") == "password" ) {
        $(".password-error").html(error); 
      }
      else if (element.attr("name") == "password_confirmation" ) {
        $(".password_confirmation-error").html(error); 
      }
      else {
          error.insertAfter(element);
      }
 },

 messages: {
   
    'current_password': {
              required: "Current Password is required.",
          },
          'password': {
              required: "Password is required.",
          },
          'password_confirmation': {
              required: "Confirm Password is required.",
              equalTo : "Password mismatch"
          },
      }

});

jQuery.validator.addMethod("checkspecial", function(value, element) {
  var pass = new RegExp("^(?=.*[!@#\$%\^&\*])")
 return this.optional(element) || (pass.test(value));
}, "* password must contain one special character");
jQuery.validator.addMethod("checkcapital", function (value, element) {
  var pass = new RegExp("^(?=.*[A-Za-z])")
  return this.optional(element) || (pass.test(value));
}, "Password must contain atleast one character");
jQuery.validator.addMethod("nospace", function (value, element) {
  return value.indexOf(" ") < 0 && value != "";
}, "No space please and don't leave it empty");
jQuery.validator.addMethod("checknumber", function (value, element) {
  var pass = new RegExp("^(?=.*[0-9])")
  return this.optional(element) || (pass.test(value));
}, "Password must contain  one number");
</script>

<script>

$('.pass-icons').click(function(e){
  $('#pass_icon').hasClass('flaticon-eye-1')?hidePassword():showPassword()
})
function hidePassword(){
  $('#pass_icon').removeClass('flaticon-eye-1').addClass('flaticon-visibility')
  $('#password').attr('type','text')
}
function showPassword(){
  $('#pass_icon').removeClass('flaticon-visibility').addClass('flaticon-eye-1')
  $('#password').attr('type','password')
}

</script>



@endpush