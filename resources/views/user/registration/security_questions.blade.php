
@extends('user.layouts.user_two')
@section('title', set_page_titile(__('Security Questions')))

@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
<style>
  .error {
  color: #F00;
  background-color: #FFF;
}

.questionborder:last-of-type{ border-bottom:0 !important; }

</style>
@endpush

@section('content')
<!--start: contact information page -->

<main class="main-wrapper">
  <div class="container inner-pg-layout">
    <div class="row mx-0">
      @include('user.layouts.includes.profile_tab')

        <div class="col-lg-9">
              <div class="right-aside">
              <div class="card-top mb-4">
                <h4>Security Questions</h4>
                <p class="mb-0">Security questions will help protect your account from unauthorized use and these answers will help us to identify its you</p>  
              </div>           
              <div class="card-content">
              <form method="post" id="security-questions-form" action="{{route('user.storeSecurityQuestions')}}">
              @csrf  
              @if(!$security_questions->isEmpty())
              @foreach($security_questions as $key=>$question)


                    <div class="form-row border-bottom questionborder pb-3 mb-3">
                        <div class="col-md-12 mb-lg-2">
                        <div class="form-group mb-0 required">
                            <label  for="quesans_{{$key}}" class="control-label">{{ $question->question }}</label> 
                            <input type="text" class="form-control col-md-6" name="answers[{{$question->id}}]" id="quesans_{{$key}}" placeholder="Anwser">                        
                    
                          </div>
                        </div>
                    </div>


              @endforeach
              @else
              <div class="form-row border-bottom">
                        <div class="col-md-6 mb-lg-2">
                        <div class="form-group required">
                        <div class="center">
                  <i><p> {{__('No security questions added.')}} </p></i>
                  </div>
                       
                          </div>
                        </div>
                    </div>
              @endif
         
                  <div class="btn-cols text-right mb-2">
                    @if(auth()->user()->step < 3)
                      <button class="btn cancel-btn mr-2" onclick="location.href='{{route('user.profileAcknowledgement')}}'"   type="button">Skip</button>
                    @else
                      <button class="btn cancel-btn mr-2 reset" type="button" onclick="location.href='{{ route('user.redirectBack') }}'">Back</button>
                    @endif
                    <button class="btn submit-btn" type="submit">Continue</button>
                  </div>                
                </form>   
              </div>
          </div>
      </div>






    </div>
  </div>
</main>






@endsection
@push('custom-scripts')

<script>
$(document).ready(function () {

$('#security-questions-form').validate({
    // options, etc.
});

$('input[id^="quesans"]').each(function () {
    $(this).rules('add', {
        required: true,
        // another rule, etc.
    });
});

});


</script>

@endpush
