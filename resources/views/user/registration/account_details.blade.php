@extends('user.layouts.user_one')
@section('title', set_page_titile(__('Create Your Account')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endpush
@section('content')

<!--start: create account page -->

<div class="content-area">
    <div class="container">
        <div class="accounts-cols">
            <h2>Create your Account</h2>
            <p class="desptn">This is a secure system and you will need to provide your login details to access the site.</p>
           
            <form method="post" id="register_account_form" action="{{route('user.store')}}">
            @csrf
                <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                              <span class="input-group-text pr-0 bg-white" id="basic-addon1"><i class="fi-flaticon flaticon-user-shape"></i></span>
                            </div>
                           
                            <input type="text" name="full_name" id="full_name" value="@if($user !="")  {{$user->full_name}} @elseif(old('full_name')) {{old('full_name')}} @endif" class="form-control border-left-0 txtOnly" placeholder="Full Name" aria-label="Full Name" minlength="3" maxlength="50" aria-describedby="basic-addon1">
                            <span class="error-txt full-name-error"></span>
                          </div>
                          </div>
                    </div>
                    <input type="hidden" name="user_type" value="{{ $user_type }}">
                    @if(isset($freelancer_details) && $freelancer_details != "")
                    <input type="hidden" name="referral_freelancer" value="{{ $freelancer_details->user_id }}">
                    @endif
                    <input type="hidden" id="org_type" name="organization_type" value="{{ $organization_type }}">
                    @if(isset($invited_client) && $invited_client != "")
                    <input type="hidden" id="invited_client" name="invited_client" value="{{ $invited_client }}">
                    @endif
                    <input type="hidden" name="user" value="@if($user){{ $user->uuid }}@endif">
                    <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                              <span class="input-group-text pr-0 bg-white" id="basic-addon3"><i class="fi-flaticon flaticon-email"></i></span>
                            </div>
                            <input type="email" id="register-email"  name="email" id="email" maxlength="50" value="@if($user){{$user->email}} @else {{old('email')}} @endif" class="form-control border-left-0 email" @if(empty($organization_type)) placeholder="Company Email" @else placeholder="Email" @endif aria-label="Email" aria-describedby="basic-addon3">
                            <span class="invalid-feedback error-txt email-error">
                              @if ($errors->has('email'))
                              {{ $errors->first('email') }}
                              @endif
                            </span>        
                          </div>
                          </div>
                          
                    </div>

                    @if(empty($organization_type))
                    
                    <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                              <span class="input-group-text pr-0 bg-white" id="basic-addon4"><i class="fi-flaticon flaticon-edit"></i></span>
                            </div>
                            <input type="text" id="company_name" name="company_name" maxlength="50" value="@if(isset($user->userDetail) && $user->userDetail){{$user->userDetail->company_name}} @elseif(old('company_name')) {{old('company_name')}}  @endif" class="form-control border-left-0" placeholder="Company Registered Name" aria-label="Company Registered Name" aria-describedby="basic-addon4">
                          <span class="error-txt company-name-error"></span>
                          </div>
                      </div>
                    </div>

                    @endif

                    <div class="col-md-12">
                    <div class="form-group">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                              <span class="input-group-text pr-0 bg-white" id="basic-addon5"><i class="fi-flaticon flaticon-locked-padlock"></i></span>
                            </div>
                            <input type="password" name="password" id="password" minlength="7" class="form-control border-left-0 border-right-0" placeholder="Create A Password" 
                            data-rule-nospace="true"   data-rule-checkcapital="true"   data-rule-checknumber="true"  data-rule-checkspecial="true" 
                            aria-label="Create A Password" aria-describedby="basic-addon5">
                            <span class="error-txt password-error"></span>
                            <div class="input-group-append">
                                <button class="btn input-group-text bg-white border-left-0 pass-icons" type="button"><i id="pass_icon" class="fi-flaticon flaticon-eye-1"></i></button>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="check-cols col-md-12 py-3">
                      <div class="form-group">
                        <div class="custom-control custom-checkbox  text-left">
                          <input type="checkbox" name="agree_terms" class="custom-control-input" id="agree_terms">
                          <span class="error-txt agree-terms-error"></span>             
                          <label class="custom-control-label" for="agree_terms">I agree on the  <a href="{{route('userPublicProfile','terms-and-conditions')}}" target="_blank"> Terms & Conditions</a> and <a target="_blank" href="{{route('userPublicProfile','privacy-policy')}}"> Privacy Policy</a></label>
                        </div>  
                        
                      </div>
                      <div class="g-recaptcha"  data-callback="recaptchaCallback" for="g-recaptcha-response" data-sitekey="{{$site_key}}"></div>
                      @if ($errors->has('g-recaptcha-response'))
                      <span class="error-txt agree-terms-error">{{ __('Captcha Invalid.') }}</span>
                     
                      @endif
                    </div>
                  
                     
                      

                    
                </div>
         
                <div class="btn-cols mb-2">                
                    <button class="btn log-reg-btn" type="submit">Create My Account</button>
                </div> 
            </form>
        </div>
    </div>
</div>
<!--end: create account page -->

@endsection
@push('custom-scripts')
<script>
    $(document).ready(function () {
        $(".txtOnly").keypress(function (event) {
            var key = event.keyCode;
            return ((key >= 65 && key <= 90) || (key >= 97 && key <= 122) || key == 8 || key == 32);
        });
        
    });
    function recaptchaCallback() {
      //$(".g-recaptcha").remove();
      $('.g-recaptcha').find('span').remove()
    };
</script>

<script>
  
  $(document).on('submit',"#register_account_form",function(e){
  
  var response = grecaptcha.getResponse(); 
  if(response.length == 0) { 
    $(".g-recaptcha").append("<span class='error-txt agree-terms-error'>Please verify you are human</span>");
     e.preventDefault(); return false;
  }else{
    $(".g-recaptcha").remove('span');
  }

 });
  $('#email').on('blur', function(){
        var usr_name = $("#email").val();
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('emailExist')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'username':usr_name
                     },
                success: function (data) { 
                    if((data['email_exist']) == 1){
                        // user name already exist message
                         $(".email-error").text('Email id already exists');
                    }else{
                         $(".email-error").text(data['msg']);
                    }
                }
        });
    });

 $("#register_account_form").validate({
   
    onfocusout: function(element){
     

      if ($(element).attr("name") == "email" ){
        var usr_name = $(".email").val();
       
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('emailExist')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'email':usr_name
                     },
                success: function (data) { 
                    if((data['email_exist']) == 1){
                         $(".email-error").text('Email id already exists');
                    }else{
                      // $(".email-error").text(data['msg']);
                    }
                }
        });
    }
        $(element).valid();
        // $(element).closest("form-group + *").prev().find('input').valid;
   	
    },
    onkeyup: function(element){
        $(element).next().text('');
        $(element).removeClass('error');
   	
        },
        // onfocusin: function(element){
        // $(element).removeClass('error');
        // $(element).next().text('');
        // },
    rules: {
        full_name:  {
            required: true,
            minlength: 3,
            maxlength: 50,
            fullname: true,
            normalizer: function(value) {
                return $.trim(value);
            },
        },
        email: {
            required: true,
            minlength: 3,
            maxlength: 50,
            email: true,
          
        },
        password:  {
            required: true,
            minlength: 7,
           // maxlength: 12,
        },
        company_name: {
            required: function(element){
            return $("#org_type").val() == 0;
            },
            minlength: 3,
            maxlength: 50,
            normalizer: function(value) {
                return $.trim(value);
            },  
        },
        agree_terms:  {
            required: true,
        },
       
       
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } else if (element.attr("name") == "full_name" ) {
          $(".full-name-error").html(error);
        }
        else if (element.attr("name") == "email" ) {
          $(".email-error").html(error); 
        }
        else if (element.attr("name") == "password" ) {
          $(".password-error").html(error); 
        }
        else if (element.attr("name") == "company_name" ) {
          $(".company-name-error").html(error); 
        }
        else if (element.attr("name") == "agree_terms" ) {
          $(".agree-terms-error").html(error); 
        }
        else {
            error.insertAfter(element);
        }
   },

   messages: {
           'agree_terms': {
                required: "You must agree the Terms & Conditions and Privacy Policy",
            },
            'full_name': {
                required: "Full name is required.",
            },
            'email': {
                required: "Email is required.",
            },
            'password': {
                required: "Password is required.",
            },
            'company_name': {
                required: "Company name is required.",
            },
        }

});
jQuery.validator.addMethod("fullname", function(value, element) {
  if (/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/.test(value)) {
    return true;
  } else {
    return false;
  };
}, 'Please enter your full name. ( Eg: Thomas Edison)');
 jQuery.validator.addMethod("checkspecial", function(value, element) {
    var pass = new RegExp("^(?=.*[!@#\$%\^&\*])")
   return this.optional(element) || (pass.test(value));
}, "* password must contain one special character");
jQuery.validator.addMethod("checkcapital", function (value, element) {
    var pass = new RegExp("^(?=.*[A-Za-z])")
    return this.optional(element) || (pass.test(value));
}, "Password must contain atleast one character");
jQuery.validator.addMethod("nospace", function (value, element) {
    return value.indexOf(" ") < 0 && value != "";
}, "No space please and don't leave it empty");
jQuery.validator.addMethod("checknumber", function (value, element) {
    var pass = new RegExp("^(?=.*[0-9])")
    return this.optional(element) || (pass.test(value));
}, "Password must contain  one number");
</script>

<script>

$('.pass-icons').click(function(e){
    $('#pass_icon').hasClass('flaticon-eye-1')?hidePassword():showPassword()
})
function hidePassword(){
    $('#pass_icon').removeClass('flaticon-eye-1').addClass('flaticon-visibility')
    $('#password').attr('type','text')
}
function showPassword(){
    $('#pass_icon').removeClass('flaticon-visibility').addClass('flaticon-eye-1')
    $('#password').attr('type','password')
}

</script>


@endpush
