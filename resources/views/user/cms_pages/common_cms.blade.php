@extends('user.layouts.homelayout')
<?php
$title=ucwords($cms_content->heading);
?>
@section('title', set_page_titile(__("$title")))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
<style> 

.banner-about-us {
    background-image: url(/images/user/bg-about-us-banner.jpg);
    background-size: cover;
    background-position: 50%;
}
</style>
@endpush
@section('content') 

<body itemscope="" itemtype="http://schema.org/WebPage">





<div class="container">
    <div class="about-us-content-wrapper">
 
      
        <div class="content-block">
       
            <h2 class="title">{{ $cms_content->heading }}</h2>
       
            <p class="text text-left">   @if(!empty($cms_content->content))
                    {!! $cms_content->content !!}
                @else
                <p>Content not available</p>
                @endif</p>
                <div class="content-block-seperator"><i class="fi-flaticon flaticon-star"></i></div>
        </div>
    </div>
</div>




@endsection

@push('custom-scripts')

@endpush
