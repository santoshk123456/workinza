@extends('user.layouts.homelayout')
@section('title', set_page_titile(__('Contact-us')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content') 

<body itemscope="" itemtype="http://schema.org/WebPage">

<!-- banner section: start -->
<section class="banner-contact-us">
    <div class="container">
        <div class="content text-center">
                <h2 class="title">Contact Us</h2>
        </div>
    </div>
</section>
<!-- banner section: end -->

<div class="container">
<div class="row contact-us-row">
    <div class="col-md-7">
        <div class="get-in-touch mb-4">
            <h2 class="title">Get In Touch with Us</h2>
            <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur accumsan augue non convallis molestie.</p>
            <div class="contact-form">

            <form id="feedback_form"  method="POST" enctype="multipart/form-data" class="php-email-form intl-form" action="{{ route('send_email_notification_contact')}}"
                    class="needs-validation @if(!$errors->isEmpty()) after-form-submit @endif" novalidate>
                    @csrf

              
                <div class="row mb-md-3">
                    <div class="col-md-2">
                        <label for="name" class="control-label">Name<span class="mand">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group required">
                            <!-- <input type="text" name="name" class="form-control" id="name" placeholder="Your name" required> -->
                            
                            <input type="text" name="full_name" id="full_name" value="{{old('full_name')}}" class="form-control txtOnly" placeholder="Full Name" aria-label="Full Name" minlength="3" maxlength="50" aria-describedby="basic-addon1">
                            <span class="error-txt full-name-error"></span>
                            <span class="error-txt name-error"></span>
                        </div>
                    </div>
                </div>
                <div class="row mb-md-3">
                    <div class="col-md-2">
                        <label for="email" class="control-label">Email<span class="mand">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group required">
                            <input type="email" name="email" data-rule-domain="true" class="form-control" id="email" placeholder="Your email" required>
                            <span class="invalid-feedback error-txt email-error">
                        </div>
                    </div>
                </div>
                <div class="row mb-md-3">
                    <div class="col-md-2">
                        <label for="phone" class="control-label">Phone<span class="mand">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group required">
                        <input type="hidden" name="country_code" id="country_code" value="{{old('country_code',1)}}">
                            <input type="text" name="phone" id="phone" class="form-control numericOnly intl-input" placeholder="Mobile number" required>  
                            <span class="error-txt phone-error"></span>
                        </div>
                    </div>
                </div>


                
                <div class="row mb-md-3">
                    <div class="col-md-2">
                        <label for="company" class="control-label">Company</label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group ">
                            <input type="text" name="company" class="form-control" id="company" placeholder="Your company" >
                            <span class="error-txt company-error"></span>
                        </div>
                    </div>
                </div>
                <div class="row mb-md-3">
                    <div class="col-md-2">
                        <label for="message" class="control-label">Message<span class="mand">*</span></label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group required">
                            <textarea class="form-control" name="message" id="message" rows="3" placeholder="Your Message" required></textarea>
                            <span class="error-txt message-error"></span>
                        </div>
                    </div>
                </div>
                <div class="row mb-md-3">
                    <div class="col-12 text-right">
                        <button type="submit" class="bg-nm-btn">Send Message</button>
                    </div>
                </div>
             </form>
            </div>
        </div>
    </div>
    <div class="col-md-5">
                <div class="benifits-with-us-contact">
                    <div class="sub-head-with-bg">
                        <h2 class="title">Benifits with Us</h2>
                    </div>
                <div class="items">
                    <div class="media">
                        <div class="icon icon-01"></div>
                        <div class="media-body">
                        <h5 class="mt-0">Verified Profile</h5>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever</p>
                        </div>
                    </div>
                    <div class="media">
                        <div class="icon icon-02"></div>
                        <div class="media-body">
                        <h5 class="mt-0">Post Project or Get Hired</h5>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever</p>
                        </div>
                    </div>
                    <div class="media">
                        <div class="icon icon-03"></div>
                        <div class="media-body">
                        <h5 class="mt-0">Manage projects through contracts and milestones</h5>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever</p>
                        </div>
                    </div>
                    <div class="media">
                        <div class="icon icon-04"></div>
                        <div class="media-body">
                        <h5 class="mt-0">Secure Payments</h5>
                        <p>Lorem Ipsum has been the industry's standard dummy text ever</p>
                        </div>
                    </div>
                </div>
                </div>

                <div class="contact-details-aside mt-4">
                    <div class="sub-head-with-bg">
                        <h2 class="title">Contact Deatils</h2>
                    </div>
                    <div class="contacts">
                        <p><i class="fi-flaticon flaticon-phone-call mr-3"></i><a href="tel:{{getSettingValue('contact_number')}}">{{getSettingValue('contact_number') }}</a></p>
                        <p><i class="fi-flaticon flaticon-email mr-3"></i><a href="mailto:{{getSettingValue('application_admin_email')}} ">{{ getSettingValue('application_admin_email')  }}</a></p>
                    </div>
                    <div class="social">
                        <h2 class="title">Social</h2>
                        <div class="icon-group">
                        <a target="blank" href="{{ getSettingValue('facebook_url') }}"><div class="icon icon-01"></div></a>
                        <a  target="blank" href="{{ getSettingValue('twitter_url') }}"><div class="icon icon-02"></div></a>
                        <a  target="blank" href="{{ getSettingValue('linkedin_url')}}"><div class="icon icon-03"></div></a>
                        <a  target="blank" href="{{ getSettingValue('pinterest_url')}}"><div class="icon icon-04"></div></a>
                        </div>
                    </div>
                </div>
    </div>
</div>
</div>




@endsection

@push('custom-scripts')
<script>
  window.i = 0;
   $(document).ready(function(){

    var phone = document.querySelector("#phone");


    var phone_iti = window.intlTelInput(phone,({

        geoIpLookup: function(callback) {
            $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                var countryCode = "in";//(resp && resp.country) ? resp.country : "in";
                callback(countryCode);
            });
        },
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.1.0/js/utils.js",
        separateDialCode: true,
        hiddenInput: "full_number",
        
        autoPlaceholder: "polite",
        initialCountry: "us",
        formatOnDisplay: false,
        customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
            return selectedCountryPlaceholder.replace(/[0-9]/g,"x");
        }, 
    }));
   
    phone.addEventListener("countrychange", function() {
        
        $('#country_code').val(phone_iti.getSelectedCountryData().dialCode);
        if(window.i == 0){
          $('.flag-container').first().remove();
        }
        
       window.i = window.i + 1;
    });
  });
</script>


<script> 
 


 $("#feedback_form").validate({
  
  onfocusout: function(element){
   


      $(element).valid();
      // $(element).closest("form-group + *").prev().find('input').valid;
     
  },
  onkeyup: function(element){
      $(element).next().text('');
      $(element).removeClass('error');
     
      },
      // onfocusin: function(element){
      // $(element).removeClass('error');
      // $(element).next().text('');
      // },
  rules: {
    full_name:  {
            required: true,
            minlength: 3,
            maxlength: 50,
            fullname: true,
            normalizer: function(value) {
                return $.trim(value);
            },
        },
      name:  {
          required: true,
          minlength: 3,
          maxlength: 50,
          name: true,
          normalizer: function(value) {
              return $.trim(value);
          },
      },
      email: {
          required: true,
          minlength: 3,
          maxlength: 50,
          email: true,
        
      },
      phone:  {
          required: true,
          minlength: 7,
          maxlength: 12,
      },
      company: {
      
          minlength: 3,
          maxlength: 50,
         
      },
      message:  {
          required: true,
      },
     
  },

  errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
          $(placement).html(error);
      }else if (element.attr("name") == "full_name" ) {
          $(".full-name-error").html(error);
        } else if (element.attr("name") == "name" ) {
        $(".name-error").html(error);
      }
      else if (element.attr("name") == "email" ) {
        $(".email-error").html(error); 
      }
      else if (element.attr("name") == "phone" ) {
        $(".phone-error").html(error); 
      }
      else if (element.attr("name") == "company" ) {
        $(".company-error").html(error); 
      }
      else if (element.attr("name") == "message" ) {
        $(".message-error").html(error); 
      }
      else {
          error.insertAfter(element);
      }
 },

 messages: {
         'message': {
              required: "Message is required",
          },
          'full_name': {
                required: "Full name is required.",
            },
          'name': {
              required: "Full name is required.",
          },
          'email': {
              required: "Email is required.",
          },
          'phone': {
              required: "Phone is required.",
          },
         
      }

});
jQuery.validator.addMethod("fullname", function(value, element) {
  if (/^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)/.test(value)) {
    return true;
  } else {
    return false;
  };
}, 'Please enter your full name. ( Eg: Thomas Edison)');



jQuery.validator.addMethod("domain", function(value, element) 
{
	return this.optional(element) || validateEmail(value);
}, 
    "Email is not valid"
);


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
</script>
@endpush
