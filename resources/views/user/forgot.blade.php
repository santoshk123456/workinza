@extends('user.layouts.user_one')
@section('title', set_page_titile(__('Login')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content') 



<!--start: reset password page -->

<div class="content-area">
    <div class="container">
        <div class="login-signup-cols accounts-cols">
          <div class="login-col login-register-page">   
  				<!-- Welcome Text -->
				  <div class="welcome-text">
					<h2>Reset your password</h2>
					<p class="desptn">Please provide your email address, a confirmation to change password will be sent to this email</p>
				</div>
				<!-- Form -->
			<form class="" id="forgot_form" method="POST" action="{{route('resetPassword')}}">
			  @csrf
			  <div class="form-group">  
				<div class="input-with-icon-left">
					<i class="icon-material-baseline-mail-outline"></i>
					<input type="email" class="input-text with-border form-control" name="email" id="email" placeholder="Email Address" required/>
					<span class="invalid-feedback error-txt email-error">
						@if ($errors->has('email'))
						{{ $errors->first('email') }}
						@endif
					</span>   
				</div>
			</div>
				<a href="{{route('showLogin')}}"  class="forgot-password mb-3 d-block">Back to login</a>				

				<!-- Button -->
		         <button class="button full-width button-sliding-icon ripple-effect margin-top-10 log-reg-btn w-100" type="submit">Submit <i class="icon-material-outline-arrow-right-alt"></i></button>
                </form>
          </div>
		</div>        
    </div>
</div>
<!--end: reset password page -->

@endsection
@push('custom-scripts')
<script>


 $("#forgot_form").validate({
    onfocusout: function(element){

        $('#email').on('blur', function(){
        var usr_name = $("#email").val();
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                type: "POST",
                url: "{{route('emailExistcheck')}}",
                data:{ 
                    '_token': "{{ csrf_token() }}",
                    'username':usr_name
                     },
                success: function (data) { 
                    if((data['email_exist']) == 1){
                        // user name already exist message
                         $(".email-error").text('Email is not existing.');
                    }
                }
        });
    });




        $(element).valid();
   	
    },
    rules: {
        email: {
            required: true,
            maxlength: 50,
            email: true,
        },
    },

    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).html(error);
        } 
        else if (element.attr("name") == "email" ) {
          $(".email-error").html(error); 
        }
        else {
            error.insertAfter(element);
        }
   },

   messages: {
            'email': {
                required: "Email is required.",
            },
        }
});
</script>
@endpush

