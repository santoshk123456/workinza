@extends('user.layouts.homelayout')
@section('title', set_page_titile(__('Home')))
@push('meta-tags')
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:title" content="" />
<meta property="og:description" content=""/>
<meta property="og:type" content="website" />
<meta property="og:image" content=" "/>
<meta property="og:image:url" content=""/>
<meta property="og:image:width" content="1200" >
<meta property="og:image:height" content="630" >
@endpush
@section('content') 

<body itemscope="" itemtype="http://schema.org/WebPage">


<!-- banner section: start -->
<section class="banner-main-home">
    <div class="content-bg">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="left-aside">
                    <h1>Find & Hire Expert Freelancers</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae tincidunt ipsum, id fringilla ante. Maecenas id elementum ante. Pellentesque iaculis ante hendrerit dignissim pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae tincidunt ipsum, id fringilla ante. Maecenas id elementum ante. Pellentesque iaculis ante hendrerit dignissim pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae tincidunt ipsum, id fringilla ante. Maecenas id elementum ante. Pellentesque iaculis ante hendrerit dignissim pharetra.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae tincidunt ipsum, id fringilla ante. Maecenas id elementum ante. 
                    </p>
                    <button class="btn btn-primary btn-banner btn-why" onclick="location.href='{{route('userPublicProfile','how-it-works')}}'" type="button" >Why Us</button>
                    <button class="btn btn-primary btn-banner btn-post" onclick="location.href='{{route('showLogin')}}'" type="button">Post a Project</button>
                    </div>
                </div>
                <div class="col-md-4 hide-on-small">
                    <div class="vector-img">
                   
                        <img  src="{{asset('/images/user/vector-img.svg')}}" alt="vector-image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- banner section: end -->

<!-- after banner section: start -->
<section class="after-banner">
    <div class="container">
        <div class="holder">
            <div class="row">
                <div class="col-md-6">
                    <div class="content">
                        <h2> {{ str_limit(ucwords($the_talent_marketplace->heading), $limit = 40, $end = '...') }}</h2>
                        <p>{{ str_limit(ucwords($the_talent_marketplace->short_description), $limit = 100, $end = '...') }}</p>
                        <button class="btn btn-primary btn-learn btn-learn" onclick="location.href='{{route('userPublicProfile','the-talent-marketplace')}}'" type="button">Learn More</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content">
                        <h2>{{ str_limit(ucwords($pre_packaged_projects->heading), $limit = 40, $end = '...') }}</h2>
                        <p>{{ str_limit(ucwords($pre_packaged_projects->short_description), $limit = 100, $end = '...') }}</p>
                        <button class="btn btn-primary btn-learn btn-learn" onclick="location.href='{{route('userPublicProfile','pre-packaged-projects')}}'" type="button">Learn More</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- after banner section: end -->

<!-- why us section: start -->
<section class="how-it-works">
    <div class="container">
        <div class="section-title">
            <h2>How It Works</h2>
        </div>
        <div class="videoWrapper">
            <iframe width="560" height="349" src="{{ getSettingValue('home_page_video') }}" allowfullscreen></iframe>
        </div>
        <div class="row why-us-holder-row">
            <div class="col-md-6">
                <div class="why-us-left-aside">
                    <div class="section-title">
                        <h2>Why Us</h2>
                    </div>
                    <h3 class="sub-title">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h3>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="why-us-right-aside">
                    <div class="items">
                        <div class="media">
                            <div class="icon icon-01"></div>
                            <div class="media-body">
                              <h5 class="mt-0">Verified Profile</h5>
                              <p>Lorem Ipsum has been the industry's standard dummy text ever</p>
                            </div>
                          </div>
                          <div class="media">
                            <div class="icon icon-02"></div>
                            <div class="media-body">
                              <h5 class="mt-0">Post Project or Get Hired</h5>
                              <p>Lorem Ipsum has been the industry's standard dummy text ever</p>
                            </div>
                          </div>
                          <div class="media">
                            <div class="icon icon-03"></div>
                            <div class="media-body">
                              <h5 class="mt-0">Manage projects through contracts and milestones</h5>
                              <p>Lorem Ipsum has been the industry's standard dummy text ever</p>
                            </div>
                          </div>
                          <div class="media">
                            <div class="icon icon-04"></div>
                            <div class="media-body">
                              <h5 class="mt-0">Secure Payments</h5>
                              <p>Lorem Ipsum has been the industry's standard dummy text ever</p>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- why us section: end -->
<?php 
$categories_list =  App\Category::where('active',1)->orderBy('created_at', 'DESC')->take(8)->get();
?>

<!-- top freelancer section :start -->
<section class="top-freelancer">
    <div class="container">
        <div class="section-title">
            <h2>Find Top Freelancers</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sagittis ullamcorper turpis convallis elementum. Nam vestibulum odio sed dui condimentum porta. </p>
        </div>
        <div class="icon-cards">
            <div class="row">

            @foreach($categories_list as $Categoryname)
                <div class="col-md-3">
                    <div class="item">

                    @if(!empty($Categoryname->icon) && (file_exists( public_path().'/storage/admin/categories/'.$Categoryname->icon)))
                        <img class="card-img-top" src="{{asset('storage/admin/categories'.$Categoryname->icon)}}" style="height: 60px; width:60px;" alt="image-post-job">
                      @else
                      <div class="icon icon-default-cat"></div>
                      @endif

                      <?php
                      $category= $Categoryname->name;
                    ?>
                        <p><a href="{{route('homeservices',["#$category"])}}"> {{$Categoryname->name}} </a></p>
                    </div>
                </div>
                @endforeach
                <!-- <div class="col-md-3">
                    <div class="item">
                        <div class="icon icon-02"></div>
                        <p><a href="#">Design & Art</a></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="item">
                        <div class="icon icon-03"></div>
                        <p><a href="#">Content Writing & Translation</a></p>
                    </div>
                </div> -->
              
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <button type="button" onclick="location.href='{{route('homeservices')}}'"  class="btn btn-all-jobs">View our all JOB Category</button>
            </div>
        </div>
    </div>
</section>
<!-- top freelancer section :end -->


<!-- section how it works:start -->
<section class="how-it-work">
    <div class="container">
        <div class="section-title">
            <h2>How it Works</h2>
        </div>
        <div class="content-box">
            <div class="row">
                <div class="col-md-4 arrange pos-01">
                    <div class="item">
                     <div class="icon icon-01"></div>
                     <h2>Post a Job - Its free</h2>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                    </div>
                </div>
                <div class="col-md-4 arrange pos-02">
                    <div class="item">
                     <div class="icon icon-02 "></div>
                     <h2>Hire the bset match</h2>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                    </div>
                </div>
                <div class="col-md-4 arrange pos-03">
                    <div class="item">
                     <div class="icon icon-03 "></div>
                     <h2>Deal with your freelancer</h2>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                    </div>
                </div>
                 <div class="col-md-4 arrange pos-04">
                    <div class="item">
                     <div class="icon icon-04"></div>
                     <h2>Get it complate your work</h2>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                    </div>
                </div>
                <div class="col-md-4 arrange pos-05">
                    <div class="item">
                     <div class="icon icon-05"></div>
                     <h2>Secure payments always</h2>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section how it works:end -->


<!-- section work done: start-->
<section class="work-done">
    <div class="container">
        <div class="section-title">
            <h2>It's Easy to Get Work Done on Keyoxa</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sagittis ullamcorper turpis convallis elementum. Nam vestibulum odio sed dui condimentum porta. </p>
        </div>
        <div class="cards-layout">
            <div class="row">
                <div class="col-md-3">
                    <div class="card w-100">
                  
                       
                      
                      @if(!empty($post_a_job->image) && (file_exists( public_path().'/storage/cms-page-images/'.$post_a_job->image)))
                        <img class="card-img-top" src="{{asset('storage/cms-page-images'.$post_a_job->image)}}" style="height: 147px; width: 263px;" alt="image-post-job">
                      @else
                      <img class="card-img-top" src="{{asset('/images/user/img-post-job.jpg')}}" style="height: 147px;
    width: 263px;" alt="image-post-job">
                      @endif
                      <?php
                      $slug=$post_a_job->slug;
                      ?>
                      
                        <div class="card-body">
                          <h5 class="card-title">{{ str_limit(ucwords($post_a_job->heading), $limit = 20, $end = '...') }}</h5>
                          <p class="card-text">{{ str_limit(ucwords($post_a_job->short_description), $limit = 60, $end = '...' )}}</p>
                          <a href="{{route('userPublicProfile',"$slug")}}" class="btn btn-primary btn-read-more">Read More<i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
                        </div>
                      </div>
                </div>
                <div class="col-md-3">
                    <div class="card w-100">

                    @if(!empty($hire_freelancers->image) && (file_exists( public_path().'/storage/cms-page-images/'.$hire_freelancers->image)))
                        <img class="card-img-top" src="{{asset('storage/cms-page-images'.$hire_freelancers->image)}}" style="height: 147px; width: 263px;"  alt="image-post-job">
                      @else
                      <img class="card-img-top" src="{{asset('/images/user/img-hire-freelancer.jpg')}}" alt="image-post-job">
                      @endif

                      <?php
                      $slug=$hire_freelancers->slug;
                      ?>

                        <div class="card-body">
                          <h5 class="card-title">{{ str_limit(ucwords($hire_freelancers->heading), $limit = 20, $end = '...') }}</h5>
                          <p class="card-text">{{ str_limit(ucwords($hire_freelancers->short_description), $limit = 60, $end = '...')}}</p>
                          <a href="{{route('userPublicProfile',"$slug")}}" class="btn btn-primary btn-read-more">Read More<i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
                        </div>
                      </div>
                </div>
                <div class="col-md-3">
                    <div class="card w-100">

                    @if(!empty($get_work_done->image) && (file_exists( public_path().'/storage/cms-page-images/'.$get_work_done->image)))
                        <img class="card-img-top" src="{{asset('storage/cms-page-images'.$get_work_done->image)}}" style="height: 147px; width: 263px;"  alt="image-post-job">
                      @else
                      <img class="card-img-top" src="{{asset('/images/user/img-get-work-done.jpg')}}" alt="image-post-job">
                      @endif

                      <?php
                      $slug=$get_work_done->slug;
                      ?>

                        <div class="card-body">
                          <h5 class="card-title">{{ str_limit(ucwords($get_work_done->heading), $limit = 20, $end = '...')}}</h5>
                          <p class="card-text">{{str_limit(ucwords($get_work_done->short_description), $limit = 60, $end = '...') }}</p>
                          <a href="{{route('userPublicProfile',"$slug")}}" class="btn btn-primary btn-read-more">Read More<i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
                        </div>
                      </div>
                </div>
                <div class="col-md-3">
                    <div class="card w-100">

                    @if(!empty($make_secure_payments->image) && (file_exists( public_path().'/storage/cms-page-images/'.$make_secure_payments->image)))
                        <img class="card-img-top" style="height: 147px; width: 263px;"  src="{{asset('storage/cms-page-images'.$make_secure_payments->image)}}" alt="image-post-job">
                      @else
                      <img class="card-img-top" src="{{asset('/images/user/img-make-payments.jpg')}}" alt="image-post-job">
                      @endif

                      <?php
                      $slug=$make_secure_payments->slug;
                      ?>
                        
                        <div class="card-body">
                          <h5 class="card-title">{{ str_limit(ucwords($make_secure_payments->heading), $limit = 20, $end = '...')}}</h5>
                          <p class="card-text">{{ str_limit(ucwords($make_secure_payments->short_description), $limit = 60, $end = '...')}}</p>
                          <a href="{{route('userPublicProfile',"$slug")}}" class="btn btn-primary btn-read-more">Read More<i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section work done: end-->



<!-- section-find & hire:start -->
<section class="find-and-hire">
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-12">
                    <h2 class="title">Find & Hire Expert Freelancers</h2>
                </div>
                <div class="col-md-8">
                    <p class="text">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                </div>
                <div class="col-md-4">
                    <button class="white-nm-btn" onclick="location.href='{{route('showLogin')}}'" type="button">Post a Project</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section-find & hire:end -->


@endsection
@push('custom-scripts')

@endpush