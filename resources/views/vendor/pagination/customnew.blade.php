
                     
                

@if ($paginator->hasPages())
    <ul class="pagination  justify-content-end pagination-sm flex-wrap" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled page-item" aria-disabled="true" aria-label="@lang('pagination.previous')">
            <a class="page-link"> <i class="fi-flaticon flaticon-angle-pointing-to-left"></i> Previous</a>
            </li>
        @else
            <li class="page-item">
                <a  class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"> <i class="fi-flaticon flaticon-angle-pointing-to-left"></i> Previous</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li  class="disabled page-item" aria-disabled="true"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active page-item" aria-current="page"><a class="page-link">{{ $page }}</a></li>
                    @else
                        <li  class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li  class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"> Next <i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
            </li>
        @else
            <li class="disabled page-item" aria-disabled="true" aria-label="@lang('pagination.next')">
            <a class="page-link"> Next <i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
            </li>
        @endif
    </ul>
@endif


<!-- <div class="pagination-col mt-4 mb-3">
                    <nav aria-label="page navigation">
                      <ul class="pagination justify-content-end pagination-sm flex-wrap">
                        <li class="page-item disabled">
                          <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="fi-flaticon flaticon-angle-pointing-to-left"></i> Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>                       
                        <li class="page-item active" aria-current="page">
                          <a class="page-link" href="#">6 <span class="sr-only">(current)</span></a>
                        </li>                      
                        <li class="page-item"><a class="page-link" href="#">7</a></li>
                        <li class="page-item"><a class="page-link" href="#">8</a></li>
                        <li class="page-item"><a class="page-link" href="#">9</a></li>
                        <li class="page-item"><a class="page-link" href="#">10</a></li>
                        <li class="page-item">
                          <a class="page-link" href="#">Next <i class="fi-flaticon flaticon-angle-arrow-pointing-to-right"></i></a>
                        </li>
                      </ul>
                    </nav>
                  </div> -->



                  