<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAndroididToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('android_deviceid',300)->after('remember_token')->nullable();
            $table->string('android_version',30)->after('android_deviceid')->nullable();
            $table->string('ios_deviceid',300)->after('android_version')->nullable();
            $table->string('ios_version',30)->after('ios_deviceid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('android_deviceid');
            $table->dropColumn('android_version');
            $table->dropColumn('ios_deviceid');
            $table->dropColumn('ios_version');
        });
    }
}
