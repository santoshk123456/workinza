<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDisputeUserWalletHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_wallet_histories', function (Blueprint $table) {
            $table->boolean('is_dispute')->default(0)->after('payment_status')->comment('0-> No, 1-> Yes');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_wallet_histories', function (Blueprint $table) {
            $table->dropColumn('is_dispute');
        });
    }
}
