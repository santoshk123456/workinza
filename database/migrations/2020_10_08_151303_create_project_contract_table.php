<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_contracts', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->foreignId('project_id')->constrained('projects');
            $table->foreignId('project_proposal_id')->constrained('project_proposals');
            $table->foreignId('client_id')->constrained('users');
            $table->foreignId('freelancer_id')->constrained('users');
            $table->double('amount', 10, 2);
            $table->unsignedTinyInteger('status')->length(2)->comment('0-> InProgress, 1-> Completed, 2-> Disputed');
            $table->datetime('deleted_at')->nullable();
            $table->unsignedTinyInteger('freelancer_acceptance')->length(2)->comment('0-> Not Accepted, 1-> Accepted');
            $table->unsignedTinyInteger('client_acceptance')->length(2)->comment('0-> Not Accepted, 1-> Accepted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_contract');
    }
}
