<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_plans', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->tinyInteger('user_type')->comment('0->client, 1->freelancer');
            $table->string('name');
            $table->double('monthly_amount',8,2);
            $table->double('yearly_amount',8,2);
            $table->double('setup_charge',8,2);
            $table->unsignedInteger('number_of_projects')->nullable();
            $table->unsignedInteger('number_of_proposals')->nullable();
            $table->unsignedInteger('number_of_products')->nullable();
            $table->double('service_percentage',6,2)->nullable();
            $table->boolean('chat_feature')->comment('0->not available, 1->available');
            $table->tinyInteger('active')->comment('0->not active, 1->active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_plans');
    }
}
