<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('type');
            $table->dropColumn('timeline_count');
            $table->dropColumn('timeline_type');
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->longText('description')->after('location');
            $table->boolean('type')->comment('0->Fixed Rate, 1->Hourly Rate')->after('title');
            $table->string('timeline_type', 50)->after('amount')->comment('Possible values : Days, Weeks, Months , Hours');
            $table->unsignedBigInteger('timeline_count')->after('timeline_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('type');
            $table->dropColumn('timeline_count');
            $table->dropColumn('timeline_type');
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->longText('description')->after('location');
            $table->boolean('type')->comment('0->Fixed Rate, 1->Hourly Rate')->after('title');
            $table->string('timeline_type', 50)->after('amount')->comment('Possible values : Days, Weeks, Months , Hours');
            $table->unsignedBigInteger('timeline_count')->after('timeline_type');
        });
    }
}
