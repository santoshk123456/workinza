<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceFeeTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->double('service_percentage',10,2)->after('type')->comment('It may be service percantage or Transaction fee percentage')->nullable();
            $table->double('service_charge',10,2)->after('service_percentage')->comment('It may be service charge or Transaction fee ')->nullable();
            $table->double('total_amount',10,2)->after('service_charge')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('service_percentage');
            $table->dropColumn('service_charge');
            $table->dropColumn('total_amount');
        });
    }
}
