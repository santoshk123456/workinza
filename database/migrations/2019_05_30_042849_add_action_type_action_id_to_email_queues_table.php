<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActionTypeActionIdToEmailQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('email_queues', function (Blueprint $table) {
            //
            $table->string('action_type',100)->after('id');
            $table->integer('action_id')->after('action_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_queues', function (Blueprint $table) {
            //
            $table->dropColumn('action_type');
            $table->dropColumn('action_id');
        });
    }
}
