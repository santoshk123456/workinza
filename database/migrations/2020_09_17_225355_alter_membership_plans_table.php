<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMembershipPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('membership_plans', function (Blueprint $table) {
            $table->dropColumn('monthly_amount');
            $table->dropColumn('yearly_amount');
        });

        Schema::table('membership_plans', function (Blueprint $table) {
            $table->double('monthly_amount',8,2)->nullable()->after('name');
            $table->double('yearly_amount',8,2)->nullable()->after('monthly_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('membership_plans', function (Blueprint $table) {
            $table->dropColumn('monthly_amount');
            $table->dropColumn('yearly_amount');
        });

        Schema::table('membership_plans', function (Blueprint $table) {
            $table->double('monthly_amount',8,2)->after('name');
            $table->double('yearly_amount',8,2)->after('monthly_amount');
        });
    }
}
