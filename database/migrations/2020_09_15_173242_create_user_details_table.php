<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('company_name', 100)->nullable();
            $table->string('paypal_email', 150)->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('set null');
            $table->string('state', 100)->nullable();
            $table->string('city', 100)->nullable();
            $table->mediumText('address_line_1')->nullable();
            $table->mediumText('address_line_2')->nullable();
            $table->string('zipcode', 15)->nullable();
            $table->string('webiste_url')->nullable();
            $table->unsignedBigInteger('industry_id')->nullable();
            $table->foreign('industry_id')->references('id')->on('industries')->onDelete('set null');
            $table->mediumText('profile_headline')->nullable();
            $table->mediumText('company_tagline')->nullable();
            $table->date('established_since')->nullable();
            $table->text('user_history')->nullable();
            $table->string('corporate_presentation', 100)->nullable();
            $table->string('video_urls')->nullable();
            $table->unsignedBigInteger('years_of_experience')->nullable();
            $table->string('referral_code', 100)->unique();
            $table->unsignedBigInteger('referral_user_id')->nullable();
            $table->foreign('referral_user_id')->references('id')->on('users');
            $table->boolean('subscription_updates')->default(false)->comment('0-> not subscribed, 1-> subscribed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
