<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('project_milestone_id')->nullable();
            $table->unsignedBigInteger('project_contract_id')->nullable();
            $table->unsignedBigInteger('user_subscription_id')->nullable();
            $table->foreign('user_subscription_id')->references('id')->on('user_subscriptions')->onDelete('cascade');
            $table->tinyInteger('type')->length(5)->comment('0-> arbitration, 1-> project payment, 2-> cancellation, 3-> refund, 4-> bonus, 5-> subscription payment');
            $table->double('amount',8,2);
            $table->unsignedInteger('status')->default(0)->comment('0-> pending, 1-> paid, 2-> rejected');
            $table->dateTime('paid_on')->nullable();
            $table->dateTime('due_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
