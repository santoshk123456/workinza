<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->tinyInteger('type')->comment('0->Fixed Rate, 1->Hourly Rate');
            $table->string('title',150);
            $table->text('description');
            $table->string('location',150);
            $table->double('budget_from',10,2);
            $table->double('budget_to',10,2);
            $table->double('amount',10,2)->nullable();
            $table->string('timeline_type',50)->comment('Possible values : Days, Weeks, Month , Hour');
            $table->double('timeline_count') ;
            $table->datetime('bid_start_date');
            $table->datetime('bid_end_date');
            $table->datetime('expected_start_date');
            $table->datetime('expected_end_date');
            $table->tinyInteger('status')->default(0)->comment('0->New,1->Active,2->Assigned,3->InProgress,4=>Completed,5=>Closed,6=>Disputed');
            $table->boolean('hide_from_public')->default(0)->comment('0=>Visible,1=>Hide');
            $table->boolean('active')->default(1)->comment('0=>Inactive,1=>Active');
            $table->text('inactive_reason')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
