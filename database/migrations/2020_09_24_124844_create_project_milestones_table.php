<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     
        Schema::create('project_milestones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id')->unsigned()->index();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('milestone_name', 191);
            $table->double('milestone_amount', 10, 2);
            $table->longText('milestone_description');
            $table->tinyInteger('status')->length(7)->comment('0 -> Not Started, 1 -> Activated, 2 -> In Progress, 3 -> Completed (In Review), 4 -> Approve, 5 -> Paid, 6 -> Rejected');
            $table->timestamps();
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_milestones');
    }
}
