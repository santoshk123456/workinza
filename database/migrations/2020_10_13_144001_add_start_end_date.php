<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStartEndDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_proposal_milestones', function (Blueprint $table) {
            $table->datetime('milestone_start_date')->after('milestone_amount');
            $table->datetime('milestone_end_date')->after('milestone_start_date');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_proposal_milestones', function (Blueprint $table) {
            $table->dropColumn('milestone_start_date');
            $table->dropColumn('milestone_end_date');
        });
    }
}
