<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('mobile_number');
            $table->dropColumn('profile_image');
            $table->dropColumn('active');
            $table->dropColumn('email_verified_at');
            $table->dropColumn('android_deviceid');
            $table->dropColumn('android_version');
            $table->dropColumn('ios_deviceid');
            $table->dropColumn('ios_version');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('user_type')->comment('0-> client, 1 -> freelancer')->after('uuid');
            $table->boolean('organization_type')->comment('0-> company, 1 -> individual')->after('user_type');
            $table->string('full_name', 100)->after('username');
            $table->string('country_code', 5)->nullable()->after('password');
            $table->string('phone', 20)->nullable()->after('country_code');
            $table->double('wallet_balance', 10, 2)->default(0)->after('phone');
            $table->string('profile_image')->nullable()->after('wallet_balance');
            $table->timestamp('email_verified_at')->nullable()->after('profile_image');
            $table->timestamp('phone_verified_at')->nullable()->after('email_verified_at');
            $table->unsignedTinyInteger('approved')->length(2)->after('phone_verified_at')->comment('0 -> Pending, 1 -> Approved, 2-> Rejected')->default(0);
            $table->timestamp('approved_at')->nullable()->after('approved');
            $table->boolean('active')->comment('0 -> Blocked, 1 -> Active')->after('approved_at')->default(1);
            $table->unsignedTinyInteger('step')->length(6)->after('active')->comment('1 -> Registration, 2-> Basic Details, 3-> Security Question, 4 -> Accept terms, 5-> Purchased a plan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('user_type');
            $table->dropColumn('organization_type');
            $table->dropColumn('full_name');
            $table->dropColumn('country_code');
            $table->dropColumn('phone');
            $table->dropColumn('wallet_balance');
            $table->dropColumn('profile_image');
            $table->dropColumn('email_verified_at');
            $table->dropColumn('phone_verified_at');
            $table->dropColumn('approved');
            $table->dropColumn('approved_at');
            $table->dropColumn('active');
            $table->dropColumn('step');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->after('username');
            $table->string('last_name')->after('first_name');
            $table->string('mobile_number')->after('last_name');
            $table->string('profile_image')->after('mobile_number');
            $table->boolean('active')->after('profile_image');
            $table->timestamp('email_verified_at')->after('email');
            $table->string('android_deviceid')->after('remember_token');
            $table->string('android_version')->after('android_deviceid');
            $table->string('ios_deviceid')->after('android_version');
            $table->string('ios_version')->after('ios_deviceid');
        });
    }
}
