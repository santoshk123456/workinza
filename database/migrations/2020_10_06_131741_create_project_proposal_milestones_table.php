<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectProposalMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_proposal_milestones', function (Blueprint $table) {
            $table->id();
            $table->foreignId('project_proposal_id')->constrained('project_proposals');
            $table->string('milestone_name', 191);
            $table->longText('milestone_description');
            $table->double('milestone_amount', 10, 2)->nullable()->default(123.4567);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_proposal_milestones');
    }
}
