<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitedClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invited_clients', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('freelancer_id');
            $table->foreign('freelancer_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('user_name',100)->nullable();
            $table->string('email',100);
            $table->string('country_code',5)->nullable();
            $table->string('mobile_number',15)->nullable();
            $table->boolean('user_type')->default(0)->comment('0->Company, 1->individual');
            $table->datetime('joined_date')->nullable();
            $table->boolean('status')->default(0)->comment('0->Pending, 1->joined');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invited_clients');
    }
}
