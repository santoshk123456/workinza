<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveForeignKeyFromUserRatings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_ratings', function (Blueprint $table) {

            $table->dropForeign(['rating_question_id']);
            $table->dropColumn('rating_question_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_ratings', function (Blueprint $table) {
            $table->unsignedBigInteger('rating_question_id');
            $table->foreign('rating_question_id')->references('id')->on('rating_questions')->onDelete('cascade');
        });
    }
}
