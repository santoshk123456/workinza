<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFieldsOfPagesTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->string("meta_title")->nullable(true)->change();
            $table->string("meta_description")->nullable(true)->change();
            $table->string("meta_keywords")->nullable(true)->change();
            $table->string("short_description")->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->string("meta_title")->nullable(false)->change();
            $table->string("meta_description")->nullable(false)->change();
            $table->string("meta_keywords")->nullable(false)->change();
            $table->string("short_description")->nullable(false)->change();
        });
    }
}
