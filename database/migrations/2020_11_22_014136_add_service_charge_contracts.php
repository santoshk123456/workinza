<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceChargeContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_contracts', function (Blueprint $table) {
            //
            $table->double('freelancer_service_charge', 8, 2)->nullable()->comment('Service charge in percentage when freelancer accept contract');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_contracts', function (Blueprint $table) {
            $table->dropColumn('freelancer_service_charge');
        });
    }
}
