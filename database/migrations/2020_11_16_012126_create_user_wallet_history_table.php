<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserWalletHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_wallet_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('transaction_id')->nullable();
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->boolean('transaction_type')->comment('0-> credit, 1-> debit');
            $table->double('amount',8,2);
            $table->double('new_balance',8,2)->comment('New wallet balance of user');
            $table->mediumText('notes');
            $table->tinyInteger('type')->length(5)->comment('0-> membership, 1-> wallet_recharge, 2-> milestone_payments, 3-> referrals, 4-> bonus, 5-> product');
            $table->tinyInteger('payment_status')->length(2)->comment('0-> Pending, 1-> Paid, 2-> Failed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_wallet_history');
    }
}
