<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReferralBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_wallet_histories', function (Blueprint $table) {
            $table->boolean('referral_or_bonus')->nullable()->comment('0-> Referral, 1-> Bonus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_wallet_histories', function (Blueprint $table) {
            $table->dropColumn('referral_or_bonus');
        });
    }
}
