<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServiceCharge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_wallet_histories', function (Blueprint $table) {
            $table->double('freelancer_service_charge', 8, 2)->nullable()->comment('Service Charge');
            $table->double('freelancer_service_percentage', 8, 2)->nullable()->comment('Service Percentage');
            $table->double('referral_bonus_percentage', 8, 2)->nullable()->comment('Referral/Bonus Percentage');
            $table->double('referral_bonus_charge', 8, 2)->nullable()->comment('Referral/Bonus Charge');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_wallet_histories', function (Blueprint $table) {
            $table->dropColumn('freelancer_service_charge');
            $table->dropColumn('freelancer_service_percentage');
            $table->dropColumn('referral_bonus_percentage');
            $table->dropColumn('referral_bonus_charge');
        });
    }
}
