<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToProjectChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_chats', function (Blueprint $table) {
            $table->tinyInteger('archive_status')->length(6)->after('dispute_id')->default('0')->nullable()->comment('0-> Not archived, 1-> archived');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_chats', function (Blueprint $table) {
          $table->dropColumn('archive_status');
        });
    }
}
