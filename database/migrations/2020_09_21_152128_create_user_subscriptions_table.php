<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('user_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users');
            $table->foreignId('membership_plan_id')->constrained('membership_plans');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->boolean('duration_type')->comment('0-> month, 1-> year');
            $table->double('amount', 8, 2);
            $table->double('setup_charge', 8, 2);
            $table->double('service_percentage', 6, 2);
            $table->double('product_commission', 6, 2)->nullable();
            $table->boolean('auto_renewal')->comment('0 -> not available, 1-> available');
            $table->boolean('active')->comment('0-> expired, 1-> active')->default(true);
            $table->timestamps();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_subscriptions');
    }
}
