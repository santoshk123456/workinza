<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->string('user_name',100)->unique()->after('id');
            $table->string('first_name',100)->after('user_name');
            $table->string('last_name',100)->after('first_name');
            $table->string('email',50)->change();
            $table->string('mobile_number',15)->unique()->after('last_name');
            $table->boolean('active')->default(1)->after('mobile_number');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->dropColumn('user_name');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('mobile_number',20);
            $table->dropColumn('active',1);
            $table->dropSoftDeletes();
        });
    }
}
