<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCorporatePresentationUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->dropColumn('corporate_presentation');
        });
        Schema::table('user_details', function (Blueprint $table) {
            $table->longText('corporate_presentation')->nullable()->after('user_history');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_details', function (Blueprint $table) {
            $table->dropColumn('corporate_presentation');
        });
        Schema::table('user_details', function (Blueprint $table) {
            $table->longText('corporate_presentation')->nullable()->after('user_history');
        });
    }
}
