<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_queues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('to_email');
            $table->string('to_name');
            $table->string('from_email');
            $table->string('from_name');
            $table->text('body');
            $table->string('attachments')->nullable();
            $table->string('subject', 100)->nullable();
            $table->boolean('sent_status');
            $table->dateTime('time_to_send')->nullable($value = true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_queues');
    }
}
