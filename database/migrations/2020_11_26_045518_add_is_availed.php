<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsAvailed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invited_clients', function (Blueprint $table) {
            $table->unsignedBigInteger('client_id')->nullable();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('is_availed')->default(0)->after('joined_date')->comment('0-> 6 step not completed,  1-> Not availed and 6 step completed 2-> Availed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invited_clients', function (Blueprint $table) {
            $table->dropColumn('client_id');
            $table->dropColumn('is_availed');
        });
    }
}
