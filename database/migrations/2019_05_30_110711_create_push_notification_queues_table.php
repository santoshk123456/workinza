<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushNotificationQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notification_queues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('action_type',100);
            $table->integer('action_id');
            $table->enum('device_type', ['ios', 'android']);
            $table->mediumText('device_id');
            $table->mediumText('message');
            $table->mediumText('extra_datas');
            $table->boolean('sent_status');
            $table->dateTime('time_to_send')->nullable($value = true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_notification_queues');
    }
}
