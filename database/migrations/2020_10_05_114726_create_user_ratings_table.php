<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('user_ratings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('project_id');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->unsignedBigInteger('rating_question_id');
            $table->foreign('rating_question_id')->references('id')->on('rating_questions')->onDelete('cascade');
            $table->unsignedBigInteger('rated_by_user_id');
            $table->foreign('rated_by_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('rating_to_user_id');
            $table->foreign('rating_to_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->Integer('rating');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_ratings');
    }
}
