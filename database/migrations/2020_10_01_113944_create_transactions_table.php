<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('user_subscription_id');
            $table->unsignedBigInteger('project_contract_id')->nullable();
            $table->unsignedBigInteger('project_milestone_id')->nullable();
            $table->foreign('project_milestone_id')->references('id')->on('project_milestones')->onDelete('cascade');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->tinyInteger('type')->length(5)->comment('0-> membership, 1-> wallet_recharge, 2-> milestone_payments, 3-> referrals, 4-> bonus, 5-> product');
            $table->double('amount',8,2);
            $table->boolean('transaction_type')->comment('0-> credit, 1-> debit');
            $table->string('payment_gateway',50)->nullable();
            $table->string('gateway_transactionID',100)->nullable();
            $table->mediumText('notes');
            $table->tinyInteger('payment_status')->length(2)->comment('0-> Pending, 1-> Paid, 2-> Failed');
            $table->text('payment_response');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
