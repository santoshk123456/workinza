<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disputes', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->unsignedBigInteger('client_user_id');
            $table->foreign('client_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('freelancer_user_id');
            $table->foreign('freelancer_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('project_contract_id')->nullable();
            $table->unsignedBigInteger('project_milestone_id')->nullable();
            $table->string('subject');
            $table->text('description');
            $table->double('amount',4,2);
            $table->boolean('status')->default(0)->comment('0->pending, 1->resolved');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disputes');
    }
}
