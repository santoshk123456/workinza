<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTablePushNotificationTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('push_notification_templates', function(Blueprint $table){
            $table->string('subject',100)->after('template');
            $table->text('value')->after('subject');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('push_notification_templates', function(Blueprint $table){
            $table->dropColumn('value');
            $table->dropColumn('subject');
        });
    }
}
