<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminPermissionRoleTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(EmailTemplatesTableSeeder::class);
        $this->call(SmsTemplatesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(TermsAndConditionsTableSeeder::class);
        $this->call(PaymentGatewaysTableSeeder::class);
    }
}
