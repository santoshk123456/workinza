<?php

use App\Page;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->truncate();
        DB::table('pages')->insert([
            'id' => '1',
            'slug' => 'about-us',
            'heading' => 'About Us',
            'meta_title' => 'About Us',
            'meta_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tortor metus, rutrum in est vitae, accumsan feugiat eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan neque id massa sodales consectetur. Nulla sodales porta tristique. Pellentesque interdum arcu a luctus imperdiet',
            'meta_keywords' => 'About Us',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tortor metus, rutrum in est vitae, accumsan feugiat eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan neque id massa sodales consectetur. Nulla sodales porta tristique. Pellentesque interdum arcu a luctus imperdiet',
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam tortor metus, rutrum in est vitae, accumsan feugiat eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce accumsan neque id massa sodales consectetur. Nulla sodales porta tristique. Pellentesque interdum arcu a luctus imperdiet',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('pages')->insert([
            'id' => '2',
            'slug' => 'contact-us',
            'heading' => 'Contact Us',
            'meta_title' => 'Contact Us',
            'meta_description' => 'Contact Us',
            'meta_keywords' => 'Contact Us',
            'short_description' => 'Contact Us',
            'content' => 'Contact Us',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('pages')->insert([
            'id' => '3',
            'slug' => 'privacy-policy',
            'heading' => 'Privacy policy',
            'meta_title' => 'Privacy policy',
            'meta_description' => 'Privacy policy',
            'meta_keywords' => 'Privacy policy',
            'short_description' => 'Privacy policy',
            'content' => 'Privacy policy',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('pages')->insert([       
            'id' => '4',
            'slug' => 'terms-and-conditions',
            'heading' => 'Terms And Conditions',
            'meta_title' => 'Terms And Conditions',
            'meta_description' => 'Terms And Conditions',
            'meta_keywords' => 'Terms And Conditions',
            'short_description' => 'Terms And Conditions',
            'content' => 'Terms And Conditions',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('pages')->insert([      
            'id' => '5', 
            'slug' => 'background-verification',
            'heading' => 'Background Verification',
            'meta_title' => 'Background Verification',
            'meta_description' => 'Background Verification',
            'meta_keywords' => 'Background Verification',
            'short_description' => 'Background Verification',
            'content' => 'Background Verification',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('pages')->insert([ 
            'id' => '6',      
            'slug' => 'post-a-job',
            'heading' => 'Post a Job',
            'meta_title' => 'Post a Job',
            'meta_description' => 'Post a Job',
            'meta_keywords' => 'Post a Job',
            'short_description' => 'Some quick example text to build on the card title and make up the bulk of the cards content.',
            'content' => 'Some quick example text to build on the card title and make up the bulk of the cards content.',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('pages')->insert([     
            'id' => '7',  
            'slug' => 'hire-freelancers',
            'heading' => 'Hire Freelancers',
            'meta_title' => 'Hire Freelancers',
            'meta_description' => 'Hire Freelancers',
            'meta_keywords' => 'Hire Freelancers',
            'short_description' => 'Some quick example text to build on the card title and make up the bulk of the cards content.',
            'content' => 'Some quick example text to build on the card title and make up the bulk of the cards content.',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('pages')->insert([   
            'id' => '8',    
            'slug' => 'get-work-done',
            'heading' => 'Get Work Done',
            'meta_title' => 'Get Work Done',
            'meta_description' => 'Get Work Done',
            'meta_keywords' => 'Get Work Done',
            'short_description' => 'Some quick example text to build on the card title and make up the bulk of the cards content.',
            'content' => 'Some quick example text to build on the card title and make up the bulk of the cards content.',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);


        DB::table('pages')->insert([   
            'id' => '9',    
            'slug' => 'make-secure-payments',
            'heading' => 'Make Secure Payments',
            'meta_title' => 'Make Secure Payments',
            'meta_description' => 'Make Secure Payments',
            'meta_keywords' => 'Make Secure Payments',
            'short_description' => 'Some quick example text to build on the card title and make up the bulk of the cards content.',
            'content' => 'Some quick example text to build on the card title and make up the bulk of the cards content.',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('pages')->insert([   
            'id' => '10',    
            'slug' => 'how-it-works',
            'heading' => 'How it Works',
            'meta_title' => 'How it Works',
            'meta_description' => 'How it Works',
            'meta_keywords' => 'How it Works',
            'short_description' => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
            'content' => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('pages')->insert([   
            'id' => '11',    
            'slug' => 'the-talent-marketplace',
            'heading' => 'The Talent Marketplace',
            'meta_title' => 'The Talent Marketplace',
            'meta_description' => 'The Talent Marketplace',
            'meta_keywords' => 'The Talent Marketplace',
            'short_description' => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
            'content' => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('pages')->insert([   
            'id' => '12',    
            'slug' => 'pre-packaged-projects',
            'heading' => 'pre-packaged Projects',
            'meta_title' => 'Pre-packaged Projects',
            'meta_description' => 'Pre-packaged Projects',
            'meta_keywords' => 'Pre-packaged Projects',
            'short_description' => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
            'content' => 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
            'active' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}