<?php

use Illuminate\Database\Seeder;

class PaymentGatewaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('payment_gateways')->truncate();
        DB::table('payment_gateways')->insert([
            'name' => 'STRIPE',
            'authentication_key' => 'pk_test_51HZVN1FPxmg246jZI7S16vsgsvKrh6xkcqIMpL2cwx9RKMHcc29J6Ii685OOR6J8nE6HxKHzBM9PzqV08i0BTFhc001ray929C',
            'authentication_token' => 'sk_test_51HZVN1FPxmg246jZRUthWxVVhvKCuiRn41fVgcqRqLta8Kq2qSH8agqpadtVrA7fshomJTOcKF56PkgEIrCeEvoH00h8k1g92h',
            'status' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
