<?php

use App\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use function Ramsey\Uuid\v2;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        DB::table('settings')->insert([
            'uuid' => 'qwerty-541',
            'type' => 'general',
            'name' => 'Application Admin Email',
            'key' => 'application_admin_email',
            'value_type' => 'email',
            'value' => 'nawas@yopmail.com',
            'description' => 'Application Admin Email',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'uuid' => 'dworak-123',
            'type' => 'general',
            'name' => 'Application Admin Name',
            'key' => 'application_admin_name',
            'value_type' => 'text',
            'value' => 'Keyoxa',
            'description' => 'Application Admin Name',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'uuid' => 'pkngrdzaq-951',
            'type' => 'general',
            'name' => 'Google Map API Key',
            'key' => 'google_api_key',
            'value_type' => 'text',
            'value' => 'AIzaSyBBJVkuMOIGpU67VJr6bARCU22wKXIZZHI',
            'description' => 'Api key for google maps',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'uuid' => 'azerty-321',
            'type' => 'mobile',
            'name' => 'Iphone Forceful Update',
            'key' => 'iphone_forceful_update',
            'value_type' => 'checkbox',
            'value' => 0,
            'description' => 'Iphone Forceful Update',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'uuid' => 'colemak-456',
            'type' => 'mobile',
            'name' => 'Android Forceful Update',
            'key' => 'android_forceful_update',
            'value_type' => 'checkbox',
            'value' => 0,
            'description' => 'Android Forceful Update',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'uuid' => 'jcuken-786',
            'type' => 'mobile',
            'name' => 'iphone Minimum Version',
            'key' => 'iphone_minimum_version',
            'value_type' => 'text',
            'value' => '',
            'description' => 'iphone Minimum Version',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'uuid' => 'maltron-258',
            'type' => 'mobile',
            'name' => 'Android Minimum Version',
            'key' => 'android_minimum_version',
            'value_type' => 'text',
            'value' => '',
            'description' => 'Android Minimum Version',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Background Verification Charge',
            'key' => 'back_ground_charge',
            'value_type' => 'number',
            'value' => '2',
            'description' => 'Background Verification Charge',
        ]);

        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'OTP Expiry Time',
            'key' => 'otp_expire_time',
            'value_type' => 'time',
            'value' => '01:00',
            'description' => 'Expire time of email otp',
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Contact Number',
            'key' => 'contact_number',
            'value_type' => 'number',
            'value' => '358956555',
            'description' => 'Support contact number',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Transaction Fee(%)',
            'key' => 'transaction_fee',
            'value_type' => 'number',
            'value' => '1',
            'description' => 'Transaction Fee (%)',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Milestone Payment Day Count',
            'key' => 'milestone_payment_day_count',
            'value_type' => 'number',
            'value' => '7',
            'description' => 'Get the all the Approved milestones for payment till this day',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Home Page Video URL',
            'key' => 'home_page_video',
            'value_type' => 'text',
            'value' => 'http://www.youtube.com/embed/n_dZNLr2cME?rel=0&hd=1',
            'description' => 'Enter Embedded Video URL',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Facebook URL',
            'key' => 'facebook_url',
            'value_type' => 'text',
            'value' => 'https://en-gb.facebook.com/login/',
            'description' => 'Facebook URL',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Twitter URL',
            'key' => 'twitter_url',
            'value_type' => 'text',
            'value' => 'https://twitter.com/login?lang=en',
            'description' => 'Twitter URL',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'LinkedIn URL',
            'key' => 'linkedin_url',
            'value_type' => 'text',
            'value' => 'https://www.linkedin.com/login',
            'description' => 'LinkedIn URL',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Pinterest URL',
            'key' => 'pinterest_url',
            'value_type' => 'text',
            'value' => 'https://www.pinterest.ca/login/',
            'description' => 'Pinterest URL',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Referral Percentage',
            'key' => 'referral_percentage',
            'value_type' => 'text',
            'value' => '10',
            'description' => 'Referral Percentage for deducting amount from service charge',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Bonus Limit',
            'key' => 'bonus_limit',
            'value_type' => 'text',
            'value' => '100000',
            'description' => 'Eligible amount for bonus',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Bonus Percentage',
            'key' => 'bonus_percentage',
            'value_type' => 'text',
            'value' => '10',
            'description' => 'Bonus Percentage for deducting amount from service charge',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Captcha Site Key',
            'key' => 'captcha_site_key',
            'value_type' => 'text',
            'value' => '6LeiFv4ZAAAAAHfnmQs9dGvUk-EdJpp7-vvI_gqV',
            'description' => 'Captcha Site Key',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('settings')->insert([
            'uuid' => DB::raw('UUID()'),
            'type' => 'general',
            'name' => 'Captcha Secret Key',
            'key' => 'captcha_secret_key',
            'value_type' => 'text',
            'value' => '6LeiFv4ZAAAAACvqOG8F9IOBcpIH6dWdQS2RadAr',
            'description' => 'Captcha Secret Key',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}