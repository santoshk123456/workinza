<?php

use App\EmailTemplate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('email_templates')->truncate(); 
        EmailTemplate::where('static_email_heading', 'ADMIN_CREATION')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'ADMIN_CREATION',
            'subject' => 'Keyoxa - Admin Creation',
            'description' => 'Admin Creation',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -60px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44); box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44);"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44); box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44);">
            <h1 style="font-size: 40px; margin: 0; padding: 0; font-weight: 500; text-align: left;"
                data-mce-style="font-size: 40px; margin: 0; padding: 0; font-weight: 500; text-align: left;">Welcome!
            </h1>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi
                [ADMIN_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Welcome to Keyoxa. Please find your <a title="Login" href="[ADMIN_LOGIN_LINK]" target="_blank"
                    rel="noopener" data-mce-href="[ADMIN_LOGIN_LINK]">Login</a> credentials below.</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Email: [ADMIN_EMAIL]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Password: [ADMIN_PASSWORD]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;"
                <="" p="">
            </p>
        </div>',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'USER_CREATION')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'USER_CREATION',
            'subject' => 'Keyoxa - User Creation',
            'description' => 'User Creation',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -60px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44); box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44);"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44); box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44);">
            <h1 style="font-size: 40px; margin: 0; padding: 0; font-weight: 500; text-align: left;"
                data-mce-style="font-size: 40px; margin: 0; padding: 0; font-weight: 500; text-align: left;">Welcome!
            </h1>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi
                [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Welcome to Keyoxa. Please find your <a title="Login" href="[USER_LOGIN_LINK]" target="_blank"
                    rel="noopener" data-mce-href="[USER_LOGIN_LINK]">Login</a> credentials below.</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Email: [USER_EMAIL]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Password: [USER_PASSWORD]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;"
                <="" p="">
            </p>
        </div>',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'WELCOME_EMAIL')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'WELCOME_EMAIL',
            'subject' => 'Welcome to Keyoxa',
            'description' => 'Admin send notiifcation to user once registered',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -60px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44); box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44);"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44); box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44);">
            <h1 style="font-size: 40px; margin: 0; padding: 0; font-weight: 500; text-align: left;"
                data-mce-style="font-size: 40px; margin: 0; padding: 0; font-weight: 500; text-align: left;">Welcome!
            </h1>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi
                [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Welcome to Keyoxa, the worlds largest marketplace for jobs and services. We have lot of job categories operating on the site and projects from all over the world waiting for you to work on.
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Please click <a title="Login" href="[USER_LOGIN_LINK]" target="_blank"
                    rel="noopener" data-mce-href="[USER_LOGIN_LINK]">here</a> to update your profile.</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;"
                <="" p="">
            </p>
        </div>',
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
        ]);
        // EmailTemplate::where('static_email_heading', 'ADMIN_RESET_PASSWORD')->delete();
        // DB::table('email_templates')->insert([
        //     'static_email_heading' => 'ADMIN_RESET_PASSWORD',
        //     'subject' => 'Admin Reset Password',
        //     'description' => 'Admin Reset Password',
        //     'template' => '<p style="line-height: 1.6; text-align: justify; padding-left: 15px;">Hi [ADMIN_NAME],</p>
        //     <p style="line-height: 1.6; text-align: justify; padding-left: 15px;">You are receiving this email because we received a password reset request for your account.</p>
        //     <p style="text-align: center;"><a class="button button-blue" style="padding-left: 15px; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[RESET_TOKEN]" target="_blank" rel="noopener">Reset Password</a></p>
        //     <p style="padding-left: 15px;">If you did not request a password reset, no further action is required.</p>',
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'updated_at' => date('Y-m-d H:i:s')
        // ]);

        EmailTemplate::where('static_email_heading', 'ADMIN_RESET_PASSWORD')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'ADMIN_RESET_PASSWORD',
            'subject' => 'Keyoxa - Admin Reset Password',
            'description' => 'Admin Reset Password',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -60px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44); box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44);"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44); box-shadow: 0px 1px 15px -5px rgba(0,0,0,0.44);">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi
                [ADMIN_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">You are receiving this email because we received a password reset request for your account.</p>
            <p style="text-align: center;"><a class="button button-blue" style="padding-left: 15px; font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[RESET_TOKEN]" target="_blank" rel="noopener">Reset Password</a></p>
            <p sstyle="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">If you did not request a password reset, no further action is required.</p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        EmailTemplate::where('static_email_heading', 'EMAIL_OTP')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'EMAIL_OTP',
            'subject' => 'Keyoxa - Account Verification Code',
            'description' => 'account otp',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">We are sharing a verification code to access your account. The code is valid for [OTP_HOUR] hour [OTP_MINUTE] minutes and usable only once.</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Your OTP: [LOGIN_OTP]</p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        EmailTemplate::where('static_email_heading', 'CONTRACT_INVITATIONS')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'CONTRACT_INVITATIONS',
            'subject' => 'Keyoxa - Contract Invitations',
            'description' => 'Client send contract invitations to freelancer',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[FROM_NAME] accepted your proposal for the project [PROJECT_NAME]. Please sign the contract by clicking below link </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[CONTRACT_LINK]" target="_blank" rel="noopener">Agree</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'ACCEPT_CONTRACT')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'ACCEPT_CONTRACT',
            'subject' => 'Keyoxa - Contract Acceptance',
            'description' => 'Freelancer send contract acceptance to client',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[FROM_NAME] accepted your contract for the project [PROJECT_NAME]. Please view the contract by clicking below link </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[CONTRACT_LINK]" target="_blank" rel="noopener">Agree</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'INVITE_FREELANCERS')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'INVITE_FREELANCERS',
            'subject' => 'Keyoxa - Project Invitations',
            'description' => 'Client send invitation to freelancer',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[FROM_NAME] invite you to join the project [PROJECT]. Please view the invitations by clicking below link </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[INVITATION_LINK]" target="_blank" rel="noopener">Accept Invitation</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'ACCEPT_INVITATION')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'ACCEPT_INVITATION',
            'subject' => 'Keyoxa - Accept Project Invitation',
            'description' => 'Freelancer send accept notification to client',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[FROM_NAME] accepted  your invitation for the project [PROJECT]. Please view the proposal details by clicking below link </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[INVITATION_LINK]" target="_blank" rel="noopener">View Proposal</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'APPROVE_USER')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'APPROVE_USER',
            'subject' => 'Keyoxa - Your background verification approved',
            'description' => 'Admin send notiifcation to user',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Your background verification has been completed successfully. Please login to enjoy our services. </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[USER_LOGIN_LINK]" target="_blank" rel="noopener">LOGIN</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'REJECT_USER')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'REJECT_USER',
            'subject' => 'Keyoxa - Your background verification rejected',
            'description' => 'Admin send notiifcation to user',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Your background verification has been rejected. Please update valid background details. </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[USER_LOGIN_LINK]" target="_blank" rel="noopener">LOGIN</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'NOTIFY_BACKGROUND_VERIFICATION')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'NOTIFY_BACKGROUND_VERIFICATION',
            'subject' => 'Keyoxa - New Background verification request',
            'description' => 'Notify admin for the background verification',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi Admin, </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">You have a new background verification request. Please find the details below: </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Name: [USER_NAME]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Email: [USER_EMAIL]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                User Type: [USER_TYPE]</p>
                <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[ADMIN_LOGIN_LINK]" target="_blank" rel="noopener">LOGIN</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'NOTIFY_ADMIN')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'NOTIFY_ADMIN',
            'subject' => 'Keyoxa - New Subscription',
            'description' => 'Notify admin for the enquiry from the customers',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi Admin, </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">We have a new user subscribed to the Keyoxa. Please find the details below: </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Name: [USER_NAME]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Email: [USER_EMAIL]</p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'NOTIFY_ADMIN_CONTACT')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'NOTIFY_ADMIN_CONTACT',
            'subject' => 'Keyoxa - Contact Us',
            'description' => 'Notify admin for the enquiry from the customers',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi Admin, </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">We have a new user contacted to the Keyoxa. Please find the details below: </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Name: [USER_NAME]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Email: [USER_EMAIL]</p>
                <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Phone: [USER_PHONE]</p>
                <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Company: [USER_COMPANY]</p>
                <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Message: [USER_MESSAGE]</p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'UPDATE_PROFILE_NOTIFICATION')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'UPDATE_PROFILE_NOTIFICATION',
            'subject' => 'Keyoxa - Request To Update Contact Information',
            'description' => 'Request admin to edit the contact information',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi Admin, </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[SUBJECT] </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Please find the details below: </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Name: [USER_NAME]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Email: [USER_EMAIL]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
                Phone: [PHONE]</p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'CREATE_CHAT')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'CREATE_CHAT',
            'subject' => 'Keyoxa - Chat Conversation',
            'description' => 'Chat conversation between user',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Your have new chat message from [FROM_USER]. Please find the message. </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;">[MESSAGE]</p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'ACTIVATE_MILESTONE')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'ACTIVATE_MILESTONE',
            'subject' => 'Keyoxa - Milestone Activated',
            'description' => 'Milestone Activation mail to freelancer',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[FROM_USER] has activated the milestone "[MILESTONE_NAME]" for the project [PROJECT]. </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[LOGIN_LINK]" target="_blank" rel="noopener">View Milestone</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'COMPLETE_MILESTONE')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'COMPLETE_MILESTONE',
            'subject' => 'Keyoxa - Milestone Completed',
            'description' => 'Milestone Completed mail to client',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[FROM_USER] has completed the milestone "[MILESTONE_NAME]" for the project [PROJECT]. </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[LOGIN_LINK]" target="_blank" rel="noopener">View Milestone</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'APPROVE_MILESTONE')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'APPROVE_MILESTONE',
            'subject' => 'Keyoxa - Milestone Approved',
            'description' => 'Milestone Approved mail to freelancer',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[FROM_USER] has approved the milestone "[MILESTONE_NAME]" for the project [PROJECT]. </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[LOGIN_LINK]" target="_blank" rel="noopener">View Milestone</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'REJECT_MILESTONE')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'REJECT_MILESTONE',
            'subject' => 'Keyoxa - Milestone Rejected',
            'description' => 'Milestone Rejected mail to freelancer',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[FROM_USER] has rejected the milestone "[MILESTONE_NAME]" for the project [PROJECT]. </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[LOGIN_LINK]" target="_blank" rel="noopener">View Milestone</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'MESSAGE_SEND')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'MESSAGE_SEND',
            'subject' => 'New Message',
            'description' => 'message between freelancer and client',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">You have got a new message from [FROM_NAME] for the project [PROJECT_NAME]. Please find the message below</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Message : [MESSAGE]</p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'INVOICE_GENERATION')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'INVOICE_GENERATION',
            'subject' => 'Keyoxa - Invoice generated',
            'description' => 'Invoice generated',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[MESSAGE]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Invoiced On : [INVOICE_DATE]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Amount : [AMOUNT]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[INVOICE_LINK]" target="_blank" rel="noopener">View Invoice</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        EmailTemplate::where('static_email_heading', 'REFER_PERSON')->delete();
        DB::table('email_templates')->insert([
            'static_email_heading' => 'REFER_PERSON',
            'subject' => 'Keyoxa - Refer a friend',
            'description' => 'Refer a person from freelancer side',
            'template' => '<div style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: 20px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;"
            data-mce-style="width: 530px; background-color: #fff; position: relative; text-align: left; height: auto; margin: -100px auto 0 auto; border-radius: 3px; padding: 30px; overflow: hidden; -webkit-box-shadow:none; box-shadow:none;">
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Hi [USER_NAME], </p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">[MESSAGE]</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Keyoxa is worlds largest market place for services, where you can get everything you need at an unbeatable value.</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">Sound too good to be true? Please click here to register by below button</p>
            <p style="line-height: 1.6; text-align: justify;" data-mce-style="line-height: 1.6; text-align: justify;">
            <p style="text-align: center;"><a class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #fff; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097d1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;" href="[INVITATION_LINK]" target="_blank" rel="noopener">Register</a></p>
            </div>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}