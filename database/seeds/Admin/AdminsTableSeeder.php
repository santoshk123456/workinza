<?php

use App\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'Super Admin',
            'email' => 'superadmin@yopmail.com',
            'mobile_number' => '9999999999',
            'uuid' => '871e68c0-53ab-42d1-8f52-cf47c3449c0f',
            'username' => 'superadmin',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Admin::first()->syncRoles(['super admin']);
        DB::table('admins')->insert([
            'name' => 'Admin',
            'email' => 'admin@yopmail.com',
            'mobile_number' => '8888888888',
            'uuid' => '971e68c0-53ab-42d1-8f52-cf47c3449c0f',
            'username' => 'admin',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Admin::where('email','admin@yopmail.com')->first()->syncRoles(['admin']);
        DB::table('admins')->insert([
            'name' => 'Manager',
            'email' => 'manager@yopmail.com',
            'mobile_number' => '7777777777',
            'uuid' => '171e68c0-53ab-42d1-8f52-cf47c3449c0f',
            'username' => 'manager',
            'password' => bcrypt('123456'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        Admin::where('email','manager@yopmail.com')->first()->syncRoles(['manager']);
    }
}