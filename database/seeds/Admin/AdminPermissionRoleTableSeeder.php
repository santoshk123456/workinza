<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminPermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        DB::table('roles')->truncate();

        DB::table('permissions')->truncate(); 

        app()['cache']->forget('spatie.permission.cache');
        $this->disableForeignKeys();
        Role::truncate();
        Permission::truncate();
        // Create Roles
        $superadmin = Role::create(['guard_name' => 'admin', 'name' => 'super admin']);
        $admin = Role::create(['guard_name' => 'admin', 'name' => 'admin']);
        $manager = Role::create(['guard_name' => 'admin', 'name' => 'manager']);
        // Create Permissions
        $permissions = array('list admin users','add admin users','edit admin users',
                             'view admin users','delete admin users','list clients',
                             'add clients','edit clients','view clients','delete clients',
                             'list freelancers','add freelancers','edit freelancers','view freelancers','delete freelancers',
                             'list pages','add pages','edit pages','view pages',
                             'delete pages','list frequently asked questions',
                             'add frequently asked questions',
                             'edit frequently asked questions',
                             'view frequently asked questions',
                             'delete frequently asked questions',
                             'list settings','add settings','edit settings',
                             'view settings','delete settings','list email queues',
                             'add email queues','edit email queues','view email queues',
                             'delete email queues','list sms queues',
                             'add sms queues','edit sms queues','view sms queues',
                             'delete sms queues',
                             'list email templates','add email templates',
                             'edit email templates','view email templates',
                             'delete email templates',
                             'add roles','edit roles','view roles','delete roles','list roles',
                             'list activity logs','list access logs',
                             'list industries','add industries','edit industries','delete industries',
                             'list subcategories','add subcategories','edit subcategories','delete subcategories','show subcategories',
                             'list skills','add skills','edit skills','delete skills','show skills',
                             'list categories','add categories','edit categories','delete categories','show categories',
                             'list projects','add projects','edit projects','delete projects','show projects',
                             'list membership plan','add membership plan','edit membership plan','delete membership plan','view membership plan',
                             'list security questions','add security questions','edit security questions','delete security questions','show security questions',
                             'list terms and conditions','add terms and conditions','edit terms and conditions','delete terms and conditions','view terms and conditions',
                             'list rating questions','add rating questions','edit rating questions','delete rating questions',
                             'list subscription payments','view subscription payments',
                             'list transactions', 'view transactions', 'edit transactions');
        foreach ($permissions as $permission) {
            Permission::create(['guard_name' => 'admin', 'name' => $permission]);
        }
        // Assign all permissions to Super Admin
        $superadmin->givePermissionTo(Permission::where('guard_name', 'admin')->get()->all());
        // Assign all permissions to Admin except the power to delete
        $admin->givePermissionTo(Permission::where('guard_name', 'admin')->where('name','NOT LIKE','delete %')->get()->all());
        // Assign the permissions to manager only to view listing and detail page
        $manager->givePermissionTo(Permission::where('guard_name', 'admin')->where('name','LIKE','list %')->orWhere('name','LIKE','view %')->get()->all());
        $this->enableForeignKeys();
    }
}
