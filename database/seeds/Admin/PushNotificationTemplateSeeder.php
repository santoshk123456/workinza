<?php

use App\PushNotificationTemplate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PushNotificationTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('push_notification_templates')->truncate();
        DB::table('push_notification_templates')->insert([
            'process_name' => 'USER_REGISTRATION',
            'description' => 'registration',
            'template' => 'Dear [name], Welcome To Keyoxa.',
            'subject' => 'User Registration',
            'value' => 'Application Admin Email',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
