<?php

use App\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactUsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contact_us')->truncate();
        DB::table('contact_us')->insert([
            'name' => '2Base Technologies',
            'address' => 'Stadium Bypass Rd, Palakkad, Kerala, India',
            'latitude' => '10.7690502',
            'longitude' => '76.661339',
            'contact_number' => '9517538264',
            'contact_email' => 'info@yopmail.com',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}