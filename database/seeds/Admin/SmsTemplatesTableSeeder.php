<?php

use App\SmsTemplate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SmsTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sms_templates')->insert([
            'process_name' => 'ADMIN_CREATION',
            'description' => 'Create new admin',
            'template' => 'Hi [ADMIN_NAME],  Your account has been created successfully.....',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}